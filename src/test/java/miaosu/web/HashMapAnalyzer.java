package miaosu.web;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * HashMap 代码分析
 */
@Slf4j
public class HashMapAnalyzer {

    @Test
    public void testHash(){
        String test = "name";
        int hashCode = hash(test);
    }

    private final int hash(Object key) {
        int h;

        if(key==null){
            return 0;
        }else{
            Integer temp = key.hashCode();
            log.info("key 对应的hashCode 十进制{}  二进制{}",temp,Integer.toBinaryString(temp));
            h =temp>>>16;
            log.info("key 无符号右移16位的结果 十进制{}  二进制{}",h,Integer.toBinaryString(h));
            Integer result = h^temp;
            log.info("两者异或 十进制{}  二进制{}",result,Integer.toBinaryString(result));
            return result;
        }
    }
}
