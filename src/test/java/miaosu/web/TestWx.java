package miaosu.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.thoughtworks.xstream.XStream;
import miaosu.dao.model.*;
import miaosu.utils.HttpsUtil;
import miaosu.utils.LoadPropertiesUtil;
import miaosu.wechat.WechatAccessToken;
import miaosu.wechat.WechatUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class TestWx {
    @Autowired
    private WechatAccessToken wxAccessToken;

    @Autowired @Qualifier("commonMap")
    protected Map<String, String> commonMap;
  @Test
    public void testToken(){
    System.out.println(WechatUtils.getAccessToken());
    System.out.println(WechatUtils.getAccessToken());
    System.out.println(WechatUtils.getAccessToken());
  }

    /**
     * 发送模板消息
   */
  @Test
  public void sendTemplateMessage(){
      String at = WechatUtils.getAccessToken();
      String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+at;
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("touser", "oDqEy6p0jFeDY8vJL5Kjsv1wrxpY");   // openid
      jsonObject.put("template_id", "giTK6c7uqT82gyoFheufpvNPQpGNZUyG0WQAJrb0TN8");
      jsonObject.put("url", "http://www.baidu.com");

      String hotelName = "XXX酒店";

      JSONObject data = new JSONObject();
      JSONObject first = new JSONObject();
      first.put("value", hotelName+"来新订单了,请及时处理！\r\n\n");
      first.put("color", "#173177");
      JSONObject keyword1 = new JSONObject();
      keyword1.put("value", hotelName+"\r\n");
      keyword1.put("color", "#173177");
      JSONObject keyword2 = new JSONObject();
      keyword2.put("value", "轻享大床房\r\n");
      keyword2.put("color", "#173177");
      JSONObject keyword3 = new JSONObject();
      keyword3.put("value", "2020-11-27 ~ 2020-11-28\r\n");
      keyword3.put("color", "#173177");
      JSONObject keyword4 = new JSONObject();
      keyword4.put("value", "1间\r\n");
      keyword4.put("color", "#173177");
      JSONObject keyword5 = new JSONObject();
      keyword5.put("value", "246.4\r\n\n");
      keyword5.put("color", "#173177");

      JSONObject remark = new JSONObject();
      remark.put("value", "入住人赵志坚");
      remark.put("color", "#173177");

      data.put("first",first);
      data.put("keyword1",keyword1);
      data.put("keyword2",keyword2);
      data.put("keyword3",keyword3);
      data.put("keyword4",keyword4);
      data.put("keyword5",keyword5);
      data.put("remark",remark);

      jsonObject.put("data", data);

      String result = HttpsUtil.doPost(url,"","",jsonObject.toJSONString());
      System.out.println(result);
      int errcode = JSON.parseObject(result).getIntValue("errcode");
      if(errcode == 0){
          // 发送成功
          System.out.println("发送成功");
      } else {
          // 发送失败
          System.out.println("发送失败");
      }
      }


      @Test
      public void testPro(){
          Properties props = LoadPropertiesUtil.loadProperties("wechat.properties");
          Set<String> set = props.stringPropertyNames();
          System.out.println(StringUtils.join(set.toArray()));
          String apiKey = props.getProperty("TOKEN");
          System.out.println(props.getProperty("SEND_TEMPLATE_MESSAGE_URL"));
          System.out.println(apiKey);
          System.out.println(commonMap.get("wechat.token"));

      }

      @Test
    public void testMsg(){
          Map<String,String> map = new HashMap<>();
          map.put("ToUserNmae","to");
          map.put("FromUserName","from");
          map.put("MsgType","type");
          WechatTextMessage tm = new WechatTextMessage(map,"走一个");


          XStream stream = new XStream();

          stream.processAnnotations(WechatTextMessage.class);
          String xml = stream.toXML(tm);

          System.out.println(xml);

      }

      @Test
    public void testButton(){
            //菜单对象
          WechatMenuButton bt = new WechatMenuButton();
            //第一个一级菜单
            bt.getButton().add(new WechatClickButton("一级点击","1"));
            //第二个一级菜单
            bt.getButton().add(new WechatViewButton("一级tz","www.baidu.com"));
            //创建第三个一级菜单
          WechatSubButton sb = new WechatSubButton("子菜单");
          sb.getSub_button().add(new WechatViewButton("公司简介","www.baidu.com"));
          sb.getSub_button().add(new WechatClickButton("我是点击事件","32"));
          //加入第三个一级菜单
          bt.getButton().add(sb);
          JSONObject jsonObject = new JSONObject();
//          jsonObject.put("bt",bt);

          System.out.println(jsonObject.toJSONString(bt));
      }
}
