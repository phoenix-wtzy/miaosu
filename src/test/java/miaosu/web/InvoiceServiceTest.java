package miaosu.web;

import com.vpiaotong.openapi.util.SecurityUtil;
import miaosu.SpringBootWebApplication;
import miaosu.config.InvoiceConfig;
import miaosu.dao.auto.BInvoiceMapper;
import miaosu.dao.model.BInvoice;
import miaosu.invoice.InvoiceBlueResp;
import miaosu.invoice.InvoiceServiceUtils;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.vo.Result;
import miaosu.utils.JsonUtils;
import miaosu.voice.AccountInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.Map;

/**
 * 发票功能测试
*/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootWebApplication.class)
@WebAppConfiguration
public class InvoiceServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(InvoiceServiceTest.class);


    @Autowired
    BInvoiceMapper bInvoiceMapper;

    @Test
    public void testInvoice(){
        BInvoice bInvoice = bInvoiceMapper.selectByPrimaryKey(2L);

        InvoiceBlueResp  invoiceBlueResp = InvoiceServiceUtils.genetatorInvoice(bInvoice, true,InvoiceBlueResp.class);

        logger.info("响应结果信息：{}", JsonUtils.objectToJson(invoiceBlueResp));
    }


//aHR0cDovL2Zwa2oudGVzdG53LnZwaWFvdG9uZy5jbi90cC9zY2FuLWludm9pY2UvaW5kZXgvZG1wNmNUWjVWRVkxUjJwc1dDdHRLeXRTV0dZeFpubHlRVUpaV0ZaRmVVbz0ucHQ=




}
