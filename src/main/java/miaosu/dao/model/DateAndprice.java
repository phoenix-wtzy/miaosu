package miaosu.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @Date: 2021/3/11 15:27
 * @Description: 多个日期内对应的房价组合
 */
public class DateAndprice {

	/**
	 * 售卖日期
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
	private Date sellDate;

	//日期对应房价
	private BigDecimal price;

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
