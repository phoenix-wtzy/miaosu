package miaosu.dao.model;

import java.io.Serializable;

/**
 *评价体系表
 */
public class EvaluationSystem implements Serializable {
	//主键id
	private Long id;
	//项目名称
	private String project_name;
	//项目代码
	private String project_code;
	//定值(状态)
	private Integer value;
	//最小值
	private Integer minValue;
	//最大值
	private Integer maxValue;
	//分值
	private Integer score;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public String getProject_code() {
		return project_code;
	}

	public void setProject_code(String project_code) {
		this.project_code = project_code;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getMinValue() {
		return minValue;
	}

	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}

	public Integer getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "EvaluationSystem{" +
				"id=" + id +
				", project_name='" + project_name + '\'' +
				", project_code='" + project_code + '\'' +
				", value=" + value +
				", minValue=" + minValue +
				", maxValue=" + maxValue +
				", score=" + score +
				'}';
	}
}
