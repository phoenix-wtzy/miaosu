package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2020/8/10 13:59
 * @Description:
 */
public class GongYingShang_hotelList {
	private String hotel_name;//酒店名字
	private Long p_hotel_id;//酒店id
	private String settle_money;//结算金额
	private String commission_money;//技术服务费

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public Long getP_hotel_id() {
		return p_hotel_id;
	}

	public void setP_hotel_id(Long p_hotel_id) {
		this.p_hotel_id = p_hotel_id;
	}

	public String getSettle_money() {
		return settle_money;
	}

	public void setSettle_money(String settle_money) {
		this.settle_money = settle_money;
	}

	public String getCommission_money() {
		return commission_money;
	}

	public void setCommission_money(String commission_money) {
		this.commission_money = commission_money;
	}
}
