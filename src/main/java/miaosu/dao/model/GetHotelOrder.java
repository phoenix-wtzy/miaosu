package miaosu.dao.model;

/**
 * 获取爬虫抓取的数据对应的参数
 */
public class GetHotelOrder {

    private String channelOrderNo;//渠道单号,就是数据的订单号
    private Integer orderChannel;//订单渠道,需要根据订单渠道名称获取订单渠道id
    private String bookTime;//预定时间
    private String bookUser;//预定人
    //private Long hotel_id_selected;//酒店id,需要再写查询(根据平台酒店id获取)
    private String hotel_only_id;//平台酒店id
    // private Long hotelRoomTypeId;//房型id，需要再写查询(根据房间id获取)
    //private Long roomTypeId;//房间id
    private String p_hotel_room_type_name;//酒店平台上的房型名字;现在的格式:商务双床房(含双早)
    private Integer orderRoomCount;//预定了几间
    private String checkInTime;//入住时间
    private String checkOutTime;//离店时间
    private String orderPrice;//订单价格
    private Integer state;//订单状态,1 待确认,  2 已确认 ,  4 已取消
    private String bookRemark;//备注
    private Integer night;//间夜,入离时间差

    public String getChannelOrderNo() {
        return channelOrderNo;
    }

    public void setChannelOrderNo(String channelOrderNo) {
        this.channelOrderNo = channelOrderNo;
    }

    public Integer getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }

    public String getBookTime() {
        return bookTime;
    }

    public void setBookTime(String bookTime) {
        this.bookTime = bookTime;
    }

    public String getBookUser() {
        return bookUser;
    }

    public void setBookUser(String bookUser) {
        this.bookUser = bookUser;
    }

    public String getHotel_only_id() {
        return hotel_only_id;
    }

    public void setHotel_only_id(String hotel_only_id) {
        this.hotel_only_id = hotel_only_id;
    }

    public String getP_hotel_room_type_name() {
        return p_hotel_room_type_name;
    }

    public void setP_hotel_room_type_name(String p_hotel_room_type_name) {
        this.p_hotel_room_type_name = p_hotel_room_type_name;
    }

    public Integer getOrderRoomCount() {
        return orderRoomCount;
    }

    public void setOrderRoomCount(Integer orderRoomCount) {
        this.orderRoomCount = orderRoomCount;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getBookRemark() {
        return bookRemark;
    }

    public void setBookRemark(String bookRemark) {
        this.bookRemark = bookRemark;
    }

    public Integer getNight() {
        return night;
    }

    public void setNight(Integer night) {
        this.night = night;
    }
}