package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2020/7/6 13:57
 * @Description:
 */
public class Code {
    //递增主键
    private Long code_id;
    //邀请码
    private String code;
    //邀请码级别
    private String code_level;

    public Long getCode_id() {
        return code_id;
    }

    public void setCode_id(Long code_id) {
        this.code_id = code_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode_level() {
        return code_level;
    }

    public void setCode_level(String code_level) {
        this.code_level = code_level;
    }
}
