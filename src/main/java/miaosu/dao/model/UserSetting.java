package miaosu.dao.model;

public class UserSetting {
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 设置类型
     */
    private Integer settingType;

    /**
     * 设置值
     */
    private Long settingValue;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getSettingType() {
        return settingType;
    }

    public void setSettingType(Integer settingType) {
        this.settingType = settingType;
    }

    public Long getSettingValue() {
        return settingValue;
    }

    public void setSettingValue(Long settingValue) {
        this.settingValue = settingValue;
    }
}