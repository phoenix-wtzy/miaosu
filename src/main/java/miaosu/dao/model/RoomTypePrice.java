package miaosu.dao.model;

import java.io.Serializable;

/**
 * 房型房价表
 */
public class RoomTypePrice  implements Serializable {
	//主键id
	private Long id;
	//酒店ID
	private Long hotel_id;
	//房型名字
	private String room_type_name;
	//此房型平均房价
	private Integer room_price_average;
	//平均结算价
	private Integer settle_average;
	//间夜数
	private Integer nightC;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(Long hotel_id) {
		this.hotel_id = hotel_id;
	}

	public String getRoom_type_name() {
		return room_type_name;
	}

	public void setRoom_type_name(String room_type_name) {
		this.room_type_name = room_type_name;
	}

	public Integer getRoom_price_average() {
		return room_price_average;
	}

	public void setRoom_price_average(Integer room_price_average) {
		this.room_price_average = room_price_average;
	}

	public Integer getSettle_average() {
		return settle_average;
	}

	public void setSettle_average(Integer settle_average) {
		this.settle_average = settle_average;
	}

	public Integer getNightC() {
		return nightC;
	}

	public void setNightC(Integer nightC) {
		this.nightC = nightC;
	}

	@Override
	public String toString() {
		return "RoomTypePrice{" +
				"id=" + id +
				", hotel_id=" + hotel_id +
				", room_type_name='" + room_type_name + '\'' +
				", room_price_average=" + room_price_average +
				", settle_average=" + settle_average +
				", nightC=" + nightC +
				'}';
	}
}
