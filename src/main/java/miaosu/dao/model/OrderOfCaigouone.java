package miaosu.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @Date: 2021/3/18 13:51
 * @Description:订单每天的结算单价统计
 */
public class OrderOfCaigouone {
	private Long id;//主键id
	private Long order_id;//订单id
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date check_in_time;//入住时间
	private BigDecimal caigou_one; //每单采购价
	private Integer caigou_one_state;//订单状态:0-正常;  1-取消
	private String remark;//备注

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	public Date getCheck_in_time() {
		return check_in_time;
	}

	public void setCheck_in_time(Date check_in_time) {
		this.check_in_time = check_in_time;
	}

	public BigDecimal getCaigou_one() {
		return caigou_one;
	}

	public void setCaigou_one(BigDecimal caigou_one) {
		this.caigou_one = caigou_one;
	}

	public Integer getCaigou_one_state() {
		return caigou_one_state;
	}

	public void setCaigou_one_state(Integer caigou_one_state) {
		this.caigou_one_state = caigou_one_state;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
