package miaosu.dao.model;

public class HotelRefUser {
    /**
     * 酒店ID
     */
    private Long hotelId;

    /**
     * 用户ID
     */
    private Long userId;

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}