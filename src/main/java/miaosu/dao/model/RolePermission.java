package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2021/1/21 10:18
 * @Description:
 */
public class RolePermission {

	//角色id
	private Long role_id;

	//权限id
	private Long permission_id;

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public Long getPermission_id() {
		return permission_id;
	}

	public void setPermission_id(Long permission_id) {
		this.permission_id = permission_id;
	}
}
