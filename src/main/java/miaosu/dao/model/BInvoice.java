package miaosu.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class BInvoice {
    private Long id;

    /**
     * 酒店id
     */
    private Long hotelid;

    /**
     * 购买方姓名
     */
    private String buyername;

    /**
     * 购买方税号
     */
    private String buyertaxpayernum;

    /**
     * 购买方地址
     */
    private String buyeraddress;

    /**
     * 购买方电话
     */
    private String buyertel;

    /**
     * 购买方银行名称
     */
    private String buyerbankname;

    /**
     * 购买方银行开户人
     */
    private String buyerbankpeople;

    /**
     * 购买方银行账户
     */
    private String buyerbankaccount;

    /**
     * 收票人名称
     */
    private String takername;

    /**
     * 收票人手机号
     */
    private String takertel;

    /**
     * 收票人详细地址
     */
    private String takerdetailaddress;

    /**
     * 收票人地址
     */
    private String takeraddress;

    /**
     * 收票人邮箱
     */
    private String takeremail;

    private String itemname;

    private String goodsname;

    private String remark;

    private Integer quantity;

    private BigDecimal unitprice;

    private BigDecimal invoiceamount;

    /**
     * 税率
     */
    private BigDecimal taxratevalue;

    private BigDecimal taxrateamount;

    private Integer type;

    /**
     * 收款人
     */
    private String cashername;

    /**
     *  审核人
     */
    private String reviewername;

    /**
     * 开票人
     */
    private String drawername;

    private Integer state;

    /**
     * 创建时间
     */
    private Date createdate;

    /**
     * 更新时间
     */
    private Date updatedate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHotelid() {
        return hotelid;
    }

    public void setHotelid(Long hotelid) {
        this.hotelid = hotelid;
    }

    public String getBuyername() {
        return buyername;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername == null ? null : buyername.trim();
    }

    public String getBuyertaxpayernum() {
        return buyertaxpayernum;
    }

    public void setBuyertaxpayernum(String buyertaxpayernum) {
        this.buyertaxpayernum = buyertaxpayernum == null ? null : buyertaxpayernum.trim();
    }

    public String getBuyeraddress() {
        return buyeraddress;
    }

    public void setBuyeraddress(String buyeraddress) {
        this.buyeraddress = buyeraddress == null ? null : buyeraddress.trim();
    }

    public String getBuyertel() {
        return buyertel;
    }

    public void setBuyertel(String buyertel) {
        this.buyertel = buyertel == null ? null : buyertel.trim();
    }

    public String getBuyerbankname() {
        return buyerbankname;
    }

    public void setBuyerbankname(String buyerbankname) {
        this.buyerbankname = buyerbankname == null ? null : buyerbankname.trim();
    }

    public String getBuyerbankpeople() {
        return buyerbankpeople;
    }

    public void setBuyerbankpeople(String buyerbankpeople) {
        this.buyerbankpeople = buyerbankpeople == null ? null : buyerbankpeople.trim();
    }

    public String getBuyerbankaccount() {
        return buyerbankaccount;
    }

    public void setBuyerbankaccount(String buyerbankaccount) {
        this.buyerbankaccount = buyerbankaccount == null ? null : buyerbankaccount.trim();
    }

    public String getTakername() {
        return takername;
    }

    public void setTakername(String takername) {
        this.takername = takername == null ? null : takername.trim();
    }

    public String getTakertel() {
        return takertel;
    }

    public void setTakertel(String takertel) {
        this.takertel = takertel == null ? null : takertel.trim();
    }

    public String getTakerdetailaddress() {
        return takerdetailaddress;
    }

    public void setTakerdetailaddress(String takerdetailaddress) {
        this.takerdetailaddress = takerdetailaddress == null ? null : takerdetailaddress.trim();
    }

    public String getTakeraddress() {
        return takeraddress;
    }

    public void setTakeraddress(String takeraddress) {
        this.takeraddress = takeraddress == null ? null : takeraddress.trim();
    }

    public String getTakeremail() {
        return takeremail;
    }

    public void setTakeremail(String takeremail) {
        this.takeremail = takeremail == null ? null : takeremail.trim();
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname == null ? null : itemname.trim();
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname == null ? null : goodsname.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }

    public BigDecimal getInvoiceamount() {
        return invoiceamount;
    }

    public void setInvoiceamount(BigDecimal invoiceamount) {
        this.invoiceamount = invoiceamount;
    }

    public BigDecimal getTaxratevalue() {
        return taxratevalue;
    }

    public void setTaxratevalue(BigDecimal taxratevalue) {
        this.taxratevalue = taxratevalue;
    }

    public BigDecimal getTaxrateamount() {
        return taxrateamount;
    }

    public void setTaxrateamount(BigDecimal taxrateamount) {
        this.taxrateamount = taxrateamount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCashername() {
        return cashername;
    }

    public void setCashername(String cashername) {
        this.cashername = cashername == null ? null : cashername.trim();
    }

    public String getReviewername() {
        return reviewername;
    }

    public void setReviewername(String reviewername) {
        this.reviewername = reviewername == null ? null : reviewername.trim();
    }

    public String getDrawername() {
        return drawername;
    }

    public void setDrawername(String drawername) {
        this.drawername = drawername == null ? null : drawername.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }
}