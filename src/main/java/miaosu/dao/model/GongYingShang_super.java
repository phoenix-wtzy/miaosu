package miaosu.dao.model;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @Date: 2020/8/21 10:28
 * @Description:
 */
public class GongYingShang_super {
	private String business_name;//供应商名称
	private Long business_id;//供应商id
	private Long user_id;//用户id
	private String legal_person;//法人
	private String legal_phone;//法人联系方式
	private String business_phone;//固定电话
	private String business_email;//电子邮件
	private String business_address;//公司地址
	private Long business_type;//行业类别
	private Long businessOrFactory;//经销商/厂家
	private String finance_person;//财务负责人
	private String finance_phone;//财务联系电话
	private String bank_type;//开户银行
	private String bank_number;//行号
	private String bank_addresss;//开户行地址
	private String account_name;//开户名称
	private String account_number;//账号
	private String settle_money;//结算金额
	private String commission_money;//技术服务费
	private String settle_StartAndEnd;//结算周期
	private String settle_start;//结算开始时间
	private String settle_end;//结算结束时间
	private String settle_status;//结算状态
	private String orderNo;//订单号
	private String bookTime;//预定时间
	private String bookUser;//预定人
	private String hotelRoomType;//房间类型
	private Integer roomCount;//房间数
	private String checkInTime;//入住时间
	private String checkOutTime;//离店时间
	private BigDecimal price;//金额
	private String bookRemark;//备注
	private String settleStatus;//订单结算状态

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public Long getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(Long business_id) {
		this.business_id = business_id;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getLegal_person() {
		return legal_person;
	}

	public void setLegal_person(String legal_person) {
		this.legal_person = legal_person;
	}

	public String getLegal_phone() {
		return legal_phone;
	}

	public void setLegal_phone(String legal_phone) {
		this.legal_phone = legal_phone;
	}

	public String getBusiness_phone() {
		return business_phone;
	}

	public void setBusiness_phone(String business_phone) {
		this.business_phone = business_phone;
	}

	public String getBusiness_email() {
		return business_email;
	}

	public void setBusiness_email(String business_email) {
		this.business_email = business_email;
	}

	public String getBusiness_address() {
		return business_address;
	}

	public void setBusiness_address(String business_address) {
		this.business_address = business_address;
	}

	public Long getBusiness_type() {
		return business_type;
	}

	public void setBusiness_type(Long business_type) {
		this.business_type = business_type;
	}

	public Long getBusinessOrFactory() {
		return businessOrFactory;
	}

	public void setBusinessOrFactory(Long businessOrFactory) {
		this.businessOrFactory = businessOrFactory;
	}

	public String getFinance_person() {
		return finance_person;
	}

	public void setFinance_person(String finance_person) {
		this.finance_person = finance_person;
	}

	public String getFinance_phone() {
		return finance_phone;
	}

	public void setFinance_phone(String finance_phone) {
		this.finance_phone = finance_phone;
	}

	public String getBank_type() {
		return bank_type;
	}

	public void setBank_type(String bank_type) {
		this.bank_type = bank_type;
	}

	public String getBank_number() {
		return bank_number;
	}

	public void setBank_number(String bank_number) {
		this.bank_number = bank_number;
	}

	public String getBank_addresss() {
		return bank_addresss;
	}

	public void setBank_addresss(String bank_addresss) {
		this.bank_addresss = bank_addresss;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getSettle_money() {
		return settle_money;
	}

	public void setSettle_money(String settle_money) {
		this.settle_money = settle_money;
	}

	public String getCommission_money() {
		return commission_money;
	}

	public void setCommission_money(String commission_money) {
		this.commission_money = commission_money;
	}

	public String getSettle_StartAndEnd() {
		return settle_StartAndEnd;
	}

	public void setSettle_StartAndEnd(String settle_StartAndEnd) {
		this.settle_StartAndEnd = settle_StartAndEnd;
	}

	public String getSettle_start() {
		return settle_start;
	}

	public void setSettle_start(String settle_start) {
		this.settle_start = settle_start;
	}

	public String getSettle_end() {
		return settle_end;
	}

	public void setSettle_end(String settle_end) {
		this.settle_end = settle_end;
	}

	public String getSettle_status() {
		return settle_status;
	}

	public void setSettle_status(String settle_status) {
		this.settle_status = settle_status;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getBookTime() {
		return bookTime;
	}

	public void setBookTime(String bookTime) {
		this.bookTime = bookTime;
	}

	public String getBookUser() {
		return bookUser;
	}

	public void setBookUser(String bookUser) {
		this.bookUser = bookUser;
	}

	public String getHotelRoomType() {
		return hotelRoomType;
	}

	public void setHotelRoomType(String hotelRoomType) {
		this.hotelRoomType = hotelRoomType;
	}

	public Integer getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Integer roomCount) {
		this.roomCount = roomCount;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(String checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getBookRemark() {
		return bookRemark;
	}

	public void setBookRemark(String bookRemark) {
		this.bookRemark = bookRemark;
	}

	public String getSettleStatus() {
		return settleStatus;
	}

	public void setSettleStatus(String settleStatus) {
		this.settleStatus = settleStatus;
	}
}
