package miaosu.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class HotelRoomCountExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HotelRoomCountExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andHotelRoomTypeIdIsNull() {
            addCriterion("hotel_room_type_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIsNotNull() {
            addCriterion("hotel_room_type_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdEqualTo(Long value) {
            addCriterion("hotel_room_type_id =", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotEqualTo(Long value) {
            addCriterion("hotel_room_type_id <>", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThan(Long value) {
            addCriterion("hotel_room_type_id >", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id >=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThan(Long value) {
            addCriterion("hotel_room_type_id <", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id <=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIn(List<Long> values) {
            addCriterion("hotel_room_type_id in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotIn(List<Long> values) {
            addCriterion("hotel_room_type_id not in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id not between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andSellDateIsNull() {
            addCriterion("sell_date is null");
            return (Criteria) this;
        }

        public Criteria andSellDateIsNotNull() {
            addCriterion("sell_date is not null");
            return (Criteria) this;
        }

        public Criteria andSellDateEqualTo(Date value) {
            addCriterionForJDBCDate("sell_date =", value, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("sell_date <>", value, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateGreaterThan(Date value) {
            addCriterionForJDBCDate("sell_date >", value, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("sell_date >=", value, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateLessThan(Date value) {
            addCriterionForJDBCDate("sell_date <", value, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("sell_date <=", value, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateIn(List<Date> values) {
            addCriterionForJDBCDate("sell_date in", values, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("sell_date not in", values, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("sell_date between", value1, value2, "sellDate");
            return (Criteria) this;
        }

        public Criteria andSellDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("sell_date not between", value1, value2, "sellDate");
            return (Criteria) this;
        }

        public Criteria andCountRoomIsNull() {
            addCriterion("count_room is null");
            return (Criteria) this;
        }

        public Criteria andCountRoomIsNotNull() {
            addCriterion("count_room is not null");
            return (Criteria) this;
        }

        public Criteria andCountRoomEqualTo(Integer value) {
            addCriterion("count_room =", value, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomNotEqualTo(Integer value) {
            addCriterion("count_room <>", value, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomGreaterThan(Integer value) {
            addCriterion("count_room >", value, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomGreaterThanOrEqualTo(Integer value) {
            addCriterion("count_room >=", value, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomLessThan(Integer value) {
            addCriterion("count_room <", value, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomLessThanOrEqualTo(Integer value) {
            addCriterion("count_room <=", value, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomIn(List<Integer> values) {
            addCriterion("count_room in", values, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomNotIn(List<Integer> values) {
            addCriterion("count_room not in", values, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomBetween(Integer value1, Integer value2) {
            addCriterion("count_room between", value1, value2, "countRoom");
            return (Criteria) this;
        }

        public Criteria andCountRoomNotBetween(Integer value1, Integer value2) {
            addCriterion("count_room not between", value1, value2, "countRoom");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}