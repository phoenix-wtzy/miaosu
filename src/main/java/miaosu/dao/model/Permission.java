package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2021/1/21 10:17
 * @Description:
 */

public class Permission {

	//主键自增
	private Long permission_id;
	//权限名称
	private String permission_name;
	//权限描述
	private String permission_description;


	public Long getPermission_id() {
		return permission_id;
	}

	public void setPermission_id(Long permission_id) {
		this.permission_id = permission_id;
	}

	public String getPermission_name() {
		return permission_name;
	}

	public void setPermission_name(String permission_name) {
		this.permission_name = permission_name;
	}

	public String getPermission_description() {
		return permission_description;
	}

	public void setPermission_description(String permission_description) {
		this.permission_description = permission_description;
	}
}
