package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/7/21 15:38
 * @Description: 公司信息
 */
public class Business extends BaseEntity11{
	private Long business_id;//公司id
	private Long user_id;//用户id
	private String business_name;//企业名称
	private String legal_person;//法人
	private String legal_phone;//法人联系方式
	private String business_phone;//固定电话
	private String business_email;//电子邮件
	private String business_address;//公司地址
	private Long business_type;//行业类别
	private String business_typeStr;//行业类别说明
	private Long businessOrFactory;//经销商/厂家
	private String businessOrFactoryStr;//经销商/厂家的字符串
	private String img_businessLicense;//营业执照
	private String img_shopHead;//公司/店铺门头
	private String img_factoryBook;//厂家授权书
	private String finance_person;//财务负责人
	private String finance_phone;//财务联系电话
	private String bank_type;//开户银行
	private String bank_number;//行号
	private String bank_addresss;//开户行地址
	private String account_name;//开户名称
	private String account_number;//账号
	//审核通过的时间
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date success_date;

	public Date getSuccess_date() {
		return success_date;
	}

	public void setSuccess_date(Date success_date) {
		this.success_date = success_date;
	}

	private String code_level;//通过与否

	//供应商关联酒店集合
	private List<HotelInfo> hotelInfoList;

	public List<HotelInfo> getHotelInfoList() {
		return hotelInfoList;
	}

	public void setHotelInfoList(List<HotelInfo> hotelInfoList) {
		this.hotelInfoList = hotelInfoList;
	}

	public String getCode_level() {
		return code_level;
	}

	public void setCode_level(String code_level) {
		this.code_level = code_level;
	}

	public Long getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(Long business_id) {
		this.business_id = business_id;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public String getLegal_person() {
		return legal_person;
	}

	public void setLegal_person(String legal_person) {
		this.legal_person = legal_person;
	}

	public String getLegal_phone() {
		return legal_phone;
	}

	public void setLegal_phone(String legal_phone) {
		this.legal_phone = legal_phone;
	}

	public String getBusiness_phone() {
		return business_phone;
	}

	public void setBusiness_phone(String business_phone) {
		this.business_phone = business_phone;
	}

	public String getBusiness_email() {
		return business_email;
	}

	public void setBusiness_email(String business_email) {
		this.business_email = business_email;
	}

	public String getBusiness_address() {
		return business_address;
	}

	public void setBusiness_address(String business_address) {
		this.business_address = business_address;
	}

	public Long getBusiness_type() {
		return business_type;
	}

	public void setBusiness_type(Long business_type) {
		this.business_type = business_type;
	}

	public Long getBusinessOrFactory() {
		return businessOrFactory;
	}

	public void setBusinessOrFactory(Long businessOrFactory) {
		this.businessOrFactory = businessOrFactory;
	}

	public String getImg_businessLicense() {
		return img_businessLicense;
	}

	public void setImg_businessLicense(String img_businessLicense) {
		this.img_businessLicense = img_businessLicense;
	}

	public String getImg_shopHead() {
		return img_shopHead;
	}

	public void setImg_shopHead(String img_shopHead) {
		this.img_shopHead = img_shopHead;
	}

	public String getImg_factoryBook() {
		return img_factoryBook;
	}

	public void setImg_factoryBook(String img_factoryBook) {
		this.img_factoryBook = img_factoryBook;
	}

	public String getFinance_person() {
		return finance_person;
	}

	public void setFinance_person(String finance_person) {
		this.finance_person = finance_person;
	}

	public String getFinance_phone() {
		return finance_phone;
	}

	public void setFinance_phone(String finance_phone) {
		this.finance_phone = finance_phone;
	}

	public String getBank_type() {
		return bank_type;
	}

	public void setBank_type(String bank_type) {
		this.bank_type = bank_type;
	}

	public String getBank_number() {
		return bank_number;
	}

	public void setBank_number(String bank_number) {
		this.bank_number = bank_number;
	}

	public String getBank_addresss() {
		return bank_addresss;
	}

	public void setBank_addresss(String bank_addresss) {
		this.bank_addresss = bank_addresss;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getBusiness_typeStr() {
		return business_typeStr;
	}

	public void setBusiness_typeStr(String business_typeStr) {
		this.business_typeStr = business_typeStr;
	}

	public String getBusinessOrFactoryStr() {
		return businessOrFactoryStr;
	}

	public void setBusinessOrFactoryStr(String businessOrFactoryStr) {
		this.businessOrFactoryStr = businessOrFactoryStr;
	}
}
