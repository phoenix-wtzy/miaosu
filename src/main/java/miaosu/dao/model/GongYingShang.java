package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2020/8/7 11:21
 * @Description: 供应商结算对应的封装类
 */
public class GongYingShang {
	private String settle_StartAndEnd;//结算周期
	private String settle_start;//结算开始时间
	private String settle_end;//结算结束时间
	private String business_name;//供应商名称(企业名称)
	private Long business_id;//供应商id(企业id)
	private String settle_money;//结算金额
	private String commission_money;//技术服务费
	private String settle_status;//结算状态

	public String getSettle_StartAndEnd() {
		return settle_StartAndEnd;
	}

	public void setSettle_StartAndEnd(String settle_StartAndEnd) {
		this.settle_StartAndEnd = settle_StartAndEnd;
	}

	public String getSettle_start() {
		return settle_start;
	}

	public void setSettle_start(String settle_start) {
		this.settle_start = settle_start;
	}

	public String getSettle_end() {
		return settle_end;
	}

	public void setSettle_end(String settle_end) {
		this.settle_end = settle_end;
	}

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public Long getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(Long business_id) {
		this.business_id = business_id;
	}

	public String getSettle_money() {
		return settle_money;
	}

	public void setSettle_money(String settle_money) {
		this.settle_money = settle_money;
	}

	public String getCommission_money() {
		return commission_money;
	}

	public void setCommission_money(String commission_money) {
		this.commission_money = commission_money;
	}

	public String getSettle_status() {
		return settle_status;
	}

	public void setSettle_status(String settle_status) {
		this.settle_status = settle_status;
	}
}
