package miaosu.dao.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.Map;

@XStreamAlias("xml")
public class WechatTextMessage extends WechatBaseMessage{
    //文本内容
    @XStreamAlias("Content")
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public WechatTextMessage(Map<String,String> requestMap,String content){
        super(requestMap);
        //设置文本消息的msgtTpe为text
        this.setMsgType("text");
        this.content = content;
    }

    @Override
    public String toString() {
        return "WechatTextMessage{" +
                "content='" + content + '\'' +
                '}';
    }
}
