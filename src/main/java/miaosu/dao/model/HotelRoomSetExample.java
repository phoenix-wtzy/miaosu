package miaosu.dao.model;

import java.util.ArrayList;
import java.util.List;

public class HotelRoomSetExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HotelRoomSetExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table hotel_room_set
     *
     * @mbg.generated
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andHotelRoomTypeIdIsNull() {
            addCriterion("hotel_room_type_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIsNotNull() {
            addCriterion("hotel_room_type_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdEqualTo(Long value) {
            addCriterion("hotel_room_type_id =", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotEqualTo(Long value) {
            addCriterion("hotel_room_type_id <>", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThan(Long value) {
            addCriterion("hotel_room_type_id >", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id >=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThan(Long value) {
            addCriterion("hotel_room_type_id <", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id <=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIn(List<Long> values) {
            addCriterion("hotel_room_type_id in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotIn(List<Long> values) {
            addCriterion("hotel_room_type_id not in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id not between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameIsNull() {
            addCriterion("hotel_room_type_name is null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameIsNotNull() {
            addCriterion("hotel_room_type_name is not null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameEqualTo(String value) {
            addCriterion("hotel_room_type_name =", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameNotEqualTo(String value) {
            addCriterion("hotel_room_type_name <>", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameGreaterThan(String value) {
            addCriterion("hotel_room_type_name >", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameGreaterThanOrEqualTo(String value) {
            addCriterion("hotel_room_type_name >=", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameLessThan(String value) {
            addCriterion("hotel_room_type_name <", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameLessThanOrEqualTo(String value) {
            addCriterion("hotel_room_type_name <=", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameLike(String value) {
            addCriterion("hotel_room_type_name like", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameNotLike(String value) {
            addCriterion("hotel_room_type_name not like", value, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameIn(List<String> values) {
            addCriterion("hotel_room_type_name in", values, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameNotIn(List<String> values) {
            addCriterion("hotel_room_type_name not in", values, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameBetween(String value1, String value2) {
            addCriterion("hotel_room_type_name between", value1, value2, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeNameNotBetween(String value1, String value2) {
            addCriterion("hotel_room_type_name not between", value1, value2, "hotelRoomTypeName");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdIsNull() {
            addCriterion("room_type_id is null");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdIsNotNull() {
            addCriterion("room_type_id is not null");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdEqualTo(Integer value) {
            addCriterion("room_type_id =", value, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdNotEqualTo(Integer value) {
            addCriterion("room_type_id <>", value, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdGreaterThan(Integer value) {
            addCriterion("room_type_id >", value, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("room_type_id >=", value, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdLessThan(Integer value) {
            addCriterion("room_type_id <", value, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdLessThanOrEqualTo(Integer value) {
            addCriterion("room_type_id <=", value, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdIn(List<Integer> values) {
            addCriterion("room_type_id in", values, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdNotIn(List<Integer> values) {
            addCriterion("room_type_id not in", values, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdBetween(Integer value1, Integer value2) {
            addCriterion("room_type_id between", value1, value2, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomTypeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("room_type_id not between", value1, value2, "roomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNull() {
            addCriterion("hotel_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNotNull() {
            addCriterion("hotel_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelIdEqualTo(Long value) {
            addCriterion("hotel_id =", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotEqualTo(Long value) {
            addCriterion("hotel_id <>", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThan(Long value) {
            addCriterion("hotel_id >", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_id >=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThan(Long value) {
            addCriterion("hotel_id <", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_id <=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdIn(List<Long> values) {
            addCriterion("hotel_id in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotIn(List<Long> values) {
            addCriterion("hotel_id not in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdBetween(Long value1, Long value2) {
            addCriterion("hotel_id between", value1, value2, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_id not between", value1, value2, "hotelId");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table hotel_room_set
     *
     * @mbg.generated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table hotel_room_set
     *
     * @mbg.generated
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}