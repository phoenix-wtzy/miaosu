package miaosu.dao.model;

public class HotelRoomType {
    private Integer roomTypeId;

    private String roomTypeName;

    private Integer otaRoomTypeId;

    public Integer getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(Integer roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName == null ? null : roomTypeName.trim();
    }

    public Integer getOtaRoomTypeId() {
        return otaRoomTypeId;
    }

    public void setOtaRoomTypeId(Integer otaRoomTypeId) {
        this.otaRoomTypeId = otaRoomTypeId;
    }
}