package miaosu.dao.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Administrator
 * @Date: 2021/2/4 13:46
 * @Description:  别的封装对象继承此类,分页返回给前段的是page/limit
 */
public class BaseEntity11 implements Serializable {
	@Id
	@Column(name = "Id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Transient
	private Integer page = 1;

	@Transient
	private Integer limit = 10;

	@Transient
	private long total ;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}
}
