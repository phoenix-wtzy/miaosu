package miaosu.dao.model;

import java.util.ArrayList;
import java.util.List;

public class HotelPriceRefRoomTypeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HotelPriceRefRoomTypeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(Long value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(Long value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(Long value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(Long value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(Long value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(Long value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<Long> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<Long> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(Long value1, Long value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(Long value1, Long value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdIsNull() {
            addCriterion("hotel_price_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdIsNotNull() {
            addCriterion("hotel_price_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdEqualTo(Long value) {
            addCriterion("hotel_price_id =", value, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdNotEqualTo(Long value) {
            addCriterion("hotel_price_id <>", value, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdGreaterThan(Long value) {
            addCriterion("hotel_price_id >", value, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_price_id >=", value, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdLessThan(Long value) {
            addCriterion("hotel_price_id <", value, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_price_id <=", value, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdIn(List<Long> values) {
            addCriterion("hotel_price_id in", values, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdNotIn(List<Long> values) {
            addCriterion("hotel_price_id not in", values, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdBetween(Long value1, Long value2) {
            addCriterion("hotel_price_id between", value1, value2, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelPriceIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_price_id not between", value1, value2, "hotelPriceId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIsNull() {
            addCriterion("hotel_room_type_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIsNotNull() {
            addCriterion("hotel_room_type_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdEqualTo(Long value) {
            addCriterion("hotel_room_type_id =", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotEqualTo(Long value) {
            addCriterion("hotel_room_type_id <>", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThan(Long value) {
            addCriterion("hotel_room_type_id >", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id >=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThan(Long value) {
            addCriterion("hotel_room_type_id <", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id <=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIn(List<Long> values) {
            addCriterion("hotel_room_type_id in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotIn(List<Long> values) {
            addCriterion("hotel_room_type_id not in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id not between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}