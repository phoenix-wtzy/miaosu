package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

/**
 * 酒店评估
 */
public class HotelEvaluation implements Serializable {

	//主键id
	private Long id;
	//当前用户id
	private  Long user_id;
	//酒店名称
	private String hotel_name;
	//创建时间
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date create_time;
	//评估时间
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date evaluation_time;
	//评估状态:0-正在评估; 1-评估完成
	private Integer evaluation_state;
	//营业执照:0-没有; 1-有
	private Integer businessLicense;
	//消防证:0-没有; 1-有
	private Integer fireLicense;
	//特种行业许可证:0-没有; 1-有
	private Integer specialLicense;
	//市政工程/临时封路:0-没有; 1-有
	private Integer publicWork;
	//代理:0-没有; 1-有 2-有两家
	private Integer agent;
	//月均销售额
	private Integer sale_average;
	//月均订单数
	private Integer orderCount_average;
	//月均间夜数
	private Integer nightC_average;
	//月均结算额
	private Integer settle_average;
	//评分值
	private Integer scoreValue;
	//备注
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public Integer getSpecialLicense() {
		return specialLicense;
	}

	public void setSpecialLicense(Integer specialLicense) {
		this.specialLicense = specialLicense;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getEvaluation_time() {
		return evaluation_time;
	}

	public void setEvaluation_time(Date evaluation_time) {
		this.evaluation_time = evaluation_time;
	}

	public Integer getEvaluation_state() {
		return evaluation_state;
	}

	public void setEvaluation_state(Integer evaluation_state) {
		this.evaluation_state = evaluation_state;
	}

	public Integer getBusinessLicense() {
		return businessLicense;
	}

	public void setBusinessLicense(Integer businessLicense) {
		this.businessLicense = businessLicense;
	}

	public Integer getFireLicense() {
		return fireLicense;
	}

	public void setFireLicense(Integer fireLicense) {
		this.fireLicense = fireLicense;
	}

	public Integer getPublicWork() {
		return publicWork;
	}

	public void setPublicWork(Integer publicWork) {
		this.publicWork = publicWork;
	}

	public Integer getAgent() {
		return agent;
	}

	public void setAgent(Integer agent) {
		this.agent = agent;
	}

	public Integer getSale_average() {
		return sale_average;
	}

	public void setSale_average(Integer sale_average) {
		this.sale_average = sale_average;
	}

	public Integer getOrderCount_average() {
		return orderCount_average;
	}

	public void setOrderCount_average(Integer orderCount_average) {
		this.orderCount_average = orderCount_average;
	}

	public Integer getNightC_average() {
		return nightC_average;
	}

	public void setNightC_average(Integer nightC_average) {
		this.nightC_average = nightC_average;
	}

	public Integer getSettle_average() {
		return settle_average;
	}

	public void setSettle_average(Integer settle_average) {
		this.settle_average = settle_average;
	}

	public Integer getScoreValue() {
		return scoreValue;
	}

	public void setScoreValue(Integer scoreValue) {
		this.scoreValue = scoreValue;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
