package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

public class HotelInfo extends BaseEntity{
    /**
     * 递增主键
     */
    private Long hotelId;

    /**
     * 酒店名称
     */
    private String hotelName;

    public String getHotel_only_id() {
        return hotel_only_id;
    }

    public void setHotel_only_id(String hotel_only_id) {
        this.hotel_only_id = hotel_only_id;
    }

    /**
     * 酒店平台ID
     */
    private String hotel_only_id;

    /**
     * 酒店地址
     */
    private String address;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 联系人手机号
     */
    private String contactMobile;

    /**
     * 关联用户ID
     */
    private Long userId;

    /**
     * 酒店前台电话
     * @return
     */
    private String receptionPhone;

    /**
     * 短信提醒的手机号
     */
    private String msgNotifyPhone;

    /**
     * 语音提醒的手机号
     */
    private String voiceNotifyPhone;

    /**
     *OTA管家电话
     */
    private String ota_phone;

    //酒店是否有效
    private int isActive;

    //酒店创建时间
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    //协议置换总金额
    private String settle_totalMoney;



    //协议置换间夜数
    private String settle_totalNight;

    //销售类型
    private int hotelType;
    //包房总金额
    private String baoFang_totalMoney;
    //投资人分润比例
    private String touZiRen_rates;

    public int getHotelType() {
        return hotelType;
    }

    public void setHotelType(int hotelType) {
        this.hotelType = hotelType;
    }

    public String getBaoFang_totalMoney() {
        return baoFang_totalMoney;
    }

    public void setBaoFang_totalMoney(String baoFang_totalMoney) {
        this.baoFang_totalMoney = baoFang_totalMoney;
    }

    public String getTouZiRen_rates() {
        return touZiRen_rates;
    }

    public void setTouZiRen_rates(String touZiRen_rates) {
        this.touZiRen_rates = touZiRen_rates;
    }

    public String getSettle_totalNight() {
        return settle_totalNight;
    }

    public void setSettle_totalNight(String settle_totalNight) {
        this.settle_totalNight = settle_totalNight;
    }
    public String getSettle_totalMoney() {
        return settle_totalMoney;
    }

    public void setSettle_totalMoney(String settle_totalMoney) {
        this.settle_totalMoney = settle_totalMoney;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName == null ? null : hotelName.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile == null ? null : contactMobile.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getReceptionPhone() {
        return receptionPhone;
    }

    public void setReceptionPhone(String receptionPhone) {
        this.receptionPhone = receptionPhone;
    }

    public String getMsgNotifyPhone() {
        return msgNotifyPhone;
    }

    public void setMsgNotifyPhone(String msgNotifyPhone) {
        this.msgNotifyPhone = msgNotifyPhone;
    }

    public String getVoiceNotifyPhone() {
        return voiceNotifyPhone;
    }

    public void setVoiceNotifyPhone(String voiceNotifyPhone) {
        this.voiceNotifyPhone = voiceNotifyPhone;
    }
    public String getOta_phone() {
        return ota_phone;
    }

    public void setOta_phone(String ota_phone) {
        this.ota_phone = ota_phone;
    }
}