package miaosu.dao.model;

import java.util.ArrayList;
import java.util.List;

public class WechatMenuButton {

    public List<WechatAbstractButton> getButton() {
        return button;
    }

    public void setButton(List<WechatAbstractButton> button) {
        this.button = button;
    }

    private List<WechatAbstractButton> button = new ArrayList();
}
