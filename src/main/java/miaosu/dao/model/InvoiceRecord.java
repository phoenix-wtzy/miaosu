package miaosu.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class InvoiceRecord {
    private Long id;

    private Long hotelId;

    private Long billId;

    private String reqserialno;

    private String sellerPayernum;

    private String invoiceContent;

    private Integer showStatus;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getReqserialno() {
        return reqserialno;
    }

    public void setReqserialno(String reqserialno) {
        this.reqserialno = reqserialno == null ? null : reqserialno.trim();
    }

    public String getSellerPayernum() {
        return sellerPayernum;
    }

    public void setSellerPayernum(String sellerPayernum) {
        this.sellerPayernum = sellerPayernum == null ? null : sellerPayernum.trim();
    }

    public String getInvoiceContent() {
        return invoiceContent;
    }

    public void setInvoiceContent(String invoiceContent) {
        this.invoiceContent = invoiceContent == null ? null : invoiceContent.trim();
    }

    public Integer getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(Integer showStatus) {
        this.showStatus = showStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}