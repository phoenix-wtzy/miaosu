package miaosu.dao.model;

import java.util.ArrayList;
import java.util.List;

public class WechatSubButton extends WechatAbstractButton {
    private List<WechatAbstractButton> sub_button = new ArrayList<>();

    public List<WechatAbstractButton> getSub_button() {
        return sub_button;
    }

    public void setSub_button(List<WechatAbstractButton> sub_button) {
        this.sub_button = sub_button;
    }

    public WechatSubButton(String name) {
        super(name);
    }
}
