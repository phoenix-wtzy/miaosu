package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2020/8/18 15:30
 * @Description:
 */
public class JiuDian{
	private Long id;
	private String settle_StartAndEnd;//结算周期
	private String settle_start;//结算开始时间
	private String settle_end;//结算结束时间
	private String hotel_name;//酒店名字
	private Long hotel_id;//酒店id
	private String settle_money;//结算金额
	private String commission_money;//技术服务费
	private String settle_status;//结算状态

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getSettle_StartAndEnd() {
		return settle_StartAndEnd;
	}

	public void setSettle_StartAndEnd(String settle_StartAndEnd) {
		this.settle_StartAndEnd = settle_StartAndEnd;
	}

	public String getSettle_start() {
		return settle_start;
	}

	public void setSettle_start(String settle_start) {
		this.settle_start = settle_start;
	}

	public String getSettle_end() {
		return settle_end;
	}

	public void setSettle_end(String settle_end) {
		this.settle_end = settle_end;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public Long getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(Long hotel_id) {
		this.hotel_id = hotel_id;
	}

	public String getSettle_money() {
		return settle_money;
	}

	public void setSettle_money(String settle_money) {
		this.settle_money = settle_money;
	}

	public String getCommission_money() {
		return commission_money;
	}

	public void setCommission_money(String commission_money) {
		this.commission_money = commission_money;
	}

	public String getSettle_status() {
		return settle_status;
	}

	public void setSettle_status(String settle_status) {
		this.settle_status = settle_status;
	}
}
