package miaosu.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HotelSettleSettingExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HotelSettleSettingExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNull() {
            addCriterion("hotel_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNotNull() {
            addCriterion("hotel_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelIdEqualTo(Long value) {
            addCriterion("hotel_id =", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotEqualTo(Long value) {
            addCriterion("hotel_id <>", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThan(Long value) {
            addCriterion("hotel_id >", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_id >=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThan(Long value) {
            addCriterion("hotel_id <", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_id <=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdIn(List<Long> values) {
            addCriterion("hotel_id in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotIn(List<Long> values) {
            addCriterion("hotel_id not in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdBetween(Long value1, Long value2) {
            addCriterion("hotel_id between", value1, value2, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_id not between", value1, value2, "hotelId");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeIsNull() {
            addCriterion("bank_use_type is null");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeIsNotNull() {
            addCriterion("bank_use_type is not null");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeEqualTo(Integer value) {
            addCriterion("bank_use_type =", value, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeNotEqualTo(Integer value) {
            addCriterion("bank_use_type <>", value, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeGreaterThan(Integer value) {
            addCriterion("bank_use_type >", value, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("bank_use_type >=", value, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeLessThan(Integer value) {
            addCriterion("bank_use_type <", value, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeLessThanOrEqualTo(Integer value) {
            addCriterion("bank_use_type <=", value, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeIn(List<Integer> values) {
            addCriterion("bank_use_type in", values, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeNotIn(List<Integer> values) {
            addCriterion("bank_use_type not in", values, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeBetween(Integer value1, Integer value2) {
            addCriterion("bank_use_type between", value1, value2, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankUseTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("bank_use_type not between", value1, value2, "bankUseType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeIsNull() {
            addCriterion("bank_account_type is null");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeIsNotNull() {
            addCriterion("bank_account_type is not null");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeEqualTo(String value) {
            addCriterion("bank_account_type =", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeNotEqualTo(String value) {
            addCriterion("bank_account_type <>", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeGreaterThan(String value) {
            addCriterion("bank_account_type >", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeGreaterThanOrEqualTo(String value) {
            addCriterion("bank_account_type >=", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeLessThan(String value) {
            addCriterion("bank_account_type <", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeLessThanOrEqualTo(String value) {
            addCriterion("bank_account_type <=", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeLike(String value) {
            addCriterion("bank_account_type like", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeNotLike(String value) {
            addCriterion("bank_account_type not like", value, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeIn(List<String> values) {
            addCriterion("bank_account_type in", values, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeNotIn(List<String> values) {
            addCriterion("bank_account_type not in", values, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeBetween(String value1, String value2) {
            addCriterion("bank_account_type between", value1, value2, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountTypeNotBetween(String value1, String value2) {
            addCriterion("bank_account_type not between", value1, value2, "bankAccountType");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameIsNull() {
            addCriterion("bank_account_name is null");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameIsNotNull() {
            addCriterion("bank_account_name is not null");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameEqualTo(String value) {
            addCriterion("bank_account_name =", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameNotEqualTo(String value) {
            addCriterion("bank_account_name <>", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameGreaterThan(String value) {
            addCriterion("bank_account_name >", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameGreaterThanOrEqualTo(String value) {
            addCriterion("bank_account_name >=", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameLessThan(String value) {
            addCriterion("bank_account_name <", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameLessThanOrEqualTo(String value) {
            addCriterion("bank_account_name <=", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameLike(String value) {
            addCriterion("bank_account_name like", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameNotLike(String value) {
            addCriterion("bank_account_name not like", value, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameIn(List<String> values) {
            addCriterion("bank_account_name in", values, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameNotIn(List<String> values) {
            addCriterion("bank_account_name not in", values, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameBetween(String value1, String value2) {
            addCriterion("bank_account_name between", value1, value2, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountNameNotBetween(String value1, String value2) {
            addCriterion("bank_account_name not between", value1, value2, "bankAccountName");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberIsNull() {
            addCriterion("bank_account_cardNumber is null");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberIsNotNull() {
            addCriterion("bank_account_cardNumber is not null");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberEqualTo(String value) {
            addCriterion("bank_account_cardNumber =", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberNotEqualTo(String value) {
            addCriterion("bank_account_cardNumber <>", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberGreaterThan(String value) {
            addCriterion("bank_account_cardNumber >", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberGreaterThanOrEqualTo(String value) {
            addCriterion("bank_account_cardNumber >=", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberLessThan(String value) {
            addCriterion("bank_account_cardNumber <", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberLessThanOrEqualTo(String value) {
            addCriterion("bank_account_cardNumber <=", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberLike(String value) {
            addCriterion("bank_account_cardNumber like", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberNotLike(String value) {
            addCriterion("bank_account_cardNumber not like", value, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberIn(List<String> values) {
            addCriterion("bank_account_cardNumber in", values, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberNotIn(List<String> values) {
            addCriterion("bank_account_cardNumber not in", values, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberBetween(String value1, String value2) {
            addCriterion("bank_account_cardNumber between", value1, value2, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankAccountCardnumberNotBetween(String value1, String value2) {
            addCriterion("bank_account_cardNumber not between", value1, value2, "bankAccountCardnumber");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneIsNull() {
            addCriterion("bank_sign_phone is null");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneIsNotNull() {
            addCriterion("bank_sign_phone is not null");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneEqualTo(String value) {
            addCriterion("bank_sign_phone =", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneNotEqualTo(String value) {
            addCriterion("bank_sign_phone <>", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneGreaterThan(String value) {
            addCriterion("bank_sign_phone >", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("bank_sign_phone >=", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneLessThan(String value) {
            addCriterion("bank_sign_phone <", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneLessThanOrEqualTo(String value) {
            addCriterion("bank_sign_phone <=", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneLike(String value) {
            addCriterion("bank_sign_phone like", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneNotLike(String value) {
            addCriterion("bank_sign_phone not like", value, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneIn(List<String> values) {
            addCriterion("bank_sign_phone in", values, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneNotIn(List<String> values) {
            addCriterion("bank_sign_phone not in", values, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneBetween(String value1, String value2) {
            addCriterion("bank_sign_phone between", value1, value2, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andBankSignPhoneNotBetween(String value1, String value2) {
            addCriterion("bank_sign_phone not between", value1, value2, "bankSignPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneIsNull() {
            addCriterion("settle_msg_phone is null");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneIsNotNull() {
            addCriterion("settle_msg_phone is not null");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneEqualTo(String value) {
            addCriterion("settle_msg_phone =", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneNotEqualTo(String value) {
            addCriterion("settle_msg_phone <>", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneGreaterThan(String value) {
            addCriterion("settle_msg_phone >", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("settle_msg_phone >=", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneLessThan(String value) {
            addCriterion("settle_msg_phone <", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneLessThanOrEqualTo(String value) {
            addCriterion("settle_msg_phone <=", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneLike(String value) {
            addCriterion("settle_msg_phone like", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneNotLike(String value) {
            addCriterion("settle_msg_phone not like", value, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneIn(List<String> values) {
            addCriterion("settle_msg_phone in", values, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneNotIn(List<String> values) {
            addCriterion("settle_msg_phone not in", values, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneBetween(String value1, String value2) {
            addCriterion("settle_msg_phone between", value1, value2, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andSettleMsgPhoneNotBetween(String value1, String value2) {
            addCriterion("settle_msg_phone not between", value1, value2, "settleMsgPhone");
            return (Criteria) this;
        }

        public Criteria andCommissionRateIsNull() {
            addCriterion("commission_rate is null");
            return (Criteria) this;
        }

        public Criteria andCommissionRateIsNotNull() {
            addCriterion("commission_rate is not null");
            return (Criteria) this;
        }

        public Criteria andCommissionRateEqualTo(Integer value) {
            addCriterion("commission_rate =", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateNotEqualTo(Integer value) {
            addCriterion("commission_rate <>", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateGreaterThan(Integer value) {
            addCriterion("commission_rate >", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateGreaterThanOrEqualTo(Integer value) {
            addCriterion("commission_rate >=", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateLessThan(Integer value) {
            addCriterion("commission_rate <", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateLessThanOrEqualTo(Integer value) {
            addCriterion("commission_rate <=", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateIn(List<Integer> values) {
            addCriterion("commission_rate in", values, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateNotIn(List<Integer> values) {
            addCriterion("commission_rate not in", values, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateBetween(Integer value1, Integer value2) {
            addCriterion("commission_rate between", value1, value2, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateNotBetween(Integer value1, Integer value2) {
            addCriterion("commission_rate not between", value1, value2, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingIsNull() {
            addCriterion("settle_cycle_setting is null");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingIsNotNull() {
            addCriterion("settle_cycle_setting is not null");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingEqualTo(Integer value) {
            addCriterion("settle_cycle_setting =", value, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingNotEqualTo(Integer value) {
            addCriterion("settle_cycle_setting <>", value, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingGreaterThan(Integer value) {
            addCriterion("settle_cycle_setting >", value, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingGreaterThanOrEqualTo(Integer value) {
            addCriterion("settle_cycle_setting >=", value, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingLessThan(Integer value) {
            addCriterion("settle_cycle_setting <", value, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingLessThanOrEqualTo(Integer value) {
            addCriterion("settle_cycle_setting <=", value, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingIn(List<Integer> values) {
            addCriterion("settle_cycle_setting in", values, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingNotIn(List<Integer> values) {
            addCriterion("settle_cycle_setting not in", values, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingBetween(Integer value1, Integer value2) {
            addCriterion("settle_cycle_setting between", value1, value2, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andSettleCycleSettingNotBetween(Integer value1, Integer value2) {
            addCriterion("settle_cycle_setting not between", value1, value2, "settleCycleSetting");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}