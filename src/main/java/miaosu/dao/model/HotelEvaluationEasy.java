package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

/**
 *酒店评估简单信息
 */
public class HotelEvaluationEasy implements Serializable {
	//主键id
	private Long id;
	//当前用户id
	private  Long user_id;
	//酒店名称
	private String hotel_name;
	//创建时间
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date create_time;
	//评估时间
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date evaluation_time;
	//评估状态:0-正在评估; 1-评估完成
	private Integer evaluation_state;

	@Override
	public String toString() {
		return "HotelEvaluationEasy{" +
				"id=" + id +
				", user_id=" + user_id +
				", hotel_name='" + hotel_name + '\'' +
				", create_time=" + create_time +
				", evaluation_time=" + evaluation_time +
				", evaluation_state=" + evaluation_state +
				'}';
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getEvaluation_time() {
		return evaluation_time;
	}

	public void setEvaluation_time(Date evaluation_time) {
		this.evaluation_time = evaluation_time;
	}

	public Integer getEvaluation_state() {
		return evaluation_state;
	}

	public void setEvaluation_state(Integer evaluation_state) {
		this.evaluation_state = evaluation_state;
	}
}
