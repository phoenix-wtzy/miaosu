package miaosu.dao.model;

public abstract class  WechatAbstractButton {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WechatAbstractButton(String name) {
        this.name = name;
    }
}
