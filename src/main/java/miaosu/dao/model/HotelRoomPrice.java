package miaosu.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class HotelRoomPrice {
    /**
     * 产品id
     */
    private Long productId;

    /**
     * 售卖日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date sellDate;

    private BigDecimal price;
    private String hotel_price_name;//价格自定义房型名称
    private Long hotel_price_id;//房价设置中的主键id
    private List<DateAndprice> dateAndprice;//多个日期内对应的房价组合



    public List<DateAndprice> getDateAndprice() {
        return dateAndprice;
    }

    public void setDateAndprice(List<DateAndprice> dateAndprice) {
        this.dateAndprice = dateAndprice;
    }

    public Long getHotel_price_id() {
        return hotel_price_id;
    }

    public void setHotel_price_id(Long hotel_price_id) {
        this.hotel_price_id = hotel_price_id;
    }

    public String getHotel_price_name() {
        return hotel_price_name;
    }

    public void setHotel_price_name(String hotel_price_name) {
        this.hotel_price_name = hotel_price_name;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Date getSellDate() {
        return sellDate;
    }

    public void setSellDate(Date sellDate) {
        this.sellDate = sellDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}