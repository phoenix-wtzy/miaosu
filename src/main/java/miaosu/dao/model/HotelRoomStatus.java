package miaosu.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class HotelRoomStatus {
    /**
     *酒店编码
     */
    private Long hotelId;

    /**
     * 房型id
     */
    private Long hotelRoomTypeId;

    /**
     * 售卖日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date sellDate;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date createTime;

    private Integer status;

    private Long productId;

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Long getHotelRoomTypeId() {
        return hotelRoomTypeId;
    }

    public void setHotelRoomTypeId(Long hotelRoomTypeId) {
        this.hotelRoomTypeId = hotelRoomTypeId;
    }

    public Date getSellDate() {
        return sellDate;
    }

    public void setSellDate(Date sellDate) {
        this.sellDate = sellDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}