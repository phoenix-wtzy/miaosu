package miaosu.dao.model;

import java.io.Serializable;

/**
 * @author Administrator
 * @Date: 2020/7/25 11:17
 * @Description:
 */
public class Picture implements Serializable {
	private Long pid;
	private Long user_id;//用户id
	private byte[] img_businessLicense;//营业执照
	private byte[] img_shopHead;//店铺门头
	private byte[] img_factoryBook;//厂家授权书

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public byte[] getImg_businessLicense() {
		return img_businessLicense;
	}

	public void setImg_businessLicense(byte[] img_businessLicense) {
		this.img_businessLicense = img_businessLicense;
	}

	public byte[] getImg_shopHead() {
		return img_shopHead;
	}

	public void setImg_shopHead(byte[] img_shopHead) {
		this.img_shopHead = img_shopHead;
	}

	public byte[] getImg_factoryBook() {
		return img_factoryBook;
	}

	public void setImg_factoryBook(byte[] img_factoryBook) {
		this.img_factoryBook = img_factoryBook;
	}
}
