package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class HotelOrder implements Serializable {
    /**
     * 递增主键
     */
    private Long orderId;

    /**
     * 订单号
     */
    private String orderNo;
    //酒店订单确认号
    private String hotel_confirm_number;

    public String getHotel_confirm_number() {
        return hotel_confirm_number;
    }

    public void setHotel_confirm_number(String hotel_confirm_number) {
        this.hotel_confirm_number = hotel_confirm_number;
    }

    /**
     * 间夜数=每单的入离时间间隔*房间数
     */

    private Integer nightC;

    public Integer getNightC() {
        return nightC;
    }

    public void setNightC(Integer nightC) {
        this.nightC = nightC;
    }


    /**
     * 渠道
     */
    private Integer orderChannel;

    /**
     * 销售类型：置换/代销
     */
    private Integer sellType;

    /**
     * 预定时间
     */
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date bookTime;

    /**
     * 预定人
     */
    private String bookUser;

    /**
     * 联系人
     */
    private String bookMobile;

    /**
     * 酒店ID
     */
    private Long hotelId;

    /**
     * 房型
     */
    private Long hotelRoomTypeId;

    //酒店平台上的房型名字
    private String p_hotel_room_type_name;

    public String getP_hotel_room_type_name() {
        return p_hotel_room_type_name;
    }

    public void setP_hotel_room_type_name(String p_hotel_room_type_name) {
        this.p_hotel_room_type_name = p_hotel_room_type_name;
    }

    /**
     * 房间数
     */
    private Integer roomCount;

    /**
     * 入住时间
     */
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date checkInTime;

    /**
     * 离店时间
     */
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date checkOutTime;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 订单状态
     */
    private Integer state;

    /**
     * 备注栏
     */
    private String bookRemark;
    /**
     * 订单结算状态
     */
    private Integer settleStatus;

    private String hotelName;

    //每日采购价
    private BigDecimal caigou_one;
    //总采购价
    private BigDecimal caigou_total;

    //订单备注
    private String remark_one;
    private Long hotel_supplier_id;//酒店关联供应商id
    private Integer commission_rate;//佣金比例
    private String settle_money;//结算金额

    public Long getHotel_supplier_id() {
        return hotel_supplier_id;
    }

    public void setHotel_supplier_id(Long hotel_supplier_id) {
        this.hotel_supplier_id = hotel_supplier_id;
    }

    public Integer getCommission_rate() {
        return commission_rate;
    }

    public void setCommission_rate(Integer commission_rate) {
        this.commission_rate = commission_rate;
    }

    public String getSettle_money() {
        return settle_money;
    }

    public void setSettle_money(String settle_money) {
        this.settle_money = settle_money;
    }

    public String getRemark_one() {
        return remark_one;
    }

    public void setRemark_one(String remark_one) {
        this.remark_one = remark_one;
    }

    public BigDecimal getCaigou_one() {
        return caigou_one;
    }

    public void setCaigou_one(BigDecimal caigou_one) {
        this.caigou_one = caigou_one;
    }

    public BigDecimal getCaigou_total() {
        return caigou_total;
    }

    public void setCaigou_total(BigDecimal caigou_total) {
        this.caigou_total = caigou_total;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    private static final long serialVersionUID = 1L;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }

    public Integer getSellType() {
        return sellType;
    }

    public void setSellType(Integer sellType) {
        this.sellType = sellType;
    }

    public Date getBookTime() {
        return bookTime;
    }

    public void setBookTime(Date bookTime) {
        this.bookTime = bookTime;
    }

    public String getBookUser() {
        return bookUser;
    }

    public void setBookUser(String bookUser) {
        this.bookUser = bookUser;
    }

    public String getBookMobile() {
        return bookMobile;
    }

    public void setBookMobile(String bookMobile) {
        this.bookMobile = bookMobile;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Long getHotelRoomTypeId() {
        return hotelRoomTypeId;
    }

    public void setHotelRoomTypeId(Long hotelRoomTypeId) {
        this.hotelRoomTypeId = hotelRoomTypeId;
    }

    public Integer getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(Integer roomCount) {
        this.roomCount = roomCount;
    }

    public Date getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(Date checkInTime) {
        this.checkInTime = checkInTime;
    }

    public Date getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(Date checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getBookRemark() {
        return bookRemark;
    }

    public void setBookRemark(String bookRemark) {
        this.bookRemark = bookRemark;
    }

    public Integer getSettleStatus() {
        return settleStatus;
    }

    public void setSettleStatus(Integer settleStatus) {
        this.settleStatus = settleStatus;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        HotelOrder other = (HotelOrder) that;
        return (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getOrderNo() == null ? other.getOrderNo() == null : this.getOrderNo().equals(other.getOrderNo()))
            && (this.getOrderChannel() == null ? other.getOrderChannel() == null : this.getOrderChannel().equals(other.getOrderChannel()))
            && (this.getSellType() == null ? other.getSellType() == null : this.getSellType().equals(other.getSellType()))
            && (this.getBookTime() == null ? other.getBookTime() == null : this.getBookTime().equals(other.getBookTime()))
            && (this.getBookUser() == null ? other.getBookUser() == null : this.getBookUser().equals(other.getBookUser()))
            && (this.getBookMobile() == null ? other.getBookMobile() == null : this.getBookMobile().equals(other.getBookMobile()))
            && (this.getHotelId() == null ? other.getHotelId() == null : this.getHotelId().equals(other.getHotelId()))
            && (this.getHotelRoomTypeId() == null ? other.getHotelRoomTypeId() == null : this.getHotelRoomTypeId().equals(other.getHotelRoomTypeId()))
            && (this.getRoomCount() == null ? other.getRoomCount() == null : this.getRoomCount().equals(other.getRoomCount()))
            && (this.getCheckInTime() == null ? other.getCheckInTime() == null : this.getCheckInTime().equals(other.getCheckInTime()))
            && (this.getCheckOutTime() == null ? other.getCheckOutTime() == null : this.getCheckOutTime().equals(other.getCheckOutTime()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getBookRemark() == null ? other.getBookRemark() == null : this.getBookRemark().equals(other.getBookRemark()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getOrderNo() == null) ? 0 : getOrderNo().hashCode());
        result = prime * result + ((getOrderChannel() == null) ? 0 : getOrderChannel().hashCode());
        result = prime * result + ((getSellType() == null) ? 0 : getSellType().hashCode());
        result = prime * result + ((getBookTime() == null) ? 0 : getBookTime().hashCode());
        result = prime * result + ((getBookUser() == null) ? 0 : getBookUser().hashCode());
        result = prime * result + ((getBookMobile() == null) ? 0 : getBookMobile().hashCode());
        result = prime * result + ((getHotelId() == null) ? 0 : getHotelId().hashCode());
        result = prime * result + ((getHotelRoomTypeId() == null) ? 0 : getHotelRoomTypeId().hashCode());
        result = prime * result + ((getRoomCount() == null) ? 0 : getRoomCount().hashCode());
        result = prime * result + ((getCheckInTime() == null) ? 0 : getCheckInTime().hashCode());
        result = prime * result + ((getCheckOutTime() == null) ? 0 : getCheckOutTime().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getBookRemark() == null) ? 0 : getBookRemark().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orderId=").append(orderId);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", orderChannel=").append(orderChannel);
        sb.append(", sellType=").append(sellType);
        sb.append(", bookTime=").append(bookTime);
        sb.append(", bookUser=").append(bookUser);
        sb.append(", bookMobile=").append(bookMobile);
        sb.append(", hotelId=").append(hotelId);
        sb.append(", hotelRoomTypeId=").append(hotelRoomTypeId);
        sb.append(", roomCount=").append(roomCount);
        sb.append(", checkInTime=").append(checkInTime);
        sb.append(", checkOutTime=").append(checkOutTime);
        sb.append(", price=").append(price);
        sb.append(", state=").append(state);
        sb.append(", bookRemark=").append(bookRemark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}