package miaosu.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HotelInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HotelInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("p_hotel_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNotNull() {
            addCriterion("p_hotel_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelIdEqualTo(Long value) {
            addCriterion("p_hotel_id =", value, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotEqualTo(Long value) {
            addCriterion("p_hotel_id <>", value, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThan(Long value) {
            addCriterion("p_hotel_id >", value, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThanOrEqualTo(Long value) {
            addCriterion("p_hotel_id >=", value, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThan(Long value) {
            addCriterion("p_hotel_id <", value, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThanOrEqualTo(Long value) {
            addCriterion("p_hotel_id <=", value, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdIn(List<Long> values) {
            addCriterion("p_hotel_id in", values, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotIn(List<Long> values) {
            addCriterion("p_hotel_id not in", values, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdBetween(Long value1, Long value2) {
            addCriterion("p_hotel_id between", value1, value2, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotBetween(Long value1, Long value2) {
            addCriterion("p_hotel_id not between", value1, value2, "pHotelId");
            return (Criteria) this;
        }

        public Criteria andHotelNameIsNull() {
            addCriterion("hotel_name is null");
            return (Criteria) this;
        }

        public Criteria andHotelNameIsNotNull() {
            addCriterion("hotel_name is not null");
            return (Criteria) this;
        }

        public Criteria andHotelNameEqualTo(String value) {
            addCriterion("hotel_name =", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameNotEqualTo(String value) {
            addCriterion("hotel_name <>", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameGreaterThan(String value) {
            addCriterion("hotel_name >", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameGreaterThanOrEqualTo(String value) {
            addCriterion("hotel_name >=", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameLessThan(String value) {
            addCriterion("hotel_name <", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameLessThanOrEqualTo(String value) {
            addCriterion("hotel_name <=", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameLike(String value) {
            addCriterion("hotel_name like", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameNotLike(String value) {
            addCriterion("hotel_name not like", value, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameIn(List<String> values) {
            addCriterion("hotel_name in", values, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameNotIn(List<String> values) {
            addCriterion("hotel_name not in", values, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameBetween(String value1, String value2) {
            addCriterion("hotel_name between", value1, value2, "hotelName");
            return (Criteria) this;
        }

        public Criteria andHotelNameNotBetween(String value1, String value2) {
            addCriterion("hotel_name not between", value1, value2, "hotelName");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andContactIsNull() {
            addCriterion("contact is null");
            return (Criteria) this;
        }

        public Criteria andContactIsNotNull() {
            addCriterion("contact is not null");
            return (Criteria) this;
        }

        public Criteria andContactEqualTo(String value) {
            addCriterion("contact =", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotEqualTo(String value) {
            addCriterion("contact <>", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactGreaterThan(String value) {
            addCriterion("contact >", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactGreaterThanOrEqualTo(String value) {
            addCriterion("contact >=", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactLessThan(String value) {
            addCriterion("contact <", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactLessThanOrEqualTo(String value) {
            addCriterion("contact <=", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactLike(String value) {
            addCriterion("contact like", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotLike(String value) {
            addCriterion("contact not like", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactIn(List<String> values) {
            addCriterion("contact in", values, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotIn(List<String> values) {
            addCriterion("contact not in", values, "contact");
            return (Criteria) this;
        }

        public Criteria andContactBetween(String value1, String value2) {
            addCriterion("contact between", value1, value2, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotBetween(String value1, String value2) {
            addCriterion("contact not between", value1, value2, "contact");
            return (Criteria) this;
        }

        public Criteria andContactMobileIsNull() {
            addCriterion("contact_mobile is null");
            return (Criteria) this;
        }

        public Criteria andContactMobileIsNotNull() {
            addCriterion("contact_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andContactMobileEqualTo(String value) {
            addCriterion("contact_mobile =", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotEqualTo(String value) {
            addCriterion("contact_mobile <>", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileGreaterThan(String value) {
            addCriterion("contact_mobile >", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileGreaterThanOrEqualTo(String value) {
            addCriterion("contact_mobile >=", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileLessThan(String value) {
            addCriterion("contact_mobile <", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileLessThanOrEqualTo(String value) {
            addCriterion("contact_mobile <=", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileLike(String value) {
            addCriterion("contact_mobile like", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotLike(String value) {
            addCriterion("contact_mobile not like", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileIn(List<String> values) {
            addCriterion("contact_mobile in", values, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotIn(List<String> values) {
            addCriterion("contact_mobile not in", values, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileBetween(String value1, String value2) {
            addCriterion("contact_mobile between", value1, value2, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotBetween(String value1, String value2) {
            addCriterion("contact_mobile not between", value1, value2, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneIsNull() {
            addCriterion("reception_phone is null");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneIsNotNull() {
            addCriterion("reception_phone is not null");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneEqualTo(String value) {
            addCriterion("reception_phone =", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneNotEqualTo(String value) {
            addCriterion("reception_phone <>", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneGreaterThan(String value) {
            addCriterion("reception_phone >", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("reception_phone >=", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneLessThan(String value) {
            addCriterion("reception_phone <", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneLessThanOrEqualTo(String value) {
            addCriterion("reception_phone <=", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneLike(String value) {
            addCriterion("reception_phone like", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneNotLike(String value) {
            addCriterion("reception_phone not like", value, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneIn(List<String> values) {
            addCriterion("reception_phone in", values, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneNotIn(List<String> values) {
            addCriterion("reception_phone not in", values, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneBetween(String value1, String value2) {
            addCriterion("reception_phone between", value1, value2, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andReceptionPhoneNotBetween(String value1, String value2) {
            addCriterion("reception_phone not between", value1, value2, "receptionPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneIsNull() {
            addCriterion("msg_notify_phone is null");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneIsNotNull() {
            addCriterion("msg_notify_phone is not null");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneEqualTo(String value) {
            addCriterion("msg_notify_phone =", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneNotEqualTo(String value) {
            addCriterion("msg_notify_phone <>", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneGreaterThan(String value) {
            addCriterion("msg_notify_phone >", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("msg_notify_phone >=", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneLessThan(String value) {
            addCriterion("msg_notify_phone <", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneLessThanOrEqualTo(String value) {
            addCriterion("msg_notify_phone <=", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneLike(String value) {
            addCriterion("msg_notify_phone like", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneNotLike(String value) {
            addCriterion("msg_notify_phone not like", value, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneIn(List<String> values) {
            addCriterion("msg_notify_phone in", values, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneNotIn(List<String> values) {
            addCriterion("msg_notify_phone not in", values, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneBetween(String value1, String value2) {
            addCriterion("msg_notify_phone between", value1, value2, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andMsgNotifyPhoneNotBetween(String value1, String value2) {
            addCriterion("msg_notify_phone not between", value1, value2, "msgNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneIsNull() {
            addCriterion("voice_notify_phone is null");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneIsNotNull() {
            addCriterion("voice_notify_phone is not null");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneEqualTo(String value) {
            addCriterion("voice_notify_phone =", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneNotEqualTo(String value) {
            addCriterion("voice_notify_phone <>", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneGreaterThan(String value) {
            addCriterion("voice_notify_phone >", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("voice_notify_phone >=", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneLessThan(String value) {
            addCriterion("voice_notify_phone <", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneLessThanOrEqualTo(String value) {
            addCriterion("voice_notify_phone <=", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneLike(String value) {
            addCriterion("voice_notify_phone like", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneNotLike(String value) {
            addCriterion("voice_notify_phone not like", value, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneIn(List<String> values) {
            addCriterion("voice_notify_phone in", values, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneNotIn(List<String> values) {
            addCriterion("voice_notify_phone not in", values, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneBetween(String value1, String value2) {
            addCriterion("voice_notify_phone between", value1, value2, "voiceNotifyPhone");
            return (Criteria) this;
        }

        public Criteria andVoiceNotifyPhoneNotBetween(String value1, String value2) {
            addCriterion("voice_notify_phone not between", value1, value2, "voiceNotifyPhone");
            return (Criteria) this;
        }
        public Criteria andisActiveNull() {
            addCriterion("isActive is null");
            return (Criteria) this;
        }

        public Criteria andisActiveNotNull() {
            addCriterion("isActive is not null");
            return (Criteria) this;
        }

        public Criteria andisActiveEqualTo(int value) {
            addCriterion("isActive =", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveNotEqualTo(int value) {
            addCriterion("isActive <>", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveGreaterThan(int value) {
            addCriterion("isActive >", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveGreaterThanOrEqualTo(int value) {
            addCriterion("isActive >=", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveLessThan(int value) {
            addCriterion("isActive <", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveLessThanOrEqualTo(int value) {
            addCriterion("isActive <=", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveIn(List<Long> values) {
            addCriterion("isActive in", values, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveNotIn(List<Long> values) {
            addCriterion("isActive not in", values, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveBetween(int value1, int value2) {
            addCriterion("isActive between", value1, value2, "isActive");
            return (Criteria) this;
        }

        public Criteria andisActiveNotBetween(int value1, int value2) {
            addCriterion("isActive not between", value1, value2, "isActive");
            return (Criteria) this;
        }

        public Criteria andcreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andcreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andcreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andcreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andhotelTypeNull() {
            addCriterion("hotelType is null");
            return (Criteria) this;
        }

        public Criteria andhotelTypeNotNull() {
            addCriterion("hotelType is not null");
            return (Criteria) this;
        }

        public Criteria andhotelTypeEqualTo(int value) {
            addCriterion("hotelType =", value, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeNotEqualTo(int value) {
            addCriterion("hotelType <>", value, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeGreaterThan(int value) {
            addCriterion("hotelType >", value, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeGreaterThanOrEqualTo(int value) {
            addCriterion("hotelType >=", value, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeLessThan(int value) {
            addCriterion("hotelType <", value, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeLessThanOrEqualTo(int value) {
            addCriterion("hotelType <=", value, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeIn(List<Long> values) {
            addCriterion("hotelType in", values, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeNotIn(List<Long> values) {
            addCriterion("hotelType not in", values, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeBetween(int value1, int value2) {
            addCriterion("hotelType between", value1, value2, "hotelType");
            return (Criteria) this;
        }

        public Criteria andhotelTypeNotBetween(int value1, int value2) {
            addCriterion("hotelType not between", value1, value2, "hotelType");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyIsNull() {
            addCriterion("baoFang_totalMoney is null");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyIsNotNull() {
            addCriterion("baoFang_totalMoney is not null");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyEqualTo(String value) {
            addCriterion("baoFang_totalMoney =", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyNotEqualTo(String value) {
            addCriterion("baoFang_totalMoney <>", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyGreaterThan(String value) {
            addCriterion("baoFang_totalMoney >", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyGreaterThanOrEqualTo(String value) {
            addCriterion("baoFang_totalMoney >=", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyLessThan(String value) {
            addCriterion("baoFang_totalMoney <", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyLessThanOrEqualTo(String value) {
            addCriterion("baoFang_totalMoney <=", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyLike(String value) {
            addCriterion("baoFang_totalMoney like", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyNotLike(String value) {
            addCriterion("baoFang_totalMoney not like", value, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyIn(List<String> values) {
            addCriterion("baoFang_totalMoney in", values, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyNotIn(List<String> values) {
            addCriterion("baoFang_totalMoney not in", values, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyBetween(String value1, String value2) {
            addCriterion("baoFang_totalMoney between", value1, value2, "baoFang_totalMoney");
            return (Criteria) this;
        }

        public Criteria andbaoFang_totalMoneyNotBetween(String value1, String value2) {
            addCriterion("baoFang_totalMoney not between", value1, value2, "baoFang_totalMoney");
            return (Criteria) this;
        }


        public Criteria andtouZiRen_ratesIsNull() {
            addCriterion("touZiRen_rates is null");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesIsNotNull() {
            addCriterion("touZiRen_rates is not null");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesEqualTo(String value) {
            addCriterion("touZiRen_rates =", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesNotEqualTo(String value) {
            addCriterion("touZiRen_rates <>", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesGreaterThan(String value) {
            addCriterion("touZiRen_rates >", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesGreaterThanOrEqualTo(String value) {
            addCriterion("touZiRen_rates >=", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesLessThan(String value) {
            addCriterion("touZiRen_rates <", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesLessThanOrEqualTo(String value) {
            addCriterion("touZiRen_rates <=", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesLike(String value) {
            addCriterion("touZiRen_rates like", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesNotLike(String value) {
            addCriterion("touZiRen_rates not like", value, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesIn(List<String> values) {
            addCriterion("touZiRen_rates in", values, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesNotIn(List<String> values) {
            addCriterion("touZiRen_rates not in", values, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesBetween(String value1, String value2) {
            addCriterion("touZiRen_rates between", value1, value2, "touZiRen_rates");
            return (Criteria) this;
        }

        public Criteria andtouZiRen_ratesNotBetween(String value1, String value2) {
            addCriterion("touZiRen_rates not between", value1, value2, "touZiRen_rates");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}