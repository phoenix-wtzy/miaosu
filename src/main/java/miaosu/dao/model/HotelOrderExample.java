package miaosu.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HotelOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HotelOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Long value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Long value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Long value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Long value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Long> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Long> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Long value1, Long value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNull() {
            addCriterion("order_no is null");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNotNull() {
            addCriterion("order_no is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNoEqualTo(String value) {
            addCriterion("order_no =", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotEqualTo(String value) {
            addCriterion("order_no <>", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThan(String value) {
            addCriterion("order_no >", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("order_no >=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThan(String value) {
            addCriterion("order_no <", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThanOrEqualTo(String value) {
            addCriterion("order_no <=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLike(String value) {
            addCriterion("order_no like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotLike(String value) {
            addCriterion("order_no not like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoIn(List<String> values) {
            addCriterion("order_no in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotIn(List<String> values) {
            addCriterion("order_no not in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoBetween(String value1, String value2) {
            addCriterion("order_no between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotBetween(String value1, String value2) {
            addCriterion("order_no not between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderChannelIsNull() {
            addCriterion("order_channel is null");
            return (Criteria) this;
        }

        public Criteria andOrderChannelIsNotNull() {
            addCriterion("order_channel is not null");
            return (Criteria) this;
        }

        public Criteria andOrderChannelEqualTo(Integer value) {
            addCriterion("order_channel =", value, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelNotEqualTo(Integer value) {
            addCriterion("order_channel <>", value, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelGreaterThan(Integer value) {
            addCriterion("order_channel >", value, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_channel >=", value, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelLessThan(Integer value) {
            addCriterion("order_channel <", value, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelLessThanOrEqualTo(Integer value) {
            addCriterion("order_channel <=", value, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelIn(List<Integer> values) {
            addCriterion("order_channel in", values, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelNotIn(List<Integer> values) {
            addCriterion("order_channel not in", values, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelBetween(Integer value1, Integer value2) {
            addCriterion("order_channel between", value1, value2, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andOrderChannelNotBetween(Integer value1, Integer value2) {
            addCriterion("order_channel not between", value1, value2, "orderChannel");
            return (Criteria) this;
        }

        public Criteria andSellTypeIsNull() {
            addCriterion("sell_type is null");
            return (Criteria) this;
        }

        public Criteria andSellTypeIsNotNull() {
            addCriterion("sell_type is not null");
            return (Criteria) this;
        }

        public Criteria andSellTypeEqualTo(Integer value) {
            addCriterion("sell_type =", value, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeNotEqualTo(Integer value) {
            addCriterion("sell_type <>", value, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeGreaterThan(Integer value) {
            addCriterion("sell_type >", value, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("sell_type >=", value, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeLessThan(Integer value) {
            addCriterion("sell_type <", value, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeLessThanOrEqualTo(Integer value) {
            addCriterion("sell_type <=", value, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeIn(List<Integer> values) {
            addCriterion("sell_type in", values, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeNotIn(List<Integer> values) {
            addCriterion("sell_type not in", values, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeBetween(Integer value1, Integer value2) {
            addCriterion("sell_type between", value1, value2, "sellType");
            return (Criteria) this;
        }

        public Criteria andSellTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("sell_type not between", value1, value2, "sellType");
            return (Criteria) this;
        }

        public Criteria andBookTimeIsNull() {
            addCriterion("book_time is null");
            return (Criteria) this;
        }

        public Criteria andBookTimeIsNotNull() {
            addCriterion("book_time is not null");
            return (Criteria) this;
        }

        public Criteria andBookTimeEqualTo(Date value) {
            addCriterion("book_time =", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeNotEqualTo(Date value) {
            addCriterion("book_time <>", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeGreaterThan(Date value) {
            addCriterion("book_time >", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("book_time >=", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeLessThan(Date value) {
            addCriterion("book_time <", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeLessThanOrEqualTo(Date value) {
            addCriterion("book_time <=", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeIn(List<Date> values) {
            addCriterion("book_time in", values, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeNotIn(List<Date> values) {
            addCriterion("book_time not in", values, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeBetween(Date value1, Date value2) {
            addCriterion("book_time between", value1, value2, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeNotBetween(Date value1, Date value2) {
            addCriterion("book_time not between", value1, value2, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookUserIsNull() {
            addCriterion("book_user is null");
            return (Criteria) this;
        }

        public Criteria andBookUserIsNotNull() {
            addCriterion("book_user is not null");
            return (Criteria) this;
        }

        public Criteria andBookUserEqualTo(String value) {
            addCriterion("book_user =", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserNotEqualTo(String value) {
            addCriterion("book_user <>", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserGreaterThan(String value) {
            addCriterion("book_user >", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserGreaterThanOrEqualTo(String value) {
            addCriterion("book_user >=", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserLessThan(String value) {
            addCriterion("book_user <", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserLessThanOrEqualTo(String value) {
            addCriterion("book_user <=", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserLike(String value) {
            addCriterion("book_user like", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserNotLike(String value) {
            addCriterion("book_user not like", value, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserIn(List<String> values) {
            addCriterion("book_user in", values, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserNotIn(List<String> values) {
            addCriterion("book_user not in", values, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserBetween(String value1, String value2) {
            addCriterion("book_user between", value1, value2, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookUserNotBetween(String value1, String value2) {
            addCriterion("book_user not between", value1, value2, "bookUser");
            return (Criteria) this;
        }

        public Criteria andBookMobileIsNull() {
            addCriterion("book_mobile is null");
            return (Criteria) this;
        }

        public Criteria andBookMobileIsNotNull() {
            addCriterion("book_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andBookMobileEqualTo(String value) {
            addCriterion("book_mobile =", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileNotEqualTo(String value) {
            addCriterion("book_mobile <>", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileGreaterThan(String value) {
            addCriterion("book_mobile >", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileGreaterThanOrEqualTo(String value) {
            addCriterion("book_mobile >=", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileLessThan(String value) {
            addCriterion("book_mobile <", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileLessThanOrEqualTo(String value) {
            addCriterion("book_mobile <=", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileLike(String value) {
            addCriterion("book_mobile like", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileNotLike(String value) {
            addCriterion("book_mobile not like", value, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileIn(List<String> values) {
            addCriterion("book_mobile in", values, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileNotIn(List<String> values) {
            addCriterion("book_mobile not in", values, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileBetween(String value1, String value2) {
            addCriterion("book_mobile between", value1, value2, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andBookMobileNotBetween(String value1, String value2) {
            addCriterion("book_mobile not between", value1, value2, "bookMobile");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNull() {
            addCriterion("hotel_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNotNull() {
            addCriterion("hotel_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelIdEqualTo(Long value) {
            addCriterion("hotel_id =", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotEqualTo(Long value) {
            addCriterion("hotel_id <>", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThan(Long value) {
            addCriterion("hotel_id >", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_id >=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThan(Long value) {
            addCriterion("hotel_id <", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_id <=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdIn(List<Long> values) {
            addCriterion("hotel_id in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotIn(List<Long> values) {
            addCriterion("hotel_id not in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdBetween(Long value1, Long value2) {
            addCriterion("hotel_id between", value1, value2, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_id not between", value1, value2, "hotelId");
            return (Criteria) this;
        }


        public Criteria andp_hotel_room_type_nameIsNull() {
            addCriterion("p_hotel_room_type_name is null");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameIsNotNull() {
            addCriterion("p_hotel_room_type_name is not null");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameEqualTo(String value) {
            addCriterion("p_hotel_room_type_name =", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameNotEqualTo(String value) {
            addCriterion("p_hotel_room_type_name <>", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameGreaterThan(String value) {
            addCriterion("p_hotel_room_type_name >", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameGreaterThanOrEqualTo(String value) {
            addCriterion("p_hotel_room_type_name >=", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameLessThan(String value) {
            addCriterion("p_hotel_room_type_name <", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameLessThanOrEqualTo(String value) {
            addCriterion("p_hotel_room_type_name <=", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameLike(String value) {
            addCriterion("p_hotel_room_type_name like", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameNotLike(String value) {
            addCriterion("p_hotel_room_type_name not like", value, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameIn(List<String> values) {
            addCriterion("p_hotel_room_type_name in", values, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameNotIn(List<String> values) {
            addCriterion("p_hotel_room_type_name not in", values, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameBetween(String value1, String value2) {
            addCriterion("p_hotel_room_type_name between", value1, value2, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andp_hotel_room_type_nameNotBetween(String value1, String value2) {
            addCriterion("p_hotel_room_type_name not between", value1, value2, "p_hotel_room_type_name");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIsNull() {
            addCriterion("hotel_room_type_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIsNotNull() {
            addCriterion("hotel_room_type_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdEqualTo(Long value) {
            addCriterion("hotel_room_type_id =", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotEqualTo(Long value) {
            addCriterion("hotel_room_type_id <>", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThan(Long value) {
            addCriterion("hotel_room_type_id >", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id >=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThan(Long value) {
            addCriterion("hotel_room_type_id <", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdLessThanOrEqualTo(Long value) {
            addCriterion("hotel_room_type_id <=", value, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdIn(List<Long> values) {
            addCriterion("hotel_room_type_id in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotIn(List<Long> values) {
            addCriterion("hotel_room_type_id not in", values, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andHotelRoomTypeIdNotBetween(Long value1, Long value2) {
            addCriterion("hotel_room_type_id not between", value1, value2, "hotelRoomTypeId");
            return (Criteria) this;
        }

        public Criteria andRoomCountIsNull() {
            addCriterion("room_count is null");
            return (Criteria) this;
        }

        public Criteria andRoomCountIsNotNull() {
            addCriterion("room_count is not null");
            return (Criteria) this;
        }

        public Criteria andRoomCountEqualTo(Integer value) {
            addCriterion("room_count =", value, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountNotEqualTo(Integer value) {
            addCriterion("room_count <>", value, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountGreaterThan(Integer value) {
            addCriterion("room_count >", value, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("room_count >=", value, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountLessThan(Integer value) {
            addCriterion("room_count <", value, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountLessThanOrEqualTo(Integer value) {
            addCriterion("room_count <=", value, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountIn(List<Integer> values) {
            addCriterion("room_count in", values, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountNotIn(List<Integer> values) {
            addCriterion("room_count not in", values, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountBetween(Integer value1, Integer value2) {
            addCriterion("room_count between", value1, value2, "roomCount");
            return (Criteria) this;
        }

        public Criteria andRoomCountNotBetween(Integer value1, Integer value2) {
            addCriterion("room_count not between", value1, value2, "roomCount");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeIsNull() {
            addCriterion("check_in_time is null");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeIsNotNull() {
            addCriterion("check_in_time is not null");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeEqualTo(Date value) {
            addCriterion("check_in_time =", value, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeNotEqualTo(Date value) {
            addCriterion("check_in_time <>", value, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeGreaterThan(Date value) {
            addCriterion("check_in_time >", value, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("check_in_time >=", value, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeLessThan(Date value) {
            addCriterion("check_in_time <", value, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeLessThanOrEqualTo(Date value) {
            addCriterion("check_in_time <=", value, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeIn(List<Date> values) {
            addCriterion("check_in_time in", values, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeNotIn(List<Date> values) {
            addCriterion("check_in_time not in", values, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeBetween(Date value1, Date value2) {
            addCriterion("check_in_time between", value1, value2, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckInTimeNotBetween(Date value1, Date value2) {
            addCriterion("check_in_time not between", value1, value2, "checkInTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeIsNull() {
            addCriterion("check_out_time is null");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeIsNotNull() {
            addCriterion("check_out_time is not null");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeEqualTo(Date value) {
            addCriterion("check_out_time =", value, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeNotEqualTo(Date value) {
            addCriterion("check_out_time <>", value, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeGreaterThan(Date value) {
            addCriterion("check_out_time >", value, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("check_out_time >=", value, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeLessThan(Date value) {
            addCriterion("check_out_time <", value, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeLessThanOrEqualTo(Date value) {
            addCriterion("check_out_time <=", value, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeIn(List<Date> values) {
            addCriterion("check_out_time in", values, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeNotIn(List<Date> values) {
            addCriterion("check_out_time not in", values, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeBetween(Date value1, Date value2) {
            addCriterion("check_out_time between", value1, value2, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andCheckOutTimeNotBetween(Date value1, Date value2) {
            addCriterion("check_out_time not between", value1, value2, "checkOutTime");
            return (Criteria) this;
        }

        public Criteria andnightCIsNull() {
            addCriterion("nightC is null");
            return (Criteria) this;
        }

        public Criteria andnightCIsNotNull() {
            addCriterion("nightC is not null");
            return (Criteria) this;
        }

        public Criteria andnightCEqualTo(Integer value) {
            addCriterion("nightC =", value, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCNotEqualTo(Integer value) {
            addCriterion("nightC <>", value, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCGreaterThan(Integer value) {
            addCriterion("nightC >", value, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCGreaterThanOrEqualTo(Integer value) {
            addCriterion("nightC >=", value, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCLessThan(Integer value) {
            addCriterion("nightC <", value, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCLessThanOrEqualTo(Integer value) {
            addCriterion("nightC <=", value, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCIn(List<Integer> values) {
            addCriterion("nightC in", values, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCNotIn(List<Integer> values) {
            addCriterion("nightC not in", values, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCBetween(Integer value1, Integer value2) {
            addCriterion("nightC between", value1, value2, "nightC");
            return (Criteria) this;
        }

        public Criteria andnightCNotBetween(Integer value1, Integer value2) {
            addCriterion("nightC not between", value1, value2, "nightC");
            return (Criteria) this;
        }
        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(BigDecimal value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(BigDecimal value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(BigDecimal value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(BigDecimal value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<BigDecimal> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<BigDecimal> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andBookRemarkIsNull() {
            addCriterion("book_remark is null");
            return (Criteria) this;
        }

        public Criteria andBookRemarkIsNotNull() {
            addCriterion("book_remark is not null");
            return (Criteria) this;
        }

        public Criteria andBookRemarkEqualTo(String value) {
            addCriterion("book_remark =", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkNotEqualTo(String value) {
            addCriterion("book_remark <>", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkGreaterThan(String value) {
            addCriterion("book_remark >", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("book_remark >=", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkLessThan(String value) {
            addCriterion("book_remark <", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkLessThanOrEqualTo(String value) {
            addCriterion("book_remark <=", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkLike(String value) {
            addCriterion("book_remark like", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkNotLike(String value) {
            addCriterion("book_remark not like", value, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkIn(List<String> values) {
            addCriterion("book_remark in", values, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkNotIn(List<String> values) {
            addCriterion("book_remark not in", values, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkBetween(String value1, String value2) {
            addCriterion("book_remark between", value1, value2, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andBookRemarkNotBetween(String value1, String value2) {
            addCriterion("book_remark not between", value1, value2, "bookRemark");
            return (Criteria) this;
        }

        public Criteria andSettleStatusIsNull() {
            addCriterion("settle_status is null");
            return (Criteria) this;
        }

        public Criteria andSettleStatusIsNotNull() {
            addCriterion("settle_status is not null");
            return (Criteria) this;
        }

        public Criteria andSettleStatusEqualTo(Integer value) {
            addCriterion("settle_status =", value, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusNotEqualTo(Integer value) {
            addCriterion("settle_status <>", value, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusGreaterThan(Integer value) {
            addCriterion("settle_status >", value, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("settle_status >=", value, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusLessThan(Integer value) {
            addCriterion("settle_status <", value, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusLessThanOrEqualTo(Integer value) {
            addCriterion("settle_status <=", value, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusIn(List<Integer> values) {
            addCriterion("settle_status in", values, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusNotIn(List<Integer> values) {
            addCriterion("settle_status not in", values, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusBetween(Integer value1, Integer value2) {
            addCriterion("settle_status between", value1, value2, "settleStatus");
            return (Criteria) this;
        }

        public Criteria andSettleStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("settle_status not between", value1, value2, "settleStatus");
            return (Criteria) this;
        }



        public Criteria andcaigou_oneIsNull() {
            addCriterion("caigou_one is null");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneIsNotNull() {
            addCriterion("caigou_one is not null");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneEqualTo(BigDecimal value) {
            addCriterion("caigou_one =", value, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneNotEqualTo(BigDecimal value) {
            addCriterion("caigou_one <>", value, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneGreaterThan(BigDecimal value) {
            addCriterion("caigou_one >", value, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("caigou_one >=", value, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneLessThan(BigDecimal value) {
            addCriterion("caigou_one <", value, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneLessThanOrEqualTo(BigDecimal value) {
            addCriterion("caigou_one <=", value, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneIn(List<BigDecimal> values) {
            addCriterion("caigou_one in", values, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneNotIn(List<BigDecimal> values) {
            addCriterion("caigou_one not in", values, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("caigou_one between", value1, value2, "caigou_one");
            return (Criteria) this;
        }

        public Criteria andcaigou_oneNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("caigou_one not between", value1, value2, "caigou_one");
            return (Criteria) this;
        }



        public Criteria andcaigou_totalIsNull() {
            addCriterion("caigou_total is null");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalIsNotNull() {
            addCriterion("caigou_total is not null");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalEqualTo(BigDecimal value) {
            addCriterion("caigou_total =", value, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalNotEqualTo(BigDecimal value) {
            addCriterion("caigou_total <>", value, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalGreaterThan(BigDecimal value) {
            addCriterion("caigou_total >", value, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("caigou_total >=", value, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalLessThan(BigDecimal value) {
            addCriterion("caigou_total <", value, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("caigou_total <=", value, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalIn(List<BigDecimal> values) {
            addCriterion("caigou_total in", values, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalNotIn(List<BigDecimal> values) {
            addCriterion("caigou_total not in", values, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("caigou_total between", value1, value2, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andcaigou_totalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("caigou_total not between", value1, value2, "caigou_total");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberIsNull() {
            addCriterion("hotel_confirm_number is null");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberIsNotNull() {
            addCriterion("hotel_confirm_number is not null");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberEqualTo(String value) {
            addCriterion("hotel_confirm_number =", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberNotEqualTo(String value) {
            addCriterion("hotel_confirm_number <>", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberGreaterThan(String value) {
            addCriterion("hotel_confirm_number >", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberGreaterThanOrEqualTo(String value) {
            addCriterion("hotel_confirm_number >=", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberLessThan(String value) {
            addCriterion("hotel_confirm_number <", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberLessThanOrEqualTo(String value) {
            addCriterion("hotel_confirm_number <=", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberLike(String value) {
            addCriterion("hotel_confirm_number like", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberNotLike(String value) {
            addCriterion("hotel_confirm_number not like", value, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberIn(List<String> values) {
            addCriterion("hotel_confirm_number in", values, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberNotIn(List<String> values) {
            addCriterion("hotel_confirm_number not in", values, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberBetween(String value1, String value2) {
            addCriterion("hotel_confirm_number between", value1, value2, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andhotel_confirm_numberNotBetween(String value1, String value2) {
            addCriterion("hotel_confirm_number not between", value1, value2, "hotel_confirm_number");
            return (Criteria) this;
        }

        public Criteria andremark_oneIsNull() {
            addCriterion("remark_one is null");
            return (Criteria) this;
        }

        public Criteria andremark_oneIsNotNull() {
            addCriterion("remark_one is not null");
            return (Criteria) this;
        }

        public Criteria andremark_oneEqualTo(String value) {
            addCriterion("remark_one =", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneNotEqualTo(String value) {
            addCriterion("remark_one <>", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneGreaterThan(String value) {
            addCriterion("remark_one >", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneGreaterThanOrEqualTo(String value) {
            addCriterion("remark_one >=", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andhremark_oneLessThan(String value) {
            addCriterion("remark_one <", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneLessThanOrEqualTo(String value) {
            addCriterion("remark_one <=", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneLike(String value) {
            addCriterion("remark_one like", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneNotLike(String value) {
            addCriterion("remark_one not like", value, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneIn(List<String> values) {
            addCriterion("remark_one in", values, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneNotIn(List<String> values) {
            addCriterion("remark_one not in", values, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneBetween(String value1, String value2) {
            addCriterion("remark_one between", value1, value2, "remark_one");
            return (Criteria) this;
        }

        public Criteria andremark_oneNotBetween(String value1, String value2) {
            addCriterion("remark_one not between", value1, value2, "remark_one");
            return (Criteria) this;
        }


        public Criteria andhotel_supplier_idIsNull() {
            addCriterion("hotel_supplier_id is null");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idIsNotNull() {
            addCriterion("hotel_supplier_id is not null");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idEqualTo(Long value) {
            addCriterion("hotel_supplier_id =", value, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idNotEqualTo(Long value) {
            addCriterion("hotel_supplier_id <>", value, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idGreaterThan(Long value) {
            addCriterion("hotel_supplier_id >", value, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idGreaterThanOrEqualTo(Long value) {
            addCriterion("hotel_supplier_id >=", value, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idLessThan(Long value) {
            addCriterion("hotel_supplier_id <", value, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idLessThanOrEqualTo(Long value) {
            addCriterion("hotel_supplier_id <=", value, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idIn(List<Long> values) {
            addCriterion("hotel_supplier_id in", values, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idNotIn(List<Long> values) {
            addCriterion("hotel_supplier_id not in", values, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idBetween(Long value1, Long value2) {
            addCriterion("hotel_supplier_id between", value1, value2, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andhotel_supplier_idNotBetween(Long value1, Long value2) {
            addCriterion("hotel_supplier_id not between", value1, value2, "hotel_supplier_id");
            return (Criteria) this;
        }

        public Criteria andcommission_rateNull() {
            addCriterion("commission_rate is null");
            return (Criteria) this;
        }

        public Criteria andcommission_rateNotNull() {
            addCriterion("commission_rate is not null");
            return (Criteria) this;
        }

        public Criteria andcommission_rateEqualTo(Integer value) {
            addCriterion("commission_rate =", value, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateNotEqualTo(Integer value) {
            addCriterion("commission_rate <>", value, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateGreaterThan(Integer value) {
            addCriterion("commission_rate >", value, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateGreaterThanOrEqualTo(Integer value) {
            addCriterion("commission_rate >=", value, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateLessThan(Integer value) {
            addCriterion("commission_rate <", value, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateLessThanOrEqualTo(Integer value) {
            addCriterion("commission_rate <=", value, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateIn(List<Long> values) {
            addCriterion("commission_rate in", values, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateNotIn(List<Long> values) {
            addCriterion("commission_rate not in", values, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateBetween(Integer value1, Integer value2) {
            addCriterion("commission_rate between", value1, value2, "commission_rate");
            return (Criteria) this;
        }

        public Criteria andcommission_rateNotBetween(Integer value1, Integer value2) {
            addCriterion("commission_rate not between", value1, value2, "commission_rate");
            return (Criteria) this;
        }
        public Criteria andsettle_moneyIsNull() {
            addCriterion("settle_money is null");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyIsNotNull() {
            addCriterion("settle_money is not null");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyEqualTo(String value) {
            addCriterion("settle_money =", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyNotEqualTo(String value) {
            addCriterion("settle_money <>", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyGreaterThan(String value) {
            addCriterion("settle_money >", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyGreaterThanOrEqualTo(String value) {
            addCriterion("settle_money >=", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyLessThan(String value) {
            addCriterion("settle_money <", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyLessThanOrEqualTo(String value) {
            addCriterion("settle_money <=", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyLike(String value) {
            addCriterion("settle_money like", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyNotLike(String value) {
            addCriterion("settle_money not like", value, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyIn(List<String> values) {
            addCriterion("settle_money in", values, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyNotIn(List<String> values) {
            addCriterion("settle_money not in", values, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyBetween(String value1, String value2) {
            addCriterion("settle_money between", value1, value2, "settle_money");
            return (Criteria) this;
        }

        public Criteria andsettle_moneyNotBetween(String value1, String value2) {
            addCriterion("settle_money not between", value1, value2, "settle_money");
            return (Criteria) this;
        }



    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}