package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

/**
 * OTA数据表
 */
public class OtaData implements Serializable {
	//主键id
	private Long id;
	//酒店ID
	private Long hotel_id;
	//平台编码
	private Integer platformCode;
	//平台名称
	private String platformName;
	//账号
	private String platformNumber;
	//密码
	private String platformPassword;
	//cookie/验证码
	private String code;
	//平台酒店名称
	private String platformHotelName;
	//OTA评级(豪华型/舒适型等)
	private String otaGrade;
	//OTA评分
	private String otaScore;
	//点评总数
	private Integer commentTotal;
	//差评数量
	private Integer badCommentTotal;
	//装修时间
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date renovation_time;
	//酒店地址
	private String hotelAddress;
	//客房总数(房间数)
	private Integer roomCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(Long hotel_id) {
		this.hotel_id = hotel_id;
	}

	public Integer getPlatformCode() {
		return platformCode;
	}

	public void setPlatformCode(Integer platformCode) {
		this.platformCode = platformCode;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public String getPlatformNumber() {
		return platformNumber;
	}

	public void setPlatformNumber(String platformNumber) {
		this.platformNumber = platformNumber;
	}

	public String getPlatformPassword() {
		return platformPassword;
	}

	public void setPlatformPassword(String platformPassword) {
		this.platformPassword = platformPassword;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPlatformHotelName() {
		return platformHotelName;
	}

	public void setPlatformHotelName(String platformHotelName) {
		this.platformHotelName = platformHotelName;
	}

	public String getOtaGrade() {
		return otaGrade;
	}

	public void setOtaGrade(String otaGrade) {
		this.otaGrade = otaGrade;
	}

	public String getOtaScore() {
		return otaScore;
	}

	public void setOtaScore(String otaScore) {
		this.otaScore = otaScore;
	}

	public Integer getCommentTotal() {
		return commentTotal;
	}

	public void setCommentTotal(Integer commentTotal) {
		this.commentTotal = commentTotal;
	}

	public Integer getBadCommentTotal() {
		return badCommentTotal;
	}

	public void setBadCommentTotal(Integer badCommentTotal) {
		this.badCommentTotal = badCommentTotal;
	}

	public Date getRenovation_time() {
		return renovation_time;
	}

	public void setRenovation_time(Date renovation_time) {
		this.renovation_time = renovation_time;
	}

	public String getHotelAddress() {
		return hotelAddress;
	}

	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}

	public Integer getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Integer roomCount) {
		this.roomCount = roomCount;
	}

	@Override
	public String toString() {
		return "OtaData{" +
				"id=" + id +
				", hotel_id=" + hotel_id +
				", platformCode=" + platformCode +
				", platformName='" + platformName + '\'' +
				", platformNumber='" + platformNumber + '\'' +
				", platformPassword='" + platformPassword + '\'' +
				", code='" + code + '\'' +
				", platformHotelName='" + platformHotelName + '\'' +
				", otaGrade='" + otaGrade + '\'' +
				", otaScore='" + otaScore + '\'' +
				", commentTotal=" + commentTotal +
				", badCommentTotal=" + badCommentTotal +
				", renovation_time=" + renovation_time +
				", hotelAddress='" + hotelAddress + '\'' +
				", roomCount=" + roomCount +
				'}';
	}
}
