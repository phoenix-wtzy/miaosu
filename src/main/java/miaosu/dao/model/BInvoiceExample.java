package miaosu.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BInvoiceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BInvoiceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHotelidIsNull() {
            addCriterion("hotelId is null");
            return (Criteria) this;
        }

        public Criteria andHotelidIsNotNull() {
            addCriterion("hotelId is not null");
            return (Criteria) this;
        }

        public Criteria andHotelidEqualTo(Long value) {
            addCriterion("hotelId =", value, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidNotEqualTo(Long value) {
            addCriterion("hotelId <>", value, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidGreaterThan(Long value) {
            addCriterion("hotelId >", value, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidGreaterThanOrEqualTo(Long value) {
            addCriterion("hotelId >=", value, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidLessThan(Long value) {
            addCriterion("hotelId <", value, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidLessThanOrEqualTo(Long value) {
            addCriterion("hotelId <=", value, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidIn(List<Long> values) {
            addCriterion("hotelId in", values, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidNotIn(List<Long> values) {
            addCriterion("hotelId not in", values, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidBetween(Long value1, Long value2) {
            addCriterion("hotelId between", value1, value2, "hotelid");
            return (Criteria) this;
        }

        public Criteria andHotelidNotBetween(Long value1, Long value2) {
            addCriterion("hotelId not between", value1, value2, "hotelid");
            return (Criteria) this;
        }

        public Criteria andBuyernameIsNull() {
            addCriterion("buyerName is null");
            return (Criteria) this;
        }

        public Criteria andBuyernameIsNotNull() {
            addCriterion("buyerName is not null");
            return (Criteria) this;
        }

        public Criteria andBuyernameEqualTo(String value) {
            addCriterion("buyerName =", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameNotEqualTo(String value) {
            addCriterion("buyerName <>", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameGreaterThan(String value) {
            addCriterion("buyerName >", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameGreaterThanOrEqualTo(String value) {
            addCriterion("buyerName >=", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameLessThan(String value) {
            addCriterion("buyerName <", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameLessThanOrEqualTo(String value) {
            addCriterion("buyerName <=", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameLike(String value) {
            addCriterion("buyerName like", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameNotLike(String value) {
            addCriterion("buyerName not like", value, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameIn(List<String> values) {
            addCriterion("buyerName in", values, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameNotIn(List<String> values) {
            addCriterion("buyerName not in", values, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameBetween(String value1, String value2) {
            addCriterion("buyerName between", value1, value2, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyernameNotBetween(String value1, String value2) {
            addCriterion("buyerName not between", value1, value2, "buyername");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumIsNull() {
            addCriterion("buyerTaxpayerNum is null");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumIsNotNull() {
            addCriterion("buyerTaxpayerNum is not null");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumEqualTo(String value) {
            addCriterion("buyerTaxpayerNum =", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumNotEqualTo(String value) {
            addCriterion("buyerTaxpayerNum <>", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumGreaterThan(String value) {
            addCriterion("buyerTaxpayerNum >", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumGreaterThanOrEqualTo(String value) {
            addCriterion("buyerTaxpayerNum >=", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumLessThan(String value) {
            addCriterion("buyerTaxpayerNum <", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumLessThanOrEqualTo(String value) {
            addCriterion("buyerTaxpayerNum <=", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumLike(String value) {
            addCriterion("buyerTaxpayerNum like", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumNotLike(String value) {
            addCriterion("buyerTaxpayerNum not like", value, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumIn(List<String> values) {
            addCriterion("buyerTaxpayerNum in", values, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumNotIn(List<String> values) {
            addCriterion("buyerTaxpayerNum not in", values, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumBetween(String value1, String value2) {
            addCriterion("buyerTaxpayerNum between", value1, value2, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyertaxpayernumNotBetween(String value1, String value2) {
            addCriterion("buyerTaxpayerNum not between", value1, value2, "buyertaxpayernum");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressIsNull() {
            addCriterion("buyerAddress is null");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressIsNotNull() {
            addCriterion("buyerAddress is not null");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressEqualTo(String value) {
            addCriterion("buyerAddress =", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressNotEqualTo(String value) {
            addCriterion("buyerAddress <>", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressGreaterThan(String value) {
            addCriterion("buyerAddress >", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressGreaterThanOrEqualTo(String value) {
            addCriterion("buyerAddress >=", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressLessThan(String value) {
            addCriterion("buyerAddress <", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressLessThanOrEqualTo(String value) {
            addCriterion("buyerAddress <=", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressLike(String value) {
            addCriterion("buyerAddress like", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressNotLike(String value) {
            addCriterion("buyerAddress not like", value, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressIn(List<String> values) {
            addCriterion("buyerAddress in", values, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressNotIn(List<String> values) {
            addCriterion("buyerAddress not in", values, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressBetween(String value1, String value2) {
            addCriterion("buyerAddress between", value1, value2, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyeraddressNotBetween(String value1, String value2) {
            addCriterion("buyerAddress not between", value1, value2, "buyeraddress");
            return (Criteria) this;
        }

        public Criteria andBuyertelIsNull() {
            addCriterion("buyerTel is null");
            return (Criteria) this;
        }

        public Criteria andBuyertelIsNotNull() {
            addCriterion("buyerTel is not null");
            return (Criteria) this;
        }

        public Criteria andBuyertelEqualTo(String value) {
            addCriterion("buyerTel =", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelNotEqualTo(String value) {
            addCriterion("buyerTel <>", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelGreaterThan(String value) {
            addCriterion("buyerTel >", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelGreaterThanOrEqualTo(String value) {
            addCriterion("buyerTel >=", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelLessThan(String value) {
            addCriterion("buyerTel <", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelLessThanOrEqualTo(String value) {
            addCriterion("buyerTel <=", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelLike(String value) {
            addCriterion("buyerTel like", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelNotLike(String value) {
            addCriterion("buyerTel not like", value, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelIn(List<String> values) {
            addCriterion("buyerTel in", values, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelNotIn(List<String> values) {
            addCriterion("buyerTel not in", values, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelBetween(String value1, String value2) {
            addCriterion("buyerTel between", value1, value2, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyertelNotBetween(String value1, String value2) {
            addCriterion("buyerTel not between", value1, value2, "buyertel");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameIsNull() {
            addCriterion("buyerBankName is null");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameIsNotNull() {
            addCriterion("buyerBankName is not null");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameEqualTo(String value) {
            addCriterion("buyerBankName =", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameNotEqualTo(String value) {
            addCriterion("buyerBankName <>", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameGreaterThan(String value) {
            addCriterion("buyerBankName >", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameGreaterThanOrEqualTo(String value) {
            addCriterion("buyerBankName >=", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameLessThan(String value) {
            addCriterion("buyerBankName <", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameLessThanOrEqualTo(String value) {
            addCriterion("buyerBankName <=", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameLike(String value) {
            addCriterion("buyerBankName like", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameNotLike(String value) {
            addCriterion("buyerBankName not like", value, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameIn(List<String> values) {
            addCriterion("buyerBankName in", values, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameNotIn(List<String> values) {
            addCriterion("buyerBankName not in", values, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameBetween(String value1, String value2) {
            addCriterion("buyerBankName between", value1, value2, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbanknameNotBetween(String value1, String value2) {
            addCriterion("buyerBankName not between", value1, value2, "buyerbankname");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleIsNull() {
            addCriterion("buyerBankPeople is null");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleIsNotNull() {
            addCriterion("buyerBankPeople is not null");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleEqualTo(String value) {
            addCriterion("buyerBankPeople =", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleNotEqualTo(String value) {
            addCriterion("buyerBankPeople <>", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleGreaterThan(String value) {
            addCriterion("buyerBankPeople >", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleGreaterThanOrEqualTo(String value) {
            addCriterion("buyerBankPeople >=", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleLessThan(String value) {
            addCriterion("buyerBankPeople <", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleLessThanOrEqualTo(String value) {
            addCriterion("buyerBankPeople <=", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleLike(String value) {
            addCriterion("buyerBankPeople like", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleNotLike(String value) {
            addCriterion("buyerBankPeople not like", value, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleIn(List<String> values) {
            addCriterion("buyerBankPeople in", values, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleNotIn(List<String> values) {
            addCriterion("buyerBankPeople not in", values, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleBetween(String value1, String value2) {
            addCriterion("buyerBankPeople between", value1, value2, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankpeopleNotBetween(String value1, String value2) {
            addCriterion("buyerBankPeople not between", value1, value2, "buyerbankpeople");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountIsNull() {
            addCriterion("buyerBankAccount is null");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountIsNotNull() {
            addCriterion("buyerBankAccount is not null");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountEqualTo(String value) {
            addCriterion("buyerBankAccount =", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountNotEqualTo(String value) {
            addCriterion("buyerBankAccount <>", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountGreaterThan(String value) {
            addCriterion("buyerBankAccount >", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountGreaterThanOrEqualTo(String value) {
            addCriterion("buyerBankAccount >=", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountLessThan(String value) {
            addCriterion("buyerBankAccount <", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountLessThanOrEqualTo(String value) {
            addCriterion("buyerBankAccount <=", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountLike(String value) {
            addCriterion("buyerBankAccount like", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountNotLike(String value) {
            addCriterion("buyerBankAccount not like", value, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountIn(List<String> values) {
            addCriterion("buyerBankAccount in", values, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountNotIn(List<String> values) {
            addCriterion("buyerBankAccount not in", values, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountBetween(String value1, String value2) {
            addCriterion("buyerBankAccount between", value1, value2, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andBuyerbankaccountNotBetween(String value1, String value2) {
            addCriterion("buyerBankAccount not between", value1, value2, "buyerbankaccount");
            return (Criteria) this;
        }

        public Criteria andTakernameIsNull() {
            addCriterion("takerName is null");
            return (Criteria) this;
        }

        public Criteria andTakernameIsNotNull() {
            addCriterion("takerName is not null");
            return (Criteria) this;
        }

        public Criteria andTakernameEqualTo(String value) {
            addCriterion("takerName =", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameNotEqualTo(String value) {
            addCriterion("takerName <>", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameGreaterThan(String value) {
            addCriterion("takerName >", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameGreaterThanOrEqualTo(String value) {
            addCriterion("takerName >=", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameLessThan(String value) {
            addCriterion("takerName <", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameLessThanOrEqualTo(String value) {
            addCriterion("takerName <=", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameLike(String value) {
            addCriterion("takerName like", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameNotLike(String value) {
            addCriterion("takerName not like", value, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameIn(List<String> values) {
            addCriterion("takerName in", values, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameNotIn(List<String> values) {
            addCriterion("takerName not in", values, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameBetween(String value1, String value2) {
            addCriterion("takerName between", value1, value2, "takername");
            return (Criteria) this;
        }

        public Criteria andTakernameNotBetween(String value1, String value2) {
            addCriterion("takerName not between", value1, value2, "takername");
            return (Criteria) this;
        }

        public Criteria andTakertelIsNull() {
            addCriterion("takerTel is null");
            return (Criteria) this;
        }

        public Criteria andTakertelIsNotNull() {
            addCriterion("takerTel is not null");
            return (Criteria) this;
        }

        public Criteria andTakertelEqualTo(String value) {
            addCriterion("takerTel =", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelNotEqualTo(String value) {
            addCriterion("takerTel <>", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelGreaterThan(String value) {
            addCriterion("takerTel >", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelGreaterThanOrEqualTo(String value) {
            addCriterion("takerTel >=", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelLessThan(String value) {
            addCriterion("takerTel <", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelLessThanOrEqualTo(String value) {
            addCriterion("takerTel <=", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelLike(String value) {
            addCriterion("takerTel like", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelNotLike(String value) {
            addCriterion("takerTel not like", value, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelIn(List<String> values) {
            addCriterion("takerTel in", values, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelNotIn(List<String> values) {
            addCriterion("takerTel not in", values, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelBetween(String value1, String value2) {
            addCriterion("takerTel between", value1, value2, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakertelNotBetween(String value1, String value2) {
            addCriterion("takerTel not between", value1, value2, "takertel");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressIsNull() {
            addCriterion("takerDetailAddress is null");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressIsNotNull() {
            addCriterion("takerDetailAddress is not null");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressEqualTo(String value) {
            addCriterion("takerDetailAddress =", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressNotEqualTo(String value) {
            addCriterion("takerDetailAddress <>", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressGreaterThan(String value) {
            addCriterion("takerDetailAddress >", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressGreaterThanOrEqualTo(String value) {
            addCriterion("takerDetailAddress >=", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressLessThan(String value) {
            addCriterion("takerDetailAddress <", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressLessThanOrEqualTo(String value) {
            addCriterion("takerDetailAddress <=", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressLike(String value) {
            addCriterion("takerDetailAddress like", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressNotLike(String value) {
            addCriterion("takerDetailAddress not like", value, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressIn(List<String> values) {
            addCriterion("takerDetailAddress in", values, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressNotIn(List<String> values) {
            addCriterion("takerDetailAddress not in", values, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressBetween(String value1, String value2) {
            addCriterion("takerDetailAddress between", value1, value2, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakerdetailaddressNotBetween(String value1, String value2) {
            addCriterion("takerDetailAddress not between", value1, value2, "takerdetailaddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressIsNull() {
            addCriterion("takerAddress is null");
            return (Criteria) this;
        }

        public Criteria andTakeraddressIsNotNull() {
            addCriterion("takerAddress is not null");
            return (Criteria) this;
        }

        public Criteria andTakeraddressEqualTo(String value) {
            addCriterion("takerAddress =", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressNotEqualTo(String value) {
            addCriterion("takerAddress <>", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressGreaterThan(String value) {
            addCriterion("takerAddress >", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressGreaterThanOrEqualTo(String value) {
            addCriterion("takerAddress >=", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressLessThan(String value) {
            addCriterion("takerAddress <", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressLessThanOrEqualTo(String value) {
            addCriterion("takerAddress <=", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressLike(String value) {
            addCriterion("takerAddress like", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressNotLike(String value) {
            addCriterion("takerAddress not like", value, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressIn(List<String> values) {
            addCriterion("takerAddress in", values, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressNotIn(List<String> values) {
            addCriterion("takerAddress not in", values, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressBetween(String value1, String value2) {
            addCriterion("takerAddress between", value1, value2, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeraddressNotBetween(String value1, String value2) {
            addCriterion("takerAddress not between", value1, value2, "takeraddress");
            return (Criteria) this;
        }

        public Criteria andTakeremailIsNull() {
            addCriterion("takerEmail is null");
            return (Criteria) this;
        }

        public Criteria andTakeremailIsNotNull() {
            addCriterion("takerEmail is not null");
            return (Criteria) this;
        }

        public Criteria andTakeremailEqualTo(String value) {
            addCriterion("takerEmail =", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailNotEqualTo(String value) {
            addCriterion("takerEmail <>", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailGreaterThan(String value) {
            addCriterion("takerEmail >", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailGreaterThanOrEqualTo(String value) {
            addCriterion("takerEmail >=", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailLessThan(String value) {
            addCriterion("takerEmail <", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailLessThanOrEqualTo(String value) {
            addCriterion("takerEmail <=", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailLike(String value) {
            addCriterion("takerEmail like", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailNotLike(String value) {
            addCriterion("takerEmail not like", value, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailIn(List<String> values) {
            addCriterion("takerEmail in", values, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailNotIn(List<String> values) {
            addCriterion("takerEmail not in", values, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailBetween(String value1, String value2) {
            addCriterion("takerEmail between", value1, value2, "takeremail");
            return (Criteria) this;
        }

        public Criteria andTakeremailNotBetween(String value1, String value2) {
            addCriterion("takerEmail not between", value1, value2, "takeremail");
            return (Criteria) this;
        }

        public Criteria andItemnameIsNull() {
            addCriterion("itemName is null");
            return (Criteria) this;
        }

        public Criteria andItemnameIsNotNull() {
            addCriterion("itemName is not null");
            return (Criteria) this;
        }

        public Criteria andItemnameEqualTo(String value) {
            addCriterion("itemName =", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotEqualTo(String value) {
            addCriterion("itemName <>", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameGreaterThan(String value) {
            addCriterion("itemName >", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameGreaterThanOrEqualTo(String value) {
            addCriterion("itemName >=", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameLessThan(String value) {
            addCriterion("itemName <", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameLessThanOrEqualTo(String value) {
            addCriterion("itemName <=", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameLike(String value) {
            addCriterion("itemName like", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotLike(String value) {
            addCriterion("itemName not like", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameIn(List<String> values) {
            addCriterion("itemName in", values, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotIn(List<String> values) {
            addCriterion("itemName not in", values, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameBetween(String value1, String value2) {
            addCriterion("itemName between", value1, value2, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotBetween(String value1, String value2) {
            addCriterion("itemName not between", value1, value2, "itemname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameIsNull() {
            addCriterion("goodsName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsnameIsNotNull() {
            addCriterion("goodsName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsnameEqualTo(String value) {
            addCriterion("goodsName =", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotEqualTo(String value) {
            addCriterion("goodsName <>", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameGreaterThan(String value) {
            addCriterion("goodsName >", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsName >=", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameLessThan(String value) {
            addCriterion("goodsName <", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameLessThanOrEqualTo(String value) {
            addCriterion("goodsName <=", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameLike(String value) {
            addCriterion("goodsName like", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotLike(String value) {
            addCriterion("goodsName not like", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameIn(List<String> values) {
            addCriterion("goodsName in", values, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotIn(List<String> values) {
            addCriterion("goodsName not in", values, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameBetween(String value1, String value2) {
            addCriterion("goodsName between", value1, value2, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotBetween(String value1, String value2) {
            addCriterion("goodsName not between", value1, value2, "goodsname");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNull() {
            addCriterion("quantity is null");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNotNull() {
            addCriterion("quantity is not null");
            return (Criteria) this;
        }

        public Criteria andQuantityEqualTo(Integer value) {
            addCriterion("quantity =", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotEqualTo(Integer value) {
            addCriterion("quantity <>", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThan(Integer value) {
            addCriterion("quantity >", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("quantity >=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThan(Integer value) {
            addCriterion("quantity <", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("quantity <=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityIn(List<Integer> values) {
            addCriterion("quantity in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotIn(List<Integer> values) {
            addCriterion("quantity not in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityBetween(Integer value1, Integer value2) {
            addCriterion("quantity between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("quantity not between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andUnitpriceIsNull() {
            addCriterion("unitPrice is null");
            return (Criteria) this;
        }

        public Criteria andUnitpriceIsNotNull() {
            addCriterion("unitPrice is not null");
            return (Criteria) this;
        }

        public Criteria andUnitpriceEqualTo(BigDecimal value) {
            addCriterion("unitPrice =", value, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceNotEqualTo(BigDecimal value) {
            addCriterion("unitPrice <>", value, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceGreaterThan(BigDecimal value) {
            addCriterion("unitPrice >", value, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("unitPrice >=", value, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceLessThan(BigDecimal value) {
            addCriterion("unitPrice <", value, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("unitPrice <=", value, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceIn(List<BigDecimal> values) {
            addCriterion("unitPrice in", values, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceNotIn(List<BigDecimal> values) {
            addCriterion("unitPrice not in", values, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("unitPrice between", value1, value2, "unitprice");
            return (Criteria) this;
        }

        public Criteria andUnitpriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("unitPrice not between", value1, value2, "unitprice");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountIsNull() {
            addCriterion("invoiceAmount is null");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountIsNotNull() {
            addCriterion("invoiceAmount is not null");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountEqualTo(BigDecimal value) {
            addCriterion("invoiceAmount =", value, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountNotEqualTo(BigDecimal value) {
            addCriterion("invoiceAmount <>", value, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountGreaterThan(BigDecimal value) {
            addCriterion("invoiceAmount >", value, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("invoiceAmount >=", value, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountLessThan(BigDecimal value) {
            addCriterion("invoiceAmount <", value, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("invoiceAmount <=", value, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountIn(List<BigDecimal> values) {
            addCriterion("invoiceAmount in", values, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountNotIn(List<BigDecimal> values) {
            addCriterion("invoiceAmount not in", values, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("invoiceAmount between", value1, value2, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andInvoiceamountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("invoiceAmount not between", value1, value2, "invoiceamount");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueIsNull() {
            addCriterion("taxRateValue is null");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueIsNotNull() {
            addCriterion("taxRateValue is not null");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueEqualTo(BigDecimal value) {
            addCriterion("taxRateValue =", value, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueNotEqualTo(BigDecimal value) {
            addCriterion("taxRateValue <>", value, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueGreaterThan(BigDecimal value) {
            addCriterion("taxRateValue >", value, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("taxRateValue >=", value, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueLessThan(BigDecimal value) {
            addCriterion("taxRateValue <", value, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("taxRateValue <=", value, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueIn(List<BigDecimal> values) {
            addCriterion("taxRateValue in", values, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueNotIn(List<BigDecimal> values) {
            addCriterion("taxRateValue not in", values, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxRateValue between", value1, value2, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxratevalueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxRateValue not between", value1, value2, "taxratevalue");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountIsNull() {
            addCriterion("taxRateAmount is null");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountIsNotNull() {
            addCriterion("taxRateAmount is not null");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountEqualTo(BigDecimal value) {
            addCriterion("taxRateAmount =", value, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountNotEqualTo(BigDecimal value) {
            addCriterion("taxRateAmount <>", value, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountGreaterThan(BigDecimal value) {
            addCriterion("taxRateAmount >", value, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("taxRateAmount >=", value, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountLessThan(BigDecimal value) {
            addCriterion("taxRateAmount <", value, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("taxRateAmount <=", value, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountIn(List<BigDecimal> values) {
            addCriterion("taxRateAmount in", values, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountNotIn(List<BigDecimal> values) {
            addCriterion("taxRateAmount not in", values, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxRateAmount between", value1, value2, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTaxrateamountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxRateAmount not between", value1, value2, "taxrateamount");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andCashernameIsNull() {
            addCriterion("casherName is null");
            return (Criteria) this;
        }

        public Criteria andCashernameIsNotNull() {
            addCriterion("casherName is not null");
            return (Criteria) this;
        }

        public Criteria andCashernameEqualTo(String value) {
            addCriterion("casherName =", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameNotEqualTo(String value) {
            addCriterion("casherName <>", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameGreaterThan(String value) {
            addCriterion("casherName >", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameGreaterThanOrEqualTo(String value) {
            addCriterion("casherName >=", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameLessThan(String value) {
            addCriterion("casherName <", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameLessThanOrEqualTo(String value) {
            addCriterion("casherName <=", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameLike(String value) {
            addCriterion("casherName like", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameNotLike(String value) {
            addCriterion("casherName not like", value, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameIn(List<String> values) {
            addCriterion("casherName in", values, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameNotIn(List<String> values) {
            addCriterion("casherName not in", values, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameBetween(String value1, String value2) {
            addCriterion("casherName between", value1, value2, "cashername");
            return (Criteria) this;
        }

        public Criteria andCashernameNotBetween(String value1, String value2) {
            addCriterion("casherName not between", value1, value2, "cashername");
            return (Criteria) this;
        }

        public Criteria andReviewernameIsNull() {
            addCriterion("reviewerName is null");
            return (Criteria) this;
        }

        public Criteria andReviewernameIsNotNull() {
            addCriterion("reviewerName is not null");
            return (Criteria) this;
        }

        public Criteria andReviewernameEqualTo(String value) {
            addCriterion("reviewerName =", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameNotEqualTo(String value) {
            addCriterion("reviewerName <>", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameGreaterThan(String value) {
            addCriterion("reviewerName >", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameGreaterThanOrEqualTo(String value) {
            addCriterion("reviewerName >=", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameLessThan(String value) {
            addCriterion("reviewerName <", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameLessThanOrEqualTo(String value) {
            addCriterion("reviewerName <=", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameLike(String value) {
            addCriterion("reviewerName like", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameNotLike(String value) {
            addCriterion("reviewerName not like", value, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameIn(List<String> values) {
            addCriterion("reviewerName in", values, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameNotIn(List<String> values) {
            addCriterion("reviewerName not in", values, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameBetween(String value1, String value2) {
            addCriterion("reviewerName between", value1, value2, "reviewername");
            return (Criteria) this;
        }

        public Criteria andReviewernameNotBetween(String value1, String value2) {
            addCriterion("reviewerName not between", value1, value2, "reviewername");
            return (Criteria) this;
        }

        public Criteria andDrawernameIsNull() {
            addCriterion("drawerName is null");
            return (Criteria) this;
        }

        public Criteria andDrawernameIsNotNull() {
            addCriterion("drawerName is not null");
            return (Criteria) this;
        }

        public Criteria andDrawernameEqualTo(String value) {
            addCriterion("drawerName =", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameNotEqualTo(String value) {
            addCriterion("drawerName <>", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameGreaterThan(String value) {
            addCriterion("drawerName >", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameGreaterThanOrEqualTo(String value) {
            addCriterion("drawerName >=", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameLessThan(String value) {
            addCriterion("drawerName <", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameLessThanOrEqualTo(String value) {
            addCriterion("drawerName <=", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameLike(String value) {
            addCriterion("drawerName like", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameNotLike(String value) {
            addCriterion("drawerName not like", value, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameIn(List<String> values) {
            addCriterion("drawerName in", values, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameNotIn(List<String> values) {
            addCriterion("drawerName not in", values, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameBetween(String value1, String value2) {
            addCriterion("drawerName between", value1, value2, "drawername");
            return (Criteria) this;
        }

        public Criteria andDrawernameNotBetween(String value1, String value2) {
            addCriterion("drawerName not between", value1, value2, "drawername");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("createDate is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("createDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("createDate =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("createDate <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("createDate >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("createDate >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("createDate <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("createDate <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("createDate in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("createDate not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("createDate between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("createDate not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("updateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("updateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("updateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("updateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("updateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("updateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("updateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("updateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("updateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("updateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("updateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("updateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}