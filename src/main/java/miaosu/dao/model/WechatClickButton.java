package miaosu.dao.model;

public class WechatClickButton extends WechatAbstractButton{
    private String type ="click";
    private String key;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public WechatClickButton(String name, String key) {
        super(name);
        this.key = key;
    }
}
