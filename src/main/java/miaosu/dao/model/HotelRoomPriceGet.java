package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2021/3/12 17:08
 * @Description: 前端传递的房价封装对象
 */
public class HotelRoomPriceGet {
	private Long product_id;//产品id
	private String date_start;//开始时间
	private String date_end;//结束时间
	private String price;//价格


	public Long getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}

	public String getDate_start() {
		return date_start;
	}

	public void setDate_start(String date_start) {
		this.date_start = date_start;
	}

	public String getDate_end() {
		return date_end;
	}

	public void setDate_end(String date_end) {
		this.date_end = date_end;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
}
