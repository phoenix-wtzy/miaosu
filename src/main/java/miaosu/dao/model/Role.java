package miaosu.dao.model;

public class Role {
    /**
     * 递增主键
     */
    private Long roleId;

    /**
     * 权限名称
     */
    private String roleName;

    //角色描述
    private String role_description;


    public String getRole_description() {
        return role_description;
    }

    public void setRole_description(String role_description) {
        this.role_description = role_description;
    }



    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }
}