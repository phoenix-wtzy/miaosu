package miaosu.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/2/3 14:02
 * @Description:
 */
public class HotelSupplierExample {
	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	public HotelSupplierExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andhotel_supplier_idIsNull() {
			addCriterion("hotel_supplier_id is null");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idIsNotNull() {
			addCriterion("hotel_supplier_id is not null");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idEqualTo(Long value) {
			addCriterion("hotel_supplier_id =", value, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idNotEqualTo(Long value) {
			addCriterion("hotel_supplier_id <>", value, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idGreaterThan(Long value) {
			addCriterion("hotel_supplier_id >", value, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idGreaterThanOrEqualTo(Long value) {
			addCriterion("hotel_supplier_id >=", value, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idLessThan(Long value) {
			addCriterion("hotel_supplier_id <", value, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idLessThanOrEqualTo(Long value) {
			addCriterion("hotel_supplier_id <=", value, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idIn(List<Long> values) {
			addCriterion("hotel_supplier_id in", values, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idNotIn(List<Long> values) {
			addCriterion("hotel_supplier_id not in", values, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idBetween(Long value1, Long value2) {
			addCriterion("hotel_supplier_id between", value1, value2, "hotel_supplier_id");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_idNotBetween(Long value1, Long value2) {
			addCriterion("hotel_supplier_id not between", value1, value2, "hotel_supplier_id");
			return (Criteria) this;
		}




		public Criteria andhotel_idIsNull() {
			addCriterion("hotel_id is null");
			return (Criteria) this;
		}

		public Criteria andhotel_idIsNotNull() {
			addCriterion("hotel_id is not null");
			return (Criteria) this;
		}

		public Criteria andhotel_idEqualTo(Long value) {
			addCriterion("hotel_id =", value, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idNotEqualTo(Long value) {
			addCriterion("hotel_id <>", value, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idGreaterThan(Long value) {
			addCriterion("hotel_id >", value, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idGreaterThanOrEqualTo(Long value) {
			addCriterion("hotel_id >=", value, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idLessThan(Long value) {
			addCriterion("hotel_id <", value, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idLessThanOrEqualTo(Long value) {
			addCriterion("hotel_id <=", value, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idIn(List<Long> values) {
			addCriterion("hotel_id in", values, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idNotIn(List<Long> values) {
			addCriterion("hotel_id not in", values, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idBetween(Long value1, Long value2) {
			addCriterion("hotel_id between", value1, value2, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andhotel_idNotBetween(Long value1, Long value2) {
			addCriterion("hotel_id not between", value1, value2, "hotel_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idIsNull() {
			addCriterion("business_id is null");
			return (Criteria) this;
		}

		public Criteria andbusiness_idIsNotNull() {
			addCriterion("business_id is not null");
			return (Criteria) this;
		}

		public Criteria andbusiness_idEqualTo(Long value) {
			addCriterion("business_id =", value, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idNotEqualTo(Long value) {
			addCriterion("business_id <>", value, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idGreaterThan(Long value) {
			addCriterion("business_id >", value, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idGreaterThanOrEqualTo(Long value) {
			addCriterion("business_id >=", value, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idLessThan(Long value) {
			addCriterion("business_id <", value, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idLessThanOrEqualTo(Long value) {
			addCriterion("business_id <=", value, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idIn(List<Long> values) {
			addCriterion("business_id in", values, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idNotIn(List<Long> values) {
			addCriterion("business_id not in", values, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idBetween(Long value1, Long value2) {
			addCriterion("business_id between", value1, value2, "business_id");
			return (Criteria) this;
		}

		public Criteria andbusiness_idNotBetween(Long value1, Long value2) {
			addCriterion("business_id not between", value1, value2, "business_id");
			return (Criteria) this;
		}

		public Criteria andhotelTypeNull() {
			addCriterion("hotelType is null");
			return (Criteria) this;
		}

		public Criteria andhotelTypeNotNull() {
			addCriterion("hotelType is not null");
			return (Criteria) this;
		}

		public Criteria andhotelTypeEqualTo(int value) {
			addCriterion("hotelType =", value, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeNotEqualTo(int value) {
			addCriterion("hotelType <>", value, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeGreaterThan(int value) {
			addCriterion("hotelType >", value, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeGreaterThanOrEqualTo(int value) {
			addCriterion("hotelType >=", value, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeLessThan(int value) {
			addCriterion("hotelType <", value, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeLessThanOrEqualTo(int value) {
			addCriterion("hotelType <=", value, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeIn(List<Long> values) {
			addCriterion("hotelType in", values, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeNotIn(List<Long> values) {
			addCriterion("hotelType not in", values, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeBetween(int value1, int value2) {
			addCriterion("hotelType between", value1, value2, "hotelType");
			return (Criteria) this;
		}

		public Criteria andhotelTypeNotBetween(int value1, int value2) {
			addCriterion("hotelType not between", value1, value2, "hotelType");
			return (Criteria) this;
		}

		public Criteria andcommission_rateNull() {
			addCriterion("commission_rate is null");
			return (Criteria) this;
		}

		public Criteria andcommission_rateNotNull() {
			addCriterion("commission_rate is not null");
			return (Criteria) this;
		}

		public Criteria andcommission_rateEqualTo(int value) {
			addCriterion("commission_rate =", value, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateNotEqualTo(int value) {
			addCriterion("commission_rate <>", value, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateGreaterThan(int value) {
			addCriterion("commission_rate >", value, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateGreaterThanOrEqualTo(int value) {
			addCriterion("commission_rate >=", value, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateLessThan(int value) {
			addCriterion("commission_rate <", value, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateLessThanOrEqualTo(int value) {
			addCriterion("commission_rate <=", value, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateIn(List<Long> values) {
			addCriterion("commission_rate in", values, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateNotIn(List<Long> values) {
			addCriterion("commission_rate not in", values, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateBetween(int value1, int value2) {
			addCriterion("commission_rate between", value1, value2, "commission_rate");
			return (Criteria) this;
		}

		public Criteria andcommission_rateNotBetween(int value1, int value2) {
			addCriterion("commission_rate not between", value1, value2, "commission_rate");
			return (Criteria) this;
		}










		public Criteria andsettle_totalMoneyIsNull() {
			addCriterion("settle_totalMoney is null");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyIsNotNull() {
			addCriterion("settle_totalMoney is not null");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyEqualTo(String value) {
			addCriterion("settle_totalMoney =", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyNotEqualTo(String value) {
			addCriterion("settle_totalMoney <>", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyGreaterThan(String value) {
			addCriterion("settle_totalMoney >", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyGreaterThanOrEqualTo(String value) {
			addCriterion("settle_totalMoney >=", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyLessThan(String value) {
			addCriterion("settle_totalMoney <", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyLessThanOrEqualTo(String value) {
			addCriterion("settle_totalMoney <=", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyLike(String value) {
			addCriterion("settle_totalMoney like", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyNotLike(String value) {
			addCriterion("settle_totalMoney not like", value, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyIn(List<String> values) {
			addCriterion("settle_totalMoney in", values, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyNotIn(List<String> values) {
			addCriterion("settle_totalMoney not in", values, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyBetween(String value1, String value2) {
			addCriterion("settle_totalMoney between", value1, value2, "settle_totalMoney");
			return (Criteria) this;
		}

		public Criteria andsettle_totalMoneyNotBetween(String value1, String value2) {
			addCriterion("settle_totalMoney not between", value1, value2, "settle_totalMoney");
			return (Criteria) this;
		}


		public Criteria andsettle_totalNightIsNull() {
			addCriterion("settle_totalNight is null");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightIsNotNull() {
			addCriterion("settle_totalNight is not null");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightEqualTo(String value) {
			addCriterion("settle_totalNight =", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightNotEqualTo(String value) {
			addCriterion("settle_totalNight <>", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightGreaterThan(String value) {
			addCriterion("settle_totalNight >", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightGreaterThanOrEqualTo(String value) {
			addCriterion("settle_totalNight >=", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightLessThan(String value) {
			addCriterion("settle_totalNight <", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightLessThanOrEqualTo(String value) {
			addCriterion("settle_totalNight <=", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightLike(String value) {
			addCriterion("settle_totalNight like", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightNotLike(String value) {
			addCriterion("settle_totalNight not like", value, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightIn(List<String> values) {
			addCriterion("settle_totalNight in", values, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightNotIn(List<String> values) {
			addCriterion("settle_totalNight not in", values, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightBetween(String value1, String value2) {
			addCriterion("settle_totalNight between", value1, value2, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andsettle_totalNightNotBetween(String value1, String value2) {
			addCriterion("settle_totalNight not between", value1, value2, "settle_totalNight");
			return (Criteria) this;
		}

		public Criteria andcreate_timeIsNull() {
			addCriterion("create_time is null");
			return (Criteria) this;
		}

		public Criteria andcreate_timeIsNotNull() {
			addCriterion("create_time is not null");
			return (Criteria) this;
		}

		public Criteria andcreate_timeEqualTo(Date value) {
			addCriterion("create_time =", value, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeNotEqualTo(Date value) {
			addCriterion("create_time <>", value, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeGreaterThan(Date value) {
			addCriterion("create_time >", value, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeGreaterThanOrEqualTo(Date value) {
			addCriterion("create_time >=", value, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeLessThan(Date value) {
			addCriterion("create_time <", value, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeLessThanOrEqualTo(Date value) {
			addCriterion("create_time <=", value, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeIn(List<Date> values) {
			addCriterion("create_time in", values, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeNotIn(List<Date> values) {
			addCriterion("create_time not in", values, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeBetween(Date value1, Date value2) {
			addCriterion("create_time between", value1, value2, "create_time");
			return (Criteria) this;
		}

		public Criteria andcreate_timeNotBetween(Date value1, Date value2) {
			addCriterion("create_time not between", value1, value2, "create_time");
			return (Criteria) this;
		}


		public Criteria andstart_timeIsNull() {
			addCriterion("start_time is null");
			return (Criteria) this;
		}

		public Criteria andstart_timeIsNotNull() {
			addCriterion("start_time is not null");
			return (Criteria) this;
		}

		public Criteria andstart_timeEqualTo(Date value) {
			addCriterion("start_time =", value, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeNotEqualTo(Date value) {
			addCriterion("start_time <>", value, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeGreaterThan(Date value) {
			addCriterion("start_time >", value, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeGreaterThanOrEqualTo(Date value) {
			addCriterion("start_time >=", value, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeLessThan(Date value) {
			addCriterion("start_time <", value, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeLessThanOrEqualTo(Date value) {
			addCriterion("start_time <=", value, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeIn(List<Date> values) {
			addCriterion("start_time in", values, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeNotIn(List<Date> values) {
			addCriterion("start_time not in", values, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeBetween(Date value1, Date value2) {
			addCriterion("start_time between", value1, value2, "start_time");
			return (Criteria) this;
		}

		public Criteria andstart_timeNotBetween(Date value1, Date value2) {
			addCriterion("start_time not between", value1, value2, "start_time");
			return (Criteria) this;
		}


		public Criteria andend_timeIsNull() {
			addCriterion("end_time is null");
			return (Criteria) this;
		}

		public Criteria andend_timeIsNotNull() {
			addCriterion("end_time is not null");
			return (Criteria) this;
		}

		public Criteria andend_timeEqualTo(Date value) {
			addCriterion("end_time =", value, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeNotEqualTo(Date value) {
			addCriterion("end_time <>", value, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeGreaterThan(Date value) {
			addCriterion("end_time >", value, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeGreaterThanOrEqualTo(Date value) {
			addCriterion("end_time >=", value, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeLessThan(Date value) {
			addCriterion("end_time <", value, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeLessThanOrEqualTo(Date value) {
			addCriterion("end_time <=", value, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeIn(List<Date> values) {
			addCriterion("end_time in", values, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeNotIn(List<Date> values) {
			addCriterion("end_time not in", values, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeBetween(Date value1, Date value2) {
			addCriterion("end_time between", value1, value2, "end_time");
			return (Criteria) this;
		}

		public Criteria andend_timeNotBetween(Date value1, Date value2) {
			addCriterion("end_time not between", value1, value2, "end_time");
			return (Criteria) this;
		}


		public Criteria andhotel_supplier_stateNull() {
			addCriterion("hotel_supplier_state is null");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateNotNull() {
			addCriterion("hotel_supplier_state is not null");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateEqualTo(int value) {
			addCriterion("hotel_supplier_state =", value, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateNotEqualTo(int value) {
			addCriterion("hotel_supplier_state <>", value, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateGreaterThan(int value) {
			addCriterion("hotel_supplier_state >", value, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateGreaterThanOrEqualTo(int value) {
			addCriterion("hotel_supplier_state >=", value, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateLessThan(int value) {
			addCriterion("hotel_supplier_state <", value, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateLessThanOrEqualTo(int value) {
			addCriterion("hotel_supplier_state <=", value, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateIn(List<Long> values) {
			addCriterion("hotel_supplier_state in", values, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateNotIn(List<Long> values) {
			addCriterion("hotel_supplier_state not in", values, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateBetween(int value1, int value2) {
			addCriterion("hotel_supplier_state between", value1, value2, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andhotel_supplier_stateNotBetween(int value1, int value2) {
			addCriterion("hotel_supplier_state not between", value1, value2, "hotel_supplier_state");
			return (Criteria) this;
		}

		public Criteria andremarkIsNull() {
			addCriterion("remark is null");
			return (Criteria) this;
		}

		public Criteria andremarkIsNotNull() {
			addCriterion("remark is not null");
			return (Criteria) this;
		}

		public Criteria andremarkEqualTo(String value) {
			addCriterion("remark =", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkNotEqualTo(String value) {
			addCriterion("remark <>", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkGreaterThan(String value) {
			addCriterion("remark >", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkGreaterThanOrEqualTo(String value) {
			addCriterion("remark >=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkLessThan(String value) {
			addCriterion("remark <", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkLessThanOrEqualTo(String value) {
			addCriterion("remark <=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkLike(String value) {
			addCriterion("remark like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkNotLike(String value) {
			addCriterion("remark not like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkIn(List<String> values) {
			addCriterion("remark in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkNotIn(List<String> values) {
			addCriterion("remark not in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkBetween(String value1, String value2) {
			addCriterion("remark between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andremarkNotBetween(String value1, String value2) {
			addCriterion("remark not between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andextIsNull() {
			addCriterion("ext is null");
			return (Criteria) this;
		}

		public Criteria andextIsNotNull() {
			addCriterion("ext is not null");
			return (Criteria) this;
		}

		public Criteria andextEqualTo(String value) {
			addCriterion("ext =", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextNotEqualTo(String value) {
			addCriterion("ext <>", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextGreaterThan(String value) {
			addCriterion("ext >", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextGreaterThanOrEqualTo(String value) {
			addCriterion("ext >=", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextLessThan(String value) {
			addCriterion("ext <", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextLessThanOrEqualTo(String value) {
			addCriterion("ext <=", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextLike(String value) {
			addCriterion("ext like", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextNotLike(String value) {
			addCriterion("ext not like", value, "ext");
			return (Criteria) this;
		}

		public Criteria andextIn(List<String> values) {
			addCriterion("ext in", values, "ext");
			return (Criteria) this;
		}

		public Criteria andextNotIn(List<String> values) {
			addCriterion("ext not in", values, "ext");
			return (Criteria) this;
		}

		public Criteria andextBetween(String value1, String value2) {
			addCriterion("ext between", value1, value2, "ext");
			return (Criteria) this;
		}

		public Criteria andextNotBetween(String value1, String value2) {
			addCriterion("ext not between", value1, value2, "ext");
			return (Criteria) this;
		}


	}


	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}


	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}
