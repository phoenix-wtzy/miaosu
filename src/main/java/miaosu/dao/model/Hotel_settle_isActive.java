package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @author Administrator
 * @Date: 2020/12/15 14:59
 * @Description: 上下线酒店结算汇总
 */
public class Hotel_settle_isActive {

	private Long hotelId;//酒店id
	private String hotel_name;//酒店名称
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date createTime;//酒店创建时间
	private String settle_totalMoney;//协议置换总金额
	private String settle_alreadyMoney;//已置换总金额
	private String settle_haveMoney;//剩余置换总金额
	private String settle_totalNight;//协议置换间夜数
	private String settle_alreadyNight;//已置换间夜
	private String settle_haveNight;//剩余置换间夜
	private String order_totalMoney;//实际订单总金额
	private int commission_rate;//佣金率
	private String commission;//佣金
	private String needPay_money;//应付
	private String alreadyPay_money;//已付
	private String havePay_money;//余额
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date todayTime;//今天的时间记录

	public Date getTodayTime() {
		return todayTime;
	}

	public void setTodayTime(Date todayTime) {
		this.todayTime = todayTime;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getSettle_totalMoney() {
		return settle_totalMoney;
	}

	public void setSettle_totalMoney(String settle_totalMoney) {
		this.settle_totalMoney = settle_totalMoney;
	}

	public String getSettle_alreadyMoney() {
		return settle_alreadyMoney;
	}

	public void setSettle_alreadyMoney(String settle_alreadyMoney) {
		this.settle_alreadyMoney = settle_alreadyMoney;
	}

	public String getSettle_haveMoney() {
		return settle_haveMoney;
	}

	public void setSettle_haveMoney(String settle_haveMoney) {
		this.settle_haveMoney = settle_haveMoney;
	}

	public String getSettle_totalNight() {
		return settle_totalNight;
	}

	public void setSettle_totalNight(String settle_totalNight) {
		this.settle_totalNight = settle_totalNight;
	}

	public String getSettle_alreadyNight() {
		return settle_alreadyNight;
	}

	public void setSettle_alreadyNight(String settle_alreadyNight) {
		this.settle_alreadyNight = settle_alreadyNight;
	}

	public String getSettle_haveNight() {
		return settle_haveNight;
	}

	public void setSettle_haveNight(String settle_haveNight) {
		this.settle_haveNight = settle_haveNight;
	}

	public String getOrder_totalMoney() {
		return order_totalMoney;
	}

	public void setOrder_totalMoney(String order_totalMoney) {
		this.order_totalMoney = order_totalMoney;
	}

	public int getCommission_rate() {
		return commission_rate;
	}

	public void setCommission_rate(int commission_rate) {
		this.commission_rate = commission_rate;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getNeedPay_money() {
		return needPay_money;
	}

	public void setNeedPay_money(String needPay_money) {
		this.needPay_money = needPay_money;
	}

	public String getAlreadyPay_money() {
		return alreadyPay_money;
	}

	public void setAlreadyPay_money(String alreadyPay_money) {
		this.alreadyPay_money = alreadyPay_money;
	}

	public String getHavePay_money() {
		return havePay_money;
	}

	public void setHavePay_money(String havePay_money) {
		this.havePay_money = havePay_money;
	}
}
