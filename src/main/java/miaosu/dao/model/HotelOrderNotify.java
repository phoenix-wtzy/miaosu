package miaosu.dao.model;

public class HotelOrderNotify {
    /**
     * 订单号
     */
    private Long orderId;

    /**
     * 通知状态0待通知，1已通知到
     */
    private Integer state;

    /**
     * 通知类型0web 1wechat 2mobile
     */
    private Integer notifyType;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(Integer notifyType) {
        this.notifyType = notifyType;
    }
}