package miaosu.dao.model;

public class HotelPriceRefRoomType {
    private Long productId;

    private Long hotelPriceId;

    private Long hotelRoomTypeId;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getHotelPriceId() {
        return hotelPriceId;
    }

    public void setHotelPriceId(Long hotelPriceId) {
        this.hotelPriceId = hotelPriceId;
    }

    public Long getHotelRoomTypeId() {
        return hotelRoomTypeId;
    }

    public void setHotelRoomTypeId(Long hotelRoomTypeId) {
        this.hotelRoomTypeId = hotelRoomTypeId;
    }
}