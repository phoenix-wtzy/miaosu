package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @author Administrator
 * @Date: 2021/1/4 9:39
 * @Description: 首页昨日/今日/汇总/酒店统计
 */
public class Charts {
	private String order_today_size;//今日订单数
	private String order_today_night;//今日间夜数
	private String order_today_price;//今日订单额
	private String order_yesterday_size;//昨日订单数
	private String order_yesterday_night;//昨日间夜数
	private String order_yesterday_price;//昨日订单额
	private String order_all_size;//汇总订单总数
	private String order_all_night;//汇总总间夜数
	private String order_all_price;//汇总总订单额
	private String hotel_all_IsActive;//在线酒店数量
	private String hotel_all_NotIsActive;//下线酒店数量
	private String hotel_all_addInThisMonth;//本月新增酒店
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date todayTime;//今天的时间记录


	public String getOrder_today_size() {
		return order_today_size;
	}

	public void setOrder_today_size(String order_today_size) {
		this.order_today_size = order_today_size;
	}

	public String getOrder_today_night() {
		return order_today_night;
	}

	public void setOrder_today_night(String order_today_night) {
		this.order_today_night = order_today_night;
	}

	public String getOrder_today_price() {
		return order_today_price;
	}

	public void setOrder_today_price(String order_today_price) {
		this.order_today_price = order_today_price;
	}

	public String getOrder_yesterday_size() {
		return order_yesterday_size;
	}

	public void setOrder_yesterday_size(String order_yesterday_size) {
		this.order_yesterday_size = order_yesterday_size;
	}

	public String getOrder_yesterday_night() {
		return order_yesterday_night;
	}

	public void setOrder_yesterday_night(String order_yesterday_night) {
		this.order_yesterday_night = order_yesterday_night;
	}

	public String getOrder_yesterday_price() {
		return order_yesterday_price;
	}

	public void setOrder_yesterday_price(String order_yesterday_price) {
		this.order_yesterday_price = order_yesterday_price;
	}

	public String getOrder_all_size() {
		return order_all_size;
	}

	public void setOrder_all_size(String order_all_size) {
		this.order_all_size = order_all_size;
	}

	public String getOrder_all_night() {
		return order_all_night;
	}

	public void setOrder_all_night(String order_all_night) {
		this.order_all_night = order_all_night;
	}

	public String getOrder_all_price() {
		return order_all_price;
	}

	public void setOrder_all_price(String order_all_price) {
		this.order_all_price = order_all_price;
	}

	public String getHotel_all_IsActive() {
		return hotel_all_IsActive;
	}

	public void setHotel_all_IsActive(String hotel_all_IsActive) {
		this.hotel_all_IsActive = hotel_all_IsActive;
	}

	public String getHotel_all_NotIsActive() {
		return hotel_all_NotIsActive;
	}

	public void setHotel_all_NotIsActive(String hotel_all_NotIsActive) {
		this.hotel_all_NotIsActive = hotel_all_NotIsActive;
	}

	public String getHotel_all_addInThisMonth() {
		return hotel_all_addInThisMonth;
	}

	public void setHotel_all_addInThisMonth(String hotel_all_addInThisMonth) {
		this.hotel_all_addInThisMonth = hotel_all_addInThisMonth;
	}

	public Date getTodayTime() {
		return todayTime;
	}

	public void setTodayTime(Date todayTime) {
		this.todayTime = todayTime;
	}
}
