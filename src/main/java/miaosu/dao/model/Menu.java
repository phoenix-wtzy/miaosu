package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2021/1/20 10:41
 * @Description:
 */
public class Menu {

	//主键递增
	private Long menu_id;
	//菜单名称
	private String menu_name;
	//菜单的访问路径(url)
	private String menu_url;
	//父菜单ID
	private String menu_parentID;
	//菜单级别
	private String level;


	public Long getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(Long menu_id) {
		this.menu_id = menu_id;
	}



	public String getMenu_name() {
		return menu_name;
	}

	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}

	public String getMenu_url() {
		return menu_url;
	}

	public void setMenu_url(String menu_url) {
		this.menu_url = menu_url;
	}

	public String getMenu_parentID() {
		return menu_parentID;
	}

	public void setMenu_parentID(String menu_parentID) {
		this.menu_parentID = menu_parentID;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
