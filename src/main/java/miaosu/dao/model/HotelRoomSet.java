package miaosu.dao.model;

public class HotelRoomSet {
    /**
     * 递增主键
     */
    private Long hotelRoomTypeId;

    /**
     * 房型自定义名称
     */
    private String hotelRoomTypeName;

    /**
     * 房型ID
     */
    private Integer roomTypeId;

    /**
     * 所属酒店id
     */
    private Long hotelId;

    public Long getHotelRoomTypeId() {
        return hotelRoomTypeId;
    }

    public void setHotelRoomTypeId(Long hotelRoomTypeId) {
        this.hotelRoomTypeId = hotelRoomTypeId;
    }

    public String getHotelRoomTypeName() {
        return hotelRoomTypeName;
    }

    public void setHotelRoomTypeName(String hotelRoomTypeName) {
        this.hotelRoomTypeName = hotelRoomTypeName == null ? null : hotelRoomTypeName.trim();
    }

    public Integer getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(Integer roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }
}