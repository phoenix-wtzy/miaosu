package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2020/7/20 15:31
 * @Description: 成员列表
 */
public class Member extends BaseEntity{
	//成员id
	private Long user_id;
	//成员名字
	private String user_nick;
	//手机号
	private String user_phone;
	//公司
	private String business;
	//审核状态
	private String code_level;

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getUser_nick() {
		return user_nick;
	}

	public void setUser_nick(String user_nick) {
		this.user_nick = user_nick;
	}

	public String getUser_phone() {
		return user_phone;
	}

	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getCode_level() {
		return code_level;
	}

	public void setCode_level(String code_level) {
		this.code_level = code_level;
	}
}
