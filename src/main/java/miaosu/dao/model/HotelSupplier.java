package miaosu.dao.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @author Administrator
 * @Date: 2021/2/3 11:37
 * @Description: 酒店关联供应商
 */
public class HotelSupplier extends BaseEntity11{

	private Long hotel_supplier_id;//主键自增
	private Long hotel_id;//酒店ID
	private Long business_id;//供应商ID
	private String business_name;//供应商名称
	private String hotel_name;//酒店名称
	private int hotelType;//销售类型:1--A类置换;2--B类分销
	private int commission_rate;//佣金比例
	private int settle_type;//置换类型;0--按照金额;1--按间夜
	private String settle_totalMoney;//协议置换总金额
	private String settle_totalNight;//协议置换间夜数
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date create_time;//创建时间:酒店和供应商关联的时间
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date start_time;//开始时间:点上线按钮
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date end_time;//结束时间:点下线按钮
	private int hotel_supplier_state;//状态:0--待上线;1--在线;2--下线 默认是0
	private String remark;//备注
	private String ext;//扩展项

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public int getSettle_type() {
		return settle_type;
	}

	public void setSettle_type(int settle_type) {
		this.settle_type = settle_type;
	}

	public Long getHotel_supplier_id() {
		return hotel_supplier_id;
	}

	public void setHotel_supplier_id(Long hotel_supplier_id) {
		this.hotel_supplier_id = hotel_supplier_id;
	}

	public Long getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(Long hotel_id) {
		this.hotel_id = hotel_id;
	}

	public Long getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(Long business_id) {
		this.business_id = business_id;
	}

	public int getHotelType() {
		return hotelType;
	}

	public void setHotelType(int hotelType) {
		this.hotelType = hotelType;
	}

	public int getCommission_rate() {
		return commission_rate;
	}

	public void setCommission_rate(int commission_rate) {
		this.commission_rate = commission_rate;
	}

	public String getSettle_totalMoney() {
		return settle_totalMoney;
	}

	public void setSettle_totalMoney(String settle_totalMoney) {
		this.settle_totalMoney = settle_totalMoney;
	}

	public String getSettle_totalNight() {
		return settle_totalNight;
	}

	public void setSettle_totalNight(String settle_totalNight) {
		this.settle_totalNight = settle_totalNight;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getStart_time() {
		return start_time;
	}

	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	public int getHotel_supplier_state() {
		return hotel_supplier_state;
	}

	public void setHotel_supplier_state(int hotel_supplier_state) {
		this.hotel_supplier_state = hotel_supplier_state;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	@Override
	public String toString() {
		return "Hotel_supplier{" +
				"hotel_supplier_id=" + hotel_supplier_id +
				", hotel_id=" + hotel_id +
				", business_id=" + business_id +
				", hotelType=" + hotelType +
				", commission_rate=" + commission_rate +
				", settle_totalMoney='" + settle_totalMoney + '\'' +
				", settle_totalNight='" + settle_totalNight + '\'' +
				", create_time=" + create_time +
				", start_time=" + start_time +
				", end_time=" + end_time +
				", hotel_supplier_state=" + hotel_supplier_state +
				", remark='" + remark + '\'' +
				", ext='" + ext + '\'' +
				'}';
	}
}
