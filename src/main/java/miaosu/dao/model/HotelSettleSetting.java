package miaosu.dao.model;

import java.util.Date;

public class HotelSettleSetting {
    private Long id;

    private Long hotelId;

    private Integer bankUseType;

    private String bankAccountType;

    private String bankAccountName;

    private String bankAccountCardnumber;

    private String bankSignPhone;

    private String settleMsgPhone;

    private Integer commissionRate;

    private Integer settleCycleSetting;

    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Integer getBankUseType() {
        return bankUseType;
    }

    public void setBankUseType(Integer bankUseType) {
        this.bankUseType = bankUseType;
    }

    public String getBankAccountType() {
        return bankAccountType;
    }

    public void setBankAccountType(String bankAccountType) {
        this.bankAccountType = bankAccountType == null ? null : bankAccountType.trim();
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName == null ? null : bankAccountName.trim();
    }

    public String getBankAccountCardnumber() {
        return bankAccountCardnumber;
    }

    public void setBankAccountCardnumber(String bankAccountCardnumber) {
        this.bankAccountCardnumber = bankAccountCardnumber == null ? null : bankAccountCardnumber.trim();
    }

    public String getBankSignPhone() {
        return bankSignPhone;
    }

    public void setBankSignPhone(String bankSignPhone) {
        this.bankSignPhone = bankSignPhone == null ? null : bankSignPhone.trim();
    }

    public String getSettleMsgPhone() {
        return settleMsgPhone;
    }

    public void setSettleMsgPhone(String settleMsgPhone) {
        this.settleMsgPhone = settleMsgPhone == null ? null : settleMsgPhone.trim();
    }

    public Integer getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Integer commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Integer getSettleCycleSetting() {
        return settleCycleSetting;
    }

    public void setSettleCycleSetting(Integer settleCycleSetting) {
        this.settleCycleSetting = settleCycleSetting;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}