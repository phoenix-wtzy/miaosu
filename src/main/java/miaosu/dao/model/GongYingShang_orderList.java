package miaosu.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @Date: 2020/8/11 16:01
 * @Description:
 */
public class GongYingShang_orderList implements Serializable {
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date bookTime; //预定时间
	private String hotel_name;//酒店名字
	private Long p_hotel_id;//酒店id
	private String orderContent; //预定信息详
	private BigDecimal price; //价格

	public Date getBookTime() {
		return bookTime;
	}

	public void setBookTime(Date bookTime) {
		this.bookTime = bookTime;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public Long getP_hotel_id() {
		return p_hotel_id;
	}

	public void setP_hotel_id(Long p_hotel_id) {
		this.p_hotel_id = p_hotel_id;
	}

	public String getOrderContent() {
		return orderContent;
	}

	public void setOrderContent(String orderContent) {
		this.orderContent = orderContent;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
