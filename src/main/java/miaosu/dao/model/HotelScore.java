package miaosu.dao.model;

import java.io.Serializable;

/**
 * 分值表
 */
public class HotelScore implements Serializable {
	//主键id
	private Long id;
	//酒店ID
	private Long hotel_id;
	//项目名称
	private String project_name;
	//分值
	private Integer score;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(Long hotel_id) {
		this.hotel_id = hotel_id;
	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "HotelScore{" +
				"id=" + id +
				", hotel_id=" + hotel_id +
				", project_name='" + project_name + '\'' +
				", score=" + score +
				'}';
	}
}
