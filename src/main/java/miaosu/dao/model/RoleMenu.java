package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2021/1/20 10:45
 * @Description: 角色菜单关联
 */
public class RoleMenu {

	//角色id
	private Long role_id;

	//菜单id
	private Long menu_id;

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public Long getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(Long menu_id) {
		this.menu_id = menu_id;
	}
}
