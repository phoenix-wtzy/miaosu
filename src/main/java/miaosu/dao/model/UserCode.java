package miaosu.dao.model;

/**
 * @author Administrator
 * @Date: 2020/7/8 11:42
 * @Description: 用户邀请码关系
 */
public class UserCode {
    //用户id
    private Long user_id;
    //邀请码id
    private Long code_id;
    //上家的邀请码
    private String code_shangjia;
    //自己的邀请码
    private String code_my;

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getCode_id() {
        return code_id;
    }

    public void setCode_id(Long code_id) {
        this.code_id = code_id;
    }

    public String getCode_shangjia() {
        return code_shangjia;
    }

    public void setCode_shangjia(String code_shangjia) {
        this.code_shangjia = code_shangjia;
    }

    public String getCode_my() {
        return code_my;
    }

    public void setCode_my(String code_my) {
        this.code_my = code_my;
    }
}
