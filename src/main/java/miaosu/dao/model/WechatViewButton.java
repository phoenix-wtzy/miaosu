package miaosu.dao.model;

public class WechatViewButton extends WechatAbstractButton{
    private String type ="view";
    private String url;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public WechatViewButton(String name, String url) {
        super(name);
        this.type = type;
        this.url = url;
    }
}
