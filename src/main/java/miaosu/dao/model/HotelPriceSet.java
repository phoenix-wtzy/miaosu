package miaosu.dao.model;

import java.math.BigDecimal;

public class HotelPriceSet {
    /**
     * 递增主键
     */
    private Long hotelPriceId;

    /**
     * 价格自定义名称
     */
    private String hotelPriceName;

    /**
     * 所属酒店id
     */
    private Long hotelId;

    /**
     * 早餐份数
     */
    private Integer breakfastNum;

    /**
     * 退订类型
     1 不可变更，不可取消
     2 提前1天以上取消免费，否则扣全额房费
     */
    private Integer unbookType;

    private BigDecimal caigou_one;

    public BigDecimal getCaigou_one() {
        return caigou_one;
    }

    public void setCaigou_one(BigDecimal caigou_one) {
        this.caigou_one = caigou_one;
    }

    public Long getHotelPriceId() {
        return hotelPriceId;
    }

    public void setHotelPriceId(Long hotelPriceId) {
        this.hotelPriceId = hotelPriceId;
    }

    public String getHotelPriceName() {
        return hotelPriceName;
    }

    public void setHotelPriceName(String hotelPriceName) {
        this.hotelPriceName = hotelPriceName == null ? null : hotelPriceName.trim();
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Integer getBreakfastNum() {
        return breakfastNum;
    }

    public void setBreakfastNum(Integer breakfastNum) {
        this.breakfastNum = breakfastNum;
    }

    public Integer getUnbookType() {
        return unbookType;
    }

    public void setUnbookType(Integer unbookType) {
        this.unbookType = unbookType;
    }
}