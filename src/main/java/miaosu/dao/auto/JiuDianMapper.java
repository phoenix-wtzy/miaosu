package miaosu.dao.auto;

import miaosu.dao.model.JiuDian;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/8/22 10:32
 * @Description:
 */
public interface JiuDianMapper {
	//添加到jiudian结算表中
	void insert(JiuDian list);

	//添加之前先判断是否存在,存在就不做处理
	JiuDian queryAll(@Param("settle_startAndEnd") String settle_startAndEnd, @Param("hotel_id") Long hotel_id);

	//更新结算信息
	void update(@Param("settle_startAndEnd") String settle_startAndEnd, @Param("hotelId") Long hotelId, @Param("settle_money1") String settle_money1, @Param("commission_money1") String commission_money1, @Param("settle_status") String settle_status);

	//不用遍历日期,直接根据hotelId获取静态表
	List<JiuDian> queryAllJiuDian(Long hotelId);
}
