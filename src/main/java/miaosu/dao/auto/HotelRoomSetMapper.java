package miaosu.dao.auto;

import miaosu.dao.model.HotelRoomSet;
import miaosu.dao.model.HotelRoomSetExample;
import miaosu.svc.vo.HotelRoomSetVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HotelRoomSetMapper {
    long countByExample(HotelRoomSetExample example);

    int deleteByExample(HotelRoomSetExample example);

    int deleteByPrimaryKey(Long hotelRoomTypeId);

    int insert(HotelRoomSet record);

    int insertSelective(HotelRoomSet record);

    List<HotelRoomSet> selectByExample(HotelRoomSetExample example);

    HotelRoomSet selectByPrimaryKey(Long hotelRoomTypeId);

    int updateByExampleSelective(@Param("record") HotelRoomSet record, @Param("example") HotelRoomSetExample example);

    int updateByExample(@Param("record") HotelRoomSet record, @Param("example") HotelRoomSetExample example);

    int updateByPrimaryKeySelective(HotelRoomSet record);

    int updateByPrimaryKey(HotelRoomSet record);

    HotelRoomSet selectByHotelRoomTypeName(@Param("hotelRoomTypeName")String hotelRoomTypeName,@Param("hotelId")Long hotelId);

    /**
     * 查询完整的房型信息包含房间数量
     * @param param 参数信息
     * @return
     */
    List<HotelRoomSetVO> selectRoomSetAndCount(Map<String, String> param);

    //根据房间类型hotelRoomTypeId,查房间类型名称hotel_room_type_name,查hotel_room_set表
    String queryHotelRoomTypeName(@Param("hotelRoomTypeId")Long hotelRoomTypeId);

    //根据酒店id和平台酒店房型名字到hotel_room_set表中去查询房型hotel_room_type_id
    Long queryHotelRoomTypeIdByP_hotel_room_type_name(@Param("p_hotel_id") Long p_hotel_id, @Param("p_hotel_room_type_name") String p_hotel_room_type_name);

}