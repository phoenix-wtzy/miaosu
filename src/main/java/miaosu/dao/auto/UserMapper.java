package miaosu.dao.auto;

import miaosu.dao.model.Member;
import miaosu.dao.model.User;
import miaosu.dao.model.UserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    long countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Long userId);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Long userId);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //查询用户电话,查user表
    String queryUser_phone(@Param("userId")Long userId);


    //注册之前判断手机号(账号)是否存在,然后返回信息给前端(根据手机号user_phone查账号user_name)
    String queryUser_name(@Param("userPhone")String userPhone);

    //插入不存在的账号
    int insert1(User record);

    //重置密码
	void resetPassword(@Param("password")String password, @Param("username")String username);

    //根据用户id查询用户名user_nick
    String queryUser_nick(@Param("currentUserId")Long currentUserId);

    //根据当前用户id查询密码
	String queryUser_password(@Param("currentUserId") Long currentUserId);


    //修改密码
    void updateUser_password(@Param("currentUserId") Long currentUserId, @Param("password") String password);

    //更新openid
    void updateUserWxOpenid(@Param("currentUserId") Long currentUserId, @Param("wxOpenid") String wxOpenid,@Param("roles") String roles);

    //根据当前登录的用户id查询自己的邀请码,再根据邀请码查询成员的邀请码,再根据成员的邀请码查询成员id,再查询成员id对应的成员名字和成员电话
    List<Member> queryMember(Long currentUserId);

    //根据用户id查询其账号user_name
	String queryUser_nameByUser_id(Long currentUserId);

	//查询酒店对应的用户信息
    //HoteInfoVO queryHotelInfoById(@Param("hotelId")Long hotelId);
}