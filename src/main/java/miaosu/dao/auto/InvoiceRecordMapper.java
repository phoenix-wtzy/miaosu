package miaosu.dao.auto;

import miaosu.dao.model.InvoiceRecord;
import miaosu.dao.model.InvoiceRecordExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InvoiceRecordMapper {
    long countByExample(InvoiceRecordExample example);

    int deleteByExample(InvoiceRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(InvoiceRecord record);

    int insertSelective(InvoiceRecord record);

    List<InvoiceRecord> selectByExample(InvoiceRecordExample example);

    InvoiceRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") InvoiceRecord record, @Param("example") InvoiceRecordExample example);

    int updateByExample(@Param("record") InvoiceRecord record, @Param("example") InvoiceRecordExample example);

    int updateByPrimaryKeySelective(InvoiceRecord record);

    int updateByPrimaryKey(InvoiceRecord record);
}