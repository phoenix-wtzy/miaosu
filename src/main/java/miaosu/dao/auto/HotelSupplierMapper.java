package miaosu.dao.auto;

import miaosu.dao.model.HotelSupplier;
import miaosu.dao.model.HotelSupplierExample;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/2/3 11:56
 * @Description:
 */
public interface HotelSupplierMapper {

	//查询该酒店对应的所有关联供应商的信息
	List<HotelSupplier> querySupplierListByHotelId(@Param("hotel_id") Long hotel_id, @Param("page") int page, @Param("limit") int limit);

	//添加酒店关联供应商信息
	Long insert(HotelSupplier hotelSupplier);

	//查询与本酒店有关的hotel_supplier中总条数
	int queryTotalSupplierList(Long hotel_id);

	//修改
	int update(HotelSupplier hotelSupplier);

	//根据封装条件查询hotel_supplier表中是否存在当前项
	HotelSupplier queryHotelSupplier(HotelSupplierExample hotelSupplierExample);

	//根据主键id删除
	int deleteByHotel_supplier_id(Long hotel_supplier_id);

	//上线
	int updateHotelSupplierStateUP(@Param("hotel_supplier_id") Long hotel_supplier_id, @Param("hotel_supplier_state") int hotel_supplier_state, @Param("start_time") Date start_time);

	//下线
	int updateHotelSupplierStateDown(@Param("hotel_supplier_id") Long hotel_supplier_id, @Param("hotel_supplier_state") int hotel_supplier_state, @Param("end_time") Date end_time);

	//根据供应商id查询名下关联酒店集合列表
	List<HotelSupplier> querySupplierListByBusiness_id(@Param("business_id") Long business_id, @Param("page") int page, @Param("limit") int limit);

	//查询与本供应商有关的hotel_supplier中酒店总条数
	int queryTotalSupplierListByBusiness_id(Long business_id);

	//上线之前先查询有没有已经在线的,酒店关联供应商在同一时间内有且只能有一个关联
	int queryHotelSupplierState(Long hotel_id);
}
