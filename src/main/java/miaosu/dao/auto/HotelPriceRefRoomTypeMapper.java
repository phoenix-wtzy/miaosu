package miaosu.dao.auto;

import miaosu.dao.model.HotelPriceRefRoomType;
import miaosu.dao.model.HotelPriceRefRoomTypeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HotelPriceRefRoomTypeMapper {
    long countByExample(HotelPriceRefRoomTypeExample example);

    int deleteByExample(HotelPriceRefRoomTypeExample example);

    int deleteByPrimaryKey(Long productId);

    int insert(HotelPriceRefRoomType record);

    int insertSelective(HotelPriceRefRoomType record);

    List<HotelPriceRefRoomType> selectByExample(HotelPriceRefRoomTypeExample example);

    HotelPriceRefRoomType selectByPrimaryKey(Long productId);

    int updateByExampleSelective(@Param("record") HotelPriceRefRoomType record, @Param("example") HotelPriceRefRoomTypeExample example);

    int updateByExample(@Param("record") HotelPriceRefRoomType record, @Param("example") HotelPriceRefRoomTypeExample example);

    int updateByPrimaryKeySelective(HotelPriceRefRoomType record);

    int updateByPrimaryKey(HotelPriceRefRoomType record);

    //根据产品productId,查询房间类型hotelRoomTypeId,查hotel_price_ref_room_type表
    Long queryHotelRoomTypeIdByProductId(@Param("productId")Long productId);

    //1.先根据hotel_room_type_id到hotel_price_ref_room_type表中查询hotel_price_id,多个
    List<Long> queryhotel_price_id(Long hotelRoomTypeId);


    //根据hotelPriceId到hotel_price_ref_room_type表中查询productId
    Long queryProductIdByHotelPriceId(@Param("hotelPriceId") Long hotelPriceId);

}