package miaosu.dao.auto;

import miaosu.dao.model.GongYingShang;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/8/24 14:27
 * @Description:
 */
public interface GongYingShangMapper {
	//添加到gongyingshang结算表中
	void insert(GongYingShang gongYingShang);

	//根据周期和酒店id查询供应商信息(添加之前先判断是否存在,存在就不做处理)
	GongYingShang queryAll(@Param("settle_startAndEnd") String settle_startAndEnd, @Param("business_id") Long business_id);

	//更新结算信息
	void update(@Param("settle_startAndEnd") String settle_startAndEnd, @Param("business_id") Long business_id, @Param("settle_money1") String settle_money1, @Param("commission_money1") String commission_money1, @Param("settle_status") String settle_status);

	//不用遍历日期,直接根据business_id获取静态表
	List<GongYingShang> queryAllGongYingShang(Long business_id);
}
