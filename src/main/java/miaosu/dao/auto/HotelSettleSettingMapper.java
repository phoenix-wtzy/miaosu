package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.HotelSettleSetting;
import miaosu.dao.model.HotelSettleSettingExample;
import org.apache.ibatis.annotations.Param;

public interface HotelSettleSettingMapper {
    long countByExample(HotelSettleSettingExample example);

    int deleteByExample(HotelSettleSettingExample example);

    int deleteByPrimaryKey(Long id);

    int insert(HotelSettleSetting record);

    int insertSelective(HotelSettleSetting record);

    List<HotelSettleSetting> selectByExample(HotelSettleSettingExample example);

    HotelSettleSetting selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") HotelSettleSetting record, @Param("example") HotelSettleSettingExample example);

    int updateByExample(@Param("record") HotelSettleSetting record, @Param("example") HotelSettleSettingExample example);

    int updateByPrimaryKeySelective(HotelSettleSetting record);

    int updateByPrimaryKey(HotelSettleSetting record);
}