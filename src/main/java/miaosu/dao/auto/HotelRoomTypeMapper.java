package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.HotelRoomType;
import miaosu.dao.model.HotelRoomTypeExample;
import org.apache.ibatis.annotations.Param;

public interface HotelRoomTypeMapper {
    long countByExample(HotelRoomTypeExample example);

    int deleteByExample(HotelRoomTypeExample example);

    int deleteByPrimaryKey(Integer roomTypeId);

    int insert(HotelRoomType record);

    int insertSelective(HotelRoomType record);

    List<HotelRoomType> selectByExample(HotelRoomTypeExample example);

    HotelRoomType selectByPrimaryKey(Integer roomTypeId);

    int updateByExampleSelective(@Param("record") HotelRoomType record, @Param("example") HotelRoomTypeExample example);

    int updateByExample(@Param("record") HotelRoomType record, @Param("example") HotelRoomTypeExample example);

    int updateByPrimaryKeySelective(HotelRoomType record);

    int updateByPrimaryKey(HotelRoomType record);
}