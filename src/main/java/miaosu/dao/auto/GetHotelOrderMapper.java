package miaosu.dao.auto;

import miaosu.dao.model.HotelOrder;
import miaosu.dao.model.HotelOrderExample;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 获取爬虫数据对应的mapper
 */
public interface GetHotelOrderMapper {
    long countByExample(HotelOrderExample example);

    int deleteByExample(HotelOrderExample example);

    int deleteByPrimaryKey(Long orderId);

    int insert(HotelOrder record);

    int insertSelective(HotelOrder record);

    List<HotelOrder> selectByExample(HotelOrderExample example);

    HotelOrder selectByPrimaryKey(Long orderId);

    int updateByExampleSelective(@Param("record") HotelOrder record, @Param("example") HotelOrderExample example);


    int updateByExample(@Param("record") HotelOrder record, @Param("example") HotelOrderExample example);

    int updateByPrimaryKeySelective(HotelOrder record);

    /**
     *根据订单号查询订单
     * @param record
     * @return
     */
    int updateByOrderNo(HotelOrder record);

    int updateByPrimaryKey(HotelOrder record);
    HotelOrder selectByorderNo(@Param("orderNo") String orderNo);


    /**
     * 查询确认订单以及取消订单信息
     * @param param
     * @return
     */
    List<HotelOrder> selectWaitConfirmOrder(Map<String, Object> param);



    /**
     * 查询当日取消订单总记录数
     * @param param
     * @return
     */
    Long  selectCancelOrderTodayCount(Map<String, Object> param);

    /**
     * //查询待确认的总记录数
     * @param param
     * @return
     */
    Long  selectConfirmOrderCount(Map<String, Object> param);

    //根据订单id添加/修改酒店订单确认号(在hotel_getorder中)
	Long updateHotel_confirm_number(@Param("order_id") Long order_id, @Param("hotel_confirm_number") String hotel_confirm_number);

    //根据订单id修改订单信息,在hotel_getorder表中
    int updateOrder(HotelOrder order);

    //根据订单id修改采购总价
    void updateGetOrderCaigou_total(@Param("orderId") long orderId, @Param("caigou_total") BigDecimal caigou_total);
}