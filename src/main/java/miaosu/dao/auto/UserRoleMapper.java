package miaosu.dao.auto;

import miaosu.dao.model.UserRole;
import miaosu.dao.model.UserRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserRoleMapper {
    long countByExample(UserRoleExample example);

    int deleteByExample(UserRoleExample example);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    List<UserRole> selectByExample(UserRoleExample example);

    int updateByExampleSelective(@Param("record") UserRole record, @Param("example") UserRoleExample example);

    int updateByExample(@Param("record") UserRole record, @Param("example") UserRoleExample example);


    //根据当前用户id查询对应的角色,二级可以看信息;;三级提示审核中,审核通过才能看,并且改为二级用户
    List<Long> queryUser_Role(Long currentUserId);

    //点击审核通过的确定,将用户从三级升级为二级,根据用户id去操作
	void updateUser_role(Long user_id);

    //根据之前的用户id和角色id修改角色id
    void updateByUser_id(@Param("aLong")Long aLong, @Param("user_id")Long user_id, @Param("aLong1")Long aLong1);
}