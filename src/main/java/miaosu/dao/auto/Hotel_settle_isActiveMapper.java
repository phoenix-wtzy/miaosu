package miaosu.dao.auto;

import miaosu.dao.model.Hotel_settle_isActive;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/12/15 16:26
 * @Description:
 */
public interface Hotel_settle_isActiveMapper {
	//添加到hotel_settle_isActive表中
	void insert(Hotel_settle_isActive hotel_settle_isActive);

	//查询表hotel_settle_isactive前天所有数据
	List<Hotel_settle_isActive> queryAll();

	//下线酒店添加到hotel_settle_isnotactive表中
	void insertInhotel_settle_isnotactive(Hotel_settle_isActive hotel_settle_isActive);

	//查询下线酒店表hotel_settle_isnotactive前天所有数据
	List<Hotel_settle_isActive> queryAllInhotel_settle_isnotactive();
}
