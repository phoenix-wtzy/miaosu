package miaosu.dao.auto;

import miaosu.dao.model.Charts;

/**
 * @author Administrator
 * @Date: 2021/1/4 9:47
 * @Description:
 */
public interface ChartsMapper {
	//将首页charts数据添加到表charts中
	Long insert(Charts charts);

	//查询charts表中数据(今天之前的)
	Charts query();
}
