package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.HotelOrderNotify;
import miaosu.dao.model.HotelOrderNotifyExample;
import org.apache.ibatis.annotations.Param;

public interface HotelOrderNotifyMapper {
    long countByExample(HotelOrderNotifyExample example);

    int deleteByExample(HotelOrderNotifyExample example);

    int deleteByPrimaryKey(Long orderId);

    int insert(HotelOrderNotify record);

    int insertSelective(HotelOrderNotify record);

    List<HotelOrderNotify> selectByExample(HotelOrderNotifyExample example);

    HotelOrderNotify selectByPrimaryKey(Long orderId);

    int updateByExampleSelective(@Param("record") HotelOrderNotify record, @Param("example") HotelOrderNotifyExample example);

    int updateByExample(@Param("record") HotelOrderNotify record, @Param("example") HotelOrderNotifyExample example);

    int updateByPrimaryKeySelective(HotelOrderNotify record);

    int updateByPrimaryKey(HotelOrderNotify record);
}