package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.SettleRefOrder;
import miaosu.dao.model.SettleRefOrderExample;
import org.apache.ibatis.annotations.Param;

public interface SettleRefOrderMapper {
    long countByExample(SettleRefOrderExample example);

    int deleteByExample(SettleRefOrderExample example);

    int insert(SettleRefOrder record);

    int insertSelective(SettleRefOrder record);

    List<SettleRefOrder> selectByExample(SettleRefOrderExample example);

    int updateByExampleSelective(@Param("record") SettleRefOrder record, @Param("example") SettleRefOrderExample example);

    int updateByExample(@Param("record") SettleRefOrder record, @Param("example") SettleRefOrderExample example);
}