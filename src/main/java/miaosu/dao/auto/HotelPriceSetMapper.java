package miaosu.dao.auto;

import miaosu.dao.model.HotelPriceSet;
import miaosu.dao.model.HotelPriceSetExample;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface HotelPriceSetMapper {
    long countByExample(HotelPriceSetExample example);

    int deleteByExample(HotelPriceSetExample example);

    int deleteByPrimaryKey(Long hotelPriceId);

    int insert(HotelPriceSet record);

    int insertSelective(HotelPriceSet record);

    List<HotelPriceSet> selectByExample(HotelPriceSetExample example);

    HotelPriceSet selectByPrimaryKey(Long hotelPriceId);

    int updateByExampleSelective(@Param("record") HotelPriceSet record, @Param("example") HotelPriceSetExample example);

    int updateByExample(@Param("record") HotelPriceSet record, @Param("example") HotelPriceSetExample example);

    int updateByPrimaryKeySelective(HotelPriceSet record);

    int updateByPrimaryKey(HotelPriceSet record);


    /**
     * 根据房型id查询该房型对应的所有计划价格
     * @param hotelRoomTypeId
     * @return
     */
    List<HotelPriceSet> selectPriceSetByRoomTypeId(@Param("hotelRoomTypeId") Long hotelRoomTypeId);

    //2.在根据hotel_price_id和hotel_id和breakfast_num的拼接查询caigou_one
    BigDecimal querycaigou_one(@Param("hotel_price_id") Long hotel_price_id, @Param("p_hotel_id") Long p_hotel_id, @Param("breakfast") Integer breakfast);
}