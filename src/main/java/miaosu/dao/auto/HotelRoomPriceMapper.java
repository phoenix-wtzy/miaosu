package miaosu.dao.auto;

import miaosu.dao.model.HotelRoomPrice;
import miaosu.dao.model.HotelRoomPriceExample;
import miaosu.svc.vo.qhh.HotelDailySafe;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface HotelRoomPriceMapper {
    long countByExample(HotelRoomPriceExample example);

    int deleteByExample(HotelRoomPriceExample example);

    int insert(HotelRoomPrice record);

    int insertSelective(HotelRoomPrice record);

    List<HotelRoomPrice> selectByExample(HotelRoomPriceExample example);

    int updateByExampleSelective(@Param("record") HotelRoomPrice record, @Param("example") HotelRoomPriceExample example);

    int updateByExample(@Param("record") HotelRoomPrice record, @Param("example") HotelRoomPriceExample example);

    /**
     * 根据产品表关联查询对应的价格信息
     * @param param  hotelPriceId hotelRoomTypeId startDate endDate
     * @return  HotelRoomPrice 价格信息
     */
    List<HotelRoomPrice> selectByExampleByProduct(Map<String,Object> param);

    /**
     * 根据产品id关联查询房态和房价信息
     * @param param
     * @return
     */
    List<HotelDailySafe> selectPriceAndStatusByProductId(Map<String,Object> param);

    //根据产品id和日期去查询对应时间的价格
    BigDecimal queryPrice(@Param("productId") Long productId, @Param("d1") Date d1);

}