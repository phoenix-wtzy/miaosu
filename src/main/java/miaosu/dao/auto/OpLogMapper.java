package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.OpLog;
import miaosu.dao.model.OpLogExample;
import org.apache.ibatis.annotations.Param;

public interface OpLogMapper {
    long countByExample(OpLogExample example);

    int deleteByExample(OpLogExample example);

    int deleteByPrimaryKey(Long logId);

    int insert(OpLog record);

    int insertSelective(OpLog record);

    List<OpLog> selectByExample(OpLogExample example);

    OpLog selectByPrimaryKey(Long logId);

    int updateByExampleSelective(@Param("record") OpLog record, @Param("example") OpLogExample example);

    int updateByExample(@Param("record") OpLog record, @Param("example") OpLogExample example);

    int updateByPrimaryKeySelective(OpLog record);

    int updateByPrimaryKey(OpLog record);
}