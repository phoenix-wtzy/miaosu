package miaosu.dao.auto;

import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelInfoExample;
import miaosu.svc.vo.HoteInfoVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HotelInfoMapper {
    long countByExample(HotelInfoExample example);

    int deleteByExample(HotelInfoExample example);

    int deleteByPrimaryKey(Long hotelId);

    int insert(HotelInfo record);

    int insertSelective(HotelInfo record);

    List<HotelInfo> selectByExample(HotelInfoExample example);

    List<HotelInfo> selectByUserAndName(@Param("userId") Long userId,@Param("hotelName") String hotelName,@Param("hotelId") Long hotelId);

    HotelInfo selectByPrimaryKey(Long hotelId);

    int updateByExampleSelective(@Param("record") HotelInfo record, @Param("example") HotelInfoExample example);

    int updateByExample(@Param("record") HotelInfo record, @Param("example") HotelInfoExample example);

    int updateByPrimaryKeySelective(HotelInfo record);

    int updateByPrimaryKey(HotelInfo record);

    HotelInfo selectIdByHotalName(@Param("hotelName") String hotelName);

    //查询酒店对应的用户信息
    HoteInfoVO queryHotelInfoById(@Param("hotelId")Long hotelId);

    //根据酒店id查询平台id
	String queryHotelOnlyId(Long hotelId);

    //根据当前登录用户id查询酒店id
	Long queryHotelIdByUserId(Long currentUserId);

    //根据当前酒店id和酒店是否有效去查询对应isActive属性的酒店,isActive是1,选择的就显示上架酒店,是0就显示下架酒店
    List<HotelInfo> selectByUserAndName2(@Param("userId") Long userId,@Param("hotelName") String hotelName,@Param("hotelId") Long hotelId, @Param("isActive")int isActive);

    //如果是admin或者供应商页面,就根据isActive属性去选择对应属性的酒店之一去显示
    HotelInfo selectByPrimaryKeyAndisActive(int isActive);

    //查询所有酒店信息
    List<HotelInfo> queryAllHotelInfo();

    //操作酒店上下线,表hotel_info中的isActive字段
	void updateHotel_isActive(@Param("hotelId") Long hotelId, @Param("isActive1") int isActive1);
}