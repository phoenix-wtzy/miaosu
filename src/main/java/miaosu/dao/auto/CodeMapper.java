package miaosu.dao.auto;

import org.apache.ibatis.annotations.Param;

/**
 * @author Administrator
 * @Date: 2020/7/6 14:04
 * @Description:
 */
public interface CodeMapper {

    //验证上家的邀请码是否存在以及正确性
    String queryIfExistCode(@Param("code_shangjia") String code_shangjia);

    //将自己的邀请码存储到code表中
    void insertCode(@Param("code_my")String code_my, @Param("code_level") String code_level);

    //根据邀请码查询自己的邀请码id
    Long queryCodeId(@Param("code_my")String code_my);
}
