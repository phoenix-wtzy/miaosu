package miaosu.dao.auto;

import miaosu.dao.model.HotelRefUser;
import miaosu.dao.model.HotelRefUserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HotelRefUserMapper {
    long countByExample(HotelRefUserExample example);

    int deleteByExample(HotelRefUserExample example);

    int insert(HotelRefUser record);

    int insertSelective(HotelRefUser record);

    List<HotelRefUser> selectByExample(HotelRefUserExample example);

    List<Map<String, Object>> selectWxOpenidByHotelid(Long hotelid);

    int updateByExampleSelective(@Param("record") HotelRefUser record, @Param("example") HotelRefUserExample example);

    int updateByExample(@Param("record") HotelRefUser record, @Param("example") HotelRefUserExample example);

	//删除之前user_id和hotel_id的关联
	void deleteHotelRefUser(@Param("user_id") Long user_id, @Param("hotel_id") Long hotel_id);
}