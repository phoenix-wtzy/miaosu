package miaosu.dao.auto;

import miaosu.dao.model.HotelEvaluation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/4/12 9:37
 * @Description:
 */
public interface HotelEvaluationMapper {

	//查询当前用户名下填写的所有酒店评估信息
	List<HotelEvaluation> queryAllByUserId(@Param("currentUserId") Long currentUserId, @Param("page") int page, @Param("limit") int limit);

	//查询当前用户名下酒店信息总条数
	int queryTotalByUserId(Long currentUserId);

	//新增酒店评估到hotel_evaluation表中
	Long insert(HotelEvaluation hotelEvaluation);
}
