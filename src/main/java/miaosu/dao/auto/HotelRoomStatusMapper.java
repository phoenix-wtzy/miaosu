package miaosu.dao.auto;

import miaosu.dao.model.HotelRoomStatus;
import miaosu.dao.model.HotelRoomStatusExample;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface HotelRoomStatusMapper {
    long countByExample(HotelRoomStatusExample example);

    int deleteByExample(HotelRoomStatusExample example);

    int insert(HotelRoomStatus record);

    int insertSelective(HotelRoomStatus record);

    List<HotelRoomStatus> selectByExample(HotelRoomStatusExample example);

    int updateByExampleSelective(@Param("record") HotelRoomStatus record, @Param("example") HotelRoomStatusExample example);

    int updateByExample(@Param("record") HotelRoomStatus record, @Param("example") HotelRoomStatusExample example);

    /**
     * 根据产品表关联查询对应房态信息
     * @param param  hotelPriceId hotelRoomTypeId startDate endDate
     * @return  HotelRoomPrice 价格信息
     */
    List<HotelRoomStatus> selectByExampleByProduct(Map<String,Object> param);

    //接受订单之前先获取某天某个房型的开关状态
    Integer queryHotelStatus(@Param("p_hotel_id") Long p_hotel_id, @Param("productId") Long productId, @Param("sell_date") Date sell_date);
}