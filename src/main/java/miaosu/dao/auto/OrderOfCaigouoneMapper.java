package miaosu.dao.auto;

import miaosu.dao.model.OrderOfCaigouone;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/3/18 13:57
 * @Description:
 */
public interface OrderOfCaigouoneMapper {
	//添加置单日历
	void insert(OrderOfCaigouone orderOfCaigouone);

	//查询比订单的置单日历表,根据酒店id和caigou_one_state=0的
	List<OrderOfCaigouone> queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(@Param("order_id") Long order_id, @Param("caigou_one_state") int caigou_one_state);

	//计算每单的置换总额
	BigDecimal sumCaigoutotal(@Param("order_id") Long order_id, @Param("caigou_one_state") int caigou_one_state);

	//修改置单日历
	void update(OrderOfCaigouone orderOfCaigouone);

	//根据订单id查询所有置单日历表
	List<OrderOfCaigouone> queryOrderOfCaigouoneListByorderId(Long order_id);
}
