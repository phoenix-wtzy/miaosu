package miaosu.dao.auto;

import miaosu.dao.model.HotelRoomCount;
import miaosu.dao.model.HotelRoomCountExample;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface HotelRoomCountMapper {
    long countByExample(HotelRoomCountExample example);

    int deleteByExample(HotelRoomCountExample example);

    int insert(HotelRoomCount record);

    int insertSelective(HotelRoomCount record);

    List<HotelRoomCount> selectByExample(HotelRoomCountExample example);

    int updateByExampleSelective(@Param("record") HotelRoomCount record, @Param("example") HotelRoomCountExample example);

    int updateByExample(@Param("record") HotelRoomCount record, @Param("example") HotelRoomCountExample example);

    //查询数据库的总房量
    Integer findCountRoom(@Param("hotelRoomTypeId")Long hotelRoomTypeId, @Param("sell_date")Date sell_date);
}