package miaosu.dao.auto;

import miaosu.dao.model.OtaData;

/**
 * @author Administrator
 * @Date: 2021/4/12 15:57
 * @Description:
 */
public interface OtaDataMapper {

	//添加酒店信息到ota_data数据表中
	Long insert(OtaData otaData);
}
