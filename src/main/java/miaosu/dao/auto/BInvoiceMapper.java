package miaosu.dao.auto;

import miaosu.dao.model.BInvoice;
import miaosu.dao.model.BInvoiceExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BInvoiceMapper {
    long countByExample(BInvoiceExample example);

    int deleteByExample(BInvoiceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BInvoice record);

    int insertSelective(BInvoice record);

    List<BInvoice> selectByExample(BInvoiceExample example);

    BInvoice selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BInvoice record, @Param("example") BInvoiceExample example);

    int updateByExample(@Param("record") BInvoice record, @Param("example") BInvoiceExample example);

    int updateByPrimaryKeySelective(BInvoice record);

    int updateByPrimaryKey(BInvoice record);
}