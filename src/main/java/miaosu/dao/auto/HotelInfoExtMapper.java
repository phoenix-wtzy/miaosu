package miaosu.dao.auto;

import miaosu.dao.model.HotelInfoExt;
import miaosu.dao.model.HotelInfoExtExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HotelInfoExtMapper {
    long countByExample(HotelInfoExtExample example);

    int deleteByExample(HotelInfoExtExample example);

    int insert(HotelInfoExt record);

    int insertSelective(HotelInfoExt record);

    List<HotelInfoExt> selectByExample(HotelInfoExtExample example);

    int updateByExampleSelective(@Param("record") HotelInfoExt record, @Param("example") HotelInfoExtExample example);

    int updateByExample(@Param("record") HotelInfoExt record, @Param("example") HotelInfoExtExample example);

    int updateByPrimaryKeySelective(@Param("record")HotelInfoExt record);

}