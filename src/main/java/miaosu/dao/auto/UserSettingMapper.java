package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.UserSetting;
import miaosu.dao.model.UserSettingExample;
import org.apache.ibatis.annotations.Param;

public interface UserSettingMapper {
    long countByExample(UserSettingExample example);

    int deleteByExample(UserSettingExample example);

    int insert(UserSetting record);

    int insertSelective(UserSetting record);

    List<UserSetting> selectByExample(UserSettingExample example);

    int updateByExampleSelective(@Param("record") UserSetting record, @Param("example") UserSettingExample example);

    int updateByExample(@Param("record") UserSetting record, @Param("example") UserSettingExample example);
}