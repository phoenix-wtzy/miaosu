package miaosu.dao.auto;

import miaosu.dao.model.HotelOrder;
import miaosu.dao.model.HotelOrderExample;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface HotelOrderMapper {
    long countByExample(HotelOrderExample example);

    int deleteByExample(HotelOrderExample example);

    int deleteByPrimaryKey(Long orderId);

    int insert(HotelOrder record);

    int insertSelective(HotelOrder record);

    List<HotelOrder> selectByExample(HotelOrderExample example);

    HotelOrder selectByPrimaryKey(Long orderId);

    int updateByExampleSelective(@Param("record") HotelOrder record, @Param("example") HotelOrderExample example);


    int updateByExample(@Param("record") HotelOrder record, @Param("example") HotelOrderExample example);

    int updateByPrimaryKeySelective(HotelOrder record);

    /**
     *根据订单号查询订单
     * @param record
     * @return
     */
    int updateByOrderNo(HotelOrder record);

    int updateByPrimaryKey(HotelOrder record);
    HotelOrder selectByorderNo(@Param("orderNo") String orderNo);


    /**
     * 查询确认订单以及取消订单信息
     * @param param
     * @return
     */
    List<HotelOrder> selectWaitConfirmOrder(Map<String,Object> param);



    /**
     * 查询当日取消订单总记录数
     * @param param
     * @return
     */
    Long  selectCancelOrderTodayCount(Map<String,Object> param);

    /**
     * //查询待确认的总记录数
     * @param param
     * @return
     */
    Long  selectConfirmOrderCount(Map<String,Object> param);

    /**
     * 根据酒店id和房间id查询房型id
     * @param roomTypeId
     * @param hotel_id_selected
     * @return
     * 加@param这个注解对应#{roomTypeId}
     * 不加@Param注解对应#{param1}
     */
    Long queryHotelRoomTypeId(@Param("hotel_id_selected") Long hotel_id_selected,@Param("roomTypeId")Long roomTypeId);

    //根据平台酒店id获取酒店id
    Long queryHotelOnlyId(@Param("hotel_only_id")String hotel_only_id);

    //查询售卖产品productId,根据hotelRoomTypeId查询productId,在hotel_price_ref_room_type中
    Long queryProductId(@Param("hotelRoomTypeId")Long hotelRoomTypeId);

    //根据渠道单号channelOrderNo查询订单号orderId
    Long queryHotelId(@Param("channelOrderNo")String channelOrderNo);

    //根据订单id添加/修改酒店订单确认号(在hotel_order中)
    Long updateHotel_confirm_number(@Param("order_id") Long order_id, @Param("hotel_confirm_number") String hotel_confirm_number);

    //根据订单id查询酒店订单确认号(前端订单确认号的回显)
    String queryHotel_confirm_number2(Long order_id);

    //查询所有订单表订单数据
    List<HotelOrder> queryAllOrderInfo();

    //查询今天的所有订单
    List<HotelOrder> queryOrder_Today(@Param("date1") Date date1, @Param("date2")Date date2);

    //查询昨天的所有订单
    List<HotelOrder> queryOrder_Yesterday(@Param("date1") Date date1, @Param("date2")Date date2);

    //根据订单id修改订单信息,在hotel_order表中
    int updateOrder(HotelOrder order);

    //根据订单id修改采购总价
	void updateCaigou_total(@Param("orderId") long orderId, @Param("caigou_total") BigDecimal caigou_total);
}