package miaosu.dao.auto;

import miaosu.dao.model.Business;
import miaosu.dao.model.Picture;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/7/22 13:36
 * @Description:
 */
public interface BusinessMapper {

	//添加公司信息
	void insert(Business business);

	//查询公司信息列表
	Business queryBusinessList(Long currentUserId);

	//在公司信息查询是否存在该用户(自己查自己),存在就不能添加,只能修改
	Long queryIfExistUserId(Long currentUserId);

	//根据用户id查询公司名称
	String queryBusiness_name(Long user_id);

	//查询所有的公司
	List<Business> queryBusinessInsertList();

	//添加图片到数据库
	void saveImage(Picture picture);

	//查询图片
	Picture queryPicture(Long currentUserId);

	//删除不通过的公司信息和照片
	void deleteBusiness(Long user_id);
	//删除不通过的公司信息和照片
	void deletePicture(Long user_id);

	//根据公司id查询公司信息
	Business queryBusinessByBusiness_id(Long business_id);

	//添加公司信息审核通过的时间
	void updateSuccess_date(@Param("user_id") Long user_id, @Param("success_date") Date success_date);

	//修改公司信息
	void updateBusiness(Business business);

	//通过分页查询所有企业信息
	List<Business> queryAllBusinessByPage(@Param("page") int page, @Param("limit") int limit);

	//查询公司信息总条数
	int queryTotalBusiness();
}
