package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.OtaProvince;
import miaosu.dao.model.OtaProvinceExample;
import org.apache.ibatis.annotations.Param;

public interface OtaProvinceMapper {
    long countByExample(OtaProvinceExample example);

    int deleteByExample(OtaProvinceExample example);

    int deleteByPrimaryKey(String code);

    int insert(OtaProvince record);

    int insertSelective(OtaProvince record);

    List<OtaProvince> selectByExample(OtaProvinceExample example);

    OtaProvince selectByPrimaryKey(String code);

    int updateByExampleSelective(@Param("record") OtaProvince record, @Param("example") OtaProvinceExample example);

    int updateByExample(@Param("record") OtaProvince record, @Param("example") OtaProvinceExample example);

    int updateByPrimaryKeySelective(OtaProvince record);

    int updateByPrimaryKey(OtaProvince record);
}