package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.OtaCity;
import miaosu.dao.model.OtaCityExample;
import org.apache.ibatis.annotations.Param;

public interface OtaCityMapper {
    long countByExample(OtaCityExample example);

    int deleteByExample(OtaCityExample example);

    int deleteByPrimaryKey(String code);

    int insert(OtaCity record);

    int insertSelective(OtaCity record);

    List<OtaCity> selectByExample(OtaCityExample example);

    OtaCity selectByPrimaryKey(String code);

    int updateByExampleSelective(@Param("record") OtaCity record, @Param("example") OtaCityExample example);

    int updateByExample(@Param("record") OtaCity record, @Param("example") OtaCityExample example);

    int updateByPrimaryKeySelective(OtaCity record);

    int updateByPrimaryKey(OtaCity record);
}