package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.OtaDistrict;
import miaosu.dao.model.OtaDistrictExample;
import org.apache.ibatis.annotations.Param;

public interface OtaDistrictMapper {
    long countByExample(OtaDistrictExample example);

    int deleteByExample(OtaDistrictExample example);

    int deleteByPrimaryKey(String code);

    int insert(OtaDistrict record);

    int insertSelective(OtaDistrict record);

    List<OtaDistrict> selectByExample(OtaDistrictExample example);

    OtaDistrict selectByPrimaryKey(String code);

    int updateByExampleSelective(@Param("record") OtaDistrict record, @Param("example") OtaDistrictExample example);

    int updateByExample(@Param("record") OtaDistrict record, @Param("example") OtaDistrictExample example);

    int updateByPrimaryKeySelective(OtaDistrict record);

    int updateByPrimaryKey(OtaDistrict record);
}