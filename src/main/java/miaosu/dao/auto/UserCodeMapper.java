package miaosu.dao.auto;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/7/8 11:46
 * @Description:
 */
public interface UserCodeMapper {
	//将user_id,code_id,code_shangjia,code_my添加到用户邀请码关系表
	void insert(@Param("userId")Long userId, @Param("codeId")Long codeId,@Param("code_shangjia") String code_shangjia, @Param("code_my")String code_my);

	//根据用户id查询自己的邀请码code_my
	String queryCode_my(@Param("currentUserId")Long currentUserId);

	//根据我的邀请码查询下家的邀请码所对应的用户id
	List<Long> queryCode_xiajia(@Param("code_my")String code_my);

	//根据成员id查询code_id
	Long queryCode_id(Long user_id);

	//根据code_id查询审核状态codel_level
	String queryCode_level(Long code_id);

	//将用户的code_level设置为已通过
	void updateCode_level(Long code_id);
}
