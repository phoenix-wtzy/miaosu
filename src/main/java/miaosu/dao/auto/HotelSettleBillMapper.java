package miaosu.dao.auto;

import java.util.List;
import miaosu.dao.model.HotelSettleBill;
import miaosu.dao.model.HotelSettleBillExample;
import org.apache.ibatis.annotations.Param;

public interface HotelSettleBillMapper {
    long countByExample(HotelSettleBillExample example);

    int deleteByExample(HotelSettleBillExample example);

    int deleteByPrimaryKey(Long id);

    int insert(HotelSettleBill record);

    int insertSelective(HotelSettleBill record);

    List<HotelSettleBill> selectByExample(HotelSettleBillExample example);

    HotelSettleBill selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") HotelSettleBill record, @Param("example") HotelSettleBillExample example);

    int updateByExample(@Param("record") HotelSettleBill record, @Param("example") HotelSettleBillExample example);

    int updateByPrimaryKeySelective(HotelSettleBill record);

    int updateByPrimaryKey(HotelSettleBill record);
}