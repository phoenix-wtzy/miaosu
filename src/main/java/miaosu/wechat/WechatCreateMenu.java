package miaosu.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import miaosu.dao.model.WechatMenuButton;
import miaosu.dao.model.WechatSubButton;
import miaosu.dao.model.WechatViewButton;
import miaosu.sms.SmsInstance;
import miaosu.utils.HttpsUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class WechatCreateMenu {
    private static Logger logger = Logger.getLogger(SmsInstance.class);
    @PostConstruct
    public static void createMenu() {
        String at = WechatUtils.getAccessToken();
        //菜单对象
        WechatMenuButton bt = new WechatMenuButton();
        //第一个一级菜单
        WechatViewButton adsSb = new WechatViewButton("ADS助手","http://ebk.miaosu.ink/");
        //adsSb.getSub_button().add(new WechatViewButton("","http://ebk.miaosu.ink/"));
        //adsSb.getSub_button().add(new WechatViewButton("酒店评估","http://ebk.miaosu.ink/"));
        //第二个一级菜单
        WechatViewButton adsSb1 = new WechatViewButton("酒店评估","http://ebk.miaosu.ink/");
        //第三个一级菜单
        WechatSubButton adssb2 = new WechatSubButton("乐易换商城1");
        adssb2.getSub_button().add(new WechatViewButton("酒店注册","http://ebk.miaosu.ink/"));
        adssb2.getSub_button().add(new WechatViewButton("供应商入驻","http://ebk.miaosu.ink/"));
        //加入第三个一级菜单
        bt.getButton().add(adsSb);
        bt.getButton().add(adsSb1);
        bt.getButton().add(adssb2);
        JSONObject jsonObject = new JSONObject();
//        System.out.println(jsonObject.toJSONString(bt));
        //发送请求
        String result = HttpsUtil.doPost(WechatConfig.getGetMenuUrl()+at,"","",jsonObject.toJSONString(bt));
        System.out.println(result);
        if (null != result) {
            int errorCode = JSON.parseObject(result).getIntValue("errcode");
            String errorMsg = JSON.parseObject(result).getString("errmsg");
            if (0 == errorCode) {
            } else {
                logger.error("创建菜单失败 errcode："+errorCode+" errmsg："+errorMsg);
            }
        }
    }
}
