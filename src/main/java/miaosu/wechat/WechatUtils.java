package miaosu.wechat;

import com.thoughtworks.xstream.XStream;
import miaosu.dao.model.WechatBaseMessage;
import miaosu.dao.model.WechatTextMessage;
import miaosu.sms.SmsInstance;
import miaosu.utils.JsonUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WechatUtils {

    //用于存储token
    private static WechatAccessToken at;

    //用于存储codeToken
    private static WechatCodeForAccessToken atCodeToken;

    private static Logger logger = Logger.getLogger(SmsInstance.class);

    public static boolean checkSignature(String signature,String timestamp,String nonce){
        String[] str = new String[]{WechatConfig.getToken(),timestamp,nonce};
        //排序
        Arrays.sort(str);
        //拼接字符串
        StringBuffer buffer = new StringBuffer();
        for(int i =0 ;i<str.length;i++){
            buffer.append(str[i]);
        }
        //进行sha1加密
        String temp = SHA1.encode(buffer.toString());
        //与微信提供的signature进行匹对
        return signature.equals(temp);
    }

    /**
     * @description: TODO
     * 获取AccessToken入口（2小时自动后失效）
     * @author zhangyz
     * @date 2020/12/3 17:14
     * @version 1.0
     */
    public static String getAccessToken(){

        if(at==null || at.isExpired()){
            getThisToken();
        }
        return at.getAccessToken();
    }

    private static void getThisToken(){
        //System.out.println(WechatConfig.getToken());
        String url = WechatConfig.getGetTokenUrl().replace("APPID",WechatConfig.getAppid()).replace("APPSECRET",WechatConfig.getAppsecret());
        String tokenStr = get(url);
        Map<String, Object> jsonObj = JsonUtils.parseJSONMap(tokenStr);
        String token = String.valueOf(jsonObj.get("access_token"));
        String expireIn =String.valueOf(jsonObj.get("expires_in"));
        //本地调试或者ip没有加入白名单
        if(token == "null" || expireIn == "null"){
            logger.error("本地调试或者ip没有加入白名单");
            expireIn = "0";
        }

        //创建token对象，并保存起来
        at = new WechatAccessToken(token,expireIn);
    }

    /**
     * @description: TODO
     * 获取网页授权access_token入口（2小时自动后失效）
     * @author zhangyz
     * @date 2020/12/3 17:14
     * @version 1.0
     */
//    public static String getCodeForAccessToken(String code){
//
//        if(atCodeToken==null){
//            getThisCodeForAccessToken(code);
//        }else if(atCodeToken.isExpired()){
//
//        }
//        return atCodeToken.getAccessToken();
//    }
//
//    private static void getThisCodeForAccessToken(String code){
//        //System.out.println(WechatConfig.getToken());
//        String url ="https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
//        url = url.replace("APPID",WechatConfig.getAppid()).replace("SECRET",WechatConfig.getAppsecret()).replace("CODE",code);
//
//       // String url = WechatConfig.getGetTokenUrl().replace("APPID",WechatConfig.getAppid()).replace("APPSECRET",WechatConfig.getAppsecret());
//        String result = get(url);
//
//        Map<String, Object> jsonObj = JsonUtils.parseJSONMap(result);
//        String token = String.valueOf(jsonObj.get("refresh_token"));
//        String expireIn =String.valueOf(jsonObj.get("expires_in"));
//        String openid = String.valueOf(jsonObj.get("openid"));
//        //创建token对象，并保存起来
//        atCodeToken = new WechatCodeForAccessToken(token,expireIn);
//    }



    /**
     * @description: TODO
     * 获取url链接
     * @author zhangyz
     * @date 2020/12/3 17:10
     * @version 1.0
     */
    public static String get(String url){
        try {
            URL urlObj = new URL(url);
            URLConnection connection = urlObj.openConnection();
            InputStream is = connection.getInputStream();
            byte[] b = new byte[1024];
            int len;
            StringBuilder sb = new StringBuilder();
            while ((len=is.read(b))!=-1){
                sb.append(new String(b,0,len));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

        /**
         * @description: TODO
         *  解析微信xml数据包
         * @author zhangyz
         * @date 2020/12/3 17:10
         * @version 1.0
         */
    public static Map<String,String> parseRequest(InputStream is){
        Map<String,String> map = new HashMap<>();
        SAXReader reader = new SAXReader();
        try {
            //读取输入流,获取文档对象
            Document document = reader.read(is);
            //根据文档对象获取根节点
            Element root = document.getRootElement();
            //获取节点所有的子节点
            List<Element> elements = root.elements();
            for(Element e: elements){
                map.put(e.getName(),e.getStringValue());
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 用于处理所有事件和消息的回复
     * @param requestMap
     * @return 返回是XML数据包
     * by zhangyz
     */
    public static String getRespose(Map<String,String> requestMap){
        WechatBaseMessage msg = null;
        String msgType = requestMap.get("MsgType");
        switch (msgType){
            //处理文本消息
            case "text":
                msg = dealTextMessage(requestMap);
                break;
            case "image":
                break;
            case "voice":
                break;
            case "location":
                break;
            case "link":
                break;

        }
        if(msg!=null){
            return beanToXml(msg);
        }
        return null;
    }


    /**
     * @description:
     * 返回微信消息处理程XML数据包
     * @author zhangyz
     * @date 2020/12/2 16:49
     * @version 1.0
     */
    private static String beanToXml(WechatBaseMessage msg) {
        Map<String,String> map = new HashMap<>();
        map.put("ToUserNmae","to");
        map.put("FromUserName","from");
        map.put("MsgType","type");

        XStream stream = new XStream();
        stream.processAnnotations(WechatTextMessage.class);
        String xml = stream.toXML(msg);
        System.out.println(xml);
        return xml;
    }

    /**
     * @description: TODO
     * 处理文本消息
     * @author zhangyz
     * @date 2020/12/3 17:11
     * @version 1.0
     */
    private static WechatBaseMessage dealTextMessage(Map<String, String> requestMap) {
        //用户发送过来的请求
        String msg = requestMap.get("Content");
        WechatTextMessage tm = null;
//        if (msg.equals("wsmiaosuadmin666")) {
//            String url = WechatConfig.getGetUserInfoUrl().replace("APPID",WechatConfig.getAppid()).
//                    replace("RedirectUri",WechatConfig.getRedirectUri());
//             tm = new WechatTextMessage(requestMap, "<a href=\"" + url + "\">点击这里绑定业务人员账号！</a>");
//            return tm;

//        }
        if (msg != null){
             tm = new WechatTextMessage(requestMap, "感谢关注河南秒宿！");
        }
        return tm;
    }
}
