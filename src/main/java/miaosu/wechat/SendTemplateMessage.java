package miaosu.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import miaosu.dao.model.HotelOrder;
import miaosu.sms.SmsInstance;
import miaosu.utils.HttpsUtil;
import miaosu.web.mvc.BaseController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @description: TODO
 * 遍历所有对应权限oppid，并发送模板信息
 * @author zhangyz
 * @date 2020/12/4 11:46
 * @version 1.0
 */
@Component
public class SendTemplateMessage extends BaseController {
    static JSONObject jsonObject = new JSONObject();
    static RedisTemplate redisTemplate;
    private static Logger logger = Logger.getLogger(SmsInstance.class);
    @Autowired
    public SendTemplateMessage(RedisTemplate redisTemplate) {
        SendTemplateMessage.redisTemplate = redisTemplate;
    }

    public static void sendMessage(HotelOrder order,List wxOpenidList){
        String at = WechatUtils.getAccessToken();
        if(at == "null"){
            logger.error("模板信息发送失败，未获取到at！");
            return;
        }
        Boolean state = order.getState() == 1 ?true:false;
        Long openidListCount = redisTemplate.opsForList().size("openidList");
        if(openidListCount>0){

            String roleName = null;
            String wxOpenid = null;

            for(int i = 0; i< wxOpenidList.size(); i++){
                Map<String, Object> mapList = (Map<String, Object>) wxOpenidList.get(i);
                Iterator<String> it = mapList.keySet().iterator();
                while( it.hasNext() ) {
                    String str = (String) it.next();
                    System.out.println( "key-->" + str + "\t value-->" + mapList.get(str) );
                    if(str.contains("role_name")){
                        roleName = mapList.get(str).toString();
                    }else{
                        wxOpenid = mapList.get(str).toString();
                    }
                }
                jsonObject.put("template_id", WechatConfig.getTemplateid1());
                jsonObject.put("url", "http://ebk.miaosu.ink/");

                JSONObject data = new JSONObject();
                JSONObject first = new JSONObject();
                //标题
                if(state){
                    first.put("value", order.getHotelName()+"来新订单了");
                }else{
                    first.put("value", order.getHotelName()+"来取消单了");
                }

                first.put("color", "#ff0000");
                //预定时间
                JSONObject keyword1 = new JSONObject();
                keyword1.put("value", DateFormatUtils.format(order.getBookTime(),"yyyy-MM-dd HH:mm:ss"));
                keyword1.put("color", "#173177");
                //客房名称
                JSONObject keyword2 = new JSONObject();
                keyword2.put("value", order.getRoomCount()+"间"+order.getP_hotel_room_type_name());
                keyword2.put("color", "#173177");
                //入离时间
                JSONObject keyword3 = new JSONObject();
                keyword3.put("value", DateFormatUtils.format(order.getCheckInTime(),"yyyy-MM-dd")
                        +" ~ "+DateFormatUtils.format(order.getCheckOutTime(),"yyyy-MM-dd")+" 共"+order.getNightC()+"晚");
                keyword3.put("color", "#173177");
                //入住人
                JSONObject customerName = new JSONObject();
                customerName.put("value", order.getBookUser());
                customerName.put("color", "#173177");
                //房费总额
                JSONObject keyword4 = new JSONObject();
                if(roleName.contains("ROLE_USER")) {
                    keyword4.put("value", order.getCaigou_total().toString());
                }else{
                    keyword4.put("value", order.getPrice().toString());
                }
                keyword4.put("color", "#173177");
                //订单号
                JSONObject keyword5 = new JSONObject();
                if(roleName.contains("ROLE_USER")) {
                    keyword5.put("value", order.getOrderId());
                }else{
                    keyword5.put("value", order.getOrderNo());
                }
                keyword5.put("color", "#173177");
                //备注
                JSONObject remark = new JSONObject();
                remark.put("value", "渠道编号："+order.getOrderChannel());
                remark.put("color", "#173177");

                data.put("first",first);
                data.put("orderdate",keyword1);
                data.put("roomNum",keyword2);
                data.put("date",keyword3);
                data.put("customerName",customerName);
                data.put("price",keyword4);
                data.put("orderNo",keyword5);
                data.put("arrivalTime","");
                data.put("remark",remark);

                jsonObject.put("data", data);
                jsonObject.put("touser", wxOpenid);   // openid

                String result = HttpsUtil.doPost(WechatConfig.getSendTemplateMessageUrl()+at,"","",jsonObject.toJSONString());
                int errcode = JSON.parseObject(result).getIntValue("errcode");
                if(errcode == 0){
                    // 发送成功
                    logger.info("wx消息模板发送成功！");
                } else {
                    // 发送失败
                    logger.info("wx消息模板发送失败！");
                }
            }
        }
    }
}