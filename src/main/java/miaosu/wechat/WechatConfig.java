package miaosu.wechat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WechatConfig {

    private static String token;

    private static String appid;

    private static String appsecret;

    private static String getTokenUrl;

    private static String sendTemplateMessageUrl;

    private static String templateid1;

    private static String getUserInfoUrl;

    private static String redirectUri;

    private static String getMenuUrl;

    public static String getToken() {
        return token;
    }
    @Value("${wechat.token}")
    public void setToken(String token) {
        this.token = token;
    }

    public static String getAppsecret() {
        return appsecret;
    }

    @Value("${wechat.appsecret}")
    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }
    public static String getGetTokenUrl() {
        return getTokenUrl;
    }

    @Value("${wechat.getTokenUrl}")
    public void setGetTokenUrl(String getTokenUrl) {
        this.getTokenUrl = getTokenUrl;
    }

    public static String getAppid() {
        return appid;
    }

    @Value("${wechat.appid}")
    public void setAppid(String appid) {
        this.appid = appid;
    }

    public static String getSendTemplateMessageUrl() {
        return sendTemplateMessageUrl;
    }

    @Value("${wechat.sendTemplateMessageUrl}")
    public void setSendTemplateMessageUrl(String sendTemplateMessageUrl) {
        this.sendTemplateMessageUrl = sendTemplateMessageUrl;
    }

    public static String getTemplateid1() {
        return templateid1;
    }

    @Value("${wechat.templateid1}")
    public void setTemplateid1(String templateid1) {
        this.templateid1 = templateid1;
    }

    public static String getGetUserInfoUrl() {
        return getUserInfoUrl;
    }

    @Value("${wechat.getUserInfoUrl}")
    public void setGetUserInfoUrl(String getUserInfoUrl) {
        this.getUserInfoUrl = getUserInfoUrl;
    }

    public static String getRedirectUri() {
        return redirectUri;
    }

    @Value("${wechat.redirectUrl}")
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public static String getGetMenuUrl() {
        return getMenuUrl;
    }
    @Value("${wechat.getMenuUrl}")
    public void setGetMenuUrl(String getMenuUrl) {
        this.getMenuUrl = getMenuUrl;
    }
}
