/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package miaosu;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import miaosu.common.SettleStatusEnum;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.svc.hotel.HotelRoomPriceService;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.hotel.HotelSupplierService;
import miaosu.svc.hotel.IHotelPriceSetService;
import miaosu.svc.order.OrderOfCaigouoneService;
import miaosu.svc.order.OrderService;
import miaosu.svc.settle.*;
import miaosu.svc.user.BusinessService;
import miaosu.svc.vo.HoteInfoVO;
import miaosu.utils.GetEveryMonthOfYearUtil;
import miaosu.utils.GetEveryWeekOfYearUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@SpringBootApplication
@MapperScan("miaosu.dao.auto")
@ComponentScan(basePackages = {"com.miaosu","miaosu"})
public class SpringBootWebApplication{

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringBootWebApplication.class, args);
	}

	@Autowired
	private OrderService orderService;
	@Autowired
	private HotelService hotelService;
	@Autowired
	private BusinessService businessService;
	@Autowired
	private HotelSettleService hotelSettleService;
	@Autowired
	private JiuDianService jiuDianService;
	@Autowired
	private UserCodeMapper userCodeMapper;
	@Autowired
	private GongYingShangService gongYingShangService;
	@Autowired
	private Hotel_settle_isActiveService hotel_settle_isActiveService;

	@Autowired
	private ChartsService chartsService;

	@Autowired
	private HotelOrderMapper orderMapper;
	@Autowired
	private HotelSupplierService hotelSupplierService;
	@Autowired
	private GetHotelOrderMapper getHotelOrderMapper;

	@Autowired
	private IHotelPriceSetService hotelPriceSetService;
	@Autowired
	private HotelPriceRefRoomTypeMapper hotelPriceRefRoomTypeMapper;
	@Autowired
	private OrderOfCaigouoneService orderOfCaigouoneService;
	@Autowired
	private HotelRoomPriceService hotelRoomPriceService;
	@Autowired
	private HotelOrderMapper hotelOrderMapper;

//	//只执行一次的代码,导入数据到房价日历订单表中order_caigou_one,随后注释掉即可,不可能每次启动都计算
//	@Configuration
//	@EnableScheduling
//	public  class insertOrder_caigou_one{
//		@Scheduled(initialDelay = 10000, fixedRate = Long.MAX_VALUE)
//		public void orderOfSettle_money()throws ParseException{
//			HotelOrderExample hotelOrderExample = new HotelOrderExample();
//			List<HotelOrder> hotelOrderList = orderService.queryOrderList(hotelOrderExample);
//			for (HotelOrder hotelOrder : hotelOrderList) {
//				Long orderId = hotelOrder.getOrderId();
//				if (orderId>3559) {
//					Long p_hotel_id = hotelOrder.getHotelId();
//					String p_hotel_room_type_name = hotelOrder.getP_hotel_room_type_name();
//					insertOrderOfCaigouone(hotelOrder, p_hotel_id, p_hotel_room_type_name, orderId);
//				}else {
//					Long p_hotel_id = hotelOrder.getHotelId();
//					String p_hotel_room_type_name = hotelOrder.getP_hotel_room_type_name();
//					String bookRemark = hotelOrder.getBookRemark();
//					String name=p_hotel_room_type_name+"("+bookRemark+")";
//					insertOrderOfCaigouone2(hotelOrder, p_hotel_id, p_hotel_room_type_name, orderId);
//
//					HotelOrder record = new HotelOrder();
//					record.setOrderId(orderId);
//					record.setCommission_rate(hotelOrder.getCommission_rate());
//					record.setP_hotel_room_type_name(name);
//					hotelOrderMapper.updateByPrimaryKeySelective(record);
//					getHotelOrderMapper.updateByPrimaryKeySelective(record);
//				}
//			}
//		}
//	}
	public void insertOrderOfCaigouone(@RequestBody HotelOrder hotelGetOrder, Long p_hotel_id, String p_hotel_room_type_name, long orderId) throws ParseException {
		Integer orderRoomCount = hotelGetOrder.getRoomCount();
		for (int i = 0; i <orderRoomCount; i++) {
			Date checkInTime = hotelGetOrder.getCheckInTime();
			Date checkOutTime = hotelGetOrder.getCheckOutTime();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String checkInTime1 = simpleDateFormat.format(checkInTime);
			String checkOutTime1 = simpleDateFormat.format(checkOutTime);
			Date startDate = DateUtils.parseDate(checkInTime1, "yyyy-MM-dd");
			Date endDate = DateUtils.parseDate(checkOutTime1, "yyyy-MM-dd");
			for (Date d = startDate;d.getTime()<endDate.getTime();d=DateUtils.addDays(d, 1)){
				OrderOfCaigouone orderOfCaigouone = new OrderOfCaigouone();
				orderOfCaigouone.setOrder_id(orderId);
				orderOfCaigouone.setCaigou_one_state(0);
				orderOfCaigouone.setCheck_in_time(d);
				//步骤1
				boolean holiday = getWeekStr(DateUtils.parseDate(checkInTime1,"yyyy-MM-dd"));
				List<HotelPriceSet> hotelPriceSets= Lists.newArrayList();
				if(holiday && p_hotel_id == 27){
					hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(29L, p_hotel_room_type_name);
				}else{
					hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(p_hotel_id, p_hotel_room_type_name);
				}
				if (CollectionUtils.isEmpty(hotelPriceSets)){
					orderOfCaigouone.setCaigou_one(BigDecimal.valueOf(-1));
				}else {
					HotelPriceSet hotelPriceSet = hotelPriceSets.get(0);
					Long hotelPriceId = hotelPriceSet.getHotelPriceId();
					//步骤2
					Long productId =hotelPriceRefRoomTypeMapper.queryProductIdByHotelPriceId(hotelPriceId);
					if (null==productId){
						orderOfCaigouone.setCaigou_one(BigDecimal.valueOf(-1));
					}else{
						//步骤3
						BigDecimal price=hotelRoomPriceService.queryPrice(productId,d);
						if (null==price){
							orderOfCaigouone.setCaigou_one(BigDecimal.valueOf(-1));
						}else {
							orderOfCaigouone.setCaigou_one(price);
						}
					}
				}
				//步骤4
				orderOfCaigouoneService.insert(orderOfCaigouone);
			}
		}
	}
	public void insertOrderOfCaigouone2(@RequestBody HotelOrder hotelGetOrder, Long p_hotel_id, String p_hotel_room_type_name, long orderId) throws ParseException {
		Integer orderRoomCount = hotelGetOrder.getRoomCount();
		for (int i = 0; i <orderRoomCount; i++) {
			Date checkInTime = hotelGetOrder.getCheckInTime();
			Date checkOutTime = hotelGetOrder.getCheckOutTime();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String checkInTime1 = simpleDateFormat.format(checkInTime);
			String checkOutTime1 = simpleDateFormat.format(checkOutTime);
			Date startDate = DateUtils.parseDate(checkInTime1, "yyyy-MM-dd");
			Date endDate = DateUtils.parseDate(checkOutTime1, "yyyy-MM-dd");
			for (Date d = startDate;d.getTime()<endDate.getTime();d=DateUtils.addDays(d, 1)){
				OrderOfCaigouone orderOfCaigouone = new OrderOfCaigouone();
				orderOfCaigouone.setOrder_id(orderId);
				orderOfCaigouone.setCaigou_one_state(0);
				orderOfCaigouone.setCheck_in_time(d);
				orderOfCaigouone.setCaigou_one(hotelGetOrder.getCaigou_one());
				//步骤4
				orderOfCaigouoneService.insert(orderOfCaigouone);
			}
		}
	}

	public Boolean getWeekStr(Date date) {
		Calendar cal = Calendar.getInstance();
		if (date != null) {
			cal.setTime(date);
			int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
			switch (week) {
				case 5:
				case 6:
					return true;
			}
		}
		return false;
	}
//	//重启代码只执行一次的任务类,用于计算订单表中的结算金额,随后注释掉即可,不可能每次启动都计算
//	@Configuration
//	@EnableScheduling
//	public  class orderOfSettle_money{
//		/**
//		 * initialDelay 程序启动多长时间后执行
//		 * fixedRate 中间间隔多久将再次执行
//		 * Long.MAX_VALUE = 106751991167天7小时12分钟55秒(大约292471209年)
//		 */
//		@Scheduled(initialDelay = 10000, fixedRate = Long.MAX_VALUE)
//		public void orderOfSettle_money(){
//			//计算订单表中的结算金额
//			HotelOrderExample hotelOrderExample = new HotelOrderExample();
//			List<HotelOrder> hotelOrderList1 = orderService.queryOrderList(hotelOrderExample);
//			HotelOrder order = new  HotelOrder();
//			for (HotelOrder hotelOrder : hotelOrderList1) {
//				Long hotelId = hotelOrder.getHotelId();
//				HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
//				HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
//				criteria.andhotel_idEqualTo(hotelId).andhotel_supplier_stateEqualTo(1);
//				HotelSupplier hotelSupplier = hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
//				int commission_rate = hotelSupplier.getCommission_rate();
//				BigDecimal price = hotelOrder.getPrice();
//				BigDecimal settle_money = new BigDecimal(0);
//				settle_money = price.multiply(new BigDecimal((100 - commission_rate))).divide(new BigDecimal(100));
//				settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
//				order.setSettle_money(String.valueOf(settle_money));
//				order.setOrderId(hotelOrder.getOrderId());
//				order.setCommission_rate(commission_rate);
//				orderMapper.updateByPrimaryKeySelective(order);
//				getHotelOrderMapper.updateByPrimaryKeySelective(order);
//			}
//		}
//	}

	//上下线酒店结算汇总
	@Configuration
	@EnableScheduling
	public  class hotel_settle_isActive {
		@Scheduled(cron = "0 55 23 * * ?")
//		@Scheduled(cron = "0/5 * * * * ?")
		public void hotel_settle_isActive()throws Exception{
			System.out.println("上下线酒店结算汇总定时任务执行了==================");
			List<HotelInfo>hotelInfoList=hotelService.queryAllHotelInfo();
			System.out.println("xxxxxxxxxxxx酒店总数"+hotelInfoList.size());
			for (HotelInfo hotelInfo : hotelInfoList) {
				//区分酒店上下线,上线酒店添加到hotel_settle_isactive表,下线添加到hotel_settle_isnotactive表中
				int isActive = hotelInfo.getIsActive();
				if (isActive==1){
					Long hotelId = hotelInfo.getHotelId();
					Hotel_settle_isActive hotel_settle_isActive = new Hotel_settle_isActive();
					//酒店id
					hotel_settle_isActive.setHotelId(hotelId);
					//酒店名称
					String hotelName = hotelInfo.getHotelName();
					hotel_settle_isActive.setHotel_name(hotelName);
					//酒店上线时间
					Date createTime = hotelInfo.getCreateTime();
					hotel_settle_isActive.setCreateTime(createTime);
					//协议置换总金额
					String settle_totalMoney = hotelInfo.getSettle_totalMoney();
					//判空
					if (Strings.isNullOrEmpty(settle_totalMoney)|| settle_totalMoney.equals("null")){
						settle_totalMoney="0";
					}
					hotel_settle_isActive.setSettle_totalMoney(settle_totalMoney);


					//根据酒店id查询所有对应的订单列表,很多个
					HotelOrderExample hotelOrderExample = new HotelOrderExample();
					HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
					and.andHotelIdEqualTo(hotelId).andStateEqualTo(2);
					List<HotelOrder> hotelOrderList = orderService.queryOrderList(hotelOrderExample);
					System.out.println("xxxxxxxxxx对应酒店总订单数"+hotelOrderList.size());
					BigDecimal order_all_price=new BigDecimal(0);//实际订单总金额=每单的价格相加
					BigDecimal order_all_caigou_total=new BigDecimal(0);//已置换总金额(实际订单总置换价)
					long order_all_night=0;//已置换间夜数(实际订单间夜数)=每单间夜数相加
					for (HotelOrder hotelOrder : hotelOrderList) {
						BigDecimal price = hotelOrder.getPrice();//房价
						//实际订单总金额=每单的价格相加
						order_all_price=order_all_price.add(price);
						//已置换总金额(实际订单总置换价)
						BigDecimal caigou_total = hotelOrder.getCaigou_total();
						//对采购总价进行判空,如果为null就设置为0,防止报错
						if (caigou_total==null){
							 caigou_total= BigDecimal.valueOf(0);
						}
						order_all_caigou_total=order_all_caigou_total.add(caigou_total);
						//已置换间夜数(实际订单间夜数)=每单间夜数相加
						Integer nightC = hotelOrder.getNightC();
						if (nightC==null){
							nightC=0;
						}
						order_all_night+=nightC;
					}
					//已置换总金额
					hotel_settle_isActive.setSettle_alreadyMoney(String.valueOf(order_all_caigou_total));
					//剩余置换总金额
					BigDecimal b1=new BigDecimal(settle_totalMoney);
					BigDecimal settle_haveMoney=new BigDecimal(0);
					settle_haveMoney=b1.subtract(order_all_caigou_total);
					hotel_settle_isActive.setSettle_haveMoney(String.valueOf(settle_haveMoney));
					//实际订单总金额
					hotel_settle_isActive.setOrder_totalMoney(String.valueOf(order_all_price));

					//协议置换间夜数
					String settle_totalNight = hotelInfo.getSettle_totalNight();
					//判空
					if (Strings.isNullOrEmpty(settle_totalNight)||settle_totalNight.equals("null")){
						settle_totalNight="0";
					}
					hotel_settle_isActive.setSettle_totalNight(settle_totalNight);
					//已置换间夜
					hotel_settle_isActive.setSettle_alreadyNight(String.valueOf(order_all_night));
					//剩余置换间夜
					int a= Integer.parseInt(settle_totalNight);
					long settle_haveNight=a-order_all_night;
					hotel_settle_isActive.setSettle_haveNight(String.valueOf(settle_haveNight));

					//对应酒店的佣金率
					HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
					HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
					criteria.andhotel_idEqualTo(hotelId).andhotel_supplier_stateEqualTo(1);
					HotelSupplier hotelSupplier = hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
					Integer commissionRate =0;
					if (hotelSupplier!=null){
						commissionRate = hotelSupplier.getCommission_rate();
						hotel_settle_isActive.setCommission_rate(commissionRate);
					}else {
						hotel_settle_isActive.setCommission_rate(commissionRate);
						System.out.println("酒店没有设置佣金比例和周期");
					}
					//佣金=实际订单总额*佣金率
					BigDecimal commission_money = new BigDecimal(0);
					commission_money = order_all_price.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
					hotel_settle_isActive.setCommission(String.valueOf(commission_money));
					//应付=实际订单总额-佣金
					BigDecimal needPay_money = order_all_price.subtract(commission_money);
					hotel_settle_isActive.setNeedPay_money(String.valueOf(needPay_money));

					//已支付
					//余额

					//今天的结算时间记录
					hotel_settle_isActive.setTodayTime(new Date());
					//上线酒店添加到hotel_settle_isActive表中
					hotel_settle_isActiveService.insert(hotel_settle_isActive);
				}else {
					Long hotelId = hotelInfo.getHotelId();
					Hotel_settle_isActive hotel_settle_isActive = new Hotel_settle_isActive();
					//酒店id
					hotel_settle_isActive.setHotelId(hotelId);
					//酒店名称
					String hotelName = hotelInfo.getHotelName();
					hotel_settle_isActive.setHotel_name(hotelName);
					//酒店上线时间
					Date createTime = hotelInfo.getCreateTime();
					hotel_settle_isActive.setCreateTime(createTime);
					//协议置换总金额
					String settle_totalMoney = hotelInfo.getSettle_totalMoney();
					//判空
					if (Strings.isNullOrEmpty(settle_totalMoney)||settle_totalMoney.equals("null")){
						settle_totalMoney="0";
					}
					hotel_settle_isActive.setSettle_totalMoney(settle_totalMoney);


					//根据酒店id查询所有对应的订单列表,很多个
					HotelOrderExample hotelOrderExample = new HotelOrderExample();
					HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
					and.andHotelIdEqualTo(hotelId).andStateEqualTo(2);
					List<HotelOrder> hotelOrderList = orderService.queryOrderList(hotelOrderExample);
					System.out.println("xxxxxxxxxx对应酒店总订单数"+hotelOrderList.size());
					BigDecimal order_all_price=new BigDecimal(0);//实际订单总金额=每单的价格相加
					BigDecimal order_all_caigou_total=new BigDecimal(0);//已置换总金额(实际订单总置换价)
					long order_all_night=0;//已置换间夜数(实际订单间夜数)=每单间夜数相加
					for (HotelOrder hotelOrder : hotelOrderList) {
						BigDecimal price = hotelOrder.getPrice();//房价
						//实际订单总金额=每单的价格相加
						order_all_price=order_all_price.add(price);
						//已置换总金额(实际订单总置换价)
						BigDecimal caigou_total = hotelOrder.getCaigou_total();
						//对采购总价进行判空,如果为null就设置为0,防止报错
						if (caigou_total==null){
							caigou_total= BigDecimal.valueOf(0);
						}
						order_all_caigou_total=order_all_caigou_total.add(caigou_total);
						//已置换间夜数(实际订单间夜数)=每单间夜数相加
						Integer nightC = hotelOrder.getNightC();
						if (nightC==null){
							nightC=0;
						}
						order_all_night+=nightC;
					}
					//已置换总金额
					hotel_settle_isActive.setSettle_alreadyMoney(String.valueOf(order_all_caigou_total));
					//剩余置换总金额
					BigDecimal b1=new BigDecimal(settle_totalMoney);
					BigDecimal settle_haveMoney=new BigDecimal(0);
					settle_haveMoney=b1.subtract(order_all_caigou_total);
					hotel_settle_isActive.setSettle_haveMoney(String.valueOf(settle_haveMoney));
					//实际订单总金额
					hotel_settle_isActive.setOrder_totalMoney(String.valueOf(order_all_price));

					//协议置换间夜数
					String settle_totalNight = hotelInfo.getSettle_totalNight();
					//判空
					if (Strings.isNullOrEmpty(settle_totalNight)||settle_totalNight.equals("null")){
						settle_totalNight="0";
					}
					hotel_settle_isActive.setSettle_totalNight(settle_totalNight);
					//已置换间夜
					hotel_settle_isActive.setSettle_alreadyNight(String.valueOf(order_all_night));
					//剩余置换间夜
					int a= Integer.parseInt(settle_totalNight);
					long settle_haveNight=a-order_all_night;
					hotel_settle_isActive.setSettle_haveNight(String.valueOf(settle_haveNight));

					//对应酒店的佣金率
					HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
					HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
					criteria.andhotel_idEqualTo(hotelId).andhotel_supplier_stateEqualTo(1);
					HotelSupplier hotelSupplier = hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
					Integer commissionRate =0;
					if (hotelSupplier!=null){
						commissionRate = hotelSupplier.getCommission_rate();
						hotel_settle_isActive.setCommission_rate(commissionRate);
					}else {
						hotel_settle_isActive.setCommission_rate(commissionRate);
						System.out.println("酒店没有设置佣金比例和周期");
					}
					//佣金=实际订单总额*佣金率
					BigDecimal commission_money = new BigDecimal(0);
					commission_money = order_all_price.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
					hotel_settle_isActive.setCommission(String.valueOf(commission_money));
					//应付=实际订单总额-佣金
					BigDecimal needPay_money = order_all_price.subtract(commission_money);
					hotel_settle_isActive.setNeedPay_money(String.valueOf(needPay_money));

					//已支付
					//余额

					//今天的结算时间记录
					hotel_settle_isActive.setTodayTime(new Date());
					//下线酒店添加到hotel_settle_isnotactive表中
					hotel_settle_isActiveService.insertInhotel_settle_isnotactive(hotel_settle_isActive);
				}

			}

		}
	}


	//针对酒店结算
	@Configuration      //1.主要用于标记配置类，兼备Component的效果。
	@EnableScheduling   // 2.开启定时任务
	public  class JiudianScheduleTask {
		//3.添加定时任务,每天夜里23点59分59秒执行一次
		@Scheduled(cron = "0 59 23 * * ?")
//		@Scheduled(cron = "0/5 * * * * ?")
		public  void jiudian() throws Exception {
			System.out.println("定时任务执行了==================");
			//查询所有订单下的订单id,去重获取
			HotelOrderExample hotelOrderExample1 = new HotelOrderExample();
			List<HotelOrder> hotelOrders1 = orderService.queryOrderList(hotelOrderExample1);
			System.out.println("查询的总订单数"+hotelOrders1.size());
			List<Long>hotelIdList=Lists.newArrayList();
			if (!CollectionUtils.isEmpty(hotelOrders1)){
				for (HotelOrder hotelOrder : hotelOrders1) {
					if (!hotelIdList.contains(hotelOrder.getHotelId())){
						hotelIdList.add(hotelOrder.getHotelId());
					}
				}
			}

			for (Long hotelId : hotelIdList) {
				//根据当前酒店id查询对应供应商审核通过的时间,以此为订单查询开始时间
				//根据酒店id查询对应的用户id列表(一般有两个)
				long settle_StartDateTime1=0;
				Date success_date=null;
				List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByHotelId(hotelId);
				for (HotelRefUser hotelRefUser : hotelRefUsers) {
					Long userId = hotelRefUser.getUserId();
					//查询business中是否存在用户id
					Long ifExistUserId = businessService.queryIfExistUserId(userId);
					if (ifExistUserId!=null){
						Business business = businessService.queryBusinessList(ifExistUserId);
						success_date = business.getSuccess_date();
						settle_StartDateTime1 = success_date.getTime();
					}
				}
				//当前时间
				Date date = new Date();
				long nowTime = date.getTime();
				//获取今天的前一天的日期
				String beforeDayByNowDay = beforeDayByNowDay("yyyy年MM月dd日");
				SimpleDateFormat s = new SimpleDateFormat("yyyy年MM月dd日");
				Date parse = s.parse(beforeDayByNowDay);
				long beforeDayByNowDayTime = parse.getTime();


				//根据当前酒店id查询hotel_settle_setting表获取佣金比例和周期设置
				HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
				HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
				criteria.andhotel_idEqualTo(hotelId).andhotel_supplier_stateEqualTo(1);
				HotelSupplier hotelSupplier = hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
				if (hotelSupplier!=null) {
					Integer commissionRate = hotelSupplier.getCommission_rate();
					Integer cycleSetting = 2;
					//设置结算周期:按周结算
					if (cycleSetting == 2) {
						//获取当前年的每一周:例如20200101~20200105
						//1.结算周期
						List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
						for (String settle_StartAndEnd : everyWeekOfYear) {
							JiuDian jiuDian = new JiuDian();
							BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
							BigDecimal settle_money = new BigDecimal(0);//结算金额
							BigDecimal commission_money = new BigDecimal(0);//技术服务费
							String[] split = settle_StartAndEnd.split("~");
							SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
							String settle_Start = split[0];//20200101
							Date settle_StartDate = yyyyMMdd.parse(settle_Start);
							long settle_StartDateTime = settle_StartDate.getTime();
							String settle_End = split[1];//20200105
							Date settle_EndDate = yyyyMMdd.parse(settle_End);
							long settle_endDateTime = settle_EndDate.getTime();

							//从公司审核通过开始计算时间,到今天的时间为止
							if (settle_StartDateTime1 <= settle_endDateTime && nowTime >= settle_StartDateTime) {
								HoteInfoVO hoteInfoVO = hotelService.queryHotelInfoById(hotelId);
								String hotelName = hoteInfoVO.getHotelName();
								//根据酒店id查询所有对应的订单列表,很多个
								HotelOrderExample hotelOrderExample = new HotelOrderExample();
								HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
								and.andHotelIdEqualTo(hotelId);
								List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
								System.out.println("一个酒店对应的查询的订单数" + hotelOrders.size());
								JiuDian jiuDian1 = new JiuDian();
								for (HotelOrder hotelOrder : hotelOrders) {
									jiuDian.setSettle_StartAndEnd(settle_StartAndEnd);
									jiuDian1 = jiuDianService.queryAll(settle_StartAndEnd, hotelId);
									jiuDian.setSettle_start(settle_Start);
									jiuDian.setSettle_end(settle_End);
									jiuDian.setHotel_name(hotelName);
									jiuDian.setHotel_id(hotelId);
									//入住时间
									Date checkInTime = hotelOrder.getCheckInTime();
									long checkInTimeTime = checkInTime.getTime();
									//订单状态
									Integer state = hotelOrder.getState();
									if (state == 2 && settle_StartDateTime <= checkInTimeTime && checkInTimeTime <= settle_endDateTime) {
										//价格
										BigDecimal price = hotelOrder.getCaigou_total();
										if (price!=null){
											int priceInteger = price.intValue();
											if (priceInteger>0){
												settleTotalMoney = settleTotalMoney.add(price);
												settleTotalMoney=settleTotalMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
												settle_money = settleTotalMoney.multiply(new BigDecimal((100 - commissionRate))).divide(new BigDecimal(100));
												settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
												commission_money = settleTotalMoney.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
												commission_money=commission_money.setScale(2, BigDecimal.ROUND_HALF_UP);
												//6.结算状态
												jiuDian.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
											}else {
												System.out.println("采购总价为负数");
											}
										}else {
											System.out.println("采购总价为null");
										}
									}
								}
								//结算金额
								jiuDian.setSettle_money(String.valueOf(settle_money));
								//技术服务费
								jiuDian.setCommission_money(String.valueOf(commission_money));
								if (jiuDian1 == null) {
									jiuDianService.insert(jiuDian);
								}
								else {
									System.out.println("走else了,不会添加=================");
								}
							}
						}

						//更新
						List<String> everyWeekOfYear1 = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
						for (String settle_StartAndEnd : everyWeekOfYear1) {
							JiuDian jiuDian = new JiuDian();
							BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
							BigDecimal settle_money = new BigDecimal(0);//结算金额
							BigDecimal commission_money = new BigDecimal(0);//技术服务费
							String[] split = settle_StartAndEnd.split("~");
							SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
							String settle_Start = split[0];//20200101
							Date settle_StartDate = yyyyMMdd.parse(settle_Start);
							long settle_StartDateTime = settle_StartDate.getTime();
							String settle_End = split[1];//20200105
							Date settle_EndDate = yyyyMMdd.parse(settle_End);
							long settle_endDateTime = settle_EndDate.getTime();

							//在最近的日期范围内就更新结算表,(之前的不要动,错了也不管)
							if (settle_StartDateTime <= nowTime && nowTime <= settle_endDateTime) {
								HoteInfoVO hoteInfoVO = hotelService.queryHotelInfoById(hotelId);
								String hotelName = hoteInfoVO.getHotelName();
								//根据酒店id查询所有对应的订单列表,很多个
								HotelOrderExample hotelOrderExample = new HotelOrderExample();
								HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
								and.andHotelIdEqualTo(hotelId);
								List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
								System.out.println("一个酒店对应的查询的订单数" + hotelOrders.size());
								JiuDian jiuDian1 = new JiuDian();
								for (HotelOrder hotelOrder : hotelOrders) {
									jiuDian.setSettle_StartAndEnd(settle_StartAndEnd);
									jiuDian1 = jiuDianService.queryAll(settle_StartAndEnd, hotelId);
									jiuDian.setSettle_start(settle_Start);
									jiuDian.setSettle_end(settle_End);
									jiuDian.setHotel_name(hotelName);
									jiuDian.setHotel_id(hotelId);
									//入住时间
									Date checkInTime = hotelOrder.getCheckInTime();
									long checkInTimeTime = checkInTime.getTime();
									//订单状态
									Integer state = hotelOrder.getState();
									if (state == 2 && settle_StartDateTime <= checkInTimeTime && checkInTimeTime <= settle_endDateTime) {
										//价格
										BigDecimal price = hotelOrder.getPrice();
										settleTotalMoney = settleTotalMoney.add(price);
										settleTotalMoney=settleTotalMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
										settle_money = settleTotalMoney.multiply(new BigDecimal((100 - commissionRate))).divide(new BigDecimal(100));
										settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
										commission_money = settleTotalMoney.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
										commission_money=commission_money.setScale(2, BigDecimal.ROUND_HALF_UP);
										//6.结算状态
										jiuDian.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
									}
								}
								//结算金额
								jiuDian.setSettle_money(String.valueOf(settle_money));
								//技术服务费
								jiuDian.setCommission_money(String.valueOf(commission_money));
								String settle_money1 = jiuDian.getSettle_money();
								String commission_money1 = jiuDian.getCommission_money();
								String settle_status = jiuDian.getSettle_status();
								jiuDianService.update(settle_StartAndEnd, hotelId, settle_money1, commission_money1, settle_status);
							}
						}
					}
					if (cycleSetting == 4) {
						//获取每月的月初和月末:20200101~20200131
						List<String> firstAndLastDayOfMonth = GetEveryMonthOfYearUtil.getFirstAndLastDayOfMonth();
						for (String settle_StartAndEnd : firstAndLastDayOfMonth) {
							JiuDian jiuDian = new JiuDian();
							BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
							BigDecimal settle_money = new BigDecimal(0);//结算金额
							BigDecimal commission_money = new BigDecimal(0);//技术服务费
							String[] split = settle_StartAndEnd.split("~");
							SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
							String settle_Start = split[0];//20200101
							Date settle_StartDate = yyyyMMdd.parse(settle_Start);
							long settle_StartDateTime = settle_StartDate.getTime();
							String settle_End = split[1];//20200105
							Date settle_EndDate = yyyyMMdd.parse(settle_End);
							long settle_endDateTime = settle_EndDate.getTime();

							//从公司审核通过开始计算时间,到今天的时间为止
							if (settle_StartDateTime1 <= settle_endDateTime && nowTime >= settle_StartDateTime) {
								HoteInfoVO hoteInfoVO = hotelService.queryHotelInfoById(hotelId);
								String hotelName = hoteInfoVO.getHotelName();
								//根据酒店id查询所有对应的订单列表,很多个
								HotelOrderExample hotelOrderExample = new HotelOrderExample();
								HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
								and.andHotelIdEqualTo(hotelId);
								List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
								System.out.println("一个酒店对应的查询的订单数" + hotelOrders.size());
								JiuDian jiuDian1 = new JiuDian();
								for (HotelOrder hotelOrder : hotelOrders) {
									jiuDian.setSettle_StartAndEnd(settle_StartAndEnd);
									jiuDian1 = jiuDianService.queryAll(settle_StartAndEnd, hotelId);
									jiuDian.setSettle_start(settle_Start);
									jiuDian.setSettle_end(settle_End);
									jiuDian.setHotel_name(hotelName);
									jiuDian.setHotel_id(hotelId);
									//入住时间
									Date checkInTime = hotelOrder.getCheckInTime();
									long checkInTimeTime = checkInTime.getTime();
									//订单状态
									Integer state = hotelOrder.getState();
									if (state == 2 && settle_StartDateTime <= checkInTimeTime && checkInTimeTime <= settle_endDateTime) {
										//价格
										BigDecimal price = hotelOrder.getPrice();
										settleTotalMoney = settleTotalMoney.add(price);
										settleTotalMoney=settleTotalMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
										settle_money = settleTotalMoney.multiply(new BigDecimal((100 - commissionRate))).divide(new BigDecimal(100));
										settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
										commission_money = settleTotalMoney.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
										commission_money=commission_money.setScale(2, BigDecimal.ROUND_HALF_UP);
										//6.结算状态
										jiuDian.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
									}
								}
								//结算金额
								jiuDian.setSettle_money(String.valueOf(settle_money));
								//技术服务费
								jiuDian.setCommission_money(String.valueOf(commission_money));
								if (jiuDian1 == null) {
									jiuDianService.insert(jiuDian);
								}
								else {
									System.out.println("走else了,不会添加=================");
								}
							}
						}

						//更新
						List<String> firstAndLastDayOfMonth1 = GetEveryMonthOfYearUtil.getFirstAndLastDayOfMonth();
						for (String settle_StartAndEnd : firstAndLastDayOfMonth1) {
							JiuDian jiuDian = new JiuDian();
							BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
							BigDecimal settle_money = new BigDecimal(0);//结算金额
							BigDecimal commission_money = new BigDecimal(0);//技术服务费
							String[] split = settle_StartAndEnd.split("~");
							SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
							String settle_Start = split[0];//20200101
							Date settle_StartDate = yyyyMMdd.parse(settle_Start);
							long settle_StartDateTime = settle_StartDate.getTime();
							String settle_End = split[1];//20200105
							Date settle_EndDate = yyyyMMdd.parse(settle_End);
							long settle_endDateTime = settle_EndDate.getTime();

							//在最近的日期范围内就更新结算表,(之前的不要动,错了也不管)
							if (settle_StartDateTime <= nowTime && nowTime <= settle_endDateTime) {
								HoteInfoVO hoteInfoVO = hotelService.queryHotelInfoById(hotelId);
								String hotelName = hoteInfoVO.getHotelName();
								//根据酒店id查询所有对应的订单列表,很多个
								HotelOrderExample hotelOrderExample = new HotelOrderExample();
								HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
								and.andHotelIdEqualTo(hotelId);
								List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
								System.out.println("一个酒店对应的查询的订单数" + hotelOrders.size());
								JiuDian jiuDian1 = new JiuDian();
								for (HotelOrder hotelOrder : hotelOrders) {
									jiuDian.setSettle_StartAndEnd(settle_StartAndEnd);
									jiuDian1 = jiuDianService.queryAll(settle_StartAndEnd, hotelId);
									jiuDian.setSettle_start(settle_Start);
									jiuDian.setSettle_end(settle_End);
									jiuDian.setHotel_name(hotelName);
									jiuDian.setHotel_id(hotelId);
									//入住时间
									Date checkInTime = hotelOrder.getCheckInTime();
									long checkInTimeTime = checkInTime.getTime();
									//订单状态
									Integer state = hotelOrder.getState();
									if (state == 2 && settle_StartDateTime <= checkInTimeTime && checkInTimeTime <= settle_endDateTime) {
										//价格
										BigDecimal price = hotelOrder.getPrice();
										settleTotalMoney = settleTotalMoney.add(price);
										settleTotalMoney=settleTotalMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
										settle_money = settleTotalMoney.multiply(new BigDecimal((100 - commissionRate))).divide(new BigDecimal(100));
										settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
										commission_money = settleTotalMoney.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
										commission_money=commission_money.setScale(2, BigDecimal.ROUND_HALF_UP);
										//6.结算状态
										jiuDian.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
									}
								}
								//结算金额
								jiuDian.setSettle_money(String.valueOf(settle_money));
								//技术服务费
								jiuDian.setCommission_money(String.valueOf(commission_money));
								String settle_money1 = jiuDian.getSettle_money();
								String commission_money1 = jiuDian.getCommission_money();
								String settle_status = jiuDian.getSettle_status();
								jiuDianService.update(settle_StartAndEnd, hotelId, settle_money1, commission_money1, settle_status);
							}
						}

					}
				}else {
					System.out.println("酒店没有设置佣金比例和周期");
				}
			}
			System.out.println("=======================任务结束");
		}

		//针对供应商结算
		@Scheduled(cron = "0 57 23 * * ?")
//		@Scheduled(cron = "0/20 * * * * ?")
		public void gongyingshang()throws Exception{
			System.out.println("[供应商]定时任务执行了==================");
			List<Business> businessList = businessService.queryBusinessInsertList();
			List<Long>businessList_UserIds=Lists.newArrayList();
			if (!CollectionUtils.isEmpty(businessList)){
				for (Business business : businessList) {
					if (!businessList_UserIds.contains(business.getUser_id())){
						businessList_UserIds.add(business.getUser_id());
					}
				}
			}

			for (Long currentUserId : businessList_UserIds) {
				Business business = businessService.queryBusinessList(currentUserId);
				//供应商id,只有一个
				Long business_id = business.getBusiness_id();
				//根据供应商id查询用户id,只有一个
				Long user_id = business.getUser_id();

				//根据当前酒店id查询对应供应商审核通过的时间,以此为订单查询开始时间
				//审核不通过的公司或者正在审核的公司是没有时间的,排出这个bug
				if (business.getSuccess_date()!=null){
					Date success_date = business.getSuccess_date();
					long settle_StartDateTime1 = success_date.getTime();
					//当前时间
					Date date = new Date();
					long nowTime = date.getTime();
					//计算佣金率
					Integer count=0;
					Integer yongjinglv=1;
					//根据当前用户id查询自己的邀请码
					String code_my = userCodeMapper.queryCode_my(user_id);
					//根据我的邀请码查询下家的邀请码所对应的用户id
					List<Long> userIds = userCodeMapper.queryCode_xiajia(code_my);
					for (Long userId : userIds) {
						//根据成员id查询邀请码id
						Long code_id = userCodeMapper.queryCode_id(userId);
						//根据邀请码id查询code_level的状态
						String code_level = userCodeMapper.queryCode_level(code_id);
						if (code_level.contains("已通过")){
							count++;
						}
					}
					//上海苜宿酒店管理有限公司供应商的结算按照13%计算,这里设死,根据邀请码(唯一)去定位供应商,不能根据公司id,因为这里的business_id有可能是user_id
					if(code_my.contains("7ea21c")){
						yongjinglv=13;
					}else {
						if(count<=15){
							yongjinglv=15;
						}
						if(count>=16&&count<=30){
							yongjinglv=14;
						}
						if(count>=31&&count<=50){
							yongjinglv=13;
						}
						if(count>=51){
							yongjinglv=12;
						}
					}

					//获取当前年的每一周:例如20200101~20200105
					//1.结算周期
					List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
					for (String settle_StartAndEnd : everyWeekOfYear) {
						//4.结算金额=总金额*xx%
						BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
						BigDecimal settle_money = new BigDecimal(0);//结算金额
						BigDecimal commission_money = new BigDecimal(0);//技术服务费
						String[] split = settle_StartAndEnd.split("~");
						SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
						String settle_Start = split[0];//20200101
						Date settle_StartDate = yyyyMMdd.parse(settle_Start);
						long settle_StartDateTime = settle_StartDate.getTime();
						String settle_End = split[1];//20200105
						Date settle_EndDate = yyyyMMdd.parse(settle_End);
						long settle_endDateTime = settle_EndDate.getTime();

						//从公司审核通过开始计算时间,到今天的时间为止
						if (settle_StartDateTime1<=settle_endDateTime&&nowTime>=settle_StartDateTime){
							GongYingShang gongYingShang1 = new GongYingShang();
							//根据周期和酒店id查询供应商信息(添加之前先判断是否存在,存在就不做处理)
							gongYingShang1=gongYingShangService.queryAll(settle_StartAndEnd, business_id);

							//计算结算过的总金额
							//根据用户id查询名下的酒店id,很多个
							List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
							if (!CollectionUtils.isEmpty(hotelRefUsers)){
								GongYingShang gongYingShang = new GongYingShang();
								gongYingShang.setSettle_StartAndEnd(settle_StartAndEnd);
								//2.供应商名称
								gongYingShang.setBusiness_name(business.getBusiness_name());
								//3.供应商id
								gongYingShang.setBusiness_id(business_id);
								gongYingShang.setSettle_start(settle_Start);
								gongYingShang.setSettle_end(settle_End);

								for (HotelRefUser hotelRefUser : hotelRefUsers){
									Long hotelId = hotelRefUser.getHotelId();
									//根据酒店id查询所有对应的订单列表,很多个
									HotelOrderExample hotelOrderExample = new HotelOrderExample();
									HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
									and.andHotelIdEqualTo(hotelId);
									List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
									if (!CollectionUtils.isEmpty(hotelOrders)){
										for (HotelOrder hotelOrder : hotelOrders) {
											//入住时间
											Date checkInTime = hotelOrder.getCheckInTime();
											long checkInTimeTime = checkInTime.getTime();
											//订单状态
											Integer state = hotelOrder.getState();
											//订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
											if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime){
												//价格
												BigDecimal price = hotelOrder.getPrice();
												settleTotalMoney = settleTotalMoney.add(price);
												settleTotalMoney=settleTotalMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
												settle_money=settleTotalMoney.multiply(new BigDecimal((100-yongjinglv))).divide(new BigDecimal(100));
												settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
												commission_money=settleTotalMoney.multiply(new BigDecimal(yongjinglv)).divide(new BigDecimal(100));
												commission_money=commission_money.setScale(2, BigDecimal.ROUND_HALF_UP);
											}
											//6.结算状态
											gongYingShang.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
										}
										//结算金额就是下面的
										gongYingShang.setSettle_money(String.valueOf(settle_money));
										//5.技术服务费=总金额*xx%
										gongYingShang.setCommission_money(String.valueOf(commission_money));
									}
								}
								//添加到gongyingshang结算表中
								if (gongYingShang1==null){
									gongYingShangService.insert(gongYingShang);
								}
								else {
									System.out.println("走else了,不会添加=================");
								}
							}
						}
					}



					//更新
					//遍历每一周,当前日期在那一周期间的话,就进行更新操作
					List<String> everyWeekOfYear1 = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
					for (String settle_StartAndEnd : everyWeekOfYear1) {
						//结算金额=总金额*xx%
						BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
						BigDecimal settle_money = new BigDecimal(0);//结算金额
						BigDecimal commission_money = new BigDecimal(0);//技术服务费
						String[] split = settle_StartAndEnd.split("~");
						SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
						String settle_Start = split[0];//20200101
						Date settle_StartDate = yyyyMMdd.parse(settle_Start);
						long settle_StartDateTime = settle_StartDate.getTime();
						String settle_End = split[1];//20200105
						Date settle_EndDate = yyyyMMdd.parse(settle_End);
						long settle_endDateTime = settle_EndDate.getTime();

						//在最近的日期范围内就更新结算表,(之前的不要动,错了也不管)
						if (settle_StartDateTime<=nowTime&&nowTime<=settle_endDateTime){
							//计算结算过的总金额
							//根据用户id查询名下的酒店id,很多个
							List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
							if (!CollectionUtils.isEmpty(hotelRefUsers)){
								GongYingShang gongYingShang = new GongYingShang();
								for (HotelRefUser hotelRefUser : hotelRefUsers){
									Long hotelId = hotelRefUser.getHotelId();
									//根据酒店id查询所有对应的订单列表,很多个
									HotelOrderExample hotelOrderExample = new HotelOrderExample();
									HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
									and.andHotelIdEqualTo(hotelId);
									List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
									if (!CollectionUtils.isEmpty(hotelOrders)){
										for (HotelOrder hotelOrder : hotelOrders) {
											//入住时间
											Date checkInTime = hotelOrder.getCheckInTime();
											long checkInTimeTime = checkInTime.getTime();
											//订单状态
											Integer state = hotelOrder.getState();
											//订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
											if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime){
												//价格
												BigDecimal price = hotelOrder.getPrice();
												settleTotalMoney = settleTotalMoney.add(price);
												settleTotalMoney=settleTotalMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
												settle_money=settleTotalMoney.multiply(new BigDecimal((100-yongjinglv))).divide(new BigDecimal(100));
												settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
												commission_money=settleTotalMoney.multiply(new BigDecimal(yongjinglv)).divide(new BigDecimal(100));
												commission_money=commission_money.setScale(2, BigDecimal.ROUND_HALF_UP);
											}
											//6.结算状态
											gongYingShang.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
										}
										//结算金额就是下面的
										gongYingShang.setSettle_money(String.valueOf(settle_money));
										//5.技术服务费=总金额*xx%
										gongYingShang.setCommission_money(String.valueOf(commission_money));
									}
								}

								String settle_money1 = gongYingShang.getSettle_money();
								String commission_money1 = gongYingShang.getCommission_money();
								String settle_status = gongYingShang.getSettle_status();
								gongYingShangService.update(settle_StartAndEnd,business_id,settle_money1,commission_money1,settle_status);
							}
						}
					}
				}
			}
			System.out.println("=======================[供应商]任务结束");
		}
	}




	/**
	 * 获取今天的前一天日期
	 * @param pattern 需要返回的日期格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String beforeDayByNowDay(String pattern){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1); //得到前一天
		Date date = calendar.getTime();
		DateFormat df = new SimpleDateFormat(pattern);
		return df.format(date);
	}
	//获取间夜数
	private long getDays(Date startDate, Date endDate) {
		if (startDate != null && endDate != null) {
			SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = null;
			Date mydate = null;

			try {
				mydate = myFormatter.parse(myFormatter.format(startDate));
				date = myFormatter.parse(myFormatter.format(endDate));
			} catch (Exception var7) {
			}

			long day = (date.getTime() - mydate.getTime()) / 86400000L;
			return day;
		} else {
			return 0L;
		}
	}
}
