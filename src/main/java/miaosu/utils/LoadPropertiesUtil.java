package miaosu.utils;


import miaosu.sms.SmsInstance;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadPropertiesUtil {
    public static Properties loadProperties(String name){
        Properties props = new Properties();
        InputStream in = SmsInstance.class.getResourceAsStream("/"+name);
        try {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }
}
