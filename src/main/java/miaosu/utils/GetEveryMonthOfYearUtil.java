package miaosu.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/8/20 10:19
 * @Description: 获取当前年的每月的月初和月末的日期
 */
public class GetEveryMonthOfYearUtil {

	public static List<String> getFirstAndLastDayOfMonth(){
		Integer time[] = {1,2,3,4,5,6,7,8,9,10,11,12};
		List list=new ArrayList();
		String  firstAndLastDayOfMonth=null;
		for (Integer integer : time) {
			String firstDayOfMonth = getFirstDayOfMonth(integer);
			String lastDayOfMonth = getLastDayOfMonth(integer);
			firstAndLastDayOfMonth=firstDayOfMonth+"~"+lastDayOfMonth;
			list.add(firstAndLastDayOfMonth);
		}
		return list;
	}

	public static String getFirstDayOfMonth(int month) {
		Calendar cal = Calendar.getInstance();
		// 设置月份
		cal.set(Calendar.MONTH, month - 1);
		// 获取某月最小天数
		int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, firstDay);
		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		String firstDayOfMonth = sdf.format(cal.getTime());
		return firstDayOfMonth;
	}
	/**
	 * 获得该月最后一天
	 *
	 * @param
	 * @param month
	 * @return
	 */
	public static String getLastDayOfMonth(int month) {
		Calendar cal = Calendar.getInstance();
		// 设置月份
		cal.set(Calendar.MONTH, month - 1);
		// 获取某月最大天数
		int lastDay=0;
		//2月的平年瑞年天数
		if(month==2) {
			lastDay = cal.getLeastMaximum(Calendar.DAY_OF_MONTH);
		}else {
			lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		// 设置日历中月份的最大天数
		cal.set(Calendar.DAY_OF_MONTH, lastDay);
		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		String lastDayOfMonth = sdf.format(cal.getTime());
		return lastDayOfMonth;
	}
}
