package miaosu.utils;

import miaosu.common.RoomTypeEnum;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Administrator on 2018/6/20.
 */

/**
 * 匹配房型工具类
 *
 * @author zyx
 * @date 2018/06/22
 * */
public class StringUtil  {

    private final static HashMap<String,Object> typeMap = new HashMap<String, Object>();

    public StringUtil(){
        typeMap.put("标准间", RoomTypeEnum.STANDARD_TWIN_ROOM.getCode());
        typeMap.put("标间", RoomTypeEnum.STANDARD_TWIN_ROOM.getCode());
        typeMap.put("双床房", RoomTypeEnum.STANDARD_TWIN_ROOM.getCode());
        typeMap.put("双人床", RoomTypeEnum.STANDARD_TWIN_ROOM.getCode());
        typeMap.put("标准房", RoomTypeEnum.STANDARD_TWIN_ROOM.getCode());
        typeMap.put("大床房",RoomTypeEnum.STANDARD_BIG_ROOM.getCode());
        typeMap.put("深夜特惠房",RoomTypeEnum.STANDARD_BIG_ROOM.getCode());
        typeMap.put("床位房",RoomTypeEnum.BED_ROOM.getCode());
 /*       typeMap.put("标间",RoomTypeEnum.SINGLE_ROOM.getCode());*/
        typeMap.put("单间",RoomTypeEnum.SINGLE_ROOM.getCode());
        typeMap.put("家庭房",RoomTypeEnum.FAMILY_ROOM.getCode());
        typeMap.put("三人间",RoomTypeEnum.TRIPLE_ROOM.getCode());
        typeMap.put("套房",RoomTypeEnum.SUITE_ROOM.getCode());
        /*typeMap.put("其他",RoomTypeEnum.OTHER_ROOM);*/

    }

    public long matchedHous(String hotelRoomTypeName) throws Exception{

        Iterator it = typeMap.entrySet().iterator();

        if(hotelRoomTypeName.equals("标间")){
            return RoomTypeEnum. SINGLE_ROOM.getCode();
        }else if(hotelRoomTypeName.equals("文艺套房")){
            return  RoomTypeEnum.OTHER_ROOM.getCode();
        }else{
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                if(hotelRoomTypeName.indexOf(key.toString())!=-1){
                    return (Long)entry.getValue();
                }
            }
        }
       return  RoomTypeEnum.OTHER_ROOM.getCode();
    }


    public static DateFormat getFormat(String patten){
        DateFormat format= new SimpleDateFormat(patten);
        return format;
    }

    public static Date formatDateStr(String dateStr,String patten) throws ParseException {
        return  StringUtil.getFormat(patten).parse(dateStr);
    }
}
