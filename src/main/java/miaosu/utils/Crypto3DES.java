package miaosu.utils;

import com.vpiaotong.openapi.exception.ExceptionHandler;
import com.vpiaotong.openapi.util.Base64Util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;

public class Crypto3DES {
    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    public static String decrypt3DES(String decryptPassword, String decryptString,Integer cipherMode) {
        try {
            Cipher cipher = init3DES(decryptPassword, cipherMode);
            byte[] deBytes = cipher.doFinal(Base64Util.decode2Byte(decryptString));
            return new String(deBytes, DEFAULT_CHARSET);
        } catch (Exception var4) {
            ExceptionHandler.castException(var4);
            return null;
        }
    }

    private static Cipher init3DES(String decryptPassword, int cipherMode) throws Exception {
        SecretKey deskey = new SecretKeySpec(decryptPassword.getBytes(), "DESede");
        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(cipherMode, deskey);
        return cipher;
    }
}
