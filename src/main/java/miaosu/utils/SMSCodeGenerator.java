package miaosu.utils;

import miaosu.sms.SmsUtil;

import java.util.Random;

/**
 * 短信验证码生成工具
 */
public class SMSCodeGenerator {

    /**
     * 生成4位验证码
     * @return
     */
    public static String sms4Place(){
         return smsCode(4);
    }

    /**
     * 生成6位验证码
     * @return
     */
    public static String sms6Place(){
        return smsCode(6);
    }


    private static String smsCode(Integer num){
        StringBuilder codeBuidler = new StringBuilder();
        Random random = new Random();
        for(int i = 0;i<num;i++){
            Integer code = random.nextInt(10);
            codeBuidler.append(code);
        }


        return codeBuidler.toString();
    }
}
