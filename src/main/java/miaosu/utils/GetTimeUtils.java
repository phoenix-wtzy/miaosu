package miaosu.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Administrator
 * @Date: 2020/9/27 10:55
 * @Description:
 */
public class GetTimeUtils {
	/***
	 * 获得当前系统时间（字符串格式精确到秒）
	 * @return
	 */
	public static String getStringTodayTime(){
		Date todat_date = new Date();
		//将日期格式化
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//转换成字符串格式
		return simpleDateFormat.format(todat_date);
	}
	/**
	 * 随时间的随机数生成
	 */
	public static String nowTimeRandomDate(){
		Date date = new Date();
		SimpleDateFormat sdf =new SimpleDateFormat("YYYYMMddHHmmss");
		String str=sdf.format(date)+(int)(Math.random()*90+10);
		return str;
	}
}
