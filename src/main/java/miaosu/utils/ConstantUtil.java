package miaosu.utils;

/**
 * Created by Administrator on 2018/6/22.
 */

/**
 * 一些错误提示
 *
 * @author zyx
 * @date 2018/06/22
 * */
public class ConstantUtil {

    public final static String UPLOADFILE_FORMATERROR_MESSAGE = "格式错误！请选择后缀为.xls或.xlsx的文件！";
    public final static String UPLOADFILE_EMPTYERROR_MESSAGE= "文件为空！";
    public final static String UPLOADFILE_NOFINDHOTELERROR_MESSAGE= "查无此酒店！";
    public final static String UPLOADFILE_DATEFORMATERROR_MESSAGE= "数据异常！";
    public final static String UPLOADFILE_SUCCESSS_MESSAGE = "导入成功！";
    public final static String UPLOADFILE_FAILD_MESSAGE = "导入失败！";

}
