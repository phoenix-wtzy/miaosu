package miaosu.utils;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.*;

/**
 * @author Administrator
 * @Date: 2020/8/7 16:00
 * @Description: 获取当前年的每一周
 */
public class GetEveryWeekOfYearUtil {

	//获取结算开始时间,例如:20200101 和 获取结算结束时间,例如:20200105
	public static Map<String,List<String>> getSettle_StartAndEnd() {
		Map<String,List<String>>map=new HashMap<>();
		List settle_startList=new ArrayList();
		List settle_endList=new ArrayList();
		String days=null;
		String currentYearStr = getCurrentYear();//当前年
		int currentYear = Integer.parseInt(currentYearStr);
		//初始化，第一周至少四天,周天数占比
		WeekFields wfs = WeekFields.of(DayOfWeek.MONDAY, 4);
		//一年最后一天日期的LocalDate，如果该天获得的周数为1或52，那么该年就只有52周，否则就是53周
		//获取指定时间所在年的周数
		for (int y = 0; y < 10; y++) {//y控制年限
			int num = LocalDate.of(currentYear + y, 12, 31).get(wfs.weekOfWeekBasedYear());
			num = num == 1 ? 52 : num;
			for (int i = 1; i <= num; i++) {//num控制周数
				days = getDay(currentYear + y, i, DayOfWeek.MONDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.TUESDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.WEDNESDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.THURSDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.FRIDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.SATURDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.SUNDAY);
				String[] split = trim(days, ",").split(",");
				days = currentYear+y+split[0] ;
				String settle_end=currentYear+y+split[split.length - 1];
				settle_startList.add(days);
				settle_endList.add(settle_end);
			}

			map.put("settle_start", settle_startList);
			map.put("settle_end",settle_endList );
		}
		return map;
	}

	//获取每一周:例如20200101~20200105
	public static List<String> getEveryWeekOfYear() {
		List<String>list=new ArrayList<>();
		String days=null;
		String currentYearStr = getCurrentYear();//当前年
		int currentYear = Integer.parseInt(currentYearStr);
		//初始化，第一周至少四天,周天数占比
		WeekFields wfs = WeekFields.of(DayOfWeek.MONDAY, 4);
		//一年最后一天日期的LocalDate，如果该天获得的周数为1或52，那么该年就只有52周，否则就是53周
		//获取指定时间所在年的周数
		for (int y = 0; y < 10; y++) {//y控制年限
			int num = LocalDate.of(currentYear + y, 12, 31).get(wfs.weekOfWeekBasedYear());
			num = num == 1 ? 52 : num;
			for (int i = 1; i <= num; i++) {//num控制周数
				 days = getDay(currentYear + y, i, DayOfWeek.MONDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.TUESDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.WEDNESDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.THURSDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.FRIDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.SATURDAY) + "," +
						getDay(currentYear + y, i, DayOfWeek.SUNDAY);
				String[] split = trim(days, ",").split(",");
				int i1 = currentYear + y;
				days = i1+"年"+split[0] + "~" + i1+"年"+split[split.length - 1];
				list.add(days);
			}
		}
		return list;
	}

	private static String getDay(Integer year, Integer num, DayOfWeek dayOfWeek) {
		//周数小于10在前面补个0
		String numStr = num < 10 ? "0" + String.valueOf(num) : String.valueOf(num);
		//2019-W01-01获取第一周的周一日期，2019-W02-07获取第二周的周日日期
		String weekDate = String.format("%s-W%s-%s", year, numStr, dayOfWeek.getValue());
		String date = LocalDate.parse(weekDate, DateTimeFormatter.ISO_WEEK_DATE).toString();
		String[] split = date.split("-");
		if (!split[0].equals(year.toString())) {
			//返回日期范围属于指定年
			return "";
		}
		date = split[1] +"月" + split[2]+"日";
		return date;
	}

	/**
	 * 去除首尾指定字符
	 *
	 * @param str     字符串
	 * @param element 指定字符
	 * @return
	 */
	public static String trim(String str, String element) {
		if (str == null || str.equals("")) return str;
		boolean beginIndexFlag = true;
		boolean endIndexFlag = true;
		do {
			int beginIndex = str.indexOf(element) == 0 ? 1 : 0;
			int endIndex = str.lastIndexOf(element) + 1 == str.length() ? str.lastIndexOf(element) : str.length();
			str = str.substring(beginIndex, endIndex);
			beginIndexFlag = (str.indexOf(element) == 0);
			endIndexFlag = (str.lastIndexOf(element) + 1 == str.length());
		} while (beginIndexFlag || endIndexFlag);
		return str;
	}
	//获取当前年
	public static String getCurrentYear() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		Date date = new Date();
		return sdf.format(date);
	}
}
