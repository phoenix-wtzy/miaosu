/**
  * Copyright 2019 bejson.com 
  */
package miaosu.invoice;
import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2019-01-30 14:13:51
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class InvoiceInfoResultBean {

    private String invoiceReqSerialNo;
    private String code;
    private String msg;
    private String buyerName;
    private String buyerTaxpayerNum;
    private String buyerAddress;
    private String buyerTel;
    private String buyerBankName;
    private String buyerBankAccount;
    private String sellerTaxpayerNum;
    private String sellerName;
    private String sellerAddress;
    private String sellerTel;
    private String sellerBankAccount;
    private String invoiceKind;
    private String specialInvoiceKind;
    private Date invoiceDate;
    private String invoiceCode;
    private String invoiceNo;
    private String invoiceType;
    private String blueInvoiceCode;
    private String blueInvoiceNo;
    private String noTaxAmount;
    private String taxAmount;
    private String amountWithTax;
    private String amountWithTaxInWords;
    private String invalidFlag;
    private String redFlag;
    private String casherName;
    private String reviewerName;
    private String drawerName;
    private String checkCode;
    private String detailedListFlag;
    private String detailedListItemName;
    private String qrCode;
    private String remark;
    private String diskNo;
    private String diskType;
    private String extensionNum;
    private String tradeNo;
    private String invoicePdf;
    private String downloadUrl;
    private List<ItemList> itemList;
    public void setInvoiceReqSerialNo(String invoiceReqSerialNo) {
         this.invoiceReqSerialNo = invoiceReqSerialNo;
     }
     public String getInvoiceReqSerialNo() {
         return invoiceReqSerialNo;
     }

    public void setCode(String code) {
         this.code = code;
     }
     public String getCode() {
         return code;
     }

    public void setMsg(String msg) {
         this.msg = msg;
     }
     public String getMsg() {
         return msg;
     }

    public void setBuyerName(String buyerName) {
         this.buyerName = buyerName;
     }
     public String getBuyerName() {
         return buyerName;
     }

    public void setBuyerTaxpayerNum(String buyerTaxpayerNum) {
         this.buyerTaxpayerNum = buyerTaxpayerNum;
     }
     public String getBuyerTaxpayerNum() {
         return buyerTaxpayerNum;
     }

    public void setBuyerAddress(String buyerAddress) {
         this.buyerAddress = buyerAddress;
     }
     public String getBuyerAddress() {
         return buyerAddress;
     }

    public void setBuyerTel(String buyerTel) {
         this.buyerTel = buyerTel;
     }
     public String getBuyerTel() {
         return buyerTel;
     }

    public void setBuyerBankName(String buyerBankName) {
         this.buyerBankName = buyerBankName;
     }
     public String getBuyerBankName() {
         return buyerBankName;
     }

    public void setBuyerBankAccount(String buyerBankAccount) {
         this.buyerBankAccount = buyerBankAccount;
     }
     public String getBuyerBankAccount() {
         return buyerBankAccount;
     }

    public void setSellerTaxpayerNum(String sellerTaxpayerNum) {
         this.sellerTaxpayerNum = sellerTaxpayerNum;
     }
     public String getSellerTaxpayerNum() {
         return sellerTaxpayerNum;
     }

    public void setSellerName(String sellerName) {
         this.sellerName = sellerName;
     }
     public String getSellerName() {
         return sellerName;
     }

    public void setSellerAddress(String sellerAddress) {
         this.sellerAddress = sellerAddress;
     }
     public String getSellerAddress() {
         return sellerAddress;
     }

    public void setSellerTel(String sellerTel) {
         this.sellerTel = sellerTel;
     }
     public String getSellerTel() {
         return sellerTel;
     }

    public void setSellerBankAccount(String sellerBankAccount) {
         this.sellerBankAccount = sellerBankAccount;
     }
     public String getSellerBankAccount() {
         return sellerBankAccount;
     }

    public void setInvoiceKind(String invoiceKind) {
         this.invoiceKind = invoiceKind;
     }
     public String getInvoiceKind() {
         return invoiceKind;
     }

    public void setSpecialInvoiceKind(String specialInvoiceKind) {
         this.specialInvoiceKind = specialInvoiceKind;
     }
     public String getSpecialInvoiceKind() {
         return specialInvoiceKind;
     }

    public void setInvoiceDate(Date invoiceDate) {
         this.invoiceDate = invoiceDate;
     }
     public Date getInvoiceDate() {
         return invoiceDate;
     }

    public void setInvoiceCode(String invoiceCode) {
         this.invoiceCode = invoiceCode;
     }
     public String getInvoiceCode() {
         return invoiceCode;
     }

    public void setInvoiceNo(String invoiceNo) {
         this.invoiceNo = invoiceNo;
     }
     public String getInvoiceNo() {
         return invoiceNo;
     }

    public void setInvoiceType(String invoiceType) {
         this.invoiceType = invoiceType;
     }
     public String getInvoiceType() {
         return invoiceType;
     }

    public void setBlueInvoiceCode(String blueInvoiceCode) {
         this.blueInvoiceCode = blueInvoiceCode;
     }
     public String getBlueInvoiceCode() {
         return blueInvoiceCode;
     }

    public void setBlueInvoiceNo(String blueInvoiceNo) {
         this.blueInvoiceNo = blueInvoiceNo;
     }
     public String getBlueInvoiceNo() {
         return blueInvoiceNo;
     }

    public void setNoTaxAmount(String noTaxAmount) {
         this.noTaxAmount = noTaxAmount;
     }
     public String getNoTaxAmount() {
         return noTaxAmount;
     }

    public void setTaxAmount(String taxAmount) {
         this.taxAmount = taxAmount;
     }
     public String getTaxAmount() {
         return taxAmount;
     }

    public void setAmountWithTax(String amountWithTax) {
         this.amountWithTax = amountWithTax;
     }
     public String getAmountWithTax() {
         return amountWithTax;
     }

    public void setAmountWithTaxInWords(String amountWithTaxInWords) {
         this.amountWithTaxInWords = amountWithTaxInWords;
     }
     public String getAmountWithTaxInWords() {
         return amountWithTaxInWords;
     }

    public void setInvalidFlag(String invalidFlag) {
         this.invalidFlag = invalidFlag;
     }
     public String getInvalidFlag() {
         return invalidFlag;
     }

    public void setRedFlag(String redFlag) {
         this.redFlag = redFlag;
     }
     public String getRedFlag() {
         return redFlag;
     }

    public void setCasherName(String casherName) {
         this.casherName = casherName;
     }
     public String getCasherName() {
         return casherName;
     }

    public void setReviewerName(String reviewerName) {
         this.reviewerName = reviewerName;
     }
     public String getReviewerName() {
         return reviewerName;
     }

    public void setDrawerName(String drawerName) {
         this.drawerName = drawerName;
     }
     public String getDrawerName() {
         return drawerName;
     }

    public void setCheckCode(String checkCode) {
         this.checkCode = checkCode;
     }
     public String getCheckCode() {
         return checkCode;
     }

    public void setDetailedListFlag(String detailedListFlag) {
         this.detailedListFlag = detailedListFlag;
     }
     public String getDetailedListFlag() {
         return detailedListFlag;
     }

    public void setDetailedListItemName(String detailedListItemName) {
         this.detailedListItemName = detailedListItemName;
     }
     public String getDetailedListItemName() {
         return detailedListItemName;
     }

    public void setQrCode(String qrCode) {
         this.qrCode = qrCode;
     }
     public String getQrCode() {
         return qrCode;
     }

    public void setRemark(String remark) {
         this.remark = remark;
     }
     public String getRemark() {
         return remark;
     }

    public void setDiskNo(String diskNo) {
         this.diskNo = diskNo;
     }
     public String getDiskNo() {
         return diskNo;
     }

    public void setDiskType(String diskType) {
         this.diskType = diskType;
     }
     public String getDiskType() {
         return diskType;
     }

    public void setExtensionNum(String extensionNum) {
         this.extensionNum = extensionNum;
     }
     public String getExtensionNum() {
         return extensionNum;
     }

    public void setTradeNo(String tradeNo) {
         this.tradeNo = tradeNo;
     }
     public String getTradeNo() {
         return tradeNo;
     }

    public void setInvoicePdf(String invoicePdf) {
         this.invoicePdf = invoicePdf;
     }
     public String getInvoicePdf() {
         return invoicePdf;
     }

    public void setDownloadUrl(String downloadUrl) {
         this.downloadUrl = downloadUrl;
     }
     public String getDownloadUrl() {
         return downloadUrl;
     }

    public void setItemList(List<ItemList> itemList) {
         this.itemList = itemList;
     }
     public List<ItemList> getItemList() {
         return itemList;
     }

}