/**
  * Copyright 2019 bejson.com 
  */
package miaosu.invoice;

/**
 * Auto-generated: 2019-01-30 14:13:51
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ItemList {

    private String goodsName;
    private String taxClassificationCode;
    private String meteringUnit;
    private String quantity;
    private String unitPrice;
    private String itemAmount;
    private String taxRate;
    private String taxRateAmount;
    private String preferentialPolicyFlag;
    private String itemProperty;
    private String itemNo;
    public void setGoodsName(String goodsName) {
         this.goodsName = goodsName;
     }
     public String getGoodsName() {
         return goodsName;
     }

    public void setTaxClassificationCode(String taxClassificationCode) {
         this.taxClassificationCode = taxClassificationCode;
     }
     public String getTaxClassificationCode() {
         return taxClassificationCode;
     }

    public void setMeteringUnit(String meteringUnit) {
         this.meteringUnit = meteringUnit;
     }
     public String getMeteringUnit() {
         return meteringUnit;
     }

    public void setQuantity(String quantity) {
         this.quantity = quantity;
     }
     public String getQuantity() {
         return quantity;
     }

    public void setUnitPrice(String unitPrice) {
         this.unitPrice = unitPrice;
     }
     public String getUnitPrice() {
         return unitPrice;
     }

    public void setItemAmount(String itemAmount) {
         this.itemAmount = itemAmount;
     }
     public String getItemAmount() {
         return itemAmount;
     }

    public void setTaxRate(String taxRate) {
         this.taxRate = taxRate;
     }
     public String getTaxRate() {
         return taxRate;
     }

    public void setTaxRateAmount(String taxRateAmount) {
         this.taxRateAmount = taxRateAmount;
     }
     public String getTaxRateAmount() {
         return taxRateAmount;
     }

    public void setPreferentialPolicyFlag(String preferentialPolicyFlag) {
         this.preferentialPolicyFlag = preferentialPolicyFlag;
     }
     public String getPreferentialPolicyFlag() {
         return preferentialPolicyFlag;
     }

    public void setItemProperty(String itemProperty) {
         this.itemProperty = itemProperty;
     }
     public String getItemProperty() {
         return itemProperty;
     }

    public void setItemNo(String itemNo) {
         this.itemNo = itemNo;
     }
     public String getItemNo() {
         return itemNo;
     }

}