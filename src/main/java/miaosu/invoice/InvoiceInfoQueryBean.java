package miaosu.invoice;

public class InvoiceInfoQueryBean {
    /**
     * 开票方的纳税人识别号
     */
    private String taxpayerNum;
    /**
     * 发票请求流水号
     */
    private String invoiceReqSerialNo;
    public void setTaxpayerNum(String taxpayerNum) {
        this.taxpayerNum = taxpayerNum;
    }
    public String getTaxpayerNum() {
        return taxpayerNum;
    }

    public void setInvoiceReqSerialNo(String invoiceReqSerialNo) {
        this.invoiceReqSerialNo = invoiceReqSerialNo;
    }
    public String getInvoiceReqSerialNo() {
        return invoiceReqSerialNo;
    }

}
