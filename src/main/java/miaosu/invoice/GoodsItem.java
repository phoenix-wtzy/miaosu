package miaosu.invoice;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GoodsItem {
    /**
     * 货物名称
     */
    private String goodsName;
    /**
     * 对应税收分类编码
     */
    private String taxClassificationCode;

    /**
     * 单位
     */
    private String meteringUnit;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 单价
     */
    private BigDecimal unitPrice;

    /**
     * 金额
     */
    private BigDecimal invoiceAmount;

    /**
     * 税率
     */
    private BigDecimal taxRateValue;
}
