package miaosu.invoice;

import miaosu.dao.model.BInvoice;

public class InvoiceListVO extends BInvoice {
    private String hotel;

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }
}
