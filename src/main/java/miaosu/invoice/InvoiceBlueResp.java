package miaosu.invoice;

/**
 * 开具蓝票的响应信息
 */
public class InvoiceBlueResp {
    /**
     * 二维码url
     */
    private String qrCodePath;
    /**
     *二维码图片
     */
    private String qrCode;
    /**
     * 发票请求流水号
     */
    private String invoiceReqSerialNo;
    public void setQrCodePath(String qrCodePath) {
        this.qrCodePath = qrCodePath;
    }
    public String getQrCodePath() {
        return qrCodePath;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
    public String getQrCode() {
        return qrCode;
    }

    public void setInvoiceReqSerialNo(String invoiceReqSerialNo) {
        this.invoiceReqSerialNo = invoiceReqSerialNo;
    }
    public String getInvoiceReqSerialNo() {
        return invoiceReqSerialNo;
    }

}
