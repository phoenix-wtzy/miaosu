package miaosu.invoice;

import lombok.Data;

import java.util.List;

@Data
public class InvoiceVO {
    /**
     *纳税人识别号
     */
    private String taxpayerNum;

    /**
     * 发票请求流水号
     * 4 位平台简称+16 位随机数,长度校验规则为字符长度，只
     * 能包括英文字母或数字
     */
    private String invoiceReqSerialNo;

    /**
     * 购买方名称
     */
    private String buyerName;

    /**
     *购买方纳税人识别号
     */
    private String buyerTaxpayerNum;

    /**
     * 购买方地址
     */
    private String buyerAddress;

    /**
     * 购买方电话
     */
    private String buyerTel;

    /**
     * 购买方开户行
     */
    private String buyerBankName;

    /**
     * 购买方银行账号
     */
    private String buyerBankAccount;
    /**
     * 销售方地址
     */
    private String sellerAddress;
    /**
     * 销售方电话
     */
    private String sellerTel;
    /**
     * 销售方开户行
     */
    private String sellerBankName;

    /**
     * 销售方银行账号
     */
    private String sellerBankAccount;

    /**
     * 主开票项目名称
     */
    private String itemName;

    /**
     * 收款人名称
     */
    private String casherName;

    /**
     * 复核人名称
     */
    private String reviewerName;
    /**
     * 开票人名称
     */
    private String drawerName;
    /**
     * 收票人邮箱
     */
    private String takerEmail;

    private List<GoodsItem> itemList;
}
