package miaosu.invoice;

import com.vpiaotong.openapi.OpenApi;
import com.vpiaotong.openapi.util.HttpUtils;
import com.vpiaotong.openapi.util.SecurityUtil;
import miaosu.config.InvoiceConfig;
import miaosu.dao.model.BInvoice;
import miaosu.utils.JsonUtils;
import miaosu.web.mvc.OrderController;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InvoiceServiceUtils {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);


    /**
     *生成发票信息并发送
     * @param bInvoice
     * @return
     */
    public static <T> T genetatorInvoice(BInvoice bInvoice,boolean isAdmin,Class<T> resultClazz){
           //获取发票抬头信息
            InvoiceVO invoiceVO = createInvoiceVO(bInvoice,isAdmin);
            //获取货物信息
            GoodsItem goodsItem = createGoodsItem(bInvoice);
            List<GoodsItem> itemList = new ArrayList<GoodsItem>();
            itemList.add(goodsItem);
            invoiceVO.setItemList(itemList);
            String content = JsonUtils.objectToJson(invoiceVO);

            logger.info("生成发票的请求内容：{}",JsonUtils.objectToJson(content));
            //生成电子发票
            return getBuleInvoice(content,resultClazz);
    }



    /**
     * 查询发票票面信息
     * @param bean
     * @return
     */
    public static InvoiceInfoResultBean findInvoiceInfo(InvoiceInfoQueryBean  bean){

        String conent = JsonUtils.beanToJson(bean);
        String buildRequest = new OpenApi(InvoiceConfig.DESS_ECRET_KEY, InvoiceConfig.PLATFORM_CODE, InvoiceConfig.PLATFORM_PREFIX,
                InvoiceConfig.PRIVATE_KEY)
                .buildRequest(conent);
        String response = HttpUtils.postJson(InvoiceConfig.TEST_INVOICE_URL, buildRequest);

        logger.info("response:{}",response);

        Map<String, Object> resultMap = JsonUtils.parseJSONMap(response);

        if (!"0000".equals(resultMap.get("code"))){
            return null;
        }

        String  content = (String)resultMap.get("content");
        try {
            return  convertContent(content,InvoiceInfoResultBean.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;

    }


    /**
     * 涉及到两方互相为购买方
     * @param bInvoice
     * @return
     */
    private static InvoiceVO createInvoiceVO( BInvoice bInvoice,Boolean isAdmin) {
        InvoiceVO invoiceVO = new InvoiceVO();
        //发票请求流水号 自动生成  4 位平台简称+16
        invoiceVO.setInvoiceReqSerialNo(InvoiceConfig.PLATFORM_PREFIX + RandomStringUtils.randomAlphanumeric(16));
        //如果是管理员，说明 分销方是销售方，酒店方是购买方
        if(isAdmin){
            //购买方的地址、开户行账号，银行名称，税号，企业名称，电话（填写）
            invoiceVO.setBuyerAddress(bInvoice.getBuyeraddress());
            invoiceVO.setBuyerBankAccount(bInvoice.getBuyerbankaccount());
            invoiceVO.setBuyerBankName(bInvoice.getBuyerbankname());
            invoiceVO.setBuyerTaxpayerNum(bInvoice.getBuyertaxpayernum());
            invoiceVO.setBuyerName(bInvoice.getBuyername());
            invoiceVO.setBuyerTel(bInvoice.getBuyertel());

            //销售方一般唯一 所以可以在配置中设置
            //销售方 地址 ，银行账号，税号   （电话，开户行名称 企业名称 没有 可不填写）
            invoiceVO.setSellerAddress(InvoiceConfig.SELLER_ADDRESS);
            invoiceVO.setSellerBankAccount(InvoiceConfig.SELLER_BANKACCOUNT);
            invoiceVO.setTaxpayerNum(InvoiceConfig.TAX_NUM);

            //收款人，复核人，开票人
            invoiceVO.setDrawerName(InvoiceConfig.DRAWER_NAME);
            invoiceVO.setCasherName(InvoiceConfig.CASHER_NAME);
            invoiceVO.setReviewerName(InvoiceConfig.REVIEWER_NAME);
        }else{ //购买方是分销方，销售方是酒店方
            //购买方的地址、开户行账号，银行名称，税号，企业名称，电话（填写）
            invoiceVO.setBuyerAddress(InvoiceConfig.SELLER_ADDRESS);
            invoiceVO.setBuyerBankAccount(InvoiceConfig.SELLER_BANKACCOUNT);
            invoiceVO.setBuyerBankName(InvoiceConfig.SELLER_BANKNAME);
            invoiceVO.setBuyerTaxpayerNum(InvoiceConfig.TAX_NUM);
            invoiceVO.setBuyerName(InvoiceConfig.SELLER_COMPAY);
            invoiceVO.setBuyerTel(InvoiceConfig.SELLER_TEL);

            //销售方一般唯一 所以可以在配置中设置
            //销售方 地址 ，银行账号，税号   （电话，开户行名称 企业名称 没有 可不填写）
            invoiceVO.setSellerAddress(bInvoice.getBuyeraddress());
            invoiceVO.setSellerBankAccount(bInvoice.getBuyerbankaccount());
            invoiceVO.setTaxpayerNum(bInvoice.getBuyertaxpayernum());

            //收款人，复核人，开票人
            invoiceVO.setDrawerName(bInvoice.getDrawername());
            invoiceVO.setCasherName(bInvoice.getCashername());
            invoiceVO.setReviewerName(bInvoice.getReviewername());
        }


        //开票项目名称
        invoiceVO.setItemName(bInvoice.getItemname());
        //电子发票的发送邮箱地址（填写）
        invoiceVO.setTakerEmail(bInvoice.getTakeremail());


        return invoiceVO;
    }

    private static <T> T getBuleInvoice(String request,Class<T> clazz) {
        String buildRequest = new OpenApi(InvoiceConfig.DESS_ECRET_KEY, InvoiceConfig.PLATFORM_CODE, InvoiceConfig.PLATFORM_PREFIX,
                InvoiceConfig.PRIVATE_KEY)
                .buildRequest(request);
        String response = HttpUtils.postJson(InvoiceConfig.TEST_URL, buildRequest);

        logger.info("response:{}",response);

        Map<String, Object> resultMap = JsonUtils.parseJSONMap(response);

        if (!"0000".equals(resultMap.get("code"))){
            return null;
        }

        String  content = (String)resultMap.get("content");
        try {
            return  convertContent(content,clazz);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取class中的所有字段并将其转换为对应的响应结果
     * @param content 3DES加密后的响应密文
     * @param clazz
     * @param <T>
     * @return
     */
    private static <T> T convertContent(String content, Class<T> clazz) throws IllegalAccessException {
        //对类信息整体字段进行解密
        String reqContent = SecurityUtil.decrypt3DES(InvoiceConfig.DESS_ECRET_KEY, content);

        T t = JsonUtils.jsonToObject(reqContent,clazz);

        //反射获取该类对象的所有字段
        /*
        Field[] fields = clazz.getDeclaredFields();
        for( Field field:fields) {
            field.setAccessible(true);
            Object objectValue = field.get(t);
            if(objectValue instanceof  String){
                String value = Base64Util.decode((String) objectValue);
                field.set(t, Base64Util.decode(value));
            }
        }
       */
        return t;
    }

    private static GoodsItem createGoodsItem(BInvoice bInvoice) {
        GoodsItem goodsItem = new GoodsItem();
        //货物名称
        goodsItem.setGoodsName(bInvoice.getGoodsname());
        //金额
        goodsItem.setInvoiceAmount(bInvoice.getInvoiceamount());
        //单位
        goodsItem.setMeteringUnit("次");
        //数量
        goodsItem.setQuantity(bInvoice.getQuantity());
        //对应税收分类编码（填写）
        goodsItem.setTaxClassificationCode(InvoiceConfig.TAXClASSIFICATION_CODE);
        //税率（填写）
        goodsItem.setTaxRateValue(bInvoice.getTaxratevalue());
        //单价
        goodsItem.setUnitPrice(bInvoice.getUnitprice());
        return goodsItem;
    }
}
