package miaosu.svc.order;


import lombok.extern.slf4j.Slf4j;
import miaosu.common.OrderChannelEnum;
import miaosu.common.OrderStatusEnum;
import miaosu.common.SellTypeEnum;
import miaosu.common.SettingTypeEnum;
import miaosu.dao.auto.HotelInfoMapper;
import miaosu.dao.auto.HotelOrderMapper;
import miaosu.dao.auto.HotelRoomSetMapper;
import miaosu.dao.model.*;
import miaosu.utils.ConstantUtil;
import miaosu.utils.ExcelUtil;
import miaosu.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;



/**
 * Created by Administrator on 2018/6/20.
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)

/**
 * 订单导入
 *
 * @author zyx
 * @date 2018/06/22
 * */
public class OrderImportService {

    @Autowired
    private HotelOrderMapper hotelOrderMapper ;


    @Autowired
    private HotelInfoMapper hotelInfoMapper ;

    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;

    public OrderImportService() {
    }

    public String importToOrder(MultipartFile file){
        List<List<Object>> listob = null;
        HotelOrder hotelOrder = null;
        HotelRoomSet hotelRoomSets = null;
        Long hotelId = null;
        try {
            InputStream in = file.getInputStream();
            //检查格式
            if(ExcelUtil.checkExcelFormat(file.getOriginalFilename())){
                listob = new ExcelUtil().getBankListByExcel(in, file.getOriginalFilename());
            }else{
                return ConstantUtil.UPLOADFILE_FORMATERROR_MESSAGE;
            }

          if(null!=listob && listob.size()>0){
              for (int i = 0; i < listob.size(); i++) {
                  List<Object> lo = listob.get(i);
                  //excel行数
                  int row = i+2;
                  //避免list自增产生的空串
                  if(lo .size()<ExcelUtil.HOTEL_ORDER_ROW_LILMIT && lo .toString().equals("[]")){
                  //避免行末无数据
                  }else if(lo .size()<ExcelUtil.HOTEL_ORDER_ROW_LILMIT){
                      return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                  }else {
                      //判断订单是否重复
                      HotelOrderExample orderExample = new HotelOrderExample();
                      String orderNo = String.valueOf(lo.get(3));
                      orderExample.createCriteria().andOrderNoEqualTo(orderNo.replaceAll(" ","").replaceAll("\t",""));
                      List<HotelOrder> orderList = hotelOrderMapper.selectByExample(orderExample);

                      if(orderList.size()>0){
                      }else{
                              hotelOrder = new HotelOrder();
                          try {
                              //酒店名称
                              if(null==lo.get(0)|| "".equals(lo.get(0).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  HotelInfoExample hotelInfo = new HotelInfoExample();
                                  String hotelName = String.valueOf(lo.get(0));
                                  hotelInfo.createCriteria().andHotelNameEqualTo(hotelName);
                                  List<HotelInfo> hotelList = hotelInfoMapper.selectByExample(hotelInfo);
                                  if(hotelList.size()>0){
                                      hotelId = hotelList.get(0).getHotelId();
                                  }else{
                                      log.info("查无此酒店!");
                                      return ConstantUtil.UPLOADFILE_NOFINDHOTELERROR_MESSAGE;
                                  }
                                  hotelOrder.setHotelId(hotelId);
                              }
                              //渠道
                              if(null==lo.get(1)|| "".equals(lo.get(1).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setOrderChannel((int) OrderChannelEnum.fromName(lo.get(1).toString()).getCode());
                              }
                              //置换.代销
                              if(null==lo.get(2) || "".equals(lo.get(2).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setSellType((int) SellTypeEnum.fromName(lo.get(2).toString()).getCode());
                              }
                              //订单号
                              if(null==lo.get(3) || "".equals(lo.get(3).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setOrderNo(lo.get(3).toString());
                              }
                              //预订时间
                              if(null==lo.get(4)|| "".equals(lo.get(4).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setBookTime(StringUtil.formatDateStr(lo.get(4).toString(),"yyyy-MM-dd HH:mm:ss"));
                              }
                              //预订人
                              hotelOrder.setBookUser(lo.get(5).toString());
                              //联系人电话
                              if("".equals(lo.get(6))){
                                  hotelOrder.setBookMobile(null);
                              }else{
                                  hotelOrder.setBookMobile(lo.get(6).toString());
                              }
                              //预订房型

                              if(null==lo.get(7)||"".equals(lo.get(7).toString())){
                                  hotelOrder.setBookTime(StringUtil.formatDateStr(lo.get(4).toString(),"yyyy-MM-dd HH:mm:ss"));
                              }else{

                                  HotelRoomSetExample roomset = new HotelRoomSetExample();
                                  String hotelRoomTypeName = String.valueOf(lo.get(7));
                                  roomset.createCriteria().andHotelRoomTypeNameEqualTo(hotelRoomTypeName).andHotelIdEqualTo(hotelId);
                                  List<HotelRoomSet> hotelRoomSetList = hotelRoomSetMapper.selectByExample(roomset);

                                  log.info(i+":根据自定义名称和hotelId获取房型表数据");

                                  if(hotelRoomSetList.size()>0){
                                      //从数据库获取ID
                                      hotelOrder.setHotelRoomTypeId(hotelRoomSetList.get(0).getHotelRoomTypeId());
                                      log.info(i+":从数据库获取HotelRoomTypeId");
                                  }else{
                                      //匹配获取房型ID
                                      hotelRoomSets = new HotelRoomSet();
                                      int roomTypeId  = (int)new StringUtil().matchedHous(lo.get(7).toString());
                                      log.info(i+":工具类匹配获取roomTypeId");
                                      hotelRoomSets.setRoomTypeId(roomTypeId);
                                      hotelRoomSets.setHotelRoomTypeName(lo.get(7).toString());
                                      hotelRoomSets.setHotelId(hotelId);
                                      hotelRoomSetMapper.insertSelective(hotelRoomSets);
                                      //新增一条自定义房型
                                      hotelOrder.setHotelRoomTypeId(hotelRoomSets.getHotelRoomTypeId());
                                  }
                              }
                              //间夜数
                              if(null==lo.get(8) || "".equals(lo.get(8).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setRoomCount(Integer.parseInt(lo.get(8).toString()));
                              }
                              //入住日期
                              if(null==lo.get(9)|| "".equals(lo.get(9).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setCheckInTime(StringUtil.formatDateStr(lo.get(9).toString(),"yyyy-MM-dd"));
                              }
                              //离店日期
                              if(null==lo.get(10) || "".equals(lo.get(10).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setCheckOutTime(StringUtil.formatDateStr(lo.get(10).toString(),"yyyy-MM-dd"));
                              }
                              //订单金额
                              if(null==lo.get(11)|| "".equals(lo.get(11).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setPrice(new BigDecimal(Integer.parseInt(lo.get(11).toString())));
                              }
                              //订单状态
                              if(null==lo.get(12) || "".equals(lo.get(12).toString())){
                                  return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                              }else{
                                  hotelOrder.setState(OrderStatusEnum.fromName(lo.get(12).toString()).getCode());
                              }

                              hotelOrderMapper.insertSelective(hotelOrder);
                          }catch (Exception e){
                              e.printStackTrace();
                              log.info("第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE);
                              return "第"+row+"行"+ConstantUtil.UPLOADFILE_DATEFORMATERROR_MESSAGE;
                          }

                      }
                  }
              }
          }
        }catch (Exception e){
            log.info("导入失败");
            e.printStackTrace();
            return ConstantUtil.UPLOADFILE_FAILD_MESSAGE;
        }
        return ConstantUtil.UPLOADFILE_SUCCESSS_MESSAGE;
    }
}
