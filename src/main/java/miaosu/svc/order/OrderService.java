package miaosu.svc.order;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import miaosu.common.OrderChannelEnum;
import miaosu.common.OrderStatusEnum;
import miaosu.common.SellTypeEnum;
import miaosu.common.SettleStatusEnum;
import miaosu.dao.auto.GetHotelOrderMapper;
import miaosu.dao.auto.HotelOrderMapper;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelOrder;
import miaosu.dao.model.HotelOrderExample;
import miaosu.dao.model.OrderOfCaigouone;
import miaosu.svc.hotel.HotelRoomSetService;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.settle.HotelSettleService;
import miaosu.svc.vo.HotelOrderVO;
import miaosu.svc.vo.PageResult;
import miaosu.svc.vo.SettleInfoVO;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jiajun.chen on 2018/1/10.
 */
@Service
public class OrderService {
    @Autowired
    private HotelOrderMapper hotelOrderMapper;

    @Autowired
    private HotelRoomSetService hotelRoomSetService;

    @Autowired
    private HotelSettleService settleService;

    @Autowired
    private GetHotelOrderMapper getHotelOrderMapper;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private HotelSettleService hotelSettleService;
    @Autowired
    private OrderOfCaigouoneService orderOfCaigouoneService;

    public List<HotelOrder> queryOrderList(HotelOrderExample order) {
        return hotelOrderMapper.selectByExample(order);
    }

    public List<Map<String,Object>> queryOrderListToExcel(HotelOrderExample orderExample) {
        List<HotelOrder> orders = hotelOrderMapper.selectByExample(orderExample);
        List<Map<String,Object>> orderMapList = new ArrayList<Map<String, Object>>();
        for(HotelOrder order:orders){
            //TODO 转换int语义化的说明 渠道 销售类型，房型 订单状态 结算状态
            HotelOrderVO  vo = new HotelOrderVO();
            BeanUtils.copyProperties(order,vo);
            HotelInfo hotelInfo = hotelService.getHotelInfoById(vo.getHotelId());
            int hotelType = hotelInfo.getHotelType();
            //B类业务,针对投资人
            if (hotelType==2){
                vo.setSettle_money("--");
            }
            //A类有结算金额显示
            else {
                vo.setSettle_money(vo.getSettle_money());
            }

            //渠道编码
            vo.setOrderChannelCode(vo.getOrderChannel()+"");
            //渠道名称
            vo.setOrderChannelName(OrderChannelEnum.fromCode(vo.getOrderChannel()).getName());
            //销售类型
            vo.setSellTypeName(SellTypeEnum.fromCode(vo.getSellType()).getName());
            //订单状态
            vo.setOrderStateStr(OrderStatusEnum.fromCode(vo.getState()).getName());
            //房型名称
            vo.setP_hotel_room_type_name( vo.getP_hotel_room_type_name());
            vo.setHotelRoomTypeName( vo.getP_hotel_room_type_name());
            //预定时间
            vo.setBookTimeStr(DateFormatUtils.format(vo.getBookTime(),"M/d HH:mm"));
            //入住时间
            vo.setCheckInTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd"));
            //离开时间
            vo.setCheckOutTimeStr(DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd"));
            //入离时间
            vo.setCheckInOutTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"M/d")+" - "+DateFormatUtils.format(vo.getCheckOutTime(),"M/d"));

            //酒店确认号
            vo.setHotel_confirm_number(vo.getHotel_confirm_number());
            //房型名称
            vo.setP_hotel_room_type_name( vo.getP_hotel_room_type_name());
            vo.setHotelRoomTypeName( vo.getP_hotel_room_type_name());
            //间夜=每单的入离时间差
            long days = getDays(vo.getCheckInTime(), vo.getCheckOutTime());
            vo.setNight(days);
            //采购单价
            vo.setCaigou_one(vo.getCaigou_one());
            //采购单价列表
            Long order_id = vo.getOrderId();
            //查询比订单的置单日历表,根据酒店id和caigou_one_state=0的
            List<OrderOfCaigouone>orderOfCaigouoneList=orderOfCaigouoneService.queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(order_id,0);
            vo.setOrderOfCaigouoneList(orderOfCaigouoneList);
            //采购总价
            vo.setCaigou_total(vo.getCaigou_total());
			//将间夜数传递给前端
			vo.setNightC(orderOfCaigouoneList.size());
            //订单备注
            String remark_one = vo.getRemark_one();
            if (null==remark_one){
                vo.setRemark_one(remark_one);
            }else {
                if (remark_one.contains("\n")||remark_one.contains("\r")){
                    remark_one = remark_one.replaceAll("\r|\n", "");
                    vo.setRemark_one(remark_one);
                }else {
                    vo.setRemark_one(remark_one);
                }
            }
            String bookRemark = vo.getBookRemark();
            if (null==bookRemark){
                vo.setBookRemark(bookRemark);
            }else {
                if (bookRemark.contains("\n")||bookRemark.contains("\r")){
                    bookRemark = bookRemark.replaceAll("\r|\n", "");
                    vo.setBookRemark(bookRemark);
                }else {
                    vo.setBookRemark(bookRemark);
                }
            }

            vo.setSettleStatusStr(SettleStatusEnum.fromCode(order.getSettleStatus()).getName());

            String json = JsonUtils.objectToJson(vo);
            Map<String,Object> orderMap = JsonUtils.parseJSONMap(json);
            orderMapList.add(orderMap);
        }

        return  orderMapList;

    }

    public PageResult<HotelOrder> queryOrderListBypage(HotelOrderExample example, int offset, int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<HotelOrder> orderList = this.queryOrderList(example);
        long total = objects.getTotal();
        return new PageResult().success(total,orderList);
    }

    public PageResult<List<HotelOrderVO>> queryOrderListBypage(Long hotelId, int offset, int limit, boolean isAdmin) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("hotelId",hotelId);
        param.put("bookTime",DateFormatUtils.format(new Date(),"yyyy-MM-dd"));
        param.put("offset",offset);
        param.put("limit",limit);
        List<HotelOrder> orderList = hotelOrderMapper.selectWaitConfirmOrder(param);

        List<HotelOrderVO> list = Lists.newArrayList();
        for(HotelOrder hotelOrder:orderList){
            HotelOrderVO  vo = new HotelOrderVO();
            BeanUtils.copyProperties(hotelOrder,vo);
            HotelInfo hotelInfo = hotelService.getHotelInfoById(vo.getHotelId());
            int hotelType = hotelInfo.getHotelType();
            //B类业务,针对投资人
            if (hotelType==2){
                vo.setSettle_money("--");
            }
            //A类有结算金额显示
            else {
                vo.setSettle_money(vo.getSettle_money());
            }

            vo.setOrderChannelName(OrderChannelEnum.fromCode(vo.getOrderChannel()).getName());
            vo.setSellTypeName(SellTypeEnum.fromCode(vo.getSellType()).getName());
            vo.setOrderStateStr(OrderStatusEnum.fromCode(vo.getState()).getName());
            vo.setBookTimeStr(DateFormatUtils.format(vo.getBookTime(),"M/d HH:mm"));
            vo.setCheckInTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd"));
            vo.setCheckOutTimeStr(DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd"));
            vo.setCheckInOutTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"M/d")+" - "+DateFormatUtils.format(vo.getCheckOutTime(),"M/d"));
            //酒店确认号
            vo.setHotel_confirm_number(vo.getHotel_confirm_number());
            //房型名称
            vo.setP_hotel_room_type_name( vo.getP_hotel_room_type_name());
            vo.setHotelRoomTypeName( vo.getP_hotel_room_type_name());

            //间夜=每单的入离时间差
            long days = getDays(vo.getCheckInTime(), vo.getCheckOutTime());
            vo.setNight(days);
            //采购单价
            vo.setCaigou_one(vo.getCaigou_one());
            //采购单价列表
            Long order_id = vo.getOrderId();
            //查询比订单的置单日历表,根据酒店id和caigou_one_state=0的
            List<OrderOfCaigouone>orderOfCaigouoneList=orderOfCaigouoneService.queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(order_id,0);
            vo.setOrderOfCaigouoneList(orderOfCaigouoneList);
            //采购总价
            vo.setCaigou_total(vo.getCaigou_total());
			//将间夜数传递给前端
			vo.setNightC(orderOfCaigouoneList.size());
            //订单备注
            String remark_one = vo.getRemark_one();
            if (null==remark_one){
                vo.setRemark_one(remark_one);
            }else {
                if (remark_one.contains("\n")||remark_one.contains("\r")){
                    remark_one = remark_one.replaceAll("\r|\n", "");
                    vo.setRemark_one(remark_one);
                }else {
                    vo.setRemark_one(remark_one);
                }
            }
            String bookRemark = vo.getBookRemark();
            if (null==bookRemark){
                vo.setBookRemark(bookRemark);
            }else {
                if (bookRemark.contains("\n")||bookRemark.contains("\r")){
                    bookRemark = bookRemark.replaceAll("\r|\n", "");
                    vo.setBookRemark(bookRemark);
                }else {
                    vo.setBookRemark(bookRemark);
                }
            }
            //非admin内容不一样
            if(!isAdmin){
                vo.setOrderNo("");
                vo.setOrderChannel(0);
                vo.setOrderChannelName("");
                vo.setPrice(null);
            }
            list.add(vo);
        }

//        long total = hotelOrderMapper.selectConfirmOrderCount(param)+hotelOrderMapper.selectCancelOrderTodayCount(param);
        return new PageResult().success(list.size(),list);
    }


    public PageResult<List<HotelOrderVO>> queryOrderListBypage(HotelOrderExample example, int offset, int limit, boolean isAdmin) {
        Page<Object> pageInfo = PageHelper.offsetPage(offset, limit);
        List<HotelOrder> orderList = this.queryOrderList(example);


        List<HotelOrderVO> list = Lists.newArrayList();
        for(HotelOrder hotelOrder:orderList){
            HotelOrderVO  vo = new HotelOrderVO();
            BeanUtils.copyProperties(hotelOrder,vo);

            HotelInfo hotelInfo = hotelService.getHotelInfoById(vo.getHotelId());
            int hotelType = hotelInfo.getHotelType();
            //B类业务,针对投资人
            if (hotelType==2){
                vo.setSettle_money("--");
            }
            //A类有结算金额显示
            else {
                vo.setSettle_money(vo.getSettle_money());
            }



            //间夜=每单的入离时间差
            long days = getDays(vo.getCheckInTime(), vo.getCheckOutTime());
            vo.setNight(days);
            //酒店确认号
            vo.setHotel_confirm_number(vo.getHotel_confirm_number());
            //房型名称
            vo.setP_hotel_room_type_name( vo.getP_hotel_room_type_name());
            vo.setHotelRoomTypeName( vo.getP_hotel_room_type_name());
            //采购单价
            vo.setCaigou_one(vo.getCaigou_one());
            //采购单价列表
            Long order_id = vo.getOrderId();
            //查询比订单的置单日历表,根据酒店id和caigou_one_state=0的
            List<OrderOfCaigouone>orderOfCaigouoneList=orderOfCaigouoneService.queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(order_id,0);
            vo.setOrderOfCaigouoneList(orderOfCaigouoneList);
            //采购总价
			vo.setCaigou_total(vo.getCaigou_total());
			//将间夜数传递给前端
			vo.setNightC(orderOfCaigouoneList.size());
            //订单备注
            String remark_one = vo.getRemark_one();
            if (null==remark_one){
                vo.setRemark_one(remark_one);
            }else {
                if (remark_one.contains("\n")||remark_one.contains("\r")){
                    remark_one = remark_one.replaceAll("\r|\n", "");
                    vo.setRemark_one(remark_one);
                }else {
                    vo.setRemark_one(remark_one);
                }
            }
            String bookRemark = vo.getBookRemark();
            if (null==bookRemark){
                vo.setBookRemark(bookRemark);
            }else {
                if (bookRemark.contains("\n")||bookRemark.contains("\r")){
                    bookRemark = bookRemark.replaceAll("\r|\n", "");
                    vo.setBookRemark(bookRemark);
                }else {
                    vo.setBookRemark(bookRemark);
                }
            }

            vo.setOrderChannelCode(vo.getOrderChannel()+"");
            vo.setOrderChannelName(OrderChannelEnum.fromCode(vo.getOrderChannel()).getName());
            vo.setSellTypeName(SellTypeEnum.fromCode(vo.getSellType()).getName());
            vo.setOrderStateStr(OrderStatusEnum.fromCode(vo.getState()).getName());
            vo.setBookTimeStr(DateFormatUtils.format(vo.getBookTime(),"yyyy-MM-dd HH:mm"));
            vo.setCheckInTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd"));
            vo.setCheckOutTimeStr(DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd"));
            vo.setCheckInOutTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd ")+" ~ "+DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd "));
            Integer settleStatus = hotelOrder.getSettleStatus();
            if(settleStatus!=null) {
                vo.setSettleStatusStr(SettleStatusEnum.fromCode(settleStatus).getName());
            }
            //非admin内容不一样 不显示订单渠道信息
            if(!isAdmin){
                vo.setOrderNo("");
                vo.setOrderChannel(0);
                vo.setOrderChannelName("");
                //vo.setPrice(null);
            }
            list.add(vo);
        }

        long total = pageInfo.getTotal();
        return new PageResult().success(total,list);
    }

    public long addOrder(HotelOrder order){
        order.setSettleStatus(0);
        hotelOrderMapper.insert(order);
        return order.getOrderId();
    }
    //添加到临时表
    public long addGetOrder(HotelOrder order){
        order.setSettleStatus(0);
        getHotelOrderMapper.insert(order);
        return order.getOrderId();
    }

    public HotelOrder queryOrder(Long orderId) {
        return hotelOrderMapper.selectByPrimaryKey(orderId);
    }

    public int updateStatus(Long orderId, int status) {
        HotelOrder record = new HotelOrder();
        record.setOrderId(orderId);
        record.setState(status);
       return hotelOrderMapper.updateByPrimaryKeySelective(record);
    }
    //更新临时表中的订单状态:已确认;已取消
    public int updateGetOrderStatus(Long orderId, int status) {
        HotelOrder record = new HotelOrder();
        record.setOrderId(orderId);
        record.setState(status);
        return getHotelOrderMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 查询需要结算的订单的结算金额相关信息
     * @param orderExample
     * @return
     */
    public SettleInfoVO queryOrderSettleInfo(HotelOrderExample orderExample,Long hotelId) {
        List<HotelOrder> hotelOrders =  queryOrderList(orderExample);

        StringBuilder orderIdsBuild = new StringBuilder();
        //结算金额
        BigDecimal settleMoney = new BigDecimal(0);

        //佣金金额
        BigDecimal commissionMoney = new BigDecimal(0);
        String orderIds = null;
        if(hotelOrders!=null && hotelOrders.size()>0){
            for(HotelOrder order:hotelOrders){
                orderIdsBuild.append(order.getOrderId()+",");
                settleMoney = settleMoney.add(order.getPrice());
            }
            orderIds = orderIdsBuild.substring(0,orderIdsBuild.length()-1);
        }


       //获取对应的佣金比例
       Integer commissionRate =  settleService.queryCommissionRate(hotelId);

       commissionMoney = settleMoney.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));

       settleMoney = settleMoney.subtract(commissionMoney);
       SettleInfoVO settleInfo = new SettleInfoVO();
       settleInfo.setSettleMoney(settleMoney.setScale(2,BigDecimal.ROUND_DOWN).toString());
       settleInfo.setCommissionMoney(commissionMoney.setScale(2,BigDecimal.ROUND_DOWN).toString());
       settleInfo.setHotelId(hotelId);
       settleInfo.setOrderIds(orderIds);

       return settleInfo;
    }

    public List<HotelOrderVO> queryBatchOrderByIds(List<Long> orderIds) {
        List<HotelOrderVO> orderDetails = Lists.newArrayList();
        for(Long orderId : orderIds){
           HotelOrder order = this.queryOrder(orderId);
            HotelOrderVO  vo = new HotelOrderVO();
            BeanUtils.copyProperties(order,vo);
            HotelInfo hotelInfo = hotelService.getHotelInfoById(vo.getHotelId());
            int hotelType = hotelInfo.getHotelType();
            //B类业务,针对投资人
            if (hotelType==2){
                vo.setSettle_money("--");
            }
            //A类有结算金额显示
            else {
                vo.setSettle_money(vo.getSettle_money());
            }


            //间夜=每单的入离时间差
            long days = getDays(vo.getCheckInTime(), vo.getCheckOutTime());
            vo.setNight(days);
            //酒店确认号
            vo.setHotel_confirm_number(vo.getHotel_confirm_number());
            //房型名称
            vo.setP_hotel_room_type_name( vo.getP_hotel_room_type_name());
            vo.setHotelRoomTypeName( vo.getP_hotel_room_type_name());
            //采购单价
            vo.setCaigou_one(vo.getCaigou_one());
            //采购单价列表
            Long order_id = vo.getOrderId();
            //查询比订单的置单日历表,根据酒店id和caigou_one_state=0的
            List<OrderOfCaigouone>orderOfCaigouoneList=orderOfCaigouoneService.queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(order_id,0);
            vo.setOrderOfCaigouoneList(orderOfCaigouoneList);
            //采购总价
            vo.setCaigou_total(vo.getCaigou_total());
			//将间夜数传递给前端
			vo.setNightC(orderOfCaigouoneList.size());
            //订单备注
            String remark_one = vo.getRemark_one();
            if (null==remark_one){
                vo.setRemark_one(remark_one);
            }else {
                if (remark_one.contains("\n")||remark_one.contains("\r")){
                    remark_one = remark_one.replaceAll("\r|\n", "");
                    vo.setRemark_one(remark_one);
                }else {
                    vo.setRemark_one(remark_one);
                }
            }
            String bookRemark = vo.getBookRemark();
            if (null==bookRemark){
                vo.setBookRemark(bookRemark);
            }else {
                if (bookRemark.contains("\n")||bookRemark.contains("\r")){
                    bookRemark = bookRemark.replaceAll("\r|\n", "");
                    vo.setBookRemark(bookRemark);
                }else {
                    vo.setBookRemark(bookRemark);
                }
            }

            vo.setOrderChannelCode(vo.getOrderChannel()+"");
            vo.setOrderChannelName(OrderChannelEnum.fromCode(vo.getOrderChannel()).getName());
            vo.setSellTypeName(SellTypeEnum.fromCode(vo.getSellType()).getName());
            vo.setOrderStateStr(OrderStatusEnum.fromCode(vo.getState()).getName());
            vo.setBookTimeStr(DateFormatUtils.format(vo.getBookTime(),"M/d HH:mm"));
            vo.setCheckInTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd"));
            vo.setCheckOutTimeStr(DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd"));
            vo.setCheckInOutTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"M/d")+" - "+DateFormatUtils.format(vo.getCheckOutTime(),"M/d"));
            vo.setSettleStatusStr(SettleStatusEnum.fromCode(order.getSettleStatus()).getName());
            orderDetails.add(vo);
        }

        return  orderDetails;
    }

    //根据酒店id和房间id查询房型id
    public Long queryHotelRoomTypeId(Long hotel_id_selected,Long roomTypeId) {
        return hotelOrderMapper.queryHotelRoomTypeId(hotel_id_selected,roomTypeId);
    }

    //根据平台酒店id获取酒店id
    public Long queryHotelOnlyId(String hotel_only_id) {
        return hotelOrderMapper.queryHotelOnlyId(hotel_only_id);
    }

    //查询售卖产品productId,根据hotelRoomTypeId查询productId,在hotel_price_ref_room_type中
    public Long queryProductId(Long hotelRoomTypeId) {
        return hotelOrderMapper.queryProductId(hotelRoomTypeId);
    }

    //根据渠道单号channelOrderNo查询订单号orderId
    public Long queryHotelId(String channelOrderNo) {
        return hotelOrderMapper.queryHotelId(channelOrderNo);
    }

    //根据酒店id和平台酒店房型名字到hotel_room_set表中去查询房型hotel_room_type_id
    public Long queryHotelRoomTypeIdByP_hotel_room_type_name(Long p_hotel_id, String p_hotel_room_type_name) {
        return hotelRoomSetService.queryHotelRoomTypeIdByP_hotel_room_type_name(p_hotel_id,p_hotel_room_type_name);
    }

    //1.先根据hotel_room_type_id到hotel_price_ref_room_type表中查询hotel_price_id,多个
    public List<Long> queryhotel_price_id(Long hotelRoomTypeId) {
        return hotelRoomSetService.queryhotel_price_id(hotelRoomTypeId);
    }


    //2.在根据hotel_price_id和hotel_id和breakfast_num的拼接查询caigou_one
    public BigDecimal querycaigou_one(Long hotel_price_id, Long p_hotel_id, Integer breakfast) {
        return hotelRoomSetService.querycaigou_one(hotel_price_id,p_hotel_id,breakfast);
    }
    //获取间夜
    private long getDays(Date startDate, Date endDate) {
        if (startDate != null && endDate != null) {
            SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            Date mydate = null;

            try {
                mydate = myFormatter.parse(myFormatter.format(startDate));
                date = myFormatter.parse(myFormatter.format(endDate));
            } catch (Exception var7) {
            }

            long day = (date.getTime() - mydate.getTime()) / 86400000L;
            return day;
        } else {
            return 0L;
        }
    }

    //根据订单id添加/修改酒店订单确认号(在hotel_order中)
    public Long updateHotel_confirm_number(Long order_id, String hotel_confirm_number) {
        return hotelOrderMapper.updateHotel_confirm_number(order_id,hotel_confirm_number);
    }
    //根据订单id添加/修改酒店订单确认号(在hotel_getorder中)
    public Long updateHotel_confirm_number2(Long order_id, String hotel_confirm_number) {
        return getHotelOrderMapper.updateHotel_confirm_number(order_id,hotel_confirm_number);
    }

    //根据订单id查询酒店订单确认号(前端订单确认号的回显)
    public String queryHotel_confirm_number2(Long order_id) {
        return hotelOrderMapper.queryHotel_confirm_number2(order_id);
    }

    //查询所有订单表订单数据
    public List<HotelOrder> queryAllOrderInfo() {
        return hotelOrderMapper.queryAllOrderInfo();
    }

    //查询今天的所有订单
    public List<HotelOrder> queryOrder_Today(Date date1, Date date2) {
        return hotelOrderMapper.queryOrder_Today(date1,date2);
    }

    //查询昨天的所有订单
    public List<HotelOrder> queryOrder_Yesterday(Date date1, Date date2) {
        return hotelOrderMapper.queryOrder_Yesterday(date1,date2);
    }

    //根据订单id修改订单信息,在hotel_order表中
    public int updateOrder(HotelOrder order) {
        return hotelOrderMapper.updateOrder(order);
    }
    //根据订单id修改订单信息,在hotel_getorder表中
    public int updateOrder2(HotelOrder order) {
        return getHotelOrderMapper.updateOrder(order);
    }

    //查询当前用户名下所有酒店对应的订单
    public PageResult queryAll(List<HotelOrderExample> orderExampleList, int offset, int limit, boolean isAdmin) {
        Page<Object> pageInfo = PageHelper.offsetPage(offset, limit);

        List<HotelOrder> orderList22=Lists.newArrayList();
        for (HotelOrderExample example : orderExampleList) {
            List<HotelOrder> orderList11 = this.queryOrderList(example);
            for (HotelOrder hotelOrder : orderList11) {
                orderList22.add(hotelOrder);
            }
        }
        List<HotelOrderVO> list = Lists.newArrayList();
        for(HotelOrder hotelOrder:orderList22){
            HotelOrderVO  vo = new HotelOrderVO();
            BeanUtils.copyProperties(hotelOrder,vo);
            HotelInfo hotelInfo = hotelService.getHotelInfoById(vo.getHotelId());
            int hotelType = hotelInfo.getHotelType();
            //B类业务,针对投资人
            if (hotelType==2){
                vo.setSettle_money("--");
            }
            //A类有结算金额显示
            else {
                vo.setSettle_money(vo.getSettle_money());
            }

            //间夜=每单的入离时间差
            long days = getDays(vo.getCheckInTime(), vo.getCheckOutTime());
            vo.setNight(days);
            //酒店确认号
            vo.setHotel_confirm_number(vo.getHotel_confirm_number());
            //房型名称
            vo.setP_hotel_room_type_name( vo.getP_hotel_room_type_name());
            vo.setHotelRoomTypeName( vo.getP_hotel_room_type_name());
            //采购单价
            vo.setCaigou_one(vo.getCaigou_one());
            //采购单价列表
            Long order_id = vo.getOrderId();
            //查询比订单的置单日历表,根据酒店id和caigou_one_state=0的
            List<OrderOfCaigouone>orderOfCaigouoneList=orderOfCaigouoneService.queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(order_id,0);
            vo.setOrderOfCaigouoneList(orderOfCaigouoneList);
            //采购总价
            vo.setCaigou_total(vo.getCaigou_total());
			//将间夜数传递给前端
			vo.setNightC(orderOfCaigouoneList.size());
            //订单备注
            String remark_one = vo.getRemark_one();
            if (null==remark_one){
                vo.setRemark_one(remark_one);
            }else {
                if (remark_one.contains("\n")||remark_one.contains("\r")){
                    remark_one = remark_one.replaceAll("\r|\n", "");
                    vo.setRemark_one(remark_one);
                }else {
                    vo.setRemark_one(remark_one);
                }
            }
            String bookRemark = vo.getBookRemark();
            if (null==bookRemark){
                vo.setBookRemark(bookRemark);
            }else {
                if (bookRemark.contains("\n")||bookRemark.contains("\r")){
                    bookRemark = bookRemark.replaceAll("\r|\n", "");
                    vo.setBookRemark(bookRemark);
                }else {
                    vo.setBookRemark(bookRemark);
                }
            }
            vo.setOrderChannelCode(vo.getOrderChannel()+"");
            vo.setOrderChannelName(OrderChannelEnum.fromCode(vo.getOrderChannel()).getName());
            vo.setSellTypeName(SellTypeEnum.fromCode(vo.getSellType()).getName());
            vo.setOrderStateStr(OrderStatusEnum.fromCode(vo.getState()).getName());
            vo.setBookTimeStr(DateFormatUtils.format(vo.getBookTime(),"yyyy-MM-dd HH:mm"));
            vo.setCheckInTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd"));
            vo.setCheckOutTimeStr(DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd"));
            vo.setCheckInOutTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd ")+" ~ "+DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd "));
            Integer settleStatus = hotelOrder.getSettleStatus();
            if(settleStatus!=null) {
                vo.setSettleStatusStr(SettleStatusEnum.fromCode(settleStatus).getName());
            }
            //非admin内容不一样 不显示订单渠道信息
            if(!isAdmin){
                vo.setOrderNo("");
                vo.setOrderChannel(0);
                vo.setOrderChannelName("");
            }
            list.add(vo);
        }
        long total = pageInfo.getTotal();
        return new PageResult().success(total,list);
    }


    //根据订单id修改采购总价
	public void updateCaigou_total(long orderId, BigDecimal caigou_total) {
		hotelOrderMapper.updateCaigou_total(orderId,caigou_total);
	}
	public void updateGetOrderCaigou_total(long orderId, BigDecimal caigou_total) {
		getHotelOrderMapper.updateGetOrderCaigou_total(orderId,caigou_total);
	}
}
