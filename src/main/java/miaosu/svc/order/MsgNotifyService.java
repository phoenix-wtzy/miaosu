package miaosu.svc.order;

import com.yunpian.sdk.constant.Code;
import com.yunpian.sdk.model.SmsBatchSend;
import lombok.extern.slf4j.Slf4j;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.sms.SmsUtil;
import miaosu.svc.vo.MessageNotifyVO;
import miaosu.svc.vo.Result;
import miaosu.utils.GetTimeUtils;
import miaosu.utils.JsonUtils;
import miaosu.voice.VoiceNotifyDTO;
import miaosu.voice.VoiceNotifyUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 消息提醒通知
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
public class MsgNotifyService {

	@Value("${accountSid}")
	private String accountSid;

	@Value("${templateId}")
	private String templateId;

	@Value("${templateId1}")
	private String templateId1;

	@Value("${templateId2}")
	private String templateId2;

	@Value("${templateId3}")
	private String templateId3;

    @Value("${templateId4}")
    private String templateId4;
    @Value("${templateId5}")
    private String templateId5;
    @Value("${templateId6}")
    private String templateId6;
    @Value("${templateId7}")
    private String templateId7;


    @Value("${token}")
	private String token;

    @Autowired
    private HotelInfoMapper hotelInfoMapper;

    /**
     * 售买产品mapper
     */
    @Autowired
    private HotelPriceRefRoomTypeMapper productMapper;

    /**
     * 计划价格
     */
    @Autowired
    private HotelPriceSetMapper priceSetMapper;

    /**
     * 房间类型mapper
     */
    @Autowired
    private HotelRoomSetMapper roomSetMapper;

    /**
     * 结算账单
     */
    @Autowired
    private HotelSettleBillMapper settleBillMapper;

    /**
     * 结算账单
     */
    @Autowired
    private HotelSettleSettingMapper settleSettingMapper;

    @Autowired
    private OrderService orderService;
    /**
     * 对应酒店的消息提醒的数据初始化
     * @param hotelId 酒店唯一标识
     * @return 包含短信提醒，语音提醒的相关手机号或者座机号
     */
    public MessageNotifyVO msgNotifyInit(Long hotelId) {
        HotelInfoExample hotelInfoExample = new HotelInfoExample();
        hotelInfoExample.createCriteria().andHotelIdEqualTo(hotelId);

        List<HotelInfo> hotelInfoList =hotelInfoMapper.selectByExample(hotelInfoExample);

        MessageNotifyVO messageNotifyVO =  null;
        if(hotelInfoList != null && hotelInfoList.size()==1){
            HotelInfo hotelInfo = hotelInfoList.get(0);
            messageNotifyVO = new MessageNotifyVO();
            BeanUtils.copyProperties(hotelInfo,messageNotifyVO);
        }
        log.info("messgaeNotify:{}", JsonUtils.objectToJson(messageNotifyVO));
        return messageNotifyVO;
    }

    /**
     * 对应酒店的短信消息提醒的手机号添加
     * @param hotelId
     * @param phone
     * @return
     */
    public Result smsPhoneAdd(Long hotelId, String phone) {
        HotelInfoExample hotelInfoExample = new HotelInfoExample();
        hotelInfoExample.createCriteria().andHotelIdEqualTo(hotelId);

        int rows = 0;
        List<HotelInfo> hotelInfoList =hotelInfoMapper.selectByExample(hotelInfoExample);
        if(hotelInfoList != null && hotelInfoList.size()==1){
            String phones =  hotelInfoList.get(0).getMsgNotifyPhone();
            if(StringUtils.isNotBlank(phones)){
                if(phones.contains(phone)){
                    return new Result().failed("手机号已经存在");
                }
                phones +=","+phone;
            }else{
                phones = phone;
            }

            HotelInfo hotelInfoUpdate = new HotelInfo();
            hotelInfoUpdate.setHotelId(hotelId);
            hotelInfoUpdate.setMsgNotifyPhone(phones);
            rows = hotelInfoMapper.updateByPrimaryKeySelective(hotelInfoUpdate);
        }

        boolean flag = rows==0?false:true ;
        if(flag){
            return  new Result().success();
        }else{
            return  new Result().failed("添加失败");
        }
    }

    /**
     *  对应酒店的短信消息提醒的手机号删除
     * @param hotelId
     * @param phone
     * @return
     */
    public boolean smsPhoneDelete(Long hotelId, String phone) {
        HotelInfoExample hotelInfoExample = new HotelInfoExample();
        hotelInfoExample.createCriteria().andHotelIdEqualTo(hotelId);

        int rows = 0;
        List<HotelInfo> hotelInfoList =hotelInfoMapper.selectByExample(hotelInfoExample);
        if(hotelInfoList != null && hotelInfoList.size()==1){
            String phones =  hotelInfoList.get(0).getMsgNotifyPhone();
            if(StringUtils.isNotBlank(phones)&&StringUtils.isNotBlank(phone)) {
                phones +=",";
                phone  +=",";
                phones = phones.replaceAll(phone,"");
                if(StringUtils.isNotBlank(phones)){
                    phones = phones.substring(0,phones.length()-1);
                }
                HotelInfo hotelInfoUpdate = new HotelInfo();
                hotelInfoUpdate.setHotelId(hotelId);
                hotelInfoUpdate.setMsgNotifyPhone(phones);
                rows = hotelInfoMapper.updateByPrimaryKeySelective(hotelInfoUpdate);
            }
        }
        return rows==0?false:true ;
    }

    /**
     * 对应酒店的语音消息提醒的手机号（座机号）添加
     * @param hotelId
     * @param phone
     * @return
     */
    public Result voicePhoneAdd(Long hotelId, String phone) {
        HotelInfo hotelInfoUpdate = new HotelInfo();
        hotelInfoUpdate.setHotelId(hotelId);
        hotelInfoUpdate.setVoiceNotifyPhone(phone);
        int rows = hotelInfoMapper.updateByPrimaryKeySelective(hotelInfoUpdate);

        boolean flag = rows==0?false:true ;
        if(flag){
            return  new Result().success();
        }else{
            return  new Result().failed("添加失败");
        }
    }

    /**
     * 对应酒店的语音消息提醒的手机号（座机号）删除
     * @param hotelId
     * @param phone
     * @return
     */
    public boolean voicePhoneDelete(Long hotelId, String phone) {
        HotelInfo hotelInfoUpdate = new HotelInfo();
        hotelInfoUpdate.setHotelId(hotelId);
        hotelInfoUpdate.setVoiceNotifyPhone("");
        int rows = hotelInfoMapper.updateByPrimaryKeySelective(hotelInfoUpdate);

        return  rows==0?false:true ;
    }


    /**
     * 消息通知  适用于本地通知订单新增的消息提醒
     * @param hotelId
     * @param hotelOrder
     * @return
     */
    public boolean sendMsg(Long hotelId, HotelOrder hotelOrder){
        //获取参数拼接参数
        //获取参数拼接参数
        HotelInfo hoteInfo = getCurrentHotelInfo(hotelId);
        if(hoteInfo == null){
            return false;
        }
        String hotelName = hoteInfo.getHotelName();
        String guestName = hotelOrder.getBookUser();
        String roomTypeName =roomSetMapper.selectByPrimaryKey(hotelOrder.getHotelRoomTypeId()).getHotelRoomTypeName();
        String productName = null;
        //产品名称 根据计划价格编码获取产品名
        //根据房间类型id 查询产品，根据产品获取价格信息
        HotelPriceRefRoomTypeExample productExample = new HotelPriceRefRoomTypeExample();
        productExample.createCriteria().andHotelRoomTypeIdEqualTo(hotelOrder.getHotelRoomTypeId());
        List<HotelPriceRefRoomType> products =  productMapper.selectByExample(productExample);
        if(products!=null && products.size()>=1){
            HotelPriceRefRoomType product = products.get(0);
            HotelPriceSet hotelPriceSet =  priceSetMapper.selectByPrimaryKey(product.getHotelPriceId());
            productName = hotelPriceSet.getHotelPriceName();
        }

        String roomNum = String.valueOf(hotelOrder.getRoomCount());
        String checkInTime = DateFormatUtils.format(hotelOrder.getCheckInTime(),"yyyy-MM-dd");
        String night = String.valueOf(getDays(hotelOrder.getCheckInTime(),hotelOrder.getCheckOutTime()));
        String bookRemark = hotelOrder.getBookRemark();//备注信息,含早不含早

        //拼接参数
        Map<String,String> params = new HashMap<String, String>(8);
        params.put("hotelName",hotelName);
        params.put("guestName",guestName);
        params.put("roomTypeName",roomTypeName);

        params.put("productName",productName);
        params.put("roomNum",roomNum);
        params.put("checkInTime", checkInTime);
        params.put("night",night);
        params.put("bookRemark",bookRemark);

        //短信通知手机信息
        String phones = hoteInfo.getMsgNotifyPhone();
        //语音通知 手机信息
        String voicePhones = hoteInfo.getVoiceNotifyPhone();
        //短信通知
        sendSms(phones,params);
        //语音通知
        sendVoiceNotify(voicePhones,params);
        return true;
    }

    public boolean sendMsg_noHotelRoomTypeId(Long hotelId, HotelOrder hotelOrder){
        //获取参数拼接参数
        HotelInfo hoteInfo = getCurrentHotelInfo(hotelId);
        if(hoteInfo == null){
            return false;
        }
        String hotelName = hoteInfo.getHotelName();
        String guestName = hotelOrder.getBookUser();
        String roomNum = String.valueOf(hotelOrder.getRoomCount());
        String checkInTime = DateFormatUtils.format(hotelOrder.getCheckInTime(),"yyyy-MM-dd");
        String night = String.valueOf(getDays(hotelOrder.getCheckInTime(),hotelOrder.getCheckOutTime()));
        String bookRemark = hotelOrder.getBookRemark();//备注信息,含早不含早

        //拼接参数
        Map<String,String> params = new HashMap<String, String>(8);
        params.put("hotelName",hotelName);
        params.put("guestName",guestName);
        params.put("roomNum",roomNum);
        params.put("checkInTime", checkInTime);
        params.put("night",night);
        params.put("bookRemark",bookRemark);

        //短信通知手机信息
        String phones = hoteInfo.getMsgNotifyPhone();
        //语音通知 手机信息
        String voicePhones = hoteInfo.getVoiceNotifyPhone();
        //短信通知
        sendSms_noHotelRoomTypeId(phones,params);
        //语音通知
        sendVoiceNotify_noHotelRoomTypeId(voicePhones,params);
        return true;
    }
    //短信
    public Boolean sendSms_noHotelRoomTypeId(String phones,Map<String,String> params){
        if(StringUtils.isBlank(phones)){
            log.info("酒店没有配置对应的短信提醒手机号，不发送短信提醒");
            return true;
        }
        //获取用于短信通知的手机号（在之前判断null）
        String hotelName = params.get("hotelName");
        String guestName = params.get("guestName");
        String roomNum = params.get("roomNum");
        String checkInTime = params.get("checkInTime");
        String night = params.get("night");
        String bookRemark = params.get("bookRemark");

        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】新订单-").append(hotelName).append("：").append("入住人-"+guestName)
                .append("，").append(bookRemark).append("-").append(roomNum+"间，住")
                .append(night+"晚,").append(checkInTime).append("入住").append("。回复TD退订");
        String content = builder.toString();

        log.info("短信发送 手机号：{} ，发送内容：{}",phones,content);

        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(phones,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }
    //语音
    public Boolean sendVoiceNotify_noHotelRoomTypeId(String phones,Map<String,String> params){
        if(StringUtils.isBlank(phones)){
            log.info("酒店没有配置对应的语音通知手机号/座机号，不发送语音提醒");
            return true;
        }

        String guestName = params.get("guestName");
        String checkInTime = convertDate(params.get("checkInTime"));
        String night = params.get("night");

        StringBuilder builder = new StringBuilder();
        builder.append(guestName).append(",")
                .append(checkInTime).append(",")
                .append(night);
        String content = builder.toString();

        log.info("短信语音 手机号/座机：{} ，发送内容：{}",phones,content);

        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,phones,Integer.parseInt(templateId5),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }

    //多次电话提醒
    public boolean sendMsgAgain_noHotelRoomTypeId(Long hotelId, HotelOrder hotelOrder){
        HotelInfo hoteInfo = getCurrentHotelInfo(hotelId);
        if(hoteInfo == null){
            return false;
        }
        String hotelName = hoteInfo.getHotelName();
        String guestName = hotelOrder.getBookUser();
        String roomNum = String.valueOf(hotelOrder.getRoomCount());
        String checkInTime = DateFormatUtils.format(hotelOrder.getCheckInTime(),"yyyy-MM-dd");
        String night = String.valueOf(getDays(hotelOrder.getCheckInTime(),hotelOrder.getCheckOutTime()));
        String bookRemark = hotelOrder.getBookRemark();//备注信息,含早不含早

        //拼接参数
        Map<String,String> params = new HashMap<String, String>(8);
        params.put("hotelName",hotelName);
        params.put("guestName",guestName);
        params.put("roomNum",roomNum);
        params.put("checkInTime", checkInTime);
        params.put("night",night);
        params.put("bookRemark",bookRemark);

        //语音通知 手机信息
        String voicePhones = hoteInfo.getVoiceNotifyPhone();
        //语音通知
        again_noHotelRoomTypeId(voicePhones,params);
        return true;
    }
    public Boolean again_noHotelRoomTypeId(String phones, Map<String, String> params) {
        String guestName = params.get("guestName");
        String checkInTime = convertDate(params.get("checkInTime"));
        StringBuilder builder = new StringBuilder();
        builder.append(guestName).append(",")
                .append(checkInTime).append(",");
        String content = builder.toString();

        log.info("多次语音通知 手机号/座机：{} ，发送内容：{}",phones,content);

        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,phones,Integer.parseInt(templateId6),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }

    //调用第三方的短信提醒平台添加（分成两个（一个短信提醒，一个）酒店id,参数（短信语音都需要））
    public Boolean sendSms(String phones,Map<String,String> params){
        if(StringUtils.isBlank(phones)){
            log.info("酒店没有配置对应的短信提醒手机号，不发送短信提醒");
            return true;
        }
        /**
         * 【秒宿乐易换】新订单-#hotelName#酒店：#guestName#，#roomTypeName#-#productName#(#roomNum#间)，#checkInTime#入住，住#night#晚
         */
        //获取用于短信通知的手机号（在之前判断null）
        String hotelName = params.get("hotelName");
        String guestName = params.get("guestName");
        String roomTypeName = params.get("roomTypeName");
        //String productName = params.get("productName");
        String roomNum = params.get("roomNum");
        String checkInTime = params.get("checkInTime");
        String night = params.get("night");
        String bookRemark = params.get("bookRemark");

        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】新订单-").append(hotelName).append("：").append("入住人-"+guestName)
                .append("，").append(roomTypeName).append("-").append(bookRemark).append("-").append(roomNum+"间，住")
                .append(night+"晚,").append(checkInTime).append("入住").append("。回复TD退订");
        String content = builder.toString();

        log.info("短信发送 手机号：{} ，发送内容：{}",phones,content);

       com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(phones,content);
       if(result.getCode() == Code.OK){
           return true;
       }
        return false;
    }

    //调用第三方的短信提醒平台添加（分成两个（一个短信提醒，一个）酒店id,参数（短信语音都需要））
    public Boolean sendVoiceNotify(String phones,Map<String,String> params){
        if(StringUtils.isBlank(phones)){
            log.info("酒店没有配置对应的语音通知手机号/座机号，不发送语音提醒");
            return true;
        }
        /**
         *  秒宿新订单来了，预订人：{XXX}, 房间类型：{复式大床房},  {2019-01-02}入住，入住：{X}晚;
         *  秒宿新订单来了，预订人：{1}, 房间类型：{2},  {3}入住，入住：{4}晚
         *  新订单来了， 订单号：{1} ,房间类型：{2}, 预订人：{3} {4}入住
         */
        //获取用于短信通知的手机号（在之前判断null）
        //String hotelName = params.get("hotelName");
        String guestName = params.get("guestName");
        String roomTypeName = params.get("roomTypeName");
        //String productName = params.get("productName");
        String roomNum = params.get("roomNum");
        String checkInTime = convertDate(params.get("checkInTime"));
        //需要将日期转换成年月日

        String night = params.get("night");

        StringBuilder builder = new StringBuilder();
        builder.append(guestName).append(",")
                .append(roomTypeName).append(",")
                .append(checkInTime).append(",")
                .append(night);
        String content = builder.toString();

        log.info("短信语音 手机号/座机：{} ，发送内容：{}",phones,content);

        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,phones,Integer.parseInt(templateId),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }

    //多次电话通知
    public boolean sendMsgAgain(Long hotelId, HotelOrder hotelOrder){
        HotelInfo hoteInfo = getCurrentHotelInfo(hotelId);
        if(hoteInfo == null){
            return false;
        }
        String hotelName = hoteInfo.getHotelName();
        String guestName = hotelOrder.getBookUser();
        String roomTypeName =roomSetMapper.selectByPrimaryKey(hotelOrder.getHotelRoomTypeId()).getHotelRoomTypeName();
        String productName = null;
        //产品名称 根据计划价格编码获取产品名
        //根据房间类型id 查询产品，根据产品获取价格信息
        HotelPriceRefRoomTypeExample productExample = new HotelPriceRefRoomTypeExample();
        productExample.createCriteria().andHotelRoomTypeIdEqualTo(hotelOrder.getHotelRoomTypeId());
        List<HotelPriceRefRoomType> products =  productMapper.selectByExample(productExample);
        if(products!=null && products.size()>=1){
            HotelPriceRefRoomType product = products.get(0);
            HotelPriceSet hotelPriceSet =  priceSetMapper.selectByPrimaryKey(product.getHotelPriceId());
            productName = hotelPriceSet.getHotelPriceName();
        }

        String roomNum = String.valueOf(hotelOrder.getRoomCount());
        String checkInTime = DateFormatUtils.format(hotelOrder.getCheckInTime(),"yyyy-MM-dd");
        String night = String.valueOf(getDays(hotelOrder.getCheckInTime(),hotelOrder.getCheckOutTime()));
        String bookRemark = hotelOrder.getBookRemark();//备注信息,含早不含早

        //拼接参数
        Map<String,String> params = new HashMap<String, String>(8);
        params.put("hotelName",hotelName);
        params.put("guestName",guestName);
        params.put("roomTypeName",roomTypeName);
        params.put("productName",productName);
        params.put("roomNum",roomNum);
        params.put("checkInTime", checkInTime);
        params.put("night",night);
        params.put("bookRemark",bookRemark);

        //语音通知 手机信息
        String voicePhones = hoteInfo.getVoiceNotifyPhone();
        //语音通知
        again(voicePhones,params);
        return true;
    }
    //多次电话提醒确认订单
    public Boolean again(String phones, Map<String, String> params) {
        String guestName = params.get("guestName");
        String roomTypeName = params.get("roomTypeName");
        String checkInTime = convertDate(params.get("checkInTime"));
        StringBuilder builder = new StringBuilder();
        builder.append(guestName).append(",")
                .append(checkInTime).append(",")
                .append(roomTypeName).append(",");
        String content = builder.toString();

        log.info("多次语音通知 手机号/座机：{} ，发送内容：{}",phones,content);

        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,phones,Integer.parseInt(templateId4),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }

    /**
     * 将字符串类型的日期转换为中文的日期类型
     * @param checkInTime
     * @return
     */
    private String convertDate(String checkInTime) {
        String[] array = checkInTime.split("-");
        String year = array[0];
        String mounth = array[1].indexOf("0")==0?array[1].replaceFirst("0",""):array[1];
        String day = array[2].indexOf("0")==0?array[2].replaceFirst("0",""):array[2];

        StringBuilder builder = new StringBuilder();
        builder.append(year).append("年").append(mounth).append("月").append(day).append("日");
        return builder.toString();
    }


    private long getDays(Date startDate, Date endDate) {
        if (startDate != null && endDate != null) {
            SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            Date mydate = null;

            try {
                mydate = myFormatter.parse(myFormatter.format(startDate));
                date = myFormatter.parse(myFormatter.format(endDate));
            } catch (Exception var7) {
            }

            long day = (date.getTime() - mydate.getTime()) / 86400000L;
            return day;
            } else {
                return 0L;
            }
    }


    private HotelInfo getCurrentHotelInfo(Long hotelId) {
        HotelInfo hoteInfo = null;
        HotelInfoExample hotelInfoExample = new HotelInfoExample();
        hotelInfoExample.createCriteria().andHotelIdEqualTo(hotelId);

        List<HotelInfo> hotelInfoList =hotelInfoMapper.selectByExample(hotelInfoExample);
        if(hotelInfoList != null && hotelInfoList.size()==1) {
            hoteInfo = hotelInfoList.get(0);
        }
        return hoteInfo;
    }

    //发送结算信息
    public void sendSettleMsg(String hotelName,Long hotelId, Long billId) {
          /*
         【秒宿乐易换】结算通知-#hotelName#酒店,开始时间#startDate#,结束时间#endDate#的订单已经完成结算，结算时间#settleTime#,结算金额#settleMoney#。
         */

          //获取手机信息
        HotelSettleSettingExample settleSettingExample = new HotelSettleSettingExample();
        settleSettingExample.createCriteria().andHotelIdEqualTo(hotelId);
        List<HotelSettleSetting> settleSettings = settleSettingMapper.selectByExample(settleSettingExample);

        if(settleSettings == null && settleSettings.size()!=1){
            return ;
        }

        String phone = settleSettings.get(0).getSettleMsgPhone();

        if(StringUtils.isBlank(phone)){
            return;
        }

        HotelSettleBill bill = settleBillMapper.selectByPrimaryKey(billId);

        String startDate = DateFormatUtils.format(bill.getStartDate(),"yyyy-MM-dd");

        String endDate = DateFormatUtils.format(bill.getEndDate(),"yyyy-MM-dd");

        String settleTime = DateFormatUtils.format(bill.getSettleTime(),"yyyy-MM-dd HH:mm:ss");

        BigDecimal settleMoneu = bill.getSettleMoney();

        String content = " 【秒宿乐易换】结算通知-"+hotelName+"酒店,开始时间"+startDate+",结束时间"+endDate+"的订单已经完成结算，结算时间"+settleTime+",结算金额"
                +settleMoneu.setScale(2,BigDecimal.ROUND_HALF_DOWN).toString()+"。";


        SmsUtil.sendMsg(phone,content);
    }

    /**
     * 三级关房时候电话通知二级管理员

     * @param params
     */
    public boolean sendUserSms(String ota_phone, Map<String, String> params) {
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的语音通知手机号/座机号，不发送语音提醒");
            return true;
        }
        String hotelName = params.get("hotelName");
        String setStartTime = params.get("setStartTime");
        String setEndTime = params.get("setEndTime");
        String roomTypeName = params.get("roomTypeName");
        StringBuilder builder = new StringBuilder();
        builder.append(hotelName).append(",")
                .append(setStartTime).append(",")
                .append(setEndTime).append(",")
                .append(roomTypeName);
        String content = builder.toString();
        log.info("短信语音 手机号/座机：{} ，发送内容：{}",ota_phone,content);
        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,ota_phone,Integer.parseInt(templateId1),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }
    //三级开房时候电话通知二级管理员
    public boolean sendUserSms11(String ota_phone, Map<String, String> params) {
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的语音通知手机号/座机号，不发送语音提醒");
            return true;
        }
        String hotelName = params.get("hotelName");
        String setStartTime = params.get("setStartTime");
        String setEndTime = params.get("setEndTime");
        String roomTypeName = params.get("roomTypeName");
        StringBuilder builder = new StringBuilder();
        builder.append(hotelName).append(",")
                .append(setStartTime).append(",")
                .append(setEndTime).append(",")
                .append(roomTypeName);
        String content = builder.toString();
        log.info("短信语音 手机号/座机：{} ，发送内容：{}",ota_phone,content);
        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,ota_phone,Integer.parseInt(templateId7),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }

    /**
     *三级关房时候短信通知二级管理员
     */
    public Boolean sendUserSms2(String ota_phone,Map<String,String> params){
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的短信提醒手机号，不发送短信提醒");
            return true;
        }
        String hotelName = params.get("hotelName");//酒店名字:海旺弘亚温泉酒店
        String setStartTime = params.get("setStartTime");//开始年月日:2020-7-29
        String setEndTime = params.get("setEndTime");//结束年月日:2020-8-1
        String roomTypeName = params.get("roomTypeName");//房间类型:豪华标准间

        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】关房通知:").append(hotelName).append("将").append(setStartTime).append("到").append(setEndTime).append("的").append(roomTypeName).append("设置成了关房,请您知晓!");
        String content = builder.toString();

        log.info("短信发送 手机号：{} ，发送内容：{}",ota_phone,content);

        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(ota_phone,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }
    //三级开房时候短信通知二级管理员
    public Boolean sendUserSms22(String ota_phone,Map<String,String> params){
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的短信提醒手机号，不发送短信提醒");
            return true;
        }
        String hotelName = params.get("hotelName");//酒店名字:海旺弘亚温泉酒店
        String setStartTime = params.get("setStartTime");//开始年月日:2020-7-29
        String setEndTime = params.get("setEndTime");//结束年月日:2020-8-1
        String roomTypeName = params.get("roomTypeName");//房间类型:豪华标准间

        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】开房通知:").append(hotelName).append("将").append(setStartTime).append("到").append(setEndTime).append("的").append(roomTypeName).append("设置成了开房,请您知晓!");
        String content = builder.toString();

        log.info("短信发送 手机号：{} ，发送内容：{}",ota_phone,content);

        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(ota_phone,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }
    //电话通知张磊
    public boolean sendUserSms3(String ota_phone, Map<String, String> params) {
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的语音通知手机号/座机号，不发送语音提醒");
            return true;
        }
        String hotelName = params.get("hotelName");
        String setStartTime = params.get("setStartTime");
        String setEndTime = params.get("setEndTime");
        String roomTypeName = params.get("roomTypeName");
        StringBuilder builder = new StringBuilder();
        builder.append(hotelName).append(",")
                .append(setStartTime).append(",")
                .append(setEndTime).append(",")
                .append(roomTypeName);
        String content = builder.toString();
        log.info("语音 手机号/座机：{} ，发送内容：{}",ota_phone,content);
        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,ota_phone,Integer.parseInt(templateId1),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }
    //开房电话通知张磊
    public boolean sendUserSms33(String ota_phone, Map<String, String> params) {
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的语音通知手机号/座机号，不发送语音提醒");
            return true;
        }
        String hotelName = params.get("hotelName");
        String setStartTime = params.get("setStartTime");
        String setEndTime = params.get("setEndTime");
        String roomTypeName = params.get("roomTypeName");
        StringBuilder builder = new StringBuilder();
        builder.append(hotelName).append(",")
                .append(setStartTime).append(",")
                .append(setEndTime).append(",")
                .append(roomTypeName);
        String content = builder.toString();
        log.info("语音 手机号/座机：{} ，发送内容：{}",ota_phone,content);
        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,ota_phone,Integer.parseInt(templateId7),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }
    //短信通知张磊
    public Boolean sendUserSms4(String ota_phone,Map<String,String> params){
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的短信提醒手机号，不发送短信提醒");
            return true;
        }
        String hotelName = params.get("hotelName");//酒店名字:海旺弘亚温泉酒店
        String setStartTime = params.get("setStartTime");//开始年月日:2020-7-29
        String setEndTime = params.get("setEndTime");//结束年月日:2020-8-1
        String roomTypeName = params.get("roomTypeName");//房间类型:豪华标准间

        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】关房通知:").append(hotelName).append("将").append(setStartTime).append("到").append(setEndTime).append("的").append(roomTypeName).append("设置成了关房,请您知晓!");
        String content = builder.toString();

        log.info("短信发送 手机号：{} ，发送内容：{}",ota_phone,content);

        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(ota_phone,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }
    //开房短信通知张磊
    public Boolean sendUserSms44(String ota_phone,Map<String,String> params){
        if(StringUtils.isBlank(ota_phone)){
            log.info("酒店没有配置对应的短信提醒手机号，不发送短信提醒");
            return true;
        }
        String hotelName = params.get("hotelName");//酒店名字:海旺弘亚温泉酒店
        String setStartTime = params.get("setStartTime");//开始年月日:2020-7-29
        String setEndTime = params.get("setEndTime");//结束年月日:2020-8-1
        String roomTypeName = params.get("roomTypeName");//房间类型:豪华标准间

        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】开房通知:").append(hotelName).append("将").append(setStartTime).append("到").append(setEndTime).append("的").append(roomTypeName).append("设置成了开房,请您知晓!");
        String content = builder.toString();

        log.info("短信发送 手机号：{} ，发送内容：{}",ota_phone,content);

        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(ota_phone,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }

    //生成验证码,并发送给手机号(账号)
    public boolean sendCode(String phones, Integer code) {
        if(StringUtils.isBlank(phones)){
            log.info("没有手机号，不发送短信提醒");
            return true;
        }
        //【秒宿乐易换】您的验证码是#code#。如非本人操作，请忽略本短信
        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】您的验证码是").append(code).append("。如非本人操作，请忽略本短信");
        String content = builder.toString();

        log.info("短信发送 手机号：{} ，发送内容：{}",phones,content);

        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(phones,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }

    //只要有人填写公司信息就打电话给审核人员进行审核
    public boolean checkBusinessInsert(String checkUser_phone, Map<String, String> params) {
        if(StringUtils.isBlank(checkUser_phone)){
            log.info("没有设置审核人员的电话,不发送语音提醒");
            return true;
        }
        String user_name = params.get("user_name");
        String business_name = params.get("business_name");
        StringBuilder builder = new StringBuilder();
        builder.append(user_name).append(",")
                .append(business_name);
        String content = builder.toString();
        log.info("短信语音 手机号：{} ，发送内容：{}",checkUser_phone,content);
        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,checkUser_phone,Integer.parseInt(templateId2),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }

    //针对添加新公司信息发送短信
    public boolean sendMsgBusinessInsert(String checkUser_phone, Map<String, String> params) {
        if(StringUtils.isBlank(checkUser_phone)){
            log.info("没有设置审核人员的电话,不发送语音提醒");
            return true;
        }

        String user_name = params.get("user_name");
        String business_name = params.get("business_name");
        String legal_person = params.get("legal_person");
        String legal_phone = params.get("legal_phone");
        String business_phone = params.get("business_phone");
        String business_email = params.get("business_email");
        String business_address = params.get("business_address");
        String business_type = params.get("business_type");
        String businessOrFactory = params.get("businessOrFactory");
        String finance_person = params.get("finance_person");
        String finance_phone = params.get("finance_phone");
        String bank_type = params.get("bank_type");
        String bank_number = params.get("bank_number");
        String bank_addresss = params.get("bank_addresss");
        String account_name = params.get("account_name");
        String account_number = params.get("account_number");

        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】新公司-").append("用户账号:").append(user_name)
                .append(",企业名称:").append(business_name)
                .append(",法人:").append(legal_person)
                .append(",法人联系方式:").append(legal_phone)
                .append(",固定电话:").append(business_phone)
                .append(",电子邮件:").append(business_email)
                .append(",公司地址:").append(business_address)
                .append(",行业类别:").append(business_type)
                .append(",经销商/厂家:").append(businessOrFactory)
                .append(",财务负责人:").append(finance_person)
                .append(",财务联系电话:").append(finance_phone)
                .append(",开户银行:").append(bank_type)
                .append(",行号:").append(bank_number)
                .append(",开户行地址:").append(bank_addresss)
                .append(",开户名称:").append(account_name)
                .append(",账号:").append(account_number);
        String content = builder.toString();
        log.info("短信发送 手机号：{} ，发送内容：{}",checkUser_phone,content);
        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(checkUser_phone,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }

    //电话通知用户审核通过
    public boolean checkSuccess(String queryUser_phone,String queryUser_nick) {
        if(StringUtils.isBlank(queryUser_phone)){
            log.info("用户没有设置手机号,不发送语音提醒");
            return true;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(queryUser_nick);//多个的话,中间用逗号隔开
        String content = builder.toString();
        //尊敬的{1}用户,您提交的企业信息已通过审核,请重新登录
        log.info("短信语音 手机号：{} ，发送内容：{}",queryUser_phone,content);
        VoiceNotifyDTO voiceNotify = new VoiceNotifyDTO(accountSid,token,queryUser_phone,Integer.parseInt(templateId3),content);
        return VoiceNotifyUtil.sendVoiceMsg(voiceNotify);
    }

    //根据公司id发送审核不通过的短信,告知原因
    public boolean checkFail(String queryUser_phone, String why) {
        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】您提交的公司信息审核不通过--原因:").append(why).append(",").append("--请重新填写并提交");
        String content = builder.toString();
        log.info("短信发送 手机号：{} ，发送内容：{}",queryUser_phone,content);
        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(queryUser_phone,content);
        if(result.getCode() == Code.OK){
            return true;
        }
        return false;
    }

    //当三级酒店点击知道了,短信通知运营人员
    public boolean sendMsgOfZhiDaoLe(Map<String, String> params) {
        String caozuoren = params.get("caozuoren");
        String bookUser1 = params.get("bookUser1");
        String ota_phone1 = params.get("ota_phone1");
        //String ota_phone2 = params.get("ota_phone2");
        if(StringUtils.isBlank(ota_phone1)){
            log.info("没有OTA电话,不发送短信提醒");
        }
//        if(StringUtils.isBlank(ota_phone2)){
//            log.info("没有OTA电话,不发送短信提醒");
//        }
        String stringTodayTime = GetTimeUtils.getStringTodayTime();
        StringBuilder builder = new StringBuilder();
        builder.append("【秒宿乐易换】订单操作-").append(caozuoren).
                append("将").append(bookUser1).append("的订单确认了!").
                append("操作时间为:").append(stringTodayTime);
        String content = builder.toString();

        com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(ota_phone1,content);
//        com.yunpian.sdk.model.Result<SmsBatchSend> result2 = SmsUtil.multi_send(ota_phone2,content);

        if(result.getCode() == Code.OK){
            return true;
        }

        return false;
    }
    /***
     * 获得当前系统时间（字符串格式精确到秒）
     * @return
     */
    public static String getStringTodayTime(){
        Date todat_date = new Date();
        //将日期格式化
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //转换成字符串格式
        return simpleDateFormat.format(todat_date);
    }

	//当超级管理员点击取消,短信通知
    public boolean sendMsgOfQuXiao(Map<String, String> params) {
		String caozuoren = params.get("caozuoren");
		String bookUser1 = params.get("bookUser1");
		String ota_phone1 = params.get("ota_phone1");
		//String ota_phone2 = params.get("ota_phone2");
		if(StringUtils.isBlank(ota_phone1)){
			log.info("没有OTA电话,不发送短信提醒");
		}
//		if(StringUtils.isBlank(ota_phone2)){
//			log.info("没有OTA电话,不发送短信提醒");
//		}
		String stringTodayTime = GetTimeUtils.getStringTodayTime();
		StringBuilder builder = new StringBuilder();
		builder.append("【秒宿乐易换】订单操作-").append(caozuoren).
				append("将").append(bookUser1).append("的订单取消了!").
				append("操作时间为:").append(stringTodayTime);
		String content = builder.toString();

		com.yunpian.sdk.model.Result<SmsBatchSend> result = SmsUtil.multi_send(ota_phone1,content);
		//com.yunpian.sdk.model.Result<SmsBatchSend> result2 = SmsUtil.multi_send(ota_phone2,content);

		if(result.getCode() == Code.OK){
			return true;
		}

		return false;
	}

}
