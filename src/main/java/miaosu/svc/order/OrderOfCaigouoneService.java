package miaosu.svc.order;

import miaosu.dao.auto.OrderOfCaigouoneMapper;
import miaosu.dao.model.OrderOfCaigouone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/3/18 13:56
 * @Description:
 */
@Service
public class OrderOfCaigouoneService {

	@Autowired
	private OrderOfCaigouoneMapper orderOfCaigouoneMapper;

	//添加置单日历
	public void insert(OrderOfCaigouone orderOfCaigouone) {
		orderOfCaigouoneMapper.insert(orderOfCaigouone);
	}

	//查询比订单的置单日历表,根据酒店id和caigou_one_state=0的
	public List<OrderOfCaigouone> queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(Long order_id, int caigou_one_state) {
		return orderOfCaigouoneMapper.queryOrderOfCaigouoneListByorderIdAndCaigou_one_state(order_id,caigou_one_state);
	}

	//计算每单的置换总额
	public BigDecimal sumCaigoutotal(Long order_id, int caigou_one_state) {
		return orderOfCaigouoneMapper.sumCaigoutotal(order_id,caigou_one_state);
	}

	//修改置单日历
	public void update(OrderOfCaigouone orderOfCaigouone) {
		orderOfCaigouoneMapper.update(orderOfCaigouone);
	}

	//根据订单id查询所有置单日历表
	public List<OrderOfCaigouone> queryOrderOfCaigouoneListByorderId(Long order_id) {
		return orderOfCaigouoneMapper.queryOrderOfCaigouoneListByorderId(order_id);
	}
}
