package miaosu.svc.order;

import miaosu.dao.model.HotelOrderExample;
import miaosu.dao.model.HotelRefUser;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.vo.HotelOrderVO;
import miaosu.svc.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/13.
 */
@Component
public class OrderMsgService {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;
    @Autowired
    private OrderService orderService;
    @Autowired
    private HotelService hotelService;

    public void sendMsgByOrderId(Long orderId) {

        HotelOrderExample order = new HotelOrderExample();
        HotelOrderExample.Criteria and = order.createCriteria();
        and.andOrderIdEqualTo(orderId);

        PageResult<List<HotelOrderVO>> pageResult = orderService.queryOrderListBypage(order,0,50,false);
        if(pageResult.getTotal()==1){
            HotelOrderVO hotelOrderVO = pageResult.getRows().get(0);
            //相关所有客户都发消息
            List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByHotelId(hotelOrderVO.getHotelId());
            for(HotelRefUser user:hotelRefUsers){
                messagingTemplate.convertAndSend("/topic/order/add/"+user.getUserId(), pageResult);
            }
        }

    }

    public void sendMsg(Serializable currentUserId, PageResult pageResult) {
        messagingTemplate.convertAndSend("/topic/order/add/"+currentUserId, pageResult);
    }
}
