package miaosu.svc.log;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import miaosu.common.OpTypeEnum;
import miaosu.dao.auto.HotelRoomSetMapper;
import miaosu.dao.auto.OpLogMapper;
import miaosu.dao.auto.UserMapper;
import miaosu.dao.model.OpLog;
import miaosu.dao.model.OpLogExample;
import miaosu.svc.vo.PageResult;
import miaosu.svc.vo.qhh.OplogVO;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/1/10.
 */
@Service
public class OpLogService {
    @Autowired
    private OpLogMapper opLogMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private HotelRoomSetMapper roomSetMapper;

    //查询日志列表
    /**
     * 查询日志信息
     * @param hotelId 酒店id
     * @param search_date_type 操作时间/售卖时间 1 操作时间 2售卖时间
     * @param search_date_start 开始日期
     * @param search_date_end 结束日期
     * @param room_type_id 房间类型id
     * @param search_op_type 操作类型 0 全部操作 1房价，2房量，3开关房
     * @param offset 页码
     * @param limit 页数
     * @return
     */
    public PageResult queryLogListByPage(Long hotelId,
                                   String search_date_start,
                                   String search_date_end,
                                   Integer search_date_type,
                                   Long room_type_id,
                                   Integer search_op_type,
                                   Integer offset, Integer limit) {
        try {
            Page<Object> page = PageHelper.offsetPage(offset, limit);
            OpLogExample opLogExample = new OpLogExample();

            opLogExample.setOrderByClause("create_time desc");
            OpLogExample.Criteria criteria = opLogExample.createCriteria();

            criteria.andHotelIdEqualTo(hotelId);

            Date endDate = DateUtils.parseDate(search_date_end, "yyyy-MM-dd");

            if(search_date_type==2){
               //操作时间
                endDate = DateUtils.addDays(endDate, 1);
               criteria.andCreateTimeBetween(
                      DateUtils.parseDate(search_date_start,"yyyy-MM-dd"),
                       endDate);
            }
            if(search_date_type==1){
                //售卖时间
                criteria.andSellTimeBetween(
                        DateUtils.parseDate(search_date_start,"yyyy-MM-dd"),
                        endDate);
            }

           if(room_type_id!=0){
                criteria.andHotelRoomTypeIdEqualTo(room_type_id);
           }
           if(search_op_type!=0){
               criteria.andOpTypeEqualTo(search_op_type);
           }

          List<OpLog> logList = opLogMapper.selectByExample(opLogExample);

          List<OplogVO> opLogVOList  = convertLogVO(logList);

         long total = page.getTotal();


         System.out.println("json: "+ JsonUtils.objectToJson(new PageResult().success(total,opLogVOList)));
          return new PageResult().success(total,opLogVOList);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new PageResult();
    }

    /**
     * 转换OpLog
     * @param logList
     * @return
     */
    private List<OplogVO> convertLogVO(List<OpLog> logList) {
        List<OplogVO> oplogVOS = new ArrayList<OplogVO>();

        for(OpLog opLog : logList){
           OplogVO oplogVO = new OplogVO();
           BeanUtils.copyProperties(opLog,oplogVO);

           //转换姓名
           Long userId = oplogVO.getUserId();
           oplogVO.setUserName(userMapper.selectByPrimaryKey(userId).getUserName());
           //转换房型
            oplogVO.setHotelRoomTypeName(roomSetMapper.selectByPrimaryKey(oplogVO.getHotelRoomTypeId()).getHotelRoomTypeName());

           //转换操作类型
           oplogVO.setOpTypeName(OpTypeEnum.fromCode(oplogVO.getOpType()).getName());

           oplogVOS.add(oplogVO);
        }
        return  oplogVOS;
    }

    /**
     * 保存日志操作信息
     * @param oplogVO
     */
    public void saveOpLogs(OplogVO oplogVO) {
        //售卖时间，房型id,操作内容
        OpLog opLog = new OpLog();
        BeanUtils.copyProperties(oplogVO,opLog);
        //构造操作内容
        Integer opType = opLog.getOpType();
        String opValue = oplogVO.getOpValue();

        //用户id
		Long userId = oplogVO.getUserId();
		try {
            //获取时间
            Date startDate = DateUtils.parseDate(oplogVO.getStartDate(),"yyyy-MM-dd");
            Date endDate = DateUtils.parseDate(oplogVO.getEndDate(),"yyyy-MM-dd");


            for (Date d = startDate;d.getTime()<=endDate.getTime();d=DateUtils.addDays(d, 1)){
                Long[] roomTypeIds = oplogVO.getHotelRoomTypeIds();
                for(int i=0;i<roomTypeIds.length;i++) {
                    opLog.setHotelRoomTypeId(roomTypeIds[i]);
                    opLog.setSellTime(d);

                    String opConetnt = null;
                    String[] opValues = opValue.split(",");
                    if(opValues.length==1){
                        String content = oplogVO.getOpValue();
                        opConetnt = getOpContent(opType, content,userId);
                    }
                    if(opValues.length>1){
                        String content = opValues[i];
                        opConetnt = getOpContent(opType, content,userId);
                    }
                    opLog.setOpContent(opConetnt);
                    //插入
                    opLogMapper.insert(opLog);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private String getOpContent(Integer opType, String content, Long userId) {
        String opConetnt = null;
        if(opType == OpTypeEnum.ROOM_COUNT.getCode()){
            opConetnt= "房量设置为 "+content;
        }
        if(opType ==OpTypeEnum.ROOM_PRICE.getCode()){
            opConetnt= "房价设置为 "+content;
        }
        if(opType ==OpTypeEnum.ON_OFF_ROOM.getCode()){
            opConetnt= "设置为  "+(Integer.parseInt(content)==0?"关房":"开房");
        }
        //待确认
		if(opType ==OpTypeEnum.order_state_1.getCode() && userId!=32){
			opConetnt= "待确认单:"+content;
		}
		if(opType ==OpTypeEnum.order_state_1.getCode() && userId==32){
			opConetnt= "【订单数据-待确认单】:"+content;
		}

		//已确认
        if(opType ==OpTypeEnum.order_state_2.getCode() && userId!=32){
            opConetnt= content+"的订单被确认了";
        }
		if(opType ==OpTypeEnum.order_state_2.getCode() && userId==32){
			opConetnt= "【订单数据-确认单】"+content;
		}

		//已取消
        if(opType ==OpTypeEnum.order_state_4.getCode() && userId!=32){
            opConetnt= content+"的订单被取消了";
        }
		if(opType ==OpTypeEnum.order_state_4.getCode() && userId==32){
			opConetnt= "【订单数据-取消单】"+content;
		}

		//已确认取消
        if(opType ==OpTypeEnum.order_state_5.getCode() && userId!=32){
            opConetnt= content+"的订单被确认取消了";
        }
        if(opType ==OpTypeEnum.order_state_5.getCode() && userId==32){
			opConetnt= "【订单数据-已确认取消单】"+content;
		}

        //修改订单
        if(opType ==OpTypeEnum.updateOrder.getCode()){
            opConetnt= "【修改订单】"+content;
        }

      return opConetnt;
    }
}
