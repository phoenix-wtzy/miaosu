package miaosu.svc.address;

import miaosu.dao.model.City;
import miaosu.dao.model.Province;

import java.util.List;

/**
 * author  xieqx
 * date   2018/8/13
 * 城市服务接口
 */
public interface ProvinceService {
    /**
     * 查询省份信息
     * @param provinceCode 存在查询指定的省份信息
     * @return
     */
    List<Province> findProvinceList(String provinceCode);
}
