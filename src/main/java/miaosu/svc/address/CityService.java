package miaosu.svc.address;

import miaosu.dao.model.City;

import java.util.List;

/**
 * author  xieqx
 * date   2018/8/13
 * 城市服务接口
 */
public interface CityService {
    /**
     * 根据省份id 和所属渠道查询城市信息
     * @param provinceCode
     * @return
     */
    List<City> findCityList(String provinceCode);

    /**
     * 同属于省份的城市信息
     * @param cityCode
     * @return
     */
    List<City> findCitySibling(String cityCode);
}
