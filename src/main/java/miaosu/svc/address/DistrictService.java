package miaosu.svc.address;

import miaosu.dao.model.City;
import miaosu.dao.model.District;

import java.util.List;

/**
 * author  Administrator
 * date   2018/8/13
 * 区域服务接口
 */
public interface DistrictService {
    /**
     * 根据cityid 和所属渠道查询城市信息
     * @param cityCode
     * @return
     */
    List<District> findDistrictList(String cityCode);

    /**
     * 同属于城市的区域信息
     * @param cityCode
     * @return
     */
    List<District> findDistrictSibling(String cityCode);

}
