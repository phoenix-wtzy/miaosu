package miaosu.svc.address;

import com.google.common.base.Strings;
import miaosu.dao.auto.ProvinceMapper;
import miaosu.dao.model.District;
import miaosu.dao.model.DistrictExample;
import miaosu.dao.model.Province;
import miaosu.dao.model.ProvinceExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.Oneway;
import java.util.List;

/**
 * author  xieqx
 * date   2018/8/13
 * 城市服务接口
 */
@Service
public class ProvinceServiceImpl  implements ProvinceService{

    @Autowired
    private ProvinceMapper provinceMapper;


    /**
     * 查询省份信息
     * @param provinceCode 存在查询指定的省份信息(存在查询指定省份信息 否则查询所有)
     * @return
     */
    @Override
    public List<Province> findProvinceList(String provinceCode){

        ProvinceExample provinceExample = new ProvinceExample();
         ProvinceExample.Criteria criteria = provinceExample.createCriteria();
        if(!Strings.isNullOrEmpty(provinceCode)){
            criteria.andCodeEqualTo(provinceCode);
        }

        List<Province> provinces = provinceMapper.selectByExample(provinceExample);
        return provinces;
    }
}
