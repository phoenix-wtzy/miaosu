package miaosu.svc.address;

import miaosu.dao.auto.CityMapper;
import miaosu.dao.model.City;
import miaosu.dao.model.CityExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * author  xieqx
 * date   2018/8/13
 * 城市服务接口
 */
@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private CityMapper cityMapper;

    @Override
    public List<City> findCityList(String provinceCode) {
        CityExample cityExample = new CityExample();
        cityExample.createCriteria().andProvinceCodeEqualTo(provinceCode);
        List<City> cities = cityMapper.selectByExample(cityExample);
        return cities;
    }

    @Override
    public List<City> findCitySibling(String cityCode) {
        City city  = cityMapper.selectByPrimaryKey(cityCode);
        return findCityList(city.getProvinceCode());
    }
}
