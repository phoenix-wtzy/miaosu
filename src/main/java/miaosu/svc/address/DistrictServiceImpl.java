package miaosu.svc.address;

import miaosu.dao.auto.CityMapper;
import miaosu.dao.auto.DistrictMapper;
import miaosu.dao.model.City;
import miaosu.dao.model.CityExample;
import miaosu.dao.model.District;
import miaosu.dao.model.DistrictExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author  xieqx
 * date   2018/8/13
 * 区域服务接口
 */
@Service
public class DistrictServiceImpl implements DistrictService {
    @Autowired
    private DistrictMapper districtMapper;



    @Override
    public List<District> findDistrictList(String cityCode) {
        DistrictExample districtExample = new DistrictExample();
        districtExample.createCriteria().andCityCodeEqualTo(cityCode);
        List<District> districts = districtMapper.selectByExample(districtExample);
        return districts;
    }

    @Override
    public List<District> findDistrictSibling(String districtCode) {
        District district = districtMapper.selectByPrimaryKey(districtCode);
        return findDistrictList(district.getCityCode());
    }
}
