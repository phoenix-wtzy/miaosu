package miaosu.svc.hotel;

import miaosu.dao.auto.OtaDataMapper;
import miaosu.dao.model.OtaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @Date: 2021/4/12 15:56
 * @Description:
 */
@Component
public class OtaDataService {

	@Autowired
	private OtaDataMapper otaDataMapper;

	//添加酒店信息到ota_data数据表中
	public Long insert(OtaData otaData) {
		otaDataMapper.insert(otaData);
		return otaData.getId();
	}
}
