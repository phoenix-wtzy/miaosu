package miaosu.svc.hotel;

import miaosu.dao.model.HotelPriceSet;
import miaosu.dao.model.HotelRoomSet;
import miaosu.svc.vo.HotelPriceSetVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/8.
 */
public interface IHotelPriceSetService {

    long addPriceType(BigDecimal caigou_one, Long hotelId, String priceTypeName, int unbookType, int breakfastNum);

    int addPriceRefRoomType(Long hotelPriceId, Long[] hotelPriceIds);

    List<HotelPriceSet> queryByRoomTypeName(long hotelId, String priceTypeName);

    List<HotelPriceSetVO> queryHotelPriceSetList(long hotelId);

    void updatePriceType(Long hotePricelId, String priceTypeName, int unbookType, int breakfastNum, BigDecimal caigou_one);

    //根据价格类型ID获取关联的房型信息
    List<HotelRoomSet>  getHotelRoomSet(long hotelPriceId);

    void deleteByHotelPriceId(Long hotelId,long hotelPriceId);

    void updatePriceRefRoomType(long hotelPriceId, Long[] hotelRoomTypeIds);

    /**
     * 根据房型id获取价格计划
     * @param roomTypeId 房型id
     * @return 计划价格id
     */
    List<HotelPriceSet> selectPriceSetList(long roomTypeId);


    /**
     * 查询计划价格是否存在
     * @param hotelId
     * @param priceTypeName
     * @param unbookType
     * @param breakfastCount
     * @return
     */
    boolean hasExitPriceSet(long hotelId, String priceTypeName,int unbookType,int breakfastCount);

    //根据关联房型和早餐份数,去判断重复与否
    boolean ifExitPriceSet(Long hotel_price_id, long hotelId, int breakfastCount);
}
