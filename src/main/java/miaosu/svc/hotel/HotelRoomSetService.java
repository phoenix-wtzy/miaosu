package miaosu.svc.hotel;

import com.google.common.collect.Lists;
import miaosu.common.RoomTypeEnum;
import miaosu.dao.auto.HotelPriceRefRoomTypeMapper;
import miaosu.dao.auto.HotelRoomSetMapper;
import miaosu.dao.model.HotelRoomSet;
import miaosu.dao.model.HotelRoomSetExample;
import miaosu.svc.vo.HotelRoomSetVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class HotelRoomSetService {

    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;

    @Autowired
    private HotelPriceRefRoomTypeMapper hotelPriceRefRoomTypeMapper;
    @Autowired
    private HotelPriceSetService hotelPriceSetService;

    public int addRoomType(Long hotelId,String roomTypeName,int roomTypeId){
        HotelRoomSet record = new HotelRoomSet();
        record.setHotelId(hotelId);
        record.setHotelRoomTypeName(roomTypeName);
        record.setRoomTypeId(roomTypeId);
        int rows = hotelRoomSetMapper.insert(record);
        return rows;
    }

    public List<HotelRoomSet> queryByRoomTypeName(long hotelId,String roomTypeName){
        HotelRoomSetExample example = new HotelRoomSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId).andHotelRoomTypeNameEqualTo(roomTypeName);
        List<HotelRoomSet> hotelRoomSets = hotelRoomSetMapper.selectByExample(example);
        return hotelRoomSets;
    }

    public HotelRoomSet getHotelRoomTypeById(long hotelRoomTypeId){
        return hotelRoomSetMapper.selectByPrimaryKey(hotelRoomTypeId);
    }

    public String getHotelRoomTypeNameById(long hotelRoomTypeId){
        HotelRoomSet type = this.getHotelRoomTypeById(hotelRoomTypeId);
        if(null == type){
            return "";
        }
        return type.getHotelRoomTypeName();
    }

    /**
     * 根据酒店id查询酒店下所有的房间信息
     * @param hotelId
     * @return
     */
    public List<HotelRoomSetVO> queryHotelRoomSetList(long hotelId){
        List<HotelRoomSetVO> list = Lists.newArrayList();

        HotelRoomSetExample example = new HotelRoomSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        List<HotelRoomSet> hotelRoomSets = hotelRoomSetMapper.selectByExample(example);
        for(HotelRoomSet set :hotelRoomSets){
            HotelRoomSetVO vo = new HotelRoomSetVO();
            BeanUtils.copyProperties(set,vo);
            vo.setRoomTypeName(RoomTypeEnum.fromCode(vo.getRoomTypeId()).getName());
            list.add(vo);
        }
        return list;
    }

    public void updateRoomType(long hotelId,long hotelRoomTypeId, String roomTypeName, int roomTypeId) {
        HotelRoomSet hotelRoomSet = new HotelRoomSet();
        hotelRoomSet.setHotelRoomTypeName(roomTypeName);
        hotelRoomSet.setRoomTypeId(roomTypeId);
        hotelRoomSet.setHotelRoomTypeId(hotelRoomTypeId);
        hotelRoomSetMapper.updateByPrimaryKeySelective(hotelRoomSet);
    }

    //根据房间类型hotelRoomTypeId,查房间类型名称hotel_room_type_name,查hotel_room_set表
    public String queryHotelRoomTypeName(Long hotelRoomTypeId) {
        return hotelRoomSetMapper.queryHotelRoomTypeName(hotelRoomTypeId);
    }

    //根据酒店id和平台酒店房型名字到hotel_room_set表中去查询房型hotel_room_type_id
	public Long queryHotelRoomTypeIdByP_hotel_room_type_name(Long p_hotel_id, String p_hotel_room_type_name) {
        return hotelRoomSetMapper.queryHotelRoomTypeIdByP_hotel_room_type_name(p_hotel_id,p_hotel_room_type_name);
	}

    //1.先根据hotel_room_type_id到hotel_price_ref_room_type表中查询hotel_price_id,多个
    public List<Long> queryhotel_price_id(Long hotelRoomTypeId) {
        return hotelPriceRefRoomTypeMapper.queryhotel_price_id(hotelRoomTypeId);
    }

    //2.在根据hotel_price_id和hotel_id和breakfast_num的拼接查询caigou_one
    public BigDecimal querycaigou_one(Long hotel_price_id, Long p_hotel_id, Integer breakfast) {
        return hotelPriceSetService.querycaigou_one(hotel_price_id,p_hotel_id,breakfast);
    }
}
