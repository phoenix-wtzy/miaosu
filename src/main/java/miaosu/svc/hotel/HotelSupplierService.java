package miaosu.svc.hotel;

import miaosu.dao.auto.HotelSupplierMapper;
import miaosu.dao.model.Business;
import miaosu.dao.model.HotelSupplier;
import miaosu.dao.model.HotelSupplierExample;
import miaosu.svc.user.BusinessService;
import miaosu.svc.vo.HoteInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/2/3 11:52
 * @Description:
 */
@Component
public class HotelSupplierService {

	@Autowired
	private HotelSupplierMapper hotelSupplierMapper;
	@Autowired
	private BusinessService businessService;
	@Autowired
	private HotelService hotelService;

	//查询该酒店对应的所有关联供应商的信息
	public List<HotelSupplier> querySupplierListByHotelId(Long hotel_id, int page1, int limit) {
		/**
		 * page 当前页
		 * limit 每页显示多少天数据
		 * (page1-1)*limit   当前页为1,从第0开始
		 *                   当前页为2,从第10开始
		 *
		 *   select  * from hotel_supplier where hotel_id=?
		 *   order by hotel_supplier_id desc
		 *   LIMIT(page1-1)*limit,limit
		 */
		int page=(page1-1)*limit;
		List<HotelSupplier> hotelSupplierList =hotelSupplierMapper.querySupplierListByHotelId(hotel_id,page,limit);
		for (HotelSupplier hotelSupplier : hotelSupplierList) {
			Long business_id = hotelSupplier.getBusiness_id();
			Business business = businessService.queryBusinessByBusiness_id(business_id);
			hotelSupplier.setBusiness_name(business.getBusiness_name());
		}
		return  hotelSupplierList;
	}

	//添加酒店关联供应商信息
	public Long insert(HotelSupplier hotelSupplier) {
		 hotelSupplierMapper.insert(hotelSupplier);
		 return hotelSupplier.getHotel_supplier_id();
	}

	//查询与本酒店有关的hotel_supplier中总条数
	public int queryTotalSupplierList(Long hotel_id) {
		return hotelSupplierMapper.queryTotalSupplierList(hotel_id);
	}

	//修改
	public int update(HotelSupplier hotelSupplier) {
		return hotelSupplierMapper.update(hotelSupplier);
	}

    //根据封装条件查询hotel_supplier表中是否存在当前项
	public HotelSupplier queryHotelSupplier(HotelSupplierExample hotelSupplierExample) {
		return hotelSupplierMapper.queryHotelSupplier(hotelSupplierExample);
	}

	//根据主键id删除
	public int deleteByHotel_supplier_id(Long hotel_supplier_id) {
		return hotelSupplierMapper.deleteByHotel_supplier_id(hotel_supplier_id);
	}


	//上线
	public int updateHotelSupplierStateUP(Long hotel_supplier_id, int hotel_supplier_state, Date start_time) {
		return hotelSupplierMapper.updateHotelSupplierStateUP(hotel_supplier_id,hotel_supplier_state,start_time);
	}

	//下线
	public int updateHotelSupplierStateDown(Long hotel_supplier_id, int hotel_supplier_state, Date end_time) {
		return hotelSupplierMapper.updateHotelSupplierStateDown(hotel_supplier_id,hotel_supplier_state,end_time);
	}

	//根据供应商id查询名下关联酒店集合列表
	public List<HotelSupplier> querySupplierListByBusiness_id(Long business_id, int page1, int limit) {
		int page=(page1-1)*limit;
		List<HotelSupplier> hotelSupplierList =hotelSupplierMapper.querySupplierListByBusiness_id(business_id,page,limit);
		for (HotelSupplier hotelSupplier : hotelSupplierList) {
			Long hotel_id = hotelSupplier.getHotel_id();
			HoteInfoVO hoteInfoVO = hotelService.queryHotelInfoById(hotel_id);
			String hotelName = hoteInfoVO.getHotelName();
			hotelSupplier.setHotel_name(hotelName);
			Business business = businessService.queryBusinessByBusiness_id(business_id);
			hotelSupplier.setBusiness_name(business.getBusiness_name());
		}
		return  hotelSupplierList;
	}

	//查询与本供应商有关的hotel_supplier中酒店总条数
	public int queryTotalSupplierListByBusiness_id(Long business_id) {
		return hotelSupplierMapper.queryTotalSupplierListByBusiness_id(business_id);
	}

	//上线之前先查询有没有已经在线的,酒店关联供应商在同一时间内有且只能有一个关联
	public boolean queryHotelSupplierState(Long hotel_id) {
		int row= hotelSupplierMapper.queryHotelSupplierState(hotel_id);
		if (row>0){
			return true;
		}else {
			return false;
		}
	}
}
