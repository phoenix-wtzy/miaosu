package miaosu.svc.hotel;

import com.google.common.collect.Lists;
import miaosu.common.RoomTypeEnum;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.svc.vo.HotelProductVO;
import miaosu.svc.vo.HotelRoomSetVO;
import miaosu.svc.vo.HotelRoomStatusVO;
import miaosu.svc.vo.HotelStatusVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/17.
 */

@Service
public class HotelRoomStatusService {
    @Autowired
    private HotelRoomStatusMapper hotelRoomStatusMapper;

    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;
    @Autowired
    private HotelRoomCountMapper hotelRoomCountMapper;

    @Autowired
    private HotelPriceSetMapper hotelPriceSetMapper;

    @Autowired
    private HotelRoomPriceMapper hotelRoomPriceMapper;

    @Autowired
    private HotelPriceRefRoomTypeMapper  hotelPriceRefRoomTypeMapper;


    public List<HotelRoomStatus> queryStatus(Date start, Date end, Long hotelId) {

        HotelRoomStatusExample example = new HotelRoomStatusExample();
        example.createCriteria().andSellDateBetween(start,end).andHotelIdEqualTo(hotelId);
        List<HotelRoomStatus> hotelRoomStatuses = hotelRoomStatusMapper.selectByExample(example);
        return hotelRoomStatuses;
    }


    /**
     * 更新房态信息(没有的状态则为新增加)
     * @param hotelId 酒店id
     * @param productId 产品id
     * @param d 日期
     * @param roomStatus 房态状态信息
     */
    public void update(Long hotelId,Long productId, Date d, int roomStatus) {
        HotelRoomStatus record = new HotelRoomStatus();
        record.setStatus(roomStatus);
        HotelRoomStatusExample example = new HotelRoomStatusExample();

        example.createCriteria().andProductIdEqualTo(productId).andHotelIdEqualTo(hotelId).andSellDateEqualTo(d);
        if(hotelRoomStatusMapper.updateByExampleSelective(record,example)==0){
            record.setProductId(productId);
            record.setHotelId(hotelId);
            record.setSellDate(d);
            record.setCreateTime(new Date());
            hotelRoomStatusMapper.insertSelective(record);
        }
    }

    /**
     * 查询该酒店下的所有房态信息
     * @param hotelId 酒店id
     * @param start 开始日期
     * @param end 结束日期
     * @return  房态信息
     */
    public List<HotelRoomStatusVO> queryHotelStatusInfo(Long hotelId, Date start, Date end) {
        List<HotelRoomStatusVO> hotelRoomStatusVOList = Lists.newArrayList();
        //查询所有的房型
        HotelRoomSetExample example = new HotelRoomSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        List<HotelRoomSet> hotelRoomSets  = hotelRoomSetMapper.selectByExample(example);

        //遍历房型信息
        if(hotelRoomSets != null && hotelRoomSets.size()>=0){
            for(HotelRoomSet room : hotelRoomSets) {
                HotelRoomStatusVO hotelRoomStatusVO = new HotelRoomStatusVO();

                HotelRoomSetVO hotelRoomSetVO = new HotelRoomSetVO();
                BeanUtils.copyProperties(room, hotelRoomSetVO);
                hotelRoomSetVO.setRoomTypeName(RoomTypeEnum.fromCode(hotelRoomSetVO.getRoomTypeId()).getName());
                //设置房型信息
                hotelRoomStatusVO.setHotelRoomSetVO(hotelRoomSetVO);

                Long roomTypeId = room.getHotelRoomTypeId();
                String roomTypeName = room.getHotelRoomTypeName();
                //查询该房型下的所有的数量信息
                HotelRoomCountExample countsExample = new HotelRoomCountExample();
                countsExample.setOrderByClause("sell_date");
                countsExample.createCriteria().andHotelRoomTypeIdEqualTo(roomTypeId).andSellDateBetween(start, end);
                List<HotelRoomCount> hotelRoomCountsList = hotelRoomCountMapper.selectByExample(countsExample);

                //设置房量信息
                hotelRoomStatusVO.setHotelRoomCounts(hotelRoomCountsList);

                //查询房型下的产品信息 再根据产品id查询
                List<HotelPriceSet> hotelPriceSets = hotelPriceSetMapper.selectPriceSetByRoomTypeId(roomTypeId);
                List<HotelStatusVO> hotelStatusVOList = Lists.newArrayList();
                for(HotelPriceSet hotelPriceSet :hotelPriceSets){
                    HotelStatusVO hotelStatusVO = new HotelStatusVO();

                    HotelProductVO productVO  = new HotelProductVO();
                    BeanUtils.copyProperties(hotelPriceSet,productVO);

                    //设置产品id
                    HotelPriceRefRoomTypeExample priceRefRoomTypeExample  = new HotelPriceRefRoomTypeExample();
                    priceRefRoomTypeExample.createCriteria().andHotelRoomTypeIdEqualTo(roomTypeId).andHotelPriceIdEqualTo(hotelPriceSet.getHotelPriceId());

                    Long productId = null;
                    List<HotelPriceRefRoomType> hotelPriceRefRoomTypes =hotelPriceRefRoomTypeMapper.selectByExample(priceRefRoomTypeExample);
                    if(hotelPriceRefRoomTypes!=null&&hotelPriceRefRoomTypes.size()==1){
                        productId = hotelPriceRefRoomTypes.get(0).getProductId();
                        productVO.setProductId(productId);
                    }

                    HotelRoomStatusExample roomStatusExample = new HotelRoomStatusExample();
                    roomStatusExample.createCriteria().andProductIdEqualTo(productId).andSellDateBetween(start,end);
                    List<HotelRoomStatus> hotelRoomStatuses =  hotelRoomStatusMapper.selectByExample(roomStatusExample);

                    hotelStatusVO.setHotelProductVO(productVO);

                    hotelStatusVO.setHotelRoomStatusList(hotelRoomStatuses);

                    hotelStatusVOList.add(hotelStatusVO);
                }

                hotelRoomStatusVO.setHotelStatusVOs(hotelStatusVOList);
                hotelRoomStatusVOList.add(hotelRoomStatusVO);

            }
        }

        return hotelRoomStatusVOList;
    }

    //接受订单之前先获取某天某个房型的开关状态
    public Integer queryHotelStatus(Long p_hotel_id, Long productId, Date sell_date) {
        return hotelRoomStatusMapper.queryHotelStatus(p_hotel_id,productId,sell_date);
    }
}
