package miaosu.svc.hotel;

import com.google.common.collect.Lists;
import miaosu.common.RoomTypeEnum;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.svc.vo.HotelPriceVO;
import miaosu.svc.vo.HotelProductVO;
import miaosu.svc.vo.HotelRoomPriceVO;
import miaosu.svc.vo.HotelRoomSetVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/17.
 */

@Service
public class HotelRoomPriceService {
    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;
    @Autowired
    private HotelPriceRefRoomTypeMapper hotelPriceRefRoomTypeMapper;

    @Autowired
    private HotelRoomCountMapper hotelRoomCountMapper;


    @Autowired
    private HotelPriceSetMapper hotelPriceSetMapper;

    @Autowired
    private HotelRoomPriceMapper hotelRoomPriceMapper;

    /**
     * 查询该酒店下的所有房价信息
     * @param hotelId 酒店id
     * @param start 开始日期
     * @param end 结束日期
     * @return  房价信息
     */
    public  List<HotelRoomPriceVO> queryHotelPriceInfo(Long hotelId,Date start,Date end) {
        List<HotelRoomPriceVO> hotelRoomPriceVOList = Lists.newArrayList();
        //查询所有的房型
        HotelRoomSetExample example = new HotelRoomSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        List<HotelRoomSet> hotelRoomSets  = hotelRoomSetMapper.selectByExample(example);

        //遍历房型信息
        if(hotelRoomSets != null && hotelRoomSets.size()>=0){
            for(HotelRoomSet room : hotelRoomSets){
                HotelRoomPriceVO hotelRoomPriceVO = new HotelRoomPriceVO();

                HotelRoomSetVO hotelRoomSetVO = new HotelRoomSetVO();
                BeanUtils.copyProperties(room,hotelRoomSetVO);
                hotelRoomSetVO.setRoomTypeName(RoomTypeEnum.fromCode(hotelRoomSetVO.getRoomTypeId()).getName());
                hotelRoomPriceVO.setHotelRoomSetVO(hotelRoomSetVO);

                Long  roomTypeId = room.getHotelRoomTypeId();
                String  roomTypeName = room.getHotelRoomTypeName();
                //查询所有的数量信息
                HotelRoomCountExample countsExample = new HotelRoomCountExample();
                countsExample.setOrderByClause("sell_date");
                countsExample.createCriteria().andHotelRoomTypeIdEqualTo(roomTypeId).andSellDateBetween(start,end);
                List<HotelRoomCount> hotelRoomCountsList  = hotelRoomCountMapper.selectByExample(countsExample);

                hotelRoomPriceVO.setHotelRoomCounts(hotelRoomCountsList);

                //价格计划以及价格信息
                //查询该房型下的所有计划价格
                List<HotelPriceSet> hotelPriceSets = hotelPriceSetMapper.selectPriceSetByRoomTypeId(roomTypeId);
                //查询产品
                List<HotelPriceVO> hotelPriceVOList = Lists.newArrayList();
                for(HotelPriceSet hotelPriceSet :hotelPriceSets){
                    HotelPriceVO hotelPriceVO = new HotelPriceVO();


                    HotelProductVO productVO  = new HotelProductVO();
                    BeanUtils.copyProperties(hotelPriceSet,productVO);
                    //设置产品id
                    HotelPriceRefRoomTypeExample priceRefRoomTypeExample  = new HotelPriceRefRoomTypeExample();
                    priceRefRoomTypeExample.createCriteria().andHotelRoomTypeIdEqualTo(roomTypeId).andHotelPriceIdEqualTo(hotelPriceSet.getHotelPriceId());

                    Long productId = null;
                    List<HotelPriceRefRoomType> hotelPriceRefRoomTypes =hotelPriceRefRoomTypeMapper.selectByExample(priceRefRoomTypeExample);
                    if(hotelPriceRefRoomTypes!=null&&hotelPriceRefRoomTypes.size()==1){
                        productId = hotelPriceRefRoomTypes.get(0).getProductId();
                        productVO.setProductId(productId);
                    }

                    HotelRoomPriceExample roomPriceExample = new HotelRoomPriceExample();
                    roomPriceExample.createCriteria().andProductIdEqualTo(productId).andSellDateBetween(start,end);
                    List<HotelRoomPrice> hotelRoomPrices =  hotelRoomPriceMapper.selectByExample(roomPriceExample);
                    for (HotelRoomPrice hotelRoomPrice : hotelRoomPrices) {
                        hotelRoomPrice.setHotel_price_name(hotelPriceSet.getHotelPriceName());
                        hotelRoomPrice.setHotel_price_id(hotelPriceSet.getHotelPriceId());
                    }
                    hotelPriceVO.setHotelProductVO(productVO);

                    hotelPriceVO.setHotelRoomPrices(hotelRoomPrices);
                    hotelPriceVOList.add(hotelPriceVO);
                }

                hotelRoomPriceVO.setHotelPriceVOs(hotelPriceVOList);

                hotelRoomPriceVOList.add(hotelRoomPriceVO);
            }
         }

        return hotelRoomPriceVOList;

    }

    public void update(Long productId, Date d, String  prices) {
        //将价格转换为Decimal
        BigDecimal decimal = new BigDecimal(prices);
        //价格保留两位小数
        BigDecimal price = decimal.setScale(2,BigDecimal.ROUND_HALF_DOWN);
        HotelRoomPrice roomPrice = new HotelRoomPrice();
        roomPrice.setPrice(price);

        HotelRoomPriceExample example = new HotelRoomPriceExample();
        example.createCriteria().andProductIdEqualTo(productId).andSellDateEqualTo(d);
        if(hotelRoomPriceMapper.updateByExampleSelective(roomPrice,example)==0){
            roomPrice.setProductId(productId);
            roomPrice.setSellDate(d);
            hotelRoomPriceMapper.insertSelective(roomPrice);
        }
    }



    //根据产品id和日期去查询对应时间的价格
    public BigDecimal queryPrice(Long productId, Date d1) {
        return hotelRoomPriceMapper.queryPrice(productId,d1);
    }


}
