package miaosu.svc.hotel;

import miaosu.dao.auto.HotelEvaluationMapper;
import miaosu.dao.model.HotelEvaluation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/4/12 9:36
 * @Description:
 */
@Component
public class HotelEvaluationService {

	@Autowired
	private HotelEvaluationMapper hotelEvaluationMapper;

	//查询当前用户名下填写的所有酒店评估信息
	public List<HotelEvaluation> queryAllByUserId(Long currentUserId, int page1, int limit) {
		int page=(page1-1)*limit;
		List<HotelEvaluation>hotelEvaluationList=hotelEvaluationMapper.queryAllByUserId(currentUserId,page,limit);
		return hotelEvaluationList;
	}

	//查询当前用户名下酒店信息总条数
	public int queryTotalByUserId(Long currentUserId) {
		return hotelEvaluationMapper.queryTotalByUserId(currentUserId);
	}

	//新增酒店评估到hotel_evaluation表中
	public Long insert(HotelEvaluation hotelEvaluation) {
		hotelEvaluationMapper.insert(hotelEvaluation);
		return hotelEvaluation.getId();
	}
}
