package miaosu.svc.hotel;

import miaosu.dao.auto.HotelPriceRefRoomTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private HotelPriceRefRoomTypeMapper productMapper;

    /**
     * 根据产品获取对应的房型id
     * @param hotelId
     * @param productIds
     * @return
     */
   public Long[] queryRoomTypeIdByProductId(Long hotelId,Long[] productIds){

       List<Long> roomTypeIds = new ArrayList<Long>();
       for(Long productId:productIds) {
           Long hotelRoomTypeId = productMapper.selectByPrimaryKey(productId).getHotelRoomTypeId();
           roomTypeIds.add(hotelRoomTypeId);
       }
       Long[] array = roomTypeIds.toArray(new Long[0]);
       return array;

   }
}
