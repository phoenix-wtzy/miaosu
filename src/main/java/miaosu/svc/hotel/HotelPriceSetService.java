package miaosu.svc.hotel;

import com.google.common.collect.Lists;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.svc.vo.HotelPriceSetVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ArrayUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/2/9.
 */
@Service
public class HotelPriceSetService implements IHotelPriceSetService {
    @Autowired
    private HotelPriceSetMapper hotelPriceSetMapper;
    @Autowired
    private HotelPriceRefRoomTypeMapper hotelPriceRefRoomTypeMapper;
    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;
    @Autowired
    private HotelRoomPriceMapper hotelRoomPriceMapper;
    @Autowired
    private HotelRoomStatusMapper hotelRoomStatusMapper;

    @Override
    public long addPriceType(BigDecimal caigou_one, Long hotelId, String priceTypeName, int unbookType, int breakfastNum){
        HotelPriceSet record = new HotelPriceSet();
        record.setHotelId(hotelId);
        record.setHotelPriceName(priceTypeName);
        record.setUnbookType(unbookType);
        record.setBreakfastNum(breakfastNum);
        record.setCaigou_one(caigou_one);
        hotelPriceSetMapper.insert(record);

        Long hotelPriceId = record.getHotelPriceId();


        return hotelPriceId;
    }

    @Override
    public int addPriceRefRoomType(Long hotelPriceId, Long[] hotelPriceIds){

        for(long hotelRoomTypeId : hotelPriceIds){
            HotelPriceRefRoomTypeExample example = new HotelPriceRefRoomTypeExample();
            example.createCriteria().andHotelPriceIdEqualTo(hotelPriceId).andHotelRoomTypeIdEqualTo(hotelRoomTypeId);
            List<HotelPriceRefRoomType> hotelPriceRefRoomTypes = hotelPriceRefRoomTypeMapper.selectByExample(example);
            if (hotelPriceRefRoomTypes.size()==0){
                HotelPriceRefRoomType record = new HotelPriceRefRoomType();
                record.setHotelPriceId(hotelPriceId);
                record.setHotelRoomTypeId(hotelRoomTypeId);
                hotelPriceRefRoomTypeMapper.insert(record);
            }
        }
        return 0;
    }

    @Override
    public List<HotelPriceSet> queryByRoomTypeName(long hotelId, String priceTypeName){
        HotelPriceSetExample example = new HotelPriceSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId).andHotelPriceNameEqualTo(priceTypeName);
        List<HotelPriceSet> hotelPriceSet = hotelPriceSetMapper.selectByExample(example);
        return hotelPriceSet;
    }

    @Override
    public List<HotelPriceSetVO> queryHotelPriceSetList(long hotelId){
        List<HotelPriceSetVO> list = Lists.newArrayList();

        HotelPriceSetExample example = new HotelPriceSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        List<HotelPriceSet> hotelRoomSets = hotelPriceSetMapper.selectByExample(example);
        for(HotelPriceSet set :hotelRoomSets){
            HotelPriceSetVO vo = new HotelPriceSetVO();
            BeanUtils.copyProperties(set,vo);
            Long hotelPriceId = set.getHotelPriceId();
            vo.setHotelRoomSetList(getHotelRoomSet(hotelPriceId));
            list.add(vo);
        }
        return list;
    }

    @Override
    public void updatePriceType(Long hotePricelId, String priceTypeName, int unbookType, int breakfastNum, BigDecimal caigou_one) {
        HotelPriceSet record = new HotelPriceSet();
        record.setHotelPriceId(hotePricelId);
        record.setHotelPriceName(priceTypeName);
        record.setUnbookType(unbookType);
        record.setBreakfastNum(breakfastNum);
        record.setCaigou_one(caigou_one);
        hotelPriceSetMapper.updateByPrimaryKeySelective(record);

        HotelPriceSet hotelPriceSet = hotelPriceSetMapper.selectByPrimaryKey(hotePricelId);

    }


    //根据价格类型ID获取关联的房型信息
    @Override
    public List<HotelRoomSet>  getHotelRoomSet(long hotelPriceId){
        List<HotelRoomSet> list = Lists.newArrayList();
        HotelPriceRefRoomTypeExample example = new HotelPriceRefRoomTypeExample();
        example.createCriteria().andHotelPriceIdEqualTo(hotelPriceId);
        List<HotelPriceRefRoomType> hotelPriceRefRoomTypes = hotelPriceRefRoomTypeMapper.selectByExample(example);
        for(HotelPriceRefRoomType type :hotelPriceRefRoomTypes){
            HotelRoomSet hotelRoomSet = hotelRoomSetMapper.selectByPrimaryKey(type.getHotelRoomTypeId());
            list.add(hotelRoomSet);
        }
        return list;
    }


    @Override
    public void deleteByHotelPriceId(Long hotelId,long hotelPriceId) {
        HotelPriceSet hotelPriceSet = hotelPriceSetMapper.selectByPrimaryKey(hotelPriceId);
        HotelPriceRefRoomTypeExample hotelPriceRefRoomTypeExample = new HotelPriceRefRoomTypeExample();
        hotelPriceRefRoomTypeExample.createCriteria().andHotelPriceIdEqualTo(hotelPriceId);
        List<HotelPriceRefRoomType>  hotelPriceRefRoomTypes = hotelPriceRefRoomTypeMapper.selectByExample(hotelPriceRefRoomTypeExample);

        HotelPriceRefRoomTypeExample example = new HotelPriceRefRoomTypeExample();
        example.createCriteria().andHotelPriceIdEqualTo(hotelPriceId);
        hotelPriceRefRoomTypeMapper.deleteByExample(example);

        if(hotelId == null){
            hotelId = hotelPriceSet.getHotelId();
        }

        hotelPriceSetMapper.deleteByPrimaryKey(hotelPriceId);

        //该价格计划下的所有产品需要删除
        for(HotelPriceRefRoomType hotelPriceRefRoomType : hotelPriceRefRoomTypes){
            //有可能该产品还没有进行进行推送
            //查询价格 和房态
            Long productId = hotelPriceRefRoomType.getProductId();
        }
    }

    private boolean haveProductStatus(Long productId) {
        HotelRoomStatusExample roomStatusExample = new HotelRoomStatusExample();
        roomStatusExample.createCriteria().andProductIdEqualTo(productId);
        return hotelRoomStatusMapper.selectByExample(roomStatusExample).size()>0?true:false;
    }

    private boolean haveProductPrice(Long productId) {
        HotelRoomPriceExample roomPriceExample = new HotelRoomPriceExample();
        roomPriceExample.createCriteria().andProductIdEqualTo(productId);
        return hotelRoomPriceMapper.selectByExample(roomPriceExample).size()>0?true:false;
    }

    @Override
    public void updatePriceRefRoomType(long hotelPriceId, Long[] hotelRoomTypeIds) {
        //找需要删除的ID
        HotelPriceRefRoomTypeExample example = new HotelPriceRefRoomTypeExample();
        example.createCriteria().andHotelPriceIdEqualTo(hotelPriceId);
        List<HotelPriceRefRoomType> hotelPriceRefRoomTypes = hotelPriceRefRoomTypeMapper.selectByExample(example);
        for(HotelPriceRefRoomType refType: hotelPriceRefRoomTypes){
            if(!ArrayUtils.contains(hotelRoomTypeIds,refType.getHotelRoomTypeId())){
                HotelPriceRefRoomTypeExample exampleDel = new HotelPriceRefRoomTypeExample();
                exampleDel.createCriteria().andHotelPriceIdEqualTo(hotelPriceId).andHotelRoomTypeIdEqualTo(refType.getHotelRoomTypeId());
                hotelPriceRefRoomTypeMapper.deleteByExample(exampleDel);
            }
        }

        //找出需要添加的ID
        this.addPriceRefRoomType(hotelPriceId,hotelRoomTypeIds);
    }

    /**
     * 根据房型id 获取价格计划
     * @param roomTypeId 房型id
     * @return 计划价格id
     */
    @Override
    public List<HotelPriceSet> selectPriceSetList(long roomTypeId) {
        List<HotelPriceSet> list = Lists.newArrayList();
        //房型和价格计划的关联表
        HotelPriceRefRoomTypeExample example = new HotelPriceRefRoomTypeExample();
        example.createCriteria().andHotelRoomTypeIdEqualTo(roomTypeId);
        List<HotelPriceRefRoomType> hotelPriceRefRoomTypes = hotelPriceRefRoomTypeMapper.selectByExample(example);
        for(HotelPriceRefRoomType type :hotelPriceRefRoomTypes){
            HotelPriceSet hotelPriceSet = hotelPriceSetMapper.selectByPrimaryKey(type.getHotelPriceId());
            list.add(hotelPriceSet);
        }
        return list;
    }

    @Override
    public boolean hasExitPriceSet(long hotelId, String priceTypeName, int unbookType, int breakfastCount) {
        HotelPriceSetExample example = new HotelPriceSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId).andHotelPriceNameEqualTo(priceTypeName).andUnbookTypeEqualTo(unbookType)
                .andBreakfastNumEqualTo(breakfastCount);
        List<HotelPriceSet> hotelPriceSets = hotelPriceSetMapper.selectByExample(example);
        if(hotelPriceSets!=null&&hotelPriceSets.size()>0){
            return true;
        }
        return false;
    }

    //根据关联房型和早餐份数,去判断重复与否
    @Override
    public boolean ifExitPriceSet(Long hotel_price_id, long hotelId, int breakfastCount) {
        HotelPriceSetExample example = new HotelPriceSetExample();
        example.createCriteria().andHotelPriceIdEqualTo(hotel_price_id).andHotelIdEqualTo(hotelId).andBreakfastNumEqualTo(breakfastCount);
        List<HotelPriceSet> hotelPriceSets = hotelPriceSetMapper.selectByExample(example);
        if(hotelPriceSets!=null&&hotelPriceSets.size()>0){
            return true;
        }
        return false;
    }

    //根据产品productId,查询房间类型hotelRoomTypeId,查hotel_price_ref_room_type表
    public Long queryHotelRoomTypeIdByProductId(Long productId) {
        return hotelPriceRefRoomTypeMapper.queryHotelRoomTypeIdByProductId(productId);
    }

    //2.在根据hotel_price_id和hotel_id和breakfast_num的拼接查询caigou_one
    public BigDecimal querycaigou_one(Long hotel_price_id, Long p_hotel_id, Integer breakfast) {
        return hotelPriceSetMapper.querycaigou_one(hotel_price_id,p_hotel_id,breakfast);
    }
}
