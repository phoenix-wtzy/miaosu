package miaosu.svc.hotel;

import miaosu.dao.auto.HotelPriceRefRoomTypeMapper;
import miaosu.dao.auto.HotelRoomCountMapper;
import miaosu.dao.model.HotelPriceRefRoomType;
import miaosu.dao.model.HotelPriceRefRoomTypeExample;
import miaosu.dao.model.HotelRoomCount;
import miaosu.dao.model.HotelRoomCountExample;
import miaosu.runtimeexception.ExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author  xieqx
 * @date  2018-08-01
 * @Description 房量相关的service
 */
@Service
public class HotelRoomCountsService {
    private Logger logger = LoggerFactory.getLogger(HotelRoomCountsService.class);
    @Autowired
    private HotelRoomCountMapper hotelRoomCountMapper;

    @Autowired
    private HotelPriceRefRoomTypeMapper productMapper;


    /**
     * 更新酒店指定房型的房量信息
     * @param hotelRoomTypeId 房间类型
     * @param d 售卖日期
     * @param roomCount 房量
     */
    @Transactional(rollbackFor = ExistException.class,readOnly = false)
    public void update(Long hotelRoomTypeId, Date d, int roomCount) {
        HotelRoomCount roomCounts = new HotelRoomCount();
        roomCounts.setCountRoom(roomCount);
        HotelRoomCountExample example = new HotelRoomCountExample();
        example.createCriteria().andHotelRoomTypeIdEqualTo(hotelRoomTypeId).andSellDateEqualTo(d);
        //对指定的房量信息进行更新 如果不存在而为新增
        if(hotelRoomCountMapper.updateByExampleSelective(roomCounts,example)==0){
            roomCounts.setHotelRoomTypeId(hotelRoomTypeId);
            roomCounts.setSellDate(d);
            hotelRoomCountMapper.insertSelective(roomCounts);
        }
    }

    /**
     *  根据房型信息查询产品id
     * @param hotelRoomTypeId
     * @return
     */
    public Long findProductId(Long hotelRoomTypeId) {
        //根据房型信息查询产品id
        HotelPriceRefRoomTypeExample productExample = new HotelPriceRefRoomTypeExample();
        productExample.createCriteria().andHotelRoomTypeIdEqualTo(hotelRoomTypeId);

        List<HotelPriceRefRoomType> products =productMapper.selectByExample(productExample);
        if(products!=null&&products.size()!=1){
            logger.warn("业务中不允许一个房型对应多个价格计划");
            return  null;
        }
        Long productId = products.get(0).getProductId();
        return productId;
    }

    //查询数据库的总房量
    public Integer findCountRoom(Long hotelRoomTypeId, Date sell_date) {
        return hotelRoomCountMapper.findCountRoom(hotelRoomTypeId,sell_date);
    }
}
