package miaosu.svc.hotel;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import miaosu.common.ChannelEnum;
import miaosu.common.RoleEnum;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.runtimeexception.ExistException;
import miaosu.svc.address.ProvinceService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.HoteInfoVO;
import miaosu.svc.vo.PageResult;
import miaosu.web.mvc.RoomPriceController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/1/10.
 */
@Service
public class HotelService {
    private static final Logger logger = LoggerFactory.getLogger(RoomPriceController.class);

    @Autowired
    private HotelInfoMapper hotelInfoMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private HotelRefUserMapper hotelRefUserMapper;

    @Autowired
    private ProvinceMapper provinceMapper;

    @Autowired
    private CityMapper cityMapper;

    @Autowired
    private DistrictMapper districtMapper;

    @Autowired
    private OtaProvinceMapper otaProvinceMapper;

    @Autowired
    private OtaCityMapper otaCityMapper;

    @Autowired
    private OtaDistrictMapper otaDistrictMapper;

    @Autowired
    private HotelInfoExtMapper hotelInfoExtMapper;

    @Autowired
    private ProvinceService provinceService;

    //手机号规则
    private static String PATTEN_REGEX_PHONE= "^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$";

    public PageResult queryHotelListWithPage(Long userId,String hotelName,Long hotelId, int offset,int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<HotelInfo> hotelList = this.queryHotelList(userId,hotelName,hotelId);
        HotelInfo hotelInfo = new HotelInfo();
        hotelInfo.setHotelId(0L);
        hotelInfo.setHotelName("全部酒店");
        hotelInfo.setUserId(32L);
        hotelList.add(0,hotelInfo);
        long total = objects.getTotal();
        return new PageResult().success(total,hotelList);
    }
    public List<HotelInfo> queryHotelList(Long userId,String hotelName,Long hotelId) {
        List<HotelInfo> hotelInfos = hotelInfoMapper.selectByUserAndName(userId,hotelName,hotelId);
        return hotelInfos;
    }

    public PageResult queryHotelListBypage(Long userId,int offset,int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<HoteInfoVO> hotelList = this.queryHotelList(userId);
        long total = objects.getTotal();
        return new PageResult().success(total,hotelList);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public List<HoteInfoVO> queryHotelList(Long userId) {
        List<HoteInfoVO> list = Lists.newArrayList();
//        List<HotelInfo> hotelInfos = hotelInfoMapper.selectByExample(order);
        List<HotelInfo> hotelInfos = this.queryHotelList(userId,null,null);

        //TODO 循环查库，要限制个数，这里可能会有坑
        for (HotelInfo hotelInfo : hotelInfos){
            HoteInfoVO vo = new HoteInfoVO();
            BeanUtils.copyProperties(hotelInfo,vo);

            User user = userService.getUserById(hotelInfo.getUserId());
            if(null!=user){
                BeanUtils.copyProperties(user,vo);
            }
            list.add(vo);
        }
        return list;
    }

    /**
     * 获取酒店信息，酒店名称是唯一的
     * @param hotelName
     * @return
     */
    public List<HotelInfo> getHotelInfoByName(String hotelName){
        HotelInfoExample example = new HotelInfoExample();
        example.createCriteria().andHotelNameEqualTo(hotelName);
        List<HotelInfo> hotelInfos = hotelInfoMapper.selectByExample(example);
        return hotelInfos;
    }
    public HotelInfo getHotelInfoById(Long hotelId){
        if(hotelId == null){
            return null;
        }
        HotelInfoExample example = new HotelInfoExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        return hotelInfoMapper.selectByPrimaryKey(hotelId);
    }

    /**
     * 添加酒店信息
     * @param hotelName
     * @param address
     * @param contact
     * @param contactMobile
     * @param userId
     * @param hotelType 销售类型
     * @param settle_totalMoney 协议置换总金额
     * @param settle_totalNight 协议置换间夜数
     * @param baoFang_totalMoney 包房总金额
     * @param touZiRen_rates 投资人分润比例
     * @param isActive 酒店有效性
     * @return
     */
    public Long addHotelInfo(String hotelName, String hotel_only_id, String address, String contact, String contactMobile, Long userId, String receptionPhone, String ota_phone, int hotelType, String settle_totalMoney, String settle_totalNight, String baoFang_totalMoney, String touZiRen_rates, int isActive){
        List<HotelInfo> hotelInfoByName = this.getHotelInfoByName(hotelName);
        if (hotelInfoByName.size()==0){
            HotelInfo record = new HotelInfo();
            record.setHotelName(hotelName);
            record.setHotel_only_id(hotel_only_id);
            record.setAddress(address);
            record.setContact(contact);
            record.setContactMobile(contactMobile);
            record.setReceptionPhone(receptionPhone);
            record.setUserId(userId);
            record.setOta_phone(ota_phone);
            //酒店创建时间
            record.setCreateTime(new Date());
            //默认设置酒店为有效
            record.setIsActive(isActive);
            //销售类型,1--a类  2--b类
            record.setHotelType(hotelType);
            //协议置换总金额
            record.setSettle_totalMoney(settle_totalMoney);
            //协议置换间夜数
            record.setSettle_totalNight(settle_totalNight);
            //包房总金额
            record.setBaoFang_totalMoney(baoFang_totalMoney);
            //投资人分润比例
            record.setTouZiRen_rates(touZiRen_rates);
            hotelInfoMapper.insert(record);
            return record.getHotelId();
        }
        return null;
    }

    /**
     * 添加酒店信息以及扩展信息
     * @param hotelName 酒店名称
     * @param address 地址
     * @param contact 联系人
     * @param contactMobile 联系电话
     * @param accountName 账号名称
     * @param accountPassword 账号密码
     * @param nickName 别名
     * @param admin 是否为管理员
     * @param currentUserId 当前用户id
     * @param provinceCode  省份名
     * @param cityCode 城市名
     * @param districtCode 区域名
     * @param hotelType 销售类型
     * @param settle_totalMoney 协议置换总金额
     * @param settle_totalNight 协议置换间夜数
     * @param baoFang_totalMoney 包房总金额
     * @param touZiRen_rates 投资人分润比例
     * @param isActive 酒店有效性
     */
    @Transactional(rollbackFor={ExistException.class})
    public void  addHotelAndUser(String hotelName, String hotel_only_id, String address, String contact, String contactMobile, String accountName, String accountPassword, String nickName, boolean admin, Long currentUserId,
                                 String provinceCode, String cityCode, String districtCode, String receptionPhone, String ota_phone, int hotelType, String settle_totalMoney, String settle_totalNight, String baoFang_totalMoney, String touZiRen_rates, int isActive){
//        //管理账号应为手机号
//        if (!accountName.matches(PATTEN_REGEX_PHONE)) {
//            throw new ExistException("管理账号应为手机号");
//        }
        //添加用户和相关权限
        Long userId = userService.insertUser(accountName, accountPassword, nickName, RoleEnum.ROLE_USER);
        if(null == userId){
            throw new ExistException("用户已经存在");
        }
        //添加酒店信息
        Long hotelId = this.addHotelInfo(hotelName,hotel_only_id, address, contact, contactMobile, userId,receptionPhone,ota_phone,hotelType,settle_totalMoney,settle_totalNight,baoFang_totalMoney,touZiRen_rates,isActive);
        //添加酒店扩展信息
        addHotelInfoExt(hotelId,provinceCode,cityCode,districtCode);
        if(null == hotelId){
            throw new ExistException("酒店已经存在");
        }
        //添加关联关系
        if(null != hotelId){
            this.addHotelRefUser(hotelId,userId);
            userService.setForCurHotel(userId,hotelId);
            if(admin){
                this.addHotelRefUser(hotelId,currentUserId);
            }
        }
    }

    /**
     * 添加酒店的扩展信息
     * @param hotelId
     * @param provinceCode
     * @param cityCode
     * @param districtCode
     */
	public void addHotelInfoExt(Long hotelId, String provinceCode, String cityCode, String districtCode) {
         //保存本地的酒店信息，并添加对应的渠道省市县信息
        HotelInfoExt infoExt = new HotelInfoExt();
        infoExt.setChannel(0);
        infoExt.setHotelId(hotelId);
        infoExt.setProvinceCode(provinceCode);
        infoExt.setCityCode(cityCode);
        infoExt.setDistrictCode(districtCode);

        //保存本地酒店信息
        hotelInfoExtMapper.insertSelective(infoExt);

        //查询所有的渠道信息
        for(ChannelEnum channelEnum : ChannelEnum.values()){
             HotelInfoExt otaInfoExt = new HotelInfoExt();
             int channel = channelEnum.getCode();
             if(channel == 0) {
                 continue;
             }
             otaInfoExt.setHotelId(hotelId);
             otaInfoExt.setChannel(channel);
             //省份
             OtaProvinceExample otaProvinceExample = new OtaProvinceExample();
            otaProvinceExample.createCriteria().andChannelEqualTo(channel)
                    .andProvinceCodeEqualTo(provinceCode);

            List<OtaProvince> otaProvinces = otaProvinceMapper.selectByExample(otaProvinceExample);
            if(otaProvinces!=null&&otaProvinces.size()==1){
                otaInfoExt.setProvinceCode(otaProvinces.get(0).getCode());
            }
            //城市
            OtaCityExample otaCityExample = new OtaCityExample();
            otaCityExample.createCriteria().andChannelEqualTo(channel)
                    .andCityCodeEqualTo(cityCode);

            List<OtaCity> otaCities = otaCityMapper.selectByExample(otaCityExample);
            if(otaCities!=null&&otaCities.size()==1){
                otaInfoExt.setCityCode(otaCities.get(0).getCode());
            }
            //区域
            OtaDistrictExample otaDistrictExample = new OtaDistrictExample();
            otaDistrictExample.createCriteria().andChannelEqualTo(channel)
                    .andDistrictCodeEqualTo(districtCode);

            List<OtaDistrict> otaDistricts = otaDistrictMapper.selectByExample(otaDistrictExample);
            if(otaDistricts!=null&&otaDistricts.size()==1){
                otaInfoExt.setDistrictCode(otaDistricts.get(0).getCode());
            }

            hotelInfoExtMapper.insertSelective(otaInfoExt);
        }


    }


    public void addHotelRefUser(long hotelId,long userId){
        HotelRefUser hotelRefUser = new HotelRefUser();
        hotelRefUser.setHotelId(hotelId);
        hotelRefUser.setUserId(userId);
        hotelRefUserMapper.insert(hotelRefUser);
    }

    public List<HotelRefUser> queryHotelRefUserByUser(long userId){
        HotelRefUserExample example = new HotelRefUserExample();
        example.createCriteria().andUserIdEqualTo(userId);
        return hotelRefUserMapper.selectByExample(example);
    }

    public List<HotelRefUser> queryHotelRefUserByHotelId(long hotelId){
        HotelRefUserExample example = new HotelRefUserExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        return hotelRefUserMapper.selectByExample(example);
    }

    public List<HotelRefUser> queryHotelRefUserByUser(long userId,long hotelId){
        HotelRefUserExample example = new HotelRefUserExample();
        example.createCriteria().andUserIdEqualTo(userId).andHotelIdEqualTo(hotelId);
        return hotelRefUserMapper.selectByExample(example);
    }


    /**
     * 根据酒店id查询酒店信息 修改的回显操作
     * @param hotelId 酒店id
     * @return
     */
    public HoteInfoVO queryHotelInfoById(Long hotelId) {
        if(hotelId==null){
            logger.warn("酒店id为空");
            return null;
        }
        HoteInfoVO hoteInfoVO = null;

        //查询酒店对应的用户信息
        hoteInfoVO = hotelInfoMapper.queryHotelInfoById(hotelId);
        //hoteInfoVO=userMapper.queryHotelInfoById(hotelId);

        //查询扩展信息省市县
        HotelInfoExtExample hotelInfoExtExample = new HotelInfoExtExample();
        hotelInfoExtExample.createCriteria().andChannelEqualTo(ChannelEnum.MIAOSU.getCode()).andHotelIdEqualTo(hotelId);

        List<HotelInfoExt> hotelInfoExts = hotelInfoExtMapper.selectByExample(hotelInfoExtExample);

        if(hotelInfoExts!=null&&hotelInfoExts.size()==1){
            HotelInfoExt infoExt = hotelInfoExts.get(0);
            //获取省市县信息
            Province province = provinceMapper.selectByPrimaryKey(infoExt.getProvinceCode());
            City city = cityMapper.selectByPrimaryKey(infoExt.getCityCode());
            District district = districtMapper.selectByPrimaryKey(infoExt.getDistrictCode());
            hoteInfoVO.setProvince(province);
            hoteInfoVO.setCity(city);
            hoteInfoVO.setDistrict(district);
        }
        hoteInfoVO.setProvinces(provinceService.findProvinceList(null));


        return hoteInfoVO;
    }

    public void updateHotelAndUser(Long hotelId, String hotelName, String hotel_only_id, String address, String contact, String contactMobile, String accountName, String accountPassword,
								   String accountNickName, boolean admin, Long currentUserId, Long userId, String provinceCode, String cityCode, String districtCode, String receptionPhone, String ota_phone, int hotelType, String settle_totalMoney, String settle_totalNight, String baoFang_totalMoney, String touZiRen_rates) {
        //修改酒店信信息
        HotelInfo hotelInfo = new HotelInfo();
        hotelInfo.setAddress(address);//地址
        hotelInfo.setHotelId(hotelId);//酒店id
        hotelInfo.setContact(contact);//联系人名字
        hotelInfo.setContactMobile(contactMobile);//联系手机
        hotelInfo.setReceptionPhone(receptionPhone);//前台手机
        hotelInfo.setHotelName(hotelName);//酒店名称
        hotelInfo.setHotel_only_id(hotel_only_id);//酒店平台ID
        hotelInfo.setOta_phone(ota_phone);//OTA管家电话
		//销售类型,1--a类  2--b类
		hotelInfo.setHotelType(hotelType);
		if (hotelType==1){
			//协议置换总金额
			hotelInfo.setSettle_totalMoney(settle_totalMoney);
			//协议置换间夜数
			hotelInfo.setSettle_totalNight(settle_totalNight);
			//包房总金额
			hotelInfo.setBaoFang_totalMoney("");
			//投资人分润比例
			hotelInfo.setTouZiRen_rates("");
		}else {
			hotelInfo.setSettle_totalMoney("");
			hotelInfo.setSettle_totalNight("");
			hotelInfo.setBaoFang_totalMoney(baoFang_totalMoney);
			hotelInfo.setTouZiRen_rates(touZiRen_rates);
		}

        hotelInfoMapper.updateByPrimaryKeySelective(hotelInfo);

        //修改用户信息
        User user = new User();
        //管理账号应为手机号
//        if (!accountName.matches(PATTEN_REGEX_PHONE)) {
//            throw new ExistException("管理账号应为手机号");
//        }
        user.setUserName(accountName);//管理账号
        user.setUserNick(accountNickName);//管理名称
        //user.setUser_phone(user_phone);//用户电话
        user.setUserPasswd(accountPassword);//管理密码
        user.setUserId(userId);//用户id
        userMapper.updateByPrimaryKeySelective(user);

        //更新酒店扩展信息
        updateHotelInfoExt(hotelId,provinceCode,cityCode,districtCode);
    }

    /**
     * 更新酒店的扩展信息
     * @param hotelId
     * @param provinceCode
     * @param cityCode
     * @param districtCode
     */
    private void updateHotelInfoExt(Long hotelId, String provinceCode, String cityCode, String districtCode) {
        //保存本地的酒店信息，并添加对应的渠道省市县信息
        HotelInfoExt infoExt = new HotelInfoExt();
        infoExt.setHotelId(hotelId);
        infoExt.setChannel(0);
        infoExt.setProvinceCode(provinceCode);
        infoExt.setCityCode(cityCode);
        infoExt.setDistrictCode(districtCode);

        HotelInfoExtExample hotelInfoExtExample = new HotelInfoExtExample();
        hotelInfoExtExample.createCriteria().andChannelEqualTo(0)
               .andHotelIdEqualTo(hotelId);

        List<HotelInfoExt> hotelInfoExts = hotelInfoExtMapper.selectByExample(hotelInfoExtExample);

        if(hotelInfoExts==null||hotelInfoExts.size()==0){
            addHotelInfoExt(hotelId,provinceCode,cityCode,districtCode);
            return;
        }
        //保存本地酒店信息
        hotelInfoExtMapper.updateByPrimaryKeySelective(infoExt);

        //查询所有的渠道信息
        for(ChannelEnum channelEnum : ChannelEnum.values()){
            HotelInfoExt otaInfoExt = new HotelInfoExt();
            int channel = channelEnum.getCode();
            if(channel == 0) {
                continue;
            }
            otaInfoExt.setHotelId(hotelId);
            otaInfoExt.setChannel(channel);
            //省份
            OtaProvinceExample otaProvinceExample = new OtaProvinceExample();
            otaProvinceExample.createCriteria().andChannelEqualTo(channel)
                    .andProvinceCodeEqualTo(provinceCode);

            List<OtaProvince> otaProvinces = otaProvinceMapper.selectByExample(otaProvinceExample);
            if(otaProvinces!=null&&otaProvinces.size()==1){
                otaInfoExt.setProvinceCode(otaProvinces.get(0).getCode());
            }
            //城市
            OtaCityExample otaCityExample = new OtaCityExample();
            otaCityExample.createCriteria().andChannelEqualTo(channel)
                    .andCityCodeEqualTo(cityCode);

            List<OtaCity> otaCities = otaCityMapper.selectByExample(otaCityExample);
            if(otaCities!=null&&otaCities.size()==1){
                otaInfoExt.setCityCode(otaCities.get(0).getCode());
            }
            //区域
            OtaDistrictExample otaDistrictExample = new OtaDistrictExample();
            otaDistrictExample.createCriteria().andChannelEqualTo(channel)
                    .andDistrictCodeEqualTo(districtCode);

            List<OtaDistrict> otaDistricts = otaDistrictMapper.selectByExample(otaDistrictExample);
            if(otaDistricts!=null&&otaDistricts.size()==1){
                otaInfoExt.setDistrictCode(otaDistricts.get(0).getCode());
            }

            hotelInfoExtMapper.updateByPrimaryKeySelective(otaInfoExt);
        }


    }

    //根据酒店id查询平台id
    public String queryHotelOnlyId(Long hotelId) {
        return hotelInfoMapper.queryHotelOnlyId(hotelId);
    }

    //根据当前登录用户id查询酒店id
	public Long queryHotelIdByUserId(Long currentUserId) {
        return hotelInfoMapper.queryHotelIdByUserId(currentUserId);
	}


    //根据当前酒店id和酒店是否有效去查询对应isActive属性的酒店,isActive是1,选择的就显示上架酒店,是0就显示下架酒店
    public PageResult queryHotelListWithPage2(Long currentUserId, String name, Long hotelId, int offset, int limit, int isActive) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<HotelInfo> hotelList = this.queryHotelList2(currentUserId,name,hotelId,isActive);
        long total = objects.getTotal();
        return new PageResult().success(total,hotelList);
    }
    public List<HotelInfo> queryHotelList2(Long currentUserId, String name, Long hotelId, int isActive) {
        List<HotelInfo> hotelInfos = hotelInfoMapper.selectByUserAndName2(currentUserId,name,hotelId,isActive);
        return hotelInfos;
    }

    //如果是admin或者供应商页面,就根据isActive属性去选择对应属性的酒店之一去显示
    public HotelInfo getHotelInfoByIdAndisActive(int isActive) {
        return hotelInfoMapper.selectByPrimaryKeyAndisActive(isActive);
    }

    //查询所有酒店信息
    public List<HotelInfo> queryAllHotelInfo() {
        return hotelInfoMapper.queryAllHotelInfo();
    }

    //操作酒店上下线,表hotel_info中的isActive字段
    public void updateHotel_isActive(Long hotelId, int isActive1) {
         hotelInfoMapper.updateHotel_isActive(hotelId,isActive1);
    }

	//删除之前user_id和hotel_id的关联
	public void deleteHotelRefUser(Long user_id, Long aLong) {
		hotelRefUserMapper.deleteHotelRefUser(user_id,aLong);
	}
}
