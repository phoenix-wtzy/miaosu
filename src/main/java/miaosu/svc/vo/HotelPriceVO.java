package miaosu.svc.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import miaosu.dao.model.HotelPriceSet;
import miaosu.dao.model.HotelRoomPrice;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 房型所属的价格计划和价格信息
 */
public class HotelPriceVO {

    /**
     * 产品vo
     */
    private HotelProductVO hotelProductVO;

    /**
     * 价格计划所对应的所有价格信息
     */
    private List<HotelRoomPrice> hotelRoomPrices;

    public HotelProductVO getHotelProductVO() {
        return hotelProductVO;
    }

    public void setHotelProductVO(HotelProductVO hotelProductVO) {
        this.hotelProductVO = hotelProductVO;
    }

    public List<HotelRoomPrice> getHotelRoomPrices() {
        return hotelRoomPrices;
    }

    public void setHotelRoomPrices(List<HotelRoomPrice> hotelRoomPrices) {
        this.hotelRoomPrices = hotelRoomPrices;
    }
}
