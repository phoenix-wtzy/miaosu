package miaosu.svc.vo.qhh;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * author  xieqx
 * date   2018/8/7
 * description 每日的售卖状态
 */
public class HotelDailySafe implements Serializable{
    /**
     * 售卖产品id
     */
    private  Long productId;

    /**
     * 房型信息
     */
    private  Long hotelRoomTypeId;

    /**
     * 售卖日期
     */
    private Date sellDate;

    /**
     * 售卖状态
     */
    private Integer status;


    /**
     * 售卖价格
     */
    private BigDecimal price;

    /**
     * 房量信息
     */
    private Integer counts;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getHotelRoomTypeId() {
        return hotelRoomTypeId;
    }

    public void setHotelRoomTypeId(Long hotelRoomTypeId) {
        this.hotelRoomTypeId = hotelRoomTypeId;
    }

    public Date getSellDate() {
        return sellDate;
    }

    public void setSellDate(Date sellDate) {
        this.sellDate = sellDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCounts() {
        return counts;
    }

    public void setCounts(Integer counts) {
        this.counts = counts;
    }
}
