package miaosu.svc.vo.qhh;

import miaosu.dao.model.OpLog;

public class OplogVO extends OpLog {
    /**
     * 操作人
     */
    private String userName;

    /**
     * 房型名称
     */
    private String hotelRoomTypeName;

    /**
     * 操作类型
     */
    private String opTypeName;

    /**
     * 开始时间
     * @return
     */
    private String startDate;

    /**
     * 结束时间
     * @return
     */
    private String endDate;

    /**
     * value值
     * @return
     */
    private String opValue;

    /**
     * 房型id集合
     * @return
     */
    private Long[] hotelRoomTypeIds;

    public Long[] getHotelRoomTypeIds() {
        return hotelRoomTypeIds;
    }

    public void setHotelRoomTypeIds(Long[] hotelRoomTypeIds) {
        this.hotelRoomTypeIds = hotelRoomTypeIds;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOpValue() {
        return opValue;
    }

    public void setOpValue(String opValue) {
        this.opValue = opValue;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHotelRoomTypeName() {
        return hotelRoomTypeName;
    }

    public void setHotelRoomTypeName(String hotelRoomTypeName) {
        this.hotelRoomTypeName = hotelRoomTypeName;
    }

    public String getOpTypeName() {
        return opTypeName;
    }

    public void setOpTypeName(String opTypeName) {
        this.opTypeName = opTypeName;
    }
}
