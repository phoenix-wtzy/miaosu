package miaosu.svc.vo;

import miaosu.dao.model.BInvoice;

public class InvoiceVO extends BInvoice {
    private String hotelName;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }
}
