package miaosu.svc.vo;

import miaosu.dao.model.HotelSettleBill;

/**
 * @author  xieqx
 * 酒店结算信息vo对象
 */
public class HotelSettleBillVO  extends HotelSettleBill{
    private String orderIds;

    //结算周期
    private String settleCycleName;

    //结算状态
    private String settleStatusName;

    //结算银行（中间星号）
    private String settleBankAccountFuzzy;


    //发票已经发送
    private Integer hasCreated;

    public Integer getHasCreated() {
        return hasCreated;
    }

    public void setHasCreated(Integer hasCreated) {
        this.hasCreated = hasCreated;
    }

    public String getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(String orderIds) {
        this.orderIds = orderIds;
    }


    public String getSettleCycleName() {
        return settleCycleName;
    }

    public void setSettleCycleName(String settleCycleName) {
        this.settleCycleName = settleCycleName;
    }

    public String getSettleStatusName() {
        return settleStatusName;
    }

    public void setSettleStatusName(String settleStatusName) {
        this.settleStatusName = settleStatusName;
    }

    public String getSettleBankAccountFuzzy() {
        return settleBankAccountFuzzy;
    }

    public void setSettleBankAccountFuzzy(String settleBankAccountFuzzy) {
        this.settleBankAccountFuzzy = settleBankAccountFuzzy;
    }
}
