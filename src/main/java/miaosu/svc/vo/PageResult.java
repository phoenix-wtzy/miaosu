package miaosu.svc.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jiajun on 2/5/18.
 */
public class PageResult<T> implements Serializable {

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public T getRows() {
        return rows;
    }

    public void setRows(T rows) {
        this.rows = rows;
    }

    public long total;
    private T rows = null;


    public PageResult<T>  success(long total,T rows) {
        this.setTotal(total);
        this.setRows(rows);
        return  this;
    }
}
