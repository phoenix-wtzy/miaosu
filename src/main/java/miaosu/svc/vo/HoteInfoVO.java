package miaosu.svc.vo;

import miaosu.dao.model.City;
import miaosu.dao.model.District;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.Province;

import java.util.List;

/**
 * Created by jiajun.chen on 2018/2/5.
 */
public class HoteInfoVO extends HotelInfo{

    private Long userId;
    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户昵称
     */
    private String userNick;
    /**
     * 用户电话
     */
    private String user_phone;


    /**
     * 密码
     */

    private String userPasswd;

    private Integer status;

    /**
     * 城市
     */
    private City city;

    /**
     * 省份
     */
    private Province province;

    /**
     * 区域信息
     */
    private District district;

    /**
     * 所有的省信息
     * @return
     */
    private List<Province> provinces;

    /**
     * 所有的城市
     * @return
     */
    private List<City> cities;


    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUserPasswd() {
        return userPasswd;
    }

    public void setUserPasswd(String userPasswd) {
        this.userPasswd = userPasswd;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public List<Province> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
