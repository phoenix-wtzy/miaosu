package miaosu.svc.vo;

import miaosu.dao.model.HotelOrder;
import miaosu.dao.model.OrderOfCaigouone;

import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/8.
 */
public class HotelOrderVO extends HotelOrder {

    private String orderChannelName; //渠道名称

    private String orderChannelCode; //渠道编码

    private String sellTypeName; //置换、代销

    private String bookTimeStr;

    private String hotelRoomTypeName;

    private String checkInTimeStr;

    private String checkOutTimeStr;

    /**
     * 预离时间
     */
    private String checkInOutTimeStr;

    private String orderStateStr;


    //结算状态
    private String settleStatusStr;
    //间夜
    private Long night;

    //结算金额=订单实际金额*(1-xx%)     xx是酒店佣金率
    private String settle_money;
    //置单日期列表
    private List<OrderOfCaigouone>orderOfCaigouoneList;












    public List<OrderOfCaigouone> getOrderOfCaigouoneList() {
        return orderOfCaigouoneList;
    }

    public void setOrderOfCaigouoneList(List<OrderOfCaigouone> orderOfCaigouoneList) {
        this.orderOfCaigouoneList = orderOfCaigouoneList;
    }

    public String getSettle_money() {
        return settle_money;
    }

    public void setSettle_money(String settle_money) {
        this.settle_money = settle_money;
    }

    public Long getNight() {
        return night;
    }

    public void setNight(Long night) {
        this.night = night;
    }

    public String getOrderChannelCode() {
        return orderChannelCode;
    }

    public void setOrderChannelCode(String orderChannelCode) {
        this.orderChannelCode = orderChannelCode;
    }

    public String getCheckInOutTimeStr() {
        return checkInOutTimeStr;
    }

    public void setCheckInOutTimeStr(String checkInOutTimeStr) {
        this.checkInOutTimeStr = checkInOutTimeStr;
    }

    public String getOrderChannelName() {
        return orderChannelName;
    }

    public void setOrderChannelName(String orderChannelName) {
        this.orderChannelName = orderChannelName;
    }

    public String getSellTypeName() {
        return sellTypeName;
    }

    public void setSellTypeName(String sellTypeName) {
        this.sellTypeName = sellTypeName;
    }

    public String getBookTimeStr() {
        return bookTimeStr;
    }

    public void setBookTimeStr(String bookTimeStr) {
        this.bookTimeStr = bookTimeStr;
    }

    public String getHotelRoomTypeName() {
        return hotelRoomTypeName;
    }

    public void setHotelRoomTypeName(String hotelRoomTypeName) {
        this.hotelRoomTypeName = hotelRoomTypeName;
    }

    public String getCheckInTimeStr() {
        return checkInTimeStr;
    }

    public void setCheckInTimeStr(String checkInTimeStr) {
        this.checkInTimeStr = checkInTimeStr;
    }

    public String getCheckOutTimeStr() {
        return checkOutTimeStr;
    }

    public void setCheckOutTimeStr(String checkOutTimeStr) {
        this.checkOutTimeStr = checkOutTimeStr;
    }

    public String getOrderStateStr() {
        return orderStateStr;
    }

    public void setOrderStateStr(String orderStateStr) {
        this.orderStateStr = orderStateStr;
    }

    public String getSettleStatusStr() {
        return settleStatusStr;
    }

    public void setSettleStatusStr(String settleStatusStr) {
        this.settleStatusStr = settleStatusStr;
    }
}
