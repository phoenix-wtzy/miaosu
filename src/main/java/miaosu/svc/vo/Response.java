package miaosu.svc.vo;

import java.io.Serializable;

/**
 * @author Administrator
 * @Date: 2020/12/15 13:42
 * @Description: 封装实体类,返回结果如下
 * {
 *
 *     "code": 0,
 *
 *     "message": "海旺酒店",
 *
 *     "count": 10,
 *
 *     "data": [
 *
 *         {
 *
 *             "id":1,
 *
 *             "name": "马洋"
 *
 *         },
 *
 *         {
 *
 *             "id": 2,
 *
 *             "name": "马洋2"
 *
 *         }
 *
 *     ]
 *
 * }
 */
public class Response implements Serializable {
	private int code;//执行结果，0为执行成功 -1为执行失败
	private String message;//返回结果信息
	private int count;//返回结果条数
	private Object data;//返回数据
	public Response(){}


	public Response(int code, String message,int count, Object data) {
		this.code = code;
		this.message = message;
		this.count=count;
		this.data = data;
	}

	public Response success(String message){
		Response response = new Response();
		response.setCode(0);
		response.setMessage(message);
		return response;
	}
	public Response success(String message,int count, Object data){
		Response response = new Response();
		response.setCode(0);
		response.setMessage(message);
		response.setCount(count);
		response.setData(data);
		return response;
	}
	public Response failed(String message){
		Response response = new Response();
		response.setCode(-1);
		response.setMessage(message);
		return response;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Response{" +
				"code=" + code +
				", message='" + message + '\'' +
				", count=" + count +
				", data=" + data +
				'}';
	}
}
