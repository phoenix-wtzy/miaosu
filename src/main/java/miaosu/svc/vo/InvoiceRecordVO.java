package miaosu.svc.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import miaosu.dao.model.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by xieqx on 2018/2/5.
 */
public class InvoiceRecordVO extends InvoiceRecord {

    private String invoiceCode;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date invoiceDate;

    private String invoiceUnit;

    private String invoiceNo;

    //不含税金额
    private String noTaxAmount;

    //税额
    private String taxAmount;

    public String getNoTaxAmount() {
        return noTaxAmount;
    }

    public void setNoTaxAmount(String noTaxAmount) {
        this.noTaxAmount = noTaxAmount;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    private String statusDesc;

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceUnit() {
        return invoiceUnit;
    }

    public void setInvoiceUnit(String invoiceUnit) {
        this.invoiceUnit = invoiceUnit;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
}
