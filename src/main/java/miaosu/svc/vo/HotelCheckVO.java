package miaosu.svc.vo;

import miaosu.dao.model.City;
import miaosu.dao.model.District;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.Province;

import java.util.List;

/**
 * @author  xieqx
 * 酒店审核信息
 */
public class HotelCheckVO {
    /**
     * 酒店名称
     */
    private String hotelName;
    /**
     * 房型名称
     */
    private String roomTypeName;
    /**
     * 审核结果信息
     */
    private Object data;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }
}
