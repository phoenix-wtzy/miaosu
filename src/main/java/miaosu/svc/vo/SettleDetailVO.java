package miaosu.svc.vo;


/**
 * 结算明细信息
 */
public class SettleDetailVO {

    /**
     * 订单号
     */
    private  Integer orderNum;

    /**
     * 应结算金额
     */
    private String shouldMoney;

    /**
     * 调整金额
     */
    private String adjustMoney;
    /**
     * 实际结算金额
     */
    private String realMoney;

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getShouldMoney() {
        return shouldMoney;
    }

    public void setShouldMoney(String shouldMoney) {
        this.shouldMoney = shouldMoney;
    }

    public String getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(String realMoney) {
        this.realMoney = realMoney;
    }

    public String getAdjustMoney() {
        return adjustMoney;
    }

    public void setAdjustMoney(String adjustMoney) {
        this.adjustMoney = adjustMoney;
    }
}
