package miaosu.svc.vo;

import miaosu.dao.model.HotelRoomCount;

import java.util.List;

/**
 * author  xieqx
 * date   2018/8/6
 * 房格所有信息的vo对象
 */
public class HotelRoomPriceVO {
    /**
     * 房型
     */
    private HotelRoomSetVO hotelRoomSetVO;

    /**
     * 该房型下的所有房间数量
     */
    private List<HotelRoomCount>  hotelRoomCounts;

    /**
     * 该房型下的所有价格计划以及价格计划所对应的价格信息
     */
    private List<HotelPriceVO> hotelPriceVOs;


    public HotelRoomSetVO getHotelRoomSetVO() {
        return hotelRoomSetVO;
    }

    public void setHotelRoomSetVO(HotelRoomSetVO hotelRoomSetVO) {
        this.hotelRoomSetVO = hotelRoomSetVO;
    }

    public List<HotelRoomCount> getHotelRoomCounts() {
        return hotelRoomCounts;
    }

    public void setHotelRoomCounts(List<HotelRoomCount> hotelRoomCounts) {
        this.hotelRoomCounts = hotelRoomCounts;
    }

    public List<HotelPriceVO> getHotelPriceVOs() {
        return hotelPriceVOs;
    }

    public void setHotelPriceVOs(List<HotelPriceVO> hotelPriceVOs) {
        this.hotelPriceVOs = hotelPriceVOs;
    }
}
