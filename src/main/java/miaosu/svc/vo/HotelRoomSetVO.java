package miaosu.svc.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import miaosu.dao.model.HotelPriceSet;
import miaosu.dao.model.HotelRoomSet;

import java.util.Date;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/2/10.
 */
public class HotelRoomSetVO extends HotelRoomSet {

    /**
     * 房型类型的名称
     */
    private String roomTypeName;


    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

}
