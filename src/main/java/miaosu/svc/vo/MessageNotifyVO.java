package miaosu.svc.vo;


/**
 * Created by jiajun.chen on 2018/4/8.
 */
public class MessageNotifyVO {

    /**
     * 消息通知手机号 多个字段以，分隔
     */
    private String msgNotifyPhone;

    /**
     * 语音通知
     */
    private String voiceNotifyPhone;


    /**
     * 微信公众号url地址(二维码)
     */
    private String wechat_url;

    public String getMsgNotifyPhone() {
        return msgNotifyPhone;
    }

    public void setMsgNotifyPhone(String msgNotifyPhone) {
        this.msgNotifyPhone = msgNotifyPhone;
    }

    public String getVoiceNotifyPhone() {
        return voiceNotifyPhone;
    }

    public void setVoiceNotifyPhone(String voiceNotifyPhone) {
        this.voiceNotifyPhone = voiceNotifyPhone;
    }

    public String getWechat_url() {
        return wechat_url;
    }

    public void setWechat_url(String wechat_url) {
        this.wechat_url = wechat_url;
    }
}
