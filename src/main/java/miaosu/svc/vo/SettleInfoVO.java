package miaosu.svc.vo;

import lombok.Data;

import java.util.List;

@Data
public class SettleInfoVO {

    /**
     * 酒店id
     */
    private Long hotelId;

    /**
     * 结算金额
     */
    private String settleMoney;

    /**
     * 佣金金额
     */
    private String commissionMoney;


    /**
     * 本次结算涉及到的所有订单id
     */
    private String orderIds;

}
