package miaosu.svc.vo;

import miaosu.dao.model.HotelPriceSet;
import miaosu.dao.model.HotelRoomSet;

import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/5.
 */
public class HotelProductVO extends HotelPriceSet {
    /**
     * 产品id
     */
    private Long productId;


    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

}