package miaosu.svc.vo;

import java.io.Serializable;

/**
 * Created by jiajun on 2/5/18.
 */
public class Result<T> implements Serializable {

    public Bstatus bstatus;
    private T data = null;

    public Bstatus getBstatus() {
        return bstatus;
    }

    public void setBstatus(Bstatus bstatus) {
        this.bstatus = bstatus;
    }

    public T getData() {
        return data;
    }

    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }

    public static class Bstatus {
        private int code = 0;
        private String des = "";

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getDes() {
            return des;
        }

        public void setDes(String des) {
            this.des = des;
        }

    }

    public Result<T> success(Object data) {
        Result result = new Result<T>();
        Bstatus bstatus = new Bstatus();
        bstatus.setCode(0);
        bstatus.setDes("success");
        result.setBstatus(bstatus);
        result.setData(data);
        return result;
    }
    //针对填写过公司信息但是没审核通过的
    public Result<T> business(Object data,String des) {
        Result result = new Result<T>();
        Bstatus bstatus = new Bstatus();
        bstatus.setCode(-1);
        bstatus.setDes(des);
        result.setBstatus(bstatus);
        result.setData(data);
        return result;
    }

    public Result<T> success() {
        Result result = new Result<T>();
        Bstatus bstatus = new Bstatus();
        bstatus.setCode(0);
        bstatus.setDes("success");
        result.setBstatus(bstatus);
        return result;
    }
    //注册成功的封装实体类
    public Result<T> zhuCeSuccess(String des) {
        Result result = new Result<T>();
        Bstatus bstatus = new Bstatus();
        bstatus.setCode(0);
        bstatus.setDes(des);
        result.setBstatus(bstatus);
        return result;
    }

    public Result<T> failed(String des) {
        Result result = new Result<T>();
        Bstatus bstatus = new Bstatus();
        bstatus.setCode(-1);
        bstatus.setDes(des);
        result.setBstatus(bstatus);
        return result;
    }
}
