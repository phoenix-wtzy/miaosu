package miaosu.svc.vo;

import miaosu.dao.model.HotelSettleSetting;

public class HotelSettleSettingVO extends HotelSettleSetting{

    private String settleCycleSettingName;

    private String hotelName;

    private String bankUserTypeName;

    public String getBankUserTypeName() {
        return bankUserTypeName;
    }

    public void setBankUserTypeName(String bankUserTypeName) {
        this.bankUserTypeName = bankUserTypeName;
    }

    public String getSettleCycleSettingName() {
        return settleCycleSettingName;
    }

    public void setSettleCycleSettingName(String settleCycleSettingName) {
        this.settleCycleSettingName = settleCycleSettingName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }
}
