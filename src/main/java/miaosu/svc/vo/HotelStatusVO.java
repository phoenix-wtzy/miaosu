package miaosu.svc.vo;

import miaosu.dao.model.HotelRoomStatus;

import java.util.List;

/**
 * author  Administrator
 * date   2018/8/7
 */
public class HotelStatusVO {
    /**
     * 产品vo
     */
    private HotelProductVO hotelProductVO;

    /**
     * 价格计划所对应的所有房态信息信息
     */
    private List<HotelRoomStatus> hotelRoomStatusList;

    public HotelProductVO getHotelProductVO() {
        return hotelProductVO;
    }

    public void setHotelProductVO(HotelProductVO hotelProductVO) {
        this.hotelProductVO = hotelProductVO;
    }

    public List<HotelRoomStatus> getHotelRoomStatusList() {
        return hotelRoomStatusList;
    }

    public void setHotelRoomStatusList(List<HotelRoomStatus> hotelRoomStatusList) {
        this.hotelRoomStatusList = hotelRoomStatusList;
    }
}
