package miaosu.svc.vo;

/**
 * Created by jiajun.chen on 2018/4/13.
 */
public class QueryHotelOrderVO {
    private Long hotelId;

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }
}
