package miaosu.svc.vo;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderDetail implements Serializable {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    //@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date bookTime;

    private String orderContent;

    /*
     * 价格
     */
    private BigDecimal price;

    /**
     * 费用类型
     */
    private String costType;



}
