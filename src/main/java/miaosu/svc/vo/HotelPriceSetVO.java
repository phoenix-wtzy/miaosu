package miaosu.svc.vo;

import miaosu.dao.model.HotelPriceRefRoomType;
import miaosu.dao.model.HotelPriceSet;
import miaosu.dao.model.HotelRoomSet;

import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/5.
 */
public class HotelPriceSetVO extends HotelPriceSet {
    private List<HotelRoomSet> hotelRoomSetList;

    public List<HotelRoomSet> getHotelRoomSetList() {
        return hotelRoomSetList;
    }

    public void setHotelRoomSetList(List<HotelRoomSet> hotelRoomSetList) {
        this.hotelRoomSetList = hotelRoomSetList;
    }

}
