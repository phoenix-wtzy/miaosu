package miaosu.svc.vo;

import miaosu.dao.model.HotelRoomCount;

import java.util.List;

/**
 * author  xieqx
 * date   2018/8/6
 * 房态所有信息的vo对象
 */
public class HotelRoomStatusVO {
    /**
     * 房型
     */
    private HotelRoomSetVO hotelRoomSetVO;

    /**
     * 该房型下的所有房间数量
     */
    private List<HotelRoomCount>  hotelRoomCounts;

    /**
     * 该房型下的所有价格计划以及价格计划所对应的房态信息
     */
    private List<HotelStatusVO> hotelStatusVOs;


    public HotelRoomSetVO getHotelRoomSetVO() {
        return hotelRoomSetVO;
    }

    public void setHotelRoomSetVO(HotelRoomSetVO hotelRoomSetVO) {
        this.hotelRoomSetVO = hotelRoomSetVO;
    }

    public List<HotelRoomCount> getHotelRoomCounts() {
        return hotelRoomCounts;
    }

    public void setHotelRoomCounts(List<HotelRoomCount> hotelRoomCounts) {
        this.hotelRoomCounts = hotelRoomCounts;
    }

    public List<HotelStatusVO> getHotelStatusVOs() {
        return hotelStatusVOs;
    }

    public void setHotelStatusVOs(List<HotelStatusVO> hotelStatusVOs) {
        this.hotelStatusVOs = hotelStatusVOs;
    }
}
