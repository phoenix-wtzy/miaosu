package miaosu.svc.invoice;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.vpiaotong.openapi.util.Base64Util;
import miaosu.common.ShowStatusEnum;
import miaosu.dao.auto.InvoiceRecordMapper;
import miaosu.dao.model.InvoiceRecord;
import miaosu.dao.model.InvoiceRecordExample;
import miaosu.invoice.InvoiceInfoQueryBean;
import miaosu.invoice.InvoiceInfoResultBean;
import miaosu.invoice.InvoiceServiceUtils;
import miaosu.svc.vo.InvoiceRecordVO;
import miaosu.svc.vo.PageResult;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class InvoiceRecordService {
    private Logger logger = LoggerFactory.getLogger(InvoiceRecordService.class);


    //发票记录信息
    @Autowired
    private InvoiceRecordMapper invoiceRecordMapper;

    /**
     * 查询发票信息
     * @param rootBean 发票信息
     */
    private void fillInvoiceInfo(InvoiceInfoResultBean rootBean,InvoiceRecordVO record) {

        record.setInvoiceCode(rootBean.getInvoiceCode());
        record.setInvoiceDate(rootBean.getInvoiceDate());
        record.setInvoiceUnit(rootBean.getSellerAddress());
        record.setInvoiceNo(rootBean.getInvoiceNo());
        record.setNoTaxAmount(rootBean.getNoTaxAmount());
        record.setTaxAmount(rootBean.getTaxAmount());
    }



    /**
     * 保存发票信息
     * @param queryBean
     * @param hotelId
     * @param content 开票内容
     * @param isAdmin
     */
    public boolean saveInvoiceInfo(InvoiceInfoQueryBean queryBean, Long hotelId, Long billId, String content, Boolean isAdmin) {
        InvoiceRecord record = new InvoiceRecord();
        record.setHotelId(hotelId);
        record.setBillId(billId);
        record.setInvoiceContent(content);
        record.setCreateTime(new Date());

        record.setReqserialno(queryBean.getInvoiceReqSerialNo());
        record.setSellerPayernum(queryBean.getTaxpayerNum());
        if(isAdmin){
            record.setShowStatus(ShowStatusEnum.ADMIN.getCode());
        }else{
            record.setShowStatus(ShowStatusEnum.USER.getCode());
        }

        return invoiceRecordMapper.insert(record)==1?true:false;
    }

    public Integer queryisHaveInvoice(Long billId,boolean isAdmin) {

        Integer status = null;
        if(isAdmin){
            status = ShowStatusEnum.ADMIN.getCode();
        }else{
            status = ShowStatusEnum.USER.getCode();
        }

        InvoiceRecordExample invoiceRecordExample = new InvoiceRecordExample();
        invoiceRecordExample.createCriteria().andBillIdEqualTo(billId)
        .andShowStatusEqualTo(status);

        List<InvoiceRecord> invoiceRecordList =invoiceRecordMapper.selectByExample(invoiceRecordExample);

        if(invoiceRecordList==null || invoiceRecordList.size()==0){
            return 0;
        }
        return 1;
    }

    public PageResult queryInvoiceListByPage(Long search_hotel_id, String search_date_start, String search_date_end, Boolean isAdmin,int offset, int limit) {
            Page<Object> pageInfo = PageHelper.offsetPage(offset, limit);
        try {
            List<InvoiceRecordVO> invoiceRecordVOS = getInvoiceRecordVOList(search_hotel_id, search_date_start, search_date_end, isAdmin);

            long total = pageInfo.getTotal();
            return new PageResult().success(total, invoiceRecordVOS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new PageResult();
    }

    private List<InvoiceRecordVO> getInvoiceRecordVOList(Long search_hotel_id, String search_date_start, String search_date_end, Boolean isAdmin) throws ParseException {
        Integer status = null;
        if(isAdmin){
           status = ShowStatusEnum.ADMIN.getCode();
        }else{
            status = ShowStatusEnum.USER.getCode();
        }
        Date startDate = DateUtils.parseDate(search_date_start, "yyyy-MM-dd");
        Date endDate = DateUtils.parseDate(search_date_end, "yyyy-MM-dd");
        endDate = DateUtils.addDays(endDate, 1);
        InvoiceRecordExample invoiceRecordExample = new InvoiceRecordExample();
        invoiceRecordExample.createCriteria().andHotelIdEqualTo(search_hotel_id).andCreateTimeBetween(startDate,endDate)
        .andShowStatusEqualTo(status);

        List<InvoiceRecord> invoiceRecords = invoiceRecordMapper.selectByExample(invoiceRecordExample);

        return convertInvoiceRecord(invoiceRecords);
    }

    private List<InvoiceRecordVO> convertInvoiceRecord(List<InvoiceRecord> invoiceRecords) {
        List<InvoiceRecordVO> list = Lists.newArrayList();

        for(InvoiceRecord record:invoiceRecords){
            InvoiceRecordVO invoiceRecordVO = new InvoiceRecordVO();
            BeanUtils.copyProperties(record,invoiceRecordVO);
            InvoiceInfoQueryBean queryBean = new InvoiceInfoQueryBean();
            //查询发票信息
            queryBean.setTaxpayerNum(record.getSellerPayernum());
            queryBean.setInvoiceReqSerialNo(record.getReqserialno());
            InvoiceInfoResultBean invoiceInfo = InvoiceServiceUtils.findInvoiceInfo(queryBean);

            if(invoiceInfo.getCode().equals("0000")){
                fillInvoiceInfo(invoiceInfo,invoiceRecordVO);
            }

            invoiceRecordVO.setStatusDesc(invoiceInfo.getMsg());
            list.add(invoiceRecordVO);
        }
        return list;
    }


    public List<Map<String,Object>> queryRecordListForExcel(Long search_hotel_id, String search_date_start, String search_date_end, boolean isAdmin) {
        List<Map<String,Object>> excelMaps = Lists.newArrayList();
        try {
            List<InvoiceRecordVO> invoiceRecordVOS = getInvoiceRecordVOList(search_hotel_id, search_date_start, search_date_end, isAdmin);

            for (InvoiceRecordVO invoiceRecordVO: invoiceRecordVOS){
                Map<String,Object> settleMap = JsonUtils.parseJSONMap(JsonUtils.objectToJson(invoiceRecordVO));
                excelMaps.add(settleMap);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  excelMaps;
    }
}
