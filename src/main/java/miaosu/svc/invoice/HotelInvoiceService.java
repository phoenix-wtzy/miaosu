package miaosu.svc.invoice;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import miaosu.config.InvoiceConfig;
import miaosu.dao.auto.BInvoiceMapper;
import miaosu.dao.auto.HotelSettleBillMapper;
import miaosu.dao.auto.InvoiceRecordMapper;
import miaosu.dao.model.*;
import miaosu.invoice.InvoiceBlueResp;
import miaosu.invoice.InvoiceInfoQueryBean;
import miaosu.invoice.InvoiceServiceUtils;
import miaosu.invoice.InvoiceInfoResultBean;
import miaosu.svc.vo.HotelSettleBillVO;
import miaosu.svc.vo.PageResult;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class HotelInvoiceService {
    private Logger logger = LoggerFactory.getLogger(HotelInvoiceService.class);

    @Autowired
    private BInvoiceMapper bInvoiceMapper;

    @Autowired
    private HotelSettleBillMapper settleBillMapper;

    @Autowired
    private InvoiceRecordService invoiceRecordService;


    public BInvoice queryHotelInvoice(long hotelId) {
        BInvoiceExample invoiceExample = new BInvoiceExample();
        invoiceExample.createCriteria().andHotelidEqualTo(hotelId);

        List<BInvoice> bInvoices = bInvoiceMapper.selectByExample(invoiceExample);

        if(bInvoices != null && bInvoices.size()==1){
            BInvoice bInvoice = bInvoices.get(0);
            return  bInvoice;
        }
        return null;

    }

    /**
     * 设置对应酒店的结算信息
     * @param bInvoice 发票结算设置
     * @return 更新成功与否的标识
     */
    public boolean updateInvoiceInfo(BInvoice bInvoice) {
        BInvoiceExample invoiceExample = new BInvoiceExample();
        invoiceExample.createCriteria().andHotelidEqualTo(bInvoice.getHotelid());
        List<BInvoice> bInvoices = bInvoiceMapper.selectByExample(invoiceExample);

        Date date = new Date();
        bInvoice.setCreatedate(date);
        bInvoice.setUpdatedate(date);
        int len = 0;
        if(bInvoices == null || (len = bInvoices.size())>1){
            return false;
        }
        int flag = 0;
        if(len == 0){
            //新增
            flag = bInvoiceMapper.insert(bInvoice);
        }

        if(len == 1){
            //更新
            long id = bInvoices.get(0).getId();
            bInvoice.setId(id);
            flag = bInvoiceMapper.updateByPrimaryKeySelective(bInvoice);
        }

        return flag==1?true:false;
    }

    public boolean generatorAndSend(Long billId, Long hotelId,Boolean isAdmin) {
        //获取结算账单信息

        HotelSettleBill settleBill = settleBillMapper.selectByPrimaryKey(billId);

        //开票项目名称 2018-10-10 到2018-10-20订单结算,  //金额  数量，单价
        String startDate = DateFormatUtils.format(settleBill.getStartDate(),"yyyy-MM-dd");
        String endDate = DateFormatUtils.format(settleBill.getEndDate(),"yyyy-MM-dd");

        String itemName = startDate+"到"+endDate+"订单结算";

        InvoiceInfoQueryBean queryBean = new InvoiceInfoQueryBean();
        BigDecimal money = null;
        BInvoiceExample invoiceExample = new BInvoiceExample();
        invoiceExample.createCriteria().andHotelidEqualTo(hotelId);
        List<BInvoice> bInvoices = bInvoiceMapper.selectByExample(invoiceExample);
        if(bInvoices == null || bInvoices.size()!=1){
            return false;
        }

        BInvoice bInvoice = bInvoices.get(0);

        if(isAdmin){
            money = settleBill.getCommissionMoney();
            //系统方的发票税号
            queryBean.setTaxpayerNum(InvoiceConfig.TAX_NUM);
        }else{
            money = settleBill.getSettleMoney();
            //酒店方
            queryBean.setTaxpayerNum(bInvoice.getBuyertaxpayernum());
        }

        bInvoice.setItemname(itemName);
        bInvoice.setInvoiceamount(money);
        bInvoice.setQuantity(1);
        bInvoice.setUnitprice(money);
        //生成发票
        InvoiceBlueResp invoiceBlueResp = InvoiceServiceUtils.genetatorInvoice(bInvoice,isAdmin,InvoiceBlueResp.class);

        if(invoiceBlueResp == null){
            return false;
        }

        queryBean.setInvoiceReqSerialNo(invoiceBlueResp.getInvoiceReqSerialNo());

        return invoiceRecordService.saveInvoiceInfo(queryBean,hotelId,billId,itemName,isAdmin);
    }



}
