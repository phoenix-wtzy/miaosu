package miaosu.svc.user;

import miaosu.dao.auto.CodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @Date: 2020/7/6 14:03
 * @Description:
 */
@Component
public class CodeService {

    @Autowired
    private CodeMapper codeMapper;

    //验证上家的邀请码是否存在以及正确性
    public String queryIfExistCode(String code_shangjia) {
        return codeMapper.queryIfExistCode(code_shangjia);
    }

    //将自己的邀请码存储到code表中
    public void insertCode(String code_my,String code_level) {
        codeMapper.insertCode(code_my,code_level);
    }

    //根据邀请码查询自己的邀请码id
    public Long queryCodeId(String code_my) {
        return codeMapper.queryCodeId(code_my);
    }
}
