package miaosu.svc.user;

import miaosu.dao.auto.BusinessMapper;
import miaosu.dao.model.Business;
import miaosu.dao.model.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/7/22 13:33
 * @Description:
 */
@Component
public class BusinessService {

	@Autowired
	private BusinessMapper businessMapper;

	//添加公司信息
	public void insert(Business business) {
		businessMapper.insert(business);
	}

	//查询公司信息列表
	public Business queryBusinessList(Long currentUserId) {
		return businessMapper.queryBusinessList(currentUserId);
	}

	//在公司信息查询是否存在该用户(自己查自己),存在就不能添加,只能修改
	public Long queryIfExistUserId(Long currentUserId) {
		return businessMapper.queryIfExistUserId(currentUserId);
	}

	//根据用户id查询公司名称
	public String queryBusiness_name(Long user_id) {
		return businessMapper.queryBusiness_name(user_id);
	}

	//查询所有的公司
	public List<Business> queryBusinessInsertList() {
		return businessMapper.queryBusinessInsertList();
	}

	//添加图片到数据库
	public void saveImage(Picture picture) {
		 businessMapper.saveImage(picture);
	}

	//查询图片
	public Picture queryPicture(Long currentUserId) {
		return businessMapper.queryPicture(currentUserId);
	}

	//删除不通过的公司信息和照片
	public void deleteBusiness(Long user_id) {
		businessMapper.deleteBusiness(user_id);
	}
	//删除不通过的公司信息和照片
	public void deletePicture(Long user_id) {
		businessMapper.deletePicture(user_id);
	}

	//根据公司id查询公司信息
	public Business queryBusinessByBusiness_id(Long business_id) {
		return businessMapper.queryBusinessByBusiness_id(business_id);
	}

	//添加公司信息审核通过的时间
	public void updateSuccess_date(Long user_id, Date success_date) {
		businessMapper.updateSuccess_date(user_id,success_date);
	}

	//修改公司信息
	public void updateBusiness(Business business) {
		businessMapper.updateBusiness(business);
	}
	//通过分页查询所有企业信息
	public List<Business> queryAllBusinessByPage(int page1, int limit) {
		int page=(page1-1)*limit;
		List<Business> businessList=businessMapper.queryAllBusinessByPage(page,limit);
		for (Business business : businessList) {
			Long business_type = business.getBusiness_type();
			if (null == business_type) {
				business.setBusiness_typeStr(null);
			} else {
				if (business_type == 1) {
					business.setBusiness_typeStr("家具");
				} else if (business_type == 2) {
					business.setBusiness_typeStr("装修");
				} else if (business_type == 3) {
					business.setBusiness_typeStr("设计");
				} else if (business_type == 4) {
					business.setBusiness_typeStr("地毯");
				} else if (business_type == 5) {
					business.setBusiness_typeStr("灯饰");
				} else if (business_type == 6) {
					business.setBusiness_typeStr("五金");
				} else if (business_type == 7) {
					business.setBusiness_typeStr("智能化");
				} else if (business_type == 8) {
					business.setBusiness_typeStr("其他");
				}
			}
			Long businessOrFactory = business.getBusinessOrFactory();
			if (null == businessOrFactory) {
				business.setBusinessOrFactoryStr(null);
			} else {
				if (businessOrFactory == 1) {
					business.setBusinessOrFactoryStr("经销商");
				} else if (businessOrFactory == 2) {
					business.setBusinessOrFactoryStr("厂家");
				}
			}
			//查询图片
//			Long user_id = business.getUser_id();
//			Picture picture = this.queryPicture(user_id);
//			if (null == picture) {
				business.setImg_businessLicense(null);
				business.setImg_shopHead(null);
				business.setImg_factoryBook(null);
//			} else {
//				byte[] img_businessLicense = picture.getImg_businessLicense();
//				String encode1 = org.apache.commons.codec.binary.Base64.encodeBase64String(img_businessLicense);
//				String base64_1 = "data:image/png;base64," + encode1;
//				business.setImg_businessLicense(base64_1);
//
//				byte[] img_shopHead = picture.getImg_shopHead();
//				String encode2 = org.apache.commons.codec.binary.Base64.encodeBase64String(img_shopHead);
//				String base64_2 = "data:image/png;base64," + encode2;
//				business.setImg_shopHead(base64_2);
//
//				byte[] img_factoryBook = picture.getImg_factoryBook();
//				String encode3 = Base64.encodeBase64String(img_factoryBook);
//				String base64_3 = "data:image/png;base64," + encode3;
//				business.setImg_factoryBook(base64_3);
//			}
		}
		return  businessList;
	}

	//查询公司信息总条数
	public int queryTotalBusiness() {
		return businessMapper.queryTotalBusiness();
	}
}
