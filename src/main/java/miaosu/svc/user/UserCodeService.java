package miaosu.svc.user;

import miaosu.dao.auto.UserCodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/7/8 11:45
 * @Description:
 */
@Component
public class UserCodeService {

	@Autowired
	private UserCodeMapper userCodeMapper;

	//将user_id,code_id,code_shangjia,code_my添加到用户邀请码关系表
	public void insert(Long userId, Long codeId, String code_shangjia, String code_my) {
		userCodeMapper.insert(userId,codeId,code_shangjia,code_my);
	}

	//根据用户id查询自己的邀请码code_my
	public String queryCode_my(Long currentUserId) {
		return userCodeMapper.queryCode_my(currentUserId);
	}

	//根据我的邀请码查询下家的邀请码所对应的用户id
	public List<Long> queryCode_xiajia(String code_my) {
		return userCodeMapper.queryCode_xiajia(code_my);

	}

	//根据用户id查询code_id
	public Long queryCode_id(Long user_id) {
		return userCodeMapper.queryCode_id(user_id);
	}

	//将用户的code_level设置为已通过
	public void updateCode_level(Long code_id) {
		userCodeMapper.updateCode_level(code_id);
	}
}
