package miaosu.svc.user;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import miaosu.common.RoleEnum;
import miaosu.common.SettingTypeEnum;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.order.OrderService;
import miaosu.svc.settle.GongYingShangService;
import miaosu.svc.settle.HotelSettleService;
import miaosu.svc.settle.JiuDianService;
import miaosu.svc.vo.PageResult;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/2/3.
 */
@Component
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private UserSettingMapper userSettingMapper;

    @Autowired
    private UserCodeMapper userCodeMapper;

    @Autowired
    private BusinessService businessService;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private HotelSettleService hotelSettleService;
    @Autowired
    private JiuDianService jiuDianService;
    @Autowired
    private GongYingShangService gongYingShangService;




    /**
     * 根据用户名获取用户信息
     * @param userName
     * @return
     */
    public User getUser(String userName){
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUserNameEqualTo(userName).andStatusEqualTo(0);
        List<User> users = userMapper.selectByExample(userExample);
        if (users.size() == 0){
            return null;
        }
        return users.get(0);
    }

    public User getUserById(Long userId){
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUserIdEqualTo(userId);
        List<User> users = userMapper.selectByExample(userExample);
        if (users.size() == 0){
            return null;
        }
        return users.get(0);
    }


    /**
     * 根据用户ID获取用户权限列表
     * @param userId
     * @return
     */
    public List<String> getRoles(Long userId){
        List<String> roles = new ArrayList<String>();

        List<UserRole> userRoles = getUserRoles(userId);
        for (UserRole userRole:userRoles){
            Long roleId = userRole.getRoleId();
            Role role = getRole(roleId);
            if (null != role){
                roles.add(role.getRoleName());
            }
        }

        return roles;
    }

    /**
     * 根据用户ID获取权限信息
     * @param roleId
     * @return
     */
    public Role getRole(Long roleId){
        RoleExample example = new RoleExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        List<Role> roles = roleMapper.selectByExample(example);
        if (roles.size()>0){
            return roles.get(0);
        }
        return null;
    }

    /**
     * 根据用户ID获取相关权限的关系列表
     * @param userId
     * @return
     */
    public List<UserRole> getUserRoles(Long userId){
        UserRoleExample example = new UserRoleExample();
        example.createCriteria().andUserIdEqualTo(userId);
        List<UserRole> userRoles = userRoleMapper.selectByExample(example);
        return userRoles;
    }
    public List<UserRole> getUserRoles(Long userId,Long roleId){
        UserRoleExample example = new UserRoleExample();
        example.createCriteria().andUserIdEqualTo(userId).andRoleIdEqualTo(roleId);
        List<UserRole> userRoles = userRoleMapper.selectByExample(example);
        return userRoles;
    }
    public int inertUserRole(Long userId,Long roleId){
        List<UserRole> userRoles = getUserRoles(userId, roleId);
        if (userRoles.size()==0){
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);
            return userRoleMapper.insert(userRole);
        }
        return 0;
    }

    /**
     * 插入用户
     * @param userName
     * @param password
     * @param nickName
     * @return
     */
    public Long insertUser(String userName, String password,String nickName,RoleEnum role){
        //插入不存在的用户
        User user = getUser(userName);
        if (null == user){
            User record = new User();
            record.setUserName(userName);
            record.setUserPasswd(password);
            record.setStatus(0);
            record.setUserNick(nickName);
            //record.setUser_phone(user_phone);
            userMapper.insert(record);
            user = getUser(userName);
            Long userId = user.getUserId();
            inertUserRole(userId,role.getCode());
            return userId;
        }
        return null;
    }

    public Long getSettingForCurHotel(Long userId){
        if(null == userId){
            return null;
        }
        UserSettingExample example = new UserSettingExample();
        example.createCriteria().andUserIdEqualTo(userId).andSettingTypeEqualTo(SettingTypeEnum.CUR_HOTEL.getCode());
        List<UserSetting> userSettings = userSettingMapper.selectByExample(example);
        if (userSettings.size() == 1){
            return userSettings.get(0).getSettingValue();
        }
        return null;
    }

    public void setForCurHotel(Long userId, Long hotelId) {
        UserSetting record = new UserSetting();
        record.setUserId(userId);
        record.setSettingType(SettingTypeEnum.CUR_HOTEL.getCode());
        record.setSettingValue(hotelId);

        UserSettingExample example = new UserSettingExample();
        example.createCriteria().andUserIdEqualTo(userId).andSettingTypeEqualTo(SettingTypeEnum.CUR_HOTEL.getCode());

        int update = userSettingMapper.updateByExampleSelective(record, example);
        if( update == 0){
            userSettingMapper.insert(record);
        }
    }

    //查询用户电话,查user表
    public String queryUser_phone(Long userId) {
        return userMapper.queryUser_phone(userId);
    }



    //注册之前判断手机号(账号)是否存在,然后返回信息给前端(根据手机号user_phone查账号user_name)
    public String queryUser_name(String userPhone) {
        return userMapper.queryUser_name(userPhone);
    }


    //插入不存在的账号
    public Long insertUser1(String username, String userNick, String userPhone, String password, RoleEnum role) {
            User record = new User();
            record.setUserName(username);
            record.setUserNick(userNick);
            record.setUser_phone(userPhone);
            record.setUserPasswd(password);
            record.setStatus(0);
            userMapper.insert1(record);
            User user = getUser(username);
            Long userId = user.getUserId();
            inertUserRole(userId,role.getCode());
            return userId;
    }

    //重置密码
    public void resetPassword(String password, String username) {
        userMapper.resetPassword(password,username);
    }

    //根据用户id查询用户名user_nick
    public String queryUser_nick(Long currentUserId) {
        return userMapper.queryUser_nick(currentUserId);
    }

    //根据当前用户id查询密码
	public String queryUser_password(Long currentUserId) {
        return userMapper.queryUser_password(currentUserId);
	}

    //修改密码
    public void updateUser_password(Long currentUserId, String password) {
        userMapper.updateUser_password(currentUserId,password);
    }

    //更新openid
    public void updateUserWxOpenid(Long currentUserId, String wxOpenid, String roles) {
        userMapper.updateUserWxOpenid(currentUserId,wxOpenid,roles);
    }

    //查询成员列表
    public PageResult queryMemberListBypage(Long currentUserId, int offset, int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<Member> memberList =  this.queryMemberList(currentUserId);
        long total = objects.getTotal();
        return new PageResult().success(total,memberList);
    }
    public List<Member> queryMemberList(Long currentUserId) {
        //根据当前登录的用户id查询自己的邀请码,再根据邀请码查询成员的邀请码,再根据成员的邀请码查询成员id,再查询成员id对应的成员名字和成员电话
        List<Member> members = userMapper.queryMember(currentUserId);
        for (Member member : members) {
            Long user_id = member.getUser_id();
            //根据成员id查询code_id
            Long code_id=userCodeMapper.queryCode_id(user_id);
            //根据code_id查询审核状态codel_level
            String code_level= userCodeMapper.queryCode_level(code_id);
            //根据用户id查询公司名称
            String business_name= businessService.queryBusiness_name(user_id);

            member.setCode_level(code_level);
            member.setBusiness(business_name);
        }
        return members;
    }

    //根据当前用户id查询对应的角色,二级可以看信息;;三级提示审核中,审核通过才能看,并且改为二级用户
    public List<Long> queryUser_Role(Long currentUserId) {
        return userRoleMapper.queryUser_Role(currentUserId);
    }

    //根据用户id查询其账号user_name
	public String queryUser_nameByUser_id(Long currentUserId) {
        return userMapper.queryUser_nameByUser_id(currentUserId);
	}


    //查询所有的公司
    public PageResult queryBusinessInsertListBypage( int offset, int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<Business> businessList =  this.queryBusinessInsertList();
        long total = objects.getTotal();
        return new PageResult().success(total,businessList);
    }
    public List<Business> queryBusinessInsertList() {
        List<Business> businesses = businessService.queryBusinessInsertList();
        for (Business business : businesses) {
            //显示通过与否
            Long code_id = userCodeMapper.queryCode_id(business.getUser_id());
            String code_level = userCodeMapper.queryCode_level(code_id);
            business.setCode_level(code_level);
        }
        return businesses;
    }

    //点击审核通过的确定,将用户从三级升级为二级,根据用户id去操作
    public void updateUser_role(Long user_id) {
        userRoleMapper.updateUser_role(user_id);
    }

    //供应商结算
    public PageResult querygongyingshangListBypage(Long currentUserId, int offset, int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<GongYingShang> gongyingshangList = null;
        try {
            gongyingshangList = this.querygongyingshangList(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long total = objects.getTotal();
        return new PageResult().success(total,gongyingshangList);
    }

    public List<GongYingShang> querygongyingshangList(Long currentUserId)throws Exception {
        //直接从数据库中获取
        List<GongYingShang> list = Lists.newArrayList();
        Business business = businessService.queryBusinessList(currentUserId);
        //供应商id,只有一个
        Long business_id = business.getBusiness_id();
//        List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
//        for (String settle_StartAndEnd : everyWeekOfYear) {
//            GongYingShang gongYingShang = gongYingShangService.queryAll(settle_StartAndEnd, business_id);
//            if (gongYingShang != null) {
//
//                list.add(gongYingShang);
//            }
//        }
        //不用遍历日期,直接根据business_id获取静态表
        List<GongYingShang>gongYingShangList=gongYingShangService.queryAllGongYingShang(business_id);
        for (GongYingShang gongYingShang : gongYingShangList) {
            if (gongYingShang != null) {
                list.add(gongYingShang);
            }
        }
        //这里引用倒序排列
        gongYingShang(list);
        return list;

//        Business business = businessService.queryBusinessList(currentUserId);
//        //供应商id,只有一个
//        Long business_id = business.getBusiness_id();
//        //根据供应商id查询用户id,只有一个
//        Long user_id = business.getUser_id();
//
//		//根据当前酒店id查询对应供应商审核通过的时间,以此为订单查询开始时间
//		Date success_date = business.getSuccess_date();
//		long settle_StartDateTime1 = success_date.getTime();
//		//当前时间
//		Date date = new Date();
//		long nowTime = date.getTime();
//
////        //查询所有供应商
////        List<Business> businesses = businessService.queryBusinessInsertList();
////        for (Business business : businesses) {
////            GongYingShang gongYingShang = new GongYingShang();
////            gongYingShang.setBusiness_name(business.getBusiness_name());
////            gongYingShang.setBusiness_id(business.getBusiness_id());
////            list.add(gongYingShang);
////        }
//
//        //计算佣金率
//        Integer count=0;
//        Integer yongjinglv=1;
//        //根据当前用户id查询自己的邀请码
//        String code_my = userCodeMapper.queryCode_my(user_id);
//        //根据我的邀请码查询下家的邀请码所对应的用户id
//        List<Long> userIds = userCodeMapper.queryCode_xiajia(code_my);
//        for (Long userId : userIds) {
//            //根据成员id查询邀请码id
//            Long code_id = userCodeMapper.queryCode_id(userId);
//           //根据邀请码id查询code_level的状态
//            String code_level = userCodeMapper.queryCode_level(code_id);
//            if (code_level.contains("已通过")){
//                count++;
//            }
//        }
//        if(count<=15){
//            yongjinglv=15;
//        }
//        if(count>=16&&count<=30){
//            yongjinglv=14;
//        }
//        if(count>=31&&count<=50){
//            yongjinglv=13;
//        }
//        if(count>=51){
//            yongjinglv=12;
//        }
//
//        //获取当前年的每一周:例如20200101~20200105
//        //1.结算周期
//        List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
//        for (String settle_StartAndEnd : everyWeekOfYear) {
//            GongYingShang gongYingShang = new GongYingShang();
//            gongYingShang.setSettle_StartAndEnd(settle_StartAndEnd);
//            //2.供应商名称
//            gongYingShang.setBusiness_name(business.getBusiness_name());
//            //3.供应商id
//            gongYingShang.setBusiness_id(business_id);
//            //4.结算金额=总金额*xx%
//            BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
//            BigDecimal settle_money = new BigDecimal(0);//结算金额
//            BigDecimal commission_money = new BigDecimal(0);//技术服务费
//            String[] split = settle_StartAndEnd.split("~");
//            SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
//            String settle_Start = split[0];//20200101
//            Date settle_StartDate = yyyyMMdd.parse(settle_Start);
//            long settle_StartDateTime = settle_StartDate.getTime();
//            String settle_End = split[1];//20200105
//            Date settle_EndDate = yyyyMMdd.parse(settle_End);
//            long settle_endDateTime = settle_EndDate.getTime();
//
//			//从公司审核通过开始计算时间,到今天的时间为止
//			if (settle_StartDateTime1<=settle_endDateTime&&nowTime>=settle_StartDateTime){
//				//计算结算过的总金额
//            //根据用户id查询名下的酒店id,很多个
//            List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
//            for (HotelRefUser hotelRefUser : hotelRefUsers) {
//                Long hotelId = hotelRefUser.getHotelId();
//                //根据酒店id查询所有对应的订单列表,很多个
//                HotelOrderExample hotelOrderExample = new HotelOrderExample();
//                HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
//                and.andHotelIdEqualTo(hotelId);
//                List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
//                for (HotelOrder hotelOrder : hotelOrders) {
//                    //入住时间
//                    Date checkInTime = hotelOrder.getCheckInTime();
//                    long checkInTimeTime = checkInTime.getTime();
//                    //订单状态
//                    Integer state = hotelOrder.getState();
//                    Integer settleStatus = hotelOrder.getSettleStatus();
//                    //订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
//                    if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime){
//                        //价格
//                        BigDecimal price = hotelOrder.getPrice();
//                        settleTotalMoney = settleTotalMoney.add(price);
//                        settle_money=settleTotalMoney.multiply(new BigDecimal((100-yongjinglv))).divide(new BigDecimal(100));
//                        commission_money=settleTotalMoney.multiply(new BigDecimal(yongjinglv)).divide(new BigDecimal(100));
//                    }
//                    //6.结算状态
//                        gongYingShang.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
//                }
//                //结算金额就是下面的
//                gongYingShang.setSettle_money(String.valueOf(settle_money));
//                //5.技术服务费=总金额*xx%
//                gongYingShang.setCommission_money(String.valueOf(commission_money));
//
//            }
//            list.add(gongYingShang);
//			}
//        }

    }
    //将list中的对象中的某个字段按照倒序排列(供应商的倒序)
    private List<GongYingShang> gongYingShang(List<GongYingShang> list) {
        Collections.sort(list, new Comparator<GongYingShang>() {
            @Override
            public int compare(GongYingShang o1, GongYingShang o2) {
                //降序
                return String.valueOf(o2.getSettle_StartAndEnd()).compareTo(String.valueOf(o1.getSettle_StartAndEnd()));
            }
        });
        return list;
    }
    //酒店的倒序
    private List<JiuDian> jiuDian(List<JiuDian> list) {
        Collections.sort(list, new Comparator<JiuDian>() {
            @Override
            public int compare(JiuDian o1, JiuDian o2) {
                //降序
                return String.valueOf(o2.getSettle_StartAndEnd()).compareTo(String.valueOf(o1.getSettle_StartAndEnd()));
            }
        });
        return list;
    }
    //展示酒店结算
    public PageResult queryjiudianListBypage(Long hotelId, int offset, int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<JiuDian> jiudianList = null;
        try {
            jiudianList = this.queryjiudianListList(hotelId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long total = objects.getTotal();
        return new PageResult().success(total,jiudianList);
    }

    public List<JiuDian> queryjiudianListList(Long hotelId)throws Exception {
        //直接从数据库中获取
        List<JiuDian> list = Lists.newArrayList();
        //根据当前酒店id查询hotel_settle_setting表获取佣金比例和周期设置
        HotelSettleSetting hotelSettleSetting=hotelSettleService.queryHotelSettle(hotelId);
        Integer cycleSetting = hotelSettleSetting.getSettleCycleSetting();
        if (cycleSetting==2){
//            List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
//            JiuDian jiuDian = new JiuDian();
//            for (String settle_StartAndEnd : everyWeekOfYear) {
//                jiuDian = jiuDianService.queryAll(settle_StartAndEnd, hotelId);
//                if (jiuDian != null) {
//                    list.add(jiuDian);
//                }
//            }

            //不用遍历日期,直接根据hotelId获取静态表
            List<JiuDian>jiuDianList=jiuDianService.queryAllJiuDian(hotelId);
            for (JiuDian jiuDian : jiuDianList) {
                if (jiuDian != null) {
                    list.add(jiuDian);
                }
            }

        }
        if (cycleSetting==4){
//            List<String> firstAndLastDayOfMonth = GetEveryMonthOfYearUtil.getFirstAndLastDayOfMonth();
//            JiuDian jiuDian = new JiuDian();
//            for (String settle_StartAndEnd : firstAndLastDayOfMonth) {
//                 jiuDian = jiuDianService.queryAll(settle_StartAndEnd, hotelId);
//                if (jiuDian!=null){
//                    list.add(jiuDian);
//                }
//            }

            //不用遍历日期,直接根据hotelId获取静态表
            List<JiuDian>jiuDianList=jiuDianService.queryAllJiuDian(hotelId);
            for (JiuDian jiuDian : jiuDianList) {
                if (jiuDian != null) {
                    list.add(jiuDian);
                }
            }
        }
        //这里引用倒序排列
        jiuDian(list);
        return list;
//        //根据当前酒店id查询对应供应商审核通过的时间,以此为订单查询开始时间
//        //根据酒店id查询对应的用户id列表(一般有两个)
//        long settle_StartDateTime1=0;
//        Date success_date=null;
//        List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByHotelId(hotelId);
//        for (HotelRefUser hotelRefUser : hotelRefUsers) {
//            Long userId = hotelRefUser.getUserId();
//            //查询business中是否存在用户id
//            Long ifExistUserId = businessService.queryIfExistUserId(userId);
//            if (ifExistUserId!=null){
//                Business business = businessService.queryBusinessList(ifExistUserId);
//                success_date = business.getSuccess_date();
//                settle_StartDateTime1 = success_date.getTime();
//            }
//        }
//        //当前时间
//        Date date = new Date();
//        long nowTime = date.getTime();
//
//        List<JiuDian> list = Lists.newArrayList();
//        //根据当前酒店id查询hotel_settle_setting表获取佣金比例和周期设置
//        HotelSettleSetting hotelSettleSetting=hotelSettleService.queryHotelSettle(hotelId);
//        Integer commissionRate = hotelSettleSetting.getCommissionRate();
//        Integer cycleSetting = hotelSettleSetting.getSettleCycleSetting();
//        //设置结算周期:按周结算
//        if (cycleSetting==2){
//            //获取当前年的每一周:例如20200101~20200105
//            //1.结算周期
//            List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
//            for (String settle_StartAndEnd : everyWeekOfYear) {
//                JiuDian jiuDian = new JiuDian();
//                BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
//                BigDecimal settle_money = new BigDecimal(0);//结算金额
//                BigDecimal commission_money = new BigDecimal(0);//技术服务费
//                String[] split = settle_StartAndEnd.split("~");
//                SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
//                String settle_Start = split[0];//20200101
//                Date settle_StartDate = yyyyMMdd.parse(settle_Start);
//                long settle_StartDateTime = settle_StartDate.getTime();
//                String settle_End = split[1];//20200105
//                Date settle_EndDate = yyyyMMdd.parse(settle_End);
//                long settle_endDateTime = settle_EndDate.getTime();
//
//                //从公司审核通过开始计算时间,到今天的时间为止
//                if (settle_StartDateTime1<=settle_endDateTime&&nowTime>=settle_StartDateTime){
//                    //根据酒店id查询所有对应的订单列表,很多个
//                    HotelOrderExample hotelOrderExample = new HotelOrderExample();
//                    HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
//                    and.andHotelIdEqualTo(hotelId);
//                    List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
//                    for (HotelOrder hotelOrder : hotelOrders) {
//                        jiuDian.setSettle_StartAndEnd(settle_StartAndEnd);
//                        //入住时间
//                        Date checkInTime = hotelOrder.getCheckInTime();
//                        long checkInTimeTime = checkInTime.getTime();
//                        //订单状态
//                        Integer state = hotelOrder.getState();
//                        if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime){
//                            //价格
//                            BigDecimal price = hotelOrder.getPrice();
//                            settleTotalMoney = settleTotalMoney.add(price);
//                            settle_money=settleTotalMoney.multiply(new BigDecimal((100-commissionRate))).divide(new BigDecimal(100));
//                            commission_money=settleTotalMoney.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
//                            //6.结算状态
//                            jiuDian.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
//                        }
//                    }
//                    //结算金额
//                    jiuDian.setSettle_money(String.valueOf(settle_money));
//                    //技术服务费
//                    jiuDian.setCommission_money(String.valueOf(commission_money));
//                    list.add(jiuDian);
//                }
//            }
//        }
//        //按月结算
//        if (cycleSetting==4){
//            //获取每月的月初和月末:20200101~20200131
//            List<String> firstAndLastDayOfMonth = GetEveryMonthOfYearUtil.getFirstAndLastDayOfMonth();
//            for (String settle_StartAndEnd : firstAndLastDayOfMonth) {
//                JiuDian jiuDian = new JiuDian();
//                BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
//                BigDecimal settle_money = new BigDecimal(0);//结算金额
//                BigDecimal commission_money = new BigDecimal(0);//技术服务费
//                String[] split = settle_StartAndEnd.split("~");
//                SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
//                String settle_Start = split[0];//20200101
//                Date settle_StartDate = yyyyMMdd.parse(settle_Start);
//                long settle_StartDateTime = settle_StartDate.getTime();
//                String settle_End = split[1];//20200131
//                Date settle_EndDate = yyyyMMdd.parse(settle_End);
//                long settle_endDateTime = settle_EndDate.getTime();
//                if (settle_endDateTime>=settle_StartDateTime1&&nowTime>=settle_StartDateTime){
//                    //根据酒店id查询所有对应的订单列表,很多个
//                HotelOrderExample hotelOrderExample = new HotelOrderExample();
//                HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
//                and.andHotelIdEqualTo(hotelId);
//                List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
//                for (HotelOrder hotelOrder : hotelOrders) {
//                    jiuDian.setSettle_StartAndEnd(settle_StartAndEnd);
//                    //入住时间
//                    Date checkInTime = hotelOrder.getCheckInTime();
//                    long checkInTimeTime = checkInTime.getTime();
//                    //订单状态
//                    Integer state = hotelOrder.getState();
//                    if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime){
//                        //价格
//                        BigDecimal price = hotelOrder.getPrice();
//                        settleTotalMoney = settleTotalMoney.add(price);
//                        settle_money=settleTotalMoney.multiply(new BigDecimal((100-commissionRate))).divide(new BigDecimal(100));
//                        commission_money=settleTotalMoney.multiply(new BigDecimal(commissionRate)).divide(new BigDecimal(100));
//                        //6.结算状态
//                        jiuDian.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
//                    }
//                }
//                //结算金额
//                jiuDian.setSettle_money(String.valueOf(settle_money));
//                //技术服务费
//                jiuDian.setCommission_money(String.valueOf(commission_money));
//                list.add(jiuDian);
//            }
//            }
//        }
    }

    //查询所有公司的详细信息
    public PageResult quertSupplierList(int offset, int limit) {
        Page<Object> objects = PageHelper.offsetPage(offset, limit);
        List<Business> businessList =  this.queryBusinessInsertList();
        for (Business business : businessList) {
            Long business_type = business.getBusiness_type();
            if (null==business_type){
				business.setBusiness_typeStr(null);
			}else {
            if (business_type==1){
                business.setBusiness_typeStr("家具");
            }else if (business_type==2){
                business.setBusiness_typeStr("装修");
            }else if (business_type==3){
                business.setBusiness_typeStr("设计");
            }else if (business_type==4){
                business.setBusiness_typeStr("地毯");
            }else if (business_type==5){
                business.setBusiness_typeStr("灯饰");
            }else if (business_type==6){
                business.setBusiness_typeStr("五金");
            }else if (business_type==7){
                business.setBusiness_typeStr("智能化");
            }else if (business_type==8){
                business.setBusiness_typeStr("其他");
            }
            }
            Long businessOrFactory = business.getBusinessOrFactory();
            if (null==businessOrFactory){
				business.setBusinessOrFactoryStr(null);
			}else {
            if (businessOrFactory==1){
                business.setBusinessOrFactoryStr("经销商");
            }else if (businessOrFactory==2){
                business.setBusinessOrFactoryStr("厂家");
            }
			}
            //查询图片
			Long user_id = business.getUser_id();
			Picture picture=businessService.queryPicture(user_id);
            if (null==picture){
                business.setImg_businessLicense(null);
                business.setImg_shopHead(null);
                business.setImg_factoryBook(null);
            }else {
                byte[] img_businessLicense = picture.getImg_businessLicense();
                String encode1 = org.apache.commons.codec.binary.Base64.encodeBase64String(img_businessLicense);
                String base64_1="data:image/png;base64,"+encode1;
                business.setImg_businessLicense(base64_1);

                byte[] img_shopHead = picture.getImg_shopHead();
                String encode2 = org.apache.commons.codec.binary.Base64.encodeBase64String(img_shopHead);
                String base64_2="data:image/png;base64,"+encode2;
                business.setImg_shopHead(base64_2);

                byte[] img_factoryBook = picture.getImg_factoryBook();
                String encode3 = Base64.encodeBase64String(img_factoryBook);
                String base64_3="data:image/png;base64,"+encode3;
                business.setImg_factoryBook(base64_3);
            }
            //供应商关联酒店
			List<HotelInfo> list = Lists.newArrayList();
			List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
			if (!CollectionUtils.isEmpty(hotelRefUsers)){
				for (HotelRefUser hotelRefUser : hotelRefUsers) {
					Long hotelId = hotelRefUser.getHotelId();
                    HotelInfo hotelInfo = hotelService.getHotelInfoById(hotelId);
                    list.add(hotelInfo);
                }
			}
			business.setHotelInfoList(list);
		}
        long total = objects.getTotal();
        return new PageResult().success(total,businessList);
    }
}
