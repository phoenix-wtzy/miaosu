package miaosu.svc.settle;

import miaosu.dao.auto.GongYingShangMapper;
import miaosu.dao.model.GongYingShang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/8/24 14:25
 * @Description:
 */
@Component
public class GongYingShangService {
	@Autowired
	private GongYingShangMapper gongYingShangMapper;
	//添加到gongyingshang结算表中
	public void insert(GongYingShang gongYingShang) {
		gongYingShangMapper.insert(gongYingShang);
	}

	//根据周期和酒店id查询供应商信息(添加之前先判断是否存在,存在就不做处理)
	public GongYingShang queryAll(String settle_startAndEnd, Long business_id) {
		return gongYingShangMapper.queryAll(settle_startAndEnd,business_id);
	}

	//更新结算信息
	public void update(String settle_startAndEnd, Long business_id, String settle_money1, String commission_money1, String settle_status) {
		gongYingShangMapper.update(settle_startAndEnd,business_id,settle_money1,commission_money1,settle_status);
	}

	//不用遍历日期,直接根据business_id获取静态表
	public List<GongYingShang> queryAllGongYingShang(Long business_id) {
		return gongYingShangMapper.queryAllGongYingShang(business_id);
	}
}
