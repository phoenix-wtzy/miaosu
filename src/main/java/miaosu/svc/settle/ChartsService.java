package miaosu.svc.settle;

import miaosu.dao.auto.ChartsMapper;
import miaosu.dao.model.Charts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @Date: 2021/1/4 9:46
 * @Description:
 */
@Component
public class ChartsService {

	@Autowired
	private ChartsMapper chartsMapper;


	//将首页charts数据添加到表charts中
	public Long insert(Charts charts) {
		return chartsMapper.insert(charts);
	}

	//查询charts表中数据(今天之前的)
	public Charts query() {
		return chartsMapper.query();
	}
}
