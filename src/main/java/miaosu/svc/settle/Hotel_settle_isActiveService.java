package miaosu.svc.settle;

import miaosu.dao.auto.Hotel_settle_isActiveMapper;
import miaosu.dao.model.Hotel_settle_isActive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/12/15 16:25
 * @Description:
 */
@Component
public class Hotel_settle_isActiveService {

	@Autowired
	private Hotel_settle_isActiveMapper hotel_settle_isActiveMapper;


	//添加到hotel_settle_isActive表中
	public void insert(Hotel_settle_isActive hotel_settle_isActive) {
		hotel_settle_isActiveMapper.insert(hotel_settle_isActive);
	}

	//查询表hotel_settle_isactive前天所有数据
	public List<Hotel_settle_isActive> queryAll() {
		return hotel_settle_isActiveMapper.queryAll();
	}

	//下线酒店添加到hotel_settle_isnotactive表中
	public void insertInhotel_settle_isnotactive(Hotel_settle_isActive hotel_settle_isActive) {
		hotel_settle_isActiveMapper.insertInhotel_settle_isnotactive(hotel_settle_isActive);
	}

	//查询下线酒店表hotel_settle_isnotactive前天所有数据
	public List<Hotel_settle_isActive> queryAllInhotel_settle_isnotactive() {
		return hotel_settle_isActiveMapper.queryAllInhotel_settle_isnotactive();
	}
}
