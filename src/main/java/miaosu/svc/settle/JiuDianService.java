package miaosu.svc.settle;

import miaosu.dao.auto.JiuDianMapper;
import miaosu.dao.model.JiuDian;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/8/22 10:16
 * @Description:
 */
@Component
public class JiuDianService {
	@Autowired
	private JiuDianMapper jiuDianMapper;
	//添加到jiudian结算表中
	public void insert(JiuDian list) {
		jiuDianMapper.insert(list);
	}

	//添加之前先判断是否存在,存在就不做处理
	public JiuDian queryAll(String settle_startAndEnd, Long hotel_id) {
		return jiuDianMapper.queryAll(settle_startAndEnd,hotel_id);
	}

	//更新结算信息
	public void update(String settle_startAndEnd, Long hotelId, String settle_money1, String commission_money1, String settle_status) {
		jiuDianMapper.update(settle_startAndEnd,hotelId,settle_money1,commission_money1,settle_status);
	}

	//不用遍历日期,直接根据hotelId获取静态表
	public List<JiuDian> queryAllJiuDian(Long hotelId) {
		return jiuDianMapper.queryAllJiuDian(hotelId);
	}
}
