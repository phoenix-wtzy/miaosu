package miaosu.svc.settle;

import lombok.extern.slf4j.Slf4j;
import miaosu.common.CycleSettleEnum;
import miaosu.common.SettleStatusEnum;
import miaosu.dao.auto.*;
import miaosu.dao.model.*;
import miaosu.svc.order.OrderService;
import miaosu.svc.vo.HotelRoomSetVO;
import miaosu.svc.vo.HotelSettleBillVO;
import miaosu.svc.vo.HotelSettleSettingVO;
import miaosu.svc.vo.SettleInfoVO;
import miaosu.utils.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * 结算服务类
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
public class HotelSettleService {

    @Autowired
    private HotelSettleSettingMapper settingMapper;

    @Autowired
    private HotelSettleBillMapper settleBillMapper;

    @Autowired
    private HotelOrderMapper orderMapper;

    @Autowired
    private SettleRefOrderMapper settleRefOrderMapper;

    @Autowired
    private OrderService orderService;

    public HotelSettleSetting queryHotelSettle(long hotelId) {
        HotelSettleSettingExample settleSettingExample = new HotelSettleSettingExample();
        settleSettingExample.createCriteria().andHotelIdEqualTo(hotelId);

        List<HotelSettleSetting> hotelSettleSettings = settingMapper.selectByExample(settleSettingExample);

        if(hotelSettleSettings != null && hotelSettleSettings.size()==1){
            HotelSettleSetting hotelSettleSetting = hotelSettleSettings.get(0);
            return  hotelSettleSetting;
        }
        return null;
    }


    public Integer queryCommissionRate(long hotelId) {
       HotelSettleSetting settleSetting = queryHotelSettle(hotelId);
       if(settleSetting != null){
            return settleSetting.getCommissionRate();
       }
       return null;
    }

    public Integer queryCycleSetting(long hotelId) {
        HotelSettleSetting settleSetting = queryHotelSettle(hotelId);
        if(settleSetting != null){
            return settleSetting.getSettleCycleSetting();
        }
        return null;
    }

    /**
     * 设置对应酒店的结算信息
     * @param settleSetting 酒店结算设置
     * @return 更新成功与否的标识
     */
    public boolean updateSettleInfo(HotelSettleSetting settleSetting) {
        HotelSettleSettingExample settleSettingExample = new HotelSettleSettingExample();
        settleSettingExample.createCriteria().andHotelIdEqualTo(settleSetting.getHotelId());

        List<HotelSettleSetting> hotelSettleSettings = settingMapper.selectByExample(settleSettingExample);
        int len = 0;
        if(hotelSettleSettings == null || (len = hotelSettleSettings.size())>1){
            return false;
        }
        int flag = 0;
        if(len == 0){
            //新增
            flag = settingMapper.insert(settleSetting);
        }

        if(len == 1){
            //更新
            long id = hotelSettleSettings.get(0).getId();
            settleSetting.setId(id);
            flag = settingMapper.updateByPrimaryKeySelective(settleSetting);
        }

       return flag==1?true:false;
    }

    /**
     * 添加结算账单信息
     * @param hotelId 酒店id
     * @param dateStart 开始时间
     * @param dateEnd 结束时间
     * @param status 订单状态
     * @return
     */
    public HotelSettleBillVO showSettleBill(Long hotelId, String dateStart, String dateEnd, Integer status) {

        HotelOrderExample orderExample = new HotelOrderExample();
        HotelOrderExample.Criteria criteria = orderExample.createCriteria();
        criteria.andHotelIdEqualTo(hotelId);
        Date startDate = null;
        Date endDate = null;
        try {
            if (StringUtils.isNotEmpty(dateStart) && StringUtils.isNotEmpty(dateEnd)) {
                startDate = DateUtils.parseDate(dateStart, "yyyy-MM-dd");
                endDate = DateUtils.parseDate(dateEnd, "yyyy-MM-dd");
                endDate = DateUtils.addDays(endDate, 1);

                criteria.andBookTimeBetween(startDate, endDate);

                if (status != 0) {
                    criteria.andStateEqualTo(status);
                }
                //查询已经进行并没有结算的订单
                criteria.andSettleStatusEqualTo(0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        SettleInfoVO settleInfo = orderService.queryOrderSettleInfo(orderExample,hotelId);

        //
        HotelSettleSetting settleSetting = this.queryHotelSettle(hotelId);

        //查询出来订单信息
        HotelSettleBillVO settleBill = new HotelSettleBillVO();
        settleBill.setHotelId(hotelId);
        settleBill.setCommissionMoney(new BigDecimal(settleInfo.getCommissionMoney()));
        settleBill.setSettleMoney(new BigDecimal(settleInfo.getSettleMoney()));
        settleBill.setOrderIds(settleInfo.getOrderIds());
        settleBill.setSettleBankType(settleSetting.getBankAccountType());
        settleBill.setSettleBankAccount(settleSetting.getBankAccountCardnumber());
        settleBill.setSettleCycle(settleSetting.getSettleCycleSetting());
        settleBill.setStartDate(startDate);
        settleBill.setEndDate(endDate);

        return settleBill;
    }

    public boolean addSettleBill(HotelSettleBillVO settleBillVO) {

        try {
            //添加结算单信息
            HotelSettleBill hotelSettleBill = new HotelSettleBill();

            BeanUtils.copyProperties(settleBillVO, hotelSettleBill);
            Date now = new Date();
            hotelSettleBill.setCreateTime(now);
            hotelSettleBill.setSettleStatus(SettleStatusEnum.SETTLEING.getCode());

            settleBillMapper.insertSelective(hotelSettleBill);

            String orderIds = settleBillVO.getOrderIds();

            Long settleBillId = hotelSettleBill.getId();
            String[] orderIdArray = orderIds.split(",");
            for (String orderIdStr : orderIdArray) {
                //更新订单所有的结算状态
                Long orderId = Long.parseLong(orderIdStr);
                HotelOrder hotelOrder = new HotelOrder();
                hotelOrder.setOrderId(orderId);
                hotelOrder.setSettleStatus(SettleStatusEnum.SETTLEING.getCode());
                orderMapper.updateByPrimaryKeySelective(hotelOrder);
                //添加两者之间的关联关系
                SettleRefOrder settleRefOrder = new SettleRefOrder();
                settleRefOrder.setBillId(settleBillId);
                settleRefOrder.setOrderId(orderId);
                settleRefOrderMapper.insertSelective(settleRefOrder);
            }

        }catch (Exception e){
            return  false;
        }
        return true;
    }


    public boolean settleMsgPhoneDelete(Long settleId, String phone) {

        HotelSettleSetting settleSetting = new HotelSettleSetting();
        settleSetting.setId(settleId);
        settleSetting.setSettleMsgPhone("");
        int flag =settingMapper.updateByPrimaryKeySelective(settleSetting);

        return flag ==1?true:false;
    }
}
