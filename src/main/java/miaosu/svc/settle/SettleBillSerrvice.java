package miaosu.svc.settle;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import miaosu.common.CycleSettleEnum;
import miaosu.common.SettleStatusEnum;
import miaosu.dao.auto.HotelOrderMapper;
import miaosu.dao.auto.HotelSettleBillMapper;
import miaosu.dao.auto.InvoiceRecordMapper;
import miaosu.dao.auto.SettleRefOrderMapper;
import miaosu.dao.model.*;
import miaosu.svc.invoice.InvoiceRecordService;
import miaosu.svc.vo.HotelOrderVO;
import miaosu.svc.vo.HotelSettleBillVO;
import miaosu.svc.vo.PageResult;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
public class SettleBillSerrvice {

    @Autowired
    private HotelSettleBillMapper settleBillMapper;
    @Autowired
    private SettleRefOrderMapper settleRefOrderMapper;

    @Autowired
    private HotelOrderMapper orderMapper;

    @Autowired
    private InvoiceRecordService invoiceRecordService;

    public PageResult queryBillListByPage(Long hotelId, String dateStart, String dateEnd, Integer settleStatus,boolean isAdmin, int offset, int limit) {
        Page<Object> pageInfo = PageHelper.offsetPage(offset, limit);

        List<HotelSettleBillVO> list = this.queryBillList(hotelId, dateStart, dateEnd, settleStatus,isAdmin);
        long total = pageInfo.getTotal();
        return new PageResult().success(total, list);
    }


    public List<Long> queryOrderIdsByBill(Long billId) {
        SettleRefOrderExample settleRefOrderExample = new SettleRefOrderExample();

        settleRefOrderExample.createCriteria().andBillIdEqualTo(billId);

        List<Long> orderIds = Lists.newArrayList();
        List<SettleRefOrder> settleRefOrders = settleRefOrderMapper.selectByExample(settleRefOrderExample);

        for (SettleRefOrder settleRefOrder : settleRefOrders) {
            orderIds.add(settleRefOrder.getOrderId());
        }

        return orderIds;
    }

    public HotelSettleBill queryBillListById(Long billId) {
        return settleBillMapper.selectByPrimaryKey(billId);
    }

    public List<HotelSettleBillVO> queryBillList(Long hotelId, String dateStart, String dateEnd, Integer settleStatus,Boolean isAdmin) {
        List<HotelSettleBillVO> list = Lists.newArrayList();
        try {
            Date startDate = DateUtils.parseDate(dateStart, "yyyy-MM-dd");
            Date endDate = DateUtils.parseDate(dateEnd, "yyyy-MM-dd");
            endDate = DateUtils.addDays(endDate, 1);
            HotelSettleBillExample billExample = new HotelSettleBillExample();
            HotelSettleBillExample.Criteria criteria = billExample.createCriteria().andHotelIdEqualTo(hotelId).andCreateTimeBetween(startDate, endDate);

            if (settleStatus != null) {
                criteria.andSettleStatusEqualTo(settleStatus);
            }

            List<HotelSettleBill> hotelSettleBills = settleBillMapper.selectByExample(billExample);

            for (HotelSettleBill hotelSettleBill : hotelSettleBills) {
                HotelSettleBillVO hotelSettleBillVO = new HotelSettleBillVO();
                BeanUtils.copyProperties(hotelSettleBill, hotelSettleBillVO);

                //结算周期
                hotelSettleBillVO.setSettleCycleName(CycleSettleEnum.fromCode(hotelSettleBill.getSettleCycle()).getName());
                //结算状态
                hotelSettleBillVO.setSettleStatusName(SettleStatusEnum.fromCode(hotelSettleBill.getSettleStatus()).getName());

                //银行卡模糊
                //6212********3244
                String bankAccount = hotelSettleBill.getSettleBankAccount();
                StringBuilder bankAccountFuzzy = new StringBuilder(bankAccount.substring(0, 4));
                bankAccountFuzzy.append("********");
                bankAccountFuzzy.append(bankAccount.substring(bankAccount.length() - 5, bankAccount.length()));
                hotelSettleBillVO.setSettleBankAccountFuzzy(bankAccountFuzzy.toString());

                //查询是否完成酒店信息
                hotelSettleBillVO.setHasCreated(invoiceRecordService.queryisHaveInvoice(hotelSettleBill.getId(),isAdmin));

                list.add(hotelSettleBillVO);
            }

        } catch (ParseException e) {
              e.printStackTrace();
        }

        return  list;
    }

    public List<Map<String,Object>> queryBillListForExcel(Long hotelId, String dateStart, String dateEnd, Integer settleStatus,Boolean isAdmin) {
        List<HotelSettleBillVO> settleBillVOS = this.queryBillList(hotelId,dateStart,dateEnd,settleStatus,isAdmin);

        List<Map<String,Object>> excelMaps = Lists.newArrayList();
        for (HotelSettleBillVO hotelSettleBillVO: settleBillVOS){
            Map<String,Object> settleMap = JsonUtils.parseJSONMap(JsonUtils.objectToJson(hotelSettleBillVO));
            excelMaps.add(settleMap);
        }

        return  excelMaps;
    }

    public boolean updateSettleStatus(Long billId, Integer status) {

        HotelSettleBill bill = new HotelSettleBill();
        bill.setId(billId);
        bill.setSettleStatus(status);
        bill.setSettleTime(new Date());

        try{
            settleBillMapper.updateByPrimaryKeySelective(bill);

            List<Long> orderIds = this.queryOrderIdsByBill(billId);

            for(Long orderId:orderIds){
                HotelOrder order = new HotelOrder();
                order.setOrderId(orderId);
                order.setSettleStatus(status);
                orderMapper.updateByPrimaryKeySelective(order);
            }
        }catch (Exception e){
           return false;
        }

        return true;
    }
}
