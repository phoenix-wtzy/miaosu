package miaosu.runtimeexception;

/**
 * Created by jiajun.chen on 2018/4/21.
 */
public class ExistException extends RuntimeException{
    public ExistException(String msg){
        super(msg);
    }
}
