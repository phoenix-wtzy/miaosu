package miaosu.config;

import miaosu.dao.model.User;
import miaosu.svc.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/2/3.
 */
@Component
public class CustUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(final String userName) throws UsernameNotFoundException {

        final User user = userService.getUser(userName);
        if (null == user){
            throw new UsernameNotFoundException("can't find user");
        }

        return new CustUserDetail(user);
    }

    public class CustUserDetail implements UserDetails{
        private User user;

        public CustUserDetail(User user){
            this.user = user;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
//            boolean flag = false;
//            if (flag) {
//                return AuthorityUtils.commaSeparatedStringToAuthorityList("");
//            }

            StringBuilder commaBuilder = new StringBuilder();
            List<String> roles = userService.getRoles(user.getUserId());
            for(String role:roles){
                commaBuilder.append(role);
                commaBuilder.append(",");
            }
//                commaBuilder.append("SUPPER MANAGER");
//                commaBuilder.append(",");
//                commaBuilder.append("ROLE_ADMIN");
            return AuthorityUtils.commaSeparatedStringToAuthorityList(commaBuilder.toString());
        }

        public User getUser(){
            return this.user;
        }

        @Override
        public String getPassword() {
            return user.getUserPasswd();
        }

        @Override
        public String getUsername() {
            return user.getUserNick();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}
