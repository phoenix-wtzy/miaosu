package miaosu.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * Created by jiajun.chen on 2018/1/9.
 * 高级写法：https://www.jianshu.com/p/4468a2fff879
 * 管理后台：https://github.com/PanJiaChen/vue-element-admin/
 */
@Configuration
//开启权限控制注解
//@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    //决策管理器
    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    //认证管理器
    @Autowired
    private CustAuthenticationProvider custAuthenticationProvider;

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//
//        http
//            // 关闭csrf保护功能（跨域访问）
//            .csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/admin/**").permitAll();
//}


    // roles admin allow to access /admin/**
    // roles user allow to access /user/**
    // custom 403 access denied handler
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/home", "/about/","/img/*","/wechat/*").permitAll()
                .antMatchers("/admin/**","/upload/**").hasAnyRole("ADMIN","SUPER","OTA")
                //.antMatchers("/admin/**","/upload/**").permitAll()
                .antMatchers("/order/**").hasAnyRole("USER","ADMIN","SUPER","OTA")
                .antMatchers("/room/**").hasAnyRole("USER","ADMIN","SUPER","OTA")
                .antMatchers("/address/**").permitAll()
                .antMatchers("/get/**","/zhuce","/zhuce/**","/forget","/forget/**",
                        "/msgVoiceNotify/**","/wechat/wechatAccess/**","/wechatAccess/**","/evaluation","/evaluation/**").permitAll()//添加爬虫接受数据的允许权限,放行注册页面及其下的路径/忘记密码进行放行
                //.antMatchers("/room/**").permitAll()
                .antMatchers("/api/CSZY/dp/order/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    }

    // create two users, admin and user
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

//        auth.inMemoryAuthentication()
//                .withUser("user").password("user").roles("USER")
//                .and()
//                .withUser("admin").password("admin").roles("ADMIN");

//        auth.jdbcAuthentication()

        auth.authenticationProvider(custAuthenticationProvider);
    }
}