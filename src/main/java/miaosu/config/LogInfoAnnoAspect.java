package miaosu.config;
import miaosu.annonation.LogInfoAnnotation;
import miaosu.dao.model.HotelInfo;
import miaosu.svc.hotel.ProductService;
import miaosu.svc.log.OpLogService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.qhh.OplogVO;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.io.ObjectInputStream;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 切面打印服务层日志信息 使用注解的形式
 */
@Aspect
@Component
public class LogInfoAnnoAspect {
    private static final Logger logger = LoggerFactory.getLogger(LogInfoAnnoAspect.class);

    //日志服务类
    @Autowired
    private OpLogService opLogService;

    @Autowired
    private ProductService productService;

    @Autowired
    public UserService userService;

    // 后期 切入点 可以为指定的注解（比如时间花费注解，日志注解等）
    //需要实现一个相关的注解
    @Pointcut("@annotation(miaosu.annonation.LogInfoAnnotation)")
    public void logAnonation() {}

    /*
    @Before("logAnonation()")
    public void logBefore(JoinPoint pjp){
        MethodSignature methodSignature = ( MethodSignature) pjp.getSignature();
        String methodName = methodSignature.getMethod().getName();
        String className = pjp.getTarget().getClass().getName();
        Object[] argsInfo = pjp.getArgs();
        logger.info("日志打印开始 class info : {}, method info : {}, args info: {}",className,methodName, JsonUtils.objectToJson(argsInfo));
    }
    */


    @After("logAnonation()")
    public void logAfter(JoinPoint pjp){

       Date createDate = new Date();
       MethodSignature methodSignature = ( MethodSignature) pjp.getSignature();
       Method method = methodSignature.getMethod();
       LocalVariableTableParameterNameDiscoverer u =
                new LocalVariableTableParameterNameDiscoverer();

        //获取参数名称
        String[] argsName = u.getParameterNames(method);
        //获取参数信息
        Object[] argsInfo = pjp.getArgs();
        Map<String,Object> params = convertMap(argsName,argsInfo);

        LogInfoAnnotation logInfoAnnotation =  method.getAnnotation(LogInfoAnnotation.class);
        //获取操作类型
        Integer opType = logInfoAnnotation.opType().getCode();
        //获取酒店的房型信息或者产品信息

        //操作人	房型	操作内容	操作类型
        //获取操作人id
        Long userId = getCurrentUserId();


        Long hotelId = getCurHotel();
        Long[] roomTypeId = findRoomTypeIds(hotelId,params,logInfoAnnotation);

        String startDate = findValueFromMap(params,logInfoAnnotation.startDateFieldName());
        String endDate = findValueFromMap(params,logInfoAnnotation.endDateFieldName());

        String opValue = findValueFromMap(params,logInfoAnnotation.opValueFieldName());

        //根据@Log注解的params一一对应获取对应的参数值
        OplogVO oplogVO = new OplogVO();
        oplogVO.setUserId(userId);
        oplogVO.setHotelId(hotelId);
        oplogVO.setOpType(opType);
        oplogVO.setStartDate(startDate);
        oplogVO.setEndDate(endDate);
        oplogVO.setOpValue(opValue);
        oplogVO.setCreateTime(createDate);
        oplogVO.setHotelRoomTypeIds(roomTypeId);
        opLogService.saveOpLogs(oplogVO);

    }

    private String findValueFromMap(Map<String, Object> params, String fieldName) {
       Object fieldValue = params.get(fieldName);
       if(fieldValue.getClass().isArray()){
           StringBuilder builder = new StringBuilder();
           if(fieldValue instanceof  Number[]){
                Long[] array = (Long[]) fieldValue;
                for(Long num:array){
                     builder.append(num).append(",");
               }
           }
           if(fieldValue instanceof  String[]){
               String[] array = (String[]) fieldValue;
               for(String num:array){
                   builder.append(num).append(",");
               }
           }
           String result = builder.substring(0,builder.length()-1);
           logger.info("result:{}",result);
           return result;
       }
       return fieldValue+"";
    }


    private Long[] findRoomTypeIds(Long hotelId,Map<String, Object> params, LogInfoAnnotation logInfoAnnotation) {
        String roomTypeFieldName = logInfoAnnotation.roomTypeFiledName();
        String productFiledName = logInfoAnnotation.productFiledName();
        if(StringUtils.isNotBlank(roomTypeFieldName)){
           return (Long[])params.get(roomTypeFieldName);
        }
        if(StringUtils.isNotBlank(productFiledName)){
            Long[] productIds = (Long[])params.get(productFiledName);
           return productService.queryRoomTypeIdByProductId(hotelId,productIds);
        }
        return null;
    }

    private Map<String,Object> convertMap(String[] argsName, Object[] argsInfo) {
        Map<String,Object> params = new HashMap<String, Object>();
        for(int i=0;i<argsInfo.length;i++){
            params.put(argsName[i],argsInfo[i]);
         }
         return  params;
    }


    private CustUserDetailsService.CustUserDetail getCurrentUser(){
        // 获取spring security封装的当前用户信息对象
        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if("anonymousUser".equals(object)){
            return null;
        }
        return (CustUserDetailsService.CustUserDetail) object;
    }

    private Long getCurrentUserId(){
        CustUserDetailsService.CustUserDetail userDetail = getCurrentUser();
        if(null == userDetail){
            return null;
        }
        return userDetail.getUser().getUserId();
    }

    /**
     * 获取当前配置信息，选择的酒店
     * @return
     */
    public Long getCurHotel(){
        return userService.getSettingForCurHotel(this.getCurrentUserId());
    }

}
