package miaosu.config;

public class InvoiceConfig {

    /**
     * 测试环境地址 开具蓝票
     *
     */
    public final static String TEST_URL = "http://fpkj.testnw.vpiaotong.cn/tp/openapi/invoiceBlue.pt";

    /**
     * 测试环境 查询生成电子发票的票务信息
     */
    public final static String TEST_INVOICE_URL ="http://fpkj.testnw.vpiaotong.cn/tp/openapi/queryInvoiceInfo.pt";

    /**
     *
     * 平台前缀(简称)
     */
    public final static String PLATFORM_PREFIX = "MSPT";
    /**
     * 平台编码
     */
    public final static String PLATFORM_CODE ="Y5311nYG";
    /**
     * 3DES密钥
     */
    public final static String DESS_ECRET_KEY = "4JMROe440C26Ne4eDLSM69l2";

    /**
     * 票通公钥
     */
    public final static String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJkx3HelhEm/U7jOCor29oHsIjCMSTyKbX5rpoAY8KDIs9mmr5Y9r+jvNJH8pK3u5gNnvleT6rQgJQW1mk0zHuPO00vy62tSA53fkSjtM+n0oC1Fkm4DRFd5qJgoP7uFQHR5OEffMjy2qIuxChY4Au0kq+6RruEgIttb7wUxy8TwIDAQAB";
    /**
     *测试环境开票使用的税号
     */
    public final static String TAX_NUM="110105201606160003";
    /**
     * 企业名称
     */
    public final static String FIRM_NAME = "110105201606160003test";

    /**
     * 私钥
     */
    public final static String PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKWLlKdufD3hEGbOWwfiMN5SBX12NjRX8QcRbRudtfNbG+RLgd7Ggu75GXVdANjif98IpWAaYUSDEc2RklArGDSRv3PDQqPlllt9dcagoWNDy7Hx7gLZqINOyK0xiXBP3NbNSHeh2oGgqJfGD6Hej7hXLVW6ospN2LevdDqYhb6zAgMBAAECgYBmu04O8VOMmwSwp9J5+/ZToRbjGt7ccM6Ukk3hW2e0k4+lhu4bL5nFbzRLTuHdtgGgiR8HmpdOCwCbT4E62uNzjJRKodXo2R/c2HJ7GK+4uuauNLNXlOX8REEmcyv1sHmSNKcbBGigubbGBLrOEUqB/N3Q68QGwXlpsLJMszmZAQJBAM76mRW+OJ4rhaXtPfDw6NKl52ugE6YYn53gBmCudXkamXBNzx/CPPsbsXfcjOcFKqf7Im5UwF76h7N5RlDx68ECQQDMwM4zf/732Xs8ag761CjZD+EJszZQ306SPs6lVQnkjGq4blERL2MiqhIBZpEBz3IbArplCVlmGTkfkeBkSpdzAkEAnjdfqAhFnVrlieXlUHXHc6aQVWwDORdfhD5IgcU6VT2pBvjjmIJRPR55NYepSbtgVRIayr4MY04dR2sv/hY/wQJAbOH/BY4EZYMGsOrpimN2UW/LSSA/OyR1IPo7Y9pkB2DQg8FOZOghPNbZ+lqztBW/5TM/MemV2+B/6xTlEhBElwJAbCTbyEba6FJf0n1IcOLiVjs51VEXQUZUzPZLul5U/KSD5R1PlTjQU+3WFF7tAG5XwL25KHzI8H/hLWrT5QuSag==";

    /**
     * 销货方名称
     */
    public final static String SELLER_COMPAY="北京秒宿信息科技有限公司";

    /**
     * 销货方地址  (可不使用)
     */
    public final static String SELLER_ADDRESS="北京市海淀区西小口路";

    /**
     * 销货方电话   (可不使用)
     */
    public final static String SELLER_TEL="17835424859";
    /**
     * 销货方银行账号
     */
    public final static String SELLER_BANKACCOUNT="1241100055135512";
    /**
     * 销货方开户行
     */
    public final static String SELLER_BANKNAME = "招商银行北京大运村支行";

    /**
     *收款人名称
     */
    public final static  String CASHER_NAME ="曾子玄";

    /**
     * 复核人名称
     */
    public final static  String REVIEWER_NAME="王丽";
    /**
     * 开票人名称
     */
    public final static  String DRAWER_NAME="姬婷婷";

    /**
     * 对应税收分类编码
     */
    public final static String TAXClASSIFICATION_CODE="1010101020000000000";
}
