package miaosu.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created by jiajun.chen on 2018/2/3.
 */

@Component
public class CustAuthenticationProvider implements AuthenticationProvider {

    private static final Logger logger = LoggerFactory.getLogger(CustAuthenticationProvider.class);

    @Autowired
    private CustUserDetailsService custUserDetailsService;


    //认证过程中主要通过调用该方法来获取对应的Authentication对象
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        try{
            UserDetails userDetials = custUserDetailsService.loadUserByUsername(username);
            Collection<? extends GrantedAuthority> authorities = userDetials.getAuthorities();

            if (userDetials.getPassword().equals(password)){
                logger.debug("用户名{} 登录成功",username);
                return new UsernamePasswordAuthenticationToken(userDetials, password, authorities);
            }else {
                logger.debug("用户名{} 和密码不匹配，登录失败",username);
                throw new BadCredentialsException("Authentication failed for " + username);
            }
        }catch (UsernameNotFoundException e){
            logger.debug("用户名{} 不存在，登录失败",username);
            throw new BadCredentialsException("Authentication failed for " + username);
        }catch (Exception e){
            throw new BadCredentialsException("Authentication failed for " + username);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
//        return false;
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
