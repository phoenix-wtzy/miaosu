package miaosu.common;


/**
 * 操作类型枚举
 * Channel
 */
public enum OpTypeEnum {

    ROOM_COUNT(1,"房量"),
    ROOM_PRICE(2,"房价"),
    ON_OFF_ROOM(3,"开关房"),
    order_state_1(7,"待确认订单"),
    order_state_2(5,"确认订单"),
    order_state_4(6,"取消订单"),
    order_state_5(8,"已确认取消订单"),
    updateOrder(9,"修改订单"),
    DEFAULT(4,"未知操作");

    private int code;
    private String name;

    OpTypeEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static OpTypeEnum fromCode(int code) {
        for (OpTypeEnum type : OpTypeEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static OpTypeEnum fromName(String name) {
        for (OpTypeEnum type : OpTypeEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
