package miaosu.common;

/**
 * Created by jiajun.chen on 2018/2/5.
 */
public enum OrderChannelEnum {

    YZG(11,"云掌柜"),
    LTJL(12,"龙腾捷旅"),
    CTRIP(1,"携程"),
    QUNAR(2,"去哪儿"),
    MEITUAN(3,"美团"),
    FEIZHU(4,"飞猪"),
    QHH(5,"去呼呼"),
    TZPT(6,"同舟平台"),
    TC(7,"同程"),
    DLT(8,"代理通"),
    MS(99,"秒宿"),
    JL(9,"捷旅"),
    UNKNOWN(-1,"UNKNOWN");

    private long code;
    private String name;

    OrderChannelEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static OrderChannelEnum fromCode(long code) {
        for (OrderChannelEnum type : OrderChannelEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return OrderChannelEnum.UNKNOWN;
    }

    public static OrderChannelEnum fromName(String name) {
        for (OrderChannelEnum type : OrderChannelEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return OrderChannelEnum.UNKNOWN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }
}
