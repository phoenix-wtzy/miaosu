package miaosu.common;


/**
 * 消息提醒方式
 */
public enum MessageNotifyEnum {
  /*
  短信提醒、语音提醒、微信公众号提醒
  */
    SMS(1,"短信提醒"),
    VOICE(2,"语音提醒"),
    WechatPublicNumber(3,"语音提醒");
    private int code;
    private String name;

    MessageNotifyEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static MessageNotifyEnum fromCode(int code) {
        for (MessageNotifyEnum type : MessageNotifyEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static MessageNotifyEnum fromName(String name) {
        for (MessageNotifyEnum type : MessageNotifyEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
