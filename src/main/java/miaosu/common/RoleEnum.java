package miaosu.common;

/**
 * Created by jiajun.chen on 2018/2/5.
 */
public enum RoleEnum {

    ROLE_SUPER(1,"ROLE_SUPER"),//超管
    ROLE_ADMIN(2,"ROLE_ADMIN"),//供应商
    ROLE_USER(3,"ROLE_USER"),//酒店
    ROLE_OTA(5,"ROLE_OTA"),//运营
    ROLE_FINANCE(6,"ROLE_FINANCE"),//财务
    ROLE_INVESTOR(7,"ROLE_INVESTOR"),//投资人
    ROLE_SALE(8,"ROLE_SALE"),//销售
    UNKNOWN(-1,"UNKNOWN");

    private long code;
    private String name;

    RoleEnum(int roleId,String roleName){
        this.code = roleId;
        this.name = roleName;
    }

    public static RoleEnum fromCode(long code) {
        for (RoleEnum type : RoleEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return RoleEnum.UNKNOWN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }
}
