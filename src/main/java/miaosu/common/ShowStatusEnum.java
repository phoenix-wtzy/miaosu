package miaosu.common;


/**
 * 发票记录的可见状态
 */
public enum ShowStatusEnum {

    ADMIN(1,"系统"),
    USER(2,"酒店");

    private int code;
    private String name;

    ShowStatusEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static ShowStatusEnum fromCode(int code) {
        for (ShowStatusEnum type : ShowStatusEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static ShowStatusEnum fromName(String name) {
        for (ShowStatusEnum type : ShowStatusEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
