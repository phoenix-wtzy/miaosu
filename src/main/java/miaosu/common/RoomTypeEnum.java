package miaosu.common;

/**
 * Created by jiajun.chen on 2018/2/5.
 */
public enum RoomTypeEnum {

    STANDARD_TWIN_ROOM(1,"标准双床房"),
    STANDARD_BIG_ROOM(2,"标准大床房"),
    BED_ROOM(3,"床位房"),
    SINGLE_ROOM(4,"单人间"),
    FAMILY_ROOM(5,"家庭房"),
    TRIPLE_ROOM(6,"三人间"),
    SUITE_ROOM(7,"套房"),
    OTHER_ROOM(8,"其他"),
    UNKNOWN(-1,"UNKNOWN");


    private long code;
    private String name;

    RoomTypeEnum(int roleId, String roleName){
        this.code = roleId;
        this.name = roleName;
    }

    public static RoomTypeEnum fromCode(long code) {
        for (RoomTypeEnum type : RoomTypeEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return RoomTypeEnum.UNKNOWN;
    }


    public static RoomTypeEnum fromName(String name) {
        for (RoomTypeEnum type : RoomTypeEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return RoomTypeEnum.UNKNOWN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }
}
