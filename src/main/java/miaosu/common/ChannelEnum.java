package miaosu.common;


/**
 * 通道信息
 * Channel
 */
public enum ChannelEnum {

    MIAOSU(0,"秒宿"),
    QHH(1,"去呼呼");

    private int code;
    private String name;

    ChannelEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static ChannelEnum fromCode(int code) {
        for (ChannelEnum type : ChannelEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static ChannelEnum fromName(String name) {
        for (ChannelEnum type : ChannelEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
