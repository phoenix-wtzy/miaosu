package miaosu.common;

/**
 * Created by jiajun.chen on 2018/2/5.
 */
public enum SellTypeEnum {

    EXCHANGE(1,"置换"),
    PROXY(2,"代销"),
    UNKNOWN(-1,"UNKNOWN");

    private long code;
    private String name;

    SellTypeEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static SellTypeEnum fromCode(long code) {
        for (SellTypeEnum type : SellTypeEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return SellTypeEnum.UNKNOWN;
    }

    public static SellTypeEnum fromName(String name) {
        for (SellTypeEnum type : SellTypeEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return SellTypeEnum.UNKNOWN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }
}
