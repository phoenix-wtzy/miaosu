package miaosu.common;

/**
 * Created by jiajun.chen on 2018/2/5.
 */
public enum OrderStatusEnum {

    WAIT_CONFIRM(1,"待确认"),
    CONFIRMED(2,"已确认"),
    CANCEL(4,"已取消"),
	CANCELYES(5,"已确认取消"),
    UNKNOWN(-1,"UNKNOWN");

    private int code;
    private String name;

    OrderStatusEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static OrderStatusEnum fromCode(long code) {
        for (OrderStatusEnum type : OrderStatusEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return OrderStatusEnum.UNKNOWN;
    }

    public static OrderStatusEnum fromName(String name) {
        for (OrderStatusEnum type : OrderStatusEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return OrderStatusEnum.UNKNOWN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
