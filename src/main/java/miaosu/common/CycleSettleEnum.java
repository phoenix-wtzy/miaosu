package miaosu.common;


/**
 * 结算枚举
 * Channel
 */
public enum CycleSettleEnum {

    DAY_SETTLE(1,"按天结算"),
    WEEK_SETTLE(2,"按周结算"),
    HALF_MOUTH_SETTLE(3,"半月结算"),
    MOUTH_SETTLE(4,"按月结算");

    private int code;
    private String name;

    CycleSettleEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static CycleSettleEnum fromCode(int code) {
        for (CycleSettleEnum type : CycleSettleEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static CycleSettleEnum fromName(String name) {
        for (CycleSettleEnum type : CycleSettleEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
