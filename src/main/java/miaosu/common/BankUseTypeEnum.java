package miaosu.common;


/**
 * 银行账户类型
 */
public enum BankUseTypeEnum {

    DAY_SETTLE(1,"个人"),
    WEEK_SETTLE(2,"公司");

    private int code;
    private String name;

    BankUseTypeEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static BankUseTypeEnum fromCode(int code) {
        for (BankUseTypeEnum type : BankUseTypeEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static BankUseTypeEnum fromName(String name) {
        for (BankUseTypeEnum type : BankUseTypeEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
