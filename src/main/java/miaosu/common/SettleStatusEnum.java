package miaosu.common;


/**
 * 结算状态枚举
 * Channel
 */
public enum SettleStatusEnum {

    NO_SETTLE(0,"未结算"),
    SETTLEING(1,"正在结算"),
    OVER_SETTLE(2,"完成结算"),
    FREEZE_SETTLE(4,"冻结结算");

    private int code;
    private String name;

    SettleStatusEnum(int code, String name){
        this.code = code;
        this.name = name;
    }

    public static SettleStatusEnum fromCode(int code) {
        for (SettleStatusEnum type : SettleStatusEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public static SettleStatusEnum fromName(String name) {
        for (SettleStatusEnum type : SettleStatusEnum.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
