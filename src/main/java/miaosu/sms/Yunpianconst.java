package miaosu.sms;

public class Yunpianconst {
    public static String HTTP_CONN_TIMEOUT = "http.conn.timeout";
    public static String HTTP_SO_TIMEOUT = "http.so.timeout";
    public static String HTTP_CHARSET = "http.charset";
    public static String HTTP_CONN_MAXPERROUTE = "http.conn.maxperroute";
    /** @deprecated */
    @Deprecated
    public static String HTTP_CONN_MAXPREROUTE = "http.conn.maxperroute";
    public static String HTTP_CONN_MAXTOTAL = "http.conn.maxtotal";
    public static String HTTP_SSL_KEYSTORE = "http.ssl.keystore";
    public static String HTTP_SSL_PASSWD = "http.ssl.passwd";
    public static String HTTP_CHARSET_DEFAULT = "utf-8";
    public static String YP_FILE = "yp.file";
    public static String YP_APIKEY = "yp.apikey";
    public static String YP_VERSION = "yp.version";
    public static String YP_USER_HOST = "yp.user.host";
    public static String YP_SIGN_HOST = "yp.sign.host";
    public static String YP_TPL_HOST = "yp.tpl.host";
    public static String YP_SMS_HOST = "yp.sms.host";
    public static String YP_VOICE_HOST = "yp.voice.host";
    public static String YP_FLOW_HOST = "yp.flow.host";
    public static String YP_CALL_HOST = "yp.call.host";
    public static String YP_SHORT_URL_HOST = "yp.short_url.host";
    public static String YP_VIDEO_SMS_HOST = "yp.vsms.host";
    public static String VERSION_V1 = "v1";
    public static String VERSION_V2 = "v2";
    public static String APIKEY = "apikey";
    public static String CODE = "code";
    public static String MSG = "msg";
    public static String DETAIL = "detail";
    public static String DATA = "data";
    public static String USER = "user";
    public static String BALANCE = "balance";
    public static String EMERGENCY_MOBILE = "emergency_mobile";
    public static String EMERGENCY_CONTACT = "emergency_contact";
    public static String ALARM_BALANCE = "alarm_balance";
    public static String IP_WHITELIST = "ip_whitelist";
    public static String EMAIL = "email";
    public static String MOBILE = "mobile";
    public static String GMT_CREATED = "gmt_created";
    public static String API_VERSION = "api_version";
    public static String SIGN = "sign";
    public static String NOTIFY = "notify";
    public static String APPLYVIP = "apply_vip";
    public static String ISONLYGLOBAL = "is_only_global";
    public static String INDUSTRYTYPE = "industry_type";
    public static String OLD_SIGN = "old_sign";
    public static String LICENSE_URL = "license_url";
    public static String TPL_ID = "tpl_id";
    public static String TPL_VALUE = "tpl_value";
    public static String TPL_CONTENT = "tpl_content";
    public static String CHECK_STATUS = "check_status";
    public static String REASON = "reason";
    public static String TEMPLATE = "template";
    public static String LAYOUT = "layout";
    public static String MATERIAL = "material";
    public static String LANG = "lang";
    public static String COUNTRY_CODE = "country_code";
    public static String NOTIFY_TYPE = "notify_type";
    public static String FROM = "from";
    public static String TO = "to";
    public static String DURATION = "duration";
    public static String AREA_CODE = "area_code";
    public static String MESSAGE_ID = "message_id";
    public static String ANONYMOUS_NUMBER = "anonymous_number";
    public static String PAGE_SIZE = "page_size";
    public static String CARRIER = "carrier";
    public static String FLOW_PACKAGE = "flow_package";
    public static String _SIGN = "_sign";
    public static String CALLBACK_URL = "callback_url";
    public static String RESULT = "result";
    public static String FLOW_STATUS = "flow_status";
    public static String DISPLAY_NUM = "display_num";
    public static String VOICE_STATUS = "voice_status";
    public static String EXTEND = "extend";
    public static String SMS_STATUS = "sms_status";
    public static String SMS_REPLY = "sms_reply";
    public static String SMS = "sms";
    public static String TOTAL = "total";
    public static String NICK = "nick";
    public static String UID = "uid";
    public static String TEXT = "text";
    public static String START_TIME = "start_time";
    public static String END_TIME = "end_time";
    public static String PAGE_NUM = "page_num";
    public static String TIME = "time";
    public static String REGISTER = "register";
    public static String MOBILE_STAT = "mobile_stat";
    public static String SHORT_URL = "short_url";
    public static String LONG_URL = "long_url";
    public static String STAT_DURATION = "stat_duration";
    public static String PROVIDER = "provider";
    public static String NAME = "name";
    public static String STAT = "stat";
    /** @deprecated */
    @Deprecated
    public static String ENCRYPT = "encrypt";
    /** @deprecated */
    @Deprecated
    public static String API_SECRET = "api_secret";
    /** @deprecated */
    @Deprecated
    public static String DEFAULT_ENCRYPT = "tea";
    public static String SN = "sn";
    public static String COUNT = "count";
    public static String FEE = "fee";
    public static String UNIT = "unit";
    public static String SID = "sid";
    public static String TOTAL_COUNT = "total_count";
    public static String TOTAL_FEE = "total_fee";
    public static String SEPERATOR_COMMA = ",";
    public static String RECORD_ID = "record_id";
}
