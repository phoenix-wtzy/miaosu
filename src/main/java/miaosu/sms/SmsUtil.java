package miaosu.sms;

import com.yunpian.sdk.YunpianClient;
import com.yunpian.sdk.model.Result;
import com.yunpian.sdk.model.SmsBatchSend;
import com.yunpian.sdk.model.SmsSingleSend;
import com.yunpian.sdk.model.Template;
import com.yunpian.sdk.util.ApiUtil;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * 发送信息工具类，传参若不带Map类型参数，则需要提前将信息填充到模板上
 * map为参数键值对，content为模板内容
 * 方法参见：ParserExpression.parseSms(smsTemplate.getContent(),map);
 */
public class SmsUtil {
    /**
     * 添加模板
     * @param tempalte
     * @return
     */
    public static Result<Template> addTemplate(String tempalte){
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(2);
        param.put(Yunpianconst.TPL_CONTENT, tempalte);
        Result<Template> r = clnt.tpl().add(param);
        return r;
    }

    /**
     * 根据id获取模板内容
     * @param id
     * @return
     */
    public static Result<List<Template>> getTemplate(String id){
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(1);
        if(StringUtils.isNotEmpty(id)){
            param.put(Yunpianconst.TPL_ID, "1");
        }
        Result<List<Template>> r = clnt.tpl().get(param);
        return r;
    }

    /**
     * 根据id删除模板
     * @param id
     * @return
     */
    public static Result<Template> deleteTemplate(String id){
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(1);
        param.put(Yunpianconst.TPL_ID, id);
        Result<Template> r = clnt.tpl().del(param);
        return r;
    }

    /**
     * 发送单个信息
     * @param telNumber
     * @param content
     * @return
     */
    public static Result<SmsSingleSend> sendMsg(String telNumber,String content){
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(2);
        param.put(Yunpianconst.MOBILE, telNumber);
       // param.put(Yunpianconst.TEXT, "【秒宿自助机】测试旅馆:尊敬的005会员 您消费了了154.15元 余额323.42元");
        param.put(Yunpianconst.TEXT, content);
        // param.put(EXTEND, "001");
        // param.put(UID, "10001");
        // param.put(CALLBACK_URL, "http://yourreceiveurl_address");
        Result<SmsSingleSend> r = clnt.sms().single_send(param);
        return r;
    }

    /**
     * 批量发送内容相同的信息
     * @param telNumber
     * @param content
     * @return
     */
    public Result<SmsBatchSend> batch_sendTest(String telNumber,String content) {
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(5);
        param.put(Yunpianconst.MOBILE, telNumber);
        param.put(Yunpianconst.TEXT, content);
        // param.put(EXTEND, "001");
        // param.put(UID, "10001");
        // param.put(CALLBACK_URL, "http://yourreceiveurl_address");
        Result<SmsBatchSend> r = clnt.sms().batch_send(param);
        return r;
    }


    /**
     * 批量发送内容相同的信息
     * @param numbers 多个手机号以，隔开
     * @param content 消息内容
     * @return
     */
    public static Result<SmsBatchSend> multi_send(String numbers,String content) {
        //动态针对多个手机号问题,一个手机就针对一条内容,两个手机就匹配两条内容.....要不然会报错,说手机个数和内容不匹配
        String[] arr = numbers.split(",");
        StringBuilder stringBuilder = new StringBuilder();
        //对context进行处理
        for(String a : arr){
            try {
              /*
              在SmsApi.java中,短信内容，多个短信内容请使用UTF-8做urlencode后，使用逗号分隔，一次不要超过1000条且短信内容条数必须与手机号个数相等
              内容示意：UrlEncode("【云片网】您的验证码是1234", "UTF-8") + "," +
              UrlEncode("【云片网】您的验证码是5678", "UTF-8")*/
                stringBuilder.append( URLEncoder.encode(content,"utf-8") + ",");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        //切割掉最后一个逗号
        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(2);
        param.put(Yunpianconst.MOBILE, numbers);
        param.put(Yunpianconst.TEXT, stringBuilder.toString());
        Result<SmsBatchSend> r = clnt.sms().multi_send(param);
        return r;
    }





    /**
     * 批量发送多个信息
     * @param number
     * @param content
     * @return
     */
    public Result<SmsBatchSend> multi_sendTest(String number,String content) {
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(5);
//        param.put(Yunpianconst.MOBILE, "18611111111,18611111112");
        param.put(Yunpianconst.MOBILE, number);
   //     param.put(Yunpianconst.TEXT, ApiUtil.urlEncodeAndLink("utf-8", ",", "【秒宿自助机】莫泰酒店:尊敬的123456会员 您消费了23元 余额111元", "【秒宿自助机】保利酒店:尊敬的789456会员 您消费了11元 余额95.523元","【秒宿自助机】速8酒店:尊敬的852会员 您消费了193元 余额1422.514元"));
        param.put(Yunpianconst.TEXT, ApiUtil.urlEncodeAndLink("utf-8", ",", content));
        Result<SmsBatchSend> r = clnt.sms().multi_send(param);
        return r;
    }

    /**
     * 更新模板
     * @param templateId
     * @param content
     * @return
     */
    public static Result<Template> updateTemplate(String templateId,String content) {
        YunpianClient clnt = getYunpianClent();
        Map<String, String> param = clnt.newParam(2);
        param.put(Yunpianconst.TPL_ID, templateId);
        param.put(Yunpianconst.TPL_CONTENT, content);
        Result<Template> r = clnt.tpl().update(param);
        return r;
    }


    public static YunpianClient getYunpianClent(){
        SmsInstance client = SmsInstance.getInstance();
        YunpianClient clnt = client.getClient();
        return clnt;
    }
}
