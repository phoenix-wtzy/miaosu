package miaosu.sms;

import com.yunpian.sdk.YunpianClient;
import miaosu.utils.LoadPropertiesUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Properties;
import java.util.Set;

public class SmsInstance {
	private static Logger logger = Logger.getLogger(SmsInstance.class);
	private volatile static SmsInstance instance;
	private YunpianClient clnt = null;
	private SmsInstance() {
		clnt = initYunpianClient();
	}

	public YunpianClient getClient() {
		return clnt;
	}

	public static SmsInstance getInstance() {
		if (instance == null) {
			synchronized (SmsInstance.class) {
				if (instance == null) {
					instance = new SmsInstance();
				}
			}
		}
		return instance;
	}

	private YunpianClient initYunpianClient() {
		try {
			System.out.println("初始化短信接口================");
			Properties props = LoadPropertiesUtil.loadProperties("yunpian_online.properties");
			Set<String> set = props.stringPropertyNames();
			System.out.println(StringUtils.join(set.toArray()));;
			String apiKey = props.getProperty("yp.apikey");
			System.out.println(apiKey);
			YunpianClient client = new YunpianClient(apiKey,
					SmsInstance.class.getResourceAsStream("/yunpian_online.properties")).init();
			if(client!=null){
				return client;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("初始化短信发送接口失败===");
		}
		return null;
	}
}
