package miaosu.voice;

/**
 * 语音通知的响应结果
 */
public class Result {

    /**
     * 成功响应码: 00000
     */
    private String respCode;

    /**
     *呼叫唯一标识Id
     */
    private String callId;

    /**
     *创建时间
     */
    private String createDate;

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
