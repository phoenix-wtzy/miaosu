package miaosu.voice;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import miaosu.utils.HttpsUtil;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 语音通知提醒
 */
@Slf4j
public class VoiceNotifyUtil {

    public static Boolean sendVoiceMsg(VoiceNotifyDTO voiceNotify){

        //Long timestamp = System.currentTimeMillis();
        Long timestamp = Long.parseLong(DateFormatUtils.format(new Date(),"yyyyMMddHHmmss"));

        //轻码云
        String voiceAllUrl = BaseUrl.VOICE_TEMPLATE_NOTIFY + "?sig=" + voiceNotify.getSign(timestamp) + "&timestamp=" + timestamp;

        //秒滴
//        String voiceAllUrl=BaseUrl.VOICE_TEMPLATE_NOTIFY;

        log.info("url地址：{}",voiceAllUrl);

        //轻码云
        Map map = new HashMap(4);
        map.put("voiceTemplate", voiceNotify.getParamsMap());

        String json = JSONObject.toJSONString(map);
        log.info("post json：{}",json);


        //秒滴
//        Map<String,String> map = new HashMap();
//        map.put("accountSid",AccountInfo.getAccountSidFormFile());
//        map.put("called",voiceNotify.getCallee());
//        map.put("templateId",""+voiceNotify.getTemplateId());
//        map.put("param",voiceNotify.getParam());
//        map.put("timestamp",""+timestamp);
//        map.put("sig",voiceNotify.getSign(timestamp));

        //轻码云
        String result  = HttpsUtil.httpPost(voiceAllUrl,"application/json","application/json;charset=utf-8", json);
        //秒滴
//        String result  = HttpsUtil.doPost(voiceAllUrl,map);
        log.info("发送语音结果:"+result);
        //String result = null;

        //轻码云
        Map<String, Object> resultMap = JsonUtils.parseJSONMap(result);
        JSONObject jsonObject = (JSONObject)resultMap.get("result");
        boolean flag = jsonObject.getString("respCode").equals("00000");

        //秒滴
//        Map<String, Object> resultMap = JsonUtils.parseJSONMap(result);
//        boolean flag = resultMap.get("respCode").equals("00000");
        return flag;
    }
}
