package miaosu.voice;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.DigestUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class AccountInfo {

    /**
     * 用户sid（注册获取）
     */
    private  String accountSid;

    /**
     * 用户token（注册获取）
     */
    private  String token ;

    public AccountInfo(String accountSid, String token) {
        this.accountSid = accountSid;
        this.token = token;
    }

    public String getAccountSid() {
        return accountSid;
    }


    public static String getAccountSidFormFile() {
        String accountSid = null;
        try {
            Properties properties = new Properties();
            ResourceLoader resourceLoader = new DefaultResourceLoader();
            Resource resource = resourceLoader.getResource("application.properties");

            properties.load(resource.getInputStream());
            accountSid = properties.getProperty("accountSid");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSign(Long timestamp){

        return  DigestUtils.md5DigestAsHex(("" + this.accountSid + this.token + timestamp).getBytes());
    }
}
