package miaosu.voice;


public class BaseUrl {

    //轻码云的
    //private final static String BASE_URL = "https://api.qingmayun.com/20141029/accounts/" + AccountInfo.getAccountSidFormFile();
    private final static String BASE_URL = "https://api.qingmayun.com/20141029/accounts/5ab602c21483408281103a10f207eaae";
    //秒滴的
//    private final static String BASE_URL = "https://openapi.miaodiyun.com/voice/voiceTemplate" ;

    /**
     * 语音验证码
     *
    protected final static String VOICE_CODE = BASE_URL + "/call/voiceCode";

    /**
     * 语音通知（固定文字）
     */
    protected final static String VOICE_NOTIFY = BASE_URL + "/calls/voiceNotify";

    /**
     *到语音通知（可变文字）
     */
    protected final static String VOICE_TEMPLATE_NOTIFY = BASE_URL + "/calls/voiceTemplateNotify";
//    protected final static String VOICE_TEMPLATE_NOTIFY = BASE_URL ;

    /**
     * 匿名通话绑定
     */
    protected final static String DOUBLE_BIND_URL = BASE_URL + "/privateCall/doubleBind";

    /**
     * 匿名通话解绑
     */
    protected final static String FREE_BIND = BASE_URL + "/privateCall/freeBind";

    /**
     * 双向外呼
     */
    protected final static String CALL_BACK = BASE_URL + "/call/callBack";

    /**
     * 下载话单
     */
    protected final static String BILL_DOWNLOAD = BASE_URL + "/record/voiceNotify/list";

    /**
     * 开发者账号信息查询
     */
    protected final static String ACCOUNT_INFO = BASE_URL + "/developer/basicInfo";

}
