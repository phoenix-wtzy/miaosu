package miaosu.voice;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.HashMap;
import java.util.Map;

/**
 * 语音通知实体类
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class VoiceNotifyDTO extends AccountInfo{
    /**
     * 被叫号码(必须)
     */
    private String callee;

    /**
     * 文字模板Id(用户中心创建后产生)
     */
    private int templateId;

    /**
     * 模板变量替换的参数(多个变量按英文逗号分开)
     */
    private String param;


    /**
     * 可选	播放次数(1-3次,默认1次)
     */
    private Integer playTimes;

    /**
     * 可选	最大通话时长(s)
     */
    private Integer allowedCallTime;

    /**
     * 可选	结束延迟时长(s)
     */
    private Integer lastDelayTime;

    /**
     * 	可选	话单推送地址(http或https 协议)
     */
    private String billUrl;

    /**
     *  可选	按键推送地址(http或https 协议)
     */
    private String dtmfUrl;

    public VoiceNotifyDTO(String accountSid,String token,String callee, int templateId, String param) {
        super(accountSid,token);
        this.callee = callee;
        this.templateId = templateId;
        this.param = param;
    }


    public String getCallee() {
        return callee;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Integer getPlayTimes() {
        return playTimes;
    }

    public void setPlayTimes(Integer playTimes) {
        this.playTimes = playTimes;
    }

    public Integer getAllowedCallTime() {
        return allowedCallTime;
    }

    public void setAllowedCallTime(Integer allowedCallTime) {
        this.allowedCallTime = allowedCallTime;
    }

    public Integer getLastDelayTime() {
        return lastDelayTime;
    }

    public void setLastDelayTime(Integer lastDelayTime) {
        this.lastDelayTime = lastDelayTime;
    }

    public String getBillUrl() {
        return billUrl;
    }

    public void setBillUrl(String billUrl) {
        this.billUrl = billUrl;
    }

    public String getDtmfUrl() {
        return dtmfUrl;
    }

    public void setDtmfUrl(String dtmfUrl) {
        this.dtmfUrl = dtmfUrl;
    }



    public Map getParamsMap() {
        Map map = new HashMap(6);
        map.put("callee", this.callee);
        map.put("templateId", this.templateId);
        map.put("param", this.param);
        return map;
    }

    public String getParamsXml() {
        StringBuilder xmlStr = new StringBuilder();
        xmlStr.append("<callee>" + this.callee + "</callee>");
        xmlStr.append("<templateId>" + this.templateId + "</templateId>");
        xmlStr.append("<param>" + this.param + "</param>");
        return xmlStr.toString();
    }

}
