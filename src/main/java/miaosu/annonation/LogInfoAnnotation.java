package miaosu.annonation;


import miaosu.common.OpTypeEnum;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * author   xieqx
 * createTime  2018/7/20
 * desc 记录日志的注解
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LogInfoAnnotation {

    //枚举日志类型
    OpTypeEnum opType() default OpTypeEnum.DEFAULT;

    //开始时间字段名称
    String startDateFieldName() default "";

    //结束时间字段名称
    String endDateFieldName() default "";

    //操作值字段
    String opValueFieldName() default "";

    //房型id字段
    String roomTypeFiledName() default "";

    //产品id
    String productFiledName() default "";

}

