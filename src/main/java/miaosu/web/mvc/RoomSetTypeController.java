package miaosu.web.mvc;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import miaosu.dao.auto.HotelRoomSetMapper;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelRoomSet;
import miaosu.dao.model.HotelRoomSetExample;
import miaosu.svc.hotel.HotelRoomSetService;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.vo.HotelCheckVO;
import miaosu.svc.vo.HotelRoomSetVO;
import miaosu.svc.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jiajun.chen on 2018/1/9.
 * 酒店设置的房型信息模块
 */
@Controller
public class RoomSetTypeController extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(RoomSetTypeController.class);

    /**
     * 酒店服务
     */
    @Autowired
    private HotelService hotelService;

    /**
     * 房间类型服务
     */
    @Autowired
    private HotelRoomSetService hotelRoomSetService;


    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;

    /**
     * 跳转到设置酒店房型列表中
     * @param model
     * @param hotelId 酒店id
     * @return
     */
    @GetMapping("/room/set/type/{hotelId}")
    public String home(Model model,@PathVariable long hotelId) {
        HotelInfo hotel = checkHotelInfoWithPermit(hotelId);

        if (null == hotel){
            model.addAttribute("error", "无操作权限");
            return "forward:/403";
        }else {
            model.addAttribute("hotelInfo", hotel);
            return "room/set/type";
        }
    }

    /**
     * 查询当前酒店下的所有房型列表
     * @param hotelId 酒店id
     * @param model
     * @return 房型列表信息
     */
    @ResponseBody
    @RequestMapping("/room/set/type/list")
    public Result list(@RequestParam(value="hotelId", required=true) long hotelId, Model model) {
        List<HotelRoomSetVO> list = Lists.newArrayList();
        HotelInfo hotel = checkHotelInfoWithPermit(hotelId); //验证权限是否可以查询该酒店
        if (null != hotel) {
            list = hotelRoomSetService.queryHotelRoomSetList(hotelId);
        }
        return new Result().success().setData(list);
    }

    /**
     * 新增一条房型信息
     * @param roomTypeName 房间类型名称
     * @param hotelId 酒店id
     * @param roomTypeId 房间类型id
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/set/type/add")
    public Result add(@RequestParam(value="roomTypeName", required=true) String roomTypeName,
                           @RequestParam(value="hotelId", required=true) long hotelId,
                           @RequestParam(value="roomTypeId", required=true) int roomTypeId,
                           Model model) {

        if (Strings.isNullOrEmpty(roomTypeName)) {
            return new Result().failed("房型名称不能为空");
        } else if (roomTypeId == 0) {
            return new Result().failed("对应房型必须选择");
        }

        logger.debug("add room type {} {}", roomTypeName, roomTypeId);
        //添加房型之前先判重
        HotelRoomSetExample example = new HotelRoomSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId).andHotelRoomTypeNameEqualTo(roomTypeName);
        List<HotelRoomSet> hotelRoomSets = hotelRoomSetMapper.selectByExample(example);
        if (hotelRoomSets.size()>0){
            return new Result().failed("房型添加重复");
        }
        hotelRoomSetService.addRoomType(hotelId,roomTypeName,roomTypeId);
//      hotelService.addHotelAndUser(hotelName,address,contact,contactMobile,accountName,accountPassword,accountNickName);

        return new Result().success();
    }


    /**
     * 更新酒店下单条房型记录
     * @param roomTypeName 房间类型名称
     * @param hotelId 酒店id
     * @param hotelRoomTypeId 房间类型id
     * @param roomTypeId 房型id
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/set/type/update")
    public Result update(@RequestParam(value="roomTypeName", required=true) String roomTypeName,
                      @RequestParam(value="hotelId", required=true) long hotelId,
                      @RequestParam(value="hotelRoomTypeId", required=true) long hotelRoomTypeId,
                      @RequestParam(value="roomTypeId", required=true) int roomTypeId,
                      Model model) {

        if (Strings.isNullOrEmpty(roomTypeName)) {
            return new Result().failed("房型名称不能为空");
        } else if (roomTypeId == 0) {
            return new Result().failed("对应房型必须选择");
        }

        logger.debug("update room type {} {}", roomTypeName, roomTypeId);
        HotelRoomSetExample example = new HotelRoomSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId).andHotelRoomTypeNameEqualTo(roomTypeName);
        List<HotelRoomSet> hotelRoomSets = hotelRoomSetMapper.selectByExample(example);
        if (hotelRoomSets.size()>0){
            return new Result().failed("房型修改重复");
        }
        hotelRoomSetService.updateRoomType(hotelId,hotelRoomTypeId,roomTypeName,roomTypeId);
//      hotelService.addHotelAndUser(hotelName,address,contact,contactMobile,accountName,accountPassword,accountNickName);

        return new Result().success();
    }


    /**
     * 设置酒店类型的有效性
     * @param hotelId 酒店id
     * @param hotelRoomTypeId 房间类型id
     * @param type 有效与否
     * @return
     */
    @ResponseBody
    @RequestMapping("room/active")
    public Result activeRoom(Long hotelId,Long hotelRoomTypeId,Integer type){
        if(hotelId==null){
            return new Result().failed("酒店id不能为空");
        }
        if(hotelRoomTypeId==null){
            return new Result().failed("房型id不能为空");
        }
        if(type==null){
            return new Result().failed("是否激活的信息为空");
        }
        boolean isActive = true;
        if(type==1){
            isActive = true;
        }
        if(type==0){
            isActive = false;
        }
        //获取审核结果
        return  new Result().success();
    }



     /**
     *查询房型的审核结果
     * @param hotelId 酒店id
     * @param hotelRoomTypeId 房型id
     * @param roomTypeName 房型名称
     * @return
     */
    @ResponseBody
    @RequestMapping("room/check")
    public Result checkRoom(Long hotelId,Long hotelRoomTypeId,String roomTypeName){
        if(hotelId==null){
            return new Result().failed("酒店id不能为空");
        }
        if(hotelRoomTypeId==null){
            return new Result().failed("房型id不能为空");
        }
        if(Strings.isNullOrEmpty(roomTypeName)){
            return new Result().failed("房型名称不能为空");
        }
        HotelCheckVO hotelCheckVO = new HotelCheckVO();
        hotelCheckVO.setRoomTypeName(roomTypeName);

        //获取审核结果
        return  new Result().success().setData(hotelCheckVO);
    }
}
