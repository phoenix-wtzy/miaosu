package miaosu.web.mvc;

import com.google.common.collect.Lists;
import miaosu.config.InvoiceConfig;
import miaosu.dao.model.*;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.order.OrderService;
import miaosu.svc.settle.HotelSettleService;
import miaosu.svc.settle.SettleBillSerrvice;
import miaosu.svc.vo.*;
import miaosu.utils.ExcelUtil;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by xieqx.chen on 2019/1/24.
 * 记账控制层
 */

@Controller
@RequestMapping("settle/bill")
public class SettleBillController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

     @Autowired
     private SettleBillSerrvice settleBillSerrvice;

    @Autowired
    private OrderService orderService;

    @Autowired
    private HotelSettleService hotelSettleService;

    @Autowired
    private MsgNotifyService msgNotifyService;

    /**
     * 查询酒店记账信息
     * @param search_hotel_id
     * @param search_date_start
     * @param search_date_end
     * @param search_settle_status
     * @param offset
     * @param limit
     * @return
     * http://localhost:8080/settle/bill/list
     * ?order=asc&offset=0&limit=10
     * &search_date_start=2019-01-24
     * &search_date_end=2019-01-25
     * &search_settle_status=-1
     * &search_hotel_id=30
     */
    @ResponseBody
    @RequestMapping("list")
    public PageResult getAll(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_date_end", required=true) String search_date_end,
            @RequestParam(value="search_settle_status", required=true) Integer search_settle_status,
            @RequestParam(value="offset", required=false,defaultValue = "1") int offset,
            @RequestParam(value="limit", required=false,defaultValue = "10") int limit) {

        PageResult pageResult = new PageResult();
        if(search_hotel_id == null){
            search_hotel_id = getCurHotel();
        }

        if(StringUtils.isBlank(search_date_start)){
            search_date_start = DateFormatUtils.format(new Date(),"yyyy-MM-dd");
        }

        if(StringUtils.isBlank(search_date_end)){
           search_date_end = compluteDateEnd(search_date_start,search_hotel_id);
        }

        if(search_settle_status == -1){
            search_settle_status = null;
        }
        pageResult = settleBillSerrvice.queryBillListByPage(search_hotel_id,search_date_start,search_date_end,search_settle_status,isAdmin(),offset,limit);
        logger.info("结算账单列表信息 : {}",JsonUtils.objectToJson(pageResult));

        return  pageResult;
    }


    /**
     * 根据账单获取订单明细
     * @param billId
     * @return
     */
    @ResponseBody
    @RequestMapping("settleBillDetail")
    public List<OrderDetail> settleBillDetail(
            @RequestParam(value="billId", required=true) Long billId) {

        if(billId == null){
            return null;
        }
        //查询账单关联信息查询账单信息
        List<Long> orderIds = settleBillSerrvice.queryOrderIdsByBill(billId);
        //查询账单
        /**
         * 杨小全，13720003693
         豪华大床房（含双早）（双早）
         1/13入住，住1晚，1间
         */

        List<OrderDetail> orderDetails = Lists.newArrayList();
        List<HotelOrderVO> hotelOrderVOS = orderService.queryBatchOrderByIds(orderIds);

        for(HotelOrderVO hotelOrderVO:hotelOrderVOS){
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setBookTime(hotelOrderVO.getBookTime());
            orderDetail.setPrice(hotelOrderVO.getPrice());
            orderDetail.setCostType("房费");
            long night = getDays(hotelOrderVO.getCheckInTime(),hotelOrderVO.getCheckOutTime());

            StringBuilder content = new StringBuilder();
            content.append(hotelOrderVO.getBookUser()).append(",").append(hotelOrderVO.getBookMobile())
                    .append(hotelOrderVO.getHotelRoomTypeName())
                    .append(DateFormatUtils.format(hotelOrderVO.getCheckInTime(),"MM/dd")).append("入住")
                    .append("住"+night+"晚 ").append(hotelOrderVO.getRoomCount()+"间");

            String orderContent = content.toString();
            logger.info("订单信息：{}",orderContent);
            orderDetail.setOrderContent(orderContent);
            orderDetails.add(orderDetail);
        }

        logger.info("订单列表 : {}",JsonUtils.objectToJson(orderDetails));
        return orderDetails;
    }


    /**
     * 根据账单获取订单明细汇总
     * @param billId 账单id
     * @return 结算订单详情信息 列表分页类型
     */
    @ResponseBody
    @RequestMapping("settleBillDetailSum")
    public List<SettleDetailVO>  settleBillDetailSum(
            @RequestParam(value="billId", required=true) Long billId) {

        if(billId == null){
            return null;
        }

        List<SettleDetailVO> settleDetailVOS = Lists.newArrayList();
        HotelSettleBill billInfo = settleBillSerrvice.queryBillListById(billId);

        String settlemoney = billInfo.getSettleMoney().setScale(2, BigDecimal.ROUND_HALF_DOWN).toString();
        //查询账单关联信息查询账单信息
        List<Long> orderIds = settleBillSerrvice.queryOrderIdsByBill(billId);

        SettleDetailVO settleDetailVO = new SettleDetailVO();
        settleDetailVO.setOrderNum(orderIds.size());
        settleDetailVO.setRealMoney(settlemoney);
        settleDetailVO.setShouldMoney(settlemoney);
        settleDetailVO.setAdjustMoney("0");

        settleDetailVOS.add(settleDetailVO);
        logger.info("结算订单细节 : {}",JsonUtils.objectToJson(settleDetailVOS));
        return settleDetailVOS;
    }


    /**
     * 导出记账信息
     * @param search_hotel_id
     * @param search_date_start
     * @param search_date_end
     * @param search_settle_status
     * @return
     */
    @RequestMapping("exportExcel")
    public void exportExcel(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_date_end", required=true) String search_date_end,
            @RequestParam(value="search_settle_status", required=true) Integer search_settle_status,
            HttpServletResponse response) {

        boolean isAdmin = isAdmin();

        if(search_hotel_id == null){
            return ;
        }

        if(StringUtils.isBlank(search_date_start)){
            search_date_start = DateFormatUtils.format(new Date(),"yyyy-MM-dd");
        }

        if(StringUtils.isBlank(search_date_end)){
            search_date_end = compluteDateEnd(search_date_start,search_hotel_id);
        }

        if(search_settle_status == -1){
            search_settle_status = null;
        }
        HotelInfo hotelInfo = checkHotelInfoWithPermit(search_hotel_id);
        List<Map<String,Object>> hotelSettleBillVOS = settleBillSerrvice.queryBillListForExcel(search_hotel_id, search_date_start, search_date_end, search_settle_status,isAdmin);

        String exportData = null;
        int defaultlen = (int) (35.7 * 140);
        int otherlen = (int) (35.7 * 130);
        int numlen = (int) (35.7 * 100);
        Integer[] columnsLen = null;

        if (isAdmin) {
            //管理员
            exportData = "[{'colkey':'createTime','name':'记账日期'},{'colkey':'settleCycleName','name':'结算周期'},"
                    + "{'colkey':'settleBankType','name':'结算银行'},{'colkey':'settleBankAccount','name':'结算账户'}"
                    + "{'colkey':'settleMoney','name':'结算金额'},{'colkey':'settleStatusName','name':' 结算状态'},"
                    + "{'colkey':'settleTime','name':'结算时间'},{'colkey':'remark','name':' 备注'},"
                    + "]";
            columnsLen = new Integer[]{defaultlen, defaultlen,
                    defaultlen, defaultlen,
                    numlen, numlen,
                    defaultlen, defaultlen,
            };
        } else {
            //普通用户
            exportData = "[{'colkey':'createTime','name':'记账日期'},{'colkey':'settleCycleName','name':'结算周期'},"
                    + "{'colkey':'settleBankType','name':'结算银行'},{'colkey':'settleBankAccount','name':'结算账户'}"
                    + "{'colkey':'settleMoney','name':'结算金额'},{'colkey':'settleStatusName','name':' 结算状态'},"
                    + "{'colkey':'settleTime','name':'结算时间'},{'colkey':'remark','name':' 备注'},"
                    + "]";
            columnsLen = new Integer[]{defaultlen, defaultlen,
                    defaultlen, defaultlen,
                    numlen, numlen,
                    defaultlen, defaultlen,
            };
        }
            String fileName = hotelInfo.getHotelName()+search_date_start+"-"+search_date_end+"账单信息";
            List<Map<String, Object>> parseJSONList = JsonUtils.parseJSONList(exportData);

            ExcelUtil.exportToExcel(response, parseJSONList, hotelSettleBillVOS, fileName, columnsLen);
    }

    /**
     * 更新结算订单状态
     * @param billId  结算订单的主键
     * @param status 订单状态 (0,"未结算")(1,"正在结算")(2,"完成结算") (4,"冻结结算")
     * @return
     */
    @ResponseBody
    @RequestMapping("updateStatus")
    public Result updateStatus( @RequestParam(value="billId", required=true) Long billId,@RequestParam(value="status", required=true) Integer status) {
        Long hotelId = getCurHotel();
        HotelInfo hotelInfo = checkHotelInfoWithPermit(hotelId);
        if(billId == null && status == null){
             return new Result().failed("缺少必要的参数");
        }

        boolean flag =  settleBillSerrvice.updateSettleStatus(billId,status);

        if(flag){
            //结算通知
            msgNotifyService.sendSettleMsg(hotelInfo.getHotelName(),hotelId,billId);
            return new Result().success();
        }
        return new Result().failed("更新结算状态失败");

    }


    private String compluteDateEnd(String search_date_start, Long hotelId) {
        Integer cycleType = hotelSettleService.queryCycleSetting(hotelId);

        try {
            Date date_start =  DateUtils.parseDate(search_date_start,"yyyy-MM-dd");
            Date prevDate = null;
            if(cycleType==1){
                //天
                prevDate = DateUtils.addDays(date_start, 0);
            }
            if(cycleType==2){
                //周
                prevDate = DateUtils.addDays(date_start, 6);
            }
            if(cycleType==3){
                //半月
                prevDate = DateUtils.addDays(date_start, 14);
            }
            if(cycleType==4){
                //月
                prevDate = DateUtils.addDays(date_start, 29);
            }
            return DateFormatUtils.format(prevDate,"yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return search_date_start;
    }


    private long getDays(Date startDate, Date endDate) {
        if (startDate != null && endDate != null) {
            SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            Date mydate = null;

            try {
                mydate = myFormatter.parse(myFormatter.format(startDate));
                date = myFormatter.parse(myFormatter.format(endDate));
            } catch (Exception var7) {
            }

            long day = (date.getTime() - mydate.getTime()) / 86400000L;
            return day;
        } else {
            return 0L;
        }
    }
}
