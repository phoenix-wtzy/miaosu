package miaosu.web.mvc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import miaosu.common.OrderStatusEnum;
import miaosu.dao.auto.HotelInfoMapper;
import miaosu.dao.model.*;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.log.OpLogService;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.order.OrderMsgService;
import miaosu.svc.order.OrderOfCaigouoneService;
import miaosu.svc.order.OrderService;
import miaosu.svc.vo.PageResult;
import miaosu.svc.vo.QueryHotelOrderVO;
import miaosu.svc.vo.Response;
import miaosu.svc.vo.Result;
import miaosu.svc.vo.qhh.OplogVO;
import miaosu.utils.ExcelUtil;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jiajun.chen on 2018/1/9.
 */
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private OrderMsgService orderMsgService;
    //日志服务类
    @Autowired
    private OpLogService opLogService;

    @Autowired
    private MsgNotifyService msgNotifyService;
    @Autowired
    private HotelInfoMapper hotelInfoMapper;
	@Autowired
	private OrderOfCaigouoneService orderOfCaigouoneService;

    /**
     * 进入到订单页面的首页面
     * @param model
     * @return
     */
    @GetMapping("")
    public String order(Model model) {
        boolean isAdmin = isAdmin();
        if (isAdmin){
            return "redirect:/admin/order";
        }

        Date now = new Date();
        Date preday7 = DateUtils.addDays(now, -7);
        String date_end = DateFormatUtils.format(now,"yyyy-MM-dd");
        String date_start = DateFormatUtils.format(preday7,"yyyy-MM-dd");
        model.addAttribute("search_date", ImmutableMap.of("date_start",date_start,"date_end",date_end));

        Long currentUserId = getCurrentUserId();
        model.addAttribute("channel",currentUserId);

        return "order";
    }


    private Long getFirstHotelId(Long currentUserId){
        List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId);
        if(hotelRefUsers.size()>0){
            return hotelRefUsers.get(0).getHotelId();
        }
        return null;
    }

    /**
     * 订单状态的更新
     * @param orderId
     * @param status
     * @return
     */
    @ResponseBody
    @RequestMapping("status/update")
    public Result hotelList(@RequestParam(value="orderId", required=false) Long orderId,
                            @RequestParam(value="status", required=false,defaultValue = "1") int status){

        Long currentUserId = getCurrentUserId();

        HotelOrder hotelOrder = orderService.queryOrder(orderId);
        if(null == hotelOrder){
            return new Result().failed("不存在该订单！");
        }

        if(hotelOrder.getState() == OrderStatusEnum.CANCELYES.getCode() ||
                hotelOrder.getState() == OrderStatusEnum.CONFIRMED.getCode()){ //已确认取消订单不能修改状态
            return new Result().failed("已确认取消订单不能修改！");
        }

        /**
        if(hotelOrder.getState()== OrderStatusEnum.CONFIRMED.getCode()){ //已确认订单不能取消
            return new Result().failed("已确认订单不能修改！");
        }
         **/

        if(hotelOrder.getState()== OrderStatusEnum.CANCEL.getCode()){ //已取消订单不能确认
            if(status == OrderStatusEnum.CONFIRMED.getCode()){
                //return new Result().failed("已取消订单不能确认！");
                //取消订单点知道了,改成已确认取消
                status=5;
            }
        }

		//拼接参数
		Map<String,String> params = new HashMap<String, String>(8);
		//根据当前用户id查询用户名
		String caozuoren= userService.queryUser_nick(currentUserId);
		//获取订单的预定人
		String bookUser1 = hotelOrder.getBookUser();
		params.put("caozuoren",caozuoren);
		params.put("bookUser1",bookUser1);
		//根据当前用户id到hotel_info表中查询OTA运营电话
		HotelInfo hotelInfo = hotelInfoMapper.selectByPrimaryKey(hotelOrder.getHotelId());
		String ota_phone1 = hotelInfo.getOta_phone();
		//String ota_phone2="15090456687";//李莉的电话,设死了
		params.put("ota_phone1",ota_phone1);
		//params.put("ota_phone2",ota_phone2);


        List<HotelInfo> hotelInfos = hotelService.queryHotelList(currentUserId, null, hotelOrder.getHotelId());
        if(hotelInfos.size()>0){
            int count = orderService.updateStatus(orderId, status);
            //更新临时表中的订单状态:已确认;已取消
            orderService.updateGetOrderStatus(orderId, status);
            //再次查询订单状态
            HotelOrder hotelOrder1 = orderService.queryOrder(orderId);
            if (hotelOrder1.getState()==2){
                //当三级酒店点击知道了,短信通知运营人员
                msgNotifyService.sendMsgOfZhiDaoLe(params);
            }
            if (hotelOrder1.getState()==5){
                //当超级管理员点击取消,短信通知
                msgNotifyService.sendMsgOfQuXiao(params);
            }
            Long hotelId = hotelOrder.getHotelId();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date checkInTime = hotelOrder.getCheckInTime();
            String startDate = simpleDateFormat.format(checkInTime);
            Date checkOutTime = hotelOrder.getCheckOutTime();
            String endDate = simpleDateFormat.format(checkOutTime);
            String bookUser = hotelOrder.getBookUser();
            Long roomTypeId = hotelOrder.getHotelRoomTypeId();
            Long[]roomTypeIds={roomTypeId};

            OplogVO oplogVO = new OplogVO();
            oplogVO.setUserId(currentUserId);
            oplogVO.setHotelId(hotelId);
            if (status==2){
                oplogVO.setOpType(5);
            }
            if (status==4){
                oplogVO.setOpType(6);
            }
            if (status==5){
                oplogVO.setOpType(8);
            }
            oplogVO.setStartDate(startDate);
            oplogVO.setEndDate(startDate);
            //操作值字段:房量是多少,房价是多少,开关房是开还是关,这里针对订单用不到,设置为0
            String opValue= "订单号为"+orderId+
                    "★"+bookUser;
            oplogVO.setOpValue(opValue);
            Date createDate = new Date();
            oplogVO.setCreateTime(createDate);
            oplogVO.setHotelRoomTypeIds(roomTypeIds);
            opLogService.saveOpLogs(oplogVO);

            if(status == OrderStatusEnum.CANCEL.getCode()){
                orderMsgService.sendMsgByOrderId(orderId);
            }
            return new Result().success().setData(count);
        }

        return new Result().failed("用户无权限！");
    }

    /**
     * 查询订单列表信息分页
     * @param search_hotel_id 查询的酒店id
     * @param search_bookUser 预定人
     * @param search_orderNo 渠道单号
     * @param search_hotel_confirm_number 酒店确认号
     * @param search_date_start 开始日期
     * @param search_date_end 结束日志
     * @param search_date_type 日期类型
     * @param search_order_status 订单状态
     * @param offset 开始页数
     * @param limit 结束页数
     * @return
     */
    @ResponseBody
    @RequestMapping("list")
    public PageResult getAll(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_bookUser", required=false) String search_bookUser,
            @RequestParam(value="search_orderNo", required=false) String search_orderNo,
            @RequestParam(value="search_hotel_confirm_number", required=false) String search_hotel_confirm_number,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_date_end", required=true) String search_date_end,
            @RequestParam(value="search_date_type", required=true) int search_date_type,
            @RequestParam(value="search_order_status", required=true) int search_order_status,
			@RequestParam(value="operation_number") int operation_number,//tab标签切换的数字类型,1 全部订单; 3 待操作
            @RequestParam(value="offset", required=false,defaultValue = "1") int offset,
            @RequestParam(value="limit", required=false,defaultValue = "10") int limit) {

        Long currentUserId = getCurrentUserId();
        logger.debug("userId = {}",currentUserId);
        boolean isAdmin = isAdmin();

        HotelOrderExample order = new HotelOrderExample();
        HotelOrderExample.Criteria and = order.createCriteria();
        PageResult orderList = new PageResult();



        Long hotelId = search_hotel_id;
        if(null != search_hotel_id){
            //全部酒店
            if (search_hotel_id==0){
                //条件
                if(StringUtils.isNotEmpty(search_orderNo) ||
                        StringUtils.isNotEmpty(search_bookUser) ||
                        StringUtils.isNotEmpty(search_hotel_confirm_number)){
                    if(NumberUtils.isNumber(search_orderNo)){
                        and.andOrderNoLike("%"+search_orderNo+"%");
                    }

                    if(StringUtils.isNotEmpty(search_bookUser)){
                        and.andBookUserLike("%"+search_bookUser+"%");
                    }

                    if(StringUtils.isNotEmpty(search_hotel_confirm_number)){
                        and.andhotel_confirm_numberLike("%"+search_hotel_confirm_number+"%");
                    }
                }
                if(StringUtils.isNotEmpty(search_date_start) && StringUtils.isNotEmpty(search_date_end)){
                    Date date1 = null;
                    try {
                        date1 = DateUtils.parseDate(search_date_start,"yyyy-MM-dd");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Date date2 = null;
                    try {
                        date2 = DateUtils.parseDate(search_date_end,"yyyy-MM-dd");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    switch (search_date_type){
                        //预定日期
                        case 1:
                            date2 = DateUtils.addDays(date2,1);
                            and.andBookTimeBetween(date1,date2);
                            break;
                        //入住日期
                        case 2:
                            and.andCheckInTimeBetween(date1,date2);
                            break;
                        //离店日期
                        case 3:
                            and.andCheckOutTimeBetween(date1,date2);
                            break;
                        default:
                            break;
                    }
                }
                if (operation_number==3 && search_order_status==0){
                    List<Integer> stateList=new ArrayList<>();
                    stateList.add(1);
                    stateList.add(4);
                    and.andStateIn(stateList);
                }
                if (operation_number==2 && search_order_status==0){
                    List<Integer> stateList=new ArrayList<>();
                    stateList.add(1);
                    stateList.add(2);
                    and.andStateIn(stateList);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String dateStr = sdf.format(new Date());
                    Date date = null;
                    try {
                        date = sdf.parse(dateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    and.andCheckInTimeEqualTo(date);
                }
                if(search_order_status != 0){
                    and.andStateEqualTo(search_order_status);
                }

                //根据用户id查询名下的酒店id,很多个
                List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId);
                List<Long> hotelIdList= Lists.newArrayList();
                //封装order的查询条件到集合中,并且向下传递
                List<HotelOrderExample> orderExampleList=new ArrayList();
                if (!CollectionUtils.isEmpty(hotelRefUsers)){
                    for (HotelRefUser hotelRefUser : hotelRefUsers) {
                        hotelId = hotelRefUser.getHotelId();
                        hotelIdList.add(hotelId);
                    }
                    and.andHotelIdIn(hotelIdList);
                    order.setOrderByClause("order_id desc");
                    orderExampleList.add(order);
                }
                //查询当前用户名下所有酒店对应的订单
                orderList = orderService.queryAll(orderExampleList,offset,limit,isAdmin);
                return orderList;
            }
            //权限过滤，看该用户是否有该酒店的权限
            List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId, search_hotel_id);
            if(hotelRefUsers.size()==0){
                return orderList;
            }
        }
        else {
            Long firstHotelId = getCurHotel();
            if (null == firstHotelId){
                firstHotelId = getFirstHotelId(currentUserId);
            }
            if(null == firstHotelId){
                logger.warn("没有关联酒店 for user {}",currentUserId);
                return orderList;
            }else {
                hotelId = firstHotelId;
            }
        }
        and.andHotelIdEqualTo(hotelId);

        if(StringUtils.isNotEmpty(search_orderNo) ||
                StringUtils.isNotEmpty(search_bookUser) ||
                StringUtils.isNotEmpty(search_hotel_confirm_number)){
            if(NumberUtils.isNumber(search_orderNo)){
                and.andOrderNoLike("%"+search_orderNo+"%");
            }

            if(StringUtils.isNotEmpty(search_bookUser)){
                and.andBookUserLike("%"+search_bookUser+"%");
            }

            if(StringUtils.isNotEmpty(search_hotel_confirm_number)){
                and.andhotel_confirm_numberLike("%"+search_hotel_confirm_number+"%");
            }

            orderList = orderService.queryOrderListBypage(order,offset,limit,isAdmin);
//            return orderList;
        }

        try {

            if(StringUtils.isNotEmpty(search_date_start) && StringUtils.isNotEmpty(search_date_end)){
                Date date1 = DateUtils.parseDate(search_date_start,"yyyy-MM-dd");
                Date date2 = DateUtils.parseDate(search_date_end,"yyyy-MM-dd");
                //date2 = DateUtils.addDays(date2,1);

                switch (search_date_type){
                    //预定日期
                    case 1:
                        date2 = DateUtils.addDays(date2,1);
                        and.andBookTimeBetween(date1,date2);
                        break;
                    //入住日期
                    case 2:
                        and.andCheckInTimeBetween(date1,date2);
                        break;
                    //离店日期
                    case 3:
                        and.andCheckOutTimeBetween(date1,date2);
                        break;
                        default:
                            break;
                }
            }

		/**
		 * tab标签切换的数字类型,1 全部订单; 3 待操作
		 * operation_number=3--待操作页面
		 * operation_number=1--全部订单页面
		 *
		 * 如果operation_number=3并且没有搜索条件[包括订单状态,此时为0(不选择订单状态的意思)],那么就搜索该酒店下的所有待确认和已取消订单(订单状态为1和4的)
		 * 如果operation_number=3并且有搜索条件[订单号,订单状态,日期等],就不走下面的if判断,而是根据条件去查询
		 *
		 * 如果operation_number=1,一切正常,按照之前的逻辑即可
		 */
			if (operation_number==3 && search_order_status==0){
				List<Integer> stateList=new ArrayList<>();
				stateList.add(1);
				stateList.add(4);
				and.andStateIn(stateList);
			}

            if (operation_number==2 && search_order_status==0){
                List<Integer> stateList=new ArrayList<>();
                stateList.add(1);
                stateList.add(2);
                and.andStateIn(stateList);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = sdf.format(new Date());
                Date date = sdf.parse(dateStr);
                and.andCheckInTimeEqualTo(date);
            }
            if(search_order_status != 0){
                and.andStateEqualTo(search_order_status);
            }


            order.setOrderByClause("order_id desc");
            orderList = orderService.queryOrderListBypage(order,offset,limit,isAdmin);
            logger.info("orderList {}",JsonUtils.objectToJson(orderList));
            return orderList;
            }
        catch (ParseException e){
            logger.error("查询订单失败",e);
            return orderList;
        }
    }

    /**
     * 用户进入订单页面 查询当前用户下的酒店中的待确认订单，
     * @param q
     * @return
     * @throws Exception
     */
    @MessageMapping("/order/wait")
    public PageResult getOrderWaitConfirmList(QueryHotelOrderVO q) throws Exception {
        Long currentUserId = getCurrentUserId();
        long hotelId = q.getHotelId();

//        HotelOrderExample order = new HotelOrderExample();
//        HotelOrderExample.Criteria and = order.createCriteria();
//        and.andStateEqualTo(OrderStatusEnum.WAIT_CONFIRM.getCode());
//        and.andHotelIdEqualTo(hotelId);
//
//        PageResult orderList = orderService.queryOrderListBypage(order,0,50,false);

        PageResult orderList = orderService.queryOrderListBypage(hotelId,0,10,false);

        //新增订单(state=1或者4)的弹框提醒
        orderMsgService.sendMsg(currentUserId,orderList);

        return orderList;
    }


    /**
     * 导出订单信息到excel表格中（根据查询条件进行导出）
     * @param search_hotel_id 酒店id
     * @param search_keywords 查询关键子
     * @param search_date_start 查询的开始日期
     * @param search_date_end 查询的结束日期
     * @param search_date_type  预定日期，入住日期，离店日期
     * @param search_order_status 订单状态
     */
    @RequestMapping("orderExportExcle")
    public void orderExportExcle(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_keywords", required=false) String search_keywords,
            @RequestParam(value="search_date_start", required=false) String search_date_start,
            @RequestParam(value="search_date_end", required=false) String search_date_end,
            @RequestParam(value="search_date_type", required=false) Integer search_date_type,
            @RequestParam(value="search_order_status", required=false) Integer search_order_status,
            @RequestParam(value="type", required=false) Integer type,
            HttpServletResponse response) {


        Long currentUserId = getCurrentUserId();
        logger.debug("userId = {}",currentUserId);
        boolean isAdmin = isAdmin();

        HotelOrderExample order = new HotelOrderExample();
        List<Map<String,Object>> orderList = new ArrayList<Map<String, Object>>();

        HotelOrderExample.Criteria and = order.createCriteria();
        Long hotelId = search_hotel_id;
        if(null != search_hotel_id){
            //权限过滤，看该用户是否有该酒店的权限
            List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId, search_hotel_id);
            if(hotelRefUsers.size()==0){
                throw new RuntimeException("该用户没有导出的权限");
            }
        }
        else {
            Long firstHotelId = getCurHotel();
            if (null == firstHotelId){
                firstHotelId = getFirstHotelId(currentUserId);
            }
            if(null == firstHotelId){
                logger.warn("没有关联酒店 for user {}",currentUserId);
            }else {
                hotelId = firstHotelId;
            }
        }
        and.andHotelIdEqualTo(hotelId);

        if(StringUtils.isNotEmpty(search_keywords)){
            if(NumberUtils.isNumber(search_keywords)){
                if(search_keywords.startsWith("1")&& search_keywords.length()==11 ){
                    and.andBookMobileEqualTo(search_keywords);
                }else {
                    and.andOrderIdEqualTo(Long.parseLong(search_keywords));
                }
            }else {
                and.andBookUserEqualTo(search_keywords);
            }
            orderList = orderService.queryOrderListToExcel(order);
        }
        try {

            if(StringUtils.isNotEmpty(search_date_start) && StringUtils.isNotEmpty(search_date_end)){
                Date endDate = DateUtils.parseDate(search_date_end,"yyyy-MM-dd");


                Date startDate = DateUtils.parseDate(search_date_start,"yyyy-MM-dd");

                switch (search_date_type){
                    //离店日期
                    case 3:
                        and.andCheckOutTimeBetween(startDate,endDate);
                        break;
                    //入住日期
                    case 2:
                        and.andCheckInTimeBetween(startDate,endDate);
                        break;
                    //预定日期
                    case 1:
                        endDate = DateUtils.addDays(endDate,1);
                        and.andBookTimeBetween(startDate,endDate);
                        break;
                    default:
                        break;
                }
            }
            if(search_order_status != 0){
                and.andStateEqualTo(search_order_status);
            }

            order.setOrderByClause("order_id desc");
            orderList = orderService.queryOrderListToExcel(order);
        }
        catch (ParseException e){
            logger.error("查询订单失败",e);
        }

        if(orderList == null || orderList.size()==0){
            return ;
        }

        String exportData = null;
        int defaultlen = (int)(35.7 * 140);
        int otherlen =  (int)(35.7 * 130);
        int numlen =  (int)(35.7 * 100);
        Integer[] columnsLen = null;


        if(isAdmin){
            //管理员
            exportData  = "[{'colkey':'hotelName','name':'酒店名称'},{'colkey':'orderId','name':'订单号'},"
                    + "{'colkey':'orderChannelName','name':'渠道名称'},{'colkey':'orderNo','name':'渠道单号'}"
                    +"{'colkey':'bookTime','name':'预定时间'},{'colkey':'bookUser','name':' 预定人'},"
                    +"{'colkey':'bookMobile','name':'联系电话'},{'colkey':'hotelRoomTypeName','name':' 房型'},"
                    +"{'colkey':'roomCount','name':'房间数'},{'colkey':'checkInOutTimeStr','name':'入离时间'},"
                    +"{'colkey':'price','name':'订单金额'},{'colkey':'bookRemark','name':'备注'},"
                    +"{'colkey':'orderStateStr','name':'订单状态'}"
                    + "]";
            columnsLen = new Integer[]{defaultlen,defaultlen,
                    defaultlen,defaultlen,
                    defaultlen,otherlen,
                    defaultlen,otherlen,
                    numlen,otherlen,
                    numlen, otherlen,
                    numlen};
        }else{
            if(type != null){
                exportData  = "[{'colkey':'hotelName','name':'酒店名称'},{'colkey':'orderId','name':'订单号'},"
                        +"{'colkey':'bookTime','name':'预定时间'},{'colkey':'bookUser','name':' 预定人'},"
                        +"{'colkey':'bookMobile','name':'联系电话'},{'colkey':'hotelRoomTypeName','name':' 房型'},"
                        +"{'colkey':'roomCount','name':'房间数'},{'colkey':'checkInOutTimeStr','name':'入离时间'},"
                        +"{'colkey':'price','name':'订单金额'},{'colkey':'bookRemark','name':'备注'},"
                        +"{'colkey':'orderStateStr','name':'订单状态'},{'colkey':'settleStatusStr','name':'结算状态'}"
                        + "]";
                columnsLen = new Integer[]{defaultlen,defaultlen,
                        defaultlen,otherlen,
                        defaultlen,otherlen,
                        numlen,otherlen,
                        numlen,defaultlen,
                        otherlen,otherlen};

            }else{
                //普通用户
                exportData  = "[{'colkey':'hotelName','name':'酒店名称'},{'colkey':'orderId','name':'订单号'},"
                        +"{'colkey':'bookTime','name':'预定时间'},{'colkey':'bookUser','name':' 预定人'},"
                        +"{'colkey':'bookMobile','name':'联系电话'},{'colkey':'hotelRoomTypeName','name':' 房型'},"
                        +"{'colkey':'roomCount','name':'房间数'},{'colkey':'checkInOutTimeStr','name':'入离时间'},"
                        +"{'colkey':'bookRemark','name':'备注'},{'colkey':'orderStateStr','name':'订单状态'}"
                        + "]";
                columnsLen = new Integer[]{defaultlen,defaultlen,
                        defaultlen,otherlen,
                        defaultlen,otherlen,
                        numlen,otherlen,
                        defaultlen,numlen};
            }

        }

        String fileName = (String) orderList.get(0).get("hotelName")+"订单信息";
        List<Map<String, Object>> parseJSONList = JsonUtils.parseJSONList(exportData);

        ExcelUtil.exportToExcel(response, parseJSONList, orderList, fileName,columnsLen);
    }



    /**
     * 导出订单结算信息到excel表格中（根据查询条件进行导出）
     * @param search_hotel_id 酒店id
     * @param search_date_start 查询的开始日期
     * @param search_date_end 查询的结束日期
     * @param search_date_type  预定日期，入住日期，离店日期
     * @param search_settle_status 订单状态
     */
    @RequestMapping("orderExportSettleExcle")
    public void orderExportSettleExcle(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=false) String search_date_start,
            @RequestParam(value="search_date_end", required=false) String search_date_end,
            @RequestParam(value="search_date_type", required=false) Integer search_date_type,
            @RequestParam(value="search_settle_status", required=false) Integer search_settle_status,
            HttpServletResponse response) {

        if(search_hotel_id == null){
            search_hotel_id = getCurHotel();
        }

        HotelOrderExample order = new HotelOrderExample();
        List<Map<String,Object>> orderList = new ArrayList<Map<String, Object>>();

        HotelOrderExample.Criteria and = order.createCriteria();
        Long hotelId = search_hotel_id;
        and.andHotelIdEqualTo(hotelId);

        try {
            if(StringUtils.isNotEmpty(search_date_start) && StringUtils.isNotEmpty(search_date_end)){
                Date endDate = DateUtils.parseDate(search_date_end,"yyyy-MM-dd");

                Date startDate = DateUtils.parseDate(search_date_start,"yyyy-MM-dd");
                switch (search_date_type){
                    //离店日期
                    case 3:
                        and.andCheckOutTimeBetween(startDate,endDate);
                        break;
                    //预定日期
                    case 1:
                        endDate = DateUtils.addDays(endDate,1);
                        and.andBookTimeBetween(startDate,endDate);
                        break;
                    //入住日期
                    case 2:
                        and.andCheckInTimeBetween(startDate,endDate);
                        break;
                    default:
                        break;
                }
            }

            if(search_settle_status == null){
                //查询已经进行并没有结算的订单
                and.andSettleStatusEqualTo(0);
            }else if(search_settle_status !=-1){
                and.andSettleStatusEqualTo(search_settle_status);
            }

            order.setOrderByClause("order_id desc");
            orderList = orderService.queryOrderListToExcel(order);
        }
        catch (ParseException e){
            logger.error("查询订单失败",e);
        }

        if(orderList == null || orderList.size()==0){
            return ;
        }

        String exportData = null;
        int defaultlen = (int)(35.7 * 140);
        int otherlen =  (int)(35.7 * 130);
        int numlen =  (int)(35.7 * 100);
        Integer[] columnsLen = null;

        exportData  = "[{'colkey':'hotelName','name':'酒店名称'},{'colkey':'orderId','name':'订单号'},"
                +"{'colkey':'bookTime','name':'预定时间'},{'colkey':'bookUser','name':' 预定人'},"
                +"{'colkey':'bookMobile','name':'联系电话'},{'colkey':'hotelRoomTypeName','name':' 房型'},"
                +"{'colkey':'roomCount','name':'房间数'},{'colkey':'checkInOutTimeStr','name':'入离时间'},"
                +"{'colkey':'price','name':'订单金额'},{'colkey':'bookRemark','name':'备注'},"
                +"{'colkey':'orderStateStr','name':'订单状态'},{'colkey':'settleStatusStr','name':'结算状态'}"
                + "]";
        columnsLen = new Integer[]{defaultlen,defaultlen,
                defaultlen,otherlen,
                defaultlen,otherlen,
                numlen,otherlen,
                numlen,defaultlen,
                otherlen,otherlen};


        String fileName = (String) orderList.get(0).get("hotelName")+"订单信息";
        List<Map<String, Object>> parseJSONList = JsonUtils.parseJSONList(exportData);

        ExcelUtil.exportToExcel(response, parseJSONList, orderList, fileName,columnsLen);
    }


    //添加/修改酒店订单确认号
    @ResponseBody
    @RequestMapping("ADD_hotel_confirm_number")
    public Result ADD_hotel_confirm_number(
            @RequestParam(value="order_id") Long order_id,
            @RequestParam(value="hotel_confirm_number") String hotel_confirm_number) {
        if (order_id==null){
            System.out.println("订单号(订单id)为空");
        }
        //根据订单id添加/修改酒店订单确认号(在hotel_order中)
        Long row=orderService.updateHotel_confirm_number(order_id,hotel_confirm_number);
        //根据订单id添加/修改酒店订单确认号(在hotel_getorder中)
        Long row1=orderService.updateHotel_confirm_number2(order_id,hotel_confirm_number);
        if(row>0){
            return new Result().zhuCeSuccess("添加/修改酒店订单确认号到订单表 成功");
        }
        else {
            return new Result().failed("添加/修改酒店订单确认号 失败");
        }
    }
    //根据订单id查询酒店订单确认号(前端订单确认号的回显)
    @ResponseBody
    @RequestMapping("QUERY_hotel_confirm_number")
    public Result QUERY_hotel_confirm_number(
            @RequestParam(value="order_id") Long order_id) {
        String hotel_confirm_number=orderService.queryHotel_confirm_number2(order_id);
        Map map=new HashMap();
        map.put("hotel_confirm_number",hotel_confirm_number);
        return new Result().success(map);
    }

    //修改订单信息,且记录日志
    /**
     * @param order_id 订单id
     * @param price  订单金额
     * @param caigou_one 结算单价
     * @param caigou_total 结算总额
     * @param room_count 房间数
     * @param check_in_time 入住时间
     * @param check_out_time 离店时间
     * @param nightC 间夜总数
     * @param state 订单状态,1 待确认,2 已确认,4 已取消,5 已确认取消
     * @param remark_one 订单备注
     * @return
     */
    @ResponseBody
    @RequestMapping("updateOrder")
    public Result updateOrder( @RequestParam(value="order_id") Long order_id,
                               @RequestParam(value="price") String price,
                               @RequestParam(value="caigou_one") String caigou_one,
                               @RequestParam(value="caigou_total") String caigou_total,
                               @RequestParam(value="room_count") String room_count,
                               @RequestParam(value="check_in_time") String check_in_time,
                               @RequestParam(value="check_out_time") String check_out_time,
                               @RequestParam(value="nightC") String nightC,
                               @RequestParam(value="state") int state,
                               @RequestParam(value="remark_one") String remark_one){
        //当前用户id
        Long currentUserId = getCurrentUserId();

        if (null == order_id){
            return new Result().failed("订单号不能为空");
        }
        else if (Strings.isNullOrEmpty(price)){
            return new Result().failed("订单金额不能为空");
        }
        else if (Strings.isNullOrEmpty(caigou_one)){
            return new Result().failed("结算单价不能为空");
        }
        else if (Strings.isNullOrEmpty(caigou_total)){
            return new Result().failed("结算总额不能为空");
        }
        else if (Strings.isNullOrEmpty(room_count)){
            return new Result().failed("房间数不能为空");
        }
        else if (Strings.isNullOrEmpty(check_in_time)){
            return new Result().failed("入住时间不能为空");
        }
        else if (Strings.isNullOrEmpty(check_out_time)){
            return new Result().failed("离店时间不能为空");
        }
        else if (Strings.isNullOrEmpty(nightC)){
            return new Result().failed("间夜总数不能为空");
        }
        //先根据订单id查询数据库是否存在此订单,不存在不能修改,存在才能根据订单id修改
        HotelOrder hotelOrder = orderService.queryOrder(order_id);
        if (null==hotelOrder){
            return new Result().failed("此订单不存在,无法修改");
        }

        //根据订单id修改订单信息,参数传递order_id和order
        HotelOrder order = new HotelOrder();
        //订单id
        order.setOrderId(order_id);
        //订单金额
        BigDecimal price1=new BigDecimal(price);
        order.setPrice(price1);
        //结算金额
		BigDecimal settle_money = new BigDecimal(0);
		int commission_rate = hotelOrder.getCommission_rate();
		settle_money = price1.multiply(new BigDecimal((100 - commission_rate))).divide(new BigDecimal(100));
		settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
		order.setSettle_money(String.valueOf(settle_money));
		//结算单价
        BigDecimal caigou_one1 = new BigDecimal(caigou_one);
        order.setCaigou_one(caigou_one1);
        //结算总额
        BigDecimal  caigou_total1=new BigDecimal(caigou_total);
        order.setCaigou_total(caigou_total1);
        //间数
        order.setRoomCount(Integer.valueOf(room_count));
        //入住时间
        try {
            order.setCheckInTime(DateUtils.parseDate(check_in_time,"yyyy-MM-dd"));
        }catch (Exception e){
            return new Result().failed("入住时间格式错误");
        }
        //离店时间
        try {
            order.setCheckOutTime(DateUtils.parseDate(check_out_time,"yyyy-MM-dd"));
        }catch (Exception e){
            return new Result().failed("离店时间格式错误");
        }
        //间夜总数
        order.setNightC(Integer.valueOf(nightC));
        //订单状态
        order.setState(state);
        //订单备注
        order.setRemark_one(remark_one);


        //根据订单id修改订单信息
        int row= orderService.updateOrder(order);
        int row2= orderService.updateOrder2(order);
        //订单状态改为1或者4的,需要弹框提醒:"来订单了..."
        if(state == 1 || state == 4){
            orderMsgService.sendMsgByOrderId(order_id);
        }
        if(row>0){
            //设置房型id,下面的日志用到
            order.setHotelRoomTypeId(hotelOrder.getHotelRoomTypeId());
            order.setHotelId(hotelOrder.getHotelId());
            //修改成功后,记录日志信息,传递原订单信息,进行日志记录对比
            opLog_HotelOrder(order,currentUserId,hotelOrder);
            return new Result().zhuCeSuccess("修改订单信息成功");
        }
        else {
            return new Result().failed("修改订单信息失败");
        }
    }

    //修改订单信息的日志记录
    public void opLog_HotelOrder(HotelOrder order, Long currentUserId, HotelOrder hotelOrder) {
        OplogVO oplogVO = new OplogVO();
        //当前用户id
        oplogVO.setUserId(currentUserId);
        //日志操作时间
        Date createDate = new Date();
        oplogVO.setCreateTime(createDate);
        //房型id
        Long[]roomTypeIds={order.getHotelRoomTypeId()};
        oplogVO.setHotelRoomTypeIds(roomTypeIds);
        //售卖时间(入住时间)
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String checkInTime = sdf.format(order.getCheckInTime());
        String checkOutTime= sdf.format(order.getCheckOutTime());
        //之前订单信息的入离时间
		String checkInTime1 = sdf.format(hotelOrder.getCheckInTime());
		String checkOutTime1= sdf.format(hotelOrder.getCheckOutTime());

        oplogVO.setStartDate(checkInTime);
        oplogVO.setEndDate(checkInTime);
        //操作内容:hotelGetOrder中的数据,不能用",",因为后面有切割
        String opValue= "●订单号:"+hotelOrder.getOrderId()+
						"●订单金额:"+hotelOrder.getPrice()+"【"+order.getPrice()+"】"+
						"●结算总额:"+hotelOrder.getCaigou_total()+"【"+order.getCaigou_total()+"】"+
						"●间数:"+hotelOrder.getRoomCount()+"【"+order.getRoomCount()+"】"+
						"●入住时间:"+checkInTime1+"【"+checkInTime+"】"+
						"●离店时间:"+checkOutTime1+"【"+checkOutTime+"】"+
						"●间夜总数:"+hotelOrder.getNightC()+"【"+order.getNightC()+"】"+
						"●订单状态:"+hotelOrder.getState()+"【"+order.getState()+"】"+
						"●备注:"+hotelOrder.getRemark_one()+"【"+order.getRemark_one()+"】";
        oplogVO.setOpValue(opValue);
        //操作类型,OpTypeEnum中的code
        oplogVO.setOpType(9);
        //酒店id
        oplogVO.setHotelId(order.getHotelId());
        opLogService.saveOpLogs(oplogVO);
    }

    //修改订单(新接口)
	@ResponseBody
	@RequestMapping("updateOrderOfNew")
	public Response updateOrderOfNew(@RequestBody JSONObject updateOrderList)throws Exception {
		JSONObject jsonObject = JSON.parseObject(updateOrderList.toJSONString());
        Long order_id = jsonObject.getLong("order_id");
        String price =jsonObject.getString("price");
        String room_count =jsonObject.getString("room_count");
        String check_in_time = jsonObject.getString("check_in_time");
        String check_out_time =jsonObject.getString("check_out_time");
        String nightC = jsonObject.getString("nightC");
        int state =jsonObject.getIntValue("state");
        String remark_one = jsonObject.getString("remark_one");
        String orderOfCaigouoneListStr = jsonObject.getString("orderOfCaigouoneList");
        //将orderOfCaigouoneListStr转换为OrderOfCaigouone对象匹配格式
        List<OrderOfCaigouone> orderOfCaigouoneList = JSON.parseArray(URLDecoder.decode(orderOfCaigouoneListStr, "UTF-8"), OrderOfCaigouone.class);
        //去除list中的空元素
        orderOfCaigouoneList.removeAll(Collections.singleton(null));
        //当前用户id
		Long currentUserId = getCurrentUserId();
		if (null == order_id){
			return new Response().failed("订单号不能为空");
		}
		else if (Strings.isNullOrEmpty(price)){
			return new Response().failed("订单金额不能为空");
		}
		else if (Strings.isNullOrEmpty(room_count)){
			return new Response().failed("房间数不能为空");
		}
		else if (Strings.isNullOrEmpty(check_in_time)){
			return new Response().failed("入住时间不能为空");
		}
		else if (Strings.isNullOrEmpty(check_out_time)){
			return new Response().failed("离店时间不能为空");
		}
		else if (Strings.isNullOrEmpty(nightC)){
			return new Response().failed("间夜总数不能为空");
		}
		//先根据订单id查询数据库是否存在此订单,不存在不能修改,存在才能根据订单id修改
		HotelOrder hotelOrder = orderService.queryOrder(order_id);
		if (null==hotelOrder){
			return new Response().failed("此订单不存在,无法修改");
		}

		//根据订单id修改订单信息,参数传递order_id和order
		HotelOrder order = new HotelOrder();
		//订单id
		order.setOrderId(order_id);
		//订单金额
		BigDecimal price1=new BigDecimal(price);
		order.setPrice(price1);
		//结算金额
		BigDecimal settle_money = new BigDecimal(0);
		int commission_rate = hotelOrder.getCommission_rate();
		settle_money = price1.multiply(new BigDecimal((100 - commission_rate))).divide(new BigDecimal(100));
		settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
		order.setSettle_money(String.valueOf(settle_money));

		//间数
		order.setRoomCount(Integer.valueOf(room_count));
		//入住时间
		try {
			order.setCheckInTime(DateUtils.parseDate(check_in_time,"yyyy-MM-dd"));
		}catch (Exception e){
			return new Response().failed("入住时间格式错误");
		}
		//离店时间
		try {
			order.setCheckOutTime(DateUtils.parseDate(check_out_time,"yyyy-MM-dd"));
		}catch (Exception e){
			return new Response().failed("离店时间格式错误");
		}
		//间夜总数
		order.setNightC(Integer.valueOf(nightC));
		//订单状态
		order.setState(state);
		//订单备注
		order.setRemark_one(remark_one);

        //修改置单日历
        //根据订单id查询所有置单日历表
        List<OrderOfCaigouone> orderOfCaigouoneList1=orderOfCaigouoneService.queryOrderOfCaigouoneListByorderId(order_id);
        List<Long> ids= Lists.newArrayList();
        Map<Long,OrderOfCaigouone>map=new HashMap<>();
        if (!CollectionUtils.isEmpty(orderOfCaigouoneList)){
            for (OrderOfCaigouone orderOfCaigouone : orderOfCaigouoneList) {
                ids.add(orderOfCaigouone.getId());
                map.put(orderOfCaigouone.getId(),orderOfCaigouone);
            }
        }else {
            return new Response().failed("需要修改的置单日历列表为空");
        }

        if (!CollectionUtils.isEmpty(orderOfCaigouoneList1)){
            for (OrderOfCaigouone orderOfCaigouone1 : orderOfCaigouoneList1) {
                Long id1 = orderOfCaigouone1.getId();
                if (ids.contains(id1)){
                    OrderOfCaigouone orderOfCaigouone = map.get(id1);
                    orderOfCaigouoneService.update(orderOfCaigouone);
                }else {
                    orderOfCaigouone1.setCaigou_one_state(1);
                    orderOfCaigouone1.setRemark("此条删除");
                    orderOfCaigouoneService.update(orderOfCaigouone1);
                }
            }
        }else {
            return new Response().failed("此单的置单日历列表为空");
        }


        //采购总价
        BigDecimal caigou_total=orderOfCaigouoneService.sumCaigoutotal(order_id,0);
        if (null==caigou_total){
            caigou_total= BigDecimal.valueOf(-1);
        }
        order.setCaigou_total(caigou_total);

		//根据订单id修改订单信息
		int row= orderService.updateOrder(order);
		int row2= orderService.updateOrder2(order);

		//订单状态改为1或者4的,需要弹框提醒:"来订单了..."
		if(state == 1 || state == 4){
			orderMsgService.sendMsgByOrderId(order_id);
		}
		if(row>0){
			//设置房型id,下面的日志用到
			order.setHotelRoomTypeId(hotelOrder.getHotelRoomTypeId());
			order.setHotelId(hotelOrder.getHotelId());
			//修改成功后,记录日志信息,传递原订单信息,进行日志记录对比
			opLog_HotelOrder(order,currentUserId,hotelOrder);
			return new Response().success("修改订单信息成功");
		}
		else {
			return new Response().failed("修改订单信息失败");
		}
	}

}
