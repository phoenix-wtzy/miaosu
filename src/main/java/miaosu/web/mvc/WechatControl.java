package miaosu.web.mvc;

import miaosu.svc.user.UserService;
import miaosu.utils.JsonUtils;
import miaosu.wechat.WechatConfig;
import miaosu.wechat.WechatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author zyz
 * @Date: 2020/12/1 10:15
 * @Description: 微信验证
 */



@Controller
public class WechatControl extends BaseController{
    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate redisTemplate;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @ResponseBody
    @RequestMapping(value = "/wechatAccess",method = RequestMethod.GET)
    public void WechatAccess(HttpServletRequest request, HttpServletResponse response) {
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");

        PrintWriter out = null;
        try {
            out = response.getWriter();
            if (WechatUtils.checkSignature(signature, timestamp, nonce)) {
                logger.info("wechat接入成功！");
                out.println(echostr);
                out.flush();
                out.close();
            }else{
                logger.info("wechat接入失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    /**
     * @description: TODO
     * 获取微信post请求,进行消息模板的推送
     * @author zhangyz
     * @date 2020/12/2 17:21
     * @version 1.0
     */
    @ResponseBody
    @RequestMapping(value = "/wechatAccess",method = RequestMethod.POST)
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("utf8");
        response.setCharacterEncoding("utf8");
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("接收消息事件推送");
        Map<String,String> requestMap = WechatUtils.parseRequest(request.getInputStream());
        //获取需要返回微信端信息
        String respXml = WechatUtils.getRespose(requestMap);
        PrintWriter out = response.getWriter();
        out.print(respXml);
        out.flush();
        out.close();
    }

    /**
     * @description: TODO
     * 获取用户信息进行绑定微信公众号
     * @author zhangyz
     * @date 2020/12/4 10:33
     * @version 1.0
     */
    @RequestMapping(value = "/wechat/wechatAccess/getUserInfo",method = RequestMethod.GET)
    public String GetUserInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("utf8");
        response.setCharacterEncoding("utf8");
        response.setContentType("text/html;charset=UTF-8");
        //获取code
        String code = request.getParameter("code");
        //换取accesstoken的地址
        String url ="https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
        url = url.replace("APPID",WechatConfig.getAppid()).replace("SECRET",WechatConfig.getAppsecret()).replace("CODE",code);
        String result = WechatUtils.get(url);
        //获取需要返回微信端信息
        Map<String, Object> jsonObj = JsonUtils.parseJSONMap(result);
        String openid = String.valueOf(jsonObj.get("openid"));
        String errcode = String.valueOf(jsonObj.get("errcode"));

        PrintWriter out = response.getWriter();
        //当前用户已绑定
        if(errcode.equals("40163")){
//            out.print("<h1>当前用户已绑定！</h1>");
//            out.flush();
//            out.close();
            logger.info("当前用户已绑定！");
            return null;
        }else {
            //保存openid并且绑定用户
            List<String> currentUserRoles = getCurrentUserRoles();
            StringBuilder roles = new StringBuilder();
            for (int i = 0; i<currentUserRoles.size(); i++){
                roles.append(currentUserRoles.get(i));
                if(currentUserRoles.size()>i+1){
                    roles.append(",");
                }
            }
            //保存用户openid跟用户角色
            userService.updateUserWxOpenid(getCurrentUserId(),openid,roles.toString());
            return "redirect:/charts";
        }

//        if(openid != null){
//            List<Object> openidList = redisTemplate.opsForList().range("openidList",0,-1);
//            for(Object openid1:openidList){
//                if(openid.equals(openid1)){
//                    out.print("<h1>当前用户已绑定！</h1>");
//                    out.flush();
//                    out.close();
//                    return;
//                }
//            }
//            redisTemplate.opsForList().leftPush("openidList",openid);
//            //输出页面信息
//            out.print("<h1>用户已成功绑定，请关闭当前页面！</h1>");
//        }else{
//            out.print("<h1>未获取到openid，用户绑定失败，请联系管理员！</h1>");
//        }
//        out.flush();
//        out.close();
        //System.out.println(result);
    }
}
