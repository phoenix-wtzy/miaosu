package miaosu.web.mvc;

import com.google.common.collect.Lists;
import miaosu.common.RoleEnum;
import miaosu.config.CustUserDetailsService;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelRefUser;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/1/9.
 */
public class BaseController {

    @Autowired
    public UserService userService;
    @Autowired
    public HotelService hotelService;

    public CustUserDetailsService.CustUserDetail getCurrentUser(){
        // 获取spring security封装的当前用户信息对象
        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if("anonymousUser".equals(object)){
            return null;
        }
        return (CustUserDetailsService.CustUserDetail) object;
    }

    public Long getCurrentUserId(){
        CustUserDetailsService.CustUserDetail userDetail = getCurrentUser();
        if(null == userDetail){
            return null;
        }
        return userDetail.getUser().getUserId();
    }

    public List<String> getCurrentUserRoles(){
        List<String> roles = Lists.newArrayList();
        CustUserDetailsService.CustUserDetail userDetail = getCurrentUser();
        if(null == userDetail){
            return roles;
        }
        Collection<? extends GrantedAuthority> authorities = userDetail.getAuthorities();
        for (GrantedAuthority grantedAuthority:authorities){
            roles.add(grantedAuthority.getAuthority());
        }
        return roles;
    }

    public boolean haveRoleAny(List<String> roles){
        List<String> currentUserRoles = getCurrentUserRoles();
        for (String role:roles){
            if (currentUserRoles.contains(role)){
                return true;
            }
        }
        return false;
    }

    public boolean isSuperAdmin(){
        boolean isSuperAdmin = haveRoleAny(Lists.newArrayList(RoleEnum.ROLE_SUPER.getName(),RoleEnum.ROLE_OTA.getName()));
        return isSuperAdmin;
    }
    public boolean isAdmin(){
        boolean isAdmin = haveRoleAny(Lists.newArrayList(RoleEnum.ROLE_ADMIN.getName(), RoleEnum.ROLE_SUPER.getName(),RoleEnum.ROLE_OTA.getName()));
        return isAdmin;
    }

    /**
     * 获取当前配置信息，选择的酒店
     * @return
     */
    public Long getCurHotel(){
       return userService.getSettingForCurHotel(this.getCurrentUserId());
    }


    /**
     * 获取酒店信息，判断权限使用，是否有该酒店的权限
     * @param hotelId
     * @return
     */
    public HotelInfo checkHotelInfoWithPermit(Long hotelId){
        Long currentUserId = this.getCurrentUserId();
        List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId, hotelId);
        if(hotelRefUsers.size()==0){
            return null;
        }
        HotelInfo hotelInfo = hotelService.getHotelInfoById(hotelId);
        return hotelInfo;
    }


}
