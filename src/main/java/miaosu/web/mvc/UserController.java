package miaosu.web.mvc;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import miaosu.dao.auto.RoleMapper;
import miaosu.dao.auto.UserMapper;
import miaosu.dao.auto.UserRoleMapper;
import miaosu.dao.model.*;
import miaosu.runtimeexception.ExistException;
import miaosu.svc.user.BusinessService;
import miaosu.svc.user.CodeService;
import miaosu.svc.user.UserCodeService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.Response;
import miaosu.utils.CodeUtils;
import miaosu.utils.GetTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/1/20 15:35
 * @Description: 用户管理
 */
@Controller
@RequestMapping("/users")
@Slf4j
public class UserController  extends BaseController{
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private UserRoleMapper userRoleMapper;
	@Autowired
	private BusinessService businessService;

	@Autowired
	private CodeService codeService;
	@Autowired
	private UserCodeService userCodeService;

	//查询所有用户及其对应的角色
	@ResponseBody
	@RequestMapping("list")
	public Response UserList(){
		List list=new ArrayList();
		UserExample userExample = new UserExample();
		userExample.setOrderByClause("user_id desc");
		List<User> users = userMapper.selectByExample(userExample);
		//查询所有用户对应的角色
		for (User user : users) {
			List<String> roles = new ArrayList<String>();
			List<UserRole> userRoles = userService.getUserRoles(user.getUserId());
			for (UserRole userRole:userRoles){
				Long roleId = userRole.getRoleId();
				Role role = getRole(roleId);
				roles.add(role.getRole_description());
			}
			user.setRoles(roles);
			list.add(user);
		}
		return new Response().success("查询所有用户",list.size(),list);
	}


	public Role getRole(Long roleId){
		RoleExample example = new RoleExample();
		example.createCriteria().andRoleIdEqualTo(roleId);
		List<Role> roles = roleMapper.selectByExample(example);
		if (roles.size()>0){
			return roles.get(0);
		}
		return null;
	}

	//添加用户
	@ResponseBody
	@RequestMapping("add")
	public Response Add(@RequestParam(value="user_nick") String user_nick,//用户名
						@RequestParam(value="user_name") String user_name,//账号(手机号)
						@RequestParam(value="user_passwd") String user_passwd,//密码
						@RequestParam(value="role_ids") Long[] role_ids){//角色id集合

		if (Strings.isNullOrEmpty(user_nick)){
			return new Response().failed("用户名不能为空");
		}
		else if (Strings.isNullOrEmpty(user_name)){
			return new Response().failed("账号不能为空");
		}
		else if (Strings.isNullOrEmpty(user_passwd)){
			return new Response().failed("密码不能为空");
		}
		else if (null==role_ids || role_ids.length==0 ){
			return new Response().failed("角色选择不能为空");
		}
		if (role_ids.length>1){
			return new Response().failed("只能选择一种角色");
		}
		//查询所有酒店id,(admin用户的user_id=1是拥有所有酒店id的)  目的是给此用户添加超管所能看到的所有酒店
//		List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(1L);
//		List<Long> hotelIdList= Lists.newArrayList();
//		for (HotelRefUser hotelRefUser : hotelRefUsers) {
//			hotelIdList.add(hotelRefUser.getHotelId());
//		}
		User user = userService.getUser(user_name);
		if (null == user){
			User record = new User();
			record.setUserNick(user_nick);
			record.setUserName(user_name);
			record.setUserPasswd(user_passwd);
			record.setUser_phone(user_name);
			record.setStatus(0);
			userMapper.insert1(record);
			user = userService.getUser(user_name);
			Long userId = user.getUserId();
			for (Long role_id : role_ids) {
				userService.inertUserRole(userId,role_id);
				//如果指定该用户为超管,那就拥有全部酒店的查看功能,在hotel_ref_user中将user_id和所有酒店id进行关联
//				if (role_id==1){
//					for (Long aLong : hotelIdList) {
//						hotelService.addHotelRefUser(aLong,userId);
//					}
//				}
				//添加的该用户角色是供应商,随机新增一个新的供应商用户
				if (role_id==2){
					//上家邀请码写死就是海南秒宿供应商的,暂时用不到邀请码了
					String Code_shangjia="b234c7";
					//生成自己的邀请码,并将自己的邀请码存储到code表中
					String code_my = CodeUtils.generateCode4String(6);
					//生成邀请码之后,设置此邀请码为未审核状态,只有填写公司信息才算审核通过,并且更改用户为二级账号
					String code_level="未通过";
					codeService.insertCode(code_my,code_level);
					//根据邀请码查询自己的邀请码id
					Long codeId=codeService.queryCodeId(code_my);
					//将user_id,code_id,code_shangjia,code_my添加到用户邀请码关系表
					userCodeService.insert(userId,codeId,Code_shangjia,code_my);

					Business business = new Business();
					business.setUser_id(userId);
					business.setBusiness_name(user_nick);
					businessService.insert(business);
				}
				//添加的新用户的角色为酒店,自动在酒店表中添加一条
				if (role_id==3){
					/**
					 * 酒店名称
					 * 酒店平台id
					 * 酒店地址:省市区
					 * 详细地址
					 * 联系人名
					 * 联系手机
					 * 前台电话
					 * 管理账号
					 * 管理密码
					 * 管理名称
					 * 销售类型
					 * 协议置换总金额
					 * 协议置换间夜数
					 * 包房总金额
					 * 投资人分润比例
					 * OTA管家电话
					 * 酒店有效性
					 */
					String hotelName=GetTimeUtils.nowTimeRandomDate();
					String hotel_only_id=CodeUtils.generateCode4String(6);
					String provinceCode="103";
					String cityCode="baicheng";
					String districtCode="33314";
					String address="河南郑州市金水区黄河路经二路璞丽中心A座";
					String contact="秒宿";
					String contactMobile="18888888888";
					String receptionPhone="0371-88888888";
					int hotelType=1;
					String settle_totalMoney="0";
					String settle_totalNight="0";
					String baoFang_totalMoney="";
					String touZiRen_rates="";
					String ota_phone="";
					int isActive=1;

					boolean admin = isAdmin();
					Long currentUserId = getCurrentUserId();
					//添加酒店信息
					Long hotelId = hotelService.addHotelInfo(hotelName,hotel_only_id, address, contact, contactMobile, userId,receptionPhone,ota_phone,hotelType,settle_totalMoney,settle_totalNight,baoFang_totalMoney,touZiRen_rates,isActive);
					//添加酒店扩展信息
					hotelService.addHotelInfoExt(hotelId,provinceCode,cityCode,districtCode);
					if(null == hotelId){
						throw new ExistException("酒店已经存在");
					}
					//添加关联关系
					if(null != hotelId){
						hotelService.addHotelRefUser(hotelId,userId);
						userService.setForCurHotel(userId,hotelId);
						if(admin){
							hotelService.addHotelRefUser(hotelId,currentUserId);
						}
					}
				}
			}
			return new Response().success("添加用户成功");
		}
		return new Response().failed("用户已经存在");
	}

	//修改用户
	@ResponseBody
	@RequestMapping("update")
	public Response Update(@RequestParam(value="user_id") Long user_id,
						   @RequestParam(value="user_nick") String user_nick,
						   @RequestParam(value="user_phone") String user_phone,
						   @RequestParam(value="user_passwd") String user_passwd,
						   @RequestParam(value="role_ids") Long[] role_ids){
		if (null == user_id){
			return new Response().failed("用户id不能为空");
		}
		else if (Strings.isNullOrEmpty(user_nick)){
			return new Response().failed("用户名不能为空");
		}
		else if (Strings.isNullOrEmpty(user_phone)){
			return new Response().failed("手机号不能为空");
		}
		else if (Strings.isNullOrEmpty(user_passwd)){
			return new Response().failed("密码不能为空");
		}
		else if (null==role_ids || role_ids.length==0 ){
			return new Response().failed("角色选择不能为空");
		}
		if (role_ids.length>1){
			return new Response().failed("只能选择一种角色");
		}
		User user = userService.getUserById(user_id);
		if (null == user){
			return new Response().failed("用户不存在,不能修改");
		}
		if (user_id==1){
			return new Response().failed("超级管理员admin不能动");
		}
		else {
			//修改用户信息
			User user1 = new User();
			user1.setUserId(user_id);
			user1.setUserNick(user_nick);
			user1.setUser_phone(user_phone);
			user1.setUserPasswd(user_passwd);
			userMapper.updateByPrimaryKeySelective(user1);
			//修改用户对应的角色
			List<Long> longs = userRoleMapper.queryUser_Role(user_id);
			if (null == longs) {
				return new Response().failed("用户对应的角色不存在");
			}
			for (int i = 0; i < longs.size(); i++) {
				//在business中存在的user_id不能成为超管和其他角色
				Long ifExistUserId = businessService.queryIfExistUserId(user_id);
				if (ifExistUserId!=null){
					return new Response().failed("该供应商的信息修改成功,但是不能修改角色");
				}else {
					//根据之前的用户id和角色id修改角色id
					userRoleMapper.updateByUser_id(role_ids[i], user_id, longs.get(i));
					//之前此用户有很多角色,但是修改后只有一种角色的操作
					if (role_ids.length < longs.size()) {
						int i1 = (longs.size() - i) - 1;
						List<Long> longs1 = new ArrayList<>();
						for (int i2 = 0; i2 < i1; i2++) {
							longs1.add(longs.get(i2 + 1));
						}
						for (int j = 0; j < i1; j++) {
							UserRoleExample userRoleExample = new UserRoleExample();
							UserRoleExample.Criteria criteria = userRoleExample.createCriteria();
							criteria.andUserIdEqualTo(user_id).andRoleIdIn(longs1);
							userRoleMapper.deleteByExample(userRoleExample);
						}
						break;
					}
				}


//			//查询所有酒店id,(admin用户的user_id=1是拥有所有酒店id的)  目的是给此用户添加超管所能看到的所有酒店
//			List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(1L);
//			List<Long> hotelIdList = Lists.newArrayList();
//			for (HotelRefUser hotelRefUser : hotelRefUsers) {
//				hotelIdList.add(hotelRefUser.getHotelId());
//			}
//				for (Long aLong : hotelIdList) {
//					//变成超管的操作,将所有酒店和此用户进行关联;先查询hotel_ref_user表中是否存在关联,没有再添加,存在不用再重复添加了
//					if (role_ids[i] == 1) {
//						List<HotelRefUser> hotelRefUsers1 = hotelService.queryHotelRefUserByUser(user_id, aLong);
//						if (CollectionUtils.isEmpty(hotelRefUsers1)) {
//							hotelService.addHotelRefUser(aLong, user_id);
//						}
//					}
//					//不是变成超管的操作,比较复杂,需要考虑供应商以及酒店对用户的关联
//					else {
//						/**
//						 * 1.查询business表,不存在user_id则再查询hotel_info表,也不存在user_id则在hotel_ref_user表中删除所有关联
//						 * 2.查询business表,不存在user_id则再查询hotel_info表,存在user_id,则在hotel_ref_user表中删除其他的关联
//						 * 3.查询business表,存在user_id,则不能修改角色
//						 */
//
//
//						//先进行查询,如果hotel_info中存在user_id和hotel_id的关联关系,则hotel_ref_user表中不能删除
//						Long aLong1 = hotelService.queryHotelIdByUserId(user_id);
//						//在hotel_info中啥关系都不存在,直接全部删除
//						if (aLong1 == null) {
//							hotelService.deleteHotelRefUser(user_id, aLong);
//						}
//						//在hotel_info中存在关联关系,但是得删除其他多余的关联
//						if (aLong1 != null && aLong1 != aLong) {
//							hotelService.deleteHotelRefUser(user_id, aLong);
//						}
//					}
//				}



			}
		}
		return new Response().success("修改用户成功");
	}

	//删除用户
	@ResponseBody
	@RequestMapping("delete")
	public Response Delete(@RequestParam(value="user_id") Long user_id){
		if (null == user_id){
			return new Response().failed("用户id不能为空");
		}
		User user = userService.getUserById(user_id);
		if (null == user){
			return new Response().failed("用户不存在,不能删除");
		}
		int i = userMapper.deleteByPrimaryKey(user_id);
		if (i>0){
			return new Response().success("删除用户成功");
		}
		return null;
	}
}