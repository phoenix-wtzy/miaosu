package miaosu.web.mvc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import miaosu.dao.auto.HotelRoomSetMapper;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelPriceSet;
import miaosu.dao.model.HotelRoomSet;
import miaosu.dao.model.HotelRoomSetExample;
import miaosu.svc.hotel.HotelRoomSetService;
import miaosu.svc.hotel.IHotelPriceSetService;
import miaosu.svc.order.OrderService;
import miaosu.svc.vo.HotelPriceSetVO;
import miaosu.svc.vo.HotelRoomSetVO;
import miaosu.svc.vo.Response;
import miaosu.svc.vo.Result;
import miaosu.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/1/9.
 * 计划价格控制层
 */
@Controller
@Slf4j
public class RoomSetPriceController extends BaseController {
//    private static final Logger logger = LoggerFactory.getLogger(RoomSetPriceController.class);

    @Autowired
    private HotelRoomSetService hotelRoomSetService;
    @Autowired
    private IHotelPriceSetService hotelPriceSetService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;
    /**
     * 进入计划价格首页
     * @param model 进入首页存放的model
     * @param hotelId 酒店id
     * @return 跳转到酒店设置的计划价格页面
     */
    @GetMapping("/room/set/price/{hotelId}")
    public String set(Model model, @PathVariable long hotelId) {

        HotelInfo hotel = checkHotelInfoWithPermit(hotelId);

        if (null == hotel){
            model.addAttribute("error", "无操作权限");
            return "forward:/403";
        }else {
            List<HotelRoomSetVO> hotelRoomSetList = hotelRoomSetService.queryHotelRoomSetList(hotelId);

            model.addAttribute("hotelInfo", hotel);
            model.addAttribute("hotelRoomSetList", hotelRoomSetList);

            return "room/set/price";
        }
    }

    /**
     * 获取计划价格详细信息 房型 和酒店类型的计划价格信息
     * @param hotelId 酒店id
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/set/price/list")
    public Result list(@RequestParam(value="hotelId", required=true) long hotelId) {
        List<HotelPriceSetVO> list = Lists.newArrayList();
        HotelInfo hotel = checkHotelInfoWithPermit(hotelId); //验证权限是否可以查询该酒店
        if (null != hotel) {
            list = hotelPriceSetService.queryHotelPriceSetList(hotelId);
        }
        log.info("计划价格列表信息：{}", JsonUtils.objectToJson(list));
        return new Result().success().setData(list);
    }

    /**
     * 添加计划价格
     * @param priceTypeName 计划价格名称
     * @param hotelRoomTypeIds 所关联的房型列表信息
     * @param hotelId 酒店id
     * @param breakfastCount 早餐数量
     * @param unbookType  退订类型
     * @param model
     * @return 成功与否的信息
     */
    @ResponseBody
    @RequestMapping("/room/set/price/add")
    public Result add(@RequestParam(value="priceTypeName", required=true) String priceTypeName,
                      @RequestParam(value="hotelRoomTypeIdAdd", required=true) Long[] hotelRoomTypeIds,
                      @RequestParam(value="hotelId", required=true) long hotelId,
                      @RequestParam(value="breakfastCount", required=true) int breakfastCount,
                      @RequestParam(value="unbookType", required=true) int unbookType,
                      @RequestParam(value="caigou_one", required=true) String caigou_one_price,
                      Model model) {

        if (Strings.isNullOrEmpty(priceTypeName)) {
            return new Result().failed("价格名称不能为空");
        } else if (hotelRoomTypeIds == null) {
            return new Result().failed("对应房型必须选择");
        }else if (Strings.isNullOrEmpty(caigou_one_price)){
            return new Result().failed("置换结算价不能为空");
        }

        List<Long> hotel_price_idList=Lists.newArrayList();
        //根据关联房型和早餐份数,去判断重复与否
        for (Long hotelRoomTypeId : hotelRoomTypeIds) {
             hotel_price_idList = orderService.queryhotel_price_id(hotelRoomTypeId);
        }
        for (Long hotel_price_id : hotel_price_idList) {
            boolean ifExitPriceSet = hotelPriceSetService.ifExitPriceSet(hotel_price_id,hotelId,breakfastCount);
            if (ifExitPriceSet){
                return new Result().failed("关联房型+早餐份数的计划价格重复");
            }
        }


        log.debug("add price type {} {}", hotelId, priceTypeName);
//        //先查询计划
//
//        boolean flag = hotelPriceSetService.hasExitPriceSet(hotelId,priceTypeName, unbookType, breakfastCount);
//        if(flag){
//            return new Result().failed("计划价格重复");
//        }
        //添加每条订单对应的每单采购价,添加到订单表中
        //将字符创的每单采购价转化成BigDecimal类型
        BigDecimal caigou_one = new BigDecimal(caigou_one_price);
        try{
            caigou_one = caigou_one.setScale(2, BigDecimal.ROUND_HALF_UP);
        }catch (Exception e){
            return new Result().failed("置换结算价格式错误,应为纯数字");
        }
        long hotelPriceId = hotelPriceSetService.addPriceType(caigou_one,hotelId, priceTypeName, unbookType, breakfastCount);
        hotelPriceSetService.addPriceRefRoomType(hotelPriceId,hotelRoomTypeIds);
        return new Result().success();
    }

    /**
     * 计划价格的更新
     * @param priceTypeName 计划价格名称
     * @param hotelRoomTypeIds 所关联的房型列表信息
     * @param hotelId 酒店id
     * @param hotelPriceId 计划价格主键
     * @param breakfastCount 早餐数量
     * @param unbookType 退订类型
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/set/price/update")
    public Result update(@RequestParam(value="priceTypeName", required=true) String priceTypeName,
                      @RequestParam(value="hotelRoomTypeId", required=true) Long[] hotelRoomTypeIds,
                      @RequestParam(value="hotelId", required=true) long hotelId,
                      @RequestParam(value="hotelPriceId", required=true) long hotelPriceId,
                      @RequestParam(value="breakfastCount", required=true) int breakfastCount,
                      @RequestParam(value="unbookType", required=true) int unbookType,
                         @RequestParam(value="caigou_one", required=true) String caigou_one_price,
                      Model model) {

        if (Strings.isNullOrEmpty(priceTypeName)) {
            return new Result().failed("价格名称不能为空");
        } else if (hotelRoomTypeIds == null) {
            return new Result().failed("对应房型必须选择");
        }else if (Strings.isNullOrEmpty(caigou_one_price)){
            return new Result().failed("置换结算价不能为空");
        }
        BigDecimal caigou_one = new BigDecimal(caigou_one_price);
        try{
            caigou_one = caigou_one.setScale(2, BigDecimal.ROUND_HALF_UP);
        }catch (Exception e){
            return new Result().failed("置换结算价格式错误,应为纯数字");
        }
        log.debug("update price type {} {} {}", hotelId, priceTypeName,hotelRoomTypeIds);
        hotelPriceSetService.updatePriceType(hotelPriceId,priceTypeName,unbookType,breakfastCount,caigou_one);
        hotelPriceSetService.updatePriceRefRoomType(hotelPriceId,hotelRoomTypeIds);

        return new Result().success();
    }

    /**
     * 计划价格的删除
     * @param hotelPriceId 计划价格id
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/set/price/delete")
    public Result add(@RequestParam(value="hotelPriceId", required=true) long hotelPriceId,
                      Model model) {
        Long hotelId = getCurHotel();
        hotelPriceSetService.deleteByHotelPriceId(hotelId,hotelPriceId);
        return new Result().success();
    }

    /**
     * 获取该计划价格下所关联的所有房型信息
     * @param hotelId 酒店id
     * @param hotelPriceId 酒店计划价格
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/set/price/refroomtype")
    public Result refroomtype(@RequestParam(value="hotelId", required=true) long hotelId,
                              @RequestParam(value="hotelPriceId", required=true) long hotelPriceId,
                              Model model) {
        List<HotelRoomSet> list = Lists.newArrayList();
        HotelInfo hotel = checkHotelInfoWithPermit(hotelId); //验证权限是否可以查询该酒店
        if (null != hotel) {
            list = hotelPriceSetService.getHotelRoomSet(hotelPriceId);
        }

        log.info("酒店下的关联房型信息列表：{}",JsonUtils.objectToJson(list));
        return new Result().success().setData(list);
    }


    /**
     *设置价格计划的有效性
     * @param hotelId 酒店id
     * @param hotelPriceId  计划价格id
     * @param type 激活与否的标识
     * @return
     */
    @ResponseBody
    @RequestMapping("room/set/price/active")
    public Result activePriceSet(Long hotelId,Long hotelPriceId,Integer type){
        if(hotelId==null){
            return new Result().failed("酒店id不能为空");
        }
        if(hotelPriceId==null){
            return new Result().failed("价格计划id不能为空");
        }
        if(type==null){
            return new Result().failed("是否激活的信息为空");
        }
        boolean isActive = true;

        //获取审核结果
        return  new Result().success();
    }
    //房价日历中的添加房型(存储到hotel_price_set中)
    @ResponseBody
    @RequestMapping("/roomType/set")
    public Response search_update(@RequestParam(value="hotelId", required=true) Long hotel_id,
                                  @RequestParam(value="hotelPriceName", required=true) String hotel_price_name){
        if (null == hotel_id){
            return new Response().failed("酒店id不能为空");
        }else if (Strings.isNullOrEmpty(hotel_price_name)){
            return new Response().failed("房型名称不能为空");
        }
        //1.添加房型
        //1.1房型判重,到hotel_room_set表中
        HotelRoomSetExample example = new HotelRoomSetExample();
        example.createCriteria().andHotelIdEqualTo(hotel_id).andHotelRoomTypeNameEqualTo(hotel_price_name);
        List<HotelRoomSet> hotelRoomSets = hotelRoomSetMapper.selectByExample(example);
		int hotelRoomTypeId;
        if (hotelRoomSets.size()>0){
			//针对重复的房型,不做任何处理,只取对应的hotelRoomTypeId方便下面使用
			HotelRoomSet hotelRoomSet = hotelRoomSets.get(0);
			hotelRoomTypeId= Math.toIntExact(hotelRoomSet.getHotelRoomTypeId());
		}else {
			//1.2添加房型设置,hotel_room_set
			hotelRoomTypeId=hotelRoomSetService.addRoomType(hotel_id,hotel_price_name,2);
		}


        //2添加房价
        //2.1房价判重,到hotel_price_set表中
        List<HotelPriceSet> hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(hotel_id, hotel_price_name);
        if (!CollectionUtils.isEmpty(hotelPriceSets)){
            return new Response().failed("房价重复,添加失败");
        }
        //2.2添加房价设置,hotel_price_set
        long hotelPriceId = hotelPriceSetService.addPriceType(BigDecimal.valueOf(-1),hotel_id, hotel_price_name, 1, 2);

        //3.添加关联,在hotel_price_ref_room_type表中
        Long[]hotelRoomTypeIds={Long.valueOf(hotelRoomTypeId)};
        hotelPriceSetService.addPriceRefRoomType(hotelPriceId,hotelRoomTypeIds);
        return new Response().success("添加房型成功");
    }

	//获取房型(从爬虫那边获取)
	@ResponseBody
	@RequestMapping("/room/set/getRoomType")
	public Response getRoomType(@RequestParam(value="hotelId", required=true) long hotelId){

		try {
			String hotel_only_id= hotelService.queryHotelOnlyId(hotelId);
			//本地url
			String url1 = "http://192.168.3.69:5000/get_three_id";//同舟,捷旅,云掌柜
			String url2 = "http://192.168.3.69:5000/get_dlt_id";//代理通,同程

			//阿里云url
//			String url1 = "http://192.168.3.69:5000/get_three_id";//同舟,捷旅,云掌柜
//			String url2 = "http://192.168.3.69:5000/get_dlt_id";//代理通,同程

			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("hotel_only_id", hotel_only_id);
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			//第一个url
			ResponseEntity<String> response1 = restTemplate.postForEntity(url1,request ,String.class);
			HttpStatus statusCode1 = response1.getStatusCode();
			if (200!=statusCode1.value()){
				return new Response().failed("爬虫那里连接不上(同舟,捷旅,云掌柜)");
			}
			JSONObject jsonObject1 = JSON.parseObject(response1.getBody());
			JSONArray contentArray1 = jsonObject1.getJSONArray("content");
			List<String> list1 =Lists.newArrayList();
			for (int i = 0; i < contentArray1.size(); i++) {
				JSONArray jsonArray1 = contentArray1.getJSONArray(i);
				for (Object o : jsonArray1) {
					list1.add(String.valueOf(o));
				}
			}
			if (CollectionUtils.isEmpty(list1)){
				return new Response().failed("房型更新的爬虫数据为空");
			}else {
				for (String hotel_price_name : list1) {
					//1.添加房型
					//1.1房型判重,到hotel_room_set表中
					HotelRoomSetExample example = new HotelRoomSetExample();
					example.createCriteria().andHotelIdEqualTo(hotelId).andHotelRoomTypeNameEqualTo(hotel_price_name);
					List<HotelRoomSet> hotelRoomSets = hotelRoomSetMapper.selectByExample(example);
					int hotelRoomTypeId;
					if (hotelRoomSets.size()>0){
						//针对重复的房型,不做任何处理,只取对应的hotelRoomTypeId方便下面使用
						HotelRoomSet hotelRoomSet = hotelRoomSets.get(0);
						hotelRoomTypeId= Math.toIntExact(hotelRoomSet.getHotelRoomTypeId());
					}else {
						//1.2添加房型设置,hotel_room_set
						hotelRoomTypeId=hotelRoomSetService.addRoomType(hotelId,hotel_price_name,2);
					}
					//2添加房价
					//2.1房价判重,到hotel_price_set表中
					List<HotelPriceSet> hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(hotelId, hotel_price_name);
					if (!CollectionUtils.isEmpty(hotelPriceSets)){
						//针对重复的房价,不做任何处理
					}else {
						//2.2添加房价设置,hotel_price_set
						long hotelPriceId = hotelPriceSetService.addPriceType(BigDecimal.valueOf(-1),hotelId, hotel_price_name, 1, 2);
						//3.添加关联,在hotel_price_ref_room_type表中
						Long[]hotelRoomTypeIds={Long.valueOf(hotelRoomTypeId)};
						hotelPriceSetService.addPriceRefRoomType(hotelPriceId,hotelRoomTypeIds);
					}
				}
			}

			//第二个url
			ResponseEntity<String> response2 = restTemplate.postForEntity(url2,request ,String.class);
			HttpStatus statusCode2 = response2.getStatusCode();
			if (200!=statusCode2.value()){
				return new Response().failed("爬虫那里连接不上(代理通,同程)");
			}
			JSONObject jsonObject2 = JSON.parseObject(response2.getBody());
			JSONArray contentArray2 = jsonObject2.getJSONArray("content");
			List<String> list2 =Lists.newArrayList();
			for (int i = 0; i < contentArray2.size(); i++) {
				JSONArray jsonArray2 = contentArray2.getJSONArray(i);
				for (Object o : jsonArray2) {
					list2.add(String.valueOf(o));
				}
			}
			if (CollectionUtils.isEmpty(list2)){
				return new Response().failed("房型更新的爬虫数据为空");
			}else {
				for (String hotel_price_name : list2) {
					//1.添加房型
					//1.1房型判重,到hotel_room_set表中
					HotelRoomSetExample example = new HotelRoomSetExample();
					example.createCriteria().andHotelIdEqualTo(hotelId).andHotelRoomTypeNameEqualTo(hotel_price_name);
					List<HotelRoomSet> hotelRoomSets = hotelRoomSetMapper.selectByExample(example);
					int hotelRoomTypeId;
					if (hotelRoomSets.size()>0){
						//针对重复的房型,不做任何处理,只取对应的hotelRoomTypeId方便下面使用
						HotelRoomSet hotelRoomSet = hotelRoomSets.get(0);
						hotelRoomTypeId= Math.toIntExact(hotelRoomSet.getHotelRoomTypeId());
					}else {
						//1.2添加房型设置,hotel_room_set
						hotelRoomTypeId=hotelRoomSetService.addRoomType(hotelId,hotel_price_name,2);
					}
					//2添加房价
					//2.1房价判重,到hotel_price_set表中
					List<HotelPriceSet> hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(hotelId, hotel_price_name);
					if (!CollectionUtils.isEmpty(hotelPriceSets)){
						//针对重复的房价,不做任何处理
					}else {
						//2.2添加房价设置,hotel_price_set
						long hotelPriceId = hotelPriceSetService.addPriceType(BigDecimal.valueOf(-1),hotelId, hotel_price_name, 1, 2);
						//3.添加关联,在hotel_price_ref_room_type表中
						Long[]hotelRoomTypeIds={Long.valueOf(hotelRoomTypeId)};
						hotelPriceSetService.addPriceRefRoomType(hotelPriceId,hotelRoomTypeIds);
					}
				}
			}
			return new Response().success("刷新房型成功");
		} catch (RestClientException e) {
			return new Response().failed("爬虫那里连接不上");
		}
	}
}
