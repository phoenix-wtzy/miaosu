package miaosu.web.mvc;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.yunpian.sdk.constant.Code;
import com.yunpian.sdk.model.SmsSingleSend;
import miaosu.common.BankUseTypeEnum;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelOrderExample;
import miaosu.dao.model.HotelRefUser;
import miaosu.dao.model.HotelSettleSetting;
import miaosu.sms.SmsUtil;
import miaosu.svc.order.OrderService;
import miaosu.svc.settle.HotelSettleService;
import miaosu.svc.vo.*;
import miaosu.utils.JsonUtils;
import miaosu.utils.SMSCodeGenerator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by xieqx.chen on 2018/4/11.
 */

@Controller
@RequestMapping("hotel/settle")
public class SettleController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private HotelSettleService hotelSettleService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private HotelSettleService settleService;

    @GetMapping("")
    public String settle(Model model) {
        boolean isAdmin = isAdmin();
        if (isAdmin){
            return "redirect:/admin/hotel/settle";
        }
        Long hotelId =  getCurHotel();
        if (hotelId==null){
            model.addAttribute("error", "无操作权限");
            return "error/404";
        }
        HotelSettleSetting settleSetting = settleService.queryHotelSettle(hotelId);
        if(settleSetting == null){
            return "error/lackSettle";
        }

        Date now = new Date();
        String date_end = DateFormatUtils.format(now,"yyyy-MM-dd");
        model.addAttribute("search_date", ImmutableMap.of("date_end",date_end));

        Long currentUserId = getCurrentUserId();
        model.addAttribute("channel",currentUserId);

        return "settle";
    }


    @ResponseBody
    @RequestMapping("/index")
    public Result set(long hotelId) {

        HotelInfo hotel = checkHotelInfoWithPermit(hotelId);
        HotelSettleSetting hotelSettleSetting = hotelSettleService.queryHotelSettle(hotelId);

        HotelSettleSettingVO settleSettingVO = new HotelSettleSettingVO();
        settleSettingVO.setHotelId(hotelId);
        settleSettingVO.setHotelName(hotel.getHotelName());

        if(hotelSettleSetting != null){
            settleSettingVO.setBankUserTypeName(BankUseTypeEnum.fromCode(hotelSettleSetting.getBankUseType()).getName());
            BeanUtils.copyProperties(hotelSettleSetting,settleSettingVO);
        }
        logger.info("结算设置信息：{}",JsonUtils.objectToJson(settleSettingVO));
        return new Result().success(settleSettingVO);
    }


    /**
     * 结算信息的更新/新增
     * @param settleSetting
     * @return
     */
    @ResponseBody
    @RequestMapping("/update")
    public Result settleUpdate(HotelSettleSetting settleSetting) {
        //HotelInfo hotel = checkHotelInfoWithPermit(settleSetting.getHotelId());

        boolean flag = hotelSettleService.updateSettleInfo(settleSetting);
        if(flag){
            return new Result().success();
        }

        return new Result().failed("设置结算信息失败");

    }


    //查询所有订单 计算其订单金额和
    @ResponseBody
    @RequestMapping("/settlOrderList")
    public PageResult getOrderBySettle(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_order_status", required=true) Integer search_order_status,
            @RequestParam(value="search_settle_status", required=false) Integer search_settle_status,
            @RequestParam(value="offset", required=false,defaultValue = "1") int offset,
            @RequestParam(value="limit", required=false,defaultValue = "10") int limit) {

        if(search_hotel_id==null){
            search_hotel_id = getCurHotel();
        }
        Long currentUserId = getCurrentUserId();
        boolean isAdmin = isAdmin();

        HotelOrderExample example = new HotelOrderExample();

        PageResult orderList = new PageResult();
        HotelOrderExample.Criteria and = example.createCriteria();
        Long hotelId = search_hotel_id;
        if(null != search_hotel_id){
            //权限过滤，看该用户是否有该酒店的权限
            List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId, search_hotel_id);
            logger.info("酒店信息：{}", JsonUtils.objectToJson(hotelRefUsers));
            if(hotelRefUsers.size()==0){
                return orderList;
            }
        }
        else {
            Long firstHotelId = getCurHotel();
            if (null == firstHotelId){
                firstHotelId = getFirstHotelId(currentUserId);
            }
            if(null == firstHotelId){
                logger.warn("没有关联酒店 for user {}",currentUserId);
                return orderList;
            }else {
                hotelId = firstHotelId;
            }
        }
        and.andHotelIdEqualTo(hotelId);
        try {
            String search_date_end = compluteDateEnd(search_date_start,hotelId);
            if (StringUtils.isNotEmpty(search_date_start) && StringUtils.isNotEmpty(search_date_end)) {
                Date date1 = DateUtils.parseDate(search_date_start, "yyyy-MM-dd");
                Date date2 = DateUtils.parseDate(search_date_end, "yyyy-MM-dd");
                date2 = DateUtils.addDays(date2, 1);

                and.andBookTimeBetween(date1, date2);

                if (search_order_status != 0) {
                    and.andStateEqualTo(search_order_status);
                }

                if(search_settle_status == null){
                    //查询已经进行并没有结算的订单
                    and.andSettleStatusEqualTo(0);
                }else if(search_settle_status !=-1){
                    and.andSettleStatusEqualTo(search_settle_status);
                }

                orderList = orderService.queryOrderListBypage(example,offset,limit,isAdmin);
                logger.info("结算订单列表：{}",JsonUtils.objectToJson(orderList));
                return orderList;
            }
        } catch (ParseException e){
            logger.error("查询订单失败",e);
            return orderList;
        }

        return  null;
    }



    @ResponseBody
    @RequestMapping("/bill/info")
    public HotelSettleBillVO billInfo(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_order_status", required=true) int search_order_status) {

        if(search_hotel_id==null){
            search_hotel_id = getCurHotel();
        }

        String search_date_end = compluteDateEnd(search_date_start,search_hotel_id);
        HotelSettleBillVO settleBillVO = hotelSettleService.showSettleBill(search_hotel_id,search_date_start,search_date_end,search_order_status);

        logger.info("结算账单 {}",JsonUtils.objectToJson(settleBillVO));
        return  settleBillVO;
    }


    @ResponseBody
    @RequestMapping("/bill/add")
    public Result billAdd(
           HotelSettleBillVO settleBillVO) {
        logger.info("结算账单：{}",JsonUtils.objectToJson(settleBillVO));
        if (Strings.isNullOrEmpty(settleBillVO.getOrderIds())){
            return new Result().failed("结算的订单不存在");
        }
        if (Strings.isNullOrEmpty(settleBillVO.getSettleBankAccount())){
            return new Result().failed("结算银行账号不存在");
        }
        if (settleBillVO.getStartDate()==null){
            return new Result().failed("结算开始时间不存在");
        }
        if (settleBillVO.getEndDate()==null){
            return new Result().failed("结算结算时间不存在");
        }
        /*
        BigDecimal commissionMoney = settleBillVO.getCommissionMoney();
        if (commissionMoney==null||commissionMoney.compareTo(new BigDecimal(0))<0){
            return new Result().failed("佣金金额不存在");
        }
        */
        boolean flag =  hotelSettleService.addSettleBill(settleBillVO);

        if(flag){
            return new Result().success();
        }
        return new Result().failed("添加结算账单失败");
    }




    //查询所有订单 计算其订单金额和
    @ResponseBody
    @RequestMapping("/settlInfo")
    public SettleInfoVO getSettleInfo(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_order_status", required=true) int search_order_status
            ) {

        Long currentUserId = getCurrentUserId();
        boolean isAdmin = isAdmin();

        HotelOrderExample order = new HotelOrderExample();

        HotelOrderExample.Criteria and = order.createCriteria();
        Long hotelId = search_hotel_id;
        if(null != search_hotel_id){
            //权限过滤，看该用户是否有该酒店的权限
            List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId, search_hotel_id);
            logger.info("酒店信息：{}", JsonUtils.objectToJson(hotelRefUsers));
            if(hotelRefUsers.size()==0){
                return null;
            }
        }
        else {
            Long firstHotelId = getCurHotel();
            if (null == firstHotelId){
                firstHotelId = getFirstHotelId(currentUserId);
            }
            if(null == firstHotelId){
                logger.warn("没有关联酒店 for user {}",currentUserId);
                return null;
            }else {
                hotelId = firstHotelId;
            }
        }
        and.andHotelIdEqualTo(hotelId);
        try {
            String search_date_end = compluteDateEnd(search_date_start,hotelId);
            if (StringUtils.isNotEmpty(search_date_start) && StringUtils.isNotEmpty(search_date_end)) {
                Date date1 = DateUtils.parseDate(search_date_start, "yyyy-MM-dd");
                Date date2 = DateUtils.parseDate(search_date_end, "yyyy-MM-dd");
                date2 = DateUtils.addDays(date2, 1);

                and.andBookTimeBetween(date1, date2);

                if (search_order_status != 0) {
                    and.andStateEqualTo(search_order_status);
                }

                and.andSettleStatusEqualTo(0);
                //进行查询处理
                SettleInfoVO settleInfo = orderService.queryOrderSettleInfo(order,hotelId);
                logger.info("查询结算信息：{}",JsonUtils.objectToJson(settleInfo));
                return settleInfo;
            }
        } catch (ParseException e){
            logger.error("查询订单失败",e);
            return null;
        }

        return  null;
    }

    @ResponseBody
    @RequestMapping("/updateBank/indentidyCode")
    public Result billInfo(
            @RequestParam(value="bankSignPhone", required=true) String bankSignPhone) {

        if(StringUtils.isBlank(bankSignPhone)){
            return  new Result().failed("签约手机号不存在");
        }

        //生成指定位数的验证码
        String code = SMSCodeGenerator.sms4Place();

        String content = "【秒宿乐易换】分销系统验证码："+code+",您正在使用该验证码进行秒宿分销系统修改结算银行账号信息的操作，非本人操作请联系秒宿分销系统相关人员。";
        //发送短信
        logger.info("更改银行卡信息的验证码：{}",code);
        com.yunpian.sdk.model.Result<SmsSingleSend> result = SmsUtil.sendMsg(bankSignPhone,content);
        if(result.getCode() == Code.OK){
            return new Result().success(code);
        }
        return  new Result().failed("短信发送失败");
    }

    @ResponseBody
    @RequestMapping("/settleMsgPhone/delete")
    public boolean phoneDelete(Long settleId,String phone) {
        return hotelSettleService.settleMsgPhoneDelete(settleId,phone);
    }


    private Long getFirstHotelId(Long currentUserId){
        List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId);
        if(hotelRefUsers.size()>0){
            return hotelRefUsers.get(0).getHotelId();
        }
        return null;
    }

    private String compluteDateEnd(String search_date_start, Long hotelId) {
        Integer cycleType = hotelSettleService.queryCycleSetting(hotelId);

        try {
            Date date_start =  DateUtils.parseDate(search_date_start,"yyyy-MM-dd");
            Date prevDate = null;
            if(cycleType==1){
                //天
                prevDate = DateUtils.addDays(date_start, 0);
            }
            if(cycleType==2){
                //周
                prevDate = DateUtils.addDays(date_start, 6);
            }
            if(cycleType==3){
                //半月
                prevDate = DateUtils.addDays(date_start, 14);
            }
            if(cycleType==4){
                //月
                prevDate = DateUtils.addDays(date_start, 29);
            }
            return DateFormatUtils.format(prevDate,"yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return search_date_start;
    }

}
