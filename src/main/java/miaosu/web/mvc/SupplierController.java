package miaosu.web.mvc;

import com.google.common.base.Strings;
import miaosu.dao.model.Business;
import miaosu.dao.model.HotelSupplier;
import miaosu.dao.model.Picture;
import miaosu.dao.model.User;
import miaosu.svc.hotel.HotelSupplierService;
import miaosu.svc.user.BusinessService;
import miaosu.svc.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @Date: 2021/1/30 11:38
 * @Description: 供应商管理
 */
@Controller
@RequestMapping("/supplier")
public class SupplierController extends BaseController {

	@Autowired
	private BusinessService businessService;

	@Autowired
	private HotelSupplierService hotelSupplierService;
//	//查询所有供应商信息
//	@ResponseBody
//	@RequestMapping("list")
//	public PageResult SupplierList(@RequestParam(value="offset", required=false,defaultValue = "1") int offset,
//								   @RequestParam(value="limit", required=false,defaultValue = "10") int limit){
//		//查询所有公司的详细信息(包括图片)
//		PageResult SupplierList = userService.quertSupplierList(offset,limit);
//		return SupplierList;
//	}

	//查询所有公司的详细信息(包括图片)
	@ResponseBody
	@RequestMapping("list")
	public Response SupplierList(@RequestParam(value="page", required=false,defaultValue = "1") int page,
								 @RequestParam(value="limit", required=false,defaultValue = "10") int limit){
		//通过分页查询所有企业信息
		List<Business> businessList = businessService.queryAllBusinessByPage(page,limit);
		//查询公司信息总条数
		int count= businessService.queryTotalBusiness();
		return new Response().success("查询所有公司信息成功",count,businessList);
	}
	//查询所有的供应商的简单信息
	@ResponseBody
	@RequestMapping("simpleList")
	public Response simpleList(){
		Map<Long,String> map=new HashMap<>();
		List<Business> businessList =  userService.queryBusinessInsertList();
		for (Business business : businessList) {
			map.put(business.getBusiness_id(),business.getBusiness_name());
		}
		return new Response().success("查询所有供应商",map.size(),map);
	}




	//添加按照以前的流程

	//修改供应商信息
	@ResponseBody
	@RequestMapping("update")
	public Response UpdateSupplier(@RequestParam(value="business_id") Long business_id,
								   @RequestParam(value="business_name") String business_name,
								   @RequestParam(value="legal_person") String legal_person,
								   @RequestParam(value="legal_phone") String legal_phone,
								   @RequestParam(value="business_phone") String business_phone,
								   @RequestParam(value="business_email") String business_email,
								   @RequestParam(value="business_address") String business_address,
								   @RequestParam(value="business_type") Long business_type,
								   @RequestParam(value="businessOrFactory") Long businessOrFactory,
								   @RequestParam(value="finance_person") String finance_person,
								   @RequestParam(value="finance_phone") String finance_phone,
								   @RequestParam(value="bank_type") String bank_type,
								   @RequestParam(value="bank_number") String bank_number,
								   @RequestParam(value="bank_addresss") String bank_addresss,
								   @RequestParam(value="account_name") String account_name,
								   @RequestParam(value="account_number") String account_number,
								   @RequestParam(required = false) MultipartFile[] files) throws Exception{

		if (null == business_id){
			return new Response().failed("企业id不能为空");
		}
		else if (Strings.isNullOrEmpty(business_name)){
			return new Response().failed("企业名称不能为空");
		}
		else if (Strings.isNullOrEmpty(legal_person)){
			return new Response().failed("法人不能为空");
		}
		else if (Strings.isNullOrEmpty(legal_phone)){
			return new Response().failed("法人联系方式不能为空");
		}
		else if (Strings.isNullOrEmpty(business_phone)){
			return new Response().failed("固定电话不能为空");
		}
		else if (Strings.isNullOrEmpty(business_email)){
			return new Response().failed("电子邮件不能为空");
		}
		else if (Strings.isNullOrEmpty(business_address)){
			return new Response().failed("公司地址不能为空");
		}
		else if (null == business_type){
			return new Response().failed("行业类别不能为空");
		}
		else if (null == businessOrFactory){
			return new Response().failed("经销商/厂家不能为空");
		}
		else if (Strings.isNullOrEmpty(finance_person)){
			return new Response().failed("财务负责人不能为空");
		}
		else if (Strings.isNullOrEmpty(finance_phone)){
			return new Response().failed("财务联系电话不能为空");
		}
		else if (Strings.isNullOrEmpty(bank_type)){
			return new Response().failed("开户银行不能为空");
		}
		else if (Strings.isNullOrEmpty(bank_number)){
			return new Response().failed("行号不能为空");
		}
		else if (Strings.isNullOrEmpty(bank_addresss)){
			return new Response().failed("开户行地址不能为空");
		}
		else if (Strings.isNullOrEmpty(account_name)){
			return new Response().failed("开户名称不能为空");
		}
		else if (Strings.isNullOrEmpty(account_number)){
			return new Response().failed("账号不能为空");
		}
		Business business = businessService.queryBusinessByBusiness_id(1l);
		Long user_id = business.getUser_id();

		//修改图片到数据库
		Picture picture = new Picture();
		//先删除之前的图片,再修改新增成新的图片
		Picture picture1 = businessService.queryPicture(user_id);
		if (null==picture1){
			//如果没有查到图片,则直接新增
			picture.setUser_id(user_id);
			for(int i=0;i<files.length;i++) {
				MultipartFile file=files[i];
				//图片以二进制存储数据库
				byte[] bytes = file.getBytes();
				if(i==0) {
					if (bytes.length==0){
						return new Response().failed("营业执照必须上传");
					}
					picture.setImg_businessLicense(bytes);
				} else if(i==1){
					if (bytes.length==0){
						return new Response().failed("店铺门头必须上传");
					}
					picture.setImg_shopHead(bytes);
				}else  if(i==2){
					if (bytes.length==0){
						return new Response().failed("授权书必须上传");
					}
					picture.setImg_factoryBook(bytes);
				}
			}
			businessService.saveImage(picture);
		}else {
			//查到了图片,先删除,再新增
			businessService.deletePicture(user_id);
			picture.setUser_id(user_id);
			for(int i=0;i<files.length;i++) {
				MultipartFile file=files[i];
				//图片以二进制存储数据库
				byte[] bytes = file.getBytes();
				if(i==0) {
					if (bytes.length==0){
						return new Response().failed("营业执照必须上传");
					}
					picture.setImg_businessLicense(bytes);
				} else if(i==1){
					if (bytes.length==0){
						return new Response().failed("店铺门头必须上传");
					}
					picture.setImg_shopHead(bytes);
				}else  if(i==2){
					if (bytes.length==0){
						return new Response().failed("授权书必须上传");
					}
					picture.setImg_factoryBook(bytes);
				}
			}
			businessService.saveImage(picture);
		}

		//修改公司信息
		business.setBusiness_name(business_name);
		business.setLegal_person(legal_person);
		business.setLegal_phone(legal_phone);
		business.setBusiness_phone(business_phone);
		business.setBusiness_email(business_email);
		business.setBusiness_address(business_address);
		business.setBusiness_type(business_type);
		business.setBusinessOrFactory(businessOrFactory);
		business.setFinance_person(finance_person);
		business.setFinance_phone(finance_phone);
		business.setBank_type(bank_type);
		business.setBank_number(bank_number);
		business.setBank_addresss(bank_addresss);
		business.setAccount_name(account_name);
		business.setAccount_number(account_number);
		businessService.updateBusiness(business);
		return new Response().success("修改供应商信息成功");
	}



		//删除供应商信息


	//查询供应商登录信息(账号密码等等)和关联的酒店(hotel_supplier表中所关联的)的信息(佣金/开始时间/结束时间/置换金额/置换间夜数/状态(待上线/已上线/下线))
	//查询供应商账号信息
	@ResponseBody
	@RequestMapping("supplierInfo")
	public Response SupplierInfo(@RequestParam(value="business_id") Long business_id){
		if (null == business_id){
			return new Response().failed("企业id不能为空");
		}
		Business business = businessService.queryBusinessByBusiness_id(business_id);
		User user=null;
		if (null!=business){
			Long user_id = business.getUser_id();
			user = userService.getUserById(user_id);
		}else {
			return new Response().failed("不存在此供应商");
		}
		return new Response().success("查询供应商账号信息",1,user);
	}
	//查询供应商关联的酒店
	@ResponseBody
	@RequestMapping("supplier_hotel")
	public Response Supplier_hotel(@RequestParam(value="business_id") Long business_id,
								   @RequestParam(value="page", required=false,defaultValue = "1") int page,
								   @RequestParam(value="limit", required=false,defaultValue = "10") int limit){
		if (null == business_id){
			return new Response().failed("企业id不能为空");
		}
		//根据供应商id查询名下关联酒店集合列表
		List<HotelSupplier> hotelSupplierList =hotelSupplierService.querySupplierListByBusiness_id(business_id,page,limit);
		//查询与本供应商有关的hotel_supplier中酒店总条数
		int count=hotelSupplierService.queryTotalSupplierListByBusiness_id(business_id);
		return new Response().success("查询供应商关联的酒店列表成功",count,hotelSupplierList);
	}
}
