package miaosu.web.mvc;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.yunpian.sdk.constant.Code;
import com.yunpian.sdk.model.SmsBatchSend;
import miaosu.common.BankUseTypeEnum;
import miaosu.dao.model.*;
import miaosu.sms.SmsUtil;
import miaosu.svc.invoice.HotelInvoiceService;
import miaosu.svc.invoice.InvoiceRecordService;
import miaosu.svc.order.OrderService;
import miaosu.svc.settle.HotelSettleService;
import miaosu.svc.vo.*;
import miaosu.utils.ExcelUtil;
import miaosu.utils.JsonUtils;
import miaosu.utils.SMSCodeGenerator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by xieqx on 2019/1/20.
 */

@Controller
@RequestMapping("hotel/invoice")
public class InvoiceController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private HotelInvoiceService invoiceService;

    @Autowired
    private InvoiceRecordService invoiceRecordService;

    @Autowired
    private OrderService orderService;

    /**
     * 进行发票管理的首页
     * @param hotelId
     * @return
     */
    @ResponseBody
    @RequestMapping("/index")
    public Result set(long hotelId) {

        HotelInfo hotel = checkHotelInfoWithPermit(hotelId);

        BInvoice bInvoice = invoiceService.queryHotelInvoice(hotelId);

        InvoiceVO invoiceVO = new InvoiceVO();
        invoiceVO.setHotelid(hotelId);
        invoiceVO.setHotelName(hotel.getHotelName());
        if(bInvoice != null){
            BeanUtils.copyProperties(bInvoice,invoiceVO);
        }
        logger.info("result: {}",JsonUtils.objectToJson(invoiceVO));
        return new Result().success(invoiceVO);
    }


    /**
     * 结算发票信息的更新/新增
     * @param bInvoice
     * @return
     */
    @ResponseBody
    @RequestMapping("/update")
    public Result settleUpdate(BInvoice bInvoice) {
        //HotelInfo hotel = checkHotelInfoWithPermit(settleSetting.getHotelId());

        boolean flag = invoiceService.updateInvoiceInfo(bInvoice);
        if(flag){
            return new Result().success();
        }

        return new Result().failed("设置发票信息失败");

    }


    /**
     * 电子发票生成并发送
     * @param billId 结算账单id
     * @return
     */
    @ResponseBody
    @RequestMapping("/genertorAndSend")
    public Result genertorAndSend(@RequestParam(value="billId", required=true) Long billId) {

        Long hotelId = getCurHotel();
        HotelInfo hotelInfo = checkHotelInfoWithPermit(hotelId);
        if(billId == null ){
            return new Result().failed("缺少必要的参数");
        }

        boolean isAdmin = isAdmin();

        boolean flag =  invoiceService.generatorAndSend(billId,hotelId,isAdmin);

        if(flag){
            return new Result().success();
        }
        return new Result().failed("生成发票失败");

    }


    /**
     * 查询结算的发票信息
     * @param search_hotel_id 酒店id
     * @param search_date_start 查询开始日期
     * @param search_date_end 查询结束日期
     * @param offset
     * @param limit
     * @return
     * http://localhost:8080/settle/bill/list
     * ?order=asc&offset=0&limit=10
     * &search_date_start=2019-01-24
     * &search_date_end=2019-01-25
     * &search_settle_status=-1
     * &search_hotel_id=30
     */
    @ResponseBody
    @RequestMapping("list")
    public PageResult getAll(
            @RequestParam(value="search_hotel_id", required=false) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_date_end", required=true) String search_date_end,
            @RequestParam(value="offset", required=false,defaultValue = "1") int offset,
            @RequestParam(value="limit", required=false,defaultValue = "10") int limit) {

        PageResult pageResult = null;
        if(search_hotel_id == null){
            search_hotel_id = getCurHotel();
        }

        boolean isAdmin = isAdmin();

        pageResult = invoiceRecordService.queryInvoiceListByPage(search_hotel_id,search_date_start,search_date_end,isAdmin,offset,limit);

        logger.info("pageResult : {}",JsonUtils.objectToJson(pageResult));

        return  pageResult;
    }

    /**
     * 导出记账信息
     * @param search_hotel_id
     * @param search_date_start
     * @param search_date_end
     * @return
     */
    @RequestMapping("exportExcel")
    public void exportExcel(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_date_end", required=true) String search_date_end,
            HttpServletResponse response) {

        boolean isAdmin = isAdmin();
        if(search_hotel_id == null){
            return ;
        }

        HotelInfo hotelInfo = checkHotelInfoWithPermit(search_hotel_id);
        List<Map<String,Object>> hotelSettleBillVOS = invoiceRecordService.queryRecordListForExcel(search_hotel_id, search_date_start, search_date_end, isAdmin);

        String exportData = null;
        int maxLen = (int) (35.7*300);
        int defaultlen = (int) (35.7 * 140);
        int numlen = (int) (35.7 * 100);
        Integer[] columnsLen = null;

        exportData = "[{'colkey':'invoiceDate','name':'开票时间'},{'colkey':'invoiceContent','name':'开票内容'},"
                    + "{'colkey':'invoiceUnit','name':'开票单位'},{'colkey':'invoiceCode','name':'发票代码'},"
                    + "{'colkey':'invoiceNo','name':'发票号码'},{'colkey':'createTime','name':' 创建时间'}"
                    + "]";
        columnsLen = new Integer[]{defaultlen, maxLen,
                    defaultlen, defaultlen,
                    numlen, numlen
        };


        String fileName = hotelInfo.getHotelName()+search_date_start+"-"+search_date_end+"账单信息";
        List<Map<String, Object>> parseJSONList = JsonUtils.parseJSONList(exportData);

        ExcelUtil.exportToExcel(response, parseJSONList, hotelSettleBillVOS, fileName, columnsLen);
    }


}
