package miaosu.web.mvc.admin;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import miaosu.dao.auto.UserRoleMapper;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelOrder;
import miaosu.dao.model.HotelRoomSet;
import miaosu.svc.hotel.HotelRoomCountsService;
import miaosu.svc.hotel.HotelRoomSetService;
import miaosu.svc.hotel.HotelRoomStatusService;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.order.OrderMsgService;
import miaosu.svc.order.OrderService;
import miaosu.svc.vo.Result;
import miaosu.web.mvc.BaseController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiajun.chen on 2018/1/9.
 */
@Controller
@RequestMapping("/admin")
public class OrderManagerController extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(OrderManagerController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderMsgService orderMsgService;

    @Autowired
    private MsgNotifyService msgNotifyService;


    @Autowired
    private HotelRoomCountsService hotelRoomCountsService;

    @Autowired
    private HotelRoomStatusService hotelRoomStatusService;
    @Autowired
    private HotelRoomSetService hotelRoomSetService;
    @Autowired
    private UserRoleMapper userRoleMapper;

    @GetMapping("order")
    public String order(Model model) {

        Date now = new Date();
        Date preday7 = DateUtils.addDays(now, -7);
        String date_end = DateFormatUtils.format(now,"yyyy-MM-dd");
        String date_start = DateFormatUtils.format(preday7,"yyyy-MM-dd");
        model.addAttribute("search_date", ImmutableMap.of("date_start",date_start,"date_end",date_end));
        Long currentUserId = getCurrentUserId();
        //根据当前用户id查询对应角色,并返给前端
        List<Long> longs = userRoleMapper.queryUser_Role(currentUserId);
        model.addAttribute("channel",longs.get(0));

        //如果是超级管理员,首页进入charts页面,而供应商进入admin/order首页,酒店进入redirect:/order首页
//        if (isSuperAdmin()){
//            return "/charts/index";
//        }else {
            if(isAdmin()){
                return "admin/order";
            }else {
                return "redirect:/order";
            }
//        }

    }


    @ResponseBody
    @RequestMapping("order/add")
    public Result hotelAdd(@RequestParam(value="hotel_id_selected", required=true) Long hotel_id_selected,
                           @RequestParam(value="sell_type", required=true) int sell_type,
                           @RequestParam(value="hotelRoomTypeId", required=true) Long hotelRoomTypeId,
                           @RequestParam(value="orderRoomCount", required=true) int orderRoomCount,
                           @RequestParam(value="orderPrice", required=true) String orderPrice,
                           @RequestParam(value="bookTime", required=true) String bookTime,
                           @RequestParam(value="checkInTime", required=true) String checkInTime,
                           @RequestParam(value="checkOutTime", required=true) String checkOutTime,
                           @RequestParam(value="bookUser", required=true) String bookUser,
                           @RequestParam(value="channelOrderNo", required=true) String channelOrderNo,
                           @RequestParam(value="orderChannel", required=true) int orderChannel,
                           @RequestParam(value="orderState", required=true) int orderState,
                           @RequestParam(value="bookRemark", required=true) String bookRemark,
                           HttpServletRequest request,
                           Model model)throws Exception{
        if (Strings.isNullOrEmpty(channelOrderNo)){
            return new Result().failed("渠道订单号不能为空");
        } else if (Strings.isNullOrEmpty(bookTime)){
            return new Result().failed("预定时间不能为空");
        } else if (Strings.isNullOrEmpty(checkInTime)){
            return new Result().failed("入店时间不能为空");
        }else if (Strings.isNullOrEmpty(checkOutTime)){
            return new Result().failed("离店时间不能为空");
        }else if (Strings.isNullOrEmpty(bookUser)){
            return new Result().failed("预订人不能为空");
        }
        else if (null == hotel_id_selected){
            return new Result().failed("必须选择酒店");
        }else if (null == hotelRoomTypeId) {
            return new Result().failed("预定房型必须选择");
        }else if (Strings.isNullOrEmpty(orderPrice)){
            return new Result().failed("订单价格不能为空");
        }
        else if (Strings.isNullOrEmpty(bookRemark)){
            return new Result().failed("备注不能为空,信息必须包含 (含双早) 或者 (不含早)");
        }

        logger.debug("add order {}", request.getParameterMap());
        HotelOrder order = new  HotelOrder();

        try{
            BigDecimal price = new BigDecimal(orderPrice);
            price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
            order.setPrice(price);
        }catch (Exception e){
            return new Result().failed("订单价格格式错误");
        }

        order.setHotelRoomTypeId(hotelRoomTypeId);
        //根据房型id查询酒店房型设置的所有信息,获取平台酒店房型名字
        HotelRoomSet hotelRoomSet=hotelRoomSetService.getHotelRoomTypeById(hotelRoomTypeId);
        String p_hotel_room_type_name = hotelRoomSet.getHotelRoomTypeName();
        order.setP_hotel_room_type_name(p_hotel_room_type_name);
        //1.先根据hotel_room_type_id到hotel_price_ref_room_type表中查询hotel_price_id,多个
        List<Long> hotel_price_idList=orderService.queryhotel_price_id(hotelRoomTypeId);
        Integer breakfast=0;
        BigDecimal caigou_one = new BigDecimal(0);
        if (!bookRemark.contains("早")){
            return new Result().failed("备注信息必须包含 (含早) 或者 (不含早)");
        }
        if (bookRemark.contains("不含早")||bookRemark.contains("无早")){
            breakfast=0;
        }else {
            breakfast=2;
        }
        //2.在根据hotel_price_id和hotel_id和breakfast_num的拼接查询caigou_one
        for (Long hotel_price_id : hotel_price_idList) {
            caigou_one=orderService.querycaigou_one(hotel_price_id,hotel_id_selected,breakfast);
            if(caigou_one!=null){
                //每单采购价
                order.setCaigou_one(caigou_one);
                //总采购价=每单采购价*间夜*房间数,可以连乘
                //获取间夜
                //字符串转日期,括号中的格式必须和字符串日期格式一致
                Date date_in = DateUtils.parseDate(checkInTime, "yyyy-MM-dd");
                Date date_out = DateUtils.parseDate(checkOutTime, "yyyy-MM-dd");
                long days = getDays(date_in, date_out);
                BigDecimal roomNum = BigDecimal.valueOf(orderRoomCount);
                BigDecimal night = BigDecimal.valueOf(days);
                BigDecimal caigou_total = new BigDecimal(0);
                caigou_total=caigou_one.multiply(night).multiply(roomNum);
                order.setCaigou_total(caigou_total);
                break;
            }else {
            	//没有就设置为0,要不其他操作的展示会报错(因为在添加订单的时候可能有的人不知道此房型有没有设置早餐规则)
                order.setCaigou_one(BigDecimal.valueOf(0));
                order.setCaigou_total(BigDecimal.valueOf(0));
                System.out.println("没有设置 置换结算价");
            }
        }



        order.setBookMobile(null);
        try {
            order.setBookTime(DateUtils.parseDate(bookTime,"yyyy-MM-dd HH:mm"));
        }catch (Exception e){
            return new Result().failed("预定时间格式错误");
        }
        try {
            order.setCheckInTime(DateUtils.parseDate(checkInTime,"yyyy-MM-dd"));
        }catch (Exception e){
            return new Result().failed("入住日期格式错误");
        }
        try {
            order.setCheckOutTime(DateUtils.parseDate(checkOutTime,"yyyy-MM-dd"));
        }catch (Exception e){
            return new Result().failed("离店日期格式错误");
        }

        order.setHotelId(hotel_id_selected);
        order.setBookUser(bookUser);
        order.setOrderChannel(orderChannel);
        order.setOrderNo(channelOrderNo);
        order.setSellType(sell_type);
        order.setState(orderState);
        order.setRoomCount(orderRoomCount);
        order.setBookRemark(bookRemark);

        Date date_in = DateUtils.parseDate(checkInTime, "yyyy-MM-dd");
        Date date_out = DateUtils.parseDate(checkOutTime, "yyyy-MM-dd");
        long days = getDays(date_in, date_out);
        long nightC = days * orderRoomCount;
        //间夜数
        order.setNightC(Math.toIntExact(nightC));

        //售卖时间,引用预定时间(房态和房量都能用到)
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //几号入住就减几号的房间数量,而不是几号预定就减去几号的,有的客人1号预定,6号入住,应该减去6号的房间数量
        Date sell_date = sdf.parse(checkInTime);


        /**
         * 添加订单后房量的变化
         */
        //查询数据库的总房量
        Integer countRoom= hotelRoomCountsService.findCountRoom(hotelRoomTypeId,sell_date);
        if (countRoom!=null) {
            System.out.println("==========总房量=========================" + countRoom);

            //如果剩余房量<=0,就操作关房并打电话给OTA管家,这里的房量对应的指的是订单的入住时间
            if (countRoom<=0){
                System.out.println("房间数量小于等于0了,要关房了");
                Map<String,String> params = new HashMap<String, String>(8);
                //根据酒店id查询酒店平台id
                String hotel_only_id= hotelService.queryHotelOnlyId(hotel_id_selected);
                params.put("hotel_only_id", hotel_only_id);
                HotelInfo hotelInfo= hotelService.getHotelInfoById(hotel_id_selected);
                String hotelName = hotelInfo.getHotelName();
                String ota_phone = hotelInfo.getOta_phone();
                params.put("hotelName",hotelName);
                params.put("setStartTime",checkInTime);
                params.put("setEndTime",checkInTime);
                params.put("roomStatus", String.valueOf(0));
                params.put("roomTypeName",p_hotel_room_type_name);

                String url1 = "http://192.168.3.4:8080/dlt_close_room";//代理通
                String url2 = "http://192.168.3.4:8080/tc_close_room";//同程
//            String url1 = "http://61.52.98.214/dlt_close_room";//代理通
//            String url2 = "http://61.52.98.214/tc_close_room";//同程
                sendRoomStatus(params,url1);
                sendRoomStatus(params,url2);
                //打电话给OTA管家
                msgNotifyService.sendUserSms(ota_phone,params);
                //短信通知OTA管家
                msgNotifyService.sendUserSms2(ota_phone,params);
            }


            int roomCount = countRoom - orderRoomCount;
            //更新房量信息
            hotelRoomCountsService.update(hotelRoomTypeId, sell_date, roomCount);
            long orderId = orderService.addOrder(order);
            //添加到数据库的临时表hotel_getOrder
            orderService.addGetOrder(order);
            msgNotifyService.sendMsg(hotel_id_selected, order);

            //TODO 发送酒店信息
            orderMsgService.sendMsgByOrderId(orderId);
            return new Result().success();
        }else {
            long orderId = orderService.addOrder(order);
            //添加到数据库的临时表hotel_getOrder
            orderService.addGetOrder(order);
            msgNotifyService.sendMsg(hotel_id_selected, order);

            //TODO 发送酒店信息
            orderMsgService.sendMsgByOrderId(orderId);
            return new Result().success();
        }

    }
    private long getDays(Date startDate, Date endDate) {
        if (startDate != null && endDate != null) {
            SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            Date mydate = null;

            try {
                mydate = myFormatter.parse(myFormatter.format(startDate));
                date = myFormatter.parse(myFormatter.format(endDate));
            } catch (Exception var7) {
            }

            long day = (date.getTime() - mydate.getTime()) / 86400000L;
            return day;
        } else {
            return 0L;
        }
    }

    //将关房数据传递给爬虫人员,让他主动操作关房
    public void sendRoomStatus(Map<String,String> params,String url){
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            String hotel_only_id = params.get("hotel_only_id");//酒店所有平台id:
            String hotelName = params.get("hotelName");//酒店名字:海旺弘亚温泉酒店
            String setStartTime = params.get("setStartTime");//开始年月日:2020-7-29
            String setEndTime = params.get("setEndTime");//结束年月日:2020-8-1
            String roomTypeName = params.get("roomTypeName");//房间类型:豪华标准间
            String roomStatus = params.get("roomStatus");//开关房状态

            MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
            map.add("hotel_only_id", hotel_only_id);
            map.add("hotelName", hotelName);
            map.add("setStartTime", setStartTime);
            map.add("setEndTime", setEndTime);
            map.add("roomTypeName", roomTypeName);
            map.add("roomStatus",roomStatus);

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.postForEntity( url, request , String.class );
            System.out.println(response.getBody());
        } catch (RestClientException e) {
            //失败不作任何处理,不影响自己的业务
            System.out.println("关房系统没开,不做任何处理");
            return;
        }
    }

}
