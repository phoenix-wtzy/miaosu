package miaosu.web.mvc.admin;

import com.google.common.collect.ImmutableMap;
import miaosu.common.CycleSettleEnum;
import miaosu.dao.model.HotelSettleSetting;
import miaosu.svc.settle.HotelSettleService;
import miaosu.svc.vo.HotelSettleSettingVO;
import miaosu.web.mvc.BaseController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * Created by jiajun.chen on 2018/1/9.
 */
@Controller
@RequestMapping("/admin/hotel")
public class SettleManagerController extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(SettleManagerController.class);

    @Autowired
    private HotelSettleService settleService;


    @GetMapping("settle")
    public String settle(Model model) {

        //获取当前酒店信息，根据当前酒店信息获取其中的佣金设置和结算周期
        Long hotelId =  getCurHotel();
        HotelSettleSetting settleSetting = settleService.queryHotelSettle(hotelId);
        if(settleSetting == null){
            return "error/lackSettle";
        }
        HotelSettleSettingVO settleSettingVO = new HotelSettleSettingVO();

        BeanUtils.copyProperties(settleSetting,settleSettingVO);
        int cycleType = settleSetting.getSettleCycleSetting();
        //TODO 确保添加操作
        settleSettingVO.setSettleCycleSettingName(CycleSettleEnum.fromCode(settleSetting.getSettleCycleSetting()).getName());

        Date now = new Date();
        Date prevDate = null;
        if(cycleType==1){
            //天
            prevDate = DateUtils.addDays(now, 0);
        }
        if(cycleType==2){
            //周
            prevDate = DateUtils.addDays(now, -6);
        }
        if(cycleType==3){
            //半月
            prevDate = DateUtils.addDays(now, -14);
        }
        if(cycleType==4){
            //月
            prevDate = DateUtils.addDays(now, -29);
        }
        String date_start = DateFormatUtils.format(prevDate,"yyyy-MM-dd");

        model.addAttribute("search_date", ImmutableMap.of("date_start",date_start));
        model.addAttribute("settleSetting",settleSettingVO);
        Long currentUserId = getCurrentUserId();
        model.addAttribute("channel",currentUserId);

        if(isAdmin()){
            return "admin/settle";
        }else {
            return "redirect:/hotel/settle";
        }

    }
}
