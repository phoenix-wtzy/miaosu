package miaosu.web.mvc.admin;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Strings;
import miaosu.common.ChannelEnum;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.Province;
import miaosu.runtimeexception.ExistException;
import miaosu.svc.address.ProvinceService;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.vo.HoteInfoVO;
import miaosu.svc.vo.HotelCheckVO;
import miaosu.svc.vo.PageResult;
import miaosu.svc.vo.Result;
import miaosu.utils.JsonUtils;
import miaosu.web.mvc.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiajun.chen on 2018/1/9.
 */
@Controller
@RequestMapping("/admin")
public class HotelManagerController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(HotelManagerController.class);

	@Autowired
	private HotelService hotelService;
	@Autowired
	private ProvinceService provinceService;


	@GetMapping("hotel")
	public String hotel(Model model) {
		Map<Integer, String> channelMap = new HashMap<Integer, String>(6);

		for (ChannelEnum channelEnum : ChannelEnum.values()) {
			channelMap.put(channelEnum.getCode(), channelEnum.getName());
		}

		List<Province> provinces = provinceService.findProvinceList(null);
		model.addAttribute("provinces", provinces);
		//不要渠道信息了
		model.addAttribute("channelMap", channelMap);

		logger.info("渠道信息：{}  省份信息：{}", JSON.toJSONString(channelMap), JSON.toJSONString(provinces));
		return "admin/hotel";
	}

	@ResponseBody
	@RequestMapping("hotel/list")
	public PageResult hotelList(HttpServletRequest request,
								@RequestParam(value = "offset", required = false, defaultValue = "1") int offset,
								@RequestParam(value = "limit", required = false, defaultValue = "20") int limit) {

		Long currentUserId = getCurrentUserId();
//        HotelInfoExample example = new HotelInfoExample();

		PageResult hotelList = hotelService.queryHotelListBypage(currentUserId, offset, limit);
		logger.info("酒店系统:{}", JsonUtils.objectToJson(hotelList));
		return hotelList;
	}

//    @ResponseBody
//    @RequestMapping("hotel/search/name")
//    public PageResult hotelList(HttpServletRequest request,
//                                @RequestParam(value="name", required=false,defaultValue = "") String name,
//                                @RequestParam(value="offset", required=false,defaultValue = "1") int offset,
//                                @RequestParam(value="limit", required=false,defaultValue = "20") int limit){
//
//        HotelInfoExample example = new HotelInfoExample();
//        if(!Strings.isNullOrEmpty(name)){
//            example.createCriteria().andHotelNameLike("%"+name+"%");
//        }
//        PageResult hotelList = hotelService.queryHotelListBypage(example,offset,limit);
//        return hotelList;
//    }

	@ResponseBody
	@RequestMapping("hotel/add")
	public Result hotelAdd(@RequestParam(value = "hotelName", required = true) String hotelName,
						   @RequestParam(value = "hotel_only_id", required = true) String hotel_only_id,
						   @RequestParam(value = "contact", required = true) String contact,
						   @RequestParam(value = "contactMobile", required = true) String contactMobile,
						   @RequestParam(value = "receptionPhone", required = true) String receptionPhone,
						   @RequestParam(value = "ota_phone", required = true) String ota_phone,
						   @RequestParam(value = "address", required = true) String address,
						   @RequestParam(value = "accountName", required = true) String accountName,
						   @RequestParam(value = "accountPassword", required = true) String accountPassword,
						   @RequestParam(value = "accountNickName", required = true) String accountNickName,
						   @RequestParam(value = "hotelType", required = true) int hotelType,//销售类型,1--a类  2--b类
						   @RequestParam(value = "settle_totalMoney") String settle_totalMoney,//协议置换总金额
						   @RequestParam(value = "settle_totalNight") String settle_totalNight,//协议置换间夜数
						   @RequestParam(value = "baoFang_totalMoney") String baoFang_totalMoney,//包房总金额
						   @RequestParam(value = "touZiRen_rates") String touZiRen_rates,//投资人分润比例
						   @RequestParam(value = "provinceCode", required = true) String provinceCode,
						   @RequestParam(value = "cityCode", required = true) String cityCode,
						   @RequestParam(value = "districtCode", required = true) String districtCode,
						   //@RequestParam(value="channels", required=true) Integer[] channels,
						   Model model) {

		if (Strings.isNullOrEmpty(hotelName)) {
			return new Result().failed("酒店名称不能为空");
		} else if (Strings.isNullOrEmpty(hotel_only_id)) {
			return new Result().failed("酒店平台ID不能为空");
		} else if (Strings.isNullOrEmpty(contact)) {
			return new Result().failed("联系人不能为空");
		} else if (Strings.isNullOrEmpty(contactMobile)) {
			return new Result().failed("联系人电话不能为空");
		} else if (Strings.isNullOrEmpty(receptionPhone)) {
			return new Result().failed("前台电话不能为空");
		} else if (Strings.isNullOrEmpty(address)) {
			return new Result().failed("酒店地址不能为空");
		} else if (Strings.isNullOrEmpty(accountName)) {
			return new Result().failed("管理员账号不能为空");
		} else if (Strings.isNullOrEmpty(accountPassword)) {
			return new Result().failed("管理员密码不能为空");
		} else if (Strings.isNullOrEmpty(accountNickName)) {
			return new Result().failed("管理员名称不能为空");
		}
//        else if (Strings.isNullOrEmpty(user_phone)) {
//            return new Result().failed("用户电话不能为空");
//        }
//        else if (Strings.isNullOrEmpty(ota_phone)) {
//            return new Result().failed("OTA管家电话不能为空");
//        }

//        else if (Strings.isNullOrEmpty(accountPassword)){
//            return new Result().failed("管理密码不能为空");
//        }
		else if (Strings.isNullOrEmpty(provinceCode)) {
			return new Result().failed("省份编码不能为空");
		} else if (Strings.isNullOrEmpty(cityCode)) {
			return new Result().failed("城市编码不能为空");
		} else if (Strings.isNullOrEmpty(districtCode)) {
			return new Result().failed("区域编码不能为空");
		}
		if (hotelType == 1) {
			if (Strings.isNullOrEmpty(settle_totalMoney)) {
				return new Result().failed("协议置换总金额不能为空");
			} else if (Strings.isNullOrEmpty(settle_totalNight)) {
				return new Result().failed("协议置换间夜数不能为空");
			}
		}
		if (hotelType == 2) {
			if (Strings.isNullOrEmpty(baoFang_totalMoney)) {
				return new Result().failed("包房总金额不能为空");
			} else if (Strings.isNullOrEmpty(touZiRen_rates)) {
				return new Result().failed("投资人分润比例不能为空");
			}
		}

		logger.debug("添加酒店:add hotel {} {} {} {} {} {} {} {} {} {} {} {} {} {}", hotelName, hotel_only_id, contact, contactMobile, address, accountName, accountPassword, accountNickName, hotelType,
				provinceCode, cityCode, districtCode, receptionPhone, ota_phone);

		boolean admin = isAdmin();
		Long currentUserId = getCurrentUserId();

		try {
			//新增酒店的时候,添加a类(协议置换总金额/协议置换间夜数)或者b类(包房总金额/投资人分润比例)到酒店信息中hotel_info表,并将酒店的有效性设置为7默认为有效isActive=1
			int isActive = 1;
			hotelService.addHotelAndUser(hotelName, hotel_only_id, address, contact, contactMobile, accountName, accountPassword, accountNickName, admin, currentUserId,
					provinceCode, cityCode, districtCode, receptionPhone, ota_phone, hotelType, settle_totalMoney, settle_totalNight, baoFang_totalMoney, touZiRen_rates, isActive);
		} catch (ExistException e) {
			return new Result().failed(e.getMessage());
		}
		return new Result().success();
	}

	/**
	 * 查找酒店信息
	 *
	 * @param hotelId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("hotel/info")
	public HoteInfoVO updateHotelInfoById(Long hotelId) {

		HoteInfoVO hoteInfoVO = hotelService.queryHotelInfoById(hotelId);
		logger.info("酒店信息：" + JSON.toJSONString(hoteInfoVO));
		return hoteInfoVO;
	}


	/**
	 * 设置酒店的有效性
	 *
	 * @param type 设置酒店类型有效性
	 * @return
	 */
	@ResponseBody
	@RequestMapping("hotel/active")
	public Result activeHotel(Long hotelId, Integer type) {
		if (hotelId == null) {
			return new Result().failed("酒店id不能为空");
		}
		if (type == null) {
			return new Result().failed("是否激活的信息");
		}
		boolean isActive = true;
		int isActive1 = 1;
		if (type == 1) {
			isActive = true;
			isActive1 = 1;
		}
		if (type == 0) {
			isActive = false;
			isActive1 = 0;
		}

		//点击酒店设置的上下架,操作酒店上下线,表hotel_info中的isActive字段
		hotelService.updateHotel_isActive(hotelId, isActive1);

		//获取审核结果
		return new Result().success();
	}

	/**
	 * 查询酒店的审核结果
	 *
	 * @param hotelId 酒店id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("hotel/check")
	public Result checkHotel(Long hotelId) {
		if (hotelId == null) {
			return new Result().failed("酒店id不能为空");
		}
		HotelCheckVO hotelCheckVO = new HotelCheckVO();

		HotelInfo hotelInfo = hotelService.queryHotelInfoById(hotelId);

		hotelCheckVO.setHotelName(hotelInfo.getHotelName());

		//获取审核结果
		return new Result().success().setData(hotelCheckVO);
	}

	@ResponseBody
	@RequestMapping("hotel/update")
	public Result hotelUpdate(@RequestParam(value = "hotelName", required = true) String hotelName,
							  @RequestParam(value = "hotel_only_id", required = true) String hotel_only_id,
							  @RequestParam(value = "contact", required = true) String contact,
							  @RequestParam(value = "contactMobile", required = true) String contactMobile,
							  @RequestParam(value = "receptionPhone", required = true) String receptionPhone,
							  @RequestParam(value = "ota_phone", required = true) String ota_phone,
							  @RequestParam(value = "address", required = true) String address,
							  @RequestParam(value = "accountName", required = true) String accountName,
							  @RequestParam(value = "accountPassword", required = true) String accountPassword,
							  @RequestParam(value = "accountNickName", required = true) String accountNickName,
							  @RequestParam(value = "hotelType", required = true) int hotelType,//销售类型,1--a类置换  2--b类分销
							  @RequestParam(value = "settle_totalMoney") String settle_totalMoney,//协议置换总金额
							  @RequestParam(value = "settle_totalNight") String settle_totalNight,//协议置换间夜数
							  @RequestParam(value = "baoFang_totalMoney") String baoFang_totalMoney,//包房总金额
							  @RequestParam(value = "touZiRen_rates") String touZiRen_rates,//投资人分润比例
							  @RequestParam(value = "hotelId", required = true) Long hotelId,
							  @RequestParam(value = "userId", required = true) Long userId,
							  @RequestParam(value = "provinceCode", required = true) String provinceCode,
							  @RequestParam(value = "cityCode", required = true) String cityCode,
							  @RequestParam(value = "districtCode", required = true) String districtCode,
							  Model model) {
		if (Strings.isNullOrEmpty(hotelName)) {
			return new Result().failed("酒店名称不能为空");
		} else if (Strings.isNullOrEmpty(hotel_only_id)) {
			return new Result().failed("酒店平台ID不能为空");
		} else if (Strings.isNullOrEmpty(contact)) {
			return new Result().failed("联系人不能为空");
		} else if (Strings.isNullOrEmpty(contactMobile)) {
			return new Result().failed("联系人电话不能为空");
		} else if (Strings.isNullOrEmpty(receptionPhone)) {
			return new Result().failed("前台电话不能为空");
		} else if (Strings.isNullOrEmpty(address)) {
			return new Result().failed("酒店地址不能为空");
		} else if (Strings.isNullOrEmpty(accountName)) {
			return new Result().failed("管理员账号不能为空");
		} else if (Strings.isNullOrEmpty(accountPassword)) {
			return new Result().failed("管理员密码不能为空");
		} else if (Strings.isNullOrEmpty(accountNickName)) {
			return new Result().failed("管理员名称不能为空");
		}
//        else if (Strings.isNullOrEmpty(user_phone)) {
//            return new Result().failed("用户电话不能为空");
//        }
//        else if (Strings.isNullOrEmpty(ota_phone)) {
//            return new Result().failed("OTA管家电话不能为空");
//        }
		else if (Strings.isNullOrEmpty(provinceCode)) {
			return new Result().failed("省份编码不能为空");
		} else if (Strings.isNullOrEmpty(cityCode)) {
			return new Result().failed("城市编码不能为空");
		} else if (Strings.isNullOrEmpty(districtCode)) {
			return new Result().failed("区域编码不能为空");
		}
		if (hotelType == 1) {
			if (Strings.isNullOrEmpty(settle_totalMoney)) {
				return new Result().failed("协议置换总金额不能为空");
			} else if (Strings.isNullOrEmpty(settle_totalNight)) {
				return new Result().failed("协议置换间夜数不能为空");
			}
		}
		if (hotelType == 2) {
			if (Strings.isNullOrEmpty(baoFang_totalMoney)) {
				return new Result().failed("包房总金额不能为空");
			} else if (Strings.isNullOrEmpty(touZiRen_rates)) {
				return new Result().failed("投资人分润比例不能为空");
			}
		}
		boolean admin = isAdmin();
		Long currentUserId = getCurrentUserId();
		try {
			hotelService.updateHotelAndUser(hotelId, hotelName, hotel_only_id, address, contact, contactMobile, accountName, accountPassword, accountNickName, admin, currentUserId
					, userId, provinceCode, cityCode, districtCode, receptionPhone, ota_phone, hotelType, settle_totalMoney, settle_totalNight, baoFang_totalMoney, touZiRen_rates);
		} catch (Exception e) {
			return new Result().failed(e.getMessage());
		}
		return new Result().success();
	}
}
