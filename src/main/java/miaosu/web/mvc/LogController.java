package miaosu.web.mvc;

import com.google.common.collect.ImmutableMap;
import miaosu.dao.model.HotelRefUser;
import miaosu.svc.hotel.HotelRoomSetService;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.log.OpLogService;
import miaosu.svc.vo.HotelRoomSetVO;
import miaosu.svc.vo.PageResult;
import miaosu.svc.vo.Result;
import miaosu.utils.JsonUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiajun.chen on 2018/1/9.
 */
@Controller
@RequestMapping("/log")
public class LogController extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(LogController.class);


    @Autowired
    private HotelService hotelService;

    @Autowired
    private HotelRoomSetService roomSetService;

    @Autowired
    private OpLogService opLogService;


    /**
     * 获取日志信息的首页面
     * @param model
     * @return
     */
    @GetMapping("")
    public String log(Model model) {
        //当前用户的第一个酒店信息
       //操作时间/售卖时间 选择其中一个作为默认
        //查询的开始时间和结束时间 默认当前日子作为开始和结束日期
        //房型 不选择
        //操作类型
        Date now = new Date();
        Date preday7 = DateUtils.addDays(now, -7);
        String date_end = DateFormatUtils.format(now,"yyyy-MM-dd");
        String date_start = DateFormatUtils.format(preday7,"yyyy-MM-dd");
        model.addAttribute("search_date", ImmutableMap.of("date_start",date_start,"date_end",date_end));

        //roomset
        Long currentUserId = getCurrentUserId();
        Long hotelId = getFirstHotelId(currentUserId);
        if (hotelId==null){
            model.addAttribute("error", "无操作权限");
            return "error/404";
        }
        List<HotelRoomSetVO> hotelRoomSetVOS  =getRoomIndoByHotelId(hotelId);
        model.addAttribute("roomset", hotelRoomSetVOS);
        return "log";
    }



    /**
     * 查询日志信息
     * @param search_hotel_id 酒店id
     * @param search_date_type 操作时间/售卖时间 1 操作时间 2售卖时间
     * @param search_date_start 开始日期
     * @param search_date_end 结束日期
     * @param room_type_id 房间类型id
     * @param search_op_type 操作类型 1房价，2房量，3开关房
     * @param offset 页码
     * @param limit 页数
     * @return
     */
    @ResponseBody
    @RequestMapping("list")
    public PageResult logPage(
            @RequestParam(value="search_hotel_id", required=true) Long search_hotel_id,
            @RequestParam(value="search_date_type", required=false) Integer search_date_type,
            @RequestParam(value="search_date_start", required=true) String search_date_start,
            @RequestParam(value="search_date_end", required=true) String search_date_end,
            @RequestParam(value="room_type_id", required=false) Long room_type_id,
            @RequestParam(value="search_op_type", required=false) Integer search_op_type,
            @RequestParam(value="offset", required=false,defaultValue = "1") Integer offset,
            @RequestParam(value="limit", required=false,defaultValue = "10") Integer limit) {

        if(search_hotel_id==null){
           search_hotel_id = getCurHotel();
        }
        Long currentUserId = getCurrentUserId();
        logger.debug("userId = {}", currentUserId);

        PageResult logList = new PageResult();

        Long hotelId = search_hotel_id;
        if (null != search_hotel_id) {
            //权限过滤，看该用户是否有该酒店的权限
            List<HotelRefUser> hotelUsers = hotelService.queryHotelRefUserByUser(currentUserId, search_hotel_id);
            if (hotelUsers.size() == 0) {
                return logList;
            }
        } else {
            Long firstHotelId = getCurHotel();
            logger.info("");
            if (null == firstHotelId) {
                firstHotelId = getFirstHotelId(currentUserId);
            }
            if (null == firstHotelId) {
                logger.warn("没有关联酒店 for user {}", currentUserId);
                return logList;
            } else {
                hotelId = firstHotelId;
            }
        }

        //房间类型
       return opLogService.queryLogListByPage(hotelId,search_date_start,search_date_end,
                       search_date_type,room_type_id,search_op_type,offset,limit);

    }

    @ResponseBody
    @RequestMapping("queryRoomSet")
    public Result queryRoomSet(Long hotelId) {

        List<HotelRoomSetVO> hotelRoomSetVOs = getRoomIndoByHotelId(hotelId);

        Map<Integer,String> roomTypeMap = new HashMap<Integer, String>();

        for(HotelRoomSetVO roomSetVO:hotelRoomSetVOs){
            roomTypeMap.put(roomSetVO.getRoomTypeId(),roomSetVO.getRoomTypeName());
        }

        if(roomTypeMap.size()==0){
            return new Result().setData(null);
        }
        logger.info(JsonUtils.objectToJson(roomTypeMap));
        return new Result().setData(roomTypeMap);

    }


        /**
         * 根据用户id对应的酒店信息
         * @param currentUserId
         * @return
         */
    private Long getFirstHotelId(Long currentUserId){
        List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId);
        if(hotelRefUsers.size()>0){
            return hotelRefUsers.get(0).getHotelId();
        }
        return null;
    }


    private List<HotelRoomSetVO> getRoomIndoByHotelId(Long hotelId) {
        //查询酒店下的所有房间类型
        List<HotelRoomSetVO> hotelRoomSetVOS =  roomSetService.queryHotelRoomSetList(hotelId);
        return hotelRoomSetVOS;
    }

}
