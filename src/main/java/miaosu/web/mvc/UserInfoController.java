package miaosu.web.mvc;

import miaosu.svc.user.UserCodeService;
import miaosu.svc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @Date: 2020/7/11 12:59
 * @Description: 用户信息
 */
@Controller
@RequestMapping("/userInfo")
public class UserInfoController  extends BaseController{
	@Autowired
	private UserCodeService userCodeService;

	@ResponseBody
	@RequestMapping("list")
	public Result hotelList(){
		Map map=new HashMap();
		//当前登录用户的id
		Long currentUserId = getCurrentUserId();
		//根据用户id查询用户名user_nick
		String user_nick=userService.queryUser_nick(currentUserId);
		//根据用户id查询用户电话user_phone
		String user_phone = userService.queryUser_phone(currentUserId);
		//根据用户id查询自己的邀请码code_my
		String code_my=userCodeService.queryCode_my(currentUserId);
		//根据我的邀请码查询下家的邀请码所对应的用户id
		List<Long>userIds=userCodeService.queryCode_xiajia(code_my);
		StringBuilder stringBuilder = new StringBuilder();
		for (Long userId : userIds) {
			System.out.println("========="+userId);
			String user_nick1= userService.queryUser_nick(userId);
			if(user_nick1!=null){
				stringBuilder.append(user_nick1+",");
			}
			//切割掉最后一个逗号,没有用户就不能切割,放在循环的里面
		}
		if (stringBuilder.length()>0){
			stringBuilder.deleteCharAt(stringBuilder.length()-1);
		}
		map.put("member", stringBuilder);
		map.put("user_nick",user_nick);
		map.put("user_phone",user_phone);
		map.put("code_my",code_my);
		map.put("id", currentUserId);
		return new Result().success(map);
	}
}
