package miaosu.web.mvc;

/**
 * Created by jiajun.chen on 2018/1/9.
 */

import miaosu.dao.auto.HotelRefUserMapper;
import miaosu.dao.auto.RoleMapper;
import miaosu.dao.model.Role;
import miaosu.dao.model.User;
import miaosu.svc.user.UserService;
import miaosu.wechat.WechatConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class DefaultController extends BaseController {

    @Autowired UserService userService;

    @Autowired
    private HotelRefUserMapper hotelRefUserMapper;
	@Autowired
	private RoleMapper roleMapper;
    //默认进入首页时去重定向到哪个controller中
    /**
     *  return "redirect:/charts"; 重定向到ChartsController
     *  return "/charts/index";  跳转到charts页面
     */

    @GetMapping("/")
    public String home1(HttpServletRequest request) {
        String userAgent = request.getHeader("user-agent").toLowerCase();
        Boolean isWeChat = userAgent == null || userAgent.indexOf("micromessenger") == -1 ? false : true;
        //判断是否是微信端的登录
        if(isWeChat){
            //当前用户id
            Long currentUserId = getCurrentUserId();
            User userInfo = userService.getUserById(currentUserId);
            if(userInfo.getWxOpenid() == null || userInfo.getWxOpenid().isEmpty()){
                request.setAttribute("currentUserId",currentUserId);
                String url = WechatConfig.getGetUserInfoUrl().replace("APPID",WechatConfig.getAppid()).
                        replace("RedirectUri",WechatConfig.getRedirectUri());
                return  "redirect:"+url;
            }
        }
        return "redirect:/charts";
    }

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }

    @GetMapping("/user")
    public String user() {
        return "user";
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    //打开模板的视图页面,打开那个注册页面
    @GetMapping("/zhuce")
    public String zhuce() {
        return "zhuce";
    }
    //忘记密码
    @GetMapping("/forget")
    public String forget() {
        return "forget";
    }

    //用户信息
    @GetMapping("/userInfo")
    public String userInfo() {
        return "userInfo";
    }

    //修改密码
    @GetMapping("/changePassword")
    public String changePassword() {
        return "changePassword";
    }

    //成员列表
    @GetMapping("/member")
    public String member() {
        return "member";
    }

    //添加企业信息
    @GetMapping("/businessInsert")
    public String businessInsert() {
        return "businessInsert";
    }

    //展示企业信息
    @GetMapping("/businessList")
    public String businessList() {
        return "businessList";
    }


    //审核企业信息
    @GetMapping("/checkBusinessInsert")
    public String checkBusinessInsert() {
        return "checkBusinessInsert";
    }

    //用户信息2
    @GetMapping("/userInfo2")
    public String userInfo2() {
        return "userInfo2";
    }

    //修改密码2
    @GetMapping("/changePassword2")
    public String changePassword2() {
        return "changePassword2";
    }

    //成员列表2
    @GetMapping("/member2")
    public String member2() {
        return "member2";
    }

    //添加企业信息2
    @GetMapping("/businessInsert2")
    public String businessInsert2() {
        return "businessInsert2";
    }

    //展示企业信息
    @GetMapping("/businessList2")
    public String businessList2() {
        return "businessList2";
    }

    @GetMapping("/403")
    public String error403() {
        return "error/403";
    }
    //酒店页面
    @GetMapping("/settle_jiudian")
    public String settle_jiudian() {
        return "settle_jiudian";
    }

    //供应商页面
    @GetMapping("/settle_gongyingshang")
    public String settle_gongyingshang() {
        return "settle_gongyingshang";
    }

    //供应商详情页面
    @GetMapping("/settle_gys_detail")
    public String settle_gys_detail() {
        return "settle_gys_detail";
    }

    //超级管理员页面
    @GetMapping("/settle_super")
    public String settle_super() {
        return "settle_super";
    }

    //超级管理员供应商列表页面
    @GetMapping("/settle_super_gyslist")
    public String settle_super_gyslist() {
        return "settle_super_gyslist";
    }

    //超级管理员供应商酒店详情页面
    @GetMapping("/settle_super_gysdetail")
    public String settle_super_gysdetail() {
        return "settle_super_gysdetail";
    }

    //管理员or供应商新增表格页
    @GetMapping("/charts/index")
    public String charts_index() {
        return "charts/index";
    }

    //数据统计
    @GetMapping("/charts/statistical")
    public String charts_statistical() { return "charts/statistical"; }

    //角色管理
    @GetMapping("/management/role")
    public String admin_role() {
        return "management/role";
    }

    //用户管理
    @GetMapping("/management/user")
    public String admin_user() {
        return "management/user";
    }

    //供应商管理
    @GetMapping("/management/supplier")
    public String admin_supplier() {
        return "management/supplier";
    }

    //酒店管理
    @GetMapping("/management/hotel")
    public String admin_hotel() {
        return "management/hotel";
    }

    //关联供应商
    @GetMapping("/management/associated")
    public String associated_supplier() {
        return "management/associated";
    }

    //房价管理
    @GetMapping("/management/roomPrice")
    public String roomPrice(Model model) {
		Long currentUserId = getCurrentUserId();
		List<Long> longs = userService.queryUser_Role(currentUserId);
		Long aLong = longs.get(0);
		Role role = roleMapper.selectByPrimaryKey(aLong);
		String roleName = role.getRoleName();
		model.addAttribute("channel",roleName);
        return "management/roomPrice";
    }

    //酒店评估
    @GetMapping("/management/assessment")
    public String assessment() {
        return "management/assessment";
    }
    //评估详情
    @GetMapping("/management/assessmentDetails")
    public String assessment_details() {
        return "management/assessmentDetails";
    }
    //添加酒店评估
    @GetMapping("/management/addAssessment")
    public String addAssessment() {
        return "management/addAssessment";
    }
}
