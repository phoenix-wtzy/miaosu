package miaosu.web.mvc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import miaosu.annonation.LogInfoAnnotation;
import miaosu.common.OpTypeEnum;
import miaosu.dao.auto.HotelPriceRefRoomTypeMapper;
import miaosu.dao.auto.HotelPriceSetMapper;
import miaosu.dao.auto.RoleMapper;
import miaosu.dao.model.*;
import miaosu.svc.hotel.HotelPriceSetService;
import miaosu.svc.hotel.HotelRoomPriceService;
import miaosu.svc.hotel.HotelRoomSetService;
import miaosu.svc.vo.HotelRoomPriceVO;
import miaosu.svc.vo.Response;
import miaosu.svc.vo.Result;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author  xieqx
 * @date  2018-08-03
 * 房间价格相关的控制层
 */
@Controller
public class RoomPriceController extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(RoomPriceController.class);

    @Autowired
    private HotelPriceSetService hotelPriceSetService;

    @Autowired
    private HotelRoomPriceService hotelRoomPriceService;
    @Autowired
    private HotelRoomSetService hotelRoomSetService;

    @Autowired
    private RoleMapper roleMapper;

    private final int days_count = 10;
    @Autowired
    private HotelPriceSetMapper hotelPriceSetMapper;
    @Autowired
    private HotelPriceRefRoomTypeMapper hotelPriceRefRoomTypeMapper;

    /**
     * 1.返回channel到前端的第1种方法:
     * 1.1在left.html中 < a href="/room/price/search"房价管理(新)< /a>
     * 1.2在这个RoomPriceController类中,写下面的方法 public String search(Model model){}
     *
     *
     * 2.返回channel到前端的第2种方法:
     * 2.1在left.html中 < a href="/management/roomPrice">房价管理(新)< /a>
     * 2.2在DefaultController中的public String roomPrice(Model model) {}中写代码进行返回
     */
//    @GetMapping("/room/price/search")
//    public String search(Model model) {
//        Long currentUserId = getCurrentUserId();
//        List<Long> longs = userService.queryUser_Role(currentUserId);
//        Long aLong = longs.get(0);
//        Role role = roleMapper.selectByPrimaryKey(aLong);
//        String roleName = role.getRoleName();
//        model.addAttribute("channel",roleName);
//        return "management/roomPrice";
//    }

    /**
     * 返回所有的房型以及对应的计划价格 返回房态的所有信息
     * @param model
     * @param date_start 开始日期
     * @return 跳转到房价首页
     */
    @GetMapping("/room/price")
    public String order(Model model,
                        @RequestParam(value="date_start", required=false) String date_start) {

        List<String> dates = Lists.newArrayList();

        Date start = null;
        if(!Strings.isNullOrEmpty(date_start)){
            try {
                start = DateUtils.parseDate(date_start,"yyyy-MM-dd");
            } catch (ParseException e) {
                logger.info("日期转换格式错误！");
                e.printStackTrace();
            }
        }
        start = new Date();

        //获取默认从
        dates.add(DateFormatUtils.format(start,"yyyy-MM-dd"));
        for (int i=1;i<days_count;i++){
            Date d1 = DateUtils.addDays(start, i);
            dates.add(DateFormatUtils.format(d1,"yyyy-MM-dd"));
        }
        Date end = DateUtils.addDays(start, days_count-1);

//        Long currentUserId = getCurrentUserId();
        model.addAttribute("dates",dates);
        model.addAttribute("start",DateFormatUtils.format(start,"MM-dd"));
        model.addAttribute("end",DateFormatUtils.format(end,"MM-dd"));

		Long hotelId = this.getCurHotel();

        List<HotelRoomPriceVO> hotelRoomPriceVOList = Lists.newArrayList();

        if(null != hotelId){
            hotelRoomPriceVOList = hotelRoomPriceService.queryHotelPriceInfo(hotelId,start,end);

        }
        model.addAttribute("roomPriceList",hotelRoomPriceVOList);

        logger.info("价格信息："+JSON.toJSONString(hotelRoomPriceVOList));

        model.addAttribute("yesterday",DateFormatUtils.format(DateUtils.addDays(start, -1),"yyyy-MM-dd"));
        Long currentUserId = getCurrentUserId();
        List<Long> longs = userService.queryUser_Role(currentUserId);
        Long aLong = longs.get(0);
        Role role = roleMapper.selectByPrimaryKey(aLong);
        String roleName = role.getRoleName();
        model.addAttribute("channel",roleName);
        return "room_price";
    }


    /**
     * 返回房价相关的列表信息
     * @param model
     * @param date_start 开始日期
     * @param date_end 结束日期
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/price/list")
    public Result list(Model model,
                       @RequestParam(value="date_start", required=true) String date_start,
                       @RequestParam(value="date_end", required=true) String date_end) {
        try {
            Date startDate = DateUtils.parseDate(date_start,"yyyy-MM-dd");
            Date endDate = DateUtils.parseDate(date_end,"yyyy-MM-dd");

			Long hotelId = getCurHotel();


            List<HotelRoomPriceVO> hotelRoomPriceVOList = Lists.newArrayList();
            if(null != hotelId){
                hotelRoomPriceVOList = hotelRoomPriceService.queryHotelPriceInfo(hotelId,startDate,endDate);
            }

            ObjectMapper mapper = new ObjectMapper();
            logger.info("房态信息："+ mapper.writeValueAsString(hotelRoomPriceVOList));

            return new Result().success().setData(hotelRoomPriceVOList);

        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new Result().failed("参数格式错误");
        }
    }

    /**
     * 下一个日期后为期10天的日期类型
     * @param model
     * @param date_start 开始日期
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/price/dates/next")
    public Result datas_next(Model model,
                       @RequestParam(value="date_start", required=true) String date_start) {
        try {
            List<String> dates = Lists.newArrayList();
            Date start = DateUtils.parseDate(date_start,"yyyy-MM-dd");
            for (int i=1;i<=days_count;i++){
                Date d1 = DateUtils.addDays(start, i);
                dates.add(DateFormatUtils.format(d1,"yyyy-MM-dd"));
            }
            return new Result().success().setData(dates);
        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        }
    }


    /**
     * 当前日期下前一个日期前10天的日期信息
     * @param model
     * @param date_end 当前日期
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/price/dates/pre")
    public Result datas_pre(Model model,
                             @RequestParam(value="date_end", required=true) String date_end) {
        try {
            List<String> dates = Lists.newArrayList();
            Date end = DateUtils.parseDate(date_end,"yyyy-MM-dd");
            for (int i=-days_count; i<0; i++){
                Date d1 = DateUtils.addDays(end, i);
                dates.add(DateFormatUtils.format(d1,"yyyy-MM-dd"));
            }
            logger.info("日期信息："+JSON.toJSONString(dates));
            return new Result().success().setData(dates);

        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        }
    }

    /**
     * 更新房间价格信息
     * （房型和计划代码唯一确认一个价格信息）
     * @param model
     * @param productIds 所有的产品id 数组
     * @param setStartTime 开始时间
     * @param setEndTime  结束时间
     * @param prices  价格信息 数组
     * @return
     */

    @ResponseBody
    @RequestMapping("/room/price/update")
    @LogInfoAnnotation(opType = OpTypeEnum.ROOM_PRICE,startDateFieldName = "setStartTime",
            endDateFieldName = "setEndTime",opValueFieldName = "prices",productFiledName = "productIds" )
    public Result update(Model model,
                       @RequestParam(value="productIds", required=true) Long[] productIds,
                       @RequestParam(value="setStartTime", required=true) String setStartTime,
                       @RequestParam(value="setEndTime", required=true) String setEndTime,
                       @RequestParam(value="prices", required=true) String[]  prices) {
        try {
            Date startDate = DateUtils.parseDate(setStartTime,"yyyy-MM-dd");
            Date endDate = DateUtils.parseDate(setEndTime,"yyyy-MM-dd");

            for (Date d = startDate;d.getTime()<=endDate.getTime();d=DateUtils.addDays(d, 1)){
                for (int i=0;i<productIds.length;i++){
                    if(Strings.isNullOrEmpty(prices[i])){
                        continue;
                    }
                    hotelRoomPriceService.update(productIds[i],d,prices[i]);
                }
            }
            return new Result().success();

        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        }
    }

    //下拉框:查询所有酒店
    @ResponseBody
    @RequestMapping("/room/price/search/name")
    public Response search_name(HttpServletRequest request,
                                @RequestParam(value="hotelName", required=false) String hotelName,
                                @RequestParam(value="hotelId", required=false) Long hotelId){

        Long currentUserId = getCurrentUserId();
		if (hotelName==null){
			List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId);
			HotelRefUser hotelRefUser = hotelRefUsers.get(0);
			hotelId = hotelRefUser.getHotelId();
		}

        String choose_hotel_type = request.getParameter("choose_hotel_type");
        List<HotelInfo> hotelList=null;
        if (null==choose_hotel_type){
            //如果当前是酒店页面
            hotelList = hotelService.queryHotelList(currentUserId,hotelName,hotelId);
        }else {
            Integer isActive = Integer.valueOf(choose_hotel_type);
            //isActive==2是显示所有酒店的意思,1是上架酒店,0是下架酒店
            if (isActive==2){
                hotelList = hotelService.queryHotelList(currentUserId,hotelName,hotelId);
            }else {
                //根据当前酒店id和酒店是否有效去查询对应isActive属性的酒店,isActive是1,选择的就显示上架酒店,是0就显示下架酒店
                hotelList = hotelService.queryHotelList2(currentUserId,hotelName,hotelId,isActive);
            }
        }
        return new Response().success("查询所有酒店信息",hotelList.size(),hotelList);
    }


    //房价日历--查询
    @ResponseBody
    @RequestMapping("/room/price/search/list")
    public Response search_list(@RequestParam(value="hotelId", required=true) Long hotelId,
                                @RequestParam(value="date_start", required=false) String date_start,
                                @RequestParam(value="date_end", required=false) String date_end) {
        Long currentUserId = getCurrentUserId();
        if (null == hotelId){
            List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId);
            HotelRefUser hotelRefUser = hotelRefUsers.get(0);
            hotelId = hotelRefUser.getHotelId();
        }
        //刚开始两者都是null;往前切的查询,date_start=null,date_end=有值;往后切的查询,date_start=有值,date_end=null;
        //房价的查询都是根据start计算的
        Date start = null;
		Date end =null;
		try {
        if(!Strings.isNullOrEmpty(date_start)){
                start = DateUtils.parseDate(date_start,"yyyy-MM-dd");
        }else {
			if(Strings.isNullOrEmpty(date_end)){
				start = new Date();
			}else {
				end=DateUtils.parseDate(date_end, "yyyy-MM-dd");
				start=DateUtils.addDays(end, -(days_count-1));
			}
		}
		} catch (ParseException e) {
			logger.info("日期转换格式错误！");
			e.printStackTrace();
		}
        List list=Lists.newArrayList();
        //查询所有的房型
        HotelPriceSetExample example = new HotelPriceSetExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        List<HotelPriceSet> hotelRoomSets = hotelPriceSetMapper.selectByExample(example);
        if (hotelRoomSets.size()<0){
            return new Response().failed("该酒店没有设置任何房价信息");
        }

        for (HotelPriceSet hotelRoomSet : hotelRoomSets) {
            HotelRoomPrice hotelRoomPrice = new HotelRoomPrice();
            Long hotelPriceId = hotelRoomSet.getHotelPriceId();
            //根据hotelPriceId到hotel_price_ref_room_type表中查询productId
            Long productId =hotelPriceRefRoomTypeMapper.queryProductIdByHotelPriceId(hotelPriceId);
            if (null==productId){
                return new Response().failed("该酒店没有设置任何房价信息");
            }
            List<DateAndprice>dateAndpriceList=Lists.newArrayList();
            for (int i=0;i<days_count;i++){
                DateAndprice dateAndprice = new DateAndprice();
                Date d1 = DateUtils.addDays(start, i);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String format = simpleDateFormat.format(d1);
                Date date1=null;
                try {
                    date1 = DateUtils.parseDate(format, "yyyy-MM-dd");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //根据产品id和日期去查询对应时间的价格
                BigDecimal price=hotelRoomPriceService.queryPrice(productId,date1);
                dateAndprice.setSellDate(date1);
                dateAndprice.setPrice(price);
                dateAndpriceList.add(dateAndprice);
            }
            hotelRoomPrice.setProductId(productId);
            hotelRoomPrice.setHotel_price_name(hotelRoomSet.getHotelPriceName());
            hotelRoomPrice.setHotel_price_id(hotelRoomSet.getHotelPriceId());
            hotelRoomPrice.setDateAndprice(dateAndpriceList);
            list.add(hotelRoomPrice);
        }
        return new Response().success("查询房价信息列表成功",list.size(),list);
    }

    //房价日历--修改
    @ResponseBody
    @RequestMapping("/room/price/search/update")
    public Response search_update(@RequestBody JSONObject hotelRoomPriceGet) throws Exception  {
        String hotelRoomPriceGetStr = hotelRoomPriceGet.toJSONString();
        JSONObject jsonObject = JSON.parseObject(hotelRoomPriceGetStr);
        String obj = jsonObject.getString("obj");
        //将obj转换为hotelRoomPriceGet对象匹配格式
        List<HotelRoomPriceGet> hotelRoomPriceGets = JSON.parseArray(URLDecoder.decode(obj, "UTF-8"), HotelRoomPriceGet.class);
        if (!CollectionUtils.isEmpty(hotelRoomPriceGets)){
            for (HotelRoomPriceGet roomPriceGet : hotelRoomPriceGets) {
                Long product_id = roomPriceGet.getProduct_id();
                String date_start = roomPriceGet.getDate_start();
                String date_end = roomPriceGet.getDate_end();
                String price = roomPriceGet.getPrice();
                Date startDate = DateUtils.parseDate(date_start,"yyyy-MM-dd");
                Date endDate = DateUtils.parseDate(date_end,"yyyy-MM-dd");
                for (Date d = startDate;d.getTime()<=endDate.getTime();d=DateUtils.addDays(d, 1)){
                    hotelRoomPriceService.update(product_id,d,price);
                }
            }
            return new Response().success("修改房价成功");
        }else {
            return new Response().failed("修改房价失败,数据为空");
        }
    }


}
