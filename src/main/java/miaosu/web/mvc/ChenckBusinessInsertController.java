package miaosu.web.mvc;

import miaosu.dao.model.Business;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.user.BusinessService;
import miaosu.svc.user.UserCodeService;
import miaosu.svc.vo.PageResult;
import miaosu.svc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @Date: 2020/7/23 17:46
 * @Description: 审核公司信息
 */
@Controller
@RequestMapping("/chenckBusinessInsert")
public class ChenckBusinessInsertController extends BaseController{

	@Autowired
	private UserCodeService userCodeService;
	@Autowired
	private MsgNotifyService msgNotifyService;
	@Autowired
	private BusinessService businessService;

	@ResponseBody
	@RequestMapping("list")
	public PageResult businessInsertList(@RequestParam(value="offset", required=false,defaultValue = "1") int offset,
								         @RequestParam(value="limit", required=false,defaultValue = "20") int limit){

		//查询所有的公司
		PageResult businessInsertList = userService.queryBusinessInsertListBypage(offset,limit);
		return businessInsertList;
	}

	//审核通过的业务
	@ResponseBody
	@RequestMapping("check")
	public Result check(Long user_id){
		if(user_id==null){
			return new Result().failed("没有用户id");
		}
		//点击审核通过的确定,将用户从三级升级为二级,根据用户id去操作
		userService.updateUser_role(user_id);
		//将用户的code_level设置为已通过
	   	Long code_id=userCodeService.queryCode_id(user_id);
		userCodeService.updateCode_level(code_id);
		//电话通知用户审核通过
		msgNotifyService.checkSuccess(userService.queryUser_phone(user_id),userService.queryUser_nick(user_id));
		//添加公司信息审核通过的时间
		Date success_date = new Date();
		businessService.updateSuccess_date(user_id,success_date);
		return new Result().success();
	}
	//公司信息详情
	@ResponseBody
	@RequestMapping("businessList")
	public Result checkBusinessList(@RequestParam(value="user_id", required=true) Long user_id){
		long startTime=System.currentTimeMillis();//获取开始时间
		Map map=new HashMap();
		//查询公司信息,返回公司对象
		Business business=businessService.queryBusinessList(user_id);
		map.put("business_name",business.getBusiness_name());
		map.put("legal_person",business.getLegal_person());
		map.put("legal_phone",business.getLegal_phone());
		map.put("business_phone",business.getBusiness_phone());
		map.put("business_email",business.getBusiness_email());
		map.put("business_address",business.getBusiness_address());

		Long business_type = business.getBusiness_type();
		if (null==business_type){
			business.setBusiness_typeStr(null);
		}else {
		if (business_type==1){
			map.put("business_type","家具");
		}else if (business_type==2){
			map.put("business_type","装修");
		}else if (business_type==3){
			map.put("business_type","设计");
		}else if (business_type==4){
			map.put("business_type","地毯");
		}else if (business_type==5){
			map.put("business_type","灯饰");
		}else if (business_type==6){
			map.put("business_type","五金");
		}else if (business_type==7){
			map.put("business_type","智能化");
		}else if (business_type==8){
			map.put("business_type","其他");
		}
		}
		Long businessOrFactory = business.getBusinessOrFactory();
		if (null==businessOrFactory){
			business.setBusinessOrFactoryStr(null);
		}else {
		if (businessOrFactory==1){
			map.put("businessOrFactory","经销商");
		}else if (businessOrFactory==2){
			map.put("businessOrFactory","厂家");
		}
		}
		map.put("finance_person",business.getFinance_person());
		map.put("finance_phone",business.getFinance_phone());
		map.put("bank_type",business.getBank_type());
		map.put("bank_number",business.getBank_number());
		map.put("bank_addresss",business.getBank_addresss());
		map.put("account_name",business.getAccount_name());
		map.put("account_number",business.getAccount_number());

		//查询图片
//		Picture picture=businessService.queryPicture(user_id);
//		if (null==picture){
			business.setImg_businessLicense(null);
			business.setImg_shopHead(null);
			business.setImg_factoryBook(null);
//		}else {
//		byte[] img_businessLicense = picture.getImg_businessLicense();
//		String encode1 = Base64.encodeBase64String(img_businessLicense);
//		String base64_1="data:image/png;base64,"+encode1;
//		map.put("img_businessLicense", base64_1);
//
//		byte[] img_shopHead = picture.getImg_shopHead();
//		String encode2 = Base64.encodeBase64String(img_shopHead);
//		String base64_2="data:image/png;base64,"+encode2;
//		map.put("img_shopHead", base64_2);
//
//		byte[] img_factoryBook = picture.getImg_factoryBook();
//		String encode3 = Base64.encodeBase64String(img_factoryBook);
//		String base64_3="data:image/png;base64,"+encode3;
//		map.put("img_factoryBook", base64_3);
//		long endTime=System.currentTimeMillis(); //获取结束时间
//		System.out.println("展示照片程序运行时间： "+(endTime-startTime)+"ms");
//		}
		return new Result().success(map);
	}

	//审核不通过
	@ResponseBody
	@RequestMapping("checkFail")
	public Result checkFail(@RequestParam(value="user_id") Long user_id,
							@RequestParam(value = "why")String why){
		if (user_id==null){
			return new Result().failed("获取不到用户id");
		}
		if (why.isEmpty()){
			return new Result().failed("不通过的原因为空");
		}
		//根据公司id发送审核不通过的短信,告知原因
		msgNotifyService.checkFail(userService.queryUser_phone(user_id),why);
		//删除不通过的公司信息和照片
		businessService.deleteBusiness(user_id);
		businessService.deletePicture(user_id);
		return new Result().success();
	}
}
