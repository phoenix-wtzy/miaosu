//package miaosu.web.mvc;
//
//import com.google.common.collect.Lists;
//import miaosu.common.SettleStatusEnum;
//import miaosu.dao.auto.HotelInfoMapper;
//import miaosu.dao.auto.HotelRoomSetMapper;
//import miaosu.dao.auto.UserCodeMapper;
//import miaosu.dao.model.*;
//import miaosu.svc.hotel.HotelService;
//import miaosu.svc.order.OrderService;
//import miaosu.svc.settle.SettleBillSerrvice;
//import miaosu.svc.user.BusinessService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
///**
// * @author Administrator
// * @Date: 2020/8/7 10:48
// * @Description:
// */
//@Controller
//@RequestMapping("gongyingshang")
//public class GongYingShangController extends BaseController{
//
//
//	@Autowired
//	private BusinessService businessService;
//	@Autowired
//	private HotelService hotelService;
//	@Autowired
//	private OrderService orderService;
//	@Autowired
//	private HotelInfoMapper hotelInfoMapper;
//	@Autowired
//	private UserCodeMapper userCodeMapper;
//	@Autowired
//	private SettleBillSerrvice settleBillSerrvice;
//	@Autowired
//	private HotelRoomSetMapper hotelRoomSetMapper;
//
//
//
//
//	//展示供应商结算
//	@ResponseBody
//	@RequestMapping("list")
//	public List<GongYingShang>  gongyingshangList(@RequestParam(value="offset", required=false,defaultValue = "1") int offset,
//								@RequestParam(value="limit", required=false,defaultValue = "20") int limit){
//		Long currentUserId = getCurrentUserId();
////		PageResult gongyingshangList = userService.querygongyingshangListBypage(currentUserId,offset,limit);
//		List<GongYingShang> gongYingShangs = null;
//		try {
//			gongYingShangs = userService.querygongyingshangList(currentUserId);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//
//		return gongYingShangs;
//	}
//
////	//展示酒店列表
////	@ResponseBody
////	@RequestMapping("hotelList")
////	public List<GongYingShang_hotelList> hotelList(
////			@RequestParam(value="business_id", required=true) Long business_id,
////			@RequestParam(value="index", required=true) Long index) throws Exception{
////		if(business_id == null){
////			return null;
////		}
////		List<GongYingShang_hotelList> hotelList = Lists.newArrayList();
////		//根据公司id查询公司信息
////		Business business= businessService.queryBusinessByBusiness_id(business_id);
////		Long user_id = business.getUser_id();
////		//根据用户id查询酒店id集合
////		List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
////		for (HotelRefUser hotelRefUser : hotelRefUsers) {
////			GongYingShang_hotelList gongYingShang_hotelList = new GongYingShang_hotelList();
////			Long hotelId = hotelRefUser.getHotelId();
////			//酒店id
////			gongYingShang_hotelList.setP_hotel_id(hotelId);
////			//根据酒店id查询酒店信息
////			HotelInfoExample hotelInfoExample = new HotelInfoExample();
////			HotelInfoExample.Criteria and = hotelInfoExample.createCriteria();
////			and.andHotelIdEqualTo(hotelId);
////			List<HotelInfo> hotelInfo= hotelInfoMapper.selectByExample(hotelInfoExample);
////			//酒店名字
////			for (HotelInfo info : hotelInfo) {
////				gongYingShang_hotelList.setHotel_name(info.getHotelName());
////			}
////			//计算佣金率
////			Integer count=0;
////			Integer yongjinglv=1;
////			//根据当前用户id查询自己的邀请码
////			String code_my = userCodeMapper.queryCode_my(user_id);
////			//根据我的邀请码查询下家的邀请码所对应的用户id
////			List<Long> userIds = userCodeMapper.queryCode_xiajia(code_my);
////			for (Long userId : userIds) {
////				//根据成员id查询邀请码id
////				Long code_id = userCodeMapper.queryCode_id(userId);
////				//根据邀请码id查询code_level的状态
////				String code_level = userCodeMapper.queryCode_level(code_id);
////				if (code_level.contains("已通过")){
////					count++;
////				}
////			}
////			if(count<=15){
////				yongjinglv=15;
////			}
////			if(count>=16&&count<=30){
////				yongjinglv=14;
////			}
////			if(count>=31&&count<=50){
////				yongjinglv=13;
////			}
////			if(count>=51){
////				yongjinglv=12;
////			}
////
////			Long settle_StartDateTime=null;
////			Long settle_endDateTime=null;
////			int i=53;
////			for (int i1 = 0; i1 < i; i1++) {
////				List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
////				if (index==i1){
////					String setSettle_StartAndEnd = everyWeekOfYear.get(i1);
////					String[] split = setSettle_StartAndEnd.split("~");
////					SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
////					String settle_Start = split[0];//20200101
////					Date settle_StartDate = yyyyMMdd.parse(settle_Start);
////					settle_StartDateTime = settle_StartDate.getTime();
////					String settle_End = split[1];//20200105
////					Date settle_EndDate = yyyyMMdd.parse(settle_End);
////					settle_endDateTime = settle_EndDate.getTime();
////				}
////			}
////
////			//结算金额=总金额*xx%
////			BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
////			BigDecimal settle_money = new BigDecimal(0);//结算金额
////			BigDecimal commission_money = new BigDecimal(0);//技术服务费
////			//根据酒店id查询所有对应的订单列表,很多个
////			HotelOrderExample hotelOrderExample = new HotelOrderExample();
////			HotelOrderExample.Criteria and1 = hotelOrderExample.createCriteria();
////			and1.andHotelIdEqualTo(hotelId);
////			List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
////			for (HotelOrder hotelOrder : hotelOrders) {
////				Long orderId = hotelOrder.getOrderId();
////				//入住时间
////				Date checkInTime = hotelOrder.getCheckInTime();
////				long checkInTimeTime = checkInTime.getTime();
////				//订单状态
////				Integer state = hotelOrder.getState();
////				Integer settleStatus = hotelOrder.getSettleStatus();
////				//订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
////				if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime) {
////					//价格
////					BigDecimal price = hotelOrder.getPrice();
////					settleTotalMoney = settleTotalMoney.add(price);
////					settle_money=settleTotalMoney.multiply(new BigDecimal((100-yongjinglv))).divide(new BigDecimal(100));
////					commission_money=settleTotalMoney.multiply(new BigDecimal(yongjinglv)).divide(new BigDecimal(100));
////				}
////			}
////			//结算金额就是下面的
////			gongYingShang_hotelList.setSettle_money(String.valueOf(settle_money));
////			//5.技术服务费=总金额*xx%
////			gongYingShang_hotelList.setCommission_money(String.valueOf(commission_money));
////			hotelList.add(gongYingShang_hotelList);
////		}
////		return hotelList;
////	}
////
////	//展示订单详细列表
////	@ResponseBody
////	@RequestMapping("orderList")
////	public List<GongYingShang_orderList> orderList(
////			@RequestParam(value="business_id", required=true) Long business_id,
////			@RequestParam(value="index", required=true) Long index)throws Exception{
////		System.out.println("1111111111");
////		if(business_id == null){
////			return null;
////		}
////		List<GongYingShang_orderList> orderDetails = Lists.newArrayList();
////		//根据公司id查询公司信息
////		Business business= businessService.queryBusinessByBusiness_id(business_id);
////		Long user_id = business.getUser_id();
////		//根据用户id查询酒店id集合
////		List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
////		for (HotelRefUser hotelRefUser : hotelRefUsers) {
////			GongYingShang_orderList gongYingShang_orderList = new GongYingShang_orderList();
////			Long hotelId = hotelRefUser.getHotelId();
////			gongYingShang_orderList.setP_hotel_id(hotelId);
////			Long settle_StartDateTime=null;
////			Long settle_endDateTime=null;
////			int i=53;
////			for (int i1 = 0; i1 < i; i1++) {
////				List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
////				if (index==i1){
////					String setSettle_StartAndEnd = everyWeekOfYear.get(i1);
////					String[] split = setSettle_StartAndEnd.split("~");
////					SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
////					String settle_Start = split[0];//20200101
////					Date settle_StartDate = yyyyMMdd.parse(settle_Start);
////					settle_StartDateTime = settle_StartDate.getTime();
////					String settle_End = split[1];//20200105
////					Date settle_EndDate = yyyyMMdd.parse(settle_End);
////					settle_endDateTime = settle_EndDate.getTime();
////				}
////			}
////
////			//根据酒店id查询所有对应的订单列表,很多个
////			HotelOrderExample hotelOrderExample = new HotelOrderExample();
////			HotelOrderExample.Criteria and1 = hotelOrderExample.createCriteria();
////			and1.andHotelIdEqualTo(hotelId);
////			List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
////			for (HotelOrder hotelOrder : hotelOrders) {
////				//入住时间
////				Date checkInTime = hotelOrder.getCheckInTime();
////				long checkInTimeTime = checkInTime.getTime();
////				//订单状态
////				Integer state = hotelOrder.getState();
////				Integer settleStatus = hotelOrder.getSettleStatus();
////				//订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
////				if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime) {
////
////				}
////			}
////
////
////
////
////			orderDetails.add(gongYingShang_orderList);
////		}
////		return orderDetails;
////	}
////	private long getDays(Date startDate, Date endDate) {
////		if (startDate != null && endDate != null) {
////			SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
////			Date date = null;
////			Date mydate = null;
////
////			try {
////				mydate = myFormatter.parse(myFormatter.format(startDate));
////				date = myFormatter.parse(myFormatter.format(endDate));
////			} catch (Exception var7) {
////			}
////
////			long day = (date.getTime() - mydate.getTime()) / 86400000L;
////			return day;
////		} else {
////			return 0L;
////		}
////	}
//
//
//	//展示酒店列表+对应的订单信息
//	@ResponseBody
//	@RequestMapping("hotelAndOrderList")
//	public List<GongYingShang_hotelAndOrderList> hotelAndOrderList(
//			@RequestParam(value="business_id", required=true) Long business_id,
//			@RequestParam(value="setSettle_StartAndEnd", required=true) String settle_StartAndEnd) throws Exception{
//		if(business_id == null){
//			return null;
//		}
//		List<GongYingShang_hotelAndOrderList> hotelAndOrderList = Lists.newArrayList();
//		//根据公司id查询公司信息
//		Business business= businessService.queryBusinessByBusiness_id(business_id);
//		Long user_id = business.getUser_id();
//		//计算佣金率
//		Integer count=0;
//		Integer yongjinglv=1;
//		//根据当前用户id查询自己的邀请码
//		String code_my = userCodeMapper.queryCode_my(user_id);
//		//根据我的邀请码查询下家的邀请码所对应的用户id
//		List<Long> userIds = userCodeMapper.queryCode_xiajia(code_my);
//		for (Long userId : userIds) {
//			//根据成员id查询邀请码id
//			Long code_id = userCodeMapper.queryCode_id(userId);
//			//根据邀请码id查询code_level的状态
//			String code_level = userCodeMapper.queryCode_level(code_id);
//			if (code_level.contains("已通过")){
//				count++;
//			}
//		}
//		if(count<=15){
//			yongjinglv=15;
//		}
//		if(count>=16&&count<=30){
//			yongjinglv=14;
//		}
//		if(count>=31&&count<=50){
//			yongjinglv=13;
//		}
//		if(count>=51){
//			yongjinglv=12;
//		}
//
//		//根据用户id查询酒店id集合
//		List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
//		for (HotelRefUser hotelRefUser : hotelRefUsers) {
//			GongYingShang_hotelAndOrderList gongYingShang_hotelAndOrderList = new GongYingShang_hotelAndOrderList();
//			Long hotelId = hotelRefUser.getHotelId();
//			//酒店id
//			gongYingShang_hotelAndOrderList.setP_hotel_id(hotelId);
//			//根据酒店id查询酒店信息
//			HotelInfo hotelInfo = hotelInfoMapper.selectByPrimaryKey(hotelId);
//			//酒店名字
//			gongYingShang_hotelAndOrderList.setHotel_name(hotelInfo.getHotelName());
//			Long settle_StartDateTime=null;
//			Long settle_endDateTime=null;
//			String[] split = settle_StartAndEnd.split("~");
//			SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
//			String settle_Start = split[0];//20200101
//			Date settle_StartDate = yyyyMMdd.parse(settle_Start);
//			settle_StartDateTime = settle_StartDate.getTime();
//			String settle_End = split[1];//20200105
//			Date settle_EndDate = yyyyMMdd.parse(settle_End);
//			settle_endDateTime = settle_EndDate.getTime();
//			//结算金额=总金额*xx%
//			BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
//			BigDecimal settle_money = new BigDecimal(0);//结算金额
//			BigDecimal commission_money = new BigDecimal(0);//技术服务费
//			//根据酒店id查询所有对应的订单列表,很多个
//			HotelOrderExample hotelOrderExample = new HotelOrderExample();
//			HotelOrderExample.Criteria and1 = hotelOrderExample.createCriteria();
//			and1.andHotelIdEqualTo(hotelId);
//			List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
//			for (HotelOrder hotelOrder : hotelOrders) {
//				Long orderId = hotelOrder.getOrderId();
//				//入住时间
//				Date checkInTime = hotelOrder.getCheckInTime();
//				long checkInTimeTime = checkInTime.getTime();
//				//订单状态
//				Integer state = hotelOrder.getState();
//				Integer settleStatus = hotelOrder.getSettleStatus();
//				//订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
//				if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime) {
//					//日期转字符串
//					SimpleDateFormat simpleDateFormat_book = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//					SimpleDateFormat simpleDateFormat_inAndOut = new SimpleDateFormat("yyyy-MM-dd");
//					gongYingShang_hotelAndOrderList.setOrderNo(hotelOrder.getOrderNo());
//					gongYingShang_hotelAndOrderList.setBookTime(simpleDateFormat_book.format(hotelOrder.getBookTime()));
//					gongYingShang_hotelAndOrderList.setBookUser(hotelOrder.getBookUser());
//					Long hotelRoomTypeId = hotelOrder.getHotelRoomTypeId();
//					String roomTypeName = hotelRoomSetMapper.queryHotelRoomTypeName(hotelRoomTypeId);
//					gongYingShang_hotelAndOrderList.setHotelRoomType(roomTypeName);
//					gongYingShang_hotelAndOrderList.setRoomCount(hotelOrder.getRoomCount());
//					gongYingShang_hotelAndOrderList.setCheckInTime(simpleDateFormat_inAndOut.format(hotelOrder.getCheckInTime()));
//					gongYingShang_hotelAndOrderList.setCheckOutTime(simpleDateFormat_inAndOut.format(hotelOrder.getCheckOutTime()));
//					gongYingShang_hotelAndOrderList.setPrice(hotelOrder.getPrice());
//					gongYingShang_hotelAndOrderList.setBookRemark(hotelOrder.getBookRemark());
//					gongYingShang_hotelAndOrderList.setSettleStatus(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
//					//价格
//					BigDecimal price = hotelOrder.getPrice();
//					settleTotalMoney = settleTotalMoney.add(price);
//					settle_money=settleTotalMoney.multiply(new BigDecimal((100-yongjinglv))).divide(new BigDecimal(100));
//					commission_money=settleTotalMoney.multiply(new BigDecimal(yongjinglv)).divide(new BigDecimal(100));
//					//结算金额就是下面的
//					gongYingShang_hotelAndOrderList.setSettle_money(String.valueOf(settle_money));
//					//5.技术服务费=总金额*xx%
//					gongYingShang_hotelAndOrderList.setCommission_money(String.valueOf(commission_money));
//					//6.结算状态
//					gongYingShang_hotelAndOrderList.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
//				}
//			}
//			hotelAndOrderList.add(gongYingShang_hotelAndOrderList);
//		}
//		return hotelAndOrderList;
//	}
//
//}
