package miaosu.web.mvc;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import miaosu.common.OrderChannelEnum;
import miaosu.common.OrderStatusEnum;
import miaosu.common.SellTypeEnum;
import miaosu.common.SettleStatusEnum;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelOrder;
import miaosu.dao.model.HotelOrderExample;
import miaosu.dao.model.Hotel_settle_isActive;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.order.OrderService;
import miaosu.svc.settle.ChartsService;
import miaosu.svc.settle.Hotel_settle_isActiveService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.HotelOrderVO;
import miaosu.svc.vo.Response;
import miaosu.svc.vo.Result;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Administrator
 * @Date: 2020/12/11 11:12
 * @Description: 首页数据统计表
 */
@Controller
@RequestMapping("/charts")
@Slf4j
public class ChartsController extends BaseController{

	@Autowired
	private OrderService orderService;
	@Autowired
	private HotelService hotelService;
	@Autowired
	private UserService userService;

	@Autowired
	private ChartsService chartsService;


	@Autowired
	private Hotel_settle_isActiveService hotel_settle_isActiveService;

	//登录就进入ChartsController类中,并判断,如果是超级管理员就进入charts首页,不是就进入订单页面
	@GetMapping("")
	public String order(Model model) {
		Date now = new Date();
		Date preday7 = DateUtils.addDays(now, -7);
		String date_end = DateFormatUtils.format(now,"yyyy-MM-dd");
		String date_start = DateFormatUtils.format(preday7,"yyyy-MM-dd");
		model.addAttribute("search_date", ImmutableMap.of("date_start",date_start,"date_end",date_end));
		Long currentUserId = getCurrentUserId();
		model.addAttribute("channel",currentUserId);

		//如果是超级管理员,首页进入charts页面,而供应商进入admin/order首页,酒店进入redirect:/order首页
        if (isSuperAdmin()){
            return "charts/index";
        }else {
		if(isAdmin()){
			return "admin/order";
		}else {
			return "redirect:/order";
		}
        }
	}

	//上线酒店结算数据
	@ResponseBody
	@RequestMapping("hotel_settle_isActive")
	public Response hotelSettle_isActive(){
		//查询表hotel_settle_isactive前天所有数据
		List<Hotel_settle_isActive>hotel_settle_isActiveList=hotel_settle_isActiveService.queryAll();
		return new Response().success("查询所有上线酒店结算数据",hotel_settle_isActiveList.size(),hotel_settle_isActiveList);
	}

	//下线酒店结算数据
	@ResponseBody
	@RequestMapping("hotel_settle_isNotActive")
	public Response hotel_settle_isNotActive(){
		//查询下线酒店表hotel_settle_isnotactive前天所有数据
		List<Hotel_settle_isActive>hotel_settle_isActiveList=hotel_settle_isActiveService.queryAllInhotel_settle_isnotactive();
		return new Response().success("查询所有下线酒店结算数据",hotel_settle_isActiveList.size(),hotel_settle_isActiveList);
	}

	//今日数据
	@ResponseBody
	@RequestMapping("order_today")
	public Result TodayOrder(){
		Map map=new HashMap();

		Date date1 = new Date();
		//昨天 2020-12-13 23:59:59
		Calendar calendar1 = new GregorianCalendar();
		calendar1.setTime(date1);
		calendar1.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
		calendar1.set(Calendar.MINUTE, 59); //将分钟至59
		calendar1.set(Calendar.SECOND,59); //将秒至59
		calendar1.set(Calendar.MILLISECOND, 999); //将毫秒至999
		calendar1.add(calendar1.DATE,-1);//把日期往后增加一天.整数往后推,负数往前移动
		date1=calendar1.getTime(); //这个时间就是日期往后推一天的结果
		//明天 2020-12-15 00:00:00
		Date date2 = new Date();
		Calendar calendar2 = new GregorianCalendar();
		calendar2.setTime(date2);
		calendar2.set(Calendar.HOUR_OF_DAY, 0);
		calendar2.set(Calendar.MINUTE, 0);
		calendar2.set(Calendar.SECOND,0);
		calendar2.set(Calendar.MILLISECOND, 0);
		calendar2.add(calendar2.DATE,1);
		date2=calendar2.getTime();
		//查询今天的所有订单
		List<HotelOrder>orderInfoList=orderService.queryOrder_Today(date1,date2);
		//今日总订单数
		map.put("order_today_size",orderInfoList.size());
		long order_today_night=0;
		BigDecimal order_today_price=new BigDecimal(0);
		for (HotelOrder hotelOrder : orderInfoList) {
			Integer nightC = hotelOrder.getNightC();
			if (nightC==null){
				nightC=0;
			}
			//总间夜数=每单间夜数相加
			order_today_night+=nightC;
			BigDecimal price = hotelOrder.getPrice();
			//总金额数=每单的价格相加即可
			order_today_price=order_today_price.add(price);
		}
		//今日总间夜数
		map.put("order_today_night",order_today_night);
		//今日总金额数
		map.put("order_today_price",order_today_price);
		return new Result().business(map,"今日数据查询");
	}

	//昨日数据
	@ResponseBody
	@RequestMapping("order_yesterday")
	public Result YesterdayOrder(){
		Map map=new HashMap();

		Date date1 = new Date();
		//前天 2020-12-12 23:59:59
		Calendar calendar1 = new GregorianCalendar();
		calendar1.setTime(date1);
		calendar1.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
		calendar1.set(Calendar.MINUTE, 59); //将分钟至59
		calendar1.set(Calendar.SECOND,59); //将秒至59
		calendar1.set(Calendar.MILLISECOND, 999); //将毫秒至999
		calendar1.add(calendar1.DATE,-2);
		date1=calendar1.getTime();
		//今天 2020-12-14 00:00:00
		Date date2 = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = sdf.format(date2);
		try {
			date2= DateUtils.parseDate(startDate, "yyyy-MM-dd");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		//查询昨天的所有订单
		List<HotelOrder>orderInfoList=orderService.queryOrder_Yesterday(date1,date2);
		//昨日总订单数
		map.put("order_yesterday_size",orderInfoList.size());
		long order_yesterday_night=0;
		BigDecimal order_yesterday_price=new BigDecimal(0);
		for (HotelOrder hotelOrder : orderInfoList) {
			Integer nightC = hotelOrder.getNightC();
			if (nightC==null){
				nightC=0;
			}
			//总间夜数=每单间夜数相加
			order_yesterday_night+=nightC;
			BigDecimal price = hotelOrder.getPrice();
			//总金额数=每单的价格相加即可
			order_yesterday_price=order_yesterday_price.add(price);
		}
		//昨天总间夜数
		map.put("order_yesterday_night",order_yesterday_night);
		//昨天总金额数
		map.put("order_yesterday_price",order_yesterday_price);
		return new Result().business(map,"昨日数据查询");
	}

	//昨日数据的详情
	@ResponseBody
	@RequestMapping("order_yesterdayList")
	public Response YesterdayOrderList(){
		Date date1 = new Date();
		//前天 2020-12-12 23:59:59
		Calendar calendar1 = new GregorianCalendar();
		calendar1.setTime(date1);
		calendar1.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
		calendar1.set(Calendar.MINUTE, 59); //将分钟至59
		calendar1.set(Calendar.SECOND,59); //将秒至59
		calendar1.set(Calendar.MILLISECOND, 999); //将毫秒至999
		calendar1.add(calendar1.DATE,-2);
		date1=calendar1.getTime();
		//今天 2020-12-14 00:00:00
		Date date2 = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = sdf.format(date2);
		try {
			date2= DateUtils.parseDate(startDate, "yyyy-MM-dd");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		//查询昨天的所有订单
		HotelOrderExample order=new HotelOrderExample();
		HotelOrderExample.Criteria and = order.createCriteria();
		and.andStateEqualTo(2).andBookTimeBetween(date1, date2);
		List<HotelOrder> orderInfoList = orderService.queryOrderList(order);

		List<HotelOrderVO> list = Lists.newArrayList();
		for(HotelOrder hotelOrder:orderInfoList){
			HotelOrderVO  vo = new HotelOrderVO();
			BeanUtils.copyProperties(hotelOrder,vo);

			//将间夜数传递给前端
			vo.setNightC(vo.getNightC());
			//间夜=每单的入离时间差
			long days = getDays(vo.getCheckInTime(), vo.getCheckOutTime());
			vo.setNight(days);
			//酒店确认号
			vo.setHotel_confirm_number(vo.getHotel_confirm_number());
			//房型名称
			vo.setP_hotel_room_type_name(vo.getP_hotel_room_type_name());
			//采购单价
			vo.setCaigou_one(vo.getCaigou_one());
			//采购总价
			vo.setCaigou_total(vo.getCaigou_total());
			//订单备注
			vo.setRemark_one(vo.getRemark_one());
			vo.setOrderChannelCode(vo.getOrderChannel()+"");
			vo.setOrderChannelName(OrderChannelEnum.fromCode(vo.getOrderChannel()).getName());
			vo.setSellTypeName(SellTypeEnum.fromCode(vo.getSellType()).getName());
			vo.setOrderStateStr(OrderStatusEnum.fromCode(vo.getState()).getName());
			vo.setBookTimeStr(DateFormatUtils.format(vo.getBookTime(),"yyyy-MM-dd HH:mm"));
			vo.setCheckInTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd"));
			vo.setCheckOutTimeStr(DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd"));
			vo.setCheckInOutTimeStr(DateFormatUtils.format(vo.getCheckInTime(),"yyyy-MM-dd ")+" ~ "+DateFormatUtils.format(vo.getCheckOutTime(),"yyyy-MM-dd "));
			Integer settleStatus = hotelOrder.getSettleStatus();
			if(settleStatus!=null) {
				vo.setSettleStatusStr(SettleStatusEnum.fromCode(settleStatus).getName());
			}
			list.add(vo);
		}
		return new Response().success("昨日数据详情",list.size(),list);
	}
	//所有订单表数据汇总
	@ResponseBody
	@RequestMapping("order_all")
	public Result AllOrder(){
		Map map=new HashMap();

		//查询所有订单表订单数据
		List<HotelOrder>orderInfoList=orderService.queryAllOrderInfo();
		//总订单数
		map.put("order_all_size",orderInfoList.size());
		long order_all_night=0;
		BigDecimal order_all_price=new BigDecimal(0);
		for (HotelOrder hotelOrder : orderInfoList) {
			Integer nightC = hotelOrder.getNightC();
			if (nightC==null){
				nightC=0;
			}
			//总间夜数=每单间夜数相加
			order_all_night+=nightC;
			BigDecimal price = hotelOrder.getPrice();
			//总金额数=每单的价格相加即可
			order_all_price=order_all_price.add(price);
		}
		//总间夜数
		map.put("order_all_night",order_all_night);
		//总金额数
		map.put("order_all_price",order_all_price);
		return new Result().business(map,"汇总数据查询");
	}


	//酒店统计
	@ResponseBody
	@RequestMapping("hotel_all")
	public Result AllHotelInfo(){
		Map map=new HashMap();

		//查询所有酒店信息
		List<HotelInfo>hotelInfoList=hotelService.queryAllHotelInfo();
		long hotel_all_IsActive=0;
		long hotel_all_NotIsActive=0;
		long hotel_all_addInThisMonth=0;
		for (HotelInfo hotelInfo : hotelInfoList) {
			//酒店是否有效
			int isActive = hotelInfo.getIsActive();
			if (isActive==1){
				hotel_all_IsActive++;
			}else {
				hotel_all_NotIsActive++;
			}
			//酒店创建时间
			Date createTime = hotelInfo.getCreateTime();
			if (createTime==null){
				System.out.println("酒店创建时间为空,这里暂时设置为当前时间,否则报错");
				createTime=new Date();
			}
			long createTime1 = createTime.getTime();
			//Calendar 是java.util下的工具类
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, 0); //获取当前月第一天
			c.set(Calendar.DAY_OF_MONTH, 1); //设置为1号,当前日期既为本月第一天
			c.set(Calendar.HOUR_OF_DAY, 0); //将小时至0
			c.set(Calendar.MINUTE, 0); //将分钟至0
			c.set(Calendar.SECOND,0); //将秒至0
			c.set(Calendar.MILLISECOND, 0); //将毫秒至0
			Calendar c2 = Calendar.getInstance();
			c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH)); //获取当前月最后一天
			c2.set(Calendar.HOUR_OF_DAY, 23); //将小时至23
			c2.set(Calendar.MINUTE, 59); //将分钟至59
			c2.set(Calendar.SECOND,59); //将秒至59
			c2.set(Calendar.MILLISECOND, 999); //将毫秒至999
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String startDate = sdf.format(c.getTime());//当月第一天时间
			String endDate = sdf.format(c2.getTime());//当月最后一天时间
			//当月第一天时间戳
			long firstDayMillis = c.getTimeInMillis();
			//当月最后一天时间戳
			long lastDayMillis = c2.getTimeInMillis();
			//本月新增酒店数量,当月第一天时间戳<=酒店创建时间戳<=当月最后一天时间戳
			if (firstDayMillis<=createTime1 && lastDayMillis>=createTime1){
				hotel_all_addInThisMonth++;
			}
		}
		//总上线酒店数量
		map.put("hotel_all_IsActive",hotel_all_IsActive);
		//总下线酒店数量
		map.put("hotel_all_NotIsActive",hotel_all_NotIsActive);
		//本月新增酒店数量
		map.put("hotel_all_addInThisMonth",hotel_all_addInThisMonth);
		return new Result().business(map,"酒店统计查询");
	}

	//获取间夜数
	private long getDays(Date startDate, Date endDate) {
		if (startDate != null && endDate != null) {
			SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = null;
			Date mydate = null;

			try {
				mydate = myFormatter.parse(myFormatter.format(startDate));
				date = myFormatter.parse(myFormatter.format(endDate));
			} catch (Exception var7) {
			}

			long day = (date.getTime() - mydate.getTime()) / 86400000L;
			return day;
		} else {
			return 0L;
		}
	}
}
