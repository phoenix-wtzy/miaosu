package miaosu.web.mvc;

import miaosu.dao.model.HotelOrder;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zyz
 * @Date: 2020/10/28 10:38
 * @Description: 短信语音提醒
 */
@Controller
@RequestMapping("/msgVoiceNotify")
public class MsgVoiceNotifyController {
	@Autowired
	@Qualifier("redisTemplate")
	private RedisTemplate redisTemplate;
	@Autowired
	private MsgNotifyService msgNotifyService;

	@ResponseBody
	@RequestMapping("/sendMsgAndVoice")
	public Result reset(@RequestBody HotelOrder hotelOrder){

		if(hotelOrder == null || hotelOrder.getOrderId() == null ){
			return new Result().failed("未获取到订单！");
		}
		int notifyCount = 0; //统计语音通知几次
		long startTime = 0;
		String orderId = String.valueOf(hotelOrder.getOrderId());

		//先验证缓存中是否有该记录
		if(redisTemplate.opsForHash().hasKey(orderId,"orderId")){
			notifyCount = (int) redisTemplate.opsForHash().get(orderId,"notifyCount");
			if(notifyCount > 1 ){
				startTime = Long.parseLong(String.valueOf(redisTemplate.opsForHash().get(orderId,"startTime")));
				//System.out.println("时间差:"+(new Date().getTime()/1000 - startTime));
				if((new Date().getTime()/1000) - startTime > 60 ){
					redisTemplate.opsForHash().put(orderId,"startTime",new Date().getTime()/1000);
					redisTemplate.opsForHash().increment(orderId,"notifyCount",-1);
//				//只打语音电话
				msgNotifyService.sendMsgAgain(hotelOrder.getHotelId(),hotelOrder);
				}
			}
		}else{
			saveCount(hotelOrder.getOrderId());
		}
//		//返回前端短信提醒了几次
		return new Result().success(notifyCount);
	}

	//将需要语音提醒订单信息存入reids缓存
	public void saveCount(long orderId){
		Map countMap = new HashMap();
		countMap.put("orderId",orderId);
		countMap.put("startTime",new Date().getTime()/1000);
		countMap.put("notifyCount",4);
		redisTemplate.opsForHash().putAll(String.valueOf(orderId),countMap);
	}
}
