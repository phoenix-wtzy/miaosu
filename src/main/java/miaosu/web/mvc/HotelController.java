package miaosu.web.mvc;

import miaosu.svc.hotel.HotelService;
import miaosu.svc.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jiajun.chen on 2018/4/11.
 */

@Controller
@RequestMapping("/hotel")
public class HotelController extends BaseController {
    @Autowired
    private HotelService hotelService;

    /**
     * 查询酒店根据名称
     * @param request http请求对象
     * @param name 酒店名称
     * @param hotelId 酒店id
     * @param offset 分页开始位置
     * @param limit 分页结束位置
     * @return
     */
    @ResponseBody
    @RequestMapping("search/name")
    public PageResult hotelList(HttpServletRequest request,
                                @RequestParam(value="name", required=false) String name,
                                @RequestParam(value="hotelId", required=false) Long hotelId,
                                @RequestParam(value="offset", required=false,defaultValue = "1") int offset,
                                @RequestParam(value="limit", required=false,defaultValue = "200") int limit){

    	//当前用户id
        Long currentUserId = getCurrentUserId();
        String choose_hotel_type = request.getParameter("choose_hotel_type");
        PageResult hotelList=null;
        if (null==choose_hotel_type){
            //如果当前是酒店页面
            hotelList = hotelService.queryHotelListWithPage(currentUserId, name,hotelId,offset,limit);
        }else {
            Integer isActive = Integer.valueOf(choose_hotel_type);
            //isActive==2是显示所有酒店的意思,1是上架酒店,0是下架酒店
            if (isActive==2){
                hotelList = hotelService.queryHotelListWithPage(currentUserId, name,hotelId,offset,limit);
            }else {
                //根据当前酒店id和酒店是否有效去查询对应isActive属性的酒店,isActive是1,选择的就显示上架酒店,是0就显示下架酒店
                hotelList = hotelService.queryHotelListWithPage2(currentUserId, name,hotelId,offset,limit,isActive);
            }
        }

        return hotelList;
    }

}
