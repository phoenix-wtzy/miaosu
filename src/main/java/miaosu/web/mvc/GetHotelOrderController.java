package miaosu.web.mvc;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import miaosu.dao.auto.HotelPriceRefRoomTypeMapper;
import miaosu.dao.auto.HotelRefUserMapper;
import miaosu.dao.auto.HotelRoomSetMapper;
import miaosu.dao.model.*;
import miaosu.svc.hotel.*;
import miaosu.svc.log.OpLogService;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.order.OrderMsgService;
import miaosu.svc.order.OrderOfCaigouoneService;
import miaosu.svc.order.OrderService;
import miaosu.svc.vo.Result;
import miaosu.svc.vo.qhh.OplogVO;
import miaosu.web.mvc.admin.OrderManagerController;
import miaosu.wechat.SendTemplateMessage;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 获取爬虫传递的数据
 */
@Controller
@RequestMapping("/get")
public class GetHotelOrderController {


    private static final Logger logger = LoggerFactory.getLogger(OrderManagerController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderMsgService orderMsgService;

    @Autowired
    private MsgNotifyService msgNotifyService;

    @Autowired
    private HotelRoomCountsService hotelRoomCountsService;

    @Autowired
    private HotelRoomStatusService hotelRoomStatusService;
    @Autowired
    private HotelRoomSetMapper hotelRoomSetMapper;
    @Autowired
    private HotelService hotelService;
    //日志服务类
    @Autowired
    private OpLogService opLogService;
    @Autowired
    private HotelRefUserMapper hotelRefUserMapper;
    @Autowired
    private HotelSupplierService hotelSupplierService;
    @Autowired
    private IHotelPriceSetService hotelPriceSetService;
    @Autowired
    private HotelPriceRefRoomTypeMapper hotelPriceRefRoomTypeMapper;
    @Autowired
    private OrderOfCaigouoneService orderOfCaigouoneService;
    @Autowired
    private HotelRoomPriceService hotelRoomPriceService;
    /*
     * 获取爬虫传递过来的订单数据,并存储到数据库hotel_order和临时表hotel_getOrder中
     * */
    @ResponseBody
    @RequestMapping(value = "/getOrder")
    public Result getOrder(@RequestBody GetHotelOrder hotelGetOrder) throws Exception {

        //根据平台酒店id获取酒店id
        String hotel_only_id = hotelGetOrder.getHotel_only_id();
        Long p_hotel_id= orderService.queryHotelOnlyId(hotel_only_id);

        if (Strings.isNullOrEmpty(hotelGetOrder.getChannelOrderNo())){
            return new Result().failed("渠道订单号不能为空");
        }
        else if (Strings.isNullOrEmpty(hotelGetOrder.getBookTime())){
            return new Result().failed("预定时间不能为空");
        }
        else if (Strings.isNullOrEmpty(hotelGetOrder.getBookUser())){
            return new Result().failed("预订人不能为空");
        }
        else if (null == p_hotel_id){
            return new Result().failed("必须选择酒店");
        }
        else if (Strings.isNullOrEmpty(hotelGetOrder.getP_hotel_room_type_name())){
            return new Result().failed("酒店平台房型名字不能为空");
        }
        else if (null==hotelGetOrder.getNight()){
            return new Result().failed("间夜不能为空");
        }
        else if (Strings.isNullOrEmpty(hotelGetOrder.getCheckInTime())){
            return new Result().failed("入店时间不能为空");
        }
        else if (Strings.isNullOrEmpty(hotelGetOrder.getCheckOutTime())){
            return new Result().failed("离店时间不能为空");
        }
        else if (Strings.isNullOrEmpty(hotelGetOrder.getOrderPrice())){
            return new Result().failed("订单价格不能为空");
        }


        HotelOrder order = new  HotelOrder();
        //先到hotel_supplier表中查询,根据hotel_id和hotel_supplier_state=1
        HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
        HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
        criteria.andhotel_idEqualTo(p_hotel_id).andhotel_supplier_stateEqualTo(1);
        HotelSupplier hotelSupplier = hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
        if (hotelSupplier==null){
            //没有关联,暂时随便设值
            order.setHotel_supplier_id(1L);
            order.setCommission_rate(0);
            order.setSettle_money("");
        }else {
            //酒店关联供应商id
            order.setHotel_supplier_id(hotelSupplier.getHotel_supplier_id());
            //佣金
            int commission_rate = hotelSupplier.getCommission_rate();
            order.setCommission_rate(commission_rate);
            //结算金额=订单实际金额*(1-xx%)     xx是酒店佣金率
            BigDecimal settle_money = new BigDecimal(0);
            String orderPrice = hotelGetOrder.getOrderPrice();
            BigDecimal price00 = new BigDecimal(orderPrice);
            settle_money = price00.multiply(new BigDecimal((100 - commission_rate))).divide(new BigDecimal(100));
            settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
            order.setSettle_money(String.valueOf(settle_money));
        }


        //渠道单号,就是数据的订单号
        order.setOrderNo(hotelGetOrder.getChannelOrderNo());
        //订单渠道,需要根据订单渠道名称获取订单渠道id
        order.setOrderChannel(hotelGetOrder.getOrderChannel());
        //销售类型:1置换/2代销
        order.setSellType(2);
        //预定时间
        try {
            order.setBookTime(DateUtils.parseDate(hotelGetOrder.getBookTime(),"yyyy-MM-dd HH:mm"));
        }catch (Exception e){
            return new Result().failed("预定时间格式错误");
        }
        //预定人
        order.setBookUser(hotelGetOrder.getBookUser());


        //酒店id,根据平台酒店id获取酒店id
        order.setHotelId(p_hotel_id);

        //根据酒店id设置酒店名称
        HotelInfo hotelInfoHotelName= hotelService.getHotelInfoById(p_hotel_id);
        order.setHotelName(hotelInfoHotelName.getHotelName());
        //根据酒店id获取当前用户对应wxOpenid
        List wxOpenidList = hotelRefUserMapper.selectWxOpenidByHotelid(p_hotel_id);

        //设置酒店的间夜数=房间数*间夜(入离时间差)
		Integer night1 = hotelGetOrder.getNight();
		Integer orderRoomCount1 = hotelGetOrder.getOrderRoomCount();
		int nightC = night1 * orderRoomCount1;
		order.setNightC(nightC);

        //直接得到平台酒店的房型名字(爬虫传递的)
        String p_hotel_room_type_name = hotelGetOrder.getP_hotel_room_type_name();
        order.setP_hotel_room_type_name(p_hotel_room_type_name);
        //根据酒店id和平台酒店房型名字到hotel_room_set表中去查询房型hotel_room_type_id
        Long hotelRoomTypeId=2L;

        //当前酒店为郑州正弘城丽呈雅瑟公寓(周日价)
        //并且为周五周六时获取节日价
        boolean holiday = getWeekStr(DateUtils.parseDate(hotelGetOrder.getCheckInTime(),"yyyy-MM-dd"));
        if(holiday && p_hotel_id == 27){
            hotelRoomTypeId=orderService.queryHotelRoomTypeIdByP_hotel_room_type_name(29L,hotelGetOrder.getP_hotel_room_type_name());
        }else{
            hotelRoomTypeId=orderService.queryHotelRoomTypeIdByP_hotel_room_type_name(p_hotel_id,hotelGetOrder.getP_hotel_room_type_name());
        }
        if (hotelRoomTypeId==null){
            //没有找到对应酒店平台房型id就执行添加操作,因为平台上的房型名字五花八门很多
            //将hotel_room_type_id和p_hotel_room_type_name作为新增加入到hotel_room_set表中,hotel_id我们知道,而room_type_id自己随便设置(这里设置为2标准大床房)
            HotelRoomSet record = new HotelRoomSet();
            record.setHotelId(p_hotel_id);
            record.setHotelRoomTypeName(p_hotel_room_type_name);
            record.setRoomTypeId(2);
            hotelRoomSetMapper.insert(record);
            Long hotelRoomTypeId1 = record.getHotelRoomTypeId();
            order.setHotelRoomTypeId(hotelRoomTypeId1);
            //针对新房型,除了添加到hotel_room_set(直接添加);
            // 还要还要添加到hotel_price_set表中(需要判重,因为之前存在过),进行房价日历的展示(需要手动配置价格);
            // 并且添加关联hotel_price_ref_room_type
            List<HotelPriceSet> hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(p_hotel_id, p_hotel_room_type_name);
            if (CollectionUtils.isEmpty(hotelPriceSets)){
                long hotelPriceId =hotelPriceSetService.addPriceType(BigDecimal.valueOf(-1),p_hotel_id, p_hotel_room_type_name, 1, 2);
                Long[]hotelRoomTypeIds={Long.valueOf(hotelRoomTypeId1)};
                hotelPriceSetService.addPriceRefRoomType(hotelPriceId,hotelRoomTypeIds);
            }
        }else {
            order.setHotelRoomTypeId(hotelRoomTypeId);
        }
        //置单和置总用不到,直接-1
        order.setCaigou_one(BigDecimal.valueOf(-1));//每单采购价
        order.setCaigou_total(BigDecimal.valueOf(-1));//总采购价

        //预定几间
        order.setRoomCount(hotelGetOrder.getOrderRoomCount());

        //入住时间
        try {
            order.setCheckInTime(DateUtils.parseDate(hotelGetOrder.getCheckInTime(),"yyyy-MM-dd"));
        }catch (Exception e){
            return new Result().failed("入住日期格式错误");
        }

        //离店时间
        try {
            order.setCheckOutTime(DateUtils.parseDate(hotelGetOrder.getCheckOutTime(),"yyyy-MM-dd"));
        }catch (Exception e){
            return new Result().failed("离店日期格式错误");
        }

        //预定价格
        try{
            BigDecimal price = new BigDecimal(hotelGetOrder.getOrderPrice());
            price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
            order.setPrice(price);
        }catch (Exception e){
            return new Result().failed("订单价格格式错误");
        }


        //备注
        order.setBookRemark(hotelGetOrder.getBookRemark());
        order.setRemark_one(hotelGetOrder.getBookRemark());

        //订单状态:1待确认;2已确认;4已取消,5已确认取消 必须有,这只为1即可
        //只要来订单,就先判断数据库是否存在/构造订单信息,判断重复订单问题
        Integer state = hotelGetOrder.getState();
        HotelOrderExample criteail = new HotelOrderExample();
        criteail.createCriteria().andHotelIdEqualTo(p_hotel_id).andOrderNoEqualTo(hotelGetOrder.getChannelOrderNo());
        List<HotelOrder> hotelOrders = orderService.queryOrderList(criteail);
        //如果订单存在,看状态,只有状态是4的时候进行更新
        if (hotelOrders!=null&&hotelOrders.size()>0){
            //根据渠道单号channelOrderNo查询订单号orderId
            Long orderId=orderService.queryHotelId(hotelGetOrder.getChannelOrderNo());

            //获取最新订单状态
            HotelOrder hotelOrder = orderService.queryOrder(Long.valueOf(orderId));
            Integer nowHotelState = hotelOrder.getState();

            //当订单已经确认取消不更新为取消状态
            if (state==4 && nowHotelState!=5) {
                orderService.updateStatus(orderId, state);
                orderService.updateGetOrderStatus(orderId, state);
                //TODO 发送酒店信息,订单状态改变的弹框提醒
                orderMsgService.sendMsgByOrderId(orderId);
				//将爬虫数据记录到日志中
				opLog_getHotelOrder(hotelGetOrder,orderId);
                //微信公众号推送模板信息
                order.setState(state);
                SendTemplateMessage.sendMessage(order,wxOpenidList);
                return new Result().zhuCeSuccess("这是取消的订单,状态已经改变");
            }
            return new Result().failed("订单已经存在");
        }

        //如果不存在,就新增订单
        if (state==1){
            order.setState(1);
        }
        if (state==2){
            order.setState(1);
        }
        //如果传过来就是4,直接按新增操作,不打电话不发短信
        if (state==4){
            order.setState(4);
        }


//        //售卖时间,引用预定时间(房态和房量都能用到)
//        String checkInTime = hotelGetOrder.getCheckInTime();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        //几号入住就减几号的房间数量,而不是几号预定就减去几号的,有的客人1号预定,6号入住,应该减去6号的房间数量
//        Date sell_date = sdf.parse(checkInTime);
//
//        Integer countRoom= hotelRoomCountsService.findCountRoom(hotelRoomTypeId,sell_date);
//        if (countRoom!=null) {
//            //预定的房间数量
//            Integer orderRoomCount = hotelGetOrder.getOrderRoomCount();
//            //如果剩余房量<=0,就操作关房并打电话给OTA管家,这里的房量对应的指的是订单的入住时间
//            if (countRoom<=0){
//                Map<String,String> params = new HashMap<String, String>(8);
//                params.put("hotel_only_id", hotel_only_id);
//                HotelInfo hotelInfo= hotelService.getHotelInfoById(p_hotel_id);
//                String hotelName = hotelInfo.getHotelName();
//                String ota_phone = hotelInfo.getOta_phone();
//                params.put("hotelName",hotelName);
//                params.put("setStartTime",checkInTime);
//                params.put("setEndTime",checkInTime);
//                params.put("roomStatus", String.valueOf(0));
//                params.put("roomTypeName",p_hotel_room_type_name);
//
//                String url1 = "http://192.168.3.4:8080/dlt_close_room";//代理通
//                String url2 = "http://192.168.3.4:8080/tc_close_room";//同程
////            String url1 = "http://61.52.98.214/dlt_close_room";//代理通
////            String url2 = "http://61.52.98.214/tc_close_room";//同程
//                sendRoomStatus(params,url1);
//                sendRoomStatus(params,url2);
//                //打电话给OTA管家
//                msgNotifyService.sendUserSms(ota_phone,params);
//                //短信通知OTA管家
//                msgNotifyService.sendUserSms2(ota_phone,params);
//            }
//            int roomCount = countRoom - orderRoomCount;
//            //更新房量信息
//            hotelRoomCountsService.update(hotelRoomTypeId, sell_date, roomCount);
//        }

            //添加到数据库hotel_order表中
            long orderId = orderService.addOrder(order);
            //添加到数据库的临时表hotel_getOrder
            orderService.addGetOrder(order);
			//将爬虫数据记录到日志中
			opLog_getHotelOrder(hotelGetOrder,orderId);
            //第一次发短信打电话提醒
            msgNotifyService.sendMsg(p_hotel_id, order);
            //微信公众号推送模板信息
            SendTemplateMessage.sendMessage(order,wxOpenidList);
            orderMsgService.sendMsgByOrderId(orderId);
            insertOrderOfCaigouone(hotelGetOrder, p_hotel_id, p_hotel_room_type_name, orderId);
            return new Result().success();
    }

    public void insertOrderOfCaigouone(@RequestBody GetHotelOrder hotelGetOrder, Long p_hotel_id, String p_hotel_room_type_name, long orderId) throws ParseException {
        /**
         * 每单置单日历,存储到order_caigou_one表中
         * 1.根据酒店id和hotel_price到hotel_price_set中查询hotel_price_id
         * 2.根据hotel_price_id到hotel_price_ref_room_type表中查询product_id(一对一)
         * 3.根据product_id和售卖时间(客人住的日期段)到hotel_room_price去查询price(对应每天的置单)
         * 4.根据order_id/售卖时间(客人住的日期段)/置单/caigou_one_state(默认为0-正常)到order_caigou_one表中进行存储
         */
		Integer orderRoomCount = hotelGetOrder.getOrderRoomCount();
		for (int i = 0; i <orderRoomCount; i++) {
		String checkInTime1 = hotelGetOrder.getCheckInTime();
        String checkOutTime1 = hotelGetOrder.getCheckOutTime();
        Date startDate = DateUtils.parseDate(checkInTime1, "yyyy-MM-dd");
        Date endDate = DateUtils.parseDate(checkOutTime1, "yyyy-MM-dd");
        for (Date d = startDate;d.getTime()<endDate.getTime();d=DateUtils.addDays(d, 1)){
            OrderOfCaigouone orderOfCaigouone = new OrderOfCaigouone();
            orderOfCaigouone.setOrder_id(orderId);
            orderOfCaigouone.setCaigou_one_state(0);
            orderOfCaigouone.setCheck_in_time(d);
            //步骤1
            boolean holiday = getWeekStr(DateUtils.parseDate(hotelGetOrder.getCheckInTime(),"yyyy-MM-dd"));
            List<HotelPriceSet> hotelPriceSets= Lists.newArrayList();
            if(holiday && p_hotel_id == 27){
                hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(29L, p_hotel_room_type_name);
            }else{
                hotelPriceSets = hotelPriceSetService.queryByRoomTypeName(p_hotel_id, p_hotel_room_type_name);
            }
            if (CollectionUtils.isEmpty(hotelPriceSets)){
                orderOfCaigouone.setCaigou_one(BigDecimal.valueOf(-1));
            }else {
                HotelPriceSet hotelPriceSet = hotelPriceSets.get(0);
                Long hotelPriceId = hotelPriceSet.getHotelPriceId();
                //步骤2
                Long productId =hotelPriceRefRoomTypeMapper.queryProductIdByHotelPriceId(hotelPriceId);
                if (null==productId){
                    orderOfCaigouone.setCaigou_one(BigDecimal.valueOf(-1));
                }else{
                    //步骤3
                    BigDecimal price=hotelRoomPriceService.queryPrice(productId,d);
                    if (null==price){
                        orderOfCaigouone.setCaigou_one(BigDecimal.valueOf(-1));
                    }else {
                        orderOfCaigouone.setCaigou_one(price);
                    }
                }
            }
            //步骤4
            orderOfCaigouoneService.insert(orderOfCaigouone);
            //计算采购总价并存储到订单表中
			BigDecimal caigou_total=orderOfCaigouoneService.sumCaigoutotal(orderId,0);
			if (null==caigou_total){
				caigou_total= BigDecimal.valueOf(-1);
			}
			orderService.updateCaigou_total(orderId, caigou_total);
			orderService.updateGetOrderCaigou_total(orderId, caigou_total);
        }
	}
    }


    //将关房数据传递给爬虫人员,让他主动操作关房
    public void sendRoomStatus(Map<String,String> params,String url){
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            String hotel_only_id = params.get("hotel_only_id");//酒店所有平台id:
            String hotelName = params.get("hotelName");//酒店名字:海旺弘亚温泉酒店
            String setStartTime = params.get("setStartTime");//开始年月日:2020-7-29
            String setEndTime = params.get("setEndTime");//结束年月日:2020-8-1
            String roomTypeName = params.get("roomTypeName");//房间类型:豪华标准间
            String roomStatus = params.get("roomStatus");//开关房状态

            MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
            map.add("hotel_only_id", hotel_only_id);
            map.add("hotelName", hotelName);
            map.add("setStartTime", setStartTime);
            map.add("setEndTime", setEndTime);
            map.add("roomTypeName", roomTypeName);
            map.add("roomStatus",roomStatus);

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.postForEntity( url, request , String.class );
        } catch (RestClientException e) {
            //失败不作任何处理,不影响自己的业务
            return;
        }
    }


    //只要是小朱传递的数据,就存储到op_log表中,但是不能影响到订单推送
    public void opLog_getHotelOrder(GetHotelOrder hotelGetOrder, Long orderId){
        //根据平台酒店id获取酒店id
        String hotel_only_id = hotelGetOrder.getHotel_only_id();
        Long p_hotel_id= orderService.queryHotelOnlyId(hotel_only_id);
        Long hotelRoomTypeId=orderService.queryHotelRoomTypeIdByP_hotel_room_type_name(p_hotel_id,hotelGetOrder.getP_hotel_room_type_name());
        //将小朱传递的数据,先存储到日志文件中
        OplogVO oplogVO = new OplogVO();
        //用户id,该酒店的账号在user中的id
        //这里的操作人是ADS推送,在user表中添加user_id=999999999,user_name=ADS推送,user_nick=ADS推送user_passwd=123456,status=0
        //INSERT INTO `user`(`user_id`, `user_name`, `user_nick`, `user_phone`, `user_passwd`, `status`) VALUES (999999999, 'ADS推送', 'ADS推送', '', '123456', 0);
        oplogVO.setUserId(32L);
        //日志操作时间
        Date createDate = new Date();
        oplogVO.setCreateTime(createDate);
        //房型id
        if (hotelRoomTypeId==null){
            //没有对应的房型id就设置为1,1是豪华标准间,不设置会报错,设置为0也会报错
            Long[]roomTypeIds={1L};
            oplogVO.setHotelRoomTypeIds(roomTypeIds);
        }else {
            Long[]roomTypeIds={hotelRoomTypeId};
            oplogVO.setHotelRoomTypeIds(roomTypeIds);
        }
        //售卖时间(入住时间)
        oplogVO.setStartDate(hotelGetOrder.getCheckInTime());
        oplogVO.setEndDate(hotelGetOrder.getCheckInTime());
        //操作内容:hotelGetOrder中的数据,不能用",",因为后面有切割
        String opValue= "▎预定人:"+hotelGetOrder.getBookUser()+
                        "▎订单号:"+orderId+
                        "▎房型:"+hotelGetOrder.getP_hotel_room_type_name()+
                        "▎订单状态:"+hotelGetOrder.getState()+
                        "▎间夜数:"+hotelGetOrder.getNight();
        oplogVO.setOpValue(opValue);
        //操作类型,OpTypeEnum中的code
        Integer state = hotelGetOrder.getState();
        if (state==1){
            oplogVO.setOpType(7);
        }
        if (state==2){
            oplogVO.setOpType(5);
        }
        if (state==4){
            oplogVO.setOpType(6);
        }
        if (state==5){
            oplogVO.setOpType(8);
        }
        //酒店id
        oplogVO.setHotelId(p_hotel_id);
        opLogService.saveOpLogs(oplogVO);
    }

    public Boolean getWeekStr(Date date) {
        Calendar cal = Calendar.getInstance();
        if (date != null) {
            cal.setTime(date);
            int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
            switch (week) {
                case 5:
                case 6:
                    return true;
            }
        }
        return false;
    }
}
