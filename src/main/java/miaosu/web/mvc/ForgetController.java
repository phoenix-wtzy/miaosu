package miaosu.web.mvc;

import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.Result;
import miaosu.utils.CodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.TimeUnit;

/**
 * @author Administrator
 * @Date: 2020/7/9 10:38
 * @Description: 忘记密码,重新设置
 */
@Controller
@RequestMapping("/forget")
public class ForgetController {
	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private UserService userService;

	@Autowired
	private MsgNotifyService msgNotifyService;


	//发送验证码并存储到redis缓存中
	@ResponseBody
	@RequestMapping("sendCode")
	public Result sendCode(@RequestParam(value="Username") String Username){
		//先验证是否存在该账号
		String queryUser_name = userService.queryUser_name(Username);
		if (queryUser_name==null){
			return new Result().failed("此账号不存在,请先注册");
		}
		//生成验证码,并发送给手机号(账号)
		Integer code = CodeUtils.generateCode(4);
		System.out.println("========验证码为:"+code);
		msgNotifyService.sendCode(Username,code);
		//把验证码存储到redis中(手机号为key,验证码为value),有效时间为5分钟
		redisTemplate.boundValueOps(Username).set(String.valueOf(code),5*60, TimeUnit.SECONDS);
		return new Result().zhuCeSuccess("验证码发送成功");
	}



	@ResponseBody
	@RequestMapping("reset")
	public Result reset(@RequestParam(value="Username") String Username,//账号也是手机号
						@RequestParam(value="Password") String Password,
						@RequestParam(value="PhoneCode") String PhoneCode){
		//先验证是否存在该账号
		String queryUser_name = userService.queryUser_name(Username);
		if (queryUser_name==null){
			return new Result().failed("此账号不存在,请先注册");
		}
		//从redis中把手机号和验证码取出来
		Object codeInRedis = redisTemplate.boundValueOps(Username).get();
		if (codeInRedis==null){
			return new Result().failed("验证码失效");
		}

		//再验证输入的验证码是否正确
		if (codeInRedis.equals(PhoneCode)){
			//重置密码
			userService.resetPassword(Password,Username);
		}else {
			return new Result().failed("验证码输入有误");
		}
		return new Result().zhuCeSuccess("您已成功重置密码！");
	}


//	@ResponseBody
//	@RequestMapping("reset")
//	public Result reset( @RequestParam(value="Username") String Username,//账号也是手机号
//						 @RequestParam(value="Password") String Password,
//						 @RequestParam(value="PhoneCode") String PhoneCode){
//		//先验证是否存在该账号
//		String queryUser_name = userService.queryUser_name(Username);
//		if (queryUser_name==null){
//			return new Result().failed("此账号不存在");
//		}
//		//生成验证码,并发送给手机号(账号)
//		Integer code = CodeUtils.generateCode(4);
//		System.out.println("========验证码为:"+code);
//		//msgNotifyService.sendCode(Username,code);
//		//再验证输入的验证码是否正确
//		if (PhoneCode.equals(code)){
//			//重置密码
//			userService.resetPassword(Password,Username);
//		}else {
//			return new Result().failed("验证码输入有误");
//		}
//		return new Result().zhuCeSuccess("您已成功重置密码！");
//	}
}
