package miaosu.web.mvc;

import miaosu.svc.user.UserService;
import miaosu.svc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Administrator
 * @Date: 2020/7/17 15:17
 * @Description: 修改密码
 */
@Controller
@RequestMapping("/changePassword")
public class ChangePasswordController extends BaseController{

	@Autowired
	private UserService userService;

	@ResponseBody
	@RequestMapping("change")
	public Result insert(@RequestParam(value="OldPassword") String OldPassword,
						 @RequestParam(value="Password") String Password,
						 @RequestParam(value="Repassword") String Repassword){
		System.out.println("-------");
		Long currentUserId = getCurrentUserId();
		//根据当前用户id查询密码
		String user_password= userService.queryUser_password(currentUserId);
		if (OldPassword.equals(user_password)){
			//根据用户id修改密码
			userService.updateUser_password(currentUserId,Password);
		}else{
			return new Result().failed("原密码不正确,请重新输入");
		}
		return new Result().zhuCeSuccess("修改密码成功");
	}
}
