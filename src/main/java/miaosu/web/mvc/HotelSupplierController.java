package miaosu.web.mvc;

import com.google.common.base.Strings;
import miaosu.dao.model.HotelSupplier;
import miaosu.dao.model.HotelSupplierExample;
import miaosu.svc.hotel.HotelSupplierService;
import miaosu.svc.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/2/3 11:47
 * @Description: 酒店关联供应商
 */
@Controller
@RequestMapping("/hotelSupplier")
public class HotelSupplierController extends BaseController{


	@Autowired
	private HotelSupplierService hotelSupplierService;

	//查询酒店关联供应列表(分页)
	@ResponseBody
	@RequestMapping("list")
	public Response HotelSupplierList(@RequestParam(value="hotel_id") Long hotel_id,
									  @RequestParam(value="page", required=false,defaultValue = "1") int page,
									  @RequestParam(value="limit", required=false,defaultValue = "10") int limit){
		//查询该酒店对应的所有关联供应商的信息
		List<HotelSupplier> hotelSupplierList = hotelSupplierService.querySupplierListByHotelId(hotel_id,page,limit);
		//查询与本酒店有关的hotel_supplier中总条数
		int count=hotelSupplierService.queryTotalSupplierList(hotel_id);
		return new Response().success("查询酒店关联供应列表",count,hotelSupplierList);
	}

	//添加 酒店关联供应商
	@ResponseBody
	@RequestMapping("add")
	public Response Add(@RequestParam(value="hotel_id") Long hotel_id,//选择的酒店id
						@RequestParam(value="business_id") Long business_id,//选择的供应商id
						@RequestParam(value="commission_rate") String commission_rate,//佣金比例
						@RequestParam(value="settle_type") int settle_type,//置换类型;0--按照金额;1--按间夜
						@RequestParam(value="settle_totalMoney") String settle_totalMoney,//协议置换总金额
						@RequestParam(value="settle_totalNight") String settle_totalNight,//协议置换间夜数
						@RequestParam(value="remark") String remark){//备注

		if(hotel_id==null){
			return new Response().failed("酒店id不能为空");
		}
		else if (business_id==null){
			return new Response().failed("供应商id不能为空");
		}
		else if (Strings.isNullOrEmpty(commission_rate)){
			return new Response().failed("佣金比例不能为空");
		}
		if (settle_type==0){
			 if (Strings.isNullOrEmpty(settle_totalMoney)){
				return new Response().failed("协议置换总金额不能为空");
			}
		}else {
			if (Strings.isNullOrEmpty(settle_totalNight)){
				return new Response().failed("间夜数不能为空");
			}
		}

		HotelSupplier hotelSupplier = new HotelSupplier();
		hotelSupplier.setHotel_id(hotel_id);//酒店ID
		hotelSupplier.setBusiness_id(business_id);//供应商ID
		hotelSupplier.setHotelType(1);//销售类型:1--A类置换;2--B类分销
		hotelSupplier.setCommission_rate(Integer.parseInt(commission_rate));//佣金比例
		hotelSupplier.setSettle_type(settle_type);//置换类型;0--按照金额;1--按间夜
		hotelSupplier.setSettle_totalMoney(settle_totalMoney);//协议置换总金额
		hotelSupplier.setSettle_totalNight(settle_totalNight);//协议置换间夜数
		hotelSupplier.setCreate_time(new Date());//创建时间:酒店和供应商关联的时间
		hotelSupplier.setHotel_supplier_state(0);//状态:0--待上线;1--在线;2--下线 默认是0
		hotelSupplier.setRemark(remark);//备注
		//添加酒店关联供应商信息
		Long hotel_supplier_id=hotelSupplierService.insert(hotelSupplier);
		if (null!=hotel_supplier_id){
			return new Response().success("添加成功");
		}else {
			return new Response().failed("添加失败");
		}

	}

	//修改 酒店关联供应商
	@ResponseBody
	@RequestMapping("update")
	public Response Update(@RequestParam(value="hotel_supplier_id") Long hotel_supplier_id,//主键id
						   @RequestParam(value="commission_rate") String commission_rate,//佣金比例
						   @RequestParam(value="settle_type") int settle_type,//置换类型;0--按照金额;1--按间夜
						   @RequestParam(value="settle_totalMoney") String settle_totalMoney,//协议置换总金额
						   @RequestParam(value="settle_totalNight") String settle_totalNight,//协议置换间夜数
						   @RequestParam(value="remark") String remark) {//备注
		if(hotel_supplier_id==null){
			return new Response().failed("主键id不能为空");
		}
		else if (Strings.isNullOrEmpty(commission_rate)){
			return new Response().failed("佣金比例不能为空");
		}
		if (settle_type==0){
			if (Strings.isNullOrEmpty(settle_totalMoney)){
				return new Response().failed("协议置换总金额不能为空");
			}
		}else {
			if (Strings.isNullOrEmpty(settle_totalNight)){
				return new Response().failed("间夜数不能为空");
			}
		}
		HotelSupplier hotelSupplier = new HotelSupplier();
		hotelSupplier.setHotel_supplier_id(hotel_supplier_id);
		hotelSupplier.setHotelType(1);//销售类型:1--A类置换;2--B类分销
		hotelSupplier.setCommission_rate(Integer.parseInt(commission_rate));
		hotelSupplier.setSettle_type(settle_type);
		//针对置换类型settle_type;0--按照金额;1--按间夜
		HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
		HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
		criteria.andhotel_supplier_idEqualTo(hotel_supplier_id);
		HotelSupplier hotelSupplier1 = hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
		int hotel_supplier_state1 = hotelSupplier1.getHotel_supplier_state();
		if (hotel_supplier_state1==1){
			return new Response().failed("在售状态不能修改");
		}
		int settle_type1 = hotelSupplier1.getSettle_type();
		if (settle_type==settle_type1){
			hotelSupplier.setSettle_totalMoney(settle_totalMoney);
			hotelSupplier.setSettle_totalNight(settle_totalNight);
		}else {
			if (settle_type==0){
				hotelSupplier.setSettle_totalMoney(settle_totalMoney);
				hotelSupplier.setSettle_totalNight("");
			}else {
				hotelSupplier.setSettle_totalMoney("");
				hotelSupplier.setSettle_totalNight(settle_totalNight);
			}
		}
		hotelSupplier.setRemark(remark);

		//先查询,针对状态:之前是什么状态,现在就是什么状态,状态是不能修改的
		int hotel_supplier_state = hotelSupplier1.getHotel_supplier_state();
		hotelSupplier.setHotel_supplier_state(hotel_supplier_state);

		//修改
		int row=hotelSupplierService.update(hotelSupplier);
		if(row>0){
			return new Response().success("修改成功");
		}else {
			return new Response().failed("修改失败");
		}
	}

	//删除
	@ResponseBody
	@RequestMapping("delete")
	public Response Delete(@RequestParam(value="hotel_supplier_id") Long hotel_supplier_id){//主键id
		if(hotel_supplier_id==null){
			return new Response().failed("主键id不能为空");
		}
		//下面的暂时注释掉,暂时设置不能删除,后期可以删除了再打开注释即可,代码可用
//		HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
//		HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
//		criteria.andhotel_supplier_idEqualTo(hotel_supplier_id);
//		//根据封装条件查询hotel_supplier表中是否存在当前项
//		HotelSupplier hotelSupplier=hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
//		if (null==hotelSupplier){
//			return new Response().failed("不存在此关联");
//		}
//
//		//根据主键id删除
//		int row=hotelSupplierService.deleteByHotel_supplier_id(hotel_supplier_id);
//		if(row>0){
//			return new Response().success("删除成功");
//		}else {
//			return new Response().failed("删除失败");
//		}
		return new Response().failed("暂时设置不能删除");
	}

	//上下线(hotel_supplier_state状态:0待上线/1在线/2下线 默认是0)
	@ResponseBody
	@RequestMapping("updateHotelSupplierState")
	public Response UpdateHotelSupplierState(@RequestParam(value="hotel_supplier_id") Long hotel_supplier_id,
											 @RequestParam(value="hotel_supplier_state") int hotel_supplier_state){
		if(hotel_supplier_id==null){
			return new Response().failed("主键id不能为空");
		}

		//上线
		if (hotel_supplier_state==0){
			//上线之前先查询有没有已经在线的,酒店关联供应商在同一时间内有且只能有一个关联
			HotelSupplierExample hotelSupplierExample = new HotelSupplierExample();
			HotelSupplierExample.Criteria criteria = hotelSupplierExample.createCriteria();
			criteria.andhotel_supplier_idEqualTo(hotel_supplier_id);
			HotelSupplier hotelSupplier = hotelSupplierService.queryHotelSupplier(hotelSupplierExample);
			if (hotelSupplier==null){
				return new Response().failed("不存在此关联");
			}
			boolean ifExit=hotelSupplierService.queryHotelSupplierState(hotelSupplier.getHotel_id());
			if (ifExit){
				return new Response().failed("上线失败,该酒店同一时间只能存在一个关联");
			}
			hotel_supplier_state=1;
			Date start_time=new Date();
			int row=hotelSupplierService.updateHotelSupplierStateUP(hotel_supplier_id,hotel_supplier_state,start_time);
			if(row>0){
				return new Response().success("上线成功");
			}else {
				return new Response().failed("上线失败");
			}
		}
		//下线
		if (hotel_supplier_state==1){
			hotel_supplier_state=2;
			Date end_time=new Date();
			int row=hotelSupplierService.updateHotelSupplierStateDown(hotel_supplier_id,hotel_supplier_state,end_time);
			if(row>0){
				return new Response().success("下线成功");
			}else {
				return new Response().failed("下线失败");
			}
		}
		if (hotel_supplier_state==2){
			return new Response().failed("不能再修改");
		}
		return null;
	}

}
