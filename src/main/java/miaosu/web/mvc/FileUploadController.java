package miaosu.web.mvc;



import miaosu.svc.order.OrderImportService;
import miaosu.utils.ConstantUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ui.Model;



/**
 * Created by Administrator on 2018/6/19.
 */

/**
 * 订单导入
 *
 * @author zyx
 * @date 2018/06/22
 * */
@Controller
@RequestMapping("/upload")
public class FileUploadController {


    @Autowired
    OrderImportService orderImportService;

    @GetMapping("/import")
    public String hotel(Model model) {
            model.addAttribute("result",null);
        return "upload";
    }

    /**
     * 上传Excel并返回信息
     * @return
     */
    @PostMapping(value="/uploadExcel")
    public String uploadFileJson(MultipartFile file,Model model) throws  Exception{
        String result = "";
        if(file.getSize()!=0){
            result  =  orderImportService.importToOrder(file);
        }else{
            result = ConstantUtil.UPLOADFILE_EMPTYERROR_MESSAGE;
        }
        model.addAttribute("result",result);
        return "upload";
    }

}
