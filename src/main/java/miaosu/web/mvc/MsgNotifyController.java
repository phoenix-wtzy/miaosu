package miaosu.web.mvc;
import miaosu.dao.model.HotelInfo;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.vo.MessageNotifyVO;
import miaosu.svc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by xieqx on 2019/1/19.
 */
/**
 * @author xieqx
 * @date 2018/06/22
 * */
@Controller
@RequestMapping("/message")
public class MsgNotifyController extends  BaseController {
    @Value("${wechat.url}")
    String wechatUrl;

    @Autowired
    MsgNotifyService msgNotifyService;


    /**
     * 进入到酒店通知设置的首页面
     * @param model
     * @return
     */
    @GetMapping("/notifySetting")
    public String notifySetting(Model model) {
        Long hotelId = getCurHotel();
        if (hotelId==null){
            model.addAttribute("error", "无操作权限");
            return "error/404";
        }
        HotelInfo hotel = checkHotelInfoWithPermit(hotelId);

        if (null == hotel){
            model.addAttribute("error", "无操作权限");
            return "forward:/403";
        }else {
            model.addAttribute("hotelInfo", hotel);
            model.addAttribute("wechatUrl", wechatUrl);
            return "notifySetting";
        }
    }

    /**
     *酒店通知设置的初始化信息
     * @param hotelId
     * @return
     */
    @ResponseBody
    @RequestMapping("/msgInit")
    public MessageNotifyVO msgInit(Long hotelId) {
        return msgNotifyService.msgNotifyInit(hotelId);
    }

    /**
     * 添加短信通知的酒店信息
     * @param phone 手机号 单个
     * @return
     */
    @ResponseBody
    @RequestMapping("/phone/add")
    public Result phoneAdd(String phone) {

        Long hotelId = getCurHotel();
        return  msgNotifyService.smsPhoneAdd(hotelId,phone);

    }

    /**
     * 删除手机号
     * @param phone
     * @return
     */
    @ResponseBody
    @RequestMapping("/phone/delete")
    public boolean phoneDelete(String phone) {
        Long hotelId = getCurHotel();
        return msgNotifyService.smsPhoneDelete(hotelId,phone);
    }


    /**
     *添加语音通知的手机号、座机号
     * @param phone 手机号、座机号
     * @return
     */
    @ResponseBody
    @RequestMapping("/voicephone/add")
    public Result voicephoneAdd(String phone) {

        Long hotelId = getCurHotel();
        return msgNotifyService.voicePhoneAdd(hotelId,phone);

    }

    /**
     *删除语音通知的手机号、座机号
     * @param phone 手机号、座机号
     * @return
     */
    @ResponseBody
    @RequestMapping("/voicephone/delete")
    public boolean voicephoneDelete(String phone) {
        Long hotelId = getCurHotel();
        return msgNotifyService.voicePhoneDelete(hotelId,phone);
    }

}
