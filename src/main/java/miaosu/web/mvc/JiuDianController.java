package miaosu.web.mvc;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import miaosu.common.SettleStatusEnum;
import miaosu.dao.auto.HotelInfoMapper;
import miaosu.dao.auto.HotelRoomSetMapper;
import miaosu.dao.auto.UserCodeMapper;
import miaosu.dao.auto.UserRoleMapper;
import miaosu.dao.model.*;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.order.OrderService;
import miaosu.svc.settle.GongYingShangService;
import miaosu.svc.settle.HotelSettleService;
import miaosu.svc.user.BusinessService;
import miaosu.svc.vo.OrderDetailList;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Date: 2020/8/17 15:19
 * @Description: 三级酒店对应的结算
 */

@Controller
@RequestMapping("jiudian/settle")
public class JiuDianController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
	@Autowired
	private HotelSettleService settleService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private HotelRoomSetMapper hotelRoomSetMapper;
	@Autowired
	private UserRoleMapper userRoleMapper;
	@Autowired
	private BusinessService businessService;
	@Autowired
	private HotelService hotelService;

	@Autowired
	private HotelInfoMapper hotelInfoMapper;
	@Autowired
	private UserCodeMapper userCodeMapper;
	@Autowired
	private GongYingShangService gongYingShangService;


	@GetMapping("")
	public String settle(Model model) {
		//超级管理员
		Long currentUserId = getCurrentUserId();
		Date now = new Date();
		String date_end = DateFormatUtils.format(now,"yyyy-MM-dd");
		model.addAttribute("search_date", ImmutableMap.of("date_end",date_end));
		model.addAttribute("channel",currentUserId);
		//根据当前id查询角色,针对超级管理员
		List<Long> queryUser_Role = userRoleMapper.queryUser_Role(currentUserId);
		for (Long aLong : queryUser_Role) {
			if (aLong==1){
				return "settle_super";
			}
			//结算页面给运营角色看
			if (aLong==5){
				return "settle_super";
			}
		}
		//二级供应商
		boolean isAdmin = isAdmin();
		if (isAdmin){
			return "settle_gongyingshang";
		}
		Long hotelId =  getCurHotel();
		if (hotelId==null){
			model.addAttribute("error", "无操作权限");
			return "error/404";
		}
		HotelSettleSetting settleSetting = settleService.queryHotelSettle(hotelId);
		if(settleSetting == null){
			return "error/lackSettle";
		}
		//三级酒店
		return "settle_jiudian";
	}

	//展示酒店结算
	@ResponseBody
	@RequestMapping("list")
	public List<JiuDian> jiudianList(@RequestParam(value="offset", required=false,defaultValue = "1") int offset,
										@RequestParam(value="limit", required=false,defaultValue = "20") int limit){
		Long currentUserId = getCurrentUserId();
		//根据当前登录用户id查询酒店id
		Long hotelId= hotelService.queryHotelIdByUserId(currentUserId);
		//PageResult jiudianList = userService.queryjiudianListBypage(hotelId,offset,limit);
		List<JiuDian> jiuDians = null;
		try {
			jiuDians = userService.queryjiudianListList(hotelId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jiuDians;
	}
	//展示酒店订单详细列表
	@ResponseBody
	@RequestMapping("orderList")
	public List<OrderDetailList> orderList(
			@RequestParam(value="user_id", required=true) Long user_id,
			@RequestParam(value="setSettle_StartAndEnd", required=true) String Settle_StartAndEnd)throws Exception{
		if(user_id == null){
			return null;
		}
		List<OrderDetailList> orderDetailLists = Lists.newArrayList();
		//时间段
		Long settle_StartDateTime=null;
		Long settle_endDateTime=null;
		String[] split = Settle_StartAndEnd.split("~");
		SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
		String settle_Start = split[0];//20200101
		Date settle_StartDate = yyyyMMdd.parse(settle_Start);
		settle_StartDateTime = settle_StartDate.getTime();
		String settle_End = split[1];//20200105
		Date settle_EndDate = yyyyMMdd.parse(settle_End);
		settle_endDateTime = settle_EndDate.getTime();

		//根据当前登录用户id查询酒店id
		Long hotelId= hotelService.queryHotelIdByUserId(user_id);
		HotelOrderExample hotelOrderExample = new HotelOrderExample();
		HotelOrderExample.Criteria and = hotelOrderExample.createCriteria();
		and.andHotelIdEqualTo(hotelId);
		List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);
		for (HotelOrder hotelOrder : hotelOrders) {
			OrderDetailList orderDetailList = new OrderDetailList();
			//入住时间
			Date checkInTime = hotelOrder.getCheckInTime();
			long checkInTimeTime = checkInTime.getTime();
			//订单状态
			Integer state = hotelOrder.getState();
			//订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
			if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime) {
				//日期转字符串
				SimpleDateFormat simpleDateFormat_book = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat simpleDateFormat_inAndOut = new SimpleDateFormat("yyyy-MM-dd");

				orderDetailList.setOrderNo(hotelOrder.getOrderNo());

				orderDetailList.setBookTime(simpleDateFormat_book.format(hotelOrder.getBookTime()));
				orderDetailList.setBookUser(hotelOrder.getBookUser());
				Long hotelRoomTypeId = hotelOrder.getHotelRoomTypeId();
				String roomTypeName = hotelRoomSetMapper.queryHotelRoomTypeName(hotelRoomTypeId);
				orderDetailList.setHotelRoomType(roomTypeName);
				orderDetailList.setRoomCount(hotelOrder.getRoomCount());
				orderDetailList.setCheckInTime(simpleDateFormat_inAndOut.format(hotelOrder.getCheckInTime()));
				orderDetailList.setCheckOutTime(simpleDateFormat_inAndOut.format(hotelOrder.getCheckOutTime()));
				orderDetailList.setPrice(hotelOrder.getPrice());
				orderDetailList.setBookRemark(hotelOrder.getBookRemark());
				orderDetailList.setSettleStatus(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
				orderDetailLists.add(orderDetailList);
			}
		}
		return orderDetailLists;
	}
	//展示供应商结算
	@ResponseBody
	@RequestMapping("list_hotel")
	public List<GongYingShang>  gongyingshangList(@RequestParam(value="offset", required=false,defaultValue = "1") int offset,
												  @RequestParam(value="limit", required=false,defaultValue = "20") int limit){
		Long currentUserId = getCurrentUserId();
//		PageResult gongyingshangList = userService.querygongyingshangListBypage(currentUserId,offset,limit);
		List<GongYingShang> gongYingShangs = null;
		try {
			gongYingShangs = userService.querygongyingshangList(currentUserId);
		} catch (Exception e) {
			e.printStackTrace();
		}


		return gongYingShangs;
	}

	//展示酒店列表+对应的订单信息
	@ResponseBody
	@RequestMapping("hotelAndOrderList")
	public List<GongYingShang_hotelAndOrderList> hotelAndOrderList(
			@RequestParam(value="business_id", required=true) Long business_id,
			@RequestParam(value="setSettle_StartAndEnd", required=true) String settle_StartAndEnd) throws Exception{
		if(business_id == null){
			return null;
		}
		List<GongYingShang_hotelAndOrderList> hotelAndOrderList = Lists.newArrayList();
		//根据公司id查询公司信息
		Business business= businessService.queryBusinessByBusiness_id(business_id);
		Long user_id=null;
		if (business==null){
			//获取的可能不是公司id,而是用户id,直接赋值即可
			 user_id =business_id;
		}else{
			//获取的是公司id,再去获取用户id
			 user_id = business.getUser_id();
		}
		//计算佣金率
		Integer count=0;
		Integer yongjinglv=1;
		//根据当前用户id查询自己的邀请码
		String code_my = userCodeMapper.queryCode_my(user_id);
		//根据我的邀请码查询下家的邀请码所对应的用户id
		List<Long> userIds = userCodeMapper.queryCode_xiajia(code_my);
		for (Long userId : userIds) {
			//根据成员id查询邀请码id
			Long code_id = userCodeMapper.queryCode_id(userId);
			//根据邀请码id查询code_level的状态
			String code_level = userCodeMapper.queryCode_level(code_id);
			if (code_level.contains("已通过")){
				count++;
			}
		}
		//上海苜宿酒店管理有限公司供应商的结算按照13%计算,这里设死,根据邀请码(唯一)去定位供应商,不能根据公司id,因为这里的business_id有可能是user_id
		if(code_my.contains("7ea21c")){
			yongjinglv=13;
		}else {
			if(count<=15){
				yongjinglv=15;
			}
			if(count>=16&&count<=30){
				yongjinglv=14;
			}
			if(count>=31&&count<=50){
				yongjinglv=13;
			}
			if(count>=51){
				yongjinglv=12;
			}
		}

		//根据用户id查询酒店id集合
		List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(user_id);
		for (HotelRefUser hotelRefUser : hotelRefUsers) {
			GongYingShang_hotelAndOrderList gongYingShang_hotelAndOrderList = new GongYingShang_hotelAndOrderList();
			Long hotelId = hotelRefUser.getHotelId();
			//酒店id
			gongYingShang_hotelAndOrderList.setP_hotel_id(hotelId);
			//根据酒店id查询酒店信息
			HotelInfo hotelInfo = hotelInfoMapper.selectByPrimaryKey(hotelId);
			//酒店名字
			gongYingShang_hotelAndOrderList.setHotel_name(hotelInfo.getHotelName());
			Long settle_StartDateTime=null;
			Long settle_endDateTime=null;
			String[] split = settle_StartAndEnd.split("~");
			SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy年MM月dd日");
			String settle_Start = split[0];//20200101
			Date settle_StartDate = yyyyMMdd.parse(settle_Start);
			settle_StartDateTime = settle_StartDate.getTime();
			String settle_End = split[1];//20200105
			Date settle_EndDate = yyyyMMdd.parse(settle_End);
			settle_endDateTime = settle_EndDate.getTime();
			//结算金额=总金额*xx%
			BigDecimal settleTotalMoney = new BigDecimal(0);//总金额
			BigDecimal settle_money = new BigDecimal(0);//结算金额
			BigDecimal commission_money = new BigDecimal(0);//技术服务费
			//根据酒店id查询所有对应的订单列表,很多个
			HotelOrderExample hotelOrderExample = new HotelOrderExample();
			HotelOrderExample.Criteria and1 = hotelOrderExample.createCriteria();
			and1.andHotelIdEqualTo(hotelId);
			List<HotelOrder> hotelOrders = orderService.queryOrderList(hotelOrderExample);

			//创建订单详情列表集合
			List<OrderDetailList> orderDetailList= Lists.newArrayList();
			for (HotelOrder hotelOrder : hotelOrders) {
				//创建详情列表封装类
				OrderDetailList orderDetailList1 = new OrderDetailList();
				Long orderId = hotelOrder.getOrderId();
				//入住时间
				Date checkInTime = hotelOrder.getCheckInTime();
				long checkInTimeTime = checkInTime.getTime();
				//订单状态
				Integer state = hotelOrder.getState();
				Integer settleStatus = hotelOrder.getSettleStatus();
				//订单状态是已确认的,结算状态是未结算的,且订单有一定的时间范围
				if (state==2&& settle_StartDateTime<=checkInTimeTime&&checkInTimeTime<=settle_endDateTime) {
					//日期转字符串
					SimpleDateFormat simpleDateFormat_book = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat simpleDateFormat_inAndOut = new SimpleDateFormat("yyyy-MM-dd");
					//设置详情列表封装类中对象的值
					orderDetailList1.setOrderNo(hotelOrder.getOrderNo());
					orderDetailList1.setBookTime(simpleDateFormat_book.format(hotelOrder.getBookTime()));
					orderDetailList1.setBookUser(hotelOrder.getBookUser());
					Long hotelRoomTypeId = hotelOrder.getHotelRoomTypeId();
					String roomTypeName = hotelRoomSetMapper.queryHotelRoomTypeName(hotelRoomTypeId);
					orderDetailList1.setHotelRoomType(roomTypeName);
					orderDetailList1.setRoomCount(hotelOrder.getRoomCount());
					orderDetailList1.setCheckInTime(simpleDateFormat_inAndOut.format(hotelOrder.getCheckInTime()));
					orderDetailList1.setCheckOutTime(simpleDateFormat_inAndOut.format(hotelOrder.getCheckOutTime()));
					orderDetailList1.setPrice(hotelOrder.getPrice());
					orderDetailList1.setBookRemark(hotelOrder.getBookRemark());
					orderDetailList1.setSettleStatus(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());

					BigDecimal price = hotelOrder.getPrice();
					settleTotalMoney = settleTotalMoney.add(price);
					settleTotalMoney=settleTotalMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
					settle_money=settleTotalMoney.multiply(new BigDecimal((100-yongjinglv))).divide(new BigDecimal(100));
					settle_money=settle_money.setScale(2, BigDecimal.ROUND_HALF_UP);
					commission_money=settleTotalMoney.multiply(new BigDecimal(yongjinglv)).divide(new BigDecimal(100));
					commission_money=commission_money.setScale(2, BigDecimal.ROUND_HALF_UP);
					//结算金额就是下面的
					gongYingShang_hotelAndOrderList.setSettle_money(String.valueOf(settle_money));
					//5.技术服务费=总金额*xx%
					gongYingShang_hotelAndOrderList.setCommission_money(String.valueOf(commission_money));
					//6.结算状态
					gongYingShang_hotelAndOrderList.setSettle_status(SettleStatusEnum.fromCode(hotelOrder.getSettleStatus()).getName());
					//将详情列表封装类添加到详情列表集合中
					orderDetailList.add(orderDetailList1);
					//设置详情列表集合数据到gongYingShang_hotelAndOrderList封装类中
					gongYingShang_hotelAndOrderList.setOrderDetailList(orderDetailList);
				}
			}
			hotelAndOrderList.add(gongYingShang_hotelAndOrderList);
		}
		return hotelAndOrderList;
	}











	//展示超级管理员结算
	@ResponseBody
	@RequestMapping("list_super")
	public List<GongYingShang_super> superList(){
		List<GongYingShang_super> list = Lists.newArrayList();
		//查询所有已通过的公司信息
		List<Business> businesses = businessService.queryBusinessInsertList();
		for (Business business : businesses) {
			GongYingShang_super gongYingShang_super = new GongYingShang_super();
			//查询通过与否
			Long code_id = userCodeMapper.queryCode_id(business.getUser_id());
			String code_level = userCodeMapper.queryCode_level(code_id);
			if (StringUtils.isEmpty(code_level)){
				System.out.println("没有code_level");
			}else {
				if (code_level.contains("已通过")){
					gongYingShang_super.setBusiness_name(business.getBusiness_name());
					gongYingShang_super.setBusiness_id(business.getBusiness_id());
					gongYingShang_super.setUser_id(business.getUser_id());
					gongYingShang_super.setLegal_person(business.getLegal_person());
					gongYingShang_super.setLegal_phone(business.getLegal_phone());
					gongYingShang_super.setBusiness_phone(business.getBusiness_phone());
					gongYingShang_super.setBusiness_email(business.getBusiness_email());
					gongYingShang_super.setBusiness_address(business.getBusiness_address());
					gongYingShang_super.setFinance_person(business.getFinance_person());
					gongYingShang_super.setFinance_phone(business.getFinance_phone());
					gongYingShang_super.setBank_type(business.getBank_type());
					gongYingShang_super.setBank_number(business.getBank_number());
					gongYingShang_super.setBank_addresss(business.getBank_addresss());
					gongYingShang_super.setAccount_name(business.getAccount_name());
					gongYingShang_super.setAccount_number(business.getAccount_number());
					list.add(gongYingShang_super);
				}
			}
		}
		return list;
	}
	//展示超级管理员详情页
	@ResponseBody
	@RequestMapping("list_superOfdetail")
	public List<GongYingShang>  gongyingshangList(@RequestParam(value="business_id", required=true) Long business_id){
		List<GongYingShang> list = Lists.newArrayList();
//		List<String> everyWeekOfYear = GetEveryWeekOfYearUtil.getEveryWeekOfYear();
//		for (String settle_StartAndEnd : everyWeekOfYear) {
//			GongYingShang gongYingShang = gongYingShangService.queryAll(settle_StartAndEnd, business_id);
//			if (gongYingShang != null) {
//				list.add(gongYingShang);
//			}
//		}
		//不用遍历日期,直接根据business_id获取静态表
		List<GongYingShang>gongYingShangList=gongYingShangService.queryAllGongYingShang(business_id);
		for (GongYingShang gongYingShang : gongYingShangList) {
			if (gongYingShang != null) {
				list.add(gongYingShang);
			}
		}
		//这里引用倒序排列
		gongYingShang(list);
		return list;
	}
	//将list中的对象中的某个字段按照倒序排列(供应商的倒序)
	private List<GongYingShang> gongYingShang(List<GongYingShang> list) {
		Collections.sort(list, new Comparator<GongYingShang>() {
			@Override
			public int compare(GongYingShang o1, GongYingShang o2) {
				//降序
				return String.valueOf(o2.getSettle_StartAndEnd()).compareTo(String.valueOf(o1.getSettle_StartAndEnd()));
			}
		});
		return list;
	}
}
