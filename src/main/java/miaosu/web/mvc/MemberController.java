package miaosu.web.mvc;

import miaosu.svc.vo.PageResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Administrator
 * @Date: 2020/7/20 13:31
 * @Description: 成员列表
 */
@Controller
@RequestMapping("/member")
public class MemberController extends BaseController{

	@ResponseBody
	@RequestMapping("list")
	public PageResult hotelList(@RequestParam(value="offset", required=false,defaultValue = "1") int offset,
								@RequestParam(value="limit", required=false,defaultValue = "20") int limit){
		Long currentUserId = getCurrentUserId();
		PageResult memberList = userService.queryMemberListBypage(currentUserId,offset,limit);
		return memberList;
	}
}
