package miaosu.web.mvc;

import lombok.extern.slf4j.Slf4j;
import miaosu.dao.model.HotelInfo;
import miaosu.dao.model.HotelRefUser;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.HoteInfoVO;
import miaosu.svc.vo.Result;
import miaosu.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by jiajun.chen on 2018/4/11.
 */

@Controller
@RequestMapping("/user")
@Slf4j
public class UserSettingController extends BaseController {
    @Autowired
    private HotelService hotelService;
	@Autowired
	private UserService userService;
    /**
     * 获取当前酒店信息
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("setting/cur/hotel")
    public Result getUserCurHotel(HttpServletRequest request){

		String isActive = request.getParameter("choose_hotel_type");
		//当前酒店id
		Long curHotel = getCurHotel();
		//当前用户id
		Long currentUserId = getCurrentUserId();


		HotelInfo hotelInfoById=null;
		//isActive是null,表示返回的是酒店页面,其他的就是admin和供应商页面
		if (null==isActive){
			//如果当前是酒店页面(没有条件,直接根据酒店id查询并返回)
			hotelInfoById = hotelService.getHotelInfoById(curHotel);
		}

		//admin和供应商页面
		else {
			//admin页面,只根据isActive属性去查询酒店之一并返回
			if (currentUserId==1){
				Integer isActive1 = Integer.valueOf(isActive);
				//对isActive1属性做判断
				//针对order.html中默认是选择全部酒店的时候,要查询所有酒店并返回,不用根据isActive属性
				if (isActive1==2){
					hotelInfoById = hotelService.getHotelInfoById(curHotel);
				}else {
					hotelInfoById = hotelService.getHotelInfoByIdAndisActive(Integer.valueOf(isActive));
				}
			}
			//供应商页面(需要考虑酒店id和isActive两个属性)
			else {
				//根据当前用户查询名下的酒店id列表集合
				List<HotelRefUser> hotelRefUsers = hotelService.queryHotelRefUserByUser(currentUserId);
				for (HotelRefUser hotelRefUser : hotelRefUsers) {
					Long hotelId = hotelRefUser.getHotelId();
					//根据该用户名下酒店id去查询酒店信息
					HoteInfoVO hoteInfoVO = hotelService.queryHotelInfoById(hotelId);
					Integer isActive1 = Integer.valueOf(isActive);
					//对isActive1属性做判断
					//针对order.html中默认是选择全部酒店的时候,要查询所有酒店并返回,不用根据isActive属性
					if (isActive1==2){
						hotelInfoById = hotelService.getHotelInfoById(hotelId);
					}else {
						//根据isActive1的属性条件,在对应用户名下的酒店中去选择对应isActive属性的酒店信息去返回
						if (isActive1.equals(hoteInfoVO.getIsActive())){
							hotelInfoById = hotelService.getHotelInfoById(hotelId);
						}
					}
				}
			}
		}


        log.info("酒店信息：{}", JsonUtils.objectToJson(hotelInfoById));
        return new Result().success().setData(hotelInfoById);
    }

    /**
     * 设置当前用户关联的酒店信息，用户下面有多个酒店默认显示该功能设置的酒店信息
     * @param request
     * @param hotelId 酒店id
     * @return
     */
    @ResponseBody
    @RequestMapping("setting/cur/hotel/set")
    public Result setUserCurHotel(HttpServletRequest request,
                                  @RequestParam(value="hotelId", required=true) Long hotelId
                                  ){

    	//当前用户id
        Long currentUserId = getCurrentUserId();
        userService.setForCurHotel(currentUserId,hotelId);

        return new Result().success();


    }
}
