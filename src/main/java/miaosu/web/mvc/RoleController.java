package miaosu.web.mvc;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import miaosu.dao.auto.RoleMapper;
import miaosu.dao.model.Role;
import miaosu.dao.model.RoleExample;
import miaosu.svc.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Administrator
 * @Date: 2021/1/19 17:02
 * @Description: 角色管理
 */
@Controller
@RequestMapping("/roles")
@Slf4j
public class RoleController extends BaseController{

	@Autowired
	private RoleMapper roleMapper;

	//查询所有的角色
	@ResponseBody
	@RequestMapping("list")
	public Response RoleList(){
		RoleExample example = new RoleExample();
		RoleExample.Criteria criteria = example.createCriteria();
		List<Role> roles = roleMapper.selectByExample(example);
		return new Response().success("查询所有角色",roles.size(),roles);
	}

	//添加角色
	@ResponseBody
	@RequestMapping("add")
	public Response Add(@RequestParam(value="role_name") String role_name,//角色编码
						@RequestParam(value="role_description") String role_description){//角色描述
		if (Strings.isNullOrEmpty(role_name)){
			return new Response().failed("角色编码不能为空");
		}
		else if (Strings.isNullOrEmpty(role_description)){
			return new Response().failed("角色描述不能为空");
		}
		RoleExample example = new RoleExample();
		example.createCriteria().andRoleNameEqualTo(role_name);
		List<Role> roles = roleMapper.selectByExample(example);
		if (roles.size()>0){
			return new Response().failed("角色已经存在,不能重复添加");
		}
		Role role = new Role();
		role.setRoleName(role_name);
		role.setRole_description(role_description);
		roleMapper.insert(role);
		return new Response().success("添加角色成功");
	}

	//更新角色
	@ResponseBody
	@RequestMapping("update")
	public Response Update(@RequestParam(value="role_id") Long role_id,
						   @RequestParam(value="role_description") String role_description){
		if (null == role_id){
			return new Response().failed("角色id不能为空");
		}
		else if (Strings.isNullOrEmpty(role_description)){
			return new Response().failed("角色描述不能为空");
		}
		Role role = roleMapper.selectByPrimaryKey(role_id);
		if (null==role){
			return new Response().failed("角色不存在,不能修改");
		}
		Role role1 = new Role();
		role1.setRoleId(role_id);
		role1.setRole_description(role_description);
		int i = roleMapper.updateByPrimaryKeySelective(role1);
		if (i>0){
			return new Response().success("更新角色成功");
		}
		return null;
	}

	//删除角色
	@ResponseBody
	@RequestMapping("delete")
	public Response Delete(@RequestParam(value="role_id") Long role_id){
		if (null == role_id){
			return new Response().failed("角色id不能为空");
		}
		Role role = roleMapper.selectByPrimaryKey(role_id);
		if (null==role){
			return new Response().failed("角色不存在,不能删除");
		}
		int i = roleMapper.deleteByPrimaryKey(role_id);
		if (i>0){
			return new Response().success("删除角色成功");
		}
		return null;
	}
}
