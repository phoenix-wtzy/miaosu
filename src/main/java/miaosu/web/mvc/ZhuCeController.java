package miaosu.web.mvc;

import miaosu.common.RoleEnum;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.user.CodeService;
import miaosu.svc.user.UserCodeService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.Result;
import miaosu.utils.CodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.TimeUnit;

/**
 * @author Administrator
 * @Date: 2020/7/3 18:16
 * @Description: 用户注册
 */
@Controller
@RequestMapping("/zhuce")
public class ZhuCeController {

    @Autowired
    private UserService userService;

    @Autowired
    private CodeService codeService;

    @Autowired
    private UserCodeService userCodeService;

    @Autowired
    private MsgNotifyService msgNotifyService;

    @Autowired
    private RedisTemplate redisTemplate;




    //发送验证码并存储到redis缓存中
    @ResponseBody
    @RequestMapping("sendCode")
    public Result sendCode(@RequestParam(value="Username") String Username){
        //先验证是否存在该账号
        String queryUser_name = userService.queryUser_name(Username);
        if (queryUser_name!=null){
            return new Result().failed("此账号已存在,请登录");
        }
        //生成验证码,并发送给手机号(账号)
        Integer code = CodeUtils.generateCode(4);
        System.out.println("========验证码为:"+code);
        msgNotifyService.sendCode(Username,code);
        //把验证码存储到redis中(手机号为key,验证码为value),有效时间为5分钟
        redisTemplate.boundValueOps(Username).set(String.valueOf(code),5*60, TimeUnit.SECONDS);
        return new Result().zhuCeSuccess("验证码发送成功");
    }

    /**
     * 用户注册
     * @param UserNick 姓名
     * @param Username 手机号/账号
     * @param Password 密码
     * @return
     */
    @ResponseBody
    @RequestMapping("insert")
    public Result insert(@RequestParam(value="UserNick") String UserNick,
                         @RequestParam(value="company") String companyName,//公司名称,这个作为用户名,只针对供应商
                         @RequestParam(value="Username") String Username,//账号也是手机号
                         @RequestParam(value="Password") String Password,
                         @RequestParam(value="PhoneCode") String PhoneCode){

        //从redis中把手机号和验证码取出来
        Object codeInRedis = redisTemplate.boundValueOps(Username).get();
        if (codeInRedis==null){
            return new Result().failed("验证码失效");
        }
        //再验证输入的验证码是否正确
        if (codeInRedis.equals(PhoneCode)){
            //上家邀请码写死就是海南秒宿供应商的,暂时用不到邀请码了
            String Code_shangjia="b234c7";
            //注册之前判断手机号(账号)是否存在,然后返回信息给前端(根据手机号user_phone查账号user_name)
            String queryUser_name= userService.queryUser_name(Username);
            if (queryUser_name==null) {
                //插入不存在的账号,并设置为三级用户
                Long userId= userService.insertUser1(Username, companyName, Username, Password, RoleEnum.ROLE_USER);
                System.out.println("注册成功的用户id=========="+userId);
                //生成自己的邀请码,并将自己的邀请码存储到code表中
                String code_my = CodeUtils.generateCode4String(6);
                //生成邀请码之后,设置此邀请码为未审核状态,只有填写公司信息才算审核通过,并且更改用户为二级账号
                String code_level="未通过";
                codeService.insertCode(code_my,code_level);
                //根据邀请码查询自己的邀请码id
                Long codeId=codeService.queryCodeId(code_my);
                //将user_id,code_id,code_shangjia,code_my添加到用户邀请码关系表
                userCodeService.insert(userId,codeId,Code_shangjia,code_my);
            }else {
                return new Result().failed("手机号(账号)存在");
            }
        }else {
            return new Result().failed("验证码输入有误");
        }
        return new Result().zhuCeSuccess("注册成功");
    }
}
