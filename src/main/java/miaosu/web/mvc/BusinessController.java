package miaosu.web.mvc;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import miaosu.dao.model.Business;
import miaosu.dao.model.Picture;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.user.BusinessService;
import miaosu.svc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @Date: 2020/7/21 15:44
 * @Description: 公司信息
 */
@Controller
@RequestMapping("/business")
@Slf4j
public class BusinessController extends BaseController {

	@Autowired
	private BusinessService businessService;

	@Autowired
	private MsgNotifyService msgNotifyService;

	//添加公司信息
	@ResponseBody
	@RequestMapping(value = "insert",method = RequestMethod.POST)
	public Result saveImage(@RequestParam(value="business_name") String business_name,
							@RequestParam(value="legal_person") String legal_person,
							@RequestParam(value="legal_phone") String legal_phone,
							@RequestParam(value="business_phone") String business_phone,
							@RequestParam(value="business_email") String business_email,
							@RequestParam(value="business_address") String business_address,
							@RequestParam(value="business_type") Long business_type,
							@RequestParam(value="businessOrFactory") Long businessOrFactory,
							@RequestParam(value="finance_person") String finance_person,
							@RequestParam(value="finance_phone") String finance_phone,
							@RequestParam(value="bank_type") String bank_type,
							@RequestParam(value="bank_number") String bank_number,
							@RequestParam(value="bank_addresss") String bank_addresss,
							@RequestParam(value="account_name") String account_name,
							@RequestParam(value="account_number") String account_number,
							@RequestParam(required = false) MultipartFile[] files) throws Exception {

		Long currentUserId = getCurrentUserId();
		//在公司信息查询是否存在该用户(自己查自己),存在就不能添加,只能展示
		Long queryIfExistUserId = businessService.queryIfExistUserId(currentUserId);
		if (queryIfExistUserId==null){
			if (Strings.isNullOrEmpty(business_name)){
				return new Result().failed("企业名称不能为空");
			}
			else if (Strings.isNullOrEmpty(legal_person)){
				return new Result().failed("法人不能为空");
			}
			else if (Strings.isNullOrEmpty(legal_phone)){
				return new Result().failed("法人联系方式不能为空");
			}
			else if (Strings.isNullOrEmpty(business_phone)){
				return new Result().failed("固定电话不能为空");
			}
			else if (Strings.isNullOrEmpty(business_email)){
				return new Result().failed("电子邮件不能为空");
			}
			else if (Strings.isNullOrEmpty(business_address)){
				return new Result().failed("公司地址不能为空");
			}
			else if (null == business_type){
				return new Result().failed("行业类别不能为空");
			}
			else if (null == businessOrFactory){
				return new Result().failed("经销商/厂家不能为空");
			}
			else if (Strings.isNullOrEmpty(finance_person)){
				return new Result().failed("财务负责人不能为空");
			}
			else if (Strings.isNullOrEmpty(finance_phone)){
				return new Result().failed("财务联系电话不能为空");
			}
			else if (Strings.isNullOrEmpty(bank_type)){
				return new Result().failed("开户银行不能为空");
			}
			else if (Strings.isNullOrEmpty(bank_number)){
				return new Result().failed("行号不能为空");
			}
			else if (Strings.isNullOrEmpty(bank_addresss)){
				return new Result().failed("开户行地址不能为空");
			}
			else if (Strings.isNullOrEmpty(account_name)){
				return new Result().failed("开户名称不能为空");
			}
			else if (Strings.isNullOrEmpty(account_number)){
				return new Result().failed("账号不能为空");
			}

			//添加图片到数据库
			Picture picture = new Picture();
			picture.setUser_id(currentUserId);
			for(int i=0;i<files.length;i++) {
				MultipartFile file=files[i];
				//图片以二进制存储数据库
				byte[] bytes = file.getBytes();
				if(i==0) {
					if (bytes.length==0){
						return new Result().failed("营业执照必须上传");
					}
					picture.setImg_businessLicense(bytes);
				} else if(i==1){
					if (bytes.length==0){
						return new Result().failed("店铺门头必须上传");
					}
					picture.setImg_shopHead(bytes);
				}else  if(i==2){
					if (bytes.length==0){
						return new Result().failed("授权书必须上传");
					}
					picture.setImg_factoryBook(bytes);
				}
			}
			businessService.saveImage(picture);

			//添加公司信息
			Business business = new Business();
			business.setUser_id(currentUserId);
			business.setBusiness_name(business_name);
			business.setLegal_person(legal_person);
			business.setLegal_phone(legal_phone);
			business.setBusiness_phone(business_phone);
			business.setBusiness_email(business_email);
			business.setBusiness_address(business_address);
			business.setBusiness_type(business_type);
			business.setBusinessOrFactory(businessOrFactory);
			business.setFinance_person(finance_person);
			business.setFinance_phone(finance_phone);
			business.setBank_type(bank_type);
			business.setBank_number(bank_number);
			business.setBank_addresss(bank_addresss);
			business.setAccount_name(account_name);
			business.setAccount_number(account_number);
			businessService.insert(business);


			//审核人员电话(暂定为超级管理员的)
			String checkUser_phone = userService.queryUser_phone((long) 1);
			//拼接参数
			Map<String,String> params = new HashMap<String, String>(8);
			//根据用户id查询其账号user_name
			String user_name= userService.queryUser_nameByUser_id(currentUserId);
			params.put("user_name",user_name);
			params.put("business_name",business_name);
			params.put("legal_person",legal_person);
			params.put("legal_phone",legal_phone);
			params.put("business_phone",business_phone);
			params.put("business_email",business_email);
			params.put("business_address",business_address);
			if (business_type==1){
				params.put("business_type","家具");
			}else if (business_type==2){
				params.put("business_type","装修");
			}else if (business_type==3){
				params.put("business_type","设计");
			}else if (business_type==4){
				params.put("business_type","地毯");
			}else if (business_type==5){
				params.put("business_type","灯饰");
			}else if (business_type==6){
				params.put("business_type","五金");
			}else if (business_type==7){
				params.put("business_type","智能化");
			}else if (business_type==8){
				params.put("business_type","其他");
			}
			if (businessOrFactory==1){
				params.put("businessOrFactory","经销商");
			}else if (businessOrFactory==2){
				params.put("businessOrFactory","厂家");
			}
			params.put("finance_person",finance_person);
			params.put("finance_phone",finance_phone);
			params.put("bank_type",bank_type);
			params.put("bank_number",bank_number);
			params.put("bank_addresss",bank_addresss);
			params.put("account_name",account_name);
			params.put("account_number",account_number);
			//只要有人填写公司信息就打电话给审核人员进行审核
			msgNotifyService.checkBusinessInsert(checkUser_phone,params);
			//针对添加新公司信息发送短信
			msgNotifyService.sendMsgBusinessInsert(checkUser_phone,params);
		}else{
			//已经存在公司信息就直接跳转到展示页面
			return new Result().failed("您已经填写过公司信息");
		}
		return new Result().zhuCeSuccess("填写成功,审核中...");
	}




	//查询公司信息列表
	@ResponseBody
	@RequestMapping("list")
	public Result businessList(HttpServletResponse response) throws Exception {
		long startTime=System.currentTimeMillis();//获取开始时间
		Map map=new HashMap();
		Long currentUserId = getCurrentUserId();
		Long queryIfExistUserId = businessService.queryIfExistUserId(currentUserId);
		if (queryIfExistUserId==null){
			return new Result().failed("请您填写企业信息");
		}

		//查询公司信息,返回公司对象
		Business business=businessService.queryBusinessList(currentUserId);
		map.put("business_name",business.getBusiness_name());
		map.put("legal_person",business.getLegal_person());
		map.put("legal_phone",business.getLegal_phone());
		map.put("business_phone",business.getBusiness_phone());
		map.put("business_email",business.getBusiness_email());
		map.put("business_address",business.getBusiness_address());

		Long business_type = business.getBusiness_type();
		if (null==business_type){
			business.setBusiness_typeStr(null);
		}else {
		if (business_type==1){
			map.put("business_type","家具");
		}else if (business_type==2){
			map.put("business_type","装修");
		}else if (business_type==3){
			map.put("business_type","设计");
		}else if (business_type==4){
			map.put("business_type","地毯");
		}else if (business_type==5){
			map.put("business_type","灯饰");
		}else if (business_type==6){
			map.put("business_type","五金");
		}else if (business_type==7){
			map.put("business_type","智能化");
		}else if (business_type==8){
			map.put("business_type","其他");
		}
		}
		Long businessOrFactory = business.getBusinessOrFactory();
		if (null==businessOrFactory){
			business.setBusinessOrFactoryStr(null);
		}else {
		if (businessOrFactory==1){
			map.put("businessOrFactory","经销商");
		}else if (businessOrFactory==2){
			map.put("businessOrFactory","厂家");
		}
		}
		map.put("finance_person",business.getFinance_person());
		map.put("finance_phone",business.getFinance_phone());
		map.put("bank_type",business.getBank_type());
		map.put("bank_number",business.getBank_number());
		map.put("bank_addresss",business.getBank_addresss());
		map.put("account_name",business.getAccount_name());
		map.put("account_number",business.getAccount_number());

//		//查询图片
//	    Picture picture=businessService.queryPicture(currentUserId);
//	    //对图片进行判空,否则报错
//		if (null==picture){
			business.setImg_businessLicense(null);
			business.setImg_shopHead(null);
			business.setImg_factoryBook(null);
//		}else {
//		byte[] img_businessLicense = picture.getImg_businessLicense();
//		String encode1 = Base64.encodeBase64String(img_businessLicense);
//		String base64_1="data:image/png;base64,"+encode1;
//		map.put("img_businessLicense", base64_1);
//
//		byte[] img_shopHead = picture.getImg_shopHead();
//		String encode2 = Base64.encodeBase64String(img_shopHead);
//		String base64_2="data:image/png;base64,"+encode2;
//		map.put("img_shopHead", base64_2);
//
//		byte[] img_factoryBook = picture.getImg_factoryBook();
//		String encode3 = Base64.encodeBase64String(img_factoryBook);
//		String base64_3="data:image/png;base64,"+encode3;
//		map.put("img_factoryBook", base64_3);
//		long endTime=System.currentTimeMillis(); //获取结束时间
//		System.out.println("展示照片程序运行时间： "+(endTime-startTime)+"ms");
//		}


		//根据当前用户id查询对应的角色,二级可以看信息;;三级提示审核中,审核通过才能看,并且改为二级用户
		List<Long> queryUser_Role= userService.queryUser_Role(currentUserId);
		for (Long aLong : queryUser_Role) {
			if (aLong==3){
				return new Result().business(map,"公司信息审核中...");
			}else {
				return new Result().success(map);
			}
		}
		return null;
	}
}