package miaosu.web.mvc;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.miaosu.hotel.gds.ctrip.util.DateUtil;
import miaosu.annonation.LogInfoAnnotation;
import miaosu.common.OpTypeEnum;
import miaosu.dao.model.HotelInfo;
import miaosu.svc.hotel.*;
import miaosu.svc.order.MsgNotifyService;
import miaosu.svc.user.UserService;
import miaosu.svc.vo.HotelRoomStatusVO;
import miaosu.svc.vo.Result;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiajun.chen on 2018/1/9.
 */
@Controller
public class RoomStatusController extends BaseController{
    private static final Logger logger = LoggerFactory.getLogger(RoomStatusController.class);

    @Autowired
    private HotelRoomStatusService hotelRoomStatusService;
    @Autowired
    private HotelPriceSetService hotelPriceSetService;
    @Autowired
    private HotelRoomSetService hotelRoomSetService;
    @Autowired
    private HotelRoomCountsService  hotelRoomCountsService;

    private final int days_count = 10;

    @Autowired
    private MsgNotifyService msgNotifyService;

    @Autowired
    private UserService userService;
    @Autowired
    private HotelService hotelService;



    /**
     * 返回房态信息 （需要进行修改 添加价格计划）
     * @param model
     * @param date_start
     * @return
     */
    @GetMapping("/room/status")
    public String order(Model model,
                        @RequestParam(value="date_start", required=false) String date_start) {

        List<String> dates = Lists.newArrayList();
        Date start = new Date();
        dates.add(DateFormatUtils.format(start,"yyyy-MM-dd"));
        for (int i=1;i<days_count;i++){
            Date d1 = DateUtils.addDays(start, i);
            dates.add(DateFormatUtils.format(d1,"yyyy-MM-dd"));
        }
        Date end = DateUtils.addDays(start, days_count-1);

//        Long currentUserId = getCurrentUserId();
        model.addAttribute("dates",dates);
        model.addAttribute("start",DateFormatUtils.format(start,"MM-dd"));
        model.addAttribute("end",DateFormatUtils.format(end,"MM-dd"));

        Long hotelId = this.getCurHotel();
        List<HotelRoomStatusVO> hotelRoomStatusVOList = Lists.newArrayList();
        if(null != hotelId){
            hotelRoomStatusVOList = hotelRoomStatusService.queryHotelStatusInfo(hotelId,start,end);
        }
        logger.info("roomStatusList:"+JSON.toJSONString(hotelRoomStatusVOList));
        model.addAttribute("roomStatusList",hotelRoomStatusVOList);

        model.addAttribute("yesterday",DateFormatUtils.format(DateUtils.addDays(start, -1),"yyyy-MM-dd"));

        //获取当前用户id,查询他的角色,是酒店就不能操作关房,返回0,不是酒店就可以操作关房,返回1
        Long currentUserId = getCurrentUserId();
        List<Long> queryUser_Role_Ids = userService.queryUser_Role(currentUserId);
        for (Long role_id : queryUser_Role_Ids) {
            if (role_id==3){
                model.addAttribute("IfCanCloseroom","0");
            }else {
                model.addAttribute("IfCanCloseroom","1");
            }
        }

        return "room_status";
    }

    @ResponseBody
    @RequestMapping("/room/status/list")
    public Result list(Model model,
                       @RequestParam(value="date_start", required=true) String date_start,
                       @RequestParam(value="date_end", required=true) String date_end) {
        try {
            Date startDate = DateUtils.parseDate(date_start,"yyyy-MM-dd");
            Date endDate = DateUtils.parseDate(date_end,"yyyy-MM-dd");
            Long hotelId = getCurHotel();

            if(hotelId==null){
                hotelId = 13L;
            }

            List<HotelRoomStatusVO> hotelRoomStatusVOList = Lists.newArrayList();
            if(null != hotelId){
                hotelRoomStatusVOList = hotelRoomStatusService.queryHotelStatusInfo(hotelId,startDate,endDate);
            }

            ObjectMapper mapper = new ObjectMapper();
            logger.info("房态信息："+ mapper.writeValueAsString(hotelRoomStatusVOList));

            return new Result().success().setData(hotelRoomStatusVOList);

        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new Result().failed("参数格式错误");
        }
    }

    @ResponseBody
    @RequestMapping("/room/status/dates/next")
    public Result datas_next(Model model,
                       @RequestParam(value="date_start", required=true) String date_start) {
        try {
            List<String> dates = Lists.newArrayList();
            Date start = DateUtils.parseDate(date_start,"yyyy-MM-dd");
            for (int i=1;i<=days_count;i++){
                Date d1 = DateUtils.addDays(start, i);
                dates.add(DateFormatUtils.format(d1,"yyyy-MM-dd"));
            }
            return new Result().success().setData(dates);
        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        }
    }

    @ResponseBody
    @RequestMapping("/room/status/dates/pre")
    public Result datas_pre(Model model,
                             @RequestParam(value="date_end", required=true) String date_end) {
        try {
            List<String> dates = Lists.newArrayList();
            Date end = DateUtils.parseDate(date_end,"yyyy-MM-dd");
            for (int i=-days_count; i<0; i++){
                Date d1 = DateUtils.addDays(end, i);
                dates.add(DateFormatUtils.format(d1,"yyyy-MM-dd"));
            }
//            dates.add(DateFormatUtils.format(end,"yyyy-MM-dd"));
            return new Result().success().setData(dates);

        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        }
    }

    /**
     * 更新房态信息
     * @param model
     * @param productIds 产品id 数组类型
     * @param setStartTime 开始时间
     * @param setEndTime 结束时间
     * @param roomStatus 房间状态
     * @return
     */
    @ResponseBody
    @RequestMapping("/room/status/update")
    //自定义日志注解
    @LogInfoAnnotation(opType = OpTypeEnum.ON_OFF_ROOM,startDateFieldName = "setStartTime",
            endDateFieldName = "setEndTime",opValueFieldName = "roomStatus",productFiledName = "productIds" )
    public Result update(Model model,
                       @RequestParam(value="productIds", required=true) Long[] productIds,
                       @RequestParam(value="setStartTime", required=true) String setStartTime,
                       @RequestParam(value="setEndTime", required=true) String setEndTime,
                       @RequestParam(value="roomStatus", required=true) int roomStatus) {
        try {
            Date startDate = DateUtils.parseDate(setStartTime,"yyyy-MM-dd");
            Date endDate = DateUtils.parseDate(setEndTime,"yyyy-MM-dd");

            Long hotelId = getCurHotel();


            //查询酒店信息,包括用户id,酒店名称
            HotelInfo hotelInfo= hotelService.getHotelInfoById(hotelId);
            Long userId = hotelInfo.getUserId();
            String hotelName = hotelInfo.getHotelName();
            String ota_phone = hotelInfo.getOta_phone();
            //查询用户电话,查user表
            String user_phone=userService.queryUser_phone(userId);
            //拼接参数
            Map<String,String> params = new HashMap<String, String>(8);
            //根据酒店id查询平台id
            String hotel_only_id= hotelService.queryHotelOnlyId(hotelId);
            params.put("hotel_only_id", hotel_only_id);
            params.put("hotelName",hotelName);
            params.put("setStartTime",setStartTime);
            params.put("setEndTime",setEndTime);
            params.put("roomStatus", String.valueOf(roomStatus));
            StringBuilder builder = new StringBuilder();
            for (Date d = startDate;d.getTime()<=endDate.getTime();d=DateUtils.addDays(d, 1)){
                for (Long productId:productIds){
                    hotelRoomStatusService.update(hotelId,productId,d,roomStatus);
                    //根据产品productId,查询房间类型hotelRoomTypeId,查hotel_price_ref_room_type表
                    Long hotelRoomTypeId= hotelPriceSetService.queryHotelRoomTypeIdByProductId(productId);
                    //根据房间类型hotelRoomTypeId,查房间类型名称hotel_room_type_name,查hotel_room_set表
                    String roomTypeName=hotelRoomSetService.queryHotelRoomTypeName(hotelRoomTypeId);
                    if (!builder.toString().contains(roomTypeName)){
                        builder.append(roomTypeName).append(",");
                    }
                }
            }
            //切割掉最后一个逗号
            builder.deleteCharAt(builder.length()-1);
            params.put("roomTypeName",builder.toString());

            String url1 = "http://192.168.3.4:8080/dlt_close_room";//代理通
            String url2 = "http://192.168.3.4:8080/tc_close_room";//同程
//            String url1 = "http://61.52.98.214/dlt_close_room";//代理通
//            String url2 = "http://61.52.98.214/tc_close_room";//同程
            //关房时电话通知OTA管家电话(三级操作关房的时候,二级需要知道)此时的房态就是更新后的操作,为0就打电话
            if (roomStatus==0){
                //打电话给OTA管家
                msgNotifyService.sendUserSms(ota_phone,params);
                //短信通知OTA管家
                msgNotifyService.sendUserSms2(ota_phone,params);
                //打电话给张磊
                //msgNotifyService.sendUserSms3("13783717856",params);
                //发短信给张磊
                //msgNotifyService.sendUserSms4("13783717856",params);
                //将关房数据传递给爬虫人员,让他主动操作关房
                sendRoomStatus(params,url1);
                sendRoomStatus(params,url2);
            }
            //开房发送给数采系统
            if (roomStatus==1){
                //打电话给OTA管家
                msgNotifyService.sendUserSms11(ota_phone,params);
                //短信通知OTA管家
                msgNotifyService.sendUserSms22(ota_phone,params);
                //打电话给张磊
                //msgNotifyService.sendUserSms33("13783717856",params);
                //发短信给张磊
                //msgNotifyService.sendUserSms44("13783717856",params);
                sendRoomStatus(params,url1);
                sendRoomStatus(params,url2);
            }


            return new Result().success();

        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        }
    }


    //将关房数据传递给爬虫人员,让他主动操作关房
    public void sendRoomStatus(Map<String,String> params,String url){
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            String hotel_only_id = params.get("hotel_only_id");//酒店所有平台id:
            String hotelName = params.get("hotelName");//酒店名字:海旺弘亚温泉酒店
            String setStartTime = params.get("setStartTime");//开始年月日:2020-7-29
            String setEndTime = params.get("setEndTime");//结束年月日:2020-8-1
            String roomTypeName = params.get("roomTypeName");//房间类型:豪华标准间
            String roomStatus = params.get("roomStatus");//开关房状态

            MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
            map.add("hotel_only_id", hotel_only_id);
            map.add("hotelName", hotelName);
            map.add("setStartTime", setStartTime);
            map.add("setEndTime", setEndTime);
            map.add("roomTypeName", roomTypeName);
            map.add("roomStatus",roomStatus);

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.postForEntity( url, request , String.class );
            System.out.println(response.getBody());
        } catch (RestClientException e) {
            //失败不作任何处理,不影响自己的业务
            return;
        }
    }







    /**
     * 更新房量信息（多个或者单个房型的房量信息的查询）
     * @param model
     * @param hotelRoomTypeIds 房型类型 数组
     * @param setStartTime 开始时间
     * @param setEndTime  结束时间
     * @param roomCounts  房量 数组 和房型类型数组相同
     * @return Result 结果对象
     */
    @ResponseBody
    @RequestMapping("/room/counts/update")
    @LogInfoAnnotation(opType = OpTypeEnum.ROOM_COUNT,startDateFieldName = "setStartTime",
            endDateFieldName = "setEndTime",opValueFieldName = "roomCounts",roomTypeFiledName = "hotelRoomTypeIds" )
    public Result updateCounts(Model model,
                         @RequestParam(value="hotelRoomTypeIds", required=true) Long[] hotelRoomTypeIds,
                         @RequestParam(value="setStartTime", required=true) String setStartTime,
                         @RequestParam(value="setEndTime", required=true) String setEndTime,
                         @RequestParam(value="roomCounts", required=true) String[] roomCounts) {
        try {
            Date startDate = DateUtils.parseDate(setStartTime,"yyyy-MM-dd");
            Date endDate = DateUtils.parseDate(setEndTime,"yyyy-MM-dd");

            //先判断日期是否在合理的范围
           boolean flag =  DateUtil.compareDate(startDate,endDate);
           if(!flag){
               return new Result().failed("开始日期大于结束日期");
           }

            Long hotelId = getCurHotel();
            for (Date d = startDate;d.getTime()<=endDate.getTime();d=DateUtils.addDays(d, 1)){
               for(int i=0;i<hotelRoomTypeIds.length;i++) {
                   if(!Strings.isNullOrEmpty(roomCounts[i])){
                       hotelRoomCountsService.update(hotelRoomTypeIds[i],d,Integer.parseInt(roomCounts[i]));
                   }
               }
            }
            for(int i=0;i<hotelRoomTypeIds.length;i++) {
                //发送消息
                //TODO 业务上确保一个房型只运行有一个计划价格，否则是一个大的bug
               Long productId  = hotelRoomCountsService.findProductId(hotelRoomTypeIds[i]);
            }
            return new Result().success();

        }catch (ParseException e){
            logger.error("参数格式错误",e);
            return new Result().failed("参数格式错误");
        }
    }
}
