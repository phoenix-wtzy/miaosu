package miaosu.web.mvc;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import miaosu.dao.model.HotelEvaluation;
import miaosu.dao.model.HotelEvaluationEasy;
import miaosu.svc.hotel.HotelEvaluationService;
import miaosu.svc.hotel.OtaDataService;
import miaosu.svc.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 酒店评估模型
 */
@Controller
@RequestMapping("/evaluation")
@Slf4j
public class HotelEvaluationController extends BaseController {


	@Autowired
	private HotelEvaluationService hotelEvaluationService;

	@Autowired
	private OtaDataService otaDataService;


	//查询当前用户名下酒店简单信息
	@ResponseBody
	@RequestMapping("easyList")
	public Response HotelEvaluationEasyList(@RequestParam(value="page", required=false,defaultValue = "1") int page,
											@RequestParam(value="limit", required=false,defaultValue = "10") int limit){
		Long currentUserId = getCurrentUserId();
		List<HotelEvaluationEasy>hotelEvaluationEasyList= Lists.newArrayList();
		List<HotelEvaluation>hotelEvaluationList=hotelEvaluationService.queryAllByUserId(currentUserId,page,limit);
		for (HotelEvaluation hotelEvaluation : hotelEvaluationList) {
			HotelEvaluationEasy hotelEvaluationEasy = new HotelEvaluationEasy();
			hotelEvaluationEasy.setId(hotelEvaluation.getId());
			hotelEvaluationEasy.setUser_id(hotelEvaluation.getUser_id());
			hotelEvaluationEasy.setCreate_time(hotelEvaluation.getCreate_time());
			hotelEvaluationEasy.setEvaluation_time(hotelEvaluation.getEvaluation_time());
			hotelEvaluationEasy.setHotel_name(hotelEvaluation.getHotel_name());
			hotelEvaluationEasy.setEvaluation_state(hotelEvaluation.getEvaluation_state());
			hotelEvaluationEasyList.add(hotelEvaluationEasy);
		}
		//查询当前用户名下酒店信息总条数
		int count=hotelEvaluationService.queryTotalByUserId(currentUserId);
		return new Response().success("查询酒店简单信息成功",count,hotelEvaluationEasyList);
	}

	//查询当前用户名下填写的所有酒店评估信息,到hotel_evaluation表中
	@ResponseBody
	@RequestMapping("list")
	public Response HotelEvaluationList(@RequestParam(value="page", required=false,defaultValue = "1") int page,
										@RequestParam(value="limit", required=false,defaultValue = "10") int limit){
		Long currentUserId = getCurrentUserId();
		List<HotelEvaluation>hotelEvaluationList=hotelEvaluationService.queryAllByUserId(currentUserId,page,limit);
		//查询当前用户名下酒店信息总条数
		int count=hotelEvaluationService.queryTotalByUserId(currentUserId);
		return new Response().success("查询酒店信息成功",count,hotelEvaluationList);
	}

	//新增一家酒店评估信息--不需要验证码
	@ResponseBody
	@RequestMapping("insert")
	public Response insert(@RequestParam(value="hotelName") String hotel_name,//酒店名字
						   @RequestParam(value="licenses") String licenses,//营业执照
						   @RequestParam(value="publicWork") int publicWork,//市政工程/临时封路
						   @RequestParam(value="agent") int agent,//代理
						   @RequestParam(value="MTplatformNumber") String MTplatformNumber,//美团账号
						   @RequestParam(value="MTplatformPassword") String MTplatformPassword,//美团密码
						   @RequestParam(value="XCplatformNumber") String XCplatformNumber,//携程账号
						   @RequestParam(value="XCplatformPassword") String XCplatformPassword){//携程密码

		Long currentUserId = getCurrentUserId();

		/**
		 * 获取到全部填写数据,根据账号密码去询问爬虫需不需要验证码
		 * 1.不需要验证码:
		 *    1.1 将不需要验证码的信息返给前端
		 *    1.2 同时直接将数据存储到数据库
		 * 2.需要验证码:
		 *    2.1  将需要验证码的信息返给前端
		 *    2.2  不进行存储,完事了
		 *    2.3  在另一个接口中,获取到全部数据(包括验证码,验证码发给爬虫),其他数据进行存储
		 */

		//询问爬虫是否需要验证码
		String username=null;
		String password=null;
		String plat_name=null;
		String plat_id=null;
		if (!Strings.isNullOrEmpty(MTplatformNumber)&&!Strings.isNullOrEmpty(MTplatformPassword)){
			username=MTplatformNumber;
			password=MTplatformPassword;
			plat_name="美团";
			plat_id="3";
			return new Response(1,"需要验证码",1,null);
		}
		if (!Strings.isNullOrEmpty(XCplatformNumber)&&!Strings.isNullOrEmpty(XCplatformPassword)){
			username=XCplatformNumber;
			password=XCplatformPassword;
			plat_name="携程";
			plat_id="1";
			return new Response().success("添加成功");
		}
//		String url = "";
//		RestTemplate restTemplate = new RestTemplate();
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
//		map.add("username", username);
//		map.add("password", password);
//		map.add("plat_name",plat_name);
//		map.add("plat_id", plat_id);
//		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//		ResponseEntity<String> response = restTemplate.postForEntity(url,request ,String.class);
//		HttpStatus statusCode = response.getStatusCode();
//		if (200!=statusCode.value()){
//			return new Response().failed("爬虫那里连接不上");
//		}
//		String body = response.getBody();
//		JSONObject jsonObject = JSON.parseObject(body);
//		Long status = jsonObject.getLong("status");
//		Object storage_info = jsonObject.get("storage_info");
//		if (0!=status){
//			return new Response(1,"需要验证码",1,null);
//		}
//
//		HotelEvaluation hotelEvaluation = new HotelEvaluation();
//		hotelEvaluation.setUser_id(currentUserId);
//		hotelEvaluation.setHotel_name(hotel_name);
//		hotelEvaluation.setCreate_time(new Date());
//		hotelEvaluation.setEvaluation_state(0);
//		JSONArray objects = JSON.parseArray(licenses);
//		Object o1 = objects.get(0);
//		if (o1.equals("99")){
//			hotelEvaluation.setBusinessLicense(0);
//		}else {
//			hotelEvaluation.setBusinessLicense(1);
//		}
//		Object o2 = objects.get(1);
//		if (o2.equals("99")){
//			hotelEvaluation.setFireLicense(0);
//		}else {
//			hotelEvaluation.setFireLicense(1);
//		}
//		Object o3 = objects.get(2);
//		if (o3.equals("99")){
//			hotelEvaluation.setSpecialLicense(0);
//		}else {
//			hotelEvaluation.setSpecialLicense(1);
//		}
//		hotelEvaluation.setPublicWork(publicWork);
//		hotelEvaluation.setAgent(agent);
//		//新增酒店评估到hotel_evaluation表中
//		Long hotel_id=hotelEvaluationService.insert(hotelEvaluation);
//		OtaData otaData = new OtaData();
//		otaData.setHotel_id(hotel_id);
//		otaData.setPlatformCode(Integer.valueOf(plat_id));
//		otaData.setPlatformName(plat_name);
//		otaData.setPlatformNumber(username);
//		otaData.setPlatformPassword(password);
//		//添加酒店信息到ota_data数据表中
//		otaDataService.insert(otaData);
//
//		map.add("hotel_id_md", String.valueOf(hotel_id));
//		String url2 = "http://192.168.3.48:8222/hotel/user";
//		HttpEntity<MultiValueMap<String, String>> request2 = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//		ResponseEntity<String> response2 = restTemplate.postForEntity(url2,request2 ,String.class);
//		HttpStatus statusCode2 = response2.getStatusCode();
//		if (200!=statusCode2.value()){
//			return new Response().failed("爬虫那里连接不上");
//		}
//		String body2 = response2.getBody();
//		JSONObject jsonObject2 = JSON.parseObject(body2);
//		Long status2 = jsonObject2.getLong("status");
//		Object storage_info2 = jsonObject2.get("storage_info");
//		if (0!=status2){
//			return new Response().failed(String.valueOf(storage_info2));
//		}
//		return new Response().success("添加成功");
		return null;
	}
	//新增一家酒店评估信息--需要验证码
	@ResponseBody
	@RequestMapping("insert2")
	public Response insert2(@RequestParam(value="hotelName") String hotel_name,//酒店名字
						    @RequestParam(value="licenses") String licenses,//营业执照
						    @RequestParam(value="publicWork") int publicWork,//市政工程/临时封路
						    @RequestParam(value="agent") int agent,//代理
						    @RequestParam(value="MTplatformNumber") String MTplatformNumber,//美团账号
						    @RequestParam(value="MTplatformPassword") String MTplatformPassword,//美团密码
						    @RequestParam(value="XCplatformNumber") String XCplatformNumber,//携程账号
						    @RequestParam(value="XCplatformPassword") String XCplatformPassword,//携程密码
							@RequestParam(value="code") String code) {//验证码

		Long currentUserId = getCurrentUserId();
		String username=null;
		String password=null;
		String plat_name=null;
		String plat_id=null;
		if (!Strings.isNullOrEmpty(MTplatformNumber)&&!Strings.isNullOrEmpty(MTplatformPassword)){
			username=MTplatformNumber;
			password=MTplatformPassword;
			plat_name="美团";
			plat_id="3";
		}
		if (!Strings.isNullOrEmpty(XCplatformNumber)&&!Strings.isNullOrEmpty(XCplatformPassword)){
			username=XCplatformNumber;
			password=XCplatformPassword;
			plat_name="携程";
			plat_id="1";
		}
//		HotelEvaluation hotelEvaluation = new HotelEvaluation();
//		hotelEvaluation.setUser_id(currentUserId);
//		hotelEvaluation.setHotel_name(hotel_name);
//		hotelEvaluation.setCreate_time(new Date());
//		hotelEvaluation.setEvaluation_state(0);
//		JSONArray objects = JSON.parseArray(licenses);
//		Object o1 = objects.get(0);
//		if (o1.equals("99")){
//			hotelEvaluation.setBusinessLicense(0);
//		}else {
//			hotelEvaluation.setBusinessLicense(1);
//		}
//		Object o2 = objects.get(1);
//		if (o2.equals("99")){
//			hotelEvaluation.setFireLicense(0);
//		}else {
//			hotelEvaluation.setFireLicense(1);
//		}
//		Object o3 = objects.get(2);
//		if (o3.equals("99")){
//			hotelEvaluation.setSpecialLicense(0);
//		}else {
//			hotelEvaluation.setSpecialLicense(1);
//		}
//		hotelEvaluation.setPublicWork(publicWork);
//		hotelEvaluation.setAgent(agent);
//		//新增酒店评估到hotel_evaluation表中
//		Long hotel_id=hotelEvaluationService.insert(hotelEvaluation);
//		OtaData otaData = new OtaData();
//		otaData.setHotel_id(hotel_id);
//		otaData.setPlatformCode(Integer.valueOf(plat_id));
//		otaData.setPlatformName(plat_name);
//		otaData.setPlatformNumber(username);
//		otaData.setPlatformPassword(password);
//		//添加酒店信息到ota_data数据表中
//		otaDataService.insert(otaData);


//		RestTemplate restTemplate = new RestTemplate();
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
//		map.add("username", username);
//		map.add("password", password);
//		map.add("plat_name",plat_name);
//		map.add("plat_id", plat_id);
//		map.add("code", code);
//		map.add("hotel_id_md", String.valueOf(hotel_id));
//		String url2 = "http://192.168.3.48:8222/hotel/user";
//		HttpEntity<MultiValueMap<String, String>> request2 = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//		ResponseEntity<String> response2 = restTemplate.postForEntity(url2,request2 ,String.class);
//		HttpStatus statusCode2 = response2.getStatusCode();
//		if (200!=statusCode2.value()){
//			return new Response().failed("爬虫那里连接不上");
//		}
//		String body2 = response2.getBody();
//		JSONObject jsonObject2 = JSON.parseObject(body2);
//		Long status2 = jsonObject2.getLong("status");
//		Object storage_info2 = jsonObject2.get("storage_info");
//		if (0!=status2){
//			return new Response().failed(String.valueOf(storage_info2));
//		}
		return new Response().success("添加成功");
	}


}
