package miaosu.web.mvc;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import miaosu.dao.model.City;
import miaosu.dao.model.District;
import miaosu.dao.model.Province;
import miaosu.svc.address.CityService;
import miaosu.svc.address.DistrictService;
import miaosu.svc.address.ProvinceService;
import miaosu.svc.hotel.HotelService;
import miaosu.svc.vo.PageResult;
import miaosu.svc.vo.Result;
import miaosu.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author  xieqx
 * @date  2018-08-13
 * 省市区相关信息
 */
@Controller
@RequestMapping("/address")
@Slf4j
public class AddressController extends BaseController {
    @Autowired
    private ProvinceService provinceService;

    @Autowired
    private CityService cityService;

    @Autowired
    private DistrictService districtService;


    /**
     *查询所有的省份信息
     * @return
     */
    @ResponseBody
    @RequestMapping("/provinces")
    public Result provinces(){
        List<Province> provinces = provinceService.findProvinceList(null);

        return  new Result().success().setData(provinces);
    }

    /**
     *根据 省份id  channel 查询城市信息
     * @param provinceCode
     * @return
     */
    @ResponseBody
    @RequestMapping("/cities")
    public Result cityList(@RequestParam(value="provinceCode", required=false) String provinceCode){

        if(provinceCode==null){
            return  new Result().failed("省份编码为空");
        }

        List<City>  cities = cityService.findCityList(provinceCode);

        log.info("cities {}", JsonUtils.objectToJson(cities));
        return  new Result().success().setData(cities);
    }


    /**
     * 根据 城市id  channel 查询区域信息
     * @param cityCode
     * @return
     */
    @ResponseBody
    @RequestMapping("/districts")
    public Result districtList(@RequestParam(value="cityCode", required=false) String cityCode){

        if(Strings.isNullOrEmpty(cityCode)){
            return  new Result().failed("城市id为空");
        }

        List<District>  districts = districtService.findDistrictList(cityCode);

        log.info("districts {}", JsonUtils.objectToJson(districts));
        return  new Result().success().setData(districts);
    }


    /**
     *根据 城市id  同属于同一省份的省份信息
     * @param cityCode
     * @return
     */
    @ResponseBody
    @RequestMapping("/citiesSibling")
    public Result citiesSibling(@RequestParam(value="cityCode", required=false) String cityCode){

        if(cityCode==null){
            return  new Result().failed("城市编码为空");
        }

        List<City>  cities = cityService.findCitySibling(cityCode);

        return  new Result().success().setData(cities);
    }


    /**
     * 根据 区域id  查询同属同一个城市的区域信息
     * @param districtCode
     * @return
     */
    @ResponseBody
    @RequestMapping("/districtsSibling")
    public Result districtsSibling(@RequestParam(value="districtCode", required=false) String districtCode){

        if(Strings.isNullOrEmpty(districtCode)){
            return  new Result().failed("区域为空");
        }
        List<District>  districts = districtService.findDistrictSibling(districtCode);

        return  new Result().success().setData(districts);
    }

}
