function show_price_set_type_list(table,url) {
    $(table).bootstrapTable({
        url: url,
        class:"table table-hover row",
        columns: [{
            field: 'hotelPriceName',
            title: '价格类型名称',
            class:"w-10",
        }, {
            field: 'breakfastNum',
            title: '早餐',
            class:"w-10",
        }, {
            field: 'unbookType',
            title: '退订规则',
            class:"w-10",
        },{
            field: 'hotelRoomTypeName',
            title: '关联房型',
            formatter:refRoomType,
            class:"col-6"
        },{
            field: 'caigou_one',
            title: '置换结算价',
            class: "col-2"
        },{
            field: 'hotelPriceId',
            title: '修改',
            formatter:room_price_update,
            class:"col-1",
        },{
            field: 'hotelPriceId',
            title: '删除',
            class:"col-1",
            formatter:room_price_delete,
        }
        ]});
}

/**
 *需要创建模态窗口
 */
function room_price_update(value, row, index) {
    // console.log(row)
    t1 = '<a href="#" onclick="show_update_room_price('+row.hotelPriceId+',\''+row.hotelPriceName+'\','+row.unbookType+','+row.breakfastNum+','+row.caigou_one+')">'+'<span class="oi oi-pencil"></span>'+'</a>';
    return '<div class="col-sm-2">'+t1+'</div>';
}
function room_price_delete(value, row, index) {
    t2 = '<a href="#" onclick="delete_room_price('+row.hotelPriceId+',\''+row.hotelPriceName+'\','+row.unbookType+')">'+'<span class="oi oi-x"></span>'+'</a>';
    return t2;
}
function delete_room_price(hotelPriceId,hotelPriceName) {
    if (confirm("确定要删除价格类型："+hotelPriceName+" ?")){
        $.ajax({
            type: 'get',
            url: '/room/set/price/delete?hotelPriceId='+hotelPriceId,
            success: function (data) {
                reload_price_type();
            }}
        )
    }
}

function refRoomType(value, row, index) {
//        console.log(row)
    var h = '';
    $.each(row.hotelRoomSetList,function () {
        h += '<div class="badge badge-info">'+this.hotelRoomTypeName+'</div>';
    })
    return h;
}

function show_update_room_price(hotelPriceId,hotelPriceName,unbookType,breakfastNum,caigou_one) {
    console.log(caigou_one)
    console.log($("#caigou_one"))
    $('#priceTypeName').val(hotelPriceName);
    $('#hotelPriceId').val(hotelPriceId);
    $("#update_hotel_error_info").text('')
    $("#caigouOne").val(caigou_one)

    var dropdownRoomType = $('#dropdownRoomType');
    dropdownRoomType.empty(); //clear

    //获取关联房型
    $.ajax({
            type: 'get',
            url: '/room/set/price/refroomtype',
            data: jQuery.param({ hotelPriceId: hotelPriceId, hotelId : g_hotelId}) ,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (data) {
                var refroomtypeChecked =[]
                var refroomtypeCheckedText = ''
                if (data.bstatus.code == 0){
                    $.each(data.data,function () {
                        refroomtypeChecked.push(this.hotelRoomTypeId);
                        if (refroomtypeCheckedText ==""){
                            refroomtypeCheckedText += this.hotelRoomTypeName;
                        }else {
                            refroomtypeCheckedText += " , "+this.hotelRoomTypeName;
                        }
                    });
                }
                var h1 = '<input type="text" style="width: 172%" class="form-control" id="dropdownMenuButtonRoomType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" value="'+refroomtypeCheckedText+'"/>';
                $(h1).appendTo(dropdownRoomType);

                //获取所有房型
                $.ajax({
                        type: 'get',
                        url: '/room/set/type/list',
                        data: jQuery.param({hotelId : g_hotelId}) ,
                        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                        success: function (data) {
                            console.log(data)
                            if (data.bstatus.code == 0){
                                var e_h2= $('<ul class="dropdown-menu" style="margin-top: -1px;padding:0;" ></ul>');
                                $.each(data.data,function () {
                                    var is_checked = $.inArray(this.hotelRoomTypeId,refroomtypeChecked)!=-1;
                                    var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
                                    var e_h4 = $("<input/>",{type:"radio",
                                        name:"hotelRoomTypeId",
                                        value:this.hotelRoomTypeId,
                                        checked:is_checked});
                                    e_h4.on('click',function () {
                                        var refroomtypeCheckedText = ''
                                        $.each($("input[name='hotelRoomTypeId']"),function () {
                                            if(this.checked){
                                                refroomtypeCheckedText += this.nextElementSibling.innerText + " , ";
                                            }
                                        });
                                        $("#dropdownMenuButtonRoomType").val(refroomtypeCheckedText);
                                    });
                                    e_h4.appendTo(e_h3);
                                    $("<span></span>",{style:"margin-left: 5px;margin-top: -1px;",text:this.hotelRoomTypeName}).appendTo(e_h3);
                                    e_h3.appendTo(e_h2);
                                });
                                e_h2.appendTo(dropdownRoomType)
                            }
                        }}
                );
                //订餐人数
                var e_bfc = $("#breakfastCount");

                e_bfc.empty();
                $.each([0,2],function () {
                    var e_h1 = $('<option value ="1">'+this+'</option>');
                    e_h1.val(this);
                    e_h1.prop('selected', breakfastNum==parseInt(this));
//                    console.log(parseInt(this)==breakfastNum);
                    e_bfc.append(e_h1)
                });

                //退订规则
                if(unbookType == 1){
                    $("#ugridRadios1").prop('checked', true);
                    $("#ugridRadios2").prop('checked', false);
                }else if(unbookType == 2){
                    $("#ugridRadios1").prop('checked', false);
                    $("#ugridRadios2").prop('checked', true);
                }
            }}
    );
    $('#updatePriceTypeModal').modal('show');
}

function roomTypeChange() {
    console.log(1111)
}

function add_room_price() {
    var frm = $('#f_add_room_price');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0){
                console.log('Submission was successful.');
                $("#add_hotel_error_info").text("添加成功！");
                $('#addPriceTypeModal').modal('hide')
                reload_price_type();
            }else {
                console.log('Submission was Failed: '+ data.bstatus.des);
                $("#add_hotel_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            console.log('An error occurred.');
            console.log(data);
            $("#add_hotel_error_info").text("系统错误");
        },
    });
}

function update_price_type() {
    var frm = $('#f_update_price_type');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0){
                //console.log('Submission was successful.');
                $("#update_hotel_error_info").text("修改成功！");
                $('#updatePriceTypeModal').modal('hide')
                reload_price_type();
            }else {
                $("#update_hotel_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            //console.log(data);
            $("#update_hotel_error_info").text("系统错误");
        },
    });
}

function reload_price_type() {
    $.ajax({
            type: 'get',
            url: url,
            success: function (data) {
//                console.log(data);
                $('#price_set_type_list').bootstrapTable('load', data.data);
            }}
    )
}


/**
* 渠道相关的价格计划有效性
**/
function ota_price_set(value,row,index){
    return "<button  onclick='set_active_price_set("+value+")'>"+"<span  id='room_active_"+index+"'>有效</span>"+"</button>";
}
/**
*设置产品的有效性
*/
function set_active_price_set(hotelPriceId){
    var type;
    $.ajax({
        type:"POST",
        url:"/room/set/price/active",
        data:jQuery.param({hotelId:g_hotelId,hotelPriceId:hotelPriceId,type:1}),
        success:function(data){
            console.log(data);
            if (data.bstatus.code == 0){
                //数据库添加有效性字段

            }
        }
    });
}
function addCheck(){
    var refroomtypeCheckedText = '';
    $.each($("input[name='hotelRoomTypeIdAdd']"),function () {
         if(this.checked){
               refroomtypeCheckedText += this.nextElementSibling.innerText + " , ";
         }
     });
    $("#addCheckTest").val(refroomtypeCheckedText);
}
function reset_add_room_type() {
    $("#addpriceTypeName").val('');
    $("#addCheckTest").val('');
    $("#eatNum").val(0);
    $(".caigou_one").val("")
    $("#gridRadios1").prop('checked',true);
}