$(function () {
   show_list($("#settle-hotel-list"), '/jiudian/settle/list_super', superParams);
    $.ajax({
        type: 'get',
        url: '/jiudian/settle/list_super',
        success: function (res) {
            console.log(res)
        }
    })
});
function superParams (params) {
    params.hotelId = g_channel
}
function show_list(table,url,qparam) {
    $(table).bootstrapTable({
        url: url,
        method: 'get',
        pagination: true,
        sidePagination:'client',  //client前端 server服务端
        pageNumber:1,
        pageSize: 10,
        pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
        search: false,
        queryParams: qparam,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        class:"table table-hover row",
        columns: [{
            field: 'business_id',
            title: 'ID',
        }, {
            field: 'business_name',
            title: '供应商',
        }, {
            field: 'business_phone',
            title: '联系电话',
        }, {
            field: 'business_address',
            title: '地址',
        }, {
            field: 'settle-detail',
            title: '操作',
            formatter:to_detail
        }
        ]});
}
function to_detail(value, row, index) {
    // console.log(row)
    var html = '';
    html = '<a class="gys-btns" href="javascript:;" onclick="openGysDetail(' + row.business_id + ')">详情</a>';
    return html
}
function openGysDetail(gysId) {
    // console.log(gysId)
    var ids = gysId
    window.location.href = "/settle_super_gyslist?business_id="+ids
}
// 导出
function daochu() {
    $('#settle-hotel-list').tableExport({ type: 'excel', escape: 'false', ignoreColumn: [4], fileName: '供应商' })
}