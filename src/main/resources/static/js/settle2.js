// function is_undefined(o) {
//     return typeof o == undefined;
// }
//
// function hotel_search_compoment(e_area,ipt_id,f_cb,search_hotel_id) {
//     //酒店搜索框
//     var e_search = $(e_area).empty();
//     var e_h0 = $('<input type="hidden" name="'+ipt_id+'" id="'+ipt_id+'" />')
//     e_search.append(e_h0);
//     var h1 = '<input type="search" placeholder="输入后按回车搜索" class="form-control" aria-haspopup="true" aria-expanded="false" />';
//     var e_h1 = $(h1);
//     e_search.append(e_h1);
//     var e_h2= $('<ul class="dropdown-menu" style="margin-top: -1px;padding:0;" ></ul>');
//     e_search.append(e_h2);
//     //初始化第一家酒店
//     $.ajax({
//         type: 'get',
//         url: '/user/setting/cur/hotel',
//         success: function (data) {
//             if (data.bstatus.code == 0){
//                 if (data.data != 'null'){
//                     var hoteId = data.data.hotelId;
//                     var hoteName = data.data.hotelName;
//                     e_h0.val(hoteId);
//                     e_h1.val(hoteName);
//
//                     if(typeof ws_order_msg != "undefined"){
//                         ws_order_msg.re_connect(hoteId); //订阅该酒店的消息
//                     }
//                 }
//                 //查询酒店下面的房型，添加的界面需要
//                 if(search_hotel_id){
//                     f_cb(search_hotel_id);
//                 }
//             }else {
//                 e_h1.val('无酒店');
//             }
//         }
//     });
//
//     //添加事件
//     e_h1.on('click',function () {
//         $.ajax({
//                 type: 'get',
//                 url: '/hotel/search/name',
// //                data: jQuery.param({name : this.value}) ,
// //                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//                 success: function (data) {
//                     e_h2.empty();
//                     $.each(data.rows,function () {
//                         var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
//                         e_h3.text(this.hotelName);
//                         // var hotelId = this.hotelId;
//                         e_h3.val(this.hotelId)
//                         e_h3.prop("showName",this.hotelName)
//                         e_h3.on('click',function () {
//                             e_h1.val(this.showName);
//                             var hotelId = this.value;
//                             $("#"+ipt_id).val(this.value)
//                             e_h2.hide();
//
//                             //订阅该酒店的消息
//                             if(typeof ws_order_msg != "undefined"){
//                                 ws_order_msg.re_connect(hotelId);
//                             }
//
//                             //设置当前关联酒店
//                             $.ajax({
//                                     type: 'get',
//                                     url: '/user/setting/cur/hotel/set',
//                                     data: jQuery.param({hotelId : hotelId}) ,
//                                     success: function (data) {
//                                         console.log(data);
//                                     }
//                                 }
//                             )
//
//                             if(f_cb){
//                                 f_cb(this.value);
//                             }
//                         });
//                         e_h3.appendTo(e_h2);
//                     });
//                     e_h2.show();
//                 }}
//         )
//     });
//     //酒店搜索添加事件
//     e_h1.on('search',function () {
//         $.ajax({
//             type: 'get',
//             url: '/hotel/search/name',
//             data: jQuery.param({name : this.value}) ,
//             contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//             success: function (data) {
//                 e_h2.empty();
//                 if (data.total == 0){
//                     e_h2.append('<li class="dropdown-item list-group-item text-danger">没有搜索到相关结果</li>')
//                 }else {
//                     $.each(data.rows,function () {
//                         var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
//                         e_h3.text(this.hotelName);
//                         e_h3.val(this.hotelId)
//                         e_h3.prop("showName",this.hotelName)
//                         e_h3.on('click',function () {
//                             e_h1.val(this.showName);
//                             var hotelId = this.hotelId;
//                             $("#"+ipt_id).val(hotelId)
//                             e_h2.hide();
//
//                             //订阅该酒店的消息
//                             if(typeof ws_order_msg != "undefined"){
//                                 ws_order_msg.re_connect(hotelId);
//                             }
//
//                             //设置当前关联酒店
//                             $.ajax({
//                                     type: 'get',
//                                     url: '/user/setting/cur/hotel/set',
//                                     data: jQuery.param({hotelId : hotelId}) ,
//                                     success: function (data) {
//                                         console.log(data);
//                                     }
//                                 }
//                             )
//
//                             //触发查询房型
//                             if(f_cb){
//                                 f_cb(this.value);
//                             }
//                         });
//                         e_h3.appendTo(e_h2);
//                     });
//                 }
//                 e_h2.show();
//             }}
//         )
//     });
// }
//
//
// function bind_datapicker() {
//     //绑定日历控件
//     $('#search_date_start').datepicker({
//         uiLibrary: 'bootstrap4',
//         format: 'yyyy-mm-dd',
//         locale: 'zh-cn',
//     });
//     $('#settle_search_date_start').datepicker({
//         uiLibrary: 'bootstrap4',
//         format: 'yyyy-mm-dd',
//         locale: 'zh-cn',
//     });
//     $('#settle_search_date_end').datepicker({
//         uiLibrary: 'bootstrap4',
//         format: 'yyyy-mm-dd',
//         locale: 'zh-cn',
//     });
//
//     $('#invoice_search_date_start').datepicker({
//         uiLibrary: 'bootstrap4',
//         format: 'yyyy-mm-dd',
//         locale: 'zh-cn',
//     });
//     $('#invoice_search_date_end').datepicker({
//         uiLibrary: 'bootstrap4',
//         format: 'yyyy-mm-dd',
//         locale: 'zh-cn',
//     });
// }
//
// function reload_order_list() {
//     query_order_list();
// }
//
// //刷新、查询订单信息
// function query_order_list() {
//
//     var search_date_start = $("#search_date_start").prop("value");
//     var search_date_end = $("#search_date_end").prop("value");
//     var search_order_status = 2;
//     var settle_status = $("#settle_status").prop("value");
//
//     //结算状态
//     $('#order_list_t').bootstrapTable('refresh', {
//         query: {offset: 0,
//             search_date_start:search_date_start,
//             search_date_end:search_date_end,
//             search_order_status:search_order_status,
//             search_settle_status:settle_status
//         }
//     });
//     settleMoney();
// }
// //显示订单列表-管理界面
// function show_order_list(table,url,qparam) {
//     $(table).bootstrapTable({
//         method: 'get',
//         pagination: true,
//         sidePagination:'server',
//         pageSize: 10,
//         pageList: [5, 10, 25, 50, 100, 200],//list can be specified here
//         url: url,
//         queryParams: qparam,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         class:"table table-hover row",
//         columns: [{
//             field:'orderChannelCode',
//             title:'渠道'
//         },{
//             field: 'orderNo',
//             title: '渠道单号',
//         }, {
//             field: 'orderId',
//             title: '订单号',
//         }, {
//             field: 'bookTimeStr',
//             title: '预定时间',
//         }, {
//             field: 'bookUser',
//             title: '预订人',
//         }, {
//             field: 'bookMobile',
//             title: '联系电话',
//         }, {
//             field: 'hotelRoomTypeName',
//             title: '房间类型',
//         },{
//             field: 'roomCount',
//             title: '房间数',
//         },{
//             field: 'checkInOutTimeStr',
//             title: '入离时间',
//         },{
//             field: 'price',
//             title: '订单金额',
//         },{
//              field:'bookRemark',
//              title:'备注',
//             // formatter:word_all,
//          },{
//             field: 'orderStateStr',
//             title: '订单状态',
//         },{
//             field: 'settleStatusStr',
//             title: '结算状态',
//         }
//         ]});
// }
//
// //显示订单列表-管理界面
// function show_checkorderList_user(table,url,qparam) {
//     $(table).bootstrapTable({
//         method: 'get',
//         pagination: true,
//         sidePagination:'server',
//         pageSize: 10,
//         pageList: [5, 10, 25, 50, 100, 200],//list can be specified here
//         url: url,
//         queryParams: qparam,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         class:"table table-hover row",
//         columns: [{
//             field: 'orderId',
//             title: '订单号',
//         }, {
//             field: 'bookTimeStr',
//             title: '预定时间',
//         }, {
//             field: 'bookUser',
//             title: '预订人',
//         }, {
//             field: 'bookMobile',
//             title: '联系电话',
//         }, {
//             field: 'hotelRoomTypeName',
//             title: '房间类型',
//         },{
//             field: 'roomCount',
//             title: '房间数',
//         },{
//             field: 'checkInOutTimeStr',
//             title: '入离时间',
//         },{
//             field: 'price',
//             title: '结算金额',
//         },{
//             field:'bookRemark',
//             title:'备注',
//         },{
//             field: 'orderStateStr',
//             title: '订单状态',
//         },{
//             field: 'settleStatusStr',
//             title: '结算状态',
//         }
//         ]});
// }
//
// //刷新、查询订单信息
// function query_order_list_user() {
//
//     var search_date_start = $("#search_date_start").prop("value");
//     var search_date_end = $("#search_date_end").prop("value");
//     var search_order_status = 2;
//     var settle_status = $("#settle_status").prop("value");
//
//     //结算状态
//     $('#order_list_t').bootstrapTable('refresh', {
//         query: {offset: 0,
//             search_date_start:search_date_start,
//             search_date_end:search_date_end,
//             search_order_status:search_order_status,
//             search_settle_status:settle_status
//         }
//     });
// }
//
// function word_all(value, row, index){
//     var operaton ='<button  data-toggle="modal" data-target="#moreOrderModal" class="btn btn-sm btn-outline-primary" onclick="showOrderDetail('+value+')"><span>明细</span></button>&nbsp;';
//
//     if(row.settleStatus == 2){
//         operaton = operaton+'<button  class="btn btn-sm disabled" disabled="disabled" onclick="fishSettle('+value+')"><span>完成</span></button>&nbsp;'
//     }else{
//         operaton = operaton+'<button  class="btn btn-sm btn-outline-primary" onclick="fishSettle('+value+')"><span>完成</span></button>&nbsp;'
//     }
//
//     if(row.hasCreated == 1){
//         operaton = operaton+'<button  class="btn btn-sm disabled" disabled="disabled" id="createInvoice_"+value onclick="sendInvoice('+value+')"><span>生成电子发票并发送</span></button>';
//     }else{
//         operaton =operaton+'<button  class="btn btn-sm btn-outline-primary"  id="createInvoice_"+value onclick="sendInvoice('+value+')"><span>生成电子发票并发送</span></button>';
//     }
//     return operaton;
// }
//
// function word_all_user(value, row, index){
//     var operaton  = '<button  data-toggle="modal" data-target="#moreOrderModal" class="btn btn-sm btn-outline-primary" onclick="showOrderDetail('+value+')"><span>明细</span></button>&nbsp;&nbsp;';
//
//     if(row.hasCreated == 1){
//         operaton = operaton+'<button  class="btn btn-sm disabled" disabled="disabled" id="createInvoice_"+value onclick="sendInvoice('+value+')"><span>生成电子发票并发送</span></button>';
//     }else{
//         operaton =operaton+'<button  class="btn btn-sm btn-outline-primary"  id="createInvoice_"+value onclick="sendInvoice('+value+')"><span>生成电子发票并发送</span></button>';
//     }
//     return operaton;
//
// }
// //显示账单所属的订单明细
// function showOrderDetail(value){
//    //显示订单汇总
//     reload_orderDetail("#orderDetailSum",value)
//    //显示订单细节
//     reload_orderDetail("#orderDetail",value);
// }
//
// function reload_orderDetail(tab,value){
//     $(tab).bootstrapTable('refresh', {
//         query: {offset: 0,
//             billId:value}
//     });
// }
//
//
// //完成结算操作
// function sendInvoice(value){
//     //更新结算订单并再次进行查询
//     var url ='/hotel/invoice/genertorAndSend';
//     $.ajax({
//         type:'get',
//         url:url,
//         data: jQuery.param({billId : value}) ,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         success: function (data) {
//             var result = data.data;
//             if (data.bstatus.code == 0) {
//                 //重新加载
//                 query_settleBill_list();
//             }
//         }
//     });
// }
//
// //完成结算操作
// function fishSettle(value){
//    //更新结算订单并再次进行查询
//     var url ='/settle/bill/updateStatus';
//     $.ajax({
//         type:'get',
//         url:url,
//         data: jQuery.param({billId : value,status:2}) ,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         success: function (data) {
//             if (data.bstatus.code == 0) {
//                 //重新加载
//                query_settleBill_list();
//             }
//         }
//     });
//
// }
//
// //显示订单列表-用户界面
// function show_order_list_user(table,url,qparam) {
//     $(table).bootstrapTable({
//         method: 'get',
//         pagination: true,
//         sidePagination:'server',
//         pageSize: 10,
//         pageList: [5, 10, 25, 50, 100, 200],//list can be specified here
//         url: url,
//         queryParams: qparam,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         class:"table table-hover row",
//         columns: [{
//             field: 'orderId',
//             title: '订单号',
//             formatter:function (value, row, index) {
//                 return [
//                     '<a class="look" href="javascript:void(0)">',
//                     value,
//                     '</a>'
//                 ].join('');
//             },
//             events:{
//                 'click .look': function (e, value, row, index) {
//                     // alert('You click like icon, row: ' + JSON.stringify(row));
//                     // console.log(value, row, index);
//                     modal_order_info({data:row});
//                 }
//             }
//         }, {
//             field: 'bookTimeStr',
//             title: '预定时间',
//         }, {
//             field: 'bookUser',
//             title: '预订人',
//         }, {
//             field: 'bookMobile',
//             title: '联系电话',
//         }, {
//             field: 'hotelRoomTypeName',
//             title: '产品名称',
//         },{
//             field: 'roomCount',
//             title: '房间数',
//         },{
//             field: 'checkInOutTimeStr',
//             title: '入离时间',
//         },
//         {
//               field:'bookRemark',
//               title:'备注',
//               formatter:word_all,
//         },{
//             field: 'orderStateStr',
//             title: '订单状态',
//
//         },{
//                 field: 'settleStatusStr',
//                 title: '结算状态',
//             }
//         ]});
// }
// function confirm_order(e,orderId,index) {
//     var url ='/order/status/update';
//     $.ajax({
//         type:'get',
//         url:url,
//         data: jQuery.param({orderId : orderId,status:2}) ,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         success: function (data) {
//             if (data.bstatus.code == 0) {
//                 $(e).removeClass('btn-outline-danger');
//                 $(e).addClass('btn-outline-secondary');
//                 $(e).parent().prev().text("已确认");
//                // $('#order_list_t').bootstrapTable('updateCell',{index:index,field:'orderStateStr',value:'已确认'});
//             }
//         }
//     });
// }
//
// function orderListQueryParams(params) {
//     params.search_date_start = $("#search_date_start").prop("value");
//     params.search_date_end = $("#search_date_end").prop("value");
//     params.search_order_status = 2;
//     params.search_hotel_id = $("#search_hotel_id").prop("value");
//     return params;
// }
//
// //根据搜索条件进行导出
// function exportBillExcel(params) {
//     var search_date_start = $("#settle_search_date_start").prop("value");
//     //结束日期
//     var search_date_end = $("#settle_search_date_end").prop("value");
//     //结算账单状态
//     var search_settle_status =$("#search_settle_status").prop("value");
//     //酒店id
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//
//    //获取查询条件 ajax 调用 所见即所得
//
//     var url ='/settle/bill/exportExcel';
//     /*
//     $.ajax({
//         type:'get',
//         url:url,
//         async: false,
//         dataType: "json",
//         data: { search_date_start: search_date_start, search_date_end: search_date_end,
//                 search_settle_status:search_settle_status,search_hotel_id:search_hotel_id },
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         success: function (data) {
//         }
//     });
//   */
//   location.href="/settle/bill/exportExcel?search_date_start="+search_date_start+ "&search_date_end="+search_date_end+"&search_hotel_id="+search_hotel_id+"&search_settle_status="+search_settle_status;
//
// }
//
//
//
// //根据搜索条件进行导出
// function exportExcel() {
//     var search_date_start =  $("#search_date_start").prop("value");
//     var search_date_end = $("#search_date_end").prop("value");
//     var search_date_type = 3;
//     var search_settle_status = $("#settle_status").prop("value");
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//
//     location.href="/order/orderExportSettleExcle?search_date_start="+search_date_start+"&search_date_end="+search_date_end+"&search_date_type="+search_date_type +"&search_settle_status="+search_settle_status +"&search_hotel_id="+search_hotel_id;
//
// }
//
//
// //根据条件查询
// function search_wheres() {
//     query_settleBill_list();
// }
//
//
// //tab 页面的切换
// function query_order_list_case() {
//     //查询全部
//     //查询今日入住
//     //查询待确认
//     $.each($("a[name=query_nav]"),function () {
//         var value = this.id;
//         if(value == 'query_nav_1'){
//             if(this.className.indexOf("active") != -1){
//                 query_order_list_all();
//             }
//         }else if(value == 'query_nav_2'){
//             if(this.className.indexOf("active") != -1){
//                 query_settle_bill();
//             }
//         }
//     });
//
// }
//
//
// //查询待确认订单
// function query_settle_bill() {
//
//     //开始日期
//     var search_date_start = $("#search_date_start").prop("value");
//     var search_date_end = $("#search_date_end").prop("value");
//     var search_order_status = 2;
//     $('#order_list_t').bootstrapTable('refresh', {
//         query: {offset: 0,
//             search_date_start:search_date_start,
//             search_date_end:search_date_end,
//             search_order_status:search_order_status,
//         }
//     });
//
// }
//
//
// //弹窗提示
// function selectPrompt(erea,info) {
//     var p = $(erea).empty();
//
//     h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
//     h2 = $('<div class="modal-dialog modal-sm">');
//     h2.appendTo(h1);
//     h3 = $('<div class="modal-content">');
//     h3.appendTo(h2);
//     h4=$('<div class="modal-header">');
//     h4.appendTo(h3);
//     h4.append('<h6 class="modal-title">提示：</h6>');
//     h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
//     h5 = $('<div class="modal-body">');
//     h5.appendTo(h3);
//     h5.html('<p class="text-danger">'+info+'</p>');
//     h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-default" data-dismiss="modal" style="float:right">取消</button>')
//     .append('<button type="button" class="btn btn-primary" data-dismiss="modal" id="selectPrompt">确定</button>')
//     h6.appendTo(h3);
//
//     p.append(h1);
//
//     h1.modal('show');
// }
// //弹窗提示
// function tips(erea,info) {
//     var p = $(erea).empty();
//
//     h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
//     h2 = $('<div class="modal-dialog modal-sm">');
//     h2.appendTo(h1);
//     h3 = $('<div class="modal-content">');
//     h3.appendTo(h2);
//     h4=$('<div class="modal-header">');
//     h4.appendTo(h3);
//     h4.append('<h6 class="modal-title">提示：</h6>');
//     h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
//     h5 = $('<div class="modal-body">');
//     h5.appendTo(h3);
//     h5.html('<p class="text-danger">'+info+'</p>');
//     h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>')
//     h6.appendTo(h3);
//
//     p.append(h1);
//
//     h1.modal('show');
// }
// function modal_order_info(info) {
//     var order = info.data;
//     var p = $("#modal_order_info").empty();
//
//     h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
//     h2 = $('<div class="modal-dialog">');
//     h2.appendTo(h1);
//     h3 = $('<div class="modal-content">');
//     h3.appendTo(h2);
//     h4=$('<div class="modal-header">');
//     h4.appendTo(h3);
//     h4.append('<h6 class="modal-title">订单详情：</h6>');
//     h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
//     h5 = $('<div class="modal-body">');
//     h5.appendTo(h3);
//     ul = $('<div class="list-group border-0">')
//     ul.append('<li class="list-group-item"><div class="ml-3">订单号：'+order.orderId+'</div></li>')
//     ul.append('<li class="list-group-item"><div>订单状态：'+order.orderStateStr+'</div></li>')
//     ul.append('<li class="list-group-item"><div>酒店名称：'+order.hotelName+'</div></li>')
//     ul.append('<li class="list-group-item"><div>预定房型：'+order.hotelRoomTypeName+' - '+order.sellTypeName+order.roomCount+'间</div></li>')
//     ul.append('<li class="list-group-item"><div>入住时间：'+order.checkInTimeStr+'</div></li>')
//     ul.append('<li class="list-group-item"><div>离店时间：'+order.checkOutTimeStr+'</div></li>')
//     ul.append('<li class="list-group-item"><div class="ml-3">预订人：'+order.bookUser+'</div></li>')
//     if(order.bookMobile !=null){
//         ul.append('<li class="list-group-item"><div>联系电话：'+order.bookMobile+'</div></li>')
//     }else{
//         ul.append('<li class="list-group-item"><div>联系电话：未添加</div></li>')
//     }
//
//     ul.append('<li class="list-group-item"><div>订单备注：'+order.bookRemark+'</div></li>')
//     h5.append(ul);
//     h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>')
//     h6.appendTo(h3);
//
//     p.append(h1);
//
//     h1.modal('show');
// }
//
// //------------------------------------------------------------- 分隔线 ------------------------------------------------------------
//
// //结算操作
// function settleMoney(){
//     var search_date_start = $("#search_date_start").prop("value");
//     var search_date_end = $("#search_date_end").prop("value");
//     var search_order_status = 2;
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//     $.ajax({
//         type:'GET',
//         url:'/hotel/settle/settlInfo',
//         data: jQuery.param({"search_date_start":search_date_start,"search_date_end":search_date_end,
//           "search_order_status":search_order_status,"search_hotel_id":search_hotel_id }) ,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         success: function (data) {
//             var settleMoney = data.settleMoney;
//             var commissionMoney = data.commissionMoney;
//             $("#settleMoneyStr").text(settleMoney);
//             $("#commissionMoneyStr").text(commissionMoney);
//         }
//     });
// }
// //查询全部
// function query_order_list_all() {
//     query_order_list();
// }
//
//
// //结算订单信息的确认
// function settle_order(){
//     var search_date_start = $("#search_date_start").prop("value");
//     var search_date_end = $("#search_date_end").prop("value");
//     var search_order_status = 2;
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//
//     $.ajax({
//         type:'GET',
//         url:'/hotel/settle/bill/info',
//         data: jQuery.param({"search_date_start":search_date_start,"search_date_end":search_date_end,
//             "search_order_status":search_order_status,"search_hotel_id":search_hotel_id }) ,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         success: function (data) {
//             var hotelId = data.hotelId;
//             var orderIds = data.orderIds;
//             var settleMoney = data.settleMoney;
//             var commissionMoney = data.commissionMoney;
//             var settleCycle = data.settleCycle;
//             var settleBankType = data.settleBankType;
//             var settleBankAccount = data.settleBankAccount;
//             var startDate = data.startDate;
//             var endDate = data.endDate;
//
//             $("#hotelId").val(hotelId);
//             $("#orderIds").val(orderIds);
//             $("#settleMoney").val(settleMoney);
//             $("#commissionMoney").val(commissionMoney);
//             $("#settleCycle option").each(function() {
//                 //var value = item.val();
//                 var value = $(this).val();
//                 if (value == settleCycle) {
//                     $(this).attr("selected", true);
//                 }
//             });
//             $("#settleBankType").val(settleBankType);
//             $("#settleBankAccount").val(settleBankAccount);
//
//             $("#startDate").val(startDate);
//             $("#endDate").val(endDate);
//         }
//     });
//
// }
//
//
// //添加结算订单信息
// function add_settleBill(){
//     var frm=$('#f_add_settlebill');
//     $.ajax({
//         type:frm.attr('method'),
//         url:frm.attr('action'),
//         data:frm.serialize(),
//         success:function(data){
//             if (data.bstatus.code == 0){
//                 $("#add_settle_error_info").text("添加成功");
//                 $("#addSettleOrderModal").modal('hide');
//                 reload_order_list();
//             }else {
//                 $("#add_settle_error_info").text(data.bstatus.des);
//             }
//         },
//         error: function (data) {
//             $("#update_hotel_error_info").text("系统错误");
//         },
//
//     });
//
// }
//
//
// function showTabs(currentEle){
//     //隐藏所有
//     $.each($("a[name=query_nav]"),function (index,item) {
//         var hrefName = $(this).attr("href");
//         $(hrefName).css('display','none');
//     });
//
//     var show = $("#"+currentEle).attr("href");
//     $(show).css('display','block');
// }
//
//
// //进行订单结算处理的tabs
// function showOrderSettleTabs(currentEle){
//     showTabs(currentEle);
//     //查询定订单列表
//     query_order_list()
// }
//
// //进行订单结算处理的tabs
// function showOrderSettleTabs_user(currentEle){
//     showTabs(currentEle);
//     //查询定订单列表
//     query_order_list_user()
// }
// //amdin 结算账单列表
// function showSettleBillTabs(currentEle){
//     showTabs(currentEle);
//
//     //手动显示日期
//     //setDateBetween("settle_search_date_start","settle_search_date_end",currentTime());
//     query_settleBill_list()
// }
//
// function show_settleBill_list_user(table,url,qparam){
//     $(table).bootstrapTable({
//         method: 'get',
//         pagination: true,
//         sidePagination:'server',
//         pageSize: 10,
//         pageList: [5, 10, 25, 50, 100, 200],//list can be specified here
//         url: url,
//         queryParams: qparam,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         class:"table table-hover row",
//         columns: [{
//             field:'createTime',
//             title:'记账日期'
//         },{
//             field: 'settleCycleName',
//             title: '结算周期',
//         }, {
//             field: 'settleBankType',
//             title: '结算银行',
//         }, {
//             field: 'settleBankAccountFuzzy',
//             title: '结算账户',
//         }, {
//             field: 'settleMoney',
//             title: '结算金额',
//         }, {
//             field: 'settleStatusName',
//             title: '结算状态',
//         }, {
//             field: 'settleTime',
//             title: '结算时间',
//         },{
//             field: 'remark',
//             title: '备注',
//         },{
//             field:'id',
//             title:'操作',
//             formatter:word_all_user,
//         }
//         ]});
//
// }
// //查询记账列表
// function query_settleBill_list(){
//     var search_date_start = $("#settle_search_date_start").prop("value");
//     //结束日期
//     var search_date_end = $("#settle_search_date_end").prop("value");
//     //结算账单状态
//     var search_settle_status =$("#search_settle_status").prop("value");
//     //酒店id
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//     $('#settle_list_t').bootstrapTable('refresh', {
//         query: {offset: 0,
//             search_date_start:search_date_start,
//             search_date_end:search_date_end,
//             search_settle_status:search_settle_status,
//             search_hotel_id:search_hotel_id
//         }
//     });
// }
//
// //显示日期
// function setDateBetween(stareEle,endEle,date){
//     $("#"+stareEle).val(date);
//     $("#"+endEle).val(date);
// }
//
//
//
// //记账初始化查询
// function settleBillListQueryParams(params) {
//     //开始日期
//     params.search_date_start = $("#settle_search_date_start").prop("value");
//     //结束日期
//     params.search_date_end = $("#settle_search_date_end").prop("value");
//     //结算账单状态
//     params.search_settle_status =$("#search_settle_status").prop("value");
//     //酒店id
//     params.search_hotel_id = $("#search_hotel_id").prop("value");
//     return params;
// }
//
//
// //记账初始化查询
// function settleInvoiceListQueryParams(params) {
//     //开始日期
//     params.search_date_start = $("#invoice_search_date_start").prop("value");
//     //结束日期
//     params.search_date_end = $("#invoice_search_date_end").prop("value");
//     return params;
// }
//
//
// function show_settleBill_list(table,url,qparam){
//     $(table).bootstrapTable({
//         method: 'get',
//         pagination: true,
//         sidePagination:'server',
//         pageSize: 10,
//         pageList: [5, 10, 25, 50, 100, 200],//list can be specified here
//         url: url,
//         queryParams: qparam,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         class:"table table-hover row",
//         columns: [{
//             field:'createTime',
//             title:'记账日期'
//         },{
//             field: 'settleCycleName',
//             title: '结算周期',
//         }, {
//             field: 'settleBankType',
//             title: '结算银行',
//         }, {
//             field: 'settleBankAccountFuzzy',
//             title: '结算账户',
//         }, {
//             field: 'settleMoney',
//             title: '结算金额',
//         }, {
//             field: 'settleStatusName',
//             title: '结算状态',
//         }, {
//             field: 'settleTime',
//             title: '结算时间',
//         },{
//             field: 'remark',
//             title: '备注',
//         },{
//             field:'id',
//             title:'操作',
//             formatter:word_all,
//         }
//         ]});
// }
//
// //获取当前时间  格式2018-07-25 04:15
// function currentTime(){
//     var currentDate = new Date();
//     var year=currentDate.getFullYear();
//     //获取当前月
//     var month=currentDate.getMonth()+1;
//     //获取当前日
//     var date=currentDate.getDate();
//     var h=currentDate.getHours();       //获取当前小时数(0-23)
//     var m=currentDate.getMinutes();     //获取当前分钟数(0-59)
//
//     var now=year+'-'+add0(month)+"-"+add0(date);
//     return now;
// }
//
//
// function add0(m){return m<10?'0'+m:m }
//
//
//
//
//
// /////////////////////////////////////////////////////////////////////////////////
//
//
// //隐藏其他tab 显示点击的tab页面
// function showTabs(currentEle){
//     //隐藏所有
//     $.each($("a[name=query_nav]"),function (index,item) {
//         var hrefName = $(this).attr("href");
//         $(hrefName).css('display','none');
//     });
//
//     var show = $("#"+currentEle).attr("href");
//     $(show).css('display','block');
// }
//
//
//
// /**
//  * 银行信息的结算管理
//  */
// function showSettleManage(currentEle){
//     showTabs(currentEle);
//     //查询手机信息并进行
//     //查询定订单列表
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//     queryBankInfo(search_hotel_id);
// }
//
// /**
//  * 查询当前酒店的银行卡信息
//  * @param hotelId 酒店id
//  */
// function queryBankInfo(hotelId){
//     $.ajax({
//         type:"POST",
//         url:"/hotel/settle/index",
//         data:{"hotelId":hotelId},
//         success:function(data){
//             var result = data.data;
//             if(data.bstatus.code == 0)
//                 show_bank_info(result);
//         }
//     });
//
// }
//
// //回显数据
// function show_bank_info(result){
//     var id = result.id;
//     var bankUseType = result.bankUseType;
//     var bankAccountType = result.bankAccountType;
//     var bankAccountName = result.bankAccountName;
//     var bankAccountCardnumber = result.bankAccountCardnumber;
//     var bankSignPhone = result.bankSignPhone;
//     var settleMsgPhone = result.settleMsgPhone
//
//     $("#bankUseTypes input").each(function() {
//         //var value = item.val();
//         var value = $(this).val();
//         if (value == bankUseType) {
//             $(this).prop("checked", "checked");
//         }
//     });
//
//     $("#settle_id").val(id);
//
//     $("#bankAccountType").val(bankAccountType);
//
//     $("#bankAccountName").val(bankAccountName);
//
//     $("#bankAccountCardnumber").val(bankAccountCardnumber);
//
//     $("#bankSignPhone").val(bankSignPhone);
//
//     if(settleMsgPhone != null && settleMsgPhone != undefined && settleMsgPhone.trim()!=""){
//         $("#settle_msg_phone").val(settleMsgPhone);
//     }
//     disableForm("disableEle");
// }
//
// //显示隐藏的字段
// function show_hiden_field(){
//     $("#signPhone").css("display", "block");
//     $("#reset_bankInfo").on("click",reset_bankInfo);
// }
// function  hiden_field() {
//     $("#SMSVerification").val("");
//     $("#resetBank").css("display","block");
//     $("#comfirmBank").css("display","none");
//     $("#signPhone").css("display", "none");
//
// }
//
// //禁用属性
// function disableForm(className){
//     $("."+className).attr("disabled","disabled");
// }
// //开启属性
// function unDisableForm(className){
//     $("."+className).removeAttr("disabled");
// }
//
// //获取验证码信息
// function getIdenCode(){
//     //获取手机号信息
//     var signPhone = $("#bankSignPhone").val();
//     if(signPhone == null || signPhone == undefined || signPhone.trim()==""){
//         //消息提醒 modal
//         tips($("#modal_warn"),"签约手机号为空，请联系管理员进行设置");
//     }
//     $.ajax({
//         type:"POST",
//         url:"/hotel/settle/updateBank/indentidyCode",
//         data:{"bankSignPhone":signPhone},
//         success:function(data){
//             if(data.bstatus.code==0){
//                 var code = data.data;
//                 $("#smsCode").val(code);
//             }
//         }
//     });
// }
//
//
// //重新填写银行信息
// function reset_bankInfo() {
//     //先判断用户输入的验证码和生成的验证码
//     var smsCode = $("#smsCode").val();
//     var smsCode_user = $("#SMSVerification").val();
//     if(smsCode == null || smsCode == undefined || smsCode.trim() == ""){
//         //||smsCode_user == null || smsCode_user == undefined || smsCode_user.trim() == ""
//         tips($("#modal_warn"),"验证码不能为空");
//         return ;
//     }
//     if(smsCode != smsCode_user){
//         tips($("#modal_warn"),"输入验证码错误，请重新输入");
//         return;
//     }
//     //将所有的银行卡相关的信息禁用状态转换为可用状态
//     unDisableForm("disableEle");
//     //隐藏 该按钮，显示别的按钮
//     $("#smsCode").val("");
//     $("#resetBank").css("display","none");
//     $("#comfirmBank").css("display","block");
//     return ;
// }
//
// //更新银行卡信息
// function update_bankInfo(){
//     var bankUseType = null;
//     var id = $("#settle_id").val();
//     var bankAccountType = $("#bankAccountType").val();
//     var bankAccountName = $("#bankAccountName").val();
//     var bankAccountCardnumber = $("#bankAccountCardnumber").val();
//     var bankSignPhone = $("#bankSignPhone").val();
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//
//     $("#bankUseTypes input").each(function() {
//         var checked = $(this).prop("checked");
//         if (checked) {
//             bankUseType = $(this).val();
//         }
//     });
//
//     $.ajax({
//         type:"POST",
//         url:"/hotel/settle/update",
//         data:{"id":id,"hotelId":search_hotel_id,"bankUseType":bankUseType,"bankAccountType":bankAccountType,"bankAccountName":bankAccountName,
//             "bankAccountCardnumber":bankAccountCardnumber,"bankSignPhone":bankSignPhone},
//         success:function(data){
//             showSettleManage("query_nav_4");
//             hiden_field();
//             return ;
//         }
//     });
// }
//
// //取消酒店信息
// function cancel_bankInfo(){
//     //重新加载
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//     queryBankInfo(search_hotel_id);
//     //隐藏 该按钮，显示别的按钮
//     $("#resetBank").css("display","block");
//     $("#comfirmBank").css("display","none");
//     //禁用
//     disableForm("disableEle");
//     hiden_field();
//     return ;
// }
//
//
// /**
//  * 初始化消息提醒的相关信息
//  * @param hotelId 当前酒店id
//
//  function init_msg_notify_info(hotelId){
//     $.ajax({
//         type:"POST",
//         url:"/message/msgInit",
//         data:{"hotelId":hotelId},
//         success:function(data){
//             show_sms_notify(data.msgNotifyPhone);
//         }
//     });
// }
//  */
// //鼠标移入
// function mouseOver(obj){
//     //var th = $(obj);
//     $(obj).children("a.delete").css("display", "block");
// }
// //鼠标移出
// function mouseOut(obj){
//     //var th = $(obj);
//     $(obj).children("a.delete").css("display", "none");
// }
//
// //弹窗提示
// function tips(erea,info) {
//     var p = $(erea).empty();
//
//     h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
//     h2 = $('<div class="modal-dialog modal-sm">');
//     h2.appendTo(h1);
//     h3 = $('<div class="modal-content">');
//     h3.appendTo(h2);
//     h4=$('<div class="modal-header">');
//     h4.appendTo(h3);
//     h4.append('<h6 class="modal-title">提示：</h6>');
//     h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
//     h5 = $('<div class="modal-body">');
//     h5.appendTo(h3);
//     h5.html('<p class="text-danger">'+info+'</p>');
//     h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>')
//     h6.appendTo(h3);
//
//     p.append(h1);
//
//     h1.modal('show');
// }
//
// //初始化voice通话
// function init_settle_notify_info(hotelId){
//     $.ajax({
//         type:"POST",
//         url:"/hotel/settle/index",
//         data:{"hotelId":hotelId},
//         success:function(data){
//             var result = data.data;
//             if(data.bstatus.code == 0)
//                 show_settle_notify(result.settleMsgPhone);
//         }
//     });
// }
//
// //显示结算消息提醒
// function show_settle_notify(settleMsgPhone){
//     if(settleMsgPhone==null || settleMsgPhone==undefined || settleMsgPhone==""){
//         //只显示
//         $("#new_voicePhone").show();
//         $("#voicePhone").hide();
//         return ;
//     }else{
//         $("#new_voicePhone").hide();
//         $("#voicePhone").show();
//         $("#voicePhone input").val(settleMsgPhone);
//     }
// }
//
// //添加一个结算手机
// function add_settle_msg_phone(){
//     var phone = $("#newVoicePhone").val()
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//     var id = $("#settle_id").val();
//
//     if ((/^1[35789]\d{9}$/).test(phone) ||(/^0\d{2,3}-\d{7,8}$/).test(phone)) {
//         $.ajax({
//             type:"POST",
//             url:"/hotel/settle/update",
//             data:{"id":id,"hotelId":search_hotel_id,"settleMsgPhone":phone},
//             success:function(result){
//                 if(result.bstatus.code == 0){
//                     init_settle_notify_info(search_hotel_id);
//                     $("#newVoicePhone").val("");
//                 }
//                 if(result.bstatus.code == -1){
//                     //TODO 弹出框
//                     tips($("#modal_warn"),result.bstatus.des);
//                     init_settle_notify_info(search_hotel_id);
//                 }
//             }
//         });
//     }else{
//         tips($("#modal_warn"),"请输入正确的手机号");
//     }
//
// }
//
// //删除结算通知短信
// function deleteSettleMsgPhone(){
//     var id = $("#settle_id").val();
//     var hotel_id = $("#search_hotel_id").prop("value");
//     $.ajax({
//         type:"POST",
//         url:"/hotel/settle/settleMsgPhone/delete",
//         data:{"settleId":id},
//         success:function(data){
//             if(data){
//                 init_settle_notify_info(hotel_id);
//             }
//         }
//     });
// }
//
//
// function showInvoiceRecordTabs(currentEle) {
//     showTabs(currentEle);
//     query_invoice_record_list()
// }
//
// function query_invoice_record_list(){
//     var search_date_start = $("#search_date_start").prop("value");
//     var search_date_end = $("#search_date_end").prop("value");
//     //酒店id
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//     //结算状态
//     $('#invoice_list_t').bootstrapTable('refresh', {
//         query: {offset: 0,
//             search_date_start:search_date_start,
//             search_date_end:search_date_end,
//             search_hotel_id:search_hotel_id
//         }
//     });
// }
//
//
// function show_invoiceRecord_list(table,url,qparam){
//     $(table).bootstrapTable({
//         method: 'get',
//         pagination: true,
//         sidePagination:'server',
//         pageSize: 10,
//         pageList: [5, 10, 25, 50, 100, 200],//list can be specified here
//         url: url,
//         queryParams: qparam,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         class:"table table-hover row",
//         columns: [{
//             field:'invoiceDate',
//             title:'开票时间'
//         },{
//             field: 'invoiceContent',
//             title: '开票内容',
//         }, {
//             field: 'invoiceUnit',
//             title: '开票单位',
//         }, {
//             field: 'invoiceCode',
//             title: '发票代码',
//         }, {
//             field: 'invoiceNo',
//             title: '发票号码',
//         },{
//             field: 'noTaxAmount',
//             title: '不含税金额',
//         }, {
//             field: 'taxAmount',
//             title: '税额',
//         },{
//             field:'createTime',
//             title:'创建时间'
//         },{
//             field:'statusDesc',
//             title:'开票状态'
//         }
//         ]});
// }
//
// //发票记录信息查询
// function invoice_search_wheres(){
//     query_invoice_record_list();
// }
//
// //订单核对信息查询
// function checkOrder_search_wheres(){
//     query_order_list_user();
// }
//
// //导出发票列表
// function exportInvoiceExcel(){
//     var search_date_start = $("#settle_search_date_start").prop("value");
//     //结束日期
//     var search_date_end = $("#settle_search_date_end").prop("value");
//     //酒店id
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//
//     location.href="/hotel/invoice/exportExcel?search_date_start="+search_date_start+"&search_date_end="+search_date_end+"&search_hotel_id="+search_hotel_id;
//
// }