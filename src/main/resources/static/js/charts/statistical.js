// var myCharts = echarts.init($(".static1"));
var chartDom1 = document.getElementById('static1');
var chartDom2 = document.getElementById('static2');
var chartDom3 = document.getElementById('static3');
var chartDom4 = document.getElementById('static4');
var myChart1 = echarts.init(chartDom1);
var myChart2 = echarts.init(chartDom2);
var myChart3 = echarts.init(chartDom3);
var myChart4 = echarts.init(chartDom4);
var option;
option = {
    title: {
        text: '月订单统计图表',
        subtext: '伪造数据'
    },
    legend: {
        data: ['订单数', '间夜数']
    },
    tooltip: {
        trigger: 'axis'
    },
    toolbox: {
        show: true,
        feature: {
            dataView: {show: true, readOnly: false},
            magicType: {show: true, type: ['line', 'bar']},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },
    // dataset: {
    //     source: [
    //         ['product', '2015', '2016', '2017'],
    //         ['Matcha Latte', 43.3, 85.8, 93.7],
    //         ['Milk Tea', 83.1, 73.4, 55.1],
    //         ['Cheese Cocoa', 86.4, 65.2, 82.5],
    //         ['Walnut Brownie', 72.4, 53.9, 39.1]
    //     ]
    // },
    xAxis: [
        {
            type: 'category',
            data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
        }
    ],
    yAxis: [
        {
            type: 'value'
        }
    ],
    series: [
        {
            name: '订单数',
            type: 'bar',
            data: [66, 88, 99, 133, 155, 22, 199, 311, 22, 44, 45, 67],
            markPoint: {
                data: [
                    {type: 'max', name: '最大值'},
                    {type: 'min', name: '最小值'}
                ]
            },
            // markLine: {
            //     data: [
            //         {type: 'average', name: '平均值'}
            //     ]
            // }
        },
        {
            name: '间夜数',
            type: 'bar',
            data: [67, 45, 44, 22, 311, 199, 22, 155, 133, 99, 88, 66],
            markPoint: {
                data: [
                    {type: 'max', name: '年最高'},
                    {type: 'min', name: '年最低'}
                ]
            },
            // markLine: {
            //     data: [
            //         {type: 'average', name: '平均值'}
            //     ]
            // }
        }
    ]
};

myChart1.setOption(option);
myChart2.setOption(option);
myChart3.setOption(option);
myChart4.setOption(option);