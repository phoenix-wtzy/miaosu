$(function(){
    $('#gongyingshang_list').bootstrapTable({
        url: "/jiudian/settle/list_hotel",
        method: 'get',
        pagination: true,
        sidePagination:'client',
        pageNumber: 1,
        pageSize: 10,
        pageList: [5, 10, 20, 50, 100, 'all'],
        columns: [{
            field: 'settle_StartAndEnd',
            title: '结算周期'
        }, {
            field: 'business_name',
            title: '供应商名称'
        }, {
            field: 'settle_money',
            title: '结算金额'
        }, {
            field: 'commission_money',
            title: '技术服务费'
        }, {
            field: 'settle_status',
            title: '结算状态'
        }, {
            field: 'hotelId',
            title: '操作',
            formatter:word_all
        }
        ]
    });
});
function word_all(value, row, index){
    // console.log(value)
    // console.log(row);
    var gysId = g_channel;
    var html = ''
    // var html = '<a class="gys-btns gys-detail-btn" href="javascript:;" onclick="openGysDetail()">详情</a>';

    if(row.settle_status == "未结算"){
        if(row.settle_money > 0) {
            // console.log(111)
            html = '<a class="gys-btns gys-detail-btn" href="javascript:;" onclick="openGysDetail(\''+row.settle_StartAndEnd+'\', '+gysId+')">详情</a>';
            // html += '<a class="gys-btns" href="javascript:;" onclick="fishSettle()">申请结算</a>';
        }else{
            // console.log(222)
            html += '<a class="gys-btns gys-detail-btn gys-btns-disable" href="javascript:;">详情</a>'
            // html += '<a class="gys-btns gys-btns-disable" href="javascript:;">申请结算</a>';
        }
    }else{
        // console.log(333)
        html = '<a class="gys-btns gys-detail-btn" href="javascript:;" onclick="openGysDetail(\''+row.settle_StartAndEnd+'\', '+gysId+')">详情</a>';
        // html += '<a class="gys-btns gys-btns-disable" href="javascript:;">&nbsp;&nbsp;已结算&nbsp;&nbsp;</a>';
    }
    return html
}
function openGysDetail(startAndEnd, gysId) {
    // console.log(startAndEnd,gysId)
    var setdates = encodeURI(startAndEnd);
    var setId = gysId;
    window.location.href = "/settle_gys_detail?setdates="+setdates+"&setId="+setId;
}
function fishSettle(value){
    //更新结算订单并再次进行查询
    var url ='/settle/bill/updateStatus';
    $.ajax({
        type:'get',
        url:url,
        data: jQuery.param({billId : value,status:2}) ,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            if (data.bstatus.code == 0) {
                //重新加载
                query_settleBill_list();
            }
        }
    });

}
function daochu() {
    $('#gongyingshang_list').tableExport({ type: 'excel', escape: 'false', ignoreColumn: [4], fileName: '结算记录' })
}