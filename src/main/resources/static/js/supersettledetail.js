// var urls = location.search;
var urls = decodeURI(decodeURI(location.search));
function urlStr(url) {
    var params = {};
    var str = url.split("?");
    var strs = str[1];
    var arr = strs.split("&");
    for (var i = 0, l = arr.length; i < l; i++){
        var a = arr[i].split("=");
        params[a[0]] = a[1];
    }
    return params
}
$(function () {
    urlStr(urls);
    // console.log(urlStr(urls));
    var temp = urlStr(urls)
    $.ajax({
        type: "GET",
        url: "/jiudian/settle/hotelAndOrderList",
        data: {
            setSettle_StartAndEnd: temp.setdates,
            business_id: temp.setId
        },

        success: function(res){
            // console.log(11111)
            // console.log(res)
            var html = ''
            $.each(res, function (index, value) {
                // console.log(value)
                var datas = [],
                    hotel_name = '',
                    total_money = 0,
                    commission_money = 0;
                if(value.orderDetailList == null) {
                }else{
                    datas = value.orderDetailList
                }
                if(value.hotel_name == null){
                    hotel_name = "酒店名获取失败"
                }else{
                    hotel_name = value.hotel_name
                }
                if(value.settle_money == null){
                    total_money = 0
                }else{
                    total_money = value.settle_money
                }
                if(value.commission_money == null){
                    commission_money = 0
                }else{
                    commission_money = value.commission_money
                }
                // console.log(datas)
                html = '<h4>' + hotel_name + '<span>服务费用: ￥' + commission_money + '</span><span>结算总额: ￥' + total_money + '</span></h4>'
                    +'<table id="detail_list'+index+'"></table>';
                $(".program").append(html)
                $('#detail_list'+index+'').bootstrapTable({
                    data: datas,
                    pagination: true,
                    sidePagination:'client',
                    pageSize: 10,
                    pageList: [5, 10, 20, 50, 100, 'all'],
                    columns: [
                        {
                            field: 'orderNo',
                            title: '订单号'
                        },
                        {
                            field: 'bookTime',
                            title: '预定时间'
                        },
                        {
                            field: 'bookUser',
                            title: '订单人',
                            class: 'bookUser'
                        },
                        {
                            field: 'checkInTime',
                            title: '入住时间'
                        },
                        {
                            field: 'checkOutTime',
                            title: '退房时间'
                        },
                        {
                            field: 'price',
                            title: '订单金额'
                        },
                        {
                            field: 'hotelRoomType',
                            title: '房间类型'
                        },
                        {
                            field: 'roomCount',
                            title: '房间数量'
                        },
                        {
                            field: 'settleStatus',
                            title: '订单状态'
                        },
                        {
                            field: 'bookRemark',
                            title: '备注'
                        }
                    ]
                });
            })
        },
        error: function(err){
            window.confirm("网络错误，请重试")
        }
    })
});