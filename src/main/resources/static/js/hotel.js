$(function () {
    totalHotel()
});

function show_hotel_list(table, url) {
    $(table).bootstrapTable({
        // search:true,
        // showRefresh:true,
        pagination: true,
        sidePagination: 'server',
        pageSize: 10,
        pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
        url: url,
        columns: [{
            field: 'hotelId',
            title: '序号',
            class: 'number-width',
            formatter: formatIndex
        }, {
            field: 'hotelName',
            title: '酒店名称',
            class: 'hotel-name-width',
            formatter: function (value, row, index) {
                return '<p style="width: 200px" class="title-space" title="' + row.hotelName + '">' + row.hotelName + '</p>'
            }
        },
            //     {
            //     field: 'hotel_only_id',
            //     title: '酒店平台ID',
            // },
            {
                field: 'contact',
                title: '联系人'
            }, {
                field: 'contactMobile',
                title: '联系电话'
            }, {
                field: 'userName',
                title: '登录账户'
            }, {
                field: 'hotelId',
                title: '关联供应商',
                formatter: function (value) {
                    return "<a href='/management/associated?" + value + "'>" + "<span class='oi oi-home'>详情</span>" + "</a>";
                }
            },
            //     {
            //     field: 'receptionPhone',
            //     title: '前台电话',
            // }, {
            //     field: 'address',
            //     title: '地址',
            // },
            {
                field: 'hotelId',
                title: '房型设置',
                formatter: room_type_set,
            }, {
                field: 'hotelId',
                title: '价格设置',
                formatter: room_price_set,
            }, {
                field: 'hotelId',
                title: '结算设置',
                formatter: hotel_settle_set,
            }, {
                field: 'hotelId',
                title: '发票设置',
                formatter: hotel_invoice_set,
            }, {
                field: 'isActive',
                title: '状态',
                formatter: formatActive
            },
            //     {
            //     field: 'userNick',
            //     title: '管理员',
            // }, {
            //     field: 'ota_phone',
            //     title: 'OTA管家电话',
            // },{
            //     field: 'userName',
            //     title: '账号',
            // }, {
            //     field: 'userPasswd',
            //     title: '密码',
            // },
            {
                field: 'hotelId',
                title: '操作',
                formatter: room_update_info,
            },
            // {
            // field:'hotelId',
            // title:'渠道相关',
            // formatter:ota_hotel_info,
            // }
        ]
    });
}

// 酒店数量统计
function totalHotel() {
    $.ajax({
        type: 'POST',
        url: '/charts/hotel_all',
        success: function (res) {
            // console.log(res);
            var data = res.data;
            $("#total").html(data.hotel_all_IsActive + data.hotel_all_NotIsActive);
            $("#isActive").html(data.hotel_all_IsActive);
            $("#noActive").html(data.hotel_all_NotIsActive);
        }
    })
}

//酒店上下架状态
function formatActive(value, row, index) {
    if (row.isActive == 1) {
        return '<span style="color: red">在售</span>'
    } else if (row.isActive == 0) {
        return '<span>已下线</span>'
    } else {
        return '待设置'
    }
}

// 格式化序号
function formatIndex(value, row, index) {
    return index + 1;
}

/**
 * 渠道相关的酒店信息
 **/
function ota_hotel_info(value, row, index) {
    return '<button  data-toggle="modal" data-target="#checkHotelModal" class="btn btn-sm btn-outline-primary" onclick="check_qhh_hotel(' + value + ')"><span>审核结果</span></button>';
}


function room_update_info(value, row, index) {
    // row.isActive  1是已上线 0是已下线
    if (row.isActive == 1) {
        return '<button  data-toggle="modal" data-target="#updateHotelModal" class="btn btn-sm btn-outline-primary" onclick="reset_update_hotel(' + value + ')"><span>详情</span></button>&nbsp;'
            // +'<button  class="btn btn-sm btn-outline-primary" style="border: 1px solid gray;color: gray" onclick="setInvalidHotel('+value+',1, this)" disabled="disabled">上架</button>&nbsp;'
            + '<button  class="btn btn-sm btn-outline-primary" onclick="setInvalidHotel(' + value + ',0, this)">下架</button>';
    } else if (row.isActive == 0) {
        return '<button  data-toggle="modal" data-target="#updateHotelModal" class="btn btn-sm btn-outline-primary" onclick="reset_update_hotel(' + value + ')"><span>详情</span></button>&nbsp;'
            + '<button  class="btn btn-sm btn-outline-primary" onclick="setInvalidHotel(' + value + ',1, this)">上架</button>&nbsp;'
        // +'<button  class="btn btn-sm btn-outline-primary" style="border: 1px solid gray;color: gray" onclick="setInvalidHotel('+value+',0, this)" disabled="disabled">下架</button>';
    } else {
        return '<button  data-toggle="modal" data-target="#updateHotelModal" class="btn btn-sm btn-outline-primary" onclick="reset_update_hotel(' + value + ')"><span>详情</span></button>&nbsp;'
            + '<button  class="btn btn-sm btn-outline-primary" onclick="setInvalidHotel(' + value + ',1, this)">上架</button>&nbsp;'
            + '<button  class="btn btn-sm btn-outline-primary" onclick="setInvalidHotel(' + value + ',0, this)">下架</button>';
    }
}

function room_type_set(value, row, index) {
    return "<a href='/room/set/type/" + value + "'>" + "<span class='oi oi-home'>设置</span>" + "</a>";
}

function room_price_set(value, row, index) {
    return "<a href='/room/set/price/" + value + "'>" + "<span class='oi oi-yen'>设置</span>" + "</a>";
}

function hotel_settle_set(value, row, index) {
    return '<a  href="javascript:void(0);" data-toggle="modal" data-target="#updateSettleSettingModal" onclick="reset_update_settle(' + value + ')"><span class="oi oi-yen">设置</span></a>';
}

function hotel_invoice_set(value, row, index) {
    return '<a  href="javascript:void(0);" data-toggle="modal" data-target="#updateInvoiceSettingModal" onclick="reset_update_invoice(' + value + ')"><span class="oi oi-yen">设置</span></a>';
}

function reset_update_invoice(value) {
    clear_invoice_modal();

    $.ajax({
        type: "POST",
        url: "/hotel/invoice/index",
        data: {"hotelId": value},
        success: function (data) {
            var result = data.data;
            console.log(result);
            $("#updateInvoiceSettingTitle").text(result.hotelName + "---发票设置");
            if (data.bstatus.code == 0) {
                //$("#invoice_id").val(result.id);
                $("#invoice_hotelId").val(result.hotelid);
                $("#buyername").val(result.buyername);
                $("#buyertaxpayernum").val(result.buyertaxpayernum);
                $("#buyeraddress").val(result.buyeraddress);
                $("#buyertel").val(result.buyertel);
                $("#buyerbankname").val(result.buyerbankname);
                $("#buyerbankpeople").val(result.buyerbankpeople);
                $("#buyerbankaccount").val(result.buyerbankaccount);
                $("#takeremail").val(result.takeremail);
                $("#cashername").val(result.cashername);
                $("#taxratevalue").val(result.taxratevalue);
                $("#reviewername").val(result.reviewername);
                $("#drawername").val(result.drawername);
            }
        }
    });
}

function clear_invoice_modal() {
    $("#invoice_id").val("");
    $("#invoice_hotelId").val("");
    $("#buyername").val("");
    $("#buyertaxpayernum").val("");
    $("#buyeraddress").val("");
    $("#buyertel").val("");
    $("#buyerbankname").val("");
    $("#buyerbankpeople").val("");
    $("#buyerbankaccount").val("");
    $("#takeremail").val("");
    $("#taxratevalue").val("");
}

//结算设置的页面初始化操作
function reset_update_settle(value) {
    clear_settle_modal();
    $.ajax({
        type: "POST",
        url: "/hotel/settle/index",
        data: {"hotelId": value},
        success: function (data) {
            var result = data.data;
            console.log(result);
            $("#updateSettleSettingTitle").text(result.hotelName + "---结算设置");
            if (data.bstatus.code == 0) {
                //标题
                $("#commissionRate").val(result.commissionRate);
                //佣金比例
                $("#commissionRate").val(result.commissionRate);
                //结算周期
                var cycleType = result.settleCycleSetting;
                $("#settleCycleSetting option").each(function () {
                    //var value = item.val();
                    var value = $(this).val();
                    if (value == cycleType) {
                        $(this).attr("selected", true);
                    }
                });
                var bankUseType = result.bankUseType;
                //银行卡账户用途类型
                $("#bankUseTypes input").each(function () {
                    //var value = item.val();
                    var value = $(this).val();
                    if (value == bankUseType) {
                        $(this).attr("checked", true);
                    }
                });

                $("#bankAccountType").val(result.bankAccountType);
                $("#bankAccountName").val(result.bankAccountName);
                $("#bankAccountCardnumber").val(result.bankAccountCardnumber);
                $("#bankSignPhone").val(result.bankSignPhone);
                $("#settle_hotelId").val(result.hotelId);
                //$("#settle_id").val(result.id);
            }

        }
    });
}

//清楚模态框中的脏数据
function clear_settle_modal() {
    $("#commissionRate").val("");
    $("#settle_error_info").text("");

    $("#settleCycleSetting").empty();

    $("#settleCycleSetting").append(
        // "<option value =\"0\">结算周期</option>\n" +
        // "<option value =\"1\">按天结算</option>\n" +
        "<option value =\"2\">按周结算</option>\n" +
        // "<option value =\"3\">半月结算</option>\n" +
        "<option value =\"4\">按月结算</option>");

    $("#bankAccountType").val("");

    $("#bankAccountName").val("");

    $("#bankAccountCardnumber").val("");

    $("#bankSignPhone").val("");

    $("#settle_id").val("")
    $("#settle_hotelId").val("");

}

//设置酒店上下架操作
function setInvalidHotel(value, active, _this) {
    $.ajax({
        type: "POST",
        url: "/admin/hotel/active",
        data: {"hotelId": value, "type": active},
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0) {
                if (active == 1) {
                    layer.msg('酒店上架成功！');
                    totalHotel();
                } else if (active == 0) {
                    layer.msg('酒店下架成功！');
                    totalHotel();
                } else {
                    layer.msg('错误，请重试！')
                }
                reload_hotel();
                // $(_this).attr('disabled', 'true')
            } else {
                if (active == 1) {
                    layer.msg('酒店上架失败！');
                } else if (active == 0) {
                    layer.msg('酒店下架失败！');
                } else {
                    layer.msg('错误，请重试！')
                }
                reload_hotel();
                // $(_this).removeAttr('disabled')
            }
        }
    });
}

function reset_update_hotel(value) {
    $("#f_update_hotel input[name='settle_totalMoney']").val('');
    $("#f_update_hotel input[name='settle_totalNight']").val('');
    $("#f_update_hotel input[name='baoFang_totalMoney']").val('');
    $("#f_update_hotel input[name='touZiRen_rates']").val('');
    $.ajax({
        type: "POST",
        url: "/admin/hotel/info",
        data: {"hotelId": value},
        success: function (data) {
            // console.log(data);
            $("#hotelName").val(data.hotelName);
            $("#hotel_only_id").val(data.hotel_only_id);
            $("#address").val(data.address);
            $("#contact").val(data.contact);
            $("#contactMobile").val(data.contactMobile);
            $("#accountName").val(data.userName);
            $("#accountPassword").val(data.userPasswd);
            $("#accountNickName").val(data.userNick);
            $("#ota_phone").val(data.ota_phone);
            $("#userId").val(data.userId);
            $("#hotelId").val(data.hotelId);
            $("#receptionPhone").val(data.receptionPhone);
            $("#f_update_hotel .custom-select").val(data.hotelType);
            if(data.hotelType == 1){
                $("#f_update_hotel .typeA").show();
                $("#f_update_hotel .typeB").hide();
                $("#f_update_hotel input[name='settle_totalMoney']").val(data.settle_totalMoney);
                $("#f_update_hotel input[name='settle_totalNight']").val(data.settle_totalNight);
            }else if(data.hotelType == 2){
                $("#f_update_hotel .typeA").hide();
                $("#f_update_hotel .typeB").show();
                $("#f_update_hotel input[name='baoFang_totalMoney']").val(data.baoFang_totalMoney);
                $("#f_update_hotel input[name='touZiRen_rates']").val(data.touZiRen_rates);
            }


            if (data.province != null) {
                $("#provinceUp").val(data.province.code).trigger("change")
                getCity($("#provinceUp"), data.city.code, data.district.code)
            } else {
                $("#provinceUp").val(0).trigger("change")
                getCity($("#provinceUp"))
            }

        }

    });
}

/**
 *设置酒店的有效性
 */
function set_active_hotel(hotelId, index) {
    //var text = $("#hotel_active").text();
    var type;
    //if(text=="有效"){
    //type = 1
    //$("#hotel_active_"+index).text("无效");
    //}
    //if(text=="无效"){
    //type = 0
    //$("#hotel_active_"+index).text("有效");
    //}

    $.ajax({
        type: "POST",
        url: "/admin/hotel/active",
        data: jQuery.param({hotelId: hotelId, type: 1}),
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0) {
                //数据库添加有效性字段

            }
        }
    });
}

/**
 *获取去呼呼渠道相关信息
 **/
function check_qhh_hotel(hotelId) {
    check_hotel(hotelId, "/admin/hotel/check", "去呼呼");
}

/**
 *设置酒店的有效性
 */
function check_hotel(hotelId, url, channelName) {
    $("#ModalCheckHotelTitle").text("");
    $("#checkHotelName").val("");
    $("#hotelcheckMessage").val("");
    $.ajax({
        type: "POST",
        url: url,
        data: jQuery.param({hotelId: hotelId}),
        success: function (data) {
            console.log(data);
            $("#ModalCheckHotelTitle").text("酒店审核结果（" + channelName + "）");
            $("#checkHotelName").val(data.data.hotelName);
            if (data.bstatus.code == 0) {
                //数据库添加有效性字段
                $("#hotelcheckMessage").val(data.data.data.checkMessage);
            } else {
                $("#hotelcheckMessage").val(data.bstatus.des);
            }

        }
    });
}

/**
 *修改酒店信息
 */
function update_hotel() {
    var frm = $('#f_update_hotel');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            if (data.bstatus.code == 0) {
                $("#update_hotel_error_info").text("修改成功");
                $("#updateHotelModal").modal('hide');
                reload_hotel();
            } else {
                $("#update_hotel_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            $("#update_hotel_error_info").text("系统错误");
        },

    });
}


/**
 *发票设置信息
 */
function invoice_setting() {
    var frm = $('#f_update_invoice');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data, textStatus, xhr) {
            // console.log(data,xhr.status);
            if (data.bstatus.code == 0) {
                $("#invoice_error_info").text("添加成功！");
                $('#updateInvoiceSettingModal').modal('hide')
            } else {
                $("#invoice_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            console.log(data);
            $("#invoice_error_info").text("系统错误");
        },
    });
}

/**
 *设置结算信息
 */
function settle_setting() {
    var frm = $('#f_update_settle');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data, textStatus, xhr) {
            // console.log(data,xhr.status);
            if (data.bstatus.code == 0) {
                $("#settle_error_info").text("添加成功！");
                $('#updateSettleSettingModal').modal('hide')
            } else {
                $("#settle_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            console.log(data);
            $("#settle_error_info").text("系统错误");
        }
    });
}

/**
 *添加酒店
 */
$(function () {
    $(".typeA").show();
    $(".typeB").hide();
    $(".custom-select").on("change", function () {
        var _val = $(this).val();
        if (_val == 1) {
            $(".typeA").show();
            $(".typeB").hide();
        } else if (_val == 2) {
            $(".typeA").hide();
            $(".typeB").show();
        }
    })
});

function add_hotel() {
    var frm = $('#f_add_hotel');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            // console.log(data);
            if (data.bstatus.code == 0) {
                // console.log('Submission was successful.');
                $("#add_hotel_error_info").text("添加成功！");
                $('#addHotelModal').modal('hide')
//                    $('#hotel_list').bootstrapTable({refresh:{silent: true,url:"/admin/hotel/list"}});
//                    $('#hotel_list').bootstrapTable('append', randomData());
                reload_hotel();
            } else {
                // console.log('Submission was Failed: '+ data.bstatus.des);
                $("#add_hotel_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            // console.log('An error occurred.');
            // console.log(data);
            $("#add_hotel_error_info").text("系统错误");
        }
    });
}

function reload_hotel() {
    // $.ajax({
    //         type: 'get',
    //         url: "/admin/hotel/list",
    //         success: function (data) {
    //             console.log(data);
    //             $('#hotel_list').bootstrapTable('load',data) ;
    //         }}
    // )

    $('#hotel_list').bootstrapTable('refresh', {
        query: {offset: 0}
    });
}

/**
 * 清空模态窗口里的输入值
 */
function reset_add_hotel() {
    $("#add_hotel_error_info").text("");
    var frm = $('#f_add_hotel');
    var inputs = frm.find("input");
    inputs.each(function (e) {
        // console.log($(this))
        if ($(this).attr('type') == 'checkbox') {
            return true;
        }
        $(this).val('')
    })

}

function getCity(e, b, c) {
    if ($(e).val() == 0) {
        $(e).next().html('');
        var str = "<option value='0'>--市--</option>";
        $(e).next().append(str);
        return false;
    }
    $(e).next().html('');
    var str = "<option value='0'>--市--</option>";
    $.ajax({
        type: "POST",
        url: "/address/cities",
        data: {"provinceCode": $(e).val()},
        success: function (data) {
            //console.log(data);
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
                str += "<option value=" + data[i].code + ">" + data[i].name + "</option>"
            }
            $(e).next().append(str);
            if (b) {
                $(e).next().val(b).trigger("change")
                getDistrict($("#cityUp"), c)
            }
            $(e).next().next().html('<option>-区-</option>');
        }
    });
}

function getDistrict(e, c) {
    // console.log(e)
    // console.log(c)
    if ($(e).val() == 0) {
        $(e).next().html('');
        var str = "<option value='0'>--区--</option>";
        $(e).next().append(str);
        return false;
    }
    $(e).next().html('');
    var str = "<option value='0'>--区--</option>";
    $.ajax({
        type: "POST",
        url: "/address/districts",
        data: {"cityCode": $(e).val()},
        success: function (data) {
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
                str += "<option value=" + data[i].code + ">" + data[i].name + "</option>"
            }
            $(e).next().append(str);
            if (c) {
                $(e).next().val(c)
            }
        }
    });
}


function provinceSibling(e) {
    $("#provinceUp").html('');
    var str = "";
    $.ajax({
        type: "POST",
        url: "/address/provinces",
        success: function (data) {
            //console.log(data);
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
                str += "<option value=" + data[i].code + ">" + data[i].name + "</option>"
            }
            $(e).html(str);
        }
    });
}


function citySibling(e) {
    var cityCode = $("#selectCityCode").value;
    if (cityCode == null) {
        return null;
    }
    var str = "";
    $.ajax({
        type: "POST",
        url: "/address/citiesSibling",
        data: {"cityCode": cityCode},
        success: function (data) {
            //console.log(data);
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
                str += "<option value=" + data[i].code + ">" + data[i].name + "</option>"
            }
            $(e).html(str);
        }
    });
}

function districtSibling(e) {
    var districtCode = $("#selectdistrictCode").value;
    if (districtCode == null) {
        return null;
    }
    var str = "";
    $.ajax({
        type: "POST",
        url: "/address/districtsSibling",
        data: {"districtCode": districtCode},
        success: function (data) {
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
                str += "<option value=" + data[i].code + ">" + data[i].name + "</option>"
            }
            $(e).html(str);
        }
    });

}

$(function () {
    $('#updateHotelModal').on('hide.bs.modal', function () {
        // 执行一些动作...
        $("#provinceUp").val(0).trigger("change")
        $("#districtUp").val(0)
    })
})