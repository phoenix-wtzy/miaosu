// 详情查看按钮点击事件----打开弹层
function openFrame (settleDate,user_id) {
    $(".hotel-detail").show()
    $("body").css({"overflow": "hidden" });
    frameAjax(settleDate, user_id);
}
// 详情查看弹层关闭事件
function closeFrame () {
    $(".hotel-detail").hide()
    $("body").css({"overflow": "auto"});
    $(".table-body tbody tr").remove()
}

// 弹层打开后数据请求
function frameAjax (settleDate, user_id) {
    var html = ''
    $.ajax({
        type: "GET",
        url: "/jiudian/settle/orderList",
        // /jiudian/settle/orderList   酒店接口
        // /jiudian/settle/list_gongyingshang   供应商接口
        // /jiudian/settle/list_super   管理员接口
        data: {
            setSettle_StartAndEnd: settleDate,
            user_id: user_id
        },
        success: function (res) {
            // console.log(res)
            $.each(res, function (index, value) {
                html += '<tr>'
                    + '<td>' + value.orderNo + '</td>'
                    + '<td>' + value.bookTime + '</td>'
                    + '<td>' + value.bookUser + '</td>'
                    + '<td>' + value.hotelRoomType + '</td>'
                    + '<td>' + value.roomCount + '</td>'
                    + '<td>' + value.checkInTime + '</td>'
                    + '<td>' + value.checkOutTime + '</td>'
                    + '<td>' + value.price + '</td>'
                    + '<td>' + value.bookRemark + '</td>'
                    + '<td>' + value.settleStatus + '</td>'
                    + '</tr>'
            })
            $(".table-body tbody").append(html)
        },
        error: function (res) {
            window.confirm('查询失败，请稍后重试')
        }
    })
}
function hotelParams (params) {
    params.hotelId = g_channel
}
function show_list(table,url,qparam) {
    $(table).bootstrapTable({
        url: url,
        method: 'get',
        pagination: true,
        sidePagination:'client',  //client前端 server服务端
        pageNumber:1,
        pageSize: 10,
        pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
        search: false,
        queryParams: qparam,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        class:"table table-hover row",
        columns: [{
            field: 'settle_StartAndEnd',
            title: '结算周期',
        }, {
            field: 'settle_money',
            title: '结算金额',
        }, {
            field: 'commission_money',
            title: '服务费用',
        }, {
            field: 'settle_status',
            title: '结算状态',
        }, {
            field: 'settle-detail',
            title: '详情',
            formatter:to_detail
        }
        ]});
}
function to_detail(value, row, index) {
    if (row.settle_money == 0) {
        return '';
    } else {
        var paramsId = g_channel;
        // console.log(paramsDate, paramsId)
        return '<a class="to-detail" href="javascript:;" onclick="openFrame(\''+row.settle_StartAndEnd+'\','+paramsId+')">查看</a>'
    }
}

// 数据加载
$(function () {
    // 酒店结算表格初始化
    show_list($("#settle-hotel-list"),'/jiudian/settle/list',hotelParams);
    // 弹出层右上角按钮关闭事件
    $(".close-detail, .hotel-detail-ok").on("click", function () {
        closeFrame()
    })

});
function daochu() {
    $('#settle-hotel-list').tableExport({ type: 'excel', escape: 'false', ignoreColumn: [4], fileName: '结算记录' })
}