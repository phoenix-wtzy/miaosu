// var urls = location.search;
var urls = decodeURI(decodeURI(location.search));
function urlStr(url) {
    var params = {};
    var str = url.split("?");
    var strs = str[1];
    var arr = strs.split("&");
    for (var i = 0, l = arr.length; i < l; i++){
        var a = arr[i].split("=");
        params[a[0]] = a[1];
    }
    return params
}
function show_list(table,url,qparam) {
    $(table).bootstrapTable({
        url: url,
        method: 'get',
        pagination: true,
        sidePagination:'client',  //client前端 server服务端
        pageNumber:1,
        pageSize: 10,
        pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
        search: false,
        queryParams: {
            business_id: qparam
        },
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        class:"table table-hover row",
        columns: [{
            field: 'settle_StartAndEnd',
            title: '结算周期',
        }, {
            field: 'settle_money',
            title: '结算金额',
        }, {
            field: 'commission_money',
            title: '服务费用',
        }, {
            field: 'settle_status',
            title: '结算状态',
        }, {
            field: 'settle-detail',
            title: '详情',
            formatter:to_detail
        }
        ]});
}
function to_detail(value, row, index) {
    // console.log(row)
    var dates = row.settle_StartAndEnd
    var id = row.business_id
    return '<a class="to-detail" href="javascript:;" onclick="openDetail(\''+dates+'\', \''+id+'\')">查看</a>'
}
function openDetail(dates, ids) {
    console.log(dates)
    console.log(ids)
    window.location.href = "/settle_super_gysdetail?setdates="+encodeURI(dates)+"&setId="+ids;

}
$(function(){
    urlStr(urls);
    // console.log(urlStr(urls));
    var temp = urlStr(urls)
    // console.log(temp)
    show_list($("#settle-hotel-list"),'/jiudian/settle/list_superOfdetail',temp.business_id);
});
function daochu() {
    $('#settle-hotel-list').tableExport({ type: 'excel', ignoreColumn: [4], escape: 'false', fileName: '结算记录' })
}
