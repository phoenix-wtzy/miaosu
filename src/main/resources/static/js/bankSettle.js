

//隐藏其他tab 显示点击的tab页面
function showTabs(currentEle){
    //隐藏所有
    $.each($("a[name=query_nav]"),function (index,item) {
        var hrefName = $(this).attr("href");
        $(hrefName).css('display','none');
    });

    var show = $("#"+currentEle).attr("href");
    $(show).css('display','block');
}



/**
 * 银行信息的结算管理
 */
function showSettleManage(currentEle){
    showTabs(currentEle);
    //查询手机信息并进行
    //查询定订单列表
    var search_hotel_id = $("#search_hotel_id").prop("value");
    queryBankInfo(search_hotel_id);
}

/**
 * 查询当前酒店的银行卡信息
 * @param hotelId 酒店id
 */
function queryBankInfo(hotelId){
    $.ajax({
        type:"POST",
        url:"/hotel/settle/index",
        data:{"hotelId":hotelId},
        success:function(data){
            var result = data.data;
            if(data.bstatus.code == 0)
                show_bank_info(result);
        }
    });

}

//回显数据
function show_bank_info(result){
   var id = result.id;
   var bankUseType = result.bankUseType;
   var bankAccountType = result.bankAccountType;
   var bankAccountName = result.bankAccountName;
   var bankAccountCardnumber = result.bankAccountCardnumber;
   var bankSignPhone = result.bankSignPhone;
   var settleMsgPhone = result.settleMsgPhone

    $("#bankUseTypes input").each(function() {
        //var value = item.val();
        var value = $(this).val();
        if (value == bankUseType) {
            $(this).prop("checked", "checked");
        }
    });

    $("#settle_id").val(id);

    $("#bankAccountType").val(bankAccountType);

    $("#bankAccountName").val(bankAccountName);

    $("#bankAccountCardnumber").val(bankAccountCardnumber);

    $("#bankSignPhone").val(bankSignPhone);

    if(settleMsgPhone != null && settleMsgPhone != undefined && settleMsgPhone.trim()!=""){
        $("#settle_msg_phone").val(settleMsgPhone);
    }
    disableForm("disableEle");
}

//显示隐藏的字段
function show_hiden_field(){
    $("#signPhone").css("display", "block");

    $("#reset_bankInfo").on("click",reset_bankInfo);
}
function  hiden_field() {
    $("#resetBank").css("display","block");
    $("#comfirmBank").css("display","none");

    $("#signPhone").css("display", "none");

}

//禁用属性
function disableForm(className){
    $("."+className).attr("disabled","disabled");
}
//开启属性
function unDisableForm(className){
    $("."+className).removeAttr("disabled");
}

//获取验证码信息
function getIdenCode(){
    //获取手机号信息
    var signPhone = $("#bankSignPhone").val();
    if(signPhone == null || signPhone == undefined || signPhone.trim()==""){
        //消息提醒 modal
        tips($("#modal_warn"),"签约手机号为空，请联系管理员进行设置");
    }
    $.ajax({
        type:"POST",
        url:"/hotel/settle/updateBank/indentidyCode",
        data:{"bankSignPhone":signPhone},
        success:function(data){
            if(data.bstatus.code==0){
                var code = data.data;
                $("#smsCode").val(code);
            }
        }
    });
}


//重新填写银行信息
function reset_bankInfo() {
    //先判断用户输入的验证码和生成的验证码
    var smsCode = $("#smsCode").val();
    var smsCode_user = $("#SMSVerification").val();
    if(smsCode == null || smsCode == undefined || smsCode.trim() == ""){
        //||smsCode_user == null || smsCode_user == undefined || smsCode_user.trim() == ""
        tips($("#modal_warn"),"验证码不能为空");
        return ;
    }
    if(smsCode != smsCode_user){
        tips($("#modal_warn"),"输入验证码错误，请重新输入");
        return;
    }
    //将所有的银行卡相关的信息禁用状态转换为可用状态
    unDisableForm("disableEle");
    //隐藏 该按钮，显示别的按钮
    $("#smsCode").val("");
    $("#resetBank").css("display","none");
    $("#comfirmBank").css("display","block");
    return ;
}

//更新银行卡信息
function update_bankInfo(){
    var bankUseType = null;
    var id = $("#settle_id").val();
    var bankAccountType = $("#bankAccountType").val();
    var bankAccountName = $("#bankAccountName").val();
    var bankAccountCardnumber = $("#bankAccountCardnumber").val();
    var bankSignPhone = $("#bankSignPhone").val();
    var search_hotel_id = $("#search_hotel_id").prop("value");

    $("#bankUseTypes input").each(function() {
        var checked = $(this).prop("checked");
        if (checked) {
            bankUseType = $(this).val();
        }
    });

    $.ajax({
        type:"POST",
        url:"/hotel/settle/update",
        data:{"id":id,"hotelId":search_hotel_id,"bankUseType":bankUseType,"bankAccountType":bankAccountType,"bankAccountName":bankAccountName,
            "bankAccountCardnumber":bankAccountCardnumber,"bankSignPhone":bankSignPhone},
        success:function(data){
            showSettleManage("query_nav_4");
            hiden_field();
            return ;
        }
    });
}

//取消酒店信息
function cancel_bankInfo(){
    //重新加载
    var search_hotel_id = $("#search_hotel_id").prop("value");
    queryBankInfo(search_hotel_id);
    //隐藏 该按钮，显示别的按钮
    $("#resetBank").css("display","block");
    $("#comfirmBank").css("display","none");
    //禁用
    disableForm("disableEle");
    return ;
}


/**
 * 初始化消息提醒的相关信息
 * @param hotelId 当前酒店id

function init_msg_notify_info(hotelId){
    $.ajax({
        type:"POST",
        url:"/message/msgInit",
        data:{"hotelId":hotelId},
        success:function(data){
            show_sms_notify(data.msgNotifyPhone);
        }
    });
}
 */
//鼠标移入
function mouseOver(obj){
    //var th = $(obj);
    $(obj).children("a.delete").css("display", "block");
}
//鼠标移出
function mouseOut(obj){
    //var th = $(obj);
    $(obj).children("a.delete").css("display", "none");
}

//弹窗提示
function tips(erea,info) {
    var p = $(erea).empty();

    h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
    h2 = $('<div class="modal-dialog modal-sm">');
    h2.appendTo(h1);
    h3 = $('<div class="modal-content">');
    h3.appendTo(h2);
    h4=$('<div class="modal-header">');
    h4.appendTo(h3);
    h4.append('<h6 class="modal-title">提示：</h6>');
    h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
    h5 = $('<div class="modal-body">');
    h5.appendTo(h3);
    h5.html('<p class="text-danger">'+info+'</p>');
    h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>')
    h6.appendTo(h3);

    p.append(h1);

    h1.modal('show');
}

//初始化voice通话
function init_settle_notify_info(hotelId){
    $.ajax({
        type:"POST",
        url:"/hotel/settle/index",
        data:{"hotelId":hotelId},
        success:function(data){
            var result = data.data;
            if(data.bstatus.code == 0)
                show_settle_notify(result.settleMsgPhone);
        }
    });
}

//显示结算消息提醒
function show_settle_notify(settleMsgPhone){
    if(settleMsgPhone==null || settleMsgPhone==undefined || settleMsgPhone==""){
        //只显示
        $("#new_voicePhone").show();
        $("#voicePhone").hide();
        return ;
    }else{
        $("#new_voicePhone").hide();
        $("#voicePhone").show();
        $("#voicePhone input").val(settleMsgPhone);
    }
}

//添加一个结算手机
function add_settle_msg_phone(){
    var phone = $("#newVoicePhone").val()
    var search_hotel_id = $("#search_hotel_id").prop("value");
    var id = $("#settle_id").val();

    if ((/^1[35789]\d{9}$/).test(phone) ||(/^0\d{2,3}\d{7,8}$/).test(phone)) {
        $.ajax({
            type:"POST",
            url:"/hotel/settle/update",
            data:{"id":id,"hotelId":search_hotel_id,"settleMsgPhone":phone},
            success:function(result){
                if(result.bstatus.code == 0){
                    init_settle_notify_info(search_hotel_id);
                    $("#newVoicePhone").val("");
                }
                if(result.bstatus.code == -1){
                    //TODO 弹出框
                    tips($("#modal_warn"),result.bstatus.des);
                    init_settle_notify_info(search_hotel_id);
                }
            }
        });
    }else{
        tips($("#modal_warn"),"请输入正确的手机号");
    }

}

//删除结算通知短信
function deleteSettleMsgPhone(){
    var id = $("#settle_id").val();
    var hotel_id = $("#search_hotel_id").prop("value");
    $.ajax({
        type:"POST",
        url:"/hotel/settle/settleMsgPhone/delete",
        data:{"settleId":id},
        success:function(data){
            if(data){
                init_settle_notify_info(hotel_id);
            }
        }
    });
}