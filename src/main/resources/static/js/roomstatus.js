


function get_pre_day_data(){
    var pre_day = $("#v_pre_day").attr("value");
    $.ajax({
        type:'get',
        url:'/room/status/dates/pre',
        data: jQuery.param({date_end:pre_day}) ,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            var dates = data.data;
            on_date_change(dates);
        }
    });
}

function get_next_day_data(date) {
    var end_day = $("#v_end_day").attr("value");
    if (date){
        end_day = date;
    }
    $.ajax({
        type:'get',
        url:'/room/status/dates/next',
        data: jQuery.param({date_start:end_day}) ,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            var dates = data.data;
            on_date_change(dates);
        }
    });
}

function on_date_change(dates) {
    var date_start = dates[0];
    var date_end = dates[dates.length-1];
    $("#v_pre_day").attr("value",date_start);
    $("#v_pre_day").text(format_date_md(date_start));
    $("#v_end_day").attr("value",date_end);
    $("#v_end_day").text(format_date_md(date_end));
    var i = 0;
    $("#header_op").find("th.date_header").each(function () {
        var cur_date = moment(dates[i++]);
        // var day_of_week =
        $(this).html('<span>'+cur_date.format("MM-DD")+'</span><small class="ml-1 text-secondary">'+cur_date.format("dd")+'<small/>');

    })

    var url ='/room/status/list';
    $.ajax({
        type:'get',
        url:url,
        data: jQuery.param({date_start: date_start,date_end:date_end}) ,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            console.log(data);
            var statusData=data.data;
            console.log(statusData.length);
            for(var i=0;i<statusData.length;i++){
                var hotelRoomCounts = statusData[i].hotelRoomCounts;
                var hotelRoomSetVO=statusData[i].hotelRoomSetVO;
                var hotelStatusVOs = statusData[i].hotelStatusVOs;
                var d=0;
                $("#tr_room_num_"+hotelRoomSetVO.hotelRoomTypeId).find("td.cell_room_num").each(function () {
                    var sell_date = dates[d++];
                    var hotelRoomTypeId = hotelRoomSetVO.hotelRoomTypeId;
                    var num = get_room_num(sell_date,hotelRoomCounts,hotelRoomTypeId);
                    var e_cell = $(this);
                    e_cell.off();
                    if(num == '-'){
                        var h1 = '<div class="center-block text-center">--</div>'
                        e_cell.html(h1);
                    }else{
                        var h2 = '<div class="center-block text-center text-primary">'+num+'</div>'
                        e_cell.html(h2);
                    }
                });
               var index=0;
               for(var j=0;j<hotelStatusVOs.length;j++){
                     var sd=0;
                     var m1=0;
                     $("#room_"+hotelRoomSetVO.hotelRoomTypeId+"_type_"+index++).find("td.cell_room_status").each(function () {
                            var sell_date = dates[sd++];
                            var hotelRoomTypeId = hotelRoomSetVO.hotelRoomTypeId;
                            var room_status=hotelStatusVOs[j].hotelRoomStatusList;
                            var status = get_room_status(room_status,hotelRoomTypeId,sell_date);
                            var productIds=hotelStatusVOs[j].hotelProductVO.productId;
                            var e_cell = $(this);
                            e_cell.off();
                                   if(status == '-'){
                                       var h1 = '<div class="center-block text-center">--</div>'
                                       e_cell.html(h1);
                                   }else if(status == 1){
                                       var h2 = '<div class="center-block text-center text-primary">可售</div>'
                                       e_cell.html(h2);

                                       e_cell.on('mouseenter',function () {
                                           e_cell.addClass("bg-dark-blue")
                                           var h2 = $('<div class="center-block text-center"></div>');
                                           var h3 = $('<button class="btn btn-primary btn-sm"><small>关房</small></button>');
                                           h2.append(h3);
                                           h3.on('click',{productIds:productIds,setStartTime:sell_date,setEndTime:sell_date,roomStatus:0},function (e) {
                                              // console.log(1231232131231)
                                               $.ajax({
                                                   type:'get',
                                                   url:'/room/status/update',
                                                   data: jQuery.param(e.data) ,
                                                   contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                                                   success: function (data) {
                                                       console.log(data);
                                                       refresh_room_status();
                                                   }
                                               });
                                               window.location.reload();
                                           })
                                           e_cell.empty().append(h2);
                                       }).on('mouseleave',function () {
                                           e_cell.html(h2)
                                           e_cell.removeClass("bg-dark-blue")
                                       })

                                   }
                                   else {
                                       var h2 = '<div class="center-block text-center text-muted">已关房</div>'
                                       e_cell.html(h2);

                                       e_cell.on('mouseenter',function () {
                                           e_cell.addClass("bg-dark-blue")
                                           var h2 = $('<div class="center-block text-center"></div>');
                                           var h3 = $('<button class="btn btn-primary btn-sm"><small>开房</small></button>')
                                           h3.on('click',{productIds:productIds,setStartTime:sell_date,setEndTime:sell_date,roomStatus:1},function (e) {
                                               $.ajax({
                                                   type:'get',
                                                   url:'/room/status/update',
                                                   data: jQuery.param(e.data) ,
                                                   contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                                                   success: function (data) {
                                                       console.log(data);
                                                       refresh_room_status();
                                                   }
                                               });
                                               window.location.reload();
                                           })
                                           h2.append(h3);
                                           e_cell.empty().append(h2);
                                       }).on('mouseleave',function () {
                                           e_cell.html(h2)
                                           e_cell.removeClass("bg-dark-blue")
                                       })

                                   }



                               });
               }
            }




            }

    });

}
function get_room_num(num_date,hotelRoomCounts,hotelRoomTypeId){
      for(var room_nums_idx in hotelRoomCounts){
         if(hotelRoomCounts[room_nums_idx].hotelRoomTypeId==hotelRoomTypeId){
            if(num_date==format_date(hotelRoomCounts[room_nums_idx].sellDate)){
                return hotelRoomCounts[room_nums_idx].countRoom;
            }
          }
      }
      return "-";
}
// function showRoomType(e){
//     if($(e).parent().parent().parent().next().css('display')=='none'){
//         $(e).find('i').removeClass("triangleDown");
//          $(e).find('i').addClass("triangleUp");
//         $(e).parent().parent().parent().next().show();
//
//     }else{
//          $(e).parent().parent().parent().next().hide();
//          $(e).find('i').removeClass("triangleUp");
//          $(e).find('i').addClass("triangleDown");
//
//     }
// }

function get_room_status(room_status,hotel_room_type_id,date) {
    for (var room_status_idx in room_status){
        var status = room_status[room_status_idx];
        var sell_date = format_date(status.sellDate);
//        if (status.hotelRoomTypeId == hotel_room_type_id){
            if (sell_date==date){
                return status.status;
            }
//        }
    }
    return '-';
}
function update_room_nums(){
    var frm = $('#f_update_room_nums');
    var info_area = $("#add_hotelNum_error_info");
    $.ajax({
        type:frm.attr('method'),
        url:frm.attr('action'),
        data:frm.serialize(),
        success:function(data){
           if(data.bstatus.code==0){
                info_area.text("批量修改成功");
                $('#updateRoomNumModal').modal('hide');
                 refresh_room_status();
           }else{
                 info_area.text(data.bstatus.des);
           }
        },
        error: function (data) {
              console.log(data);
              info_area.text("系统错误");
        },
    });
}

function update_room_status() {
    var frm = $('#f_update_room_status');
    var info_area = $("#add_hotel_error_info");
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data,textStatus,xhr) {
            if (data.bstatus.code == 0){
                info_area.text("添加成功！");
                $('#updateRoomStatusModal').modal('hide')
                refresh_room_status();
            }else {
                info_area.text(data.bstatus.des);
            }
        },
        error: function (data) {
            console.log(data);
            info_area.text("系统错误");
        },
    });
}

function bind_datepicker() {
    $('#setStartTime').datepicker({
        uiLibrary: 'bootstrap4',
        language:"zh-cn",
        format: 'yyyy-mm-dd',
    });
    $('#setEndTime').datepicker({
        uiLibrary: 'bootstrap4',
        language:"zh-cn",
        format: 'yyyy-mm-dd',
    });
}

function init_room_status() {
    get_next_day_data(g_yesterday);
}

function refresh_room_status() {
    var pre_day = $("#v_pre_day").attr("value");
    var day = moment(pre_day,"YYYY-MM-DD").subtract(1, 'days').format("YYYY-MM-DD");
    get_next_day_data(day);
}