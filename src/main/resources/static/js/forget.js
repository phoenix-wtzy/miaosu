/**
 * 表单提交时验证
 * @returns {boolean}
 */
function forget() {

    var Form = document.getElementById("forgetId");
    var bool = true;
    if (!InputUsernameBlur()) bool = false;
    if (!InputPhoneCodeBlur()) bool = false;
    if (!InputPasswordBlur()) bool = false;
    if (!InputRepasswordBlur()) bool = false;
    if (bool==true) {
        $.ajax({
            type: 'POST',
            url:'/forget/reset',
            data:$(Form).serialize(),
            success:function(data){
                if(data.bstatus.code==0){
                    alert(data.bstatus.des);
                    //返回上次浏览的页面,也就是登录页面
                    window.history.back(-1);
                }else{
                    alert(data.bstatus.des);
                }
            }
        });
    }

}



/**
 * 手机号(账号)输入框失去焦点
 */
function InputUsernameBlur() {
    var uname = document.getElementById("Username");
    var ename = document.getElementById("errorUsername");

    /* 手机号(账号)为空/不为空 且校验手机号规则*/
    if (uname.value=="" ||!(/^1[3456789]\d{9}$/).test($("#Username").val())) {
        $('#errorUsername').html('请输入正确的手机号(账号)。').css('color', 'red');
        return false;
    }
    else {
        ename.innerHTML="";
    }
    return true;
}


/**
 * 验证码输入框失去焦点
 */
function InputPhoneCodeBlur() {
    var code = document.getElementById("PhoneCode");
    var ecode = document.getElementById("errorPhoneCode");
    /* 邀请码不为空 */
    if (code.value=="") {
        $('#errorPhoneCode').html('验证码不能为空。').css('color', 'red');
        return false;
    }
    else {
        ecode.innerHTML="";
    }
    return true;
}


/**
 * 密码输入框失去焦点
 */
function InputPasswordBlur() {
    var pwd = document.getElementById("Password");
    var epwd = document.getElementById("errorPassword");
    /* 密码为空/不为空 */
    if (pwd.value=="") {
        $('#errorPassword').html('密码不为空。').css('color', 'red');
        return false;
    }
    else {
        epwd.innerHTML="";
    }
    /* 密码长度 */
    if (pwd.value.length<6 || pwd.value.length>16) {
        $('#errorPassword').html('长度为6-16。').css('color', 'red');
        return false;
    }
    else {
        epwd.innerHTML="";
    }
    return true;
}

/**
 * 确认密码输入框失去焦点
 */
function InputRepasswordBlur() {
    var rpwd = document.getElementById("Repassword");
    var erpwd = document.getElementById("errorRepassword");
    /* 确认密码不为空 */
    if (rpwd.value=="") {
        $('#errorRepassword').html('确认密码不为空。').css('color', 'red');
        return false;
    }
    else {
        erpwd.innerHTML="";
    }
    /* 确认密码与密码不一致 */
    var pwd = document.getElementById("Password");
    if (pwd.value != rpwd.value) {
        $('#errorRepassword').html('密码不一致。').css('color', 'red');
        return false;
    }
    else {
        erpwd.innerHTML="";
    }
    return true;
}


//发送验证码
/**
 *  onblur='InputUsernameBlur()' onclick="sendValidateCode()"
 *  onblur,就是光标离开输入框,有先后顺序的,光标在谁身上就只执行谁
 *
 */
function sendValidateCode(){
    var uname = document.getElementById("Username");
    var ename = document.getElementById("errorUsername");
    if (uname.value=="" ||!(/^1[3456789]\d{9}$/).test($("#Username").val())) {
        $('#errorUsername').html('请输入正确的手机号(账号)。').css('color', 'red');
        return false;
    }
    //向手机号(账号)发送验证码,指向后端
    $.ajax({
        type: 'POST',
        url:'/forget/sendCode',
        data:$(uname).serialize(),
        success:function(data){
            if(data.bstatus.code==0){
                alert(data.bstatus.des);
            }else{
                alert(data.bstatus.des);
            }
        }
    });
    //获取按钮对象
    validateCodeButton=$("#validateCodeButton")[0];
    //定时任务:一秒执行一次
    clock=window.setInterval(doLoop,1000);
}
var clock = '';//定时器对象，用于页面30秒倒计时效果
var nums = 30;
var validateCodeButton;
//基于定时器实现30秒倒计时效果
function doLoop() {
    validateCodeButton.disabled = true;//将按钮置为不可点击
    nums--;
    if (nums > 0) {
        validateCodeButton.value = nums + '秒后重新获取';
    } else {
        clearInterval(clock); //清除js定时器
        validateCodeButton.disabled = false;
        validateCodeButton.value = '重新获取验证码';
        nums = 30; //重置时间
    }
}



