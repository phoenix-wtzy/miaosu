/**
 * 表单提交时验证
 * @returns {boolean}
 */
function changePassword() {

    var Form = document.getElementById("changePasswordId");
    var bool = true;
    if (!InputOldPasswordBlur()) bool = false;
    if (!InputPasswordBlur()) bool = false;
    if (!InputRepasswordBlur()) bool = false;
    if (bool==true) {
        $.ajax({
            type: 'POST',
            url:'/changePassword/change',
            data:$(Form).serialize(),
            success:function(data){
                if(data.bstatus.code==0){
                    alert(data.bstatus.des);
                    //修改完成后刷洗页面
                    window.location.reload();
                }else{
                    alert(data.bstatus.des);
                }
            }
        });
    }

}

/**
 * 原密码输入框失去焦点
 */
function InputOldPasswordBlur() {
    var uname = document.getElementById("OldPassword");
    var ename = document.getElementById("errorOldPassword");

    /* 手机号(账号)为空/不为空 且校验手机号规则*/
    if (uname.value=="") {
        $('#errorOldPassword').html('原密码为空').css('color', 'red');
        return false;
    }
    else {
        ename.innerHTML="";
    }
    return true;
}

/**
 * 密码输入框失去焦点
 */
function InputPasswordBlur() {
    var pwd = document.getElementById("Password");
    var epwd = document.getElementById("errorPassword");
    /* 密码为空/不为空 */
    if (pwd.value=="") {
        $('#errorPassword').html('密码不为空。').css('color', 'red');
        return false;
    }
    else {
        epwd.innerHTML="";
    }
    /* 密码长度 */
    if (pwd.value.length<6 || pwd.value.length>16) {
        $('#errorPassword').html('长度为6-16。').css('color', 'red');
        return false;
    }
    else {
        epwd.innerHTML="";
    }
    return true;
}

/**
 * 确认密码输入框失去焦点
 */
function InputRepasswordBlur() {
    var rpwd = document.getElementById("Repassword");
    var erpwd = document.getElementById("errorRepassword");
    /* 确认密码不为空 */
    if (rpwd.value=="") {
        $('#errorRepassword').html('确认密码不为空。').css('color', 'red');
        return false;
    }
    else {
        erpwd.innerHTML="";
    }
    /* 确认密码与密码不一致 */
    var pwd = document.getElementById("Password");
    if (pwd.value != rpwd.value) {
        $('#errorRepassword').html('密码不一致。').css('color', 'red');
        return false;
    }
    else {
        erpwd.innerHTML="";
    }
    return true;
}






