//绑定日历控件
function bind_datapicker() {
    $('#search_date_start').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        locale: 'zh-cn',
    });
    $('#search_date_end').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        locale: 'zh-cn',
    });
}


/**
 * 切换酒店信息
 * @param e_area 内存放置的容器
 * @param ipt_id 酒店id
 * @param f_cb
 * @param search_hotel_id
 */
function hotel_search_compoment(e_area,ipt_id,f_cb,search_hotel_id) {
    //酒店搜索框
    var e_search = $(e_area).empty();
    var e_h0 = $('<input type="hidden" name="'+ipt_id+'" id="'+ipt_id+'" />')
    e_search.append(e_h0);
    var h1 = '<input type="search" placeholder="输入后按回车搜索" class="form-control" aria-haspopup="true" aria-expanded="false" />';
    var e_h1 = $(h1);
    e_search.append(e_h1);
    var e_h2= $('<ul class="dropdown-menu" style="height:200px ;overflow:scroll ;margin-top: -1px;padding:0;" ></ul>');
    e_search.append(e_h2);
    //初始化第一家酒店
    $.ajax({
        type: 'get',
        url: '/user/setting/cur/hotel',
        success: function (data) {
            if (data.bstatus.code == 0){
                if (data.data != 'null'){
                    var hoteId = data.data.hotelId;
                    var hoteName = data.data.hotelName;
                    e_h0.val(hoteId);
                    e_h1.val(hoteName);
                    $("#hideinput").val(hoteName)
                }
                if(search_hotel_id){
                    f_cb(search_hotel_id);
                }
            }else {
                e_h1.val('无酒店');
            }
        }
    });

    //添加事件
    e_h1.on('click',function () {
        $("#hideinput").val('')
        $.ajax({
                type: 'get',
                url: '/hotel/search/name',
                success: function (data) {
                    e_h2.empty();
                    $.each(data.rows,function () {
                        var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
                        e_h3.text(this.hotelName);
                        // var hotelId = this.hotelId;
                        e_h3.val(this.hotelId)
                        e_h3.prop("showName",this.hotelName)
                        e_h3.on('click',function () {
                            e_h1.val(this.showName);
                            $("#hideinput").val(this.showName)
                            var hotelId = this.value;
                            $("#"+ipt_id).val(this.value);

                            e_h2.hide();

                            //设置当前关联酒店
                            $.ajax({
                                type: 'get',
                                url: '/user/setting/cur/hotel/set',
                                data: jQuery.param({hotelId : hotelId}) ,
                                success: function (data) {
                                    console.log(data);
                                }
                            });
                            if(f_cb){
                                f_cb(this.value);
                            }
                        });
                        e_h3.appendTo(e_h2);
                    });
                    e_h2.show();
                }}
        )
    });
    //酒店搜索添加事件
    e_h1.on('search',function () {
        console.log(222)
        $.ajax({
            type: 'get',
            url: '/hotel/search/name',
            data: jQuery.param({name : this.value}) ,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (data) {
                e_h2.empty();
                if (data.total == 0){
                    e_h2.append('<li class="dropdown-item list-group-item text-danger">没有搜索到相关结果</li>')
                }else {
                    $.each(data.rows,function () {
                        var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
                        e_h3.text(this.hotelName);
                        e_h3.val(this.hotelId)
                        e_h3.prop("showName",this.hotelName)
                        e_h3.on('click',function () {
                            e_h1.val(this.showName);
                            var hotelId = this.hotelId;
                            $("#"+ipt_id).val(hotelId);
                            e_h2.hide();

                            //设置当前关联酒店
                            $.ajax({
                                    type: 'get',
                                    url: '/user/setting/cur/hotel/set',
                                    data: jQuery.param({hotelId : hotelId}) ,
                                    success: function (data) {
                                        console.log(data);
                                    }
                                }
                            )
                            if(f_cb){
                                f_cb(this.value);
                            }
                        });
                        e_h3.appendTo(e_h2);
                    });
                }
                e_h2.show();
            }}
        )
    });
}

//显示订单列表-管理界面
//操作时间	操作人	售卖日期	房型	操作内容	操作类型
function show_log_list(table,url,qparam) {
    $(table).bootstrapTable({
        method: 'get',
        pagination: true,
        sidePagination:'server',
        pageSize: 15,
        pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
        url: url,
        queryParams: qparam,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        class:"table table-hover row",
        columns: [{
            field: 'sellTime',
            title: '售卖日期',
        },{
            field: 'hotelRoomTypeName',
            title: '房型',
        },{
            field: 'opContent',
            title: '操作内容',
            width : '20%'
        }, {
            field:'createTime',
            title:'操作时间'
        },{
            field: 'userName',
            title: '操作人',
            width : '12%'
        },{
            field: 'opTypeName',
            title: '操作类型',
        }
        ]});
}


/**
 * 日志查询信息
 * @param params
 * @returns {*}
 */
function logListQueryParams(params) {
    //操作时间/售卖时间
    params.search_date_type = $("#search_date_type").prop("value");
    //开始日期
    params.search_date_start = $("#search_date_start").prop("value");
    //结束日期
    params.search_date_end = $("#search_date_end").prop("value");
    //房型
    params.room_type_id = $("#room_type_id").prop("value");
    //操作类型
    params.search_op_type = $("#search_op_type").prop("value");
    //酒店id
    params.search_hotel_id = $("#search_hotel_id").prop("value");
    // console.log(params);
    return params;
}

function query_log_list_hotelId(hotelId) {
    $('#log_list_t').bootstrapTable('refresh', {
        query: {offset: 0,
            search_date_type:$("#search_date_type").prop("value"),
            search_date_start:$("#search_date_start").prop("value"),
            search_date_end:$("#search_date_end").prop("value"),
            room_type_id:$("#room_type_id").prop("value"),
            search_op_type:$("#search_op_type").prop("value"),
            search_hotel_id:hotelId
        }
    });

    //根据酒店查询其对应的房型
    $.ajax({
        type: 'post',
        url: '/log/queryRoomSet',
        data: jQuery.param({hotelId : hotelId}) ,
        success: function (data) {
            $("#room_type_id").empty();
            $("#room_type_id").append("<option value=\"0\">房型</option>");
                if (data.data != 'null'&&data.data != null){
                    //<option th:each="room:${roomset}" th:value="${room.hotelRoomTypeId}" th:text="${room.roomTypeName}"></option>
                    $.each(data.data,function(key,value){
                        $("#room_type_id").append("<option value=\""+key+"\" text=\""+value+"\"></option>");
                    });
                }
        }
    });

}

//查询
function search_wheres(){

    $('#log_list_t').bootstrapTable('refresh', {
        query: {offset: 0,
            search_date_type:$("#search_date_type").prop("value"),
            search_date_start:$("#search_date_start").prop("value"),
            search_date_end:$("#search_date_end").prop("value"),
            room_type_id:$("#room_type_id").prop("value"),
            search_op_type:$("#search_op_type").prop("value"),
            search_hotel_id:$("#search_hotel_id").prop("value")
        }
    });

}

// 导出
function daochu() {
    // var datass = $("#log_list_t").bootstrapTable('getData')
    // console.log(datass)
    // return false
    var datas = $("#hideinput").val()
    // console.log(datas)
    $('#log_list_t').tableExport({ type: 'excel', escape: 'false', ignoreColumn: [5], fileName: datas + '操作日志' })
}

//layui表格渲染
// layui.use('table', function () {
//     var table = layui.table;
//     // 实例
//     // console.log(logListQueryParams)
//     var search_date_type = $("#search_date_type").prop("value");
//     //开始日期
//     var search_date_start = $("#search_date_start").prop("value");
//     //结束日期
//     var search_date_end = $("#search_date_end").prop("value");
//     //房型
//     var room_type_id = $("#room_type_id").prop("value");
//     //操作类型
//     var search_op_type = $("#search_op_type").prop("value");
//     //酒店id
//     var search_hotel_id = $("#search_hotel_id").prop("value");
//     table.render({
//         elem: '#demo',
//         // height: 1000,
//         url: '/log/list',
//         where: {
//             order: 'asc',
//             offset: 0,
//             search_date_type: search_date_type,
//             search_date_start: search_date_start,
//             search_date_end: search_date_end,
//             room_type_id: room_type_id,
//             search_op_type: search_op_type,
//             search_hotel_id: search_hotel_id
//         },
//         page: 'true',
//         limit: 15,
//         cols: [[
//             //表头
//             {field: 'sellTime', title: '售卖日期', sort: true},
//             {field: 'hotelRoomTypeName', title: '房型', sort: false},
//             {field: 'opContent', title: '操作内容', sort: false},
//             {field: 'createTime', title: '操作时间', sort: false},
//             {field: 'userName', title: '操作人', sort: false},
//             {field: 'opTypeName', title: '操作类型', sort: false}
//         ]]
//     })
// });