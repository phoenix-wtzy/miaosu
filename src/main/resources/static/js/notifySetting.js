/**
 * 初始化消息提醒的相关信息
 * @param hotelId 当前酒店id
 */
function init_msg_notify_info(hotelId){
    $.ajax({
        type:"POST",
        url:"/message/msgInit",
        data:{"hotelId":hotelId},
        success:function(data){
            show_sms_notify(data.msgNotifyPhone);
        }
    });
}

//显示短信提醒的信息
function show_sms_notify(msgNotifyPhone){
    //先判断是否为null或者undefined
    if(msgNotifyPhone==null || msgNotifyPhone==undefined || msgNotifyPhone==""){
        //只显示
        $("#new_phone").css({ "margin-left": "0px"})
        $("#new_phone").show();
        $("#phoe_1").hide();
        $("#phoe_2").hide();
        $("#phoe_3").hide();
        return ;
    }
    // $("#new_phone").css({ "margin-left": "-80px"})
    var phones = msgNotifyPhone.split(",");
    var len =  phones.length;
    if(len==3){
        $("#new_phone").hide();
        $("#phoe_1").show();
        $("#phoe_1 input").val(phones[0]);
        $("#phoe_2").show();
        $("#phoe_2 input").val(phones[1]);
        $("#phoe_3").show();
        $("#phoe_3 input").val(phones[2]);
    }else{
        hidenAll();
        for(var i = 1;i<=len;i++){
            $("#phoe_"+i).show();
            $("#phoe_"+i+" input").val(phones[i-1]);
        }
        $("#new_phone").show();
    }


}

//隐藏所有
function hidenAll() {
    $("#new_phone").hide();
    $("#phoe_1").hide();
    $("#phoe_2").hide();
    $("#phoe_3").hide();
}

//添加一个手机
function add_msg_phone(){
    var phone = $("#newPhone").val();
    if (!(/^1[35789]\d{9}$/).test(phone)) {
        tips($("#modal_warn"),"请输入正确的手机号");
        return false
    }
    var hotleId = $("#search_hotel_id").val();
    $.ajax({
        type:"POST",
        url:"/message/phone/add",
        data:{"phone":phone},
        success:function(result){
            if(result.bstatus.code == 0){
                init_msg_notify_info(hotleId);
                $("#new_phone input").val("");
            }
            if(result.bstatus.code == -1){
                //TODO 弹出框
                tips($("#modal_warn"),result.bstatus.des);
                init_msg_notify_info(hotleId);
            }
        }
    });
}

//鼠标移入
// function mouseOver(obj){
//     //var th = $(obj);
//     $(obj).children("a.delete").css("display", "block");
// }
// //鼠标移出
// function mouseOut(obj){
//     //var th = $(obj);
//     $(obj).children("a.delete").css("display", "none");
// }

//删除短信提醒
function deletePhone(textId){
    if(window.confirm("是否确定删除手机号码？")){
        var deletePhone = $("#"+textId).val();
        var hotleId = $("#search_hotel_id").val();
        $.ajax({
            type:"POST",
            url:"/message/phone/delete",
            data:{"phone":deletePhone},
            success:function(data){
                if(data){
                    init_msg_notify_info(hotleId);
                }
            }
        });
    }else{
        return false;
    }
}


//弹窗提示
function tips(erea,info) {
    var p = $(erea).empty();

    h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
    h2 = $('<div class="modal-dialog modal-sm">');
    h2.appendTo(h1);
    h3 = $('<div class="modal-content">');
    h3.appendTo(h2);
    h4=$('<div class="modal-header">');
    h4.appendTo(h3);
    h4.append('<h6 class="modal-title">提示：</h6>');
    h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
    h5 = $('<div class="modal-body">');
    h5.appendTo(h3);
    h5.html('<p class="text-danger">'+info+'</p>');
    h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>')
    h6.appendTo(h3);

    p.append(h1);

    h1.modal('show');
}

//初始化voice通话
function init_voice_notify_info(hotelId){
    $.ajax({
        type:"POST",
        url:"/message/msgInit",
        data:{"hotelId":hotelId},
        success:function(data){
            show_voice_notify(data.voiceNotifyPhone);
        }
    });
}

//显示voice
function show_voice_notify(voiceNotifyPhone){
    if(voiceNotifyPhone==null || voiceNotifyPhone==undefined || voiceNotifyPhone==""){
        //只显示
        $("#new_voicePhone").show();
        $("#voicePhone").hide();
        return ;
    }else{
        $("#new_voicePhone").hide();
        $("#voicePhone").show();
        $("#voicePhone input").val(voiceNotifyPhone);
    }
}

//添加一个手机
function add_voice_phone(){
    var hotleId = $("#search_hotel_id").val();
    var phone = $("#newVoicePhone").val();
    if ((/^1[35789]\d{9}$/).test(phone) ||(/^0\d{2,3}\d{7,8}$/).test(phone)) {
        $.ajax({
            type:"POST",
            url:"/message/voicephone/add",
            data:{"phone":phone},
            success:function(result){
                if(result.bstatus.code == 0){
                    init_voice_notify_info(hotleId);
                    $("#newVoicePhone").val("");
                }
                if(result.bstatus.code == -1){
                    //TODO 弹出框
                    tips($("#modal_warn"),result.bstatus.des);
                    init_voice_notify_info(hotleId);
                }
            }
        });
    }else{
        tips($("#modal_warn"),"请输入正确的手机号或者座机号");
    }

}

//删除短信提醒
function deleteVoicePhone(textId){
    var hotelId = $("#search_hotel_id").val();
    var deletePhone = $("#"+textId).val();
    $.ajax({
        type:"POST",
        url:"/message/voicephone/delete",
        data:{"phone":deletePhone},
        success:function(data){
            if(data){
                init_voice_notify_info(hotelId);
            }
        }
    });
}


/**
 * 切换酒店信息
 * @param e_area 内存放置的容器
 * @param ipt_id 酒店id
 * @param f_cb
 * @param search_hotel_id
 */
function hotel_search_compoment(e_area,ipt_id,f_cb,search_hotel_id) {
    //酒店搜索框
    var e_search = $(e_area).empty();
    var e_h0 = $('<input type="hidden" name="'+ipt_id+'" id="'+ipt_id+'" />')
    e_search.append(e_h0);
    var h1 = '<input type="search" placeholder="输入后按回车搜索" class="form-control" aria-haspopup="true" aria-expanded="false" />';
    var e_h1 = $(h1);
    e_search.append(e_h1);
    var e_h2= $('<ul class="dropdown-menu" style="height:200px ;overflow:scroll ;margin-top: -1px;padding:0;" ></ul>');
    e_search.append(e_h2);
    //初始化第一家酒店
    $.ajax({
        type: 'get',
        url: '/user/setting/cur/hotel',
        success: function (data) {
            if (data.bstatus.code == 0){
                if (data.data != 'null'){
                    var hoteId = data.data.hotelId;
                    var hoteName = data.data.hotelName;
                    e_h0.val(hoteId);
                    e_h1.val(hoteName);
                }
                if(search_hotel_id){
                    f_cb(search_hotel_id);
                }
            }else {
                e_h1.val('无酒店');
            }
        }
    });

    //添加事件
    e_h1.on('click',function () {
        $.ajax({
            type: 'get',
            url: '/hotel/search/name',
            success: function (data) {
                e_h2.empty();
                $.each(data.rows,function () {
                    var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
                    e_h3.text(this.hotelName);
                    // var hotelId = this.hotelId;
                    e_h3.val(this.hotelId)
                    e_h3.prop("showName",this.hotelName)
                    e_h3.on('click',function () {
                        e_h1.val(this.showName);
                        var hotelId = this.value;
                        $("#"+ipt_id).val(this.value);

                        e_h2.hide();

                        //设置当前关联酒店
                        $.ajax({
                            type: 'get',
                            url: '/user/setting/cur/hotel/set',
                            data: jQuery.param({hotelId : hotelId}) ,
                            success: function (data) {
                                console.log(data);
                            }
                        });
                        if(f_cb){
                            f_cb(this.value);
                        }
                    });
                    e_h3.appendTo(e_h2);
                });
                e_h2.show();
            }}
        )
    });
    //酒店搜索添加事件
    e_h1.on('search',function () {
        $.ajax({
            type: 'get',
            url: '/hotel/search/name',
            data: jQuery.param({name : this.value}) ,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (data) {
                e_h2.empty();
                if (data.total == 0){
                    e_h2.append('<li class="dropdown-item list-group-item text-danger">没有搜索到相关结果</li>')
                }else {
                    $.each(data.rows,function () {
                        var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
                        e_h3.text(this.hotelName);
                        e_h3.val(this.hotelId)
                        e_h3.prop("showName",this.hotelName)
                        e_h3.on('click',function () {
                            e_h1.val(this.showName);
                            var hotelId = this.hotelId;
                            $("#"+ipt_id).val(hotelId);
                            e_h2.hide();

                            //设置当前关联酒店
                            $.ajax({
                                    type: 'get',
                                    url: '/user/setting/cur/hotel/set',
                                    data: jQuery.param({hotelId : hotelId}) ,
                                    success: function (data) {
                                        console.log(data);
                                    }
                                }
                            )
                            if(f_cb){
                                f_cb(this.value);
                            }
                        });
                        e_h3.appendTo(e_h2);
                    });
                }
                e_h2.show();
            }}
        )
    });
}

function query_msg_notify(hotelId){
    init_msg_notify_info(hotelId);
    init_voice_notify_info(hotelId);
}