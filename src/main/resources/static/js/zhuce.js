/**
 * 表单提交时验证
 * @returns {boolean}
 */
function checkForm() {

    var Form = document.getElementById("formId");
    var bool = true;

    if (!InputUserNickBlur()) bool = false;
    if (!InputUsernameBlur()) bool = false;
    if (!InputPhoneCodeBlur()) bool = false;
    if (!InputPasswordBlur()) bool = false;
    if (!InputRepasswordBlur()) bool = false;
    // if (!InputCode_shangjiaBlur()) bool = false;
    if (bool==true) {
        $.ajax({
            type: 'POST',
            url:'/zhuce/insert',
            data:$(Form).serialize(),
            success:function(data){
                console.log(data)
                if(data.bstatus.code==0){
                    alert(data.bstatus.des);
                    //返回上次浏览的页面,也就是登录页面
                    window.history.back(-1);
                }else{
                    alert(data.bstatus.des);
                }
            }
        });
    }

}

// 公司名称失去焦点
function InputCompanyBlur() {
    var company = $("#company");
    if(company.val() == ''){
        layer.msg("公司名称不能为空。");
        return false;
    }
    return true;
}

/**
 * 姓名输入框失去焦点
 */
function InputUserNickBlur() {
    var nick = $("#UserNick");
    /* 姓名为空/不为空 */
    if (nick.val() == "") {
        // enick.innerHTML="姓名 不能为空。";
        layer.msg("姓名 不能为空。");
        return false;
    }
    return true;
}


/**
 * 手机号(账号)输入框失去焦点
 */
function InputUsernameBlur() {
    var uname = $("#Username");
    var telStr =/^[1](([3][0-9])|([4][0,1,4-9])|([5][0-3,5-9])|([6][2,5,6,7])|([7][0-8])|([8][0-9])|([9][0-3,5-9]))[0-9]{8}$/;

    /* 手机号(账号)为空/不为空 且校验手机号规则*/
    if (uname.val() == "" ||!telStr.test($("#Username").val())) {
        // ename.innerHTML="手机号(账号)不能为空。";
        layer.msg('手机号(账号)不能为空。');
        return false;
    }
    return true;
}


/**
 * 验证码输入框失去焦点
 */
function InputPhoneCodeBlur() {
    var code = $("#PhoneCode");
    /* 邀请码不为空 */
    if (code.val() == "") {
        layer.msg("验证码不能为空");
        return false;
    }
    return true;
}


/**
 * 密码输入框失去焦点
 */
function InputPasswordBlur() {
    var pwd = $("#Password");
    /* 密码为空/不为空 */
    if (pwd.val() == "") {
        // epwd.innerHTML="密码不为空。";
        layer.msg("密码不为空。");
        return false;
    }
    /* 密码长度 */
    if (pwd.val().length<6 || pwd.val().length>16) {
        // epwd.innerHTML="长度为6-16。";
        layer.msg("密码长度为6-16。");
        return false;
    }
    return true;
}

/**
 * 确认密码输入框失去焦点
 */
function InputRepasswordBlur() {
    var rpwd = $("#Repassword");
    /* 确认密码不为空 */
    if (rpwd.val()=="") {
        // erpwd.innerHTML="确认密码不为空。";
        layer.msg("确认密码不为空。");
        return false;
    }
    /* 确认密码与密码不一致 */
    var pwd = $("#Password");
    if (pwd.val() !== rpwd.val()) {
        // erpwd.innerHTML="密码不一致。";
        layer.msg("密码不一致。");
        return false;
    }
    return true;
}

//发送验证码
/**
 *  onblur='InputUsernameBlur()' onclick="sendValidateCode()"
 *  onblur,就是光标离开输入框,有先后顺序的,光标在谁身上就只执行谁
 *
 */
function sendValidateCode(){
    var uname = $("#Username");
    var telStr =/^[1](([3][0-9])|([4][0,1,4-9])|([5][0-3,5-9])|([6][2,5,6,7])|([7][0-8])|([8][0-9])|([9][0-3,5-9]))[0-9]{8}$/;
    if (uname.val() == "" ||!telStr.test($("#Username").val())) {
        layer.msg("请输入正确的手机号(账号)。");
        return false;
    }
    //向手机号(账号)发送验证码,指向后端
    $.ajax({
        type: 'POST',
        url:'/zhuce/sendCode',
        data:uname.serialize(),
        success:function(data){
            if(data.bstatus.code==0){
                alert(data.bstatus.des);
                // layer.msg(data.bstatus.des)
            }else{
                alert(data.bstatus.des);
                // layer.msg(data.bstatus.des)
            }
        }
    });
    //获取按钮对象
    validateCodeButton=$("#validateCodeButton")[0];
    //定时任务:一秒执行一次
    clock=window.setInterval(doLoop,1000);
}
var clock = '';//定时器对象，用于页面30秒倒计时效果
var nums = 30;
var validateCodeButton;
//基于定时器实现30秒倒计时效果
function doLoop() {
    validateCodeButton.disabled = true;//将按钮置为不可点击
    nums--;
    if (nums > 0) {
        validateCodeButton.value = nums + '秒后重新获取';
    } else {
        clearInterval(clock); //清除js定时器
        validateCodeButton.disabled = false;
        validateCodeButton.value = '重新获取验证码';
        nums = 30; //重置时间
    }
}


layui.use(['layer', 'form'], function(){
    var form = layui.form;

    //监听提交
    form.on('submit(formDemo)', function(data){
        // console.log(data);
        // layer.msg(JSON.stringify(data.field));
        checkForm();
        return false;
    });
});

function browserRedirect() {
    var sUserAgent = navigator.userAgent.toLowerCase();
    if (/ipad|iphone|midp|rv:1.2.3.4|ucweb|android|windows ce|windows mobile/.test(sUserAgent)) {
        //跳转移动端页面
        $(".hiddenInput").val('移动端');
        // layer.msg('移动端')
    } else {
        //跳转pc端页面
        $(".hiddenInput").val('PC端');
        // layer.msg('PC端')
    }
}
window.onload = function () {
    browserRedirect()
};