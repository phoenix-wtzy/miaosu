/*
        {monthNames:["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],monthShortNames:["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],weekDaysMin:["日", "一", "二", "三", "四", "五", "六"],weekDaysShort:["日", "一", "二", "三", "四", "五", "六"],weekDays:["周日", "周一", "周二", "周三", "周四", "周五", "周六"],am:"上午", pm: "下午", ok: "确定", cancel: "取消"}


/*
"en-us":{monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthShortNames:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],weekDaysMin:["S","M","T","W","T","F","S"],weekDaysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],weekDays:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],am:"AM",pm:"PM",ok:"Ok",cancel:"Cancel"}