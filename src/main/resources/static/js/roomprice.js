function bind_datepicker() {
    $('#setStartTime').datepicker({
        uiLibrary: 'bootstrap4',
        language: "zh-cn",
        format: 'yyyy-mm-dd',
    });
    $('#setEndTime').datepicker({
        uiLibrary: 'bootstrap4',
        language: "zh-cn",
        format: 'yyyy-mm-dd',
    });
}

function init_room_price() {
    get_next_day_data(g_yesterday);
}

function get_next_day_data(date) {
    var end_day = $("#v_end_day").attr("value");
    if (date) {
        end_day = date;
    }
    $.ajax({
        type: 'get',
        url: '/room/status/dates/next',
        data: jQuery.param({date_start: end_day}),
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            var dates = data.data;
            on_date_change(dates);
        }
    });
}

function get_pre_day_data() {
    var pre_day = $("#v_pre_day").attr("value");
    $.ajax({
        type: 'get',
        url: '/room/status/dates/pre',
        data: jQuery.param({date_end: pre_day}),
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            var dates = data.data;
            on_date_change(dates);
        }
    });
}

function on_date_change(dates) {
    var date_start = dates[0];
    var date_end = dates[dates.length - 1];
    $("#v_pre_day").attr("value", date_start);
    $("#v_pre_day").text(format_date_md(date_start));
    $("#v_end_day").attr("value", date_end);
    $("#v_end_day").text(format_date_md(date_end));
    var i = 0;
    $("#header_op").find("th.date_header").each(function () {
        var cur_date = moment(dates[i++]);
        // var day_of_week =
        $(this).html('<span>' + cur_date.format("MM-DD") + '</span><small class="ml-1 text-secondary">' + cur_date.format("dd") + '<small/>');
        
    })
    
    var url = '/room/price/list';
    $.ajax({
        type: 'get',
        url: url,
        data: jQuery.param({date_start: date_start, date_end: date_end}),
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            var priceData = data.data;
            for (var i = 0; i < priceData.length; i++) {
                //console.log(JSON.stringify(priceData[i]));
                //价格计划数组
                var hotelPriceVOs = priceData[i].hotelPriceVOs;
                //房数数组
                var hotelRoomCounts = priceData[i].hotelRoomCounts;
                //房型
                var hotelRoomSetVO = priceData[i].hotelRoomSetVO;
                var d = 0;
                var m = 0;
                $("#room_num_" + hotelRoomSetVO.hotelRoomTypeId).find("td.cell_room_num").each(function () {
                    var hotelRoomTypeId = hotelRoomSetVO.hotelRoomTypeId;
                    var num_date = dates[d++];
                    var nums = get_room_nums(num_date, hotelRoomCounts, hotelRoomTypeId);
                    var e_cell = $(this);
                    var roomCounts = hotelRoomCounts[m];
                    if (nums == "-") {
                        var h1 = '<div class="center-block text-center">--</div>'
                        e_cell.html(h1);
                    } else {
                        var h1 = '<div class="center-block text-center">有房</div>'
                        e_cell.html(h1);
                    }
                });
                
                var index = 0;
                for (var j = 0; j < hotelPriceVOs.length; j++) {
                    var m1 = 0;
                    var pd = 0;
                    $("#room_" + hotelRoomSetVO.hotelRoomTypeId + "_price_" + index++).find("td.cell_room_price").each(function () {
                        var price_data = dates[pd++];
                        var hotelRoomPrices = hotelPriceVOs[j].hotelProductVO.caigou_one;
                        // var price = get_room_prices(price_data, hotelRoomPrices);
                        var e_cell = $(this);
                        e_cell.off();
                        var priceData = new Date(price_data).getTime();
                        if (hotelRoomPrices == "-") {
                            var h2 = '<div class="center-block text-center">--</div>';
                            e_cell.html(h2);
                        } else {
                            // var h2 = '<div class="center-block text-center" data-toggle="modal" data-target="#updateSingleRoomPriceModel" onclick="updateSinglePrice(' + hotelRoomPrices + ',' + hotelPriceVOs[j].hotelProductVO.productId + ',' + priceData + ')">' + hotelRoomPrices + '</div>';
                            var h2 = '<div class="center-block text-center">' + hotelRoomPrices + '</div>';
                            e_cell.html(h2);
                        }
                        
                    });
                }
                
                
            }
        }
    });
    
}

function get_room_prices(price_data, hotelRoomPrices) {
    for (var room_price_idx in hotelRoomPrices) {
        //if(hotelRoomPrices[room_price_idx].hotelPriceId==hotelPriceId){
        //var pd=format_date(price_data);
        //var sd=hotelRoomPrices[room_price_idx].sellDate;
        if (format_date(price_data) == hotelRoomPrices[room_price_idx].sellDate) {
            return hotelRoomPrices[room_price_idx].price;
        }
        // }
    }
    return "-";
}

function get_room_nums(num_date, hotelRoomCounts, hotelRoomTypeId) {
    for (var room_nums_idx in hotelRoomCounts) {
        if (hotelRoomCounts[room_nums_idx].hotelRoomTypeId == hotelRoomTypeId) {
            if (num_date == format_date(hotelRoomCounts[room_nums_idx].sellDate)) {
                return hotelRoomCounts[room_nums_idx].countRoom;
            }
        }
    }
    return "-";
}

function update_room_price() {
    var frm = $('#f_update_room_prices');
    var info_area = $("#add_hotelPrice_error_info");
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0) {
                info_area.text("修改成功！");
                refresh_room_status();
                $('#updateRoomPriceModal').modal('hide');
            } else {
                info_area.text(data.bstatus.des);
            }
            
        },
        error: function (data) {
            console.log(data);
            info_area.text("系统错误");
        },
        
    });
}

function update_single_room_price() {
    var frm = $('#f_update_single_room_prices');
    var info_area = $("#add_ShotelPrice_error_info");
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0) {
                info_area.text("修改成功！");
                refresh_room_status();
                $('#updateSingleRoomPriceModel').modal('hide');
            } else {
                info_area.text(data.bstatus.des);
            }
            
        },
        error: function (data) {
            console.log(data);
            info_area.text("系统错误");
        },
        
    });
}

function refresh_room_status() {
    var pre_day = $("#v_pre_day").attr("value");
    var day = moment(pre_day, "YYYY-MM-DD").subtract(1, 'days').format("YYYY-MM-DD");
    get_next_day_data(day);
}

function updateSinglePrice(price, productId, priceData) {
    var date = format_date(priceData);
    $('#setTime').text(date);
    $('#setStartT').val(date);
    $('#setEndT').val(date);
    $('#price').val(price);
    $('#productId').val(productId);
    
}

function showPriceSet(e) {
    if ($(e).parent().parent().next().css('display') == 'none') {
        $(e).find('i').removeClass("triangleDown");
        $(e).find('i').addClass("triangleUp");
        $(e).parent().parent().next().show();
        
    } else {
        $(e).parent().parent().next().hide();
        $(e).find('i').removeClass("triangleUp");
        $(e).find('i').addClass("triangleDown");
        
    }
    
}