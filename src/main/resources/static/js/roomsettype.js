function show_list(table,url) {
    $(table).bootstrapTable({
        url: url,
        columns: [{
            field: 'roomTypeName',
            title: '房型归属',
        }, {
            field: 'hotelRoomTypeName',
            title: '房型名称',
        }, {
            field: 'hotelRoomTypeId',
            title: '修改',
            formatter:room_type_update,
        },{
          field:'hotelRoomTypeId',
          title:'渠道相关',
          formatter:ota_room_info,
          }
        ]});
}

/**
 *需要创建模态窗口
 */
function room_type_update(value, row, index) {
    return '<a href="#" onclick="show_update_room_type('+row.hotelRoomTypeId+',\''+row.hotelRoomTypeName+'\','+row.roomTypeId+')">'+'<span class="oi oi-pencil"></span>'+'</a>';
    //return '<span class="glyphicons glyphicons-pencil" aria-hidden="true">0</span>';
}

function show_update_room_type(hotelRoomTypeId,hotelRoomTypeName,roomTypeId) {
    $('#update_room_type_name').val(hotelRoomTypeName);
    $('#hotelRoomTypeId').val(hotelRoomTypeId);
    $('#roomtype').val(roomTypeId);
    $('#updateRoomTypeModal').modal('show');
}

function add_room_type() {
    var frm = $('#f_add_room_type');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0){
                console.log('Submission was successful.');
                $("#add_hotel_error_info").text("添加成功！");
                $('#addRoomTypeModal').modal('hide')
                reload_room_type();
            }else {
                console.log('Submission was Failed: '+ data.bstatus.des);
                $("#add_hotel_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            console.log('An error occurred.');
            console.log(data);
            $("#add_hotel_error_info").text("系统错误");
        },
    });
}

function update_room_type() {
    var frm = $('#f_update_room_type');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            console.log(data);
            if (data.bstatus.code == 0){
                console.log('Submission was successful.');
                $("#update_hotel_error_info").text("添加成功！");
                $('#updateRoomTypeModal').modal('hide')
                reload_room_type();
            }else {
                console.log('Submission was Failed: '+ data.bstatus.des);
                $("#update_hotel_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            console.log(data);
            $("#update_hotel_error_info").text("系统错误");
        },
    });
}

function reset_add_room_type() {

}

function reload_room_type() {
    $.ajax({
        type: 'get',
        url: url,
        success: function (data) {
            console.log(data);
            $('#room_set_type_list').bootstrapTable('load', data.data);
        }}
    )
}


/**
* 渠道相关的房型信息信息
**/
function ota_room_info(value,row,index){
    return '<button  data-toggle="modal" class="btn btn-sm btn-outline-primary" data-target="#checkRoomModal" onclick="check_qhh_room('+row.hotelRoomTypeId+',\''+row.hotelRoomTypeName+'\')"><span>审核结果</span></button>' ;
}
/**
*设置房型的有效性
*/
function set_active_room(hotelRoomTypeId,index){
    //var text = $("#hotel_active").text();
    var type;
    //if(text=="有效"){
        //type = 1
        //$("#hotel_active_"+index).text("无效");
    //}
    //if(text=="无效"){
         //type = 0
         //$("#hotel_active_"+index).text("有效");
    //}

    $.ajax({
        type:"POST",
        url:"/room/active",
        data:jQuery.param({hotelId:g_hotelId,hotelRoomTypeId:hotelRoomTypeId,type:1}),
        success:function(data){
            console.log(data);
            if (data.bstatus.code == 0){
                //数据库添加有效性字段

            }
        }
    });
}
/**
*获取去呼呼渠道相关房型审核信息
**/
function check_qhh_room(hotelRoomTypeId,roomTypeName){
    check_room(g_hotelId,hotelRoomTypeId,roomTypeName,"/room/check","去呼呼");
}

/**
*查看房型的审核信息
*/
function check_room(hotelId,hotelRoomTypeId,roomTypeName,url,channelName){
      $("#ModalCheckRoomTitle").text("");
      $("#checkRoomName").val("");
      $("#roomcheckMessage").val("");
    $.ajax({
        type:"POST",
        url:url,
        data:jQuery.param({hotelId:hotelId,hotelRoomTypeId:hotelRoomTypeId,roomTypeName:roomTypeName}),
        success:function(data){
            console.log(data);
            $("#ModalCheckRoomTitle").text("房型审核结果（"+channelName+"）");
            $("#checkRoomName").val(data.data.roomTypeName);
            if (data.bstatus.code == 0){
                //数据库添加有效性字段
               $("#roomcheckMessage").val(data.data.data.roomTypeCheckInfo[0].checkMessage);
            }else{
              $("#roomcheckMessage").val(data.bstatus.des);
            }
        }
    });
}