function is_undefined(o) {
    return typeof o == undefined;
}
function clearSearchInfo() {
    $("#search_orderNo, #search_bookUser, #search_hotel_confirm_number, #search_date_start, #search_date_end").val('');
}
function hotel_search_compoment(e_area, ipt_id, f_cb, search_hotel_id) {
    var e_choose = $("#choose_hotel_type");
    var choose_hotel_type = e_choose.val();
    // console.log(choose_hotel_type)
    //酒店搜索框
    var e_search = $(e_area).empty();
    var e_h0 = $('<input type="hidden" name="' + ipt_id + '" id="' + ipt_id + '" />')
    e_search.append(e_h0);
    var h1 = '<input type="search" placeholder="输入酒店名自动搜索" class="form-control" aria-haspopup="true" aria-expanded="false" />';
    var e_h1 = $(h1);
    e_search.append(e_h1);
    var e_h2 = $('<ul class="dropdown-menu" style="height:500px ;overflow:scroll ; margin-top: -1px;padding:0;" style="height:20px ;overflow:scroll" ></ul>');
    e_search.append(e_h2);
    //初始化第一家酒店
    $.ajax({
        type: 'get',
        url: '/user/setting/cur/hotel',
        data: {
            'choose_hotel_type': choose_hotel_type
        },
        success: function (data) {
            // console.log(data)
            if (data.bstatus.code == 0) {
                if (data.data != null) {
                    var hoteId = data.data.hotelId;
                    var hoteName = data.data.hotelName;
                    e_h0.val(hoteId);
                    e_h1.val(hoteName);
                    
                    if (typeof ws_order_msg != "undefined") {
                        ws_order_msg.re_connect(hoteId); //订阅该酒店的消息
                    }
                }
                //查询酒店下面的房型，添加的界面需要
                if (search_hotel_id) {
                    f_cb(search_hotel_id);
                }
            } else {
                e_h1.val('无酒店');
            }
        }
    });
    
    //添加事件
    e_choose.on('click', function () {
        e_h2.hide()
    });
    var datas = [];
    e_h1.on('click', function () {
        $("#search_hotel_id").val('');
        e_h1.val('');
        var choose_hotel_type = e_choose.val();
        // console.log(choose_hotel_type)
        $.ajax({
                type: 'get',
                url: '/hotel/search/name',
                data: {
                    'choose_hotel_type': choose_hotel_type
                },
//                data: jQuery.param({name : this.value}) ,
//                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    datas = data.rows;
                    // console.log(datas);
                    // console.log('==========')
                    e_h2.empty();
                    if(datas.length === 0){
                        var e_h3 = $('<li class="dropdown-item list-group-item">没有搜索到相关结果</li>');
                        e_h3.appendTo(e_h2);
                        e_h2.show();
                        // e_h2.append('<li class="dropdown-item list-group-item text-danger">没有搜索到相关结果</li>').show();
                    }else{
                        $.each(datas, function () {
                            var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
                            e_h3.text(this.hotelName);
                            // var hotelId = this.hotelId;
                            e_h3.val(this.hotelId);
                            e_h3.prop("showName", this.hotelName);
                            e_h3.on('click', function () {
                                e_h1.val(this.showName);
                                var hotelId = this.value;
                                $("#" + ipt_id).val(this.value);
                                e_h2.hide();

                                //订阅该酒店的消息
                                if (typeof ws_order_msg != "undefined") {
                                    ws_order_msg.re_connect(hotelId);
                                }

                                //设置当前关联酒店
                                $.ajax({
                                        type: 'get',
                                        url: '/user/setting/cur/hotel/set',
                                        data: jQuery.param({hotelId: hotelId}),
                                        success: function (data) {
                                            // console.log(data+ '11111111');
                                        }
                                    }
                                )

                                if (f_cb) {
                                    f_cb(this.value);
                                }
                            });
                            e_h3.appendTo(e_h2);
                        });
                        e_h2.show();
                    }
                }
            }
        )
    });
    e_h1.on('input propertychange', function() {
        // console.log(datas)
        e_h2.empty();
        // console.log($(this).val());
        var valName = $(this).val();
        var arr = [];
        $.each(datas, function () {
            if(this.hotelName.indexOf(valName) !== -1){
                arr.push(this)
            }
        });
        // console.log(arr);
        if(arr.length === 0){
            var e_h3 = $('<li class="dropdown-item list-group-item">没有搜索到相关结果</li>');
            e_h3.appendTo(e_h2);
        }else{
            $.each(arr, function () {
                var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
                e_h3.text(this.hotelName);
                e_h3.val(this.hotelId);
                e_h3.prop('showName', this.hotelName);
                e_h3.on('click', function () {
                    e_h1.val(this.showName);
                    var hotelId = this.value;
                    $("#" + ipt_id).val(this.value)
                    e_h2.hide();

                    //订阅该酒店的消息
                    if (typeof ws_order_msg != "undefined") {
                        ws_order_msg.re_connect(hotelId);
                    }

                    //设置当前关联酒店
                    $.ajax({
                            type: 'get',
                            url: '/user/setting/cur/hotel/set',
                            data: jQuery.param({hotelId: hotelId}),
                            success: function (data) {
                                // console.log(data);
                            }
                        }
                    )

                    if (f_cb) {
                        f_cb(this.value);
                    }
                });
                e_h3.appendTo(e_h2);
            });
        }
    });
    //酒店搜索添加事件
    // e_h1.on('search', function () {
    //     $.ajax({
    //             type: 'get',
    //             url: '/hotel/search/name',
    //             data: jQuery.param({name: this.value}),
    //             contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    //             success: function (data) {
    //                 e_h2.empty();
    //                 if (data.total == 0) {
    //                     e_h2.append('<li class="dropdown-item list-group-item text-danger">没有搜索到相关结果</li>')
    //                 } else {
    //                     $.each(data.rows, function () {
    //                         var e_h3 = $('<li class="dropdown-item list-group-item"></li>');
    //                         e_h3.text(this.hotelName);
    //                         e_h3.val(this.hotelId)
    //                         e_h3.prop("showName", this.hotelName)
    //                         e_h3.on('click', function () {
    //                             e_h1.val(this.showName);
    //                             var hotelId = this.hotelId;
    //                             $("#" + ipt_id).val(hotelId)
    //                             e_h2.hide();
    //
    //                             //订阅该酒店的消息
    //                             if (typeof ws_order_msg != "undefined") {
    //                                 ws_order_msg.re_connect(hotelId);
    //                             }
    //
    //                             //设置当前关联酒店
    //                             $.ajax({
    //                                     type: 'get',
    //                                     url: '/user/setting/cur/hotel/set',
    //                                     data: jQuery.param({hotelId: hotelId}),
    //                                     success: function (data) {
    //                                         // console.log(data);
    //                                     }
    //                                 }
    //                             )
    //
    //                             //触发查询房型
    //                             if (f_cb) {
    //                                 f_cb(this.value);
    //                             }
    //                         });
    //                         e_h3.appendTo(e_h2);
    //                     });
    //                 }
    //                 e_h2.show();
    //             }
    //         }
    //     )
    // });
}

//触发房型列表
function hotel_search_compoment_triger_room_type(hotelId) {
    //触发查询房型
    var e_hotelRoomTypeId = $("#hotelRoomTypeId").empty();
    $.ajax({
        type: 'get',
        url: '/room/set/type/list',
        data: jQuery.param({hotelId: hotelId}),
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
//                console.log(data)
            if (data.bstatus.code == 0) {
                $.each(data.data, function () {
                    var e_h1 = $('<option>' + this.hotelRoomTypeName + '</option>');
                    e_h1.val(this.hotelRoomTypeId);
                    e_hotelRoomTypeId.append(e_h1)
                })
            }
        }
    });
}

function add0(m) {
    return m < 10 ? '0' + m : m
}

//获取当前时间  格式2018-07-25 04:15
function currentTime() {
    var currentDate = new Date();
    var year = currentDate.getFullYear();
    //获取当前月
    var month = currentDate.getMonth() + 1;
    //获取当前日
    var date = currentDate.getDate();
    var h = currentDate.getHours();       //获取当前小时数(0-23)
    var m = currentDate.getMinutes();     //获取当前分钟数(0-59)
    
    var now = year + '-' + add0(month) + "-" + add0(date) + " " + add0(h) + ':' + add0(m);
    
    
    return now;
}


//添加订单
function reset_add_order() {
    
    //重置输入框
    $("#channelOrderNo").val('');
    $("#orderPrice").val('');
    $("#bookTime").val(currentTime());
    $("#checkInTime").val('');
    $("#checkOutTime").val('');
    $("#bookUser").val('');
    $("#mobile").val('');
    $("#bookRemark").val('');
    var search_hotel_id = $("#search_hotel_id").prop("value");
    hotel_search_compoment($("#hotelIdAdd"), "hotel_id_selected", hotel_search_compoment_triger_room_type, search_hotel_id);
}

function bind_datapicker() {
    //绑定日历控件
    $('#bookTime').datetimepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd HH:MM',
        locale: 'zh-cn',
    });
    $('#checkInTime').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        locale: 'zh-cn',
    });
    $('#checkOutTime').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        locale: 'zh-cn',
    });
    
    $('#search_date_start').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        locale: 'zh-cn',
    });
    $('#search_date_end').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        locale: 'zh-cn',
    });
    
    
}

function add_order() {
    var frm = $('#f_add_order');
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data, textStatus, xhr) {
            // console.log(data,xhr.status);
            if (data.bstatus.code == 0) {
                $("#add_hotel_error_info").text("添加成功！");
                $('#addOrderModal').modal('hide')
                reload_order_list();
            } else {
                $("#add_hotel_error_info").text(data.bstatus.des);
            }
        },
        error: function (data) {
            // console.log(data);
            $("#add_hotel_error_info").text("系统错误");
        }
    });
}
function reload_order_list() {
    query_order_list();
    var data = $("#order_list_t").bootstrapTable('getData')
    // console.log(data);
    var arr = [];
    $.each(data, function () {
        if(this.state == '1' || this.state == '4'){
            arr.push(this);
        }
    });
    // console.log(arr[0]);
    if(arr.length > 0){
        $.ajax({
            type: 'POST',
            url: '/msgVoiceNotify/sendMsgAndVoice',
            data: JSON.stringify(arr[0]),
            contentType: 'application/json; charset=UTF-8',
            success: function (res) {
                console.log(res)
            }
        })
    }
}
//刷新、查询订单信息
function query_order_list() {
    $('#order_list_t').bootstrapTable('refresh', {
        query: {offset: 0}
    });
}

//显示订单列表-管理界面
function show_order_list(table, url, qparam) {
    setTimeout(function () {
        $(table).bootstrapTable({
            method: 'get',
            pagination: true,
            sidePagination: 'server',
            pageSize: 10,
            pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
            url: url,
            queryParams: qparam,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            class: "table table-hover row",
            columns: [{
                field: 'orderChannelCode',  //  orderChannelName编号
                title: '渠道'
            }, {
                field: 'orderNo',
                title: '渠道单号',
                // class: 'add-at',
                formatter: function (value, row, index) {
                    return '<p style="width: 100px" class="title-space" title="' + row.orderNo + '">' + row.orderNo + '</p>';
                }
            }, {
                field: 'orderId',
                title: '订单号'
            }, {
                field: 'bookTimeStr',
                title: '预定时间'
            }, {
                field: 'hotelName',
                title: '酒店名称',
                formatter: function (value, row, index) {
                    return '<p style="width: 150px" class="title-space" title="' + row.hotelName + '">' + row.hotelName + '</p>'
                }
            },{
                field: 'bookUser',
                title: '预订人',
                class: 'man-name'
            }, {
                field: 'hotelRoomTypeName',
                title: '房间类型'
            }, {
                field: 'roomCount',
                title: '间数'
            }, {
                field: 'checkInOutTimeStr',
                title: '入离时间'
            }, {
                field: 'nightC',
                title: '间夜数'
            },{
                field: 'price',
                title: '订单金额',
                formatter: formatPrice
            }, {
                field: 'settle_money',
                title: '结算金额',
                formatter: function (value, row, index) {
                    if(row.settle_money < 0 || row.settle_money == null){
                        return '<span>房型匹配失败</span>'
                    }else{
                        return '<span>￥' + row.settle_money + '</span>'
                    }
                }
            },{
                field: 'price',
                title: '置换金额',
                formatter: formatPrice1
            },
            //     {
            //     field: 'bookRemark',
            //     // title:'备注',
            //     title: '早餐',
            //     class: 'zaocan',
            //     formatter: word_all
            // },
                {
                field: 'orderStateStr',
                title: '订单状态'
            }, {
                field: 'hotel_confirm_number',
                title: '酒店确认号',
                formatter: format_hotel_confirm_number
            },{
                field: 'operation',
                title: '操作',
                class: 'operations',
                formatter: cancel_order_btn
            }]
        });
        // console.log($(".page-list .dropup button"))
        $(".page-list .dropup button").removeAttr("data-toggle")
    }, 1000)
}
//现实订单列表-供应商界面
function show_order_list_gys(table, url, qparam) {
    setTimeout(function () {
        $(table).bootstrapTable({
            method: 'get',
            pagination: true,
            sidePagination: 'server',
            pageSize: 10,
            pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
            url: url,
            queryParams: qparam,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            class: "table table-hover row",
            columns: [
                //     {
                //     field: 'orderChannelCode',  //  orderChannelName编号
                //     title: '渠道'
                // }, {
                //     field: 'orderNo',
                //     title: '渠道单号',
                //     class: 'add-at'
                // },
                {
                    field: 'orderId',
                    title: '订单号'
                }, {
                    field: 'bookTimeStr',
                    title: '预定时间'
                }, {
                    field: 'hotelName',
                    title: '酒店名称',
                    formatter: function (value, row, index) {
                        return '<p style="width: 150px" class="title-space" title="' + row.hotelName + '">' + row.hotelName + '</p>'
                    }
                },{
                    field: 'bookUser',
                    title: '预订人',
                    class: 'man-name'
                }, {
                    field: 'hotelRoomTypeName',
                    title: '房间类型'
                }, {
                    field: 'roomCount',
                    title: '间数'
                }, {
                    field: 'checkInOutTimeStr',
                    title: '入离时间'
                }, {
                    field: 'nightC',
                    title: '间夜数'
                },{
                    field: 'price',
                    title: '订单金额',
                    formatter: formatPrice
                }, {
                    field: 'settle_money',
                    title: '结算金额',
                    formatter: function (value, row, index) {
                        if(row.settle_money < 0 || row.settle_money == null){
                            return '<span>房型匹配失败</span>'
                        }else{
                            return '<span>￥' + row.settle_money + '</span>'
                        }
                    }
                },{
                    field: 'price',
                    title: '置换金额',
                    formatter: formatPrice1
                },
                //     {
                //     field: 'bookRemark',
                //     // title:'备注',
                //     title: '早餐',
                //     class: 'zaocan',
                //     formatter: word_all
                // },
                {
                    field: 'orderStateStr',
                    title: '订单状态'
                }, {
                    field: 'hotel_confirm_number',
                    title: '酒店确认号',
                    formatter: format_hotel_confirm_number
                },{
                    field: 'operation',
                    title: '操作',
                    class: 'operations',
                    formatter: cancel_order_btn
                }]
        });
        // console.log($(".page-list .dropup button"))
        $(".page-list .dropup button").removeAttr("data-toggle")
    }, 1000)
}
function format_hotel_confirm_number (value, row, index) {
    if(row.hotel_confirm_number == null || row.hotel_confirm_number == ''){
        return '<span>-</span>'
    }else {
        // var reg = new RegExp(",","g");
        // var reg1 = new RegExp("，","g");
        // var result = row.hotel_confirm_number.replace(reg,"/");
        // var result1 = result.replace(reg1, '/')
        // return '<span>' + result1 + '</span>'
        var result = row.hotel_confirm_number + '';
        return '<p style="width: 150px" class="title-space" title="' + result + '">'+result+'</p>'
    }
}
// 管理员列表给订单金额、结算总额添加￥符号
function formatPrice(value, row, index) {
    if(row.price < 0 || row.price == null){
        return '<span>房型匹配失败</span>'
    }else{
        return '<span>￥' + row.price + '</span>'
    }
}
function formatPrice1(value, row, index) {
    if(row.caigou_total < 0 || row.caigou_total == null){
        return '<span>房型匹配失败</span>'
    }else{
        return '<span>￥' + row.caigou_total + '</span>'
    }
}

function openFrame(row) {
    $("body").css({
        'overflow-y': 'scroll'
    })
    layui.use(['layer', 'table', 'form', 'laydate', 'element'], function () {
        var layer = layui.layer,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;
        var element = layui.element;
        layer.open({
            type: 1,
            title: ['订单详情', 'font-size: 20px'],
            area: ['700px', '650px'],
            shadeClose: false,
            btn: ['打印', '取消'],
            btnAlign: 'c',
            offset: 'auto',
            content: $('#o-frame'),
            scrollbar: false,
            success: function (layero, index) {
                // console.log(JSON.parse(row));
                var data = JSON.parse(row.replace(/[\r\n\s+]/g, ''));
                $(".orderNo").text(data.orderId);  //订单号
                $(".orderState").text(data.orderStateStr); //订单状态
                $(".hotelName").text(data.hotelName);  //酒店名称
                $(".bookUser").text(data.bookUser);  //预订人--客人姓名
                // 入离时间
                $(".checkInTimeStr").text(data.checkInTimeStr);
                $(".checkOutTimeStr").text(data.checkOutTimeStr);
                $(".days").text(data.night);
                // 预定房型
                $(".hotelRoomTypeName").text(data.p_hotel_room_type_name);
                // 间夜总数
                $(".nightC").text(data.nightC);
                // $(".bookRemark").text(data.bookRemark);
                $(".roomCount").text(data.roomCount);
                // if(data.caigou_one < 0 || data.caigou_one == null){
                //     $(".caigou_one").text('房型匹配失败');
                // }else{
                //     $(".caigou_one").text('￥' + data.caigou_one);
                // }
                if(data.caigou_total < 0 || data.caigou_total == null){
                    $(".caigou_total").text('房型匹配失败');
                }else{
                    $(".caigou_total").text('￥' + data.caigou_total);
                }
                $(".confirm-number").val(data.hotel_confirm_number)
                if(data.hotel_confirm_number === '' || data.hotel_confirm_number == undefined || data.hotel_confirm_number == null){
                    $(".confirmbtns").text('添加')
                }else{
                    $(".confirmbtns").text('修改')
                }
                if(data.remark_one === '' || data.remark_one == undefined || data.remark_one == null){
                    $(".remark-one").text('--')
                }else{
                    $(".remark-one").text(data.remark_one)
                }
                table.render({
                    elem: '#calendar-table',
                    // method: 'get',
                    // url: false,
                    data: data.orderOfCaigouoneList,
                    page: false,
                    loading: false,
                    toolbar: false,
                    totalRow: true,
                    cols: [[
                        {field:'check_in_time', title: '入住时间', width: 120, align: 'center', totalRowText: '价格总计'},
                        // {field:'', title: '//', width:60, align: 'center', templet: function (d) {
                        //         return '//'
                        //     }},
                        {field:'caigou_one', title: '结算单价', width: 120, align: 'center', totalRow: true},
                        // {field:'', title: '//', width: 60, align: 'center', templet: function (d) {
                        //         return '//'
                        //     }},
                        {field: 'remark', align: 'center', title: '备注', width: 240}
                    ]],
                    done:function (res,currentCount) {
                        element.render()
                    }
                });
            },
            yes: function (index, layero) {
                // console.log('yes');
                // var input = $(".confirm-number");
                // input.each(function () {
                //     $(this).attr('value', $(this).val())
                // });
                // var prints = $(".o-frame")[0].innerHTML;
                //
                // var newWindow = window.open("打印窗口", "_blank");
                //
                // newWindow.document.write(prints);
                //
                // newWindow.document.close();
                //
                // newWindow.print();
                //
                // newWindow.close();
                $("#o-frame").jqprint({
                    debug: false,
                    importCSS: false,
                    printContainer: true,
                    operaSupport: false
                });
            },
            btn2: function (index, layero) {
                // console.log('no');
                $("body").css({
                    'overflow-y': 'unset'
                })
            },
            cancel: function (index, layero) {
                $("body").css({
                    'overflow-y': 'unset'
                })
            }
        })
    })
}

function word_all(value, row, index) {
    if (value == null) {
        return '<div class="table-overflow" >--</div>'
    }
    return '<a  class="table-overflow" onmouseover="hovertext()" data-toggle="popover"  data-placement="bottom" data-content="' + value + '">' + value + '</a>'
}

function hovertext() {
    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        container: 'body'
    })
}

function cancel_order_btn(value, row, index) {
    var res = JSON.stringify(row).replace(/"/g, '&quot;');
    //console.log(value+"----"+JSON.stringify(row)+"-------"+index);
//     var className = row.state==1?'btn-outline-primary':'btn-outline-secondary';
//     return '<button type="button" class="btn btn-sm '+className+'" id="cal'+index+'" onclick="cancel_order('+value+','+index+',this)" value="20">取消</button>';
    if (row.state == 1) {
        return '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-primary" onclick="confirmOrderNo(\'' + res + '\')">确认订单</button>' +
            '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-primary" onclick="openFrame(\'' + res + '\')">详情</button>' +
            '<br/>' +
            '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-primary" onclick="updateOrder(\'' + res + '\')">修改订单</button>' +
            '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-primary" class="btn btn-sm btn-outline-primary"  onclick="cancel_Tip(' + row.orderId + ',' + index + ',this)">取消</button>';
    } else {
        return '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-primary" onclick="confirmOrderNo(\'' + res + '\')">确认订单</button>' +
            '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-primary" onclick="openFrame(\'' + res + '\')">详情</button>' +
            '<br/>' +
            '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-primary" onclick="updateOrder(\'' + res + '\')">修改订单</button>' +
            '<button type="button" style="margin: 0 10px 10px 0" class="btn btn-sm btn-outline-secondary"  disabled="disabled">取消</button>';
    }
    
}

function cancel_Tip(orderId, index, e) {
    selectPrompt($("#modal_warn"), "是否取消？");
    $('#selectPrompt').click(function () {
        cancel_order(orderId, index, e);
    })
}
// 修改订单
function updateOrder(result) {
    // console.log(result)
    $("body").css({
        'overflow-y': 'scroll'
    });
    layui.use(['layer', 'table', 'form', 'laydate', 'element'], function () {
        var layer = layui.layer,
            form = layui.form,
            table = layui.table;
        var element = layui.element;
        layer.open({
            type: 1,
            title: ['修改订单', 'font-size:18px'],
            area: ['650px', '650px'],
            // shade: 0.3,
            shadeClose: false,
            btn: ["确定", "取消"],
            btnAlign: 'c',
            offset: 'auto',
            content: $('#update-order'),
            scrollbar: false,
            success: function (layero, index) {
                var res = JSON.parse(result.replace(/[\r\n\s+]/g, ''));
                // console.log(res);
                $("#update-form .order-name").val(res.bookUser);
                $("#update-form .order-id").val(res.orderId);
                $("#update-form .order-price").val(res.price);
                // $("#update-form .caigou-total").val(res.caigou_total);
                // if(res.caigou_one < 0 || res.caigou_one == null || res.caigou_one == undefined){
                //     $("#update-form .caigou-one").val("房型匹配失败");
                // } else {
                //     $("#update-form .caigou-one").val(res.caigou_one);
                // }
                $("#update-form .room-count").val(res.roomCount);
                $("#update-form .check-in-time").val(res.checkInTimeStr);
                $("#update-form .check-out-time").val(res.checkOutTimeStr);
                $("#update-form .nightC").val(res.nightC);
                $("#update-form .remark-one").val(res.remark_one);
                if(res.orderStateStr == '待确认'){
                    $("select[name='state']").val(1);
                }else if(res.orderStateStr == '已确认'){
                    $("select[name='state']").val(2);
                }else if(res.orderStateStr == '已取消'){
                    $("select[name='state']").val(4);
                }else if(res.orderStateStr == '已确认取消'){
                    $("select[name='state']").val(5);
                }else{
                    layer.msg('订单状态错误!')
                }
                // $("select[name='state']").val(res.orderStateStr);
                //重新渲染表单,只有执行了这一步，部分表单元素才会自动修饰成功
                form.render('select');
                table.render({
                    elem: '#calendar1-table',
                    // method: 'get',
                    // url: false,
                    data: res.orderOfCaigouoneList,
                    page: false,
                    loading: false,
                    toolbar: false,
                    totalRow: true,
                    cols: [[
                        // {field:'id', hide: true, title: 'id'},
                        {field:'check_in_time', title: '入住时间', totalRowText: '价格总计'},
                        {field:'caigou_one', title: '结算单价', edit: 'text', totalRow: true},
                        {field:'caigou_one_state', hide: true, title: '结算单价状态'},
                        {field:'remark', title: '备注', edit: 'text'},
                        {field: 'operation', title: '操作', width: 80, templet: function (d) {
                                return '<a class="layui-btn layui-btn-danger layui-btn-xs" style="color: #fff" lay-event="del">删除</a>';
                            }
                        }
                    ]],
                    done:function (res,currentCount) {
                        element.render()
                    }
                });
                //监听单元格编辑
                table.on('edit(calendar1-table)', function(obj){
                    // console.log(obj)
                    var value = obj.value //得到修改后的值
                        ,data = obj.data //得到所在行所有键值
                        ,field = obj.field; //得到字段
                    // console.log(field)
                    var newDate = new Date().getTime();
                    if(field == 'caigou_one'){
                        obj.update({
                            remark: getMyDate(newDate) + '修改了价格'
                        });
                    }else if(field == 'remark'){
                        obj.update();
                    }else{
                        layer.msg('系统错误，请联系管理员！')
                    }
                    table.reload('calendar1-table', {
                        data: layui.table.cache["calendar1-table"]
                    });
                });
                table.on('tool(calendar1-table)', function(obj){
                    // var data = obj.data; //获得当前行数据
                    var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                    var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
                    if(layEvent === 'del'){
                        layer.confirm('真的删除吗？', function(index){
                            var total;
                            var totalNight = $("#update-order .nightC").val();
                            if(totalNight > 0){
                                total = totalNight -1;
                                $("#update-order .nightC").val(total)
                                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                obj.update();
                                table.reload('calendar1-table', {
                                    data: layui.table.cache["calendar1-table"]
                                });
                                layer.close(index);
                            }else{
                                layer.msg('此订单异常，暂时无法修改。请联系管理员查询！')
                            }
                        });
                    }
                })
            },
            yes: function (index, layero) {
                var temp = {};
                var order_id = $("#update-form .order-id").val(),
                    yudingren = $("#update-form .order-name").val(),
                    price = $("#update-form .order-price").val(),
                    // caigou_total = $("#update-form .caigou-total").val(),
                    orderOfCaigouoneList = layui.table.cache["calendar1-table"],
                    room_count = $("#update-form .room-count").val(),
                    check_in_time = $("#update-form .check-in-time").val(),
                    check_out_time = $("#update-form .check-out-time").val(),
                    nightC = $("#update-form .nightC").val(),
                    state = $("#update-form #order-state").val(),
                    remark_one = $("#update-form .remark-one").val();
                temp['order_id'] = Number(order_id);
                temp['yudingren'] = yudingren;
                temp['price'] = price;
                // temp['caigou_total'] = caigou_total;
                temp['orderOfCaigouoneList'] = orderOfCaigouoneList;
                temp['room_count'] = room_count;
                temp['check_in_time'] = check_in_time;
                temp['check_out_time'] = check_out_time;
                temp['nightC'] = nightC;
                temp['state'] = state;
                temp['remark_one'] = remark_one;
                // console.log(temp)
                if(temp.check_in_time === '' || temp.check_in_time == undefined || temp.check_in_time == null){
                    layer.msg('此订单异常，暂时无法修改。请联系管理员查询！');
                }else if(temp.check_out_time == '' || temp.check_out_time == undefined || temp.check_out_time == null){
                    layer.msg('此订单异常，暂时无法修改。请联系管理员查询！');
                }else if(temp.nightC == 0 || temp.nightC == undefined || temp.nightC == null){
                    layer.msg('此订单异常，暂时无法修改。请联系管理员查询！');
                }else{
                    $.ajax({
                        type: 'POST',
                        url: '/order/updateOrderOfNew',
                        data: JSON.stringify(temp),
                        contentType: 'application/json; charset=UTF-8',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res)
                            $("#update-form")[0].reset();
                            form.render();
                            layer.close(index);
                            layer.msg(res.message);
                            reload_order_list();
                        },
                        error: function (res) {
                            layer.msg('网络错误，请重试或者联系管理员！')
                        }
                    });
                }
            },
            btn2: function (index, layero) {
                $("body").css({
                    'overflow-y': 'unset'
                });
                $("#update-form")[0].reset();
                form.render();
                layer.close(index)
            },
            cancel: function (index, layero) {
                $("body").css({
                    'overflow-y': 'unset'
                });
                $("#update-form")[0].reset();
                form.render();
                layer.close(index)
            }
        });
        //监听提交
        // form.on('submit(formDemo)', function(data){
        //     layer.msg(JSON.stringify(data.field));
        //     return false;
        // });
        // var index = layer.open();
    });
}
//修改订单取消按钮
// function layCancelBtnClick() {
//     layui.use('layer', function() {
//         layer.close(layer.index)
//     });
// }
// 修改订单提交
function updateOrderSubmit(_index) {
    // var datas = layui.table.cache["calendar1-table"];
    // console.log(datas)
    // var forms = $("#update-form");
}
function cancel_order(orderId, index, e) {
    //var flag = window.confirm("是否取消");
    //if(flag){
    var url = '/order/status/update';
    $.ajax({
        type: 'get',
        url: url,
        data: jQuery.param({orderId: orderId, status: 4}),
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            if (data.bstatus.code == 0) {
                $(e).parent().prev().prev().text("已取消");
                $(e).removeClass('btn-outline-danger');
                $(e).removeClass('btn-outline-primary');
                $(e).addClass('btn-outline-secondary');
                $(e).attr("disabled", "disabled");
                // $('#order_list_t').bootstrapTable('updateCell',{index:index,field:'orderStateStr',value:'已取消',reinit:false});
                
            } else {
                tips($("#modal_warn"), data.bstatus.des);
            }
        }
    });
    // }
}

//显示订单列表-用户界面
function show_order_list_user(table, url, qparam) {
    setTimeout(function () {
        $(table).bootstrapTable({
            method: 'get',
            pagination: true,
            sidePagination: 'server',
            pageSize: 10,
            pageList: [5, 10, 20, 50, 100, 'all'],//list can be specified here
            url: url,
            queryParams: qparam,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            class: "table table-hover row",
            columns: [{
                field: 'orderId',
                title: '订单号',
                // formatter:function (value, row, index) {
                //     return [
                //         '<a class="look" href="javascript:void(0)">',
                //         value,
                //         '</a>'
                //     ].join('');
                // },
                // events:{
                //     'click .look': function (e, value, row, index) {
                //         // alert('You click like icon, row: ' + JSON.stringify(row));
                //         // console.log(value, row, index);
                //         modal_order_info({data:row});
                //     }
                // }
            }, {
                field: 'bookTimeStr',
                title: '预定时间',
            }, {
                field: 'bookUser',
                title: '预订人',
                class: 'man-name'
            }, {
                field: 'hotelRoomTypeName',
                title: '房间类型',
            }, {
                field: 'roomCount',
                title: '间数',
            }, {
                field: 'checkInOutTimeStr',
                title: '入离时间',
            }, {
                field: 'nightC',
                title: '间夜数'
            },
            //     {
            //     field: 'caigou_one',
            //     title: '结算单价',
            //     formatter: formatCaiOne
            // },
                {
                field: 'caigou_total',
                title: '结算总价',
                formatter: formatCaiOneTotal
            },
            //     {
            //     field: 'bookRemark',
            //     title: '早餐',
            //     class: 'zaocan',
            //     formatter: word_all
            // },
                {
                field: 'orderStateStr',
                title: '订单状态'
            }, {
                field: 'hotel_confirm_number',
                title: '酒店确认号',
                formatter: format_hotel_confirm_number
            },{
                field: 'operation',
                title: '操作',
                formatter: confirm_order_btn,
            }]
        });
    }, 1000)
}

// 酒店列表给结算单价添加￥
function formatCaiOne(value, row, index) {
    if(row.caigou_one < 0 || row.caigou_one == null){
        return '<span>房型匹配失败</span>'
    }else{
        return '<span>￥' + row.caigou_one + '</span>'
    }
}
function formatCaiOneTotal(value, row, index) {
    if(row.caigou_total < 0 || row.caigou_total == null){
        return '<span>房型匹配失败</span>'
    }else{
        return '<span>￥' + row.caigou_total + '</span>'
    }
}

//知道了
function confirm_order_btn(value, row, index) {
    var className = row.state == 2 ? 'btn-outline-secondary' : 'btn-outline-danger';
    if (row.state == 5||row.state == 2) {
        return '<a style="margin-right: 10px" href="javascript:;" onclick="openFrame(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">详情</a>' +
            '<a style="margin-right: 10px" href="javascript:;" onclick="confirmOrderNo(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">确认订单</a>' +
            '<button type="button" class=" btn btn-sm btn-outline-secondary" disabled="disabled">知道了</button>';
    } else {
        
        return '<a style="margin-right: 10px" href="javascript:;" onclick="openFrame(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">详情</a>' +
            '<a style="margin-right: 10px" href="javascript:;" onclick="confirmOrderNo(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">确认订单</a>' +
            '<button type="button" class="' + className + ' btn btn-sm" onclick="confirm_order(this,\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">知道了</button>';
    }
}
//打开确认订单弹窗
// 确认订单提交
var isClick = true;
function confirmOrderNo(row) {
    var data = JSON.parse(row.replace(/[\r\n\s+]/g, ''));
    $(".confirm-frame").attr('state', data.orderId);
    $.ajax({
        type: "GET",
        url: "/order/QUERY_hotel_confirm_number",
        data: {
            order_id: data.orderId
        },
        success: function (res) {
            if (res.data.hotel_confirm_number == '' || res.data.hotel_confirm_number == null) {
                $("#c-order-num").val('')
            } else {
                $("#c-order-num").val(res.data.hotel_confirm_number)
            }
        },
        error: function (err) {
           layer.msg('网络错误，提交失败，请重试！')
        }
    });
    layui.use('layer', function () {
        var layer = layui.layer;
        layer.open({
            type: 1,
            title: ['请输入确认号确认订单', 'font-size: 20px'],
            area: ['500px'],
            shadeClose: false,
            btn: ['提交', '取消'],
            btnAlign: 'c',
            offset: 'auto',
            content: $('#confirm-frame'),
            scrollbar: false,
            yes: function (index, layero) {
                var val = $("#c-order-num").val();
                var orderId = $(".confirm-frame").attr('state');
                if (isClick) {
                    isClick = false;
                    if ($.trim(val) == '') {
                        layer.msg('确认号格式不正确，请按以下格式重新提交：1.仅允许:数字、字母及符号,/-;#. 2.仅允许纯数字，数字+字母+符号，数字+符号，数字+字母组合');
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "/order/ADD_hotel_confirm_number",
                            data: {
                                order_id: orderId,
                                hotel_confirm_number: val
                            },
                            success: function (res) {
                                layer.close(index);
                                $("#c-order-num").val('');
                                query_order_list();
                            },
                            error: function (err) {
                                layer.msg('网络错误，提交失败，请重试！')
                            }
                        })
                    }
                }
                setTimeout(function () {
                    isClick = true;
                }, 2000)   //2秒内不能重复点击
            },
            btn2: function (index, layero) {
                // console.log('nonono');
            }
        })
    })
}

//订单详情弹窗内酒店确认号修改
function confirmbtns() {
    // console.log($(".confirm-number").val());
    var temp = $(".confirm-number").val();
    var order_id = $(".orderNo").text();
    if (isClick) {
        isClick = false;
        if ($.trim(temp) == '') {
            layer.msg('确认号格式不正确，请按以下格式重新提交：1.仅允许:数字、字母及符号,/-;#. 2.仅允许纯数字，数字+字母+符号，数字+符号，数字+字母组合')
        } else {
            $.ajax({
                type: "POST",
                url: "/order/ADD_hotel_confirm_number",
                data: {
                    order_id: order_id,
                    hotel_confirm_number: temp
                },
                success: function (res) {
                    layer.msg('修改成功');
                    query_order_list()
                },
                error: function (err) {
                    layer.msg('网络错误，修改失败，请重试！');
                }
            })
        }
    }
    setTimeout(function () {
        isClick = true;
    }, 2000)   //2秒内不能重复点击
}

function confirm_order(e, row, index) {
    var result = JSON.parse(row.replace(/[\r\n\s+]/g, ''));
    // var url = '/order/status/update';
    var lengths = $("#orderMsgBoxUl").find("li").length;
    if(lengths <= 1){
        $("#orderMsgBoxUl").find("li").remove()
        $("#orderMsgBox").hide();
    }else{
        $("#orderMsgBoxUl .theOrderId").each(function (index) {
            if(result.orderId == this.innerHTML){
                $(this).parent().parent().parent().remove()
            }
        })
        if($("#orderMsgBoxUl").find("li").length == 0){
            $("#orderMsgBox").hide();
        }
    }
    update_order_status(result.orderId, 2)
    // $.ajax({
    //     type: 'get',
    //     url: url,
    //     data: jQuery.param({orderId: result.orderId, status: 2}),
    //     contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    //     success: function (data) {
    //         console.log(data)
    //         if (data.bstatus.code == 0) {
    //             $(e).removeClass('btn-outline-danger');
    //             $(e).addClass('btn-outline-secondary');
    //             // $(e).parent().prev().prev().text("已确认");
    //             // $('#order_list_t').bootstrapTable('updateCell',{index:index,field:'orderStateStr',value:'已确认'});
    //             // update_order_status(result.orderId, 2)
    //         }
    //         query_order_list();
    //     }
    // });

}

function orderListQueryParams(params) {
    var operation_number = '';
    $.each($("li[name=query_nav]"), function () {
        var value = this.id;
        if (value == 'query_nav_1') {
            //全部订单
            if (this.className.indexOf("layui-this") !== -1) {
                operation_number = '1';
                // query_order_list_all();
            }
        }else if (value == 'query_nav_3') {
            //待操作订单
            if (this.className.indexOf("layui-this") !== -1) {
                operation_number = '3';
                // query_order_list_status();
            }
        }else if(value == 'query_nav_2') {
            //今日入住
            if (this.className.indexOf("layui-this") !== -1) {
                operation_number = '2';
            }
        }
    });
    params.search_date_start = $("#search_date_start").prop("value");
    params.search_date_end = $("#search_date_end").prop("value");
    params.search_date_type = $("#search_date_type").prop("value");
    params.search_order_status = $("#search_order_status").prop("value");
    params.search_hotel_id = $("#search_hotel_id").prop("value");
    params.search_bookUser = $("#search_bookUser").prop("value");
    params.search_orderNo = $("#search_orderNo").prop("value");
    params.search_hotel_confirm_number = $("#search_hotel_confirm_number").prop("value");
    params.operation_number = operation_number;
    return params;
}

function todayQueryParams(params) {
    params.search_date_start = get_today_date();
    params.search_date_end = get_today_date();
    params.search_date_type = 2;
    params.search_order_status = 0;
    params.search_hotel_id = $("#search_hotel_id").prop("value");
    // console.log(params);
    return params;
}


//刷新、查询订单信息，根据关键词搜索
function query_order_list_keywords() {
    $('#order_list_t').bootstrapTable('refresh', {
        query: {
            offset: 0,
            search_date_start: '',
            search_date_end: '',
            search_date_type: 0,
            search_order_status: 0,
            // search_keywords: $("#search_orderNo").prop("value").trim()
        }
    });
}

//根据关键词搜索
// function search_keywords() {
//     var search_orderNo = $("#search_orderNo").prop("value").trim();
//     if (search_orderNo == '') {
//         tips($("#modal_warn"), "请输入查询内容（订单号/手机号/人名），再次查询 ！");
//         return;
//     }
//     query_order_list_keywords();
// }

//根据搜索条件进行导出
function exportExcel() {
    var g_channels = g_channel
    // console.log(g_channel)
    // var search_date_start =  $("#search_date_start").prop("value");
    // var search_date_end = $("#search_date_end").prop("value");
    // var search_date_type = $("#search_date_type").prop("value");
    // var search_order_status = $("#search_order_status").prop("value");
    // location.href="/order/orderExportExcle?search_date_start="+search_date_start+ "&search_date_end="+search_date_end+"&search_date_type="+search_date_type +"&search_order_status="+search_order_status +"&search_hotel_id="+search_hotel_id;
    $("td.add-at").attr("data-tableexport-msonumberformat", "\\@");
    var datas = $("#order_list_t").bootstrapTable('getData')
    // if (g_channels) {
    //     console.log(g_channels)
    //     console.log(111111)
    //     // return false;
    // }
    if (datas.length == 0) {
        alert('列表无查询数据，无法导出。')
        return false
    } else {
        var search_hotel_id = datas[0].hotelName;
        setTimeout(function () {
            $('#order_list_t').tableExport({
                type: 'excel',
                escape: 'false',
                ignoreColumn: ['operation'],
                fileName: search_hotel_id + '订单'
            })
        }, 1000);
    }
}

//根据条件查询
function search_wheres() {
    // $.each($("li[name=query_nav]"), function () {
    //     var value = this.id;
    //     $(this).removeClass("active");
    //     if (value == 'query_nav_1') {
    //         $(this).addClass("active")
    //     }
    // });
    query_order_list();
}

//查询全部
function query_order_list_all() {
    query_order_list();
}

//查询今日入住
function query_order_list_today() {
    $('#order_list_t').bootstrapTable('refresh', {
        query: {
            offset: 0,
            search_date_start: get_today_date(),
            search_date_end: get_today_date(),
            search_date_type: 0,
            search_order_status: 0,
            operation_number: 2
        }
    });
}

//查询待确认订单
function query_order_list_status() {
    $('#order_list_t').bootstrapTable('refresh', {
        query: {
            offset: 0,
            search_date_start: '',
            search_date_end: '',
            search_date_type: 0,
            search_order_status: 0,

        }
    });
}

//根据情况决定查询什么内容
function query_order_list_case() {
    //查询全部
    //查询今日入住
    //查询待确认
    // $.each($("li[name=query_nav]"), function () {
    //     var value = this.id;
    //     // console.log(this.className)
    //     if (value == 'query_nav_1') {
    //         if (this.className.indexOf("active") != -1) {
    //             query_order_list_all();
    //         }
    //     } else if (value == 'query_nav_2') {
    //         if (this.className.indexOf("active") != -1) {
    //             query_order_list_today();
    //         }
    //     } else if (value == 'query_nav_3') {
    //         if (this.className.indexOf("active") != -1) {
    //             query_order_list_status();
    //         }
    //     }
    //
    // })
    $.each($("li[name=query_nav]"), function () {
        var value = this.id;
        // console.log(this.className)
        if (value == 'query_nav_1') {
            if (this.className.indexOf("layui-this") !== -1) {
                query_order_list_all();
            }
        } else if (value == 'query_nav_2') {
            if (this.className.indexOf("layui-this") !== -1) {
                query_order_list_today();
            }
        } else if (value == 'query_nav_3') {
            if (this.className.indexOf("layui-this") !== -1) {
                query_order_list_status();
            }
        }

    })
}

//弹窗提示
function selectPrompt(erea, info) {
    var p = $(erea).empty();
    
    h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
    h2 = $('<div class="modal-dialog modal-sm">');
    h2.appendTo(h1);
    h3 = $('<div class="modal-content">');
    h3.appendTo(h2);
    h4 = $('<div class="modal-header">');
    h4.appendTo(h3);
    h4.append('<h6 class="modal-title">提示：</h6>');
    h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
    h5 = $('<div class="modal-body">');
    h5.appendTo(h3);
    h5.html('<p class="text-danger">' + info + '</p>');
    h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-default" data-dismiss="modal" style="float:right">取消</button>')
        .append('<button type="button" class="btn btn-primary" data-dismiss="modal" id="selectPrompt">确定</button>')
    h6.appendTo(h3);
    
    p.append(h1);
    
    h1.modal('show');
}

//弹窗提示
function tips(erea, info) {
    var p = $(erea).empty();
    
    h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
    h2 = $('<div class="modal-dialog modal-sm">');
    h2.appendTo(h1);
    h3 = $('<div class="modal-content">');
    h3.appendTo(h2);
    h4 = $('<div class="modal-header">');
    h4.appendTo(h3);
    h4.append('<h6 class="modal-title">提示：</h6>');
    h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
    h5 = $('<div class="modal-body">');
    h5.appendTo(h3);
    h5.html('<p class="text-danger">' + info + '</p>');
    h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>')
    h6.appendTo(h3);
    
    p.append(h1);
    
    h1.modal('show');
}

// function modal_order_info(info) {
    // var order = info.data;

    // var p = $("#modal_order_info").empty();
    //
    // h1 = $('<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">');
    // h2 = $('<div class="modal-dialog">');
    // h2.appendTo(h1);
    // h3 = $('<div class="modal-content">');
    // h3.appendTo(h2);
    // h4 = $('<div class="modal-header">');
    // h4.appendTo(h3);
    // h4.append('<h6 class="modal-title">订单详情：</h6>');
    // h4.append('<button type="s" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
    // h5 = $('<div class="modal-body">');
    // h5.appendTo(h3);
    // ul = $('<div class="list-group border-0">')
    // ul.append('<li class="list-group-item"><div class="ml-3">订单号：' + order.orderId + '</div></li>')
    // ul.append('<li class="list-group-item"><div>订单状态：' + order.orderStateStr + '</div></li>')
    // ul.append('<li class="list-group-item"><div>酒店名称：' + order.hotelName + '</div></li>')
    // ul.append('<li class="list-group-item"><div>预定房型：' + order.hotelRoomTypeName + ' - ' + order.sellTypeName + order.roomCount + '间</div></li>')
    // ul.append('<li class="list-group-item"><div>入住时间：' + order.checkInTimeStr + '</div></li>')
    // ul.append('<li class="list-group-item"><div>离店时间：' + order.checkOutTimeStr + '</div></li>')
    // ul.append('<li class="list-group-item"><div class="ml-3">预订人：' + order.bookUser + '</div></li>')
    // if (order.bookMobile != null) {
    //     ul.append('<li class="list-group-item"><div>联系电话：' + order.bookMobile + '</div></li>')
    // } else {
    //     ul.append('<li class="list-group-item"><div>联系电话：未添加</div></li>')
    // }
    //
    // ul.append('<li class="list-group-item"><div>订单备注：' + order.bookRemark + '</div></li>')
    // h5.append(ul);
    // h6 = $('<div class="modal-footer">').append('<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>')
    // h6.appendTo(h3);
    //
    // p.append(h1);
    //
    // h1.modal('show');
// }


//新订单通知
function WsOrderMsg(channel) {
    this.socket = new SockJS('/gs-ws');
    this.stompClient = Stomp.over(this.socket);
    this.subcribe_list = [];
    this.channel = channel;
}

WsOrderMsg.prototype = {
    init: function () {
    
    },
    //发送消息 查询所有待确认订单
    send: function (msg) {
        this.stompClient.send("/ws/order/wait", {}, msg);
    },
    //获取订阅消息 //
    subscribe: function (channel, hotelId) {
        var subscribe = this.stompClient.subscribe('/topic/order/add/' + channel, function (data) {
            // console.log(data)
            show_order_msg_box(data);
        });
        this.subcribe_list.push(subscribe);
        
        setTimeout(function () {
            ws_order_msg.send(JSON.stringify({'hotelId': hotelId}));
        }, 2000);
    },
    //webscoket  重新连接
    re_connect: function (hotelId) {
        var stompClient = this.stompClient;
        var channel = this.channel;
        clear_order_msg_box();
        if (this.subcribe_list.length > 0) {
            $.each(this.subcribe_list, function () {
                // console.log("unsubscribe",this);
                //取消订阅 依据当前酒店信息
                stompClient.unsubscribe(this.id);
            });
            this.subcribe_list = [];
            //重新订阅
            this.subscribe(channel, hotelId);
        } else {
            stompClient.connect({}, function (frame) {
                // console.log('Connected: ' + frame);
                ws_order_msg.subscribe(channel, hotelId);
            });
        }
    }
}

function check_order_msg_box() {
    if ($("#orderMsgBoxUl").find("li").length > 0) {
        $("#orderMsgBox").show();
        media_player.playaudio();
    }
}

function clear_order_msg_box() {
    $("#orderMsgBox").hide();
    $("#orderMsgBoxUl").empty(); //显示新订单的列表清空
}

function close_order_msg_box() {
    $("#orderMsgBox").hide();
}

function remove_order_msg_box(e) {
    e.preventDefault();
    // var val = $(this).parent().parent().attr('value');
    
    // $("#orderMsgBoxUl .theOrderId").each(function (index) {
    //     if(val == this.innerHTML){
    //         $(this).parent().parent().parent().remove()
        // }
    // })
    $(this).parent().parent().remove()
    if ($("#orderMsgBoxUl").find("li").length == 0) {
        $("#orderMsgBox").hide();
    }
    update_order_status(e.data.orderId, 2);
}

function update_order_status(orderId, status, f_cb) {
    var url = '/order/status/update';
    $.ajax({
        type: 'get',
        url: url,
        data: jQuery.param({orderId: orderId, status: status}),
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            query_order_list();
            if (f_cb) {
                f_cb(data);
            }
        }
    });
}

function removeall_order_msg_box() {
    var lis = $("#orderMsgBoxUl").find("li");
    if (lis.length > 0) {
        $.each(lis, function () {
            var orderId = $(this).prop("value");
            // console.log(orderId);
            update_order_status(orderId, 2);
            $(this).parent().remove();
        })
    }
    $("#orderMsgBox").hide();
}

function show_order_msg_box(msg) {
    var data = JSON.parse(msg.body);
    var boxLength = $("#orderMsgBoxUl").find("li").length
    // console.log(boxLength)

    // console.log(data.rows)
    if (data.total > 0) {
        $("#orderMsgBox").show();
        var e_body = $("#orderMsgBoxUl");
        $.each(data.rows, function () {
            var os = this.orderId
            if(boxLength > 0){
                $("#orderMsgBoxUl .theOrderId").each(function (index) {
                    if(os == this.innerHTML){
                        $(this).parent().parent().parent().remove()
                    }
                })
            }
            var li = $('<li class="list-group-item" value="' + this.orderId + '"></li>');
            var order_msg_prefix = '新订单来啦';
            if (this.state == 4) {
                order_msg_prefix = '取消单来啦';
            }
            /*
            杨小全，13720003693
            豪华大床房（含双早）（双早）
            1/13入住，住1晚，1间
            */
            // li.append('<div class="hideObj" style="display: none">' + JSON.stringify(this) + '</div>');
            li.append('<div style="text-indent: 20px"><small>' + order_msg_prefix + ' (' + format_date_time(this.bookTime) + ')</small></div>');
            // li.append('<span>订单号：<span class="theOrderId">' + this.orderId + '</span></span>')
            li.append('<div style="text-indent: 20px"><span>订&nbsp;单&nbsp;号：<span class="theOrderId">' + this.orderId + '</span></span></div>');
            li.append('<div style="text-indent: 20px"><span>预&nbsp;订&nbsp;人：<span>' + this.bookUser + '</span></span></div>');
            li.append('<div style="text-indent: 20px"><span>房&emsp;&emsp;型：<span>' + this.hotelRoomTypeName + '</span></span></div>');
            li.append('<div style="text-indent: 20px"><span>入住时间：<span>' + this.checkInTimeStr + '</span></span></div>');
            var e_h3 = $('<div style="text-align: center"></div>');
            var e_btn = $('<button type="button" class="btn btn-sm btn-outline-primary font-sm">知道了</button>');
            e_btn.on('click', this, remove_order_msg_box);
            // var e_a = $('<small><a class="ml-3" href="javascript:void(0)">查看订单</a></small>')
            // e_a.on('click', this, modal_order_info(msg))
            e_h3.append(e_btn);
            // e_h3.append(e_a);
            li.append(e_h3);
            e_body.prepend(li);
            media_player.playaudio();
            query_order_list(); //接收到消息推送时刷新表格
        });
    }
}

function MS_MediaPlayer() {
}

MS_MediaPlayer.prototype = {
    initAudio: function () {
        var audio5js = new Audio5js({
            ready: function () {
                this.load('/media/order111.mp3');
            }
        });
        this.audio = audio5js;
        return this;
    },
    playaudio: function () {
        if (!$.isEmptyObject(this.audio)) {
            this.audio.seek(0);
            this.audio.play();
        }
    },
}

function fileChange(target) {
    var fileSize = 0;
    var isIE = /msie/i.test(navigator.userAgent) && !window.opera;
    if (isIE && !target.files) {
        var filePath = target.value;
        var fileSystem = new ActiveXObject("Scripting.FileSystemObject");
        var file = fileSystem.GetFile(filePath);
        fileSize = file.Size;
    } else {
        fileSize = target.files[0].size;
    }
    var size = fileSize / 1024;
    if (size > 2000) {
        alert("附件不能大于2M");
        target.value = "";
        return
    }
    var name = target.value;
    var fileName = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
    if (fileName != "xls" && fileName != "xlsx") {
        alert("请选择execl格式文件上传！");
        target.value = "";
        return
    }
}

//定时刷新订单信息 参数单位 分钟
function reloadOrder(millons) {
    //每隔离1分钟刷新页面
    setInterval("reload_order_list()", millons * 6000 * 10);
    // setInterval('adsTime()', millons * 1000 * 60);
}
// function adsTime(){
//     window.location.reload();
// }

//获得年月日      得到日期oTime
function getMyDate(str) {
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth() + 1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        //最后拼接时间
        // oTime = oYear + '-' + getzf(oMonth) + '-' + getzf(oDay) + ' ' + getzf(oHour) + ':' + getzf(oMin) + ':' + getzf(oSen);
        oTime = getzf(oMonth) + '月' + getzf(oDay) + '日';
    return oTime;
}

//补0操作
function getzf(num) {
    if (parseInt(num) < 10) {
        num = '0' + num;
    }
    return num;
}