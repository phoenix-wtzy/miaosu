$(function () {
    // 获取‘汇总数据’表格
    $.ajax({
        type: 'POST',
        url: '/charts/order_all',
        success: function (res) {
            var data = res.data;
            $(".order-all-size").text(data.order_all_size);
            $(".order-all-night").text(data.order_all_night);
            $(".order-all-price").text(data.order_all_price)
        }
    });
    // 获取‘酒店统计’表格数据
    $.ajax({
        type: 'POST',
        url: '/charts/hotel_all',
        success: function (res) {
            // console.log(res);
            var data = res.data;
            $(".hotel-isActive").text(data.hotel_all_IsActive);
            $(".hotel-noActive").text(data.hotel_all_NotIsActive);
            $(".hotel-newAdd").text(data.hotel_all_addInThisMonth)
        }
    });
    // 获取‘昨日数据’表格数据
    $.ajax({
        type: 'POST',
        url: '/charts/order_yesterday',
        success: function (res) {
            // console.log(res);
            var data = res.data;
            $(".yesterday-size").text(data.order_yesterday_size);
            $(".yesterday-night").text(data.order_yesterday_night);
            $(".yesterday-price").text(data.order_yesterday_price)
        }
    });
    // 获取‘今日数据’表格数据
    $.ajax({
        type: 'POST',
        url: '/charts/order_today',
        success: function (res) {
            // console.log(res);
            var data = res.data;
            $(".today-size").text(data.order_today_size);
            $(".today-night").text(data.order_today_night);
            $(".today-price").text(data.order_today_price)
        }
    });

});
// 初始化layui结构
layui.use(['table', 'element'], function(){
    var table = layui.table;
    var element = layui.element;
    getList1('#settle-online', '/charts/hotel_settle_isActive', '');
    // 根据导航栏当前选中项加载表格内容
    element.on('tab(tab-all)', function(data){
        // console.log(data); //获取tab导航栏切换信息
        // console.log(this); //当前Tab标题所在的原始DOM元素
        // console.log(data.index); //得到当前Tab的所在下标
        // console.log(data.elem); //得到当前的Tab大容器
        var status = $(this).attr('data-status');
        var position = '#settle-online';
        var urls = '/charts/hotel_settle_isActive';
        switch (status) {
            case '1':
                position = '#settle-underline';//下线酒店结算总额
                urls = '/charts/hotel_settle_isNotActive';
                getList2(position, urls, status);
                break;
            case '2':
                position = '#night-online';
                urls = '/charts/hotel_settle_isActive';//上线酒店结算总间夜数
                getList3(position, urls, status);
                break;
            case '3':
                position = '#night-underline';//下线酒店结算总间夜数
                urls = '/charts/hotel_settle_isNotActive';
                getList4(position, urls, status);
                break;
            default:
                position = '#settle-online';//上线酒店结算总额
                urls = '/charts/hotel_settle_isActive';
                getList1(position, urls, status);
        }
    });

    // 上线酒店统计
    function getList1(position, urls, status){
        table.render({
            elem: position,
            method: 'get',
            url: urls,
            where: {
                status: status
            },
            page: false,
            limit: 10,
            limits: [10, 20, 50, 100],
            loading: true,
            cellMinWidth: 80,
            totalRow: true,
            cols: [[
                // {field:'hotelId', title: '酒店ID', totalRowText: '合计'},
                {field:'hotel_name', title: '酒店名称', templet:'<div><span title="{{d.hotel_name}}">{{d.hotel_name}}</span></div>' ,totalRowText: '合计'},
                {field:'createTime', title: '上线时间'},
                {field:'settle_totalMoney', title: '<span title="协议置换总额">'+'协议置换总额'+'</span>', totalRow: true},
                {field:'settle_alreadyMoney', title: '<span title="已置换总额">'+'已置换总额'+'</span>', totalRow: true},
                {field:'settle_haveMoney', title: '<span title="剩余置换总额">'+'剩余置换总额'+'</span>', totalRow: true},
                {field:'order_totalMoney', title: '<span title="实际订单总额">'+'实际订单总额'+'</span>', totalRow: true},
                {field:'commission_rate', title: '佣金率'},
                {field:'commission', title: '佣金', totalRow: true},
                {field:'needPay_money', title: '应付金额', totalRow: true},
                {field:'alreadyPay_money', title: '已支付金额', totalRow: true},
                {field:'havePay_money', title: '余额', totalRow: true}
            ]],
            parseData: function(res){
                $.each(res.data, function (index, item) {
                    item.createTime = formatDate(item.createTime)
                });
                // 解析res
                return {
                    "code": res.code,
                    "msg": res.message,
                    "count": res.count,
                    "data": res.data
                }
            }
        })
    }
    function getList3(position, urls, status){
        table.render({
            elem: position,
            method: 'get',
            url: urls,
            where: {
                status: status
            },
            page: false,
            limit: 10,
            limits: [10, 20, 50, 100],
            loading: true,
            cellMinWidth: 80,
            totalRow: true,
            cols: [[
                // {field:'hotelId', title: '酒店ID'},
                {field:'hotel_name', title: '酒店名称', templet:'<div><span title="{{d.hotel_name}}">{{d.hotel_name}}</span></div>', totalRowText: '合计'},
                {field:'createTime', title: '上线时间'},
                {field:'settle_totalNight', title: '<span title="协议置换总间夜数">'+'协议置换总间夜数'+'</span>', totalRow: true},
                {field:'settle_alreadyNight', title: '<span title="已置换总间夜数">'+'已置换总间夜数'+'</span>', totalRow: true},
                {field:'settle_haveNight', title: '<span title="剩余置换总间夜数">'+'剩余置换总间夜数'+'</span>', totalRow: true},
                {field:'order_totalMoney', title: '<span title="实际订单总额">'+'实际订单总额'+'</span>', totalRow: true},
                {field:'commission_rate', title: '佣金率'},
                {field:'commission', title: '佣金', totalRow: true},
                {field:'needPay_money', title: '应付金额', totalRow: true},
                {field:'alreadyPay_money', title: '已支付金额', totalRow: true},
                {field:'havePay_money', title: '余额', totalRow: true}
            ]],
            parseData: function(res){
                $.each(res.data, function (index, item) {
                    item.createTime = formatDate(item.createTime)
                });
                // 解析res
                return {
                    "code": res.code,
                    "msg": res.message,
                    "count": res.count,
                    "data": res.data
                }
            }
        })
    }
    // 下线酒店统计
    function getList2(position, urls, status){
        table.render({
            elem: position,
            method: 'get',
            url: urls,
            where: {
                status: status
            },
            page: false,
            limit: 10,
            limits: [10, 20, 50, 100],
            loading: true,
            cellMinWidth: 80,
            totalRow: true,
            cols: [[
                // {field:'hotelId', title: '酒店ID'},
                {field:'hotel_name', title: '酒店名称', templet:'<div><span title="{{d.hotel_name}}">{{d.hotel_name}}</span></div>', totalRowText: '合计'},
                {field:'createTime', title: '上线时间'},
                {field:'settle_totalMoney', title: '<span title="协议置换总额">'+'协议置换总额'+'</span>', totalRow: true},
                {field:'settle_alreadyMoney', title: '<span title="已置换总额">'+'已置换总额'+'</span>', totalRow: true},
                {field:'settle_haveMoney', title: '<span title="剩余置换总额">'+'剩余置换总额'+'</span>', totalRow: true},
                {field:'order_totalMoney', title: '<span title="实际订单总额">'+'实际订单总额'+'</span>', totalRow: true},
                {field:'commission_rate', title: '佣金率'},
                {field:'commission', title: '佣金', totalRow: true},
                {field:'needPay_money', title: '应付金额', totalRow: true},
                {field:'alreadyPay_money', title: '已支付金额', totalRow: true},
                {field:'havePay_money', title: '余额', totalRow: true}
            ]],
            parseData: function(res){
                $.each(res.data, function (index, item) {
                    item.createTime = formatDate(item.createTime)
                });
                // 解析res
                return {
                    "code": res.code,
                    "msg": res.message,
                    "count": res.count,
                    "data": res.data
                }
            }
        })
    }
    function getList4(position, urls, status){
        table.render({
            elem: position,
            method: 'get',
            url: urls,
            where: {
                status: status
            },
            page: false,
            limit: 10,
            limits: [10, 20, 50, 100],
            loading: true,
            cellMinWidth: 80,
            totalRow: true,
            cols: [[
                // {field:'hotelId', title: '酒店ID'},
                {field:'hotel_name', title: '酒店名称', templet:'<div><span title="{{d.hotel_name}}">{{d.hotel_name}}</span></div>', totalRowText: '合计'},
                {field:'createTime', title: '上线时间'},
                {field:'settle_totalNight', title: '<span title="协议置换总间夜数">'+'协议置换总间夜数'+'</span>', totalRow: true},
                {field:'settle_alreadyNight', title: '<span title="已置换总间夜数">'+'已置换总间夜数'+'</span>', totalRow: true},
                {field:'settle_haveNight', title: '<span title="剩余置换总间夜数">'+'剩余置换总间夜数'+'</span>', totalRow: true},
                {field:'order_totalMoney', title: '<span title="实际订单总额">'+'实际订单总额'+'</span>', totalRow: true},
                {field:'commission_rate', title: '佣金率'},
                {field:'commission', title: '佣金', totalRow: true},
                {field:'needPay_money', title: '应付金额', totalRow: true},
                {field:'alreadyPay_money', title: '已支付金额', totalRow: true},
                {field:'havePay_money', title: '余额', totalRow: true}
            ]],
            parseData: function(res){
                $.each(res.data, function (index, item) {
                    item.createTime = formatDate(item.createTime)
                });
                // 解析res
                return {
                    "code": res.code,
                    "msg": res.message,
                    "count": res.count,
                    "data": res.data
                }
            }
        })
    }













    // table.render({
    //     elem: '#total-data',
    //     url: '/charts/order_all',
    //     // height: 300,
    //     title: '汇总数据',
    //     cols: [[
    //         {field: 'order_all_size', title: '汇总订单总数'},
    //         {field: 'order_all_night', title: '汇总总间夜数'},
    //         {field: 'order_all_price', title: '汇总总订单额'}
    //     ]],
    //     parseData: function (res) {
    //         console.log(res);
    //         return {
    //             "code": 0,
    //             "msg": res.bstatus.des,
    //             "count": 3,
    //             "data": [
    //                 {
    //                     order_all_size: 100,
    //                     order_all_night: 101,
    //                     order_all_price: 102
    //                 }
    //             ]
    //         }
    //     }
    // })
    // table.render({
    //     elem: '#test'
    //     // ,url:'/demo/table/user/'
    //     ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
    //     ,cols: [[
    //         {field:'id', width:80, title: 'ID', sort: true}
    //         ,{field:'username', width:80, title: '用户名'}
    //         ,{field:'sex', width:80, title: '性别', sort: true}
    //         ,{field:'city', width:80, title: '城市'}
    //         ,{field:'sign', title: '签名', width: '30%', minWidth: 100} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
    //         ,{field:'experience', title: '积分', sort: true}
    //         ,{field:'score', title: '评分', sort: true}
    //         ,{field:'classify', title: '职业'}
    //         ,{field:'wealth', width:137, title: '财富', sort: true}
    //     ]]
    // });
});
// 日期格式转换
function formatDate (now) {
    var date = new Date(now);
    var y = date.getFullYear(); // 年份
    var m = date.getMonth() + 1; // 月份，注意：js里的月要加1
    var d = date.getDate(); // 日
    var h = date.getHours(); // 小时
    var min = date.getMinutes(); // 分钟
    var s = date.getSeconds(); // 秒
    // 返回值，根据自己需求调整，现在已经拿到了年月日时分秒了
    // return y + '-' + m + '-' + d + ' ' + h + ':' + min + ':' + s
    return y + '-' + m + '-' + d;
}
// 昨日订单列表弹窗
$(".yesterday-size").click(function () {
   layui.use(['layer', 'table'], function () {
       var layer = layui.layer;
       var table = layui.table;
       layer.open({
           type: 1,
           title: ['昨日订单', 'font-size: 20px'],
           area: ['90%', '590px'],
           shadeClose: false,
           btn: ["关闭"],
           btnAlign: 'c',
           offset: 'auto',
           content: $('#yesterdayOrder'),
           scrollbar: false,
           yes: function (index, layero) {
               // console.log('yesyesyes');
               layer.close(index)
           }
           // btn2: function (index, layero) {
           //     console.log('nonono')
           // }
       });
       table.render({
           elem: '#yesterdayOrderTable',
           method: 'get',
           url: '/charts/order_yesterdayList',
           page: true,
           limit: 10,
           limits: [10, 20, 30, 50],
           loading: true,
           // cellMinWidth: 80,
           cols: [[
               {field:'orderChannelCode', title: '渠道'},
               {field:'orderNo', title: '渠道单号'},
               {field:'orderId', title: '订单号'},
               {field:'hotelName', title: '酒店名称'},
               {field:'bookTimeStr', title: '预定时间'},
               {field:'bookUser', title: '预订人'},
               {field:'p_hotel_room_type_name', title: '房间类型'},
               {field:'roomCount', title: '间数'},
               {field:'checkInOutTimeStr', title: '入离时间'},
               {field:'nightC', title: '间夜数'},
               {field:'price', title: '订单金额'},
               {field:'caigou_total', title: '结算总额'},
               {field:'bookRemark', title: '早餐'},
               {field:'orderStateStr', title: '订单状态'},
               {field: 'hotel_confirm_number', title: '酒店确认号'}
           ]],
           parseData: function (res) {
               // console.log(res);
               var dataList = res.data;
               var total = dataList.length;
               var list = [];
               var page = $("#layui-table-page2").find(".layui-laypage-em").next().html();
               var limit = $("#layui-table-page2").find(".layui-laypage-limits select").val();
               // console.log(page + ', ' + limit);
               if(page == undefined || page == null || page == ''){
                   page =1;
               }
               if(limit == undefined || limit == null || limit == ''){
                   limit =10
               }
               var start = (page-1) * limit;
               var end = page * limit;
               if(end > total){
                   end = total;
               }
               for(var i = start; i < end; i++){
                   list.push(dataList[i])
               }
               return {
                   "code": res.code,
                   "msg": res.message,
                   "count": total,
                   "data": list
               }
           }
       });
   })
});