layui.use(['layer', 'table', 'form'], function () {
    var layer = layui.layer;
    var table = layui.table;
    var form = layui.form;
    table.render({
        elem: '#role-table',
        method: 'get',
        url: '/roles/list',
        page: true,
        // limits: [10, 20, 30, 50],
        loading: true,
        toolbar: '#toolBar',
        defaultToolbar: ['filter'],
        cols: [[
            {field: 'roleId', width: 120, title: 'id'},
            {field: 'roleName', title: '角色编码'},
            {field: 'role_description', title: '角色描述'},
            // {field: 'status', title: '状态', minWidth: 120, templet: '#statusTemp'},
            {field: 'caozuo', title: '操作', width: 120, toolbar: '#barDemo'}
        ]],
        parseData: function (res) {
            // console.log(res)
            var dataList = res.data;
            var total = dataList.length;
            var list = [];
            var page = $("#layui-table-page1").find(".layui-laypage-em").next().html();
            var limit = $("#layui-table-page1").find(".layui-laypage-limits select").val();
            // console.log(page + ', ' + limit);
            if (page == undefined || page == null || page == '') {
                page = 1;
            }
            if (limit == undefined || limit == null || limit == '') {
                limit = 10
            }
            var start = (page - 1) * limit;
            var end = page * limit;
            if (end > total) {
                end = total;
            }
            for (var i = start; i < end; i++) {
                list.push(dataList[i])
            }
            // console.log(list)
            return {
                "code": res.code,
                "msg": res.message,
                "count": total,
                "data": list
            }
        }
    });
    table.on('toolbar(role-table)', function (obj) {
        var layEvent = obj.event;
        if (layEvent == 'addRole') {
            layer.open({
                type: 1,
                title: ['添加角色', 'font-size: 18px'],
                area: ['500px'],
                shadeClose: false,
                btn: ['提交', '取消'],
                btnAlign: 'c',
                offset: 'auto',
                content: $('#add-role'),
                scrollbar: false,
                success: function (layero, index) {
                    // console.log(layero);
                    // console.log(index);
                },
                yes: function (index, layero) {
                    console.log('yes');
                    var forms = $("#add-form");
                    var roleName = $(".role-name").val();
                    var roleDescription = $(".role-description").val();
                    var pattern = /^[0-9a-zA-Z_]{6,16}$/;
                    if (roleName == '' || roleName == undefined || roleName == null) {
                        layer.msg('角色编码不能为空')
                    } else if (roleDescription == '' || roleDescription == undefined || roleDescription == null) {
                        layer.msg('角色描述不能为空')
                    } else {
                        $.ajax({
                            type: forms.attr('method'),
                            url: forms.attr('action'),
                            data: forms.serialize(),
                            success: function (res) {
                                console.log(res);
                                if (res.code == 0) {
                                    layer.close(index);
                                    layer.msg(res.message);
                                    table.reload('role-table', {
                                        page: {
                                            curr: 1
                                        }
                                    })
                                } else {
                                    layer.msg(res.message)
                                }
                            },
                            error: function (res) {
                                layer.msg(res.message)
                            }
                        })
                    }
                },
                btn2: function (index, layero) {
                    // console.log('no');
                    $("#add-form")[0].reset();
                    form.render();
                    layer.close(index);
                },
                cancel: function (index, layero) {
                    // console.log('cancel');
                    $("#add-form")[0].reset();
                    form.render();
                    layer.close(index);
                }
            })
        }
    })
    table.on('tool(role-table)', function (obj) {
        // console.log(obj);
        var data = obj.data;
        var layEvent = obj.event;
        if(layEvent == 'edit'){
            layer.open({
                type: 1,
                title: ['修改角色信息', 'font-size: 18px'],
                area: ['500px'],
                shadeClose: false,
                btn: ['提交', '取消'],
                btnAlign: 'c',
                offset: 'auto',
                content: $('#update-role'),
                scrollbar: false,
                success: function (layero, index) {
                    $("#role-id").val(data.roleId);
                    $("#role-name").val(data.roleName);
                    $("#role-description").val(data.role_description);
                },
                yes: function (index, layero) {
                    // console.log('yes');
                    var forms = $("#update-form");
                    var roleName = $("#role-name").val();
                    var roleDescription = $("#role-description").val();
                    if(roleName == '' || roleName == undefined || roleName == null){
                        layer.msg('角色编码不能为空');
                    }
                    else if(roleDescription == '' || roleDescription == undefined || roleDescription == null){
                        layer.msg('角色描述不能为空');
                    } else {
                        $.ajax({
                            type: forms.attr('method'),
                            url: forms.attr('action'),
                            data: forms.serialize(),
                            success: function (res) {
                                if(res.code == 0){
                                    layer.close(index);
                                    layer.msg(res.message);
                                    table.reload('role-table', {
                                        page: {
                                            curr: 1
                                        }
                                    })
                                }else{
                                    layer.msg(res.message);
                                }
                            },
                            error: function (res) {
                                layer.msg(res.message)
                            }
                        })
                    }
                },
                btn2: function (index, layero) {
                    // console.log('no');
                },
                cancel: function (index, layero) {
                    // console.log('cancel');
                }
            })
        }
    })
});