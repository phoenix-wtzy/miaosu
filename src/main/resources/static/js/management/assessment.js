$(function () {
   list();
});
function list() {
    layui.use(['layer', 'table', 'form'], function () {
        var layer = layui.layer,
            form = layui.form,
            table = layui.table;
        table.render({
            elem: '#assess-table',
            method: 'get',
            url: '/evaluation/easyList',
            // where: {
            //     hotelId: id,
            //     date_start: startDate,
            //     date_end: endDate
            // },
            page: true,
            loading: true,
            toolbar: '#toolBar',
            defaultToolbar: ["filter","print"],
            cols: [[
                {field: 'id', type: 'numbers', width: 100, title: '序号'},
                {field:'hotel_name', title: '酒店名称'},
                {field:'create_time', title: '创建时间', templet: function (d) {
                        return formatDate(d.create_time)
                    }},
                {field:'evaluation_state', title: '评估状态', templet: function (d) {
                    // console.log(d)
                        if(d.evaluation_state === 0){
                            return '<a style="color: gray">评估中</a>';
                        }else{
                            return '<a style="color: #01AAED" href="javascript:;" onclick="handleClick(' + d.id + ')">评估详情</a>';
                        }
                    }}
            ]],
            parseData: function (res) {
                // console.log(res)
            }
        });
        table.on('toolbar(assess-table)', function (obj) {
            var layEvent = obj.event;
            if(layEvent == 'add'){
                console.log('add');
                window.location.href = '/management/addAssessment'
            }
        })
    })
}
function handleClick(id) {
    console.log(id);
    window.location.href = '/management/assessmentDetails?id=' + id;
}
// 日期格式转换
function formatDate (now) {
    var date = new Date(now);
    var y = date.getFullYear(); // 年份
    var m = date.getMonth() + 1; // 月份，注意：js里的月要加1
    var d = date.getDate(); // 日
    var h = date.getHours(); // 小时
    var min = date.getMinutes(); // 分钟
    var s = date.getSeconds(); // 秒
    // 返回值，根据自己需求调整，现在已经拿到了年月日时分秒了
    // return y + '-' + m + '-' + d + ' ' + h + ':' + min + ':' + s
    return y + '-' + m + '-' + d;
}