$(function () {

});
layui.use(['layer', 'table', 'form'], function () {
    var layer = layui.layer;
    var table = layui.table;
    var form = layui.form;
    table.render({
        elem: '#user-table',
        method: 'get',
        url: '/users/list',
        page: true,
        // limit: 20,
        limits: [10, 20, 30, 50],
        loading: true,
        toolbar: '#toolBar',
        defaultToolbar: ['filter'],
        cols: [[
            {field:'userId', title: 'id'},
            {field:'userName', title: '账号'},
            {field:'userNick', title: '用户名'},
            {field:'user_phone', title: '电话'},
            {field:'roles', title: '角色'},
            {field:'status', title: '状态', minWidth: 120, templet: '#statusTemp'},
            {field: 'caozuo', title: '操作', toolbar: '#barDemo'}
        ]],
        parseData: function (res) {
            // console.log(res)
            var dataList = res.data;
            var total = dataList.length;
            var list = [];
            var page = $("#layui-table-page1").find(".layui-laypage-em").next().html();
            var limit = $("#layui-table-page1").find(".layui-laypage-limits select").val();
            // console.log(page + ', ' + limit);
            if(page == undefined || page == null || page == ''){
                page =1;
            }
            if(limit == undefined || limit == null || limit == ''){
                limit =10
            }
            var start = (page-1) * limit;
            var end = page * limit;
            if(end > total){
                end = total;
            }
            for(var i = start; i < end; i++){
                list.push(dataList[i])
            }
            // console.log(list)
            return {
                "code": res.code,
                "msg": res.message,
                "count": total,
                "data": list
            }
        }
    });
    table.on('toolbar(user-table)', function (obj) {
        switch (obj.event) {
            case 'addUser':
                layer.open({
                    type: 1,
                    title: ['添加用户', 'font-size: 18px'],
                    area: ['500px'],
                    shadeClose: false,
                    btn: ['提交', '取消'],
                    btnAlign: 'c',
                    offset: 'auto',
                    content: $('#add-user'),
                    scrollbar: false,
                    success: function(layero, index){
                        $.ajax({
                            type: 'get',
                            url: '/roles/list',
                            success: function (res) {
                                var html = '';
                                $.each(res.data, function (i, val) {
                                    html += '<input type="checkbox" name="role_ids" value="'+val.roleId+'" title="' +val.role_description +'" />'
                                });
                                $("#box-check").html(html);
                                form.render();
                            }
                        });
                    },
                    yes: function (index, layero) {
                        var forms = $("#add-form");
                        var userNick = $(".user-nick").val();
                        var userName = $(".user-name").val();
                        var password = $(".user-passwd").val();
                        var pattern = /^[0-9a-zA-Z_]{6,16}$/;
                        if(userNick == '' || userNick == undefined || userNick == null){
                            layer.msg('用户名不能为空');
                        }
                        else if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(userNick)){
                            layer.msg('用户名不能有特殊字符');
                        }
                        else if(/(^\_)|(\__)|(\_+$)/.test(userNick)){
                            layer.msg('用户名首尾不能出现下划线\'_\'')
                        }
                        else if(userName == '' || userName == undefined || userName == null){
                            layer.msg('手机号码不能为空')
                        }
                        else if(!new RegExp("^((13[0-9])|(14[5-9])|(15([0-3]|[5-9]))|(16[6-7])|(17[1-8])|(18[0-9])|(19[1|3])|(19[5|6])|(19[8|9]))\\d{8}$").test(userName)){
                            layer.msg('请输入正确的手机号码')
                        }
                        else if(password == '' || password == undefined || password == null){
                            layer.msg('密码不能为空')
                        }
                        else if(!pattern.test(password)){
                            layer.msg('密码只能包含字母、数字、下划线，并且长度在6-16位之间')
                        }
                        else if(!$("#box-check .layui-form-checkbox").hasClass('layui-form-checked')){
                            layer.msg('角色权限至少选择一项')
                        }
                        else {
                            $.ajax({
                                type: forms.attr('method'),
                                url: forms.attr('action'),
                                data: forms.serialize(),
                                success: function (res) {
                                    if(res.code == 0){
                                        layer.close(index);
                                        layer.msg(res.message);
                                        table.reload('user-table', {
                                            page: {
                                                curr: 1
                                            }
                                        })
                                    }else{
                                        layer.msg(res.message);
                                    }
                                },
                                error: function (res) {
                                    layer.msg(res.statusText)
                                }
                            })
                        }
                    },
                    btn2: function (index, layero) {
                        $("#add-form")[0].reset();
                        form.render();
                        layer.close(index);
                    },
                    cancel: function (index, layero) {
                        $("#add-form")[0].reset();
                        form.render();
                        layer.close(index);
                    }
                });
                break;
        }
    });
    table.on('tool(user-table)', function (obj) {
        var data = obj.data;
        // console.log(data)
        var layEvent = obj.event;
        if(layEvent === 'edit'){
            layer.open({
                type: 1,
                title: ['修改用户信息', 'font-size: 18px'],
                area: ['500px'],
                shadeClose: false,
                btn: ['提交', '取消'],
                btnAlign: 'c',
                offset: 'auto',
                content: $('#update-user'),
                scrollbar: false,
                success: function (layero, index) {
                    var roles = data.roles;
                    // console.log(roles)
                    $("#user-id").val(data.userId);
                    $("#user-nick").val(data.userNick);
                    $("#user-phone").val(data.user_phone);
                    $("#user-password").val(data.userPasswd)
                    $.ajax({
                        type: 'get',
                        url: '/roles/list',
                        success: function (res) {
                            // console.log(res.data)
                            var html = '';
                            if(roles == '' || roles == [] || roles == undefined || roles == null){
                                $.each(res.data, function (i, val) {
                                    html += '<input type="checkbox" name="role_ids" value="'+val.roleId+'" title="' +val.role_description +'" />'
                                });
                            }else{
                                $.each(res.data, function (i, val) {
                                    var ss = val.role_description.toString();
                                    if(roles.indexOf(ss) > -1){
                                        // console.log('check');
                                        html += '<input type="checkbox" checked="checked" name="role_ids" value="'+val.roleId+'" title="' +val.role_description +'" />'
                                    }else{
                                        // console.log('nocheck');
                                        html += '<input type="checkbox" name="role_ids" value="'+val.roleId+'" title="' +val.role_description +'" />'
                                    }
                                });
                            }
                            $("#box-checks").html(html);
                            form.render();
                        }
                    })
                },
                yes: function (index, layero) {
                    // console.log('yes');
                    var forms = $("#update-form");
                    var userNick = $("#user-nick").val();
                    var userPhone = $("#user-phone").val();
                    var password = $("#user-password").val();
                    var pattern = /^[0-9a-zA-Z_]{6,16}$/;
                    if(userNick == '' || userNick == undefined || userNick == null){
                        layer.msg('用户名不能为空');
                    }
                    else if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(userNick)){
                        layer.msg('用户名不能有特殊字符');
                    }
                    else if(/(^\_)|(\__)|(\_+$)/.test(userNick)){
                        layer.msg('用户名首尾不能出现下划线\'_\'')
                    }
                    else if(userPhone == '' || userPhone == undefined || userPhone == null){
                        layer.msg('手机号码不能为空')
                    }
                    else if(!new RegExp("^((13[0-9])|(14[5-9])|(15([0-3]|[5-9]))|(16[6-7])|(17[1-8])|(18[0-9])|(19[1|3])|(19[5|6])|(19[8|9]))\\d{8}$").test(userPhone)){
                        layer.msg('请输入正确的手机号码')
                    }
                    else if(password == '' || password == undefined || password == null){
                        layer.msg('密码不能为空')
                    }
                    else if(!pattern.test(password)){
                        layer.msg('密码只能包含字母、数字、下划线，并且长度在6-16位之间')
                    }
                    else if(!$("#box-checks .layui-form-checkbox").hasClass('layui-form-checked')){
                        layer.msg('角色权限至少选择一项')
                    }
                    else {
                        $.ajax({
                            type: forms.attr('method'),
                            url: forms.attr('action'),
                            data: forms.serialize(),
                            success: function (res) {
                                if(res.code == 0){
                                    layer.close(index);
                                    layer.msg(res.message);
                                    table.reload('user-table', {
                                        page: {
                                            curr: 1
                                        }
                                    })
                                }else{
                                    layer.msg(res.message);
                                }
                            },
                            error: function (res) {
                                layer.msg(res.statusText)
                            }
                        })
                    }
                },
                btn2: function (index, layero) {
                    // console.log('nonono')
                },
                cancel: function (index, layero) {
                    // console.log(layero)
                }
            })
        }
    });
    // 监听开关控件
    form.on('switch(status)', function (data) {
        // console.log(data);
        // console.log(data.elem);
        // console.log(data.elem.checked);
        // console.log(data.value);
        // console.log(data.othis)
    })
});