var jsonData = {};
var forms = $("form"),
    form1 = $("#form1"),
    form2 = $("#form2"),
    formMT = $("#formMT"),
    formXC = $("#formXC"),
    formSuccess = $("#formSuccess");
forms.hide();
form1.show();
layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = layui.layer;
    $(".toMT").on("click", function () {
        var hotelName = $(".hotel-name").val(),
            licenses = [],
            publicWork = $('input[name="publicWork"]:checked').val(),
            agent = $('input[name="agent"]:checked').val();
        $(".more-checkbox input[type=checkbox]").each(function () {
            if ($(this).is(":checked")) {
                licenses.push($(this).val())
            } else {
                licenses.push('99')
            }
        });
        if (hotelName === '' || hotelName == undefined) {
            layer.msg('酒店名称必须填写.');
        } else {
            jsonData["hotelName"] = hotelName;
            jsonData["licenses"] = JSON.stringify(licenses);
            jsonData["publicWork"] = publicWork;
            jsonData["agent"] = agent;
            forms.hide();
            formMT.show();
        }
    });
    $(".toXC").on("click", function () {
        var hotelName = $(".hotel-name").val(),
            licenses = [],
            publicWork = $('input[name="publicWork"]:checked').val(),
            agent = $('input[name="agent"]:checked').val();
        $(".more-checkbox input[type=checkbox]").each(function () {
            if ($(this).is(":checked")) {
                licenses.push($(this).val())
            } else {
                licenses.push('99')
            }
        });
        if (hotelName === '' || hotelName == undefined) {
            layer.msg('酒店名称必须填写.');
        } else {
            jsonData["hotelName"] = hotelName;
            jsonData["licenses"] = JSON.stringify(licenses);
            jsonData["publicWork"] = publicWork;
            jsonData["agent"] = agent;
            forms.hide();
            formXC.show();
        }
    });
    $(".nextMT").on("click", function () {
        var username = $('input[name="MTplatformNumber"]').val(),
            password = $('input[name="MTplatformPassword"]').val();
        if (username === '' || username == undefined) {
            layer.msg('请输入美团OTA账户.');
        } else if (password === '' || password == undefined) {
            layer.msg('请输入美团OTA密码.');
        } else {
            jsonData["MTplatformNumber"] = username;
            jsonData["MTplatformPassword"] = password;
            jsonData["XCplatformNumber"] = '';
            jsonData["XCplatformPassword"] = '';
            // ajax
            $.ajax({
                type: 'POST',
                url: '/evaluation/insert',
                data: jsonData,
                success: function (res) {
                    if (res.code == 0) {
                        forms.hide();
                        formSuccess.find("#texts").text("酒店评估申请成功，需要一定时间，请等待...");
                        formSuccess.show();
                        jsonData = {};
                    } else if (res.code == 1) {
                        forms.hide();
                        form2.show();
                        handleClick(jsonData)
                    } else {
                        layer.msg('网络错误，请重试!')
                    }
                },
                error: function (res) {
                    layer.msg(res.message)
                }
            });
        }
    });
    $(".nextXC").on("click", function () {
        var username = $('input[name="XCplatformNumber"]').val(),
            password = $('input[name="XCplatformPassword"]').val();
        if (username === '' || username == undefined) {
            layer.msg('请输入携程OTA账户.')
        } else if (username === '' || username == undefined) {
            layer.msg('请输入携程OTA密码.')
        } else {
            jsonData["XCplatformNumber"] = username;
            jsonData["XCplatformPassword"] = password;
            jsonData["MTplatformNumber"] = '';
            jsonData["MTplatformPassword"] = '';
            $.ajax({
                type: 'POST',
                url: '/evaluation/insert',
                data: jsonData,
                success: function (res) {
                    if (res.code == 0) {
                        // layer.msg('酒店评估数据添加成功.');
                        // $("#addForm")[0].reset();
                        // layui.form.render();
                        forms.hide();
                        formSuccess.find("#texts").text("酒店评估申请成功，需要一定时间，请等待...");
                        formSuccess.show();
                        jsonData = {};
                    } else if (res.code == 1) {
                        forms.hide();
                        form2.show();
                        handleClick(jsonData)
                    } else {
                        layer.msg('网络错误，请重试!')
                    }
                },
                error: function (res) {
                    layer.msg(res.message)
                }
            });
        }
    });
    form.on('submit(formMT)', function (data) {
        // var fields=$(data.form).serialize();
        return false;
    });
});
function handleClick(data) {
    var datas = data;
    $(".submitBtn").on("click", function () {
        var code = $('input[name="code"]').val();
        if(code === '' || code == undefined){
            layer.msg('验证码不能为空.')
        }else{
            datas["code"] = code;
            $.ajax({
                type: 'POST',
                url: '/evaluation/insert2',
                data: datas,
                success: function (res) {
                    if (res.code == 0) {
                        forms.hide();
                        formSuccess.find("#texts").text("酒店评估申请成功，需要一定时间，请等待...");
                        formSuccess.show();
                        jsonData = {};
                    } else {
                        layer.msg('网络错误，请重试!')
                    }
                },
                error: function (res) {
                    layer.msg(res.message)
                }
            });
        }
    })
}