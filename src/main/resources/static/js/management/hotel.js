layui.use(['layer', 'table', 'form'], function () {
    var layer = layui.layer,
        table = layui.table,
        form = layui.form;
    table.render({
        elem: '#hotel-table',
        method: 'get',
        url: '/admin/hotel/list',
        page: true,
        limit: 10,
        limits: [10, 20, 30, 50],
        loading: true,
        // toolbar: '#toolBar',
        defaultToolbar: ['filter'],
        cols: [[
            {field: 'id', type: 'numbers', width: 60, title: '序号'},
            {field: 'hotelName', title: '酒店名称'},
            {field: 'contact', title: '联系人'},
            {field: 'contactMobile', title: '联系电话'},
            {field: 'userName', title: '账号'},
            {field: 'associated_supplier', title: '关联供应商', width: 100, toolbar: '#associated_supplier'},
            {field: 'room_setting', title: '房型设置', width: 100, toolbar: '#room_setting'},
            {field: 'price_setting', title: '价格设置', width: 100, toolbar: '#price_setting'},
            {field: 'settlement_setting', title: '结算设置', width: 100, toolbar: '#settlement_setting'},
            {field: 'invoice_setting', title: '发票设置', width: 100, toolbar: '#invoice_setting'},
            {field: 'isActive',width: 100, title: '销售状态', templet: function (d) {
                    console.log(d)
                    var isActive = d.isActive;
                    if(isActive == 0) {
                        return '<span style="color: red;">已下线</span>'
                    }else if(isActive == 1){
                        return '<span style="color: #01AAED">在售</span>'
                    }
                }},
            {field: 'operation', title: '操作', width: 250, toolbar: '#operation'}
        ]],
        parseData: function (res) {
            console.log(res)
            // var list = [
            //     {
            //         hotel_name: '海旺1',
            //         contact_name: '郑亚涛1',
            //         contact_phone: '13111111111',
            //         account: 'haiwang1',
            //         hotel_status: '在售'
            //     },{
            //         hotel_name: '海旺2',
            //         contact_name: '郑亚涛2',
            //         contact_phone: '13111111112',
            //         account: 'haiwang2',
            //         hotel_status: '在售'
            //     },{
            //         hotel_name: '海旺3',
            //         contact_name: '郑亚涛3',
            //         contact_phone: '13111111113',
            //         account: 'haiwang3',
            //         hotel_status: '在售'
            //     }
            // ];
            // console.log(list)
            return {
                'code': 0,
                'msg': 'success',
                'count': res.total,
                'data': res.rows
            }
        }
    });
});