layui.use(['layer', 'table', 'form'], function () {
    var layer = layui.layer;
    var table = layui.table;
    var form = layui.form;
    table.render({
        elem: '#supplier-table',
        method: 'get',
        url: '/supplier/list',
        page: true,
        // limit: 20,
        limits: [10, 20, 30, 50],
        loading: true,
        toolbar: '#addBtn',
        defaultToolbar: ['filter'],
        cols: [[
            {field: 'id', type: 'numbers', width: 60, title: '序号'},
            {field: 'business_id', title: '公司id'},
            // {field: 'user_id', title: '当前账号id'},
            {field: 'business_name', title: '企业名称'},
            {field: 'legal_person', title: '法人'},
            {field: 'legal_phone', title: '法人联系方式'},
            {field: 'business_phone', title: '固定电话'},
            {field: 'business_email', title: '电子邮件'},
            {field: 'hotelList', title: '旗下酒店', width: 100, toolbar: '#hotelList'},
            {field: 'operation', title: '操作', width: 100, toolbar: '#operation'}
        ]],
        parseData: function (res) {
            // console.log(res)
        }
    });
    //查看旗下酒店列表
    table.on('tool(supplier-table)', function (obj) {
        var layEvent = obj.event;
        var data = obj.data;
        // console.log(data);
        var id = data.business_id;
        if (layEvent === 'list') {
            table.render({
                elem: '#hotel-list',
                method: 'get',
                url: '/supplier/supplier_hotel',
                where: {
                    business_id: id
                },
                page: true,
                // limit: 20,
                limits: [10, 20, 30, 50],
                // loading: true,
                toolbar: true,
                defaultToolbar: ['filter'],
                cols: [[
                    {field: 'id', type: 'numbers', width: 60, title: '序号'},
                    {field: 'hotel_id', width: 60, title: '酒店ID'},
                    {field: 'hotel_name', title: '酒店名称'},
                    {field: 'business_id', width: 60, title: '供应商ID'},
                    {field: 'business_name', title: '供应商名称'},
                    {
                        field: 'hotelType', title: '业务类型', templet: function (d) {
                            if (d.hotelType == 1) {
                                return 'A类置换'
                            }else if(d.hotelType == 2){
                                return 'B类分销'
                            } else {
                                return '-'
                            }
                        }
                    },
                    {field: 'commission_rate', title: '佣金比例', templet: function (d) {
                            return d.commission_rate + '%'
                        }},
                    {field: 'settle_totalMoney', title: '协议置换总金额'},
                    {field: 'settle_totalNight', title: '协议置换间夜数'},
                    {field: 'create_time', title: '创建时间', templet: function (d) {
                            var time = d.create_time;
                            if (time == null || time == '' || time == undefined || time == 0) {
                                return '-'
                            } else {
                                return getMyDate(time)
                            }
                        }},
                    {field: 'start_time', title: '上线时间', templet: function (d) {
                            var time = d.start_time;
                            if (time == null || time == '' || time == undefined || time == 0) {
                                return '-'
                            } else {
                                return getMyDate(time)
                            }
                        }},
                    {field: 'end_time', title: '下线时间', templet: function (d) {
                            var time = d.end_time;
                            if (time == null || time == '' || time == undefined || time == 0) {
                                return '-'
                            } else {
                                return getMyDate(time)
                            }
                        }},
                    {field: 'hotel_supplier_state', title: '状态', templet: function (d) {
                            if (d.hotel_supplier_state == 0) {
                                return '待上线'
                            }else if(d.hotel_supplier_state == 1){
                                return '在售'
                            } else if(d.hotel_supplier_state == 2){
                                return '已下线'
                            }else {
                                return '-'
                            }
                        }},
                    {field: 'remark', title: '备注'}
                ]],
                parseData: function (res) {
                    // console.log(res)
                }
            });
            layer.open({
                type: 1,
                title: ['旗下酒店列表', 'font-size: 18px'],
                area: ['92%', '600px'],
                shadeClose: false,
                btn: ['关闭'],
                btnAlign: 'c',
                offset: 'auto',
                content: $('#hotel_list'),
                scrollbar: false,
                success: function (layero, index) {
                    // console.log(layero, index)
                },
                yes: function (index, layero) {
                    // console.log('yes')
                    layer.close(index)
                },
                btn2: function (index, layero) {
                    // console.log('no')
                },
                cancel: function (index, layero) {
                    // console.log('cancel')
                }

            })
        } else if (layEvent == 'detail') {
            layer.open({
                type: 1,
                title: ['供应商详情', 'font-size: 18px'],
                area: ['500px'],
                shadeClose: false,
                // btn: ['提交', '取消'],
                btn: ['关闭'],
                btnAlign: 'c',
                offset: 'auto',
                content: $('#supplier-detail'),
                scrollbar: false,
                success: function (layero, index) {
                    // console.log(layero, index)
                    $.ajax({
                        type: 'POST',
                        url: '/supplier/supplierInfo',
                        data: {
                            business_id: id
                        },
                        success: function (res) {
                            console.log(res);
                            if (res.data == null) {
                                layer.msg("暂无数据")
                            } else {
                                $("#supplier-detail .business-id").val(res.data.userId);
                                $("#supplier-detail .user-name").val(res.data.userName);
                                $("#supplier-detail .user-nick").val(res.data.userNick);
                                $("#supplier-detail .user-phone").val(res.data.user_phone);
                                $("#supplier-detail .user-password").val(res.data.userPasswd);
                                $("#supplier-detail .status").val(res.data.status);
                                $("#supplier-detail .openid").val(res.data.wxOpenid);
                                $("#supplier-detail .wx-name").val(res.data.roleName);
                            }
                        }
                    });
                },
                yes: function (index, layero) {
                    // console.log('yes')
                    layer.close(index)
                },
                btn2: function (index, layero) {
                    // console.log('no')
                },
                cancel: function (index, layero) {
                    // console.log('cancel')
                }

            })
        } else if (layEvent == 'del') {
            console.log('del')
        }
    })
});

//获得年月日      得到日期oTime
function getMyDate(str) {
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth() + 1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        //最后拼接时间
        oTime = oYear + '-' + getzf(oMonth) + '-' + getzf(oDay) + ' ' + getzf(oHour) + ':' + getzf(oMin) + ':' + getzf(oSen);
    return oTime;
}

//补0操作
function getzf(num) {
    if (parseInt(num) < 10) {
        num = '0' + num;
    }
    return num;
}
