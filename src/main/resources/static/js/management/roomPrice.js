$(function () {
    // 初始化
    // console.log(g_channel)
    //酒店登陆
    if(g_channel === 'ROLE_USER'){
        // 酒店登陆
        $(".hotelSearch .hotel-type").hide();
        $(".hotelSearch #hotelName").attr("disabled", "disabled");
    }else{
        //其他登陆
        $(".hotelSearch .hotel-type").show();
        $(".hotelSearch #hotelName").removeAttr("disabled")
    }
    loadHotel(g_channel);
    // 点击酒店名输入框
    var ele = $("#hotelName");
    ele.on("click", function () {
        $(".hotel-list ul").empty();
        var hotelType = $("#hotelType").val();
        var hotelName = null;
        // console.log(hotelType);
        $.ajax({
            type: 'POST',
            url: '/room/price/search/name',
            data: {
                choose_hotel_type: hotelType,
                hotelName: hotelName
            },
            success: function (res) {
                // console.log(res);
                if (res.data == '' || res.data == null || res.data.length == 0) {
                    $("#hotel-list").text('暂无数据').show()
                } else {
                    $.each(res.data, function (index, item) {
                        var obj = {
                            id: item.hotelId,
                            name: item.hotelName
                        };
                        var html = '<li onclick="hotelListClick(\'' + JSON.stringify(obj).replace(/"/g, '&quot;') + '\', \'' + g_channel + '\')">' + item.hotelName + '</li>';
                        $(".hotel-list").show();
                        $(".hotel-list ul").append(html);
                    })
                }
            }
        });
    });
//监听input值变化
    ele.bind("input propertychange", function (event) {
        $(".hotel-list ul").empty();
        var hotelType = $("#hotelType").val();
        var name = $(this).val();
        $.ajax({
            type: 'POST',
            url: '/room/price/search/name',
            data: {
                choose_hotel_type: hotelType,
                hotelName: name
            },
            success: function (res) {
                // console.log(res)
                if (res.data == '' || res.data == null || res.data.length == 0) {
                    var html = '<li>暂无数据</li>';
                    $(".hotel-list ul").append(html);
                    $(".hotel-list").show();
                } else {
                    $.each(res.data, function (index, item) {
                        var obj = {
                            id: item.hotelId,
                            name: item.hotelName
                        };
                        var html = '<li onclick="hotelListClick(\'' + JSON.stringify(obj).replace(/"/g, '&quot;') + '\')">' + item.hotelName + '</li>';
                        $(".hotel-list").show();
                        $(".hotel-list ul").append(html);
                    })
                }
            }
        })
    });
});
// 表格渲染
function list(id, startDate, endDate, channel) {
    var startTime,
        endTime;
    $.ajax({
        type: 'POST',
        url: '/room/price/search/list',
        data: {
            hotelId: id,
            date_start: startDate,
            date_end: endDate
        },
        success: function (res) {
            // console.log(res);
            // console.log(res.data[0].dateAndprice[0])
            // console.log(res.data[0].dateAndprice[res.data[0].dateAndprice.length - 1])
            var date1 = res.data[0].dateAndprice[0].sellDate,
                date2 = res.data[0].dateAndprice[res.data[0].dateAndprice.length - 1].sellDate;
            // console.log(date1, date2)
            startTime = date1.slice(5, date1.length);
            endTime = date2.slice(5, date2.length);
            layui.use(['layer', 'table', 'form', 'laydate'], function () {
                var layer = layui.layer,
                    form = layui.form,
                    laydate = layui.laydate,
                    table = layui.table;
                var updateData = [];
                if(channel === 'ROLE_SUPER' || channel === 'ROLE_OTA'){
                    table.render({
                        elem: '#price-table',
                        method: 'get',
                        url: '/room/price/search/list',
                        where: {
                            hotelId: id,
                            date_start: startDate,
                            date_end: endDate
                        },
                        page: false,
                        // limit: Number.MAX_VALUE,
                        // limits: [10, 20, 30, 50],
                        loading: true,
                        toolbar: '#toolBar',
                        defaultToolbar: [''],
                        cols: [[
                            {type: 'checkbox', rowspan: 2},
                            {
                                field: "roomType",
                                title: '<i class="layui-icon layui-icon-left handle-click" onclick="preDate(' + id + ', null, \'' + date1 + '\', \'' + channel + '\')"></i>&nbsp;' +
                                    '<span>' + startTime + '</span>' +
                                    '~' +
                                    '<span>' + endTime + '</span>' +
                                    '&nbsp;<i class="layui-icon layui-icon-right handle-click" onclick="nextDate(' + id + ', \'' + date2 + '\', null, \'' + channel + '\')"></i>',
                                rowspan: 2,
                                align: "center",
                                minWidth: 200,
                                templet: function (d) {
                                    var result = d.hotel_price_name;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {field: "", title: "价格", colspan: 10, align: "center"}
                        ], [
                            {
                                field: 'price1', title: res.data[0].dateAndprice[0].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[0].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price2', title: res.data[0].dateAndprice[1].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[1].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price3', title: res.data[0].dateAndprice[2].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[2].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price4', title: res.data[0].dateAndprice[3].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[3].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price5', title: res.data[0].dateAndprice[4].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[4].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price6', title: res.data[0].dateAndprice[5].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[5].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[6].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[6].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[7].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[7].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[8].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[8].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[9].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[9].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            }
                        ]],
                        parseData: function (res) {
                            // console.log(res)
                        }
                    });
                    table.on('checkbox(price-table)', function (obj) {
                        // console.log(obj.checked); //当前是否选中状态
                        // console.log(obj.data); //选中行的相关数据
                        // console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one
                        if (obj.checked == true && obj.type == 'all') {
                            updateData = [];
                            $.each(res.data, function (index, item) {
                                updateData.push(item);
                            })
                        } else if (obj.checked == false && obj.type == 'all') {
                            updateData.splice(0, updateData.length);
                        } else if (obj.checked == true && obj.type == 'one') {
                            updateData.push(obj.data)
                        } else if (obj.checked == false && obj.type == 'one') {
                            var temp = obj.data.productId
                            $.each(updateData, function (index, item) {
                                var ids = item.productId;
                                if (temp == ids) {
                                    updateData.splice($.inArray(item, updateData), 1)
                                    return false
                                }
                            })
                        }
                    });
                    table.on('toolbar(price-table)', function (obj) {
                        // console.log(obj)
                        var types;
                        var layEvent = obj.event;
                        if (layEvent == 'editPrice') {
                            if (updateData.length == 0) {
                                layer.msg('请勾选要修改价格的房型，可多选！')
                            } else {
                                $("#update-form")[0].reset();
                                form.render();
                                $("#update-room-price .box").empty();
                                // console.log(updateData)
                                layer.open({
                                    type: 1,
                                    title: ['修改价格', 'font-size: 16px'],
                                    area: ['600px', '600px'],
                                    shadeClose: false,
                                    skin: 'to-fix-select',
                                    btn: ['提交', '取消'],
                                    btnAlign: 'c',
                                    offset: 'auto',
                                    content: $('#update-room-price'),
                                    scrollbar: false,
                                    success: function (layero, index) {
                                        // console.log(id)
                                        var allStartTime = '';
                                        var allEndTime = '';
                                        laydate.render({
                                            elem: '#pickerS',
                                            type: 'date',
                                            range: false,
                                            trigger: 'click',
                                            min: 0,
                                            done: function (value, date, endDate) {
                                                $(".picker .start-time").val(value)
                                            }
                                        });
                                        laydate.render({
                                            elem: '#pickerE',
                                            type: 'date',
                                            range: false,
                                            trigger: 'click',
                                            min: 0,
                                            done: function (value, date, endDate) {
                                                $(".picker .end-time").val(value)
                                            }
                                        });
                                        var html = '';
                                        $.each(updateData, function (index, item) {
                                            var sort = index + 1;
                                            html += '<div class="picker">' +
                                                '<div class="pick-i">' + sort + '</div>' +
                                                '<div class="pick-l" state="' + item.productId + '">' + item.hotel_price_name + '</div>' +
                                                '<div class="pick-s"><input type="text" class="layui-input start-time" id="pickerl' + index + '" onfocus="layDates(' + index + ')" /></div>' +
                                                '<div class="pick-e"><input type="text" class="layui-input end-time" id="pickerr' + index + '" onfocus="layDates(' + index + ')" /></div>' +
                                                '<div class="pick-r"><input type="text" class="layui-input new-price" id="price' + index + '" /></div>' +
                                                '</div>';
                                        });
                                        $("#update-room-price .box").append(html)
                                    },
                                    yes: function (index, layero) {
                                        var result = {};
                                        var arr = [];
                                        $(".box .picker").each(function (index) {
                                            var res = {
                                                product_id: '',
                                                date_start: '',
                                                date_end: '',
                                                price: ''
                                            };
                                            var productId = $(this).find('.pick-l').attr('state');
                                            var startTime = $(this).find(".start-time").val();
                                            var endTime = $(this).find(".end-time").val();
                                            var price = $(this).find(".new-price").val();
                                            if (startTime == '' || startTime == undefined || startTime == null) {
                                                layer.msg('有房型未选择开始时间，请确认！');
                                                return false
                                            } else if (endTime == '' || endTime == undefined || endTime == null) {
                                                layer.msg('有房型未选择结束时间，请确认！');
                                                return false
                                            } else if (price == '' || price == undefined || price == null) {
                                                layer.msg('有房型未填写价格，请确认！');
                                                return false
                                            } else {
                                                res.product_id = productId;
                                                res.date_start = startTime;
                                                res.date_end = endTime;
                                                res.price = price;
                                                arr.push(res);
                                                result["obj"] = arr;
                                            }
                                        });
                                        $.ajax({
                                            type: 'POST',
                                            url: '/room/price/search/update',
                                            // data: result,
                                            data: JSON.stringify(result),
                                            contentType: "application/json;charsetset=UTF-8",
                                            dataType: 'json',
                                            success: function (res) {
                                                console.log(res)
                                                if (res.code == 0) {
                                                    layer.close(index);
                                                    layer.msg(res.message);
                                                    $("#update-form")[0].reset();
                                                    form.render();
                                                    $("#update-room-price .box").empty();
                                                    updateData = [];
                                                    table.reload('price-table');
                                                } else {
                                                    layer.msg(res.message);
                                                }
                                            },
                                            error: function (res) {
                                                layer.msg('系统错误,请重试!')
                                            }
                                        })
                                    },
                                    btn2: function (index, layero) {
                                        $("#update-form")[0].reset();
                                        form.render();
                                        layer.close(index);
                                    },
                                    cancel: function (index, layero) {
                                        $("#update-form")[0].reset();
                                        form.render();
                                        layer.close(index);
                                    }
                                })
                            }
                        } else if (layEvent == 'updateRoomType') {
                            layer.msg('房型信息更新中,需要一定时间,请等待......');
                            $.ajax({
                                type: 'get',
                                url: '/room/set/getRoomType',
                                data: {
                                    hotelId: id
                                },
                                success: function (res) {
                                    console.log(res)
                                    if(res.code == -1){
                                        layer.msg('房型更新失败，请联系管理员!')
                                    }else{
                                        layer.msg('房型更新完成,请刷新表格!')
                                    }
                                }
                            });
                        } else if (layEvent == 'addRoomType') {
                            var hotelId = id;
                            layer.open({
                                type: 1,
                                title: ['添加房型', 'font-size: 16px'],
                                area: ['500px', '200px'],
                                shadeClose: false,
                                skin: 'to-fix-select',
                                btn: ['提交', '取消'],
                                btnAlign: 'c',
                                offset: 'auto',
                                content: $('#add-room-type'),
                                scrollbar: false,
                                success: function (layero, index) {
                                    $("#add-room-type #hotelId").val(hotelId);
                                    // console.log('success');
                                },
                                yes: function (index, layero) {
                                    // console.log('yes');
                                    var frm = $('#add-form');
                                    $.ajax({
                                        type: frm.attr('method'),
                                        url: frm.attr('action'),
                                        data: frm.serialize(),
                                        success: function (data) {
                                            if (data.code == 0){
                                                layer.close(index);
                                                layer.msg('添加成功');
                                                $("#add-form")[0].reset();
                                                form.render();
                                                table.reload('price-table');
                                            }else {
                                                layer.msg(data.message);
                                            }
                                        },
                                        error: function (data) {
                                            layer.msg(data.message);
                                        }
                                    });
                                },
                                btn2: function (index, layero) {
                                    // console.log('no');
                                    $("#add-form")[0].reset();
                                    form.render();
                                    layer.close(index);
                                },
                                cancel: function (index, layero) {
                                    // console.log('cancel');
                                    $("#add-form")[0].reset();
                                    form.render();
                                    layer.close(index);
                                }
                            })
                        }
                    });
                }else{
                    table.render({
                        elem: '#price-table',
                        method: 'get',
                        url: '/room/price/search/list',
                        where: {
                            hotelId: id,
                            date_start: startDate,
                            date_end: endDate
                        },
                        page: false,
                        // limit: Number.MAX_VALUE,
                        // limits: [10, 20, 30, 50],
                        loading: true,
                        toolbar: false,
                        // defaultToolbar: ['filter'],
                        cols: [[
                            // {type: 'checkbox', rowspan: 2},
                            {
                                field: "roomType",
                                title: '<i class="layui-icon layui-icon-left handle-click" onclick="preDate(' + id + ', null, \'' + date1 + '\', \'' + channel + '\')"></i>&nbsp;' +
                                    '<span>' + startTime + '</span>' +
                                    '~' +
                                    '<span>' + endTime + '</span>' +
                                    '&nbsp;<i class="layui-icon layui-icon-right handle-click" onclick="nextDate(' + id + ', \'' + date2 + '\', null, \'' + channel + '\')"></i>',
                                rowspan: 2,
                                align: "center",
                                minWidth: 200,
                                templet: function (d) {
                                    var result = d.hotel_price_name;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {field: "", title: "价格", colspan: 10, align: "center"}
                        ], [
                            {
                                field: 'price1', title: res.data[0].dateAndprice[0].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[0].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price2', title: res.data[0].dateAndprice[1].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[1].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price3', title: res.data[0].dateAndprice[2].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[2].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price4', title: res.data[0].dateAndprice[3].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[3].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price5', title: res.data[0].dateAndprice[4].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[4].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price6', title: res.data[0].dateAndprice[5].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[5].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[6].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[6].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[7].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[7].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[8].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[8].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            },
                            {
                                field: 'price7', title: res.data[0].dateAndprice[9].sellDate, templet: function (d) {
                                    var result = d.dateAndprice[9].price;
                                    if (result == null) {
                                        return '--'
                                    } else {
                                        return result
                                    }
                                }
                            }
                        ]],
                        parseData: function (res) {
                            // console.log(res)
                        }
                    });
                }
            });
        }
    });
}

function layDates(index) {
    layui.use('laydate', function () {
        var laydate = layui.laydate;
        laydate.render({
            elem: '#pickerl' + index,
            type: 'date',
            range: false,
            trigger: 'click',
            min: 0
        });
        laydate.render({
            elem: '#pickerr' + index,
            type: 'date',
            range: false,
            trigger: 'click',
            min: 0
        });
    })
}

//判断数组对象中是否有某个值
function findElem(arrayToSearch, attr, val) {
    //debugger
    for (var i = 0; i < arrayToSearch.length; i++) {
        if (arrayToSearch[i][attr] == val) {
            return i; //返回当前索引值
        }
    }
    return -1;
}

// 往前查询
function preDate(id, date1, date2, channel) {
    list(id, date1, date2, channel)
}

// 往后查询
function nextDate(id, date1, date2, channel) {
    list(id, date1, date2, channel)
}

//获得年月日      得到日期oTime
function getMyDate(str) {
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth() + 1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        //最后拼接时间
        // oTime = oYear + '-' + getzf(oMonth) + '-' + getzf(oDay) + ' ' + getzf(oHour) + ':' + getzf(oMin) + ':' + getzf(oSen);
        oTime = getzf(oMonth) + '-' + getzf(oDay);
    return oTime;
}

//补0操作
function getzf(num) {
    if (parseInt(num) < 10) {
        num = '0' + num;
    }
    return num;
}

function loadHotel(channel) {
    var hotelType = $("#hotelType").val();
    $.ajax({
        type: 'POST',
        url: '/room/price/search/name',
        data: {
            choose_hotel_type: hotelType
            // hotelName: hotelName
        },
        success: function (res) {
            // console.log(res);
            var name = res.data[0].hotelName;
            var id = res.data[0].hotelId;
            $("#hotelName").val(name).attr('state', res.data[0].hotelId);
            list(id, null, null, channel)
        }
    });
}


//酒店名下拉框点击事件
function hotelListClick(data, channel) {
    var temp = JSON.parse(data);
    // console.log(temp)
    $("#hotelName").val(temp.name).attr('state', temp.id);
    $(".hotel-list").hide();
    list(temp.id, null, null, channel);
}
// 点击空白隐藏列表框
$("body").on('click', function () {
    var dom1 = $(event.target).closest('.hotel-list').length;
    if (dom1 == 0) {
        $(".hotel-list").hide();
    }
});
//初始化form
layui.use('form', function () {
    var form = layui.form;
    form.render();
});