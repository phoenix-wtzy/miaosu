layui.use(['layer', 'table', 'form'], function () {
    var url = location.search;
    var hotelId = url.substr(1);
    var layer = layui.layer,
        form = layui.form,
        table = layui.table;
    $("#amount").show();
    $("#night").hide();
    table.render({
        elem: '#associated-table',
        method: 'get',
        url: '/hotelSupplier/list',
        where: {
            hotel_id: hotelId
        },
        page: true,
        // limit: 10,
        limits: [10, 20, 30, 50],
        loading: true,
        toolbar: '#addBtn',
        defaultToolbar: ['filter'],
        cols: [[
            {field: 'id', type: 'numbers', width: 60, title: '序号'},
            {field: 'business_name', title: '供应商名称'},
            {
                field: 'create_time', title: '创建时间', templet: function (d) {
                    var time = d.create_time;
                    if (time == null || time == '' || time == undefined || time == 0) {
                        return '-'
                    } else {
                        return getMyDate(time)
                    }
                }
            },
            {
                field: 'start_time', title: '开始时间', templet: function (d) {
                    var time = d.start_time;
                    if (time == null || time == '' || time == undefined || time == 0) {
                        return '-'
                    } else {
                        return getMyDate(time)
                    }
                }
            },
            {
                field: 'end_time', title: '结束时间', templet: function (d) {
                    var time = d.end_time;
                    if (time == null || time == '' || time == undefined || time == 0) {
                        return '-'
                    } else {
                        return getMyDate(time)
                    }
                }
            },
            {
                field: 'hotelType', title: '业务类型', width: 120, templet: function (d) {
                    if (d.hotelType == 1) {
                        return 'A类置换'
                    } else {
                        return '-'
                    }
                }
            },
            {
                field: 'commission_rate', title: '佣金比例', width: 120, templet: function (d) {
                    return d.commission_rate + '%'
                }
            },
            {
                field: 'settle_totalMoney', title: '协议总金额', width: 150, templet: function (d) {
                    var res = d.settle_totalMoney;
                    if (res == null || res == '' || res == undefined) {
                        return '-'
                    } else {
                        return d.settle_totalMoney;
                    }
                }
            },
            {
                field: 'settle_totalNight', title: '协议总间夜数', width: 150, templet: function (d) {
                    var res = d.settle_totalNight;
                    if (res == null || res == '' || res == undefined) {
                        return '-'
                    } else {
                        return d.settle_totalNight;
                    }
                }
            },
            {field: 'remark', title: '备注', width: 100},
            {
                field: 'hotel_supplier_state', title: '状态', width: 100, templet: function (d) {
                    var res = d.hotel_supplier_state;
                    // 0--待上线;1--在线;2--下线
                    if (res == null || res === '' || res == undefined) {
                        return '-'
                    } else if (res == 0) {
                        return '待上线'
                    } else if (res == 1) {
                        return '在售'
                    } else if (res == 2) {
                        return '已下线'
                    }
                }
            },
            // {field: 'operation', title: '操作', width: 160, toolbar: '#operation'}
            {field: 'operation', title: '操作', width: 200, templet: function (d) {
                    // console.log(d)
                    var temp = JSON.stringify(d).replace(/"/g, '&quot;');
                    var state = d.hotel_supplier_state;
                    var html = '';
                    if(state == 0){
                        html += '<a class="layui-btn layui-btn-xs" lay-event="update" onclick="update(' + temp + ')">修改</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del" onclick="deleted(' + temp + ')">删除</a>' +
                            '<a class="layui-btn layui-btn-xs" lay-event="online" onclick="online(' + temp + ')">上线</a>'
                    }else if(state == 1){
                        html += '<a class="layui-btn layui-btn-xs" lay-event="update" onclick="update(' + temp + ')">修改</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del" onclick="deleted(' + temp + ')">删除</a>' +
                            '<a class="layui-btn layui-btn-xs" lay-event="underline" onclick="underline(' + temp + ')">下线</a>'
                    }else if(state == 2){
                        html += '<a class="layui-btn layui-btn-xs" lay-event="update" onclick="update(' + temp + ')">修改</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del" onclick="deleted(' + temp + ')">删除</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-disabled" style="color: gray !important;" lay-event="online">上线</a>'
                    }else{
                        html += '<a class="layui-btn layui-btn-xs" lay-event="update" onclick="update(' + temp + ')">修改</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del" onclick="deleted(' + temp + ')">删除</a>' +
                            '<a class="layui-btn layui-btn-xs" lay-event="online" onclick="online(' + temp + ')">上线</a>'
                    }
                    return html
                }}
        ]],
        parseData: function (res) {
            // console.log(res)
        }
    });
    table.on('toolbar(associated-table)', function (obj) {
        // console.log(obj)
        $("#add-form .hotelId").val(hotelId);
        $.ajax({
            type: 'GET',
            url: '/supplier/simpleList',
            // data: JSON.stringify(arr[0]),
            contentType: 'application/json; charset=UTF-8',
            success: function (res) {
                // console.log(res);
                var html = '';
                $.each(res.data, function (i, val) {
                    html += '<option value="' + i + '">' + val + '</option>'
                });
                $("#supplier select").html(html)
                form.render()
            }
        });
        var layEvent = obj.event;
        if (layEvent == 'addAssociated') {
            layer.open({
                type: 1,
                title: ['添加关联供应商', 'font-size: 18px'],
                area: ['500px'],
                shadeClose: false,
                skin: 'to-fix-select',
                btn: ['提交', '取消'],
                btnAlign: 'c',
                offset: 'auto',
                content: $('#add-associated'),
                scrollbar: false,
                success: function (layero, index) {
                    // form.render();
                    $(".layui-layer-btn .layui-layer-btn0").css({
                        'color': '#fff'
                    })
                },
                yes: function (index, layero) {
                    // console.log('yes');
                    var forms = $("#add-form");
                    $.ajax({
                        type: forms.attr('method'),
                        url: forms.attr('action'),
                        data: forms.serialize(),
                        success: function (res) {
                            // console.log(res)
                            if(res.code == 0){
                                layer.close(index);
                                layer.msg(res.message);
                                table.reload('associated-table', {
                                    page: {
                                        curr: 1
                                    }
                                })
                            }else{
                                layer.msg(res.message);
                            }
                        },
                        error: function (res) {
                            layer.msg('系统错误,请重试!')
                        }
                    })
                    // $("#add-form")[0].reset();
                    // form.render();
                    // layer.close(index);
                },
                btn2: function (index, layero) {
                    // console.log('no')
                    $("#add-form")[0].reset();
                    form.render();
                    layer.close(index);
                },
                cancel: function (index, layero) {
                    $("#add-form")[0].reset();
                    form.render();
                    layer.close(index);
                }
            })
        }
    });
    form.on('select(changeType)', function (obj) {
        var val= obj.value;
        if(val == 0){
            $("#amount").show();
            $("#night").hide();
        }else if(val == 1){
            $("#amount").hide();
            $("#night").show();
        }
    })
});
//修改
function update(data) {
    layui.use(['layer', 'form', 'table'], function () {
        var layer = layui.layer,
            form = layui.form,
            table = layui.table;
        // console.log(data);
        $("#update-info .hotel-supplier-id").val(data.hotel_supplier_id);
        $("#update-info .supplier-input").val(data.business_name);
        $("#update-info .commission-rate").val(data.commission_rate);
        $("#update-info .amount input").val(data.settle_totalMoney);
        $("#update-info .night input").val(data.settle_totalNight);
        $("#update-info .remark").val(data.remark);
        var changeType = data.settle_type;
        // console.log(changeType);
        if(changeType == 0) {
            $("#update-info .amount").show();
            $("#update-info .night").hide();
        }else if(changeType == 1){
            $("#update-info .amount").hide();
            $("#update-info .night").show()
        }
        $("#changeType").val(changeType);
        form.render();
        form.on('select(changeType_2)', function (obj) {
            // console.log(obj)
            var val= obj.value;
            if(val == 0){
                $("#update-info .amount").show();
                $("#update-info .night").hide();
            }else if(val == 1){
                $("#update-info .amount").hide();
                $("#update-info .night").show()
            }
        });
        layer.open({
            type: 1,
            title: ['修改关联供应商', 'font-size: 18px'],
            area: ['500px'],
            shadeClose: false,
            btn: ['提交', '取消'],
            btnAlign: 'c',
            offset: 'auto',
            scrollbar: false,
            content: $('#update-info'),
            success: function (layero, index) {
                // console.log('success')
                $(".layui-layer-btn .layui-layer-btn0").css({
                    'color': '#fff'
                })
            },
            yes: function (index, layero) {
                console.log('yes')
                var forms = $("#update-form");
                $.ajax({
                    type: forms.attr('method'),
                    url: forms.attr('action'),
                    data: forms.serialize(),
                    success: function (res) {
                        // console.log(res)
                        if(res.code == 0){
                            layer.close(index);
                            layer.msg(res.message);
                            table.reload('associated-table', {
                                page: {
                                    curr: 1
                                }
                            })
                        }else{
                            layer.msg(res.message);
                        }
                    },
                    error: function (res) {
                        layer.msg(res.statusText)
                    }
                })
                // $("#add-form")[0].reset();
                // form.render();
                // layer.close(index);
            },
            btn2: function (index, layero) {
                // console.log('no');
                $("#update-form")[0].reset();
                form.render();
                layer.close(index);
            },
            cancel: function (index, layero) {
                // console.log('cancel')
                $("#update-form")[0].reset();
                form.render();
                layer.close(index);
            }
        })
    });
}
//删除
function deleted(data) {
    // console.log(data);
    var id = data.hotel_supplier_id;
    layui.use(['layer', 'table', 'form'], function () {
        var layer = layui.layer;
        var table = layui.table;
        var form = layui.form;
        layer.open({
            type: 1,
            title: ['提示', 'font-size: 18px'],
            area: ['500px'],
            shadeClose: false,
            btn: ['是的，我要删除！', '不，我点错了。'],
            btnAlign: 'c',
            offset: 'auto',
            scrollbar: false,
            content: '是否删除此条数据？',
            success: function (layero, index) {
                // console.log('success')
                addIcon();
            },
            yes: function (index, layero) {
                // console.log('yes')
                layer.open({
                    type: 1,
                    title: ['特别提示', 'font-size: 18px'],
                    area: ['500px'],
                    shadeClose: false,
                    btn: ['确认', '取消'],
                    btnAlign: 'c',
                    offset: 'auto',
                    scrollbar: false,
                    content: '事关重大！确认删除此条数据？',
                    success: function (layero, index) {
                        $(".layui-layer-btn .layui-layer-btn0").css({
                            'color': '#fff'
                        })
                    },
                    yes: function (index, layero) {
                        $.ajax({
                            type: 'POST',
                            url: '/hotelSupplier/delete',
                            data: {
                                hotel_supplier_id: id
                            },
                            success: function (res) {
                                // console.log(res);
                                if(res.code == 0){
                                    layer.closeAll();
                                    layer.msg(res.message);
                                    table.reload('associated-table', {
                                        page: {
                                            curr: 1
                                        }
                                    })
                                }else{
                                    layer.msg(res.message)
                                }
                            }
                        });
                    },
                    btn2: function (index, layero) {
                        // console.log('no2');
                        layer.closeAll();
                    }
                })
            },
            btn2: function (index, layero) {
                // console.log('no')
            }
        })
    });
}
//上线
function online(data) {
    // console.log(data);
    var id = data.hotel_supplier_id,
        name = data.business_name,
        state = data.hotel_supplier_state;
    layui.use(['layer', 'table', 'form'], function () {
        var layer = layui.layer;
        var table = layui.table;
        var form = layui.form;
        layer.open({
            type: 1,
            title: ['提示', 'font-size: 18px'],
            area: ['500px'],
            shadeClose: false,
            btn: ['确定', '取消'],
            btnAlign: 'c',
            offset: 'auto',
            scrollbar: false,
            content: '是否确定与<' + name +'>的合作上线？',
            success: function (layero, index) {
                // console.log('success')
                $(".layui-layer-btn .layui-layer-btn0").css({
                    'color': '#fff'
                })
            },
            yes: function (index, layero) {
                // console.log('yes');
                $.ajax({
                    type: 'POST',
                    url: '/hotelSupplier/updateHotelSupplierState',
                    data: {
                        hotel_supplier_id: id,
                        hotel_supplier_state: state
                    },
                    success: function (res) {
                        // console.log(res);
                        if(res.code == 0){
                            layer.closeAll();
                            layer.msg(res.message);
                            table.reload('associated-table', {
                                page: {
                                    curr: 1
                                }
                            })
                        }else{
                            layer.msg(res.message)
                        }
                    }
                });
            },
            btn2: function (index, layero) {
                // console.log('no')
            }
        })
    })
}

//下线
function underline(data) {
    // console.log(data);
    var id = data.hotel_supplier_id,
        name = data.business_name,
        state = data.hotel_supplier_state;
    layui.use(['layer', 'table', 'form'], function () {
        var layer = layui.layer;
        var table = layui.table;
        var form = layui.form;
        layer.open({
            type: 1,
            title: ['提示', 'font-size: 18px'],
            area: ['500px'],
            shadeClose: false,
            btn: ['确定', '取消'],
            btnAlign: 'c',
            offset: 'auto',
            scrollbar: false,
            content: '是否确定下线与<' + name +'>的合作？',
            // content: '是否确定下线<'+ name +'>?',
            success: function (layero, index) {
                // console.log('success')
                $(".layui-layer-btn .layui-layer-btn0").css({
                    'color': '#fff'
                })
            },
            yes: function (index, layero) {
                // console.log('yes');
                $.ajax({
                    type: 'POST',
                    url: '/hotelSupplier/updateHotelSupplierState',
                    data: {
                        hotel_supplier_id: id,
                        hotel_supplier_state: state
                    },
                    success: function (res) {
                        // console.log(res);
                        if(res.code == 0){
                            layer.closeAll();
                            layer.msg(res.message);
                            table.reload('associated-table', {
                                page: {
                                    curr: 1
                                }
                            })
                        }else{
                            layer.msg(res.message)
                        }
                    }
                });
            },
            btn2: function (index, layero) {
                // console.log('no')
            }
        })
    })
}
//获得年月日      得到日期oTime
function getMyDate(str) {
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth() + 1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        //最后拼接时间
        oTime = oYear + '-' + getzf(oMonth) + '-' + getzf(oDay) + ' ' + getzf(oHour) + ':' + getzf(oMin) + ':' + getzf(oSen);
    return oTime;
}

//补0操作
function getzf(num) {
    if (parseInt(num) < 10) {
        num = '0' + num;
    }
    return num;
}

//修改删除弹窗按钮样式
function addIcon() {
    var btn1 = $(".layui-layer-btn .layui-layer-btn0"),
        btn2 = $(".layui-layer-btn .layui-layer-btn1");
    btn2.css({
        'border-color': '#1E9FFF',
        'background-color': '#1E9FFF',
        'color': '#fff'
    });
    btn1.css({
        'border': '1px solid #dedede',
        'background-color': '#fff',
        'color': '#333'
    })
}