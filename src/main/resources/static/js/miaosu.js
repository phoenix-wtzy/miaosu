layui.use('carousel', function(){
    var carousel = layui.carousel;
    //建造实例
    carousel.render({
        elem: '#notice-carousel',
        width: '700px', //设置容器宽度,
        height: '45px',
        autoplay: true,
        interval: 3000,
        arrow: 'hover',
        indicator: 'none'
    });
});
$(".item-click").on('click', function () {
    var _index = $(this).attr('state');
    console.log(_index);
    if(_index == 0){
        layer.open({
            type: 1,
            title: ['新手须知', 'font-size: 20px'],
            area: ['80%', '80%'],
            shadeClose: false,
            btn: ['关闭'],
            btnAlign: 'c',
            offset: 'auto',
            // content: $("#notice0"),
            content: '<div id="notice0" style="width: 100%;height: auto;overflow: hidden"><img style="width: 100%;" src="http://qiniu.miaosu.ink/notice0.jpg" alt="" /></div>',
            scrollbar: false,
            yes: function (index, layero) {
                console.log('yes');
                layer.close(index);
            }
        })
    }
    else if(_index == 1){
        layer.open({
            type: 1,
            title: ['业务流程', 'font-size: 20px'],
            area: ['80%', '80%'],
            shadeClose: false,
            btn: ['关闭'],
            btnAlign: 'c',
            offset: 'auto',
            // content: $("#notice1"),
            content: '<div id="notice1" style="width: 100%;height: auto;overflow: hidden"><img style="width: 100%;" src="http://qiniu.miaosu.ink/notice1.jpg" alt="" /></div>',
            scrollbar: false,
            yes: function (index, layero) {
                console.log('yes');
                layer.close(index);
            }
        })
    }
});


function get_today_date() {
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var output = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
    return output;
}


function format_date(date) {
    var d = new Date(date);
    var month = d.getMonth()+1;
    var day = d.getDate();
    var output = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
    return output;
}

function format_date_time(date) {
    var d = new Date(date);
    var month = d.getMonth()+1;
    var day = d.getDate();
    var curr_hour = d.getHours();
    var curr_min = d.getMinutes();
    var curr_sec = d.getSeconds();
    var output = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
    output = output +' '+ (curr_hour<10 ? '0' : '') + curr_hour+':'+(curr_min<10 ? '0' : '') + curr_min+':'+(curr_sec<10 ? '0' : '') + curr_sec;
    return output;
}

function format_date_md(date) {
    var d = new Date(date);
    var month = d.getMonth()+1;
    var day = d.getDate();
    var output = (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
    return output;
}

