#!/bin/bash
    PID=$(ps -ef | grep miaosu-hotel-gds | grep -v grep | awk '{ print $2 }')
    if [ -z "$PID" ]
    then
        echo Application is already stopped
    else
        echo kill $PID
        kill -9 $PID
    fi
