-- CREATE SCHEMA `m_fx` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;


CREATE TABLE user (
user_id BIGINT  NOT NULL AUTO_INCREMENT COMMENT '递增主键',
user_name VARCHAR(50) NOT NULL COMMENT '用户名',
user_nick VARCHAR (50) NOT NULL COMMENT '用户昵称',
user_passwd VARCHAR(50) NOT NULL COMMENT '密码',
status INT NOT NULL DEFAULT 0 COMMENT '状态 0正常 1停用',
PRIMARY KEY (user_id),
UNIQUE INDEX uniq_idx_uname (user_name)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';


CREATE TABLE role (
role_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '递增主键',
role_name VARCHAR (100) NOT NULL COMMENT '权限名称',
PRIMARY KEY (role_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';


create table user_role (
  user_id BIGINT UNSIGNED not null COMMENT '用户ID',
  role_id BIGINT UNSIGNED not null COMMENT '权限ID',
  UNIQUE INDEX uniq_idx (user_id,role_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户权限关系表';

create table user_setting (
  user_id BIGINT  not null COMMENT '用户ID',
  setting_type INT  not null COMMENT '设置类型',
  setting_value BIGINT not null COMMENT '设置值',
  UNIQUE INDEX uniq_idx (user_id,setting_type)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户配置表';

CREATE TABLE `hotel_info` (
  `p_hotel_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '递增主键',
  `hotel_name` varchar(50) NOT NULL COMMENT '酒店名称',
  `address` varchar(120) NOT NULL COMMENT '酒店地址',
  `contact` varchar(50) NOT NULL COMMENT '联系人',
  `contact_mobile` varchar(50) NOT NULL COMMENT '联系人手机号',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '关联用户ID',
  PRIMARY KEY (`p_hotel_id`),
  UNIQUE KEY `uniq_idx_hotel_name` (`hotel_name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='酒店信息';


CREATE TABLE `hotel_ref_user` (
  `hotel_id` bigint(20) NOT NULL COMMENT '酒店ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  UNIQUE KEY `idx_h_u` (`hotel_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='酒店用户关系';


CREATE TABLE `hotel_room_type` (
  `room_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '房型ID',
  `room_type_name` varchar(20) DEFAULT NULL COMMENT '房型名称',
  PRIMARY KEY (`room_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='房型信息';


CREATE TABLE `hotel_room_set` (
  `hotel_room_type_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '递增主键',
  `hotel_room_type_name` varchar(20) DEFAULT NULL COMMENT '房型自定义名称',
  `room_type_id` int(10) unsigned NOT NULL COMMENT '房型ID',
  `hotel_id` bigint(20) unsigned NOT NULL COMMENT '所属酒店id',
  PRIMARY KEY (`hotel_room_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='酒店房型设置';


CREATE TABLE `hotel_price_set` (
  `hotel_price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '递增主键',
  `hotel_price_name` varchar(20) DEFAULT NULL COMMENT '价格自定义名称',
  `hotel_id` bigint(20) unsigned NOT NULL COMMENT '所属酒店id',
  `breakfast_num` int(11) NOT NULL COMMENT '早餐份数',
  `unbook_type` int(11) NOT NULL COMMENT '退订类型0,1',
  PRIMARY KEY (`hotel_price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='酒店价格设置';



CREATE TABLE `hotel_price_ref_room_type` (
  `hotel_price_id` bigint(20) unsigned NOT NULL COMMENT '酒店价格设置',
  `hotel_room_type_id` bigint(20) unsigned NOT NULL COMMENT '价格关联自定义房型',
  UNIQUE KEY `uniq_idx_1` (`hotel_price_id`,`hotel_room_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='酒店价格关联房型设置';



CREATE TABLE `hotel_order` (
  `order_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '递增主键',
  `order_no` varchar(30) NOT NULL COMMENT '订单号',
  `order_channel` int(11) NOT NULL COMMENT '渠道',
  `sell_type` int(11) NOT NULL COMMENT '销售类型：置换/代销',
  `book_time` datetime NOT NULL COMMENT '预定时间',
  `book_user` varchar(10) DEFAULT '' COMMENT '预定人',
  `book_mobile` varchar(20) DEFAULT '' COMMENT '联系人',
  `hotel_id` bigint(20) NOT NULL COMMENT '酒店ID',
  `hotel_room_type_id` bigint(20) NOT NULL COMMENT '房型',
  `room_count` int(11) NOT NULL DEFAULT '1' COMMENT '房间数',
  `check_in_time` datetime NOT NULL COMMENT '入住时间',
  `check_out_time` datetime NOT NULL COMMENT '离店时间',
  `price` decimal(8,2) NOT NULL COMMENT '价格',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '订单状态',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `uniq_idx_order` (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=180000016 DEFAULT CHARSET=utf8 COMMENT='订单信息';



CREATE TABLE `hotel_order_notify` (
  `order_id` bigint(20) unsigned NOT NULL COMMENT '订单号',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '通知状态0待通知，1已通知到',
  `notify_type` int(11) DEFAULT NULL COMMENT '通知类型0web 1wechat 2mobile',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单通知';

CREATE TABLE `hotel_room_status` (
  `hotel_id` bigint(20) NOT NULL COMMENT '酒店ID',
  `hotel_room_type_id` bigint(20)  NOT NULL COMMENT '价格类型',
  `sell_date` date NOT NULL COMMENT '售卖时间',
  `create_time` datetime NOT NULL DEFAULT current_timestamp COMMENT '创建时间',
  `status` int(11) NOT NULL COMMENT '状态0：关，1：开',
  UNIQUE KEY `uniq_idx_1` (`hotel_room_type_id`,`sell_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房态';


CREATE TABLE `hotel_room_price` (
  `hotel_room_type_id` bigint(20) unsigned NOT NULL COMMENT '价格类型',
  `sell_date` date NOT NULL COMMENT '售卖时间',
  `price` decimal(4,2) NOT NULL COMMENT '价格',
  UNIQUE KEY `uniq_idx_1` (`hotel_room_type_id`,`sell_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房价';


create table op_log (
  log_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '递增主键',
  user_id BIGINT NOT NULL COMMENT '用户ID',
  `create_time` datetime NOT NULL DEFAULT current_timestamp COMMENT '创建时间',
  PRIMARY KEY (log_id)
);



-- -------------初始化数据-------------
-- 权限初始化
INSERT INTO role(role_id,role_name) VALUE (1,"ROLE_SUPER");
INSERT INTO role(role_id,role_name) VALUE (2,"ROLE_ADMIN");
INSERT INTO role(role_id,role_name) VALUE (3,"ROLE_USER");
-- 添加用户
INSERT INTO user(user_id,user_name,user_nick,user_passwd,status) VALUES (1,'admin_hainan','管理员1','54321',0);
# INSERT INTO user(user_id,user_name,user_nick,user_passwd,status) VALUES (2,'user','用户1','12345',0);
-- 添加用户权限
INSERT INTO user_role(user_id,role_id) VALUES (1,1);
INSERT INTO user_role(user_id,role_id) VALUES (1,2);

# INSERT INTO user(user_id,user_name,user_nick,user_passwd,status) VALUES (2,'cuiqc','管理员2','54321',0);
# INSERT INTO user_role(user_id,role_id) VALUES (2,2);
# INSERT INTO user(user_id,user_name,user_nick,user_passwd,status) VALUES (3,'zengzx','曾子轩','54321',0);
# INSERT INTO user_role(user_id,role_id) VALUES (3,2);
INSERT INTO user(user_id,user_name,user_nick,user_passwd,status) VALUES (4,'admin_yunnan','云南管理员','8768909765',0);
INSERT INTO user_role(user_id,role_id) VALUES (4,2);


INSERT INTO user(user_id,user_name,user_nick,user_passwd,status) VALUES (5,'admin_yuanqi','郑州源启','8368909066',0);
INSERT INTO user_role(user_id,role_id) VALUES (5,2);


-- 房型初始化
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (1,'标准双床房');
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (2,'标准大床房');
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (3,'床位房');
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (4,'单人间');
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (5,'家庭房');
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (6,'三人间');
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (7,'套房');
INSERT INTO hotel_room_type(room_type_id,room_type_name) VALUES (8,'其他');

# INSERT INTO user(user_id,user_name,user_nick,user_passwd,status) VALUES (1,'cuiqc','管理员2','54321',0);
# INSERT INTO user_role(user_id,role_id) VALUES (2,2);

-- 修改
ALTER TABLE `hotel_order`
  MODIFY COLUMN `book_user` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '预定人' AFTER `book_time`;

ALTER TABLE `hotel_order`
  ADD COLUMN `book_remark` varchar(100) CHARACTER SET utf8 NULL DEFAULT '' COMMENT '备注' AFTER `state`;

-- ---------------------- 2018-08-08 数据库修改 start------------------------------
#第一种方式
DROP TABLE IF EXISTS `hotel_room_status`;
CREATE TABLE `hotel_room_status` (
  `hotel_id` bigint(20) NOT NULL COMMENT '酒店ID',
  `hotel_room_type_id` bigint(20) DEFAULT NULL COMMENT '价格类型',
  `sell_date` date NOT NULL COMMENT '售卖时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) NOT NULL COMMENT '状态0：关，1：开',
  `product_id` bigint(20) NOT NULL COMMENT '产品id',
  UNIQUE KEY `uniq_idx_1` (`product_id`,`sell_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房态';


#第二种方式
#添加字段productId
#ALTER TABLE `hotel_room_status`
#  ADD COLUMN
#    `product_id` bigint(20) NOT NULL COMMENT '产品id';

#先删除索引
#DROP INDEX `uniq_idx_1` on `hotel_room_status`;
#创建索引
#CREATE UNIQUE INDEX `uniq_idx_1` USING  BTREE ON `hotel_room_status`(`sell_date`,`product_id`);




#更新价格 添加其与产品进行关联
DROP TABLE IF EXISTS `hotel_room_price`;
CREATE TABLE `hotel_room_price` (
  `product_id` bigint(20) NOT NULL,
  `sell_date` date NOT NULL COMMENT '售卖时间',
  `price` decimal(7,2) NOT NULL COMMENT '价格',
  UNIQUE KEY `uniq_idx_1` (`sell_date`,`product_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房价';



#新增房量
CREATE TABLE `hotel_room_count` (
  `hotel_room_type_id` bigint(20) unsigned NOT NULL COMMENT '房型类型',
  `sell_date` date NOT NULL COMMENT '售卖时间',
  `count_room` int(11) NOT NULL COMMENT '房间数量',
  UNIQUE KEY `uniq_idx_1` (`hotel_room_type_id`,`sell_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房量';


#更改酒店计划价格和房型信息 添加产品id
ALTER TABLE `hotel_price_ref_room_type`
  ADD COLUMN `product_id` bigint(20)  PRIMARY KEY AUTO_INCREMENT COMMENT '产品id' FIRST;



-- ---------------------- 2018-08-08 数据库修改 end----------------------------------


-- ---------------------- 2018-08-10 新增数据库酒店的扩展类省市县信息 start --------------------
CREATE TABLE `hotel_info_ext` (

`hotel_id`  bigint(20) NOT NULL COMMENT '酒店id' ,
`province_code`  varchar(25)  NOT NULL COMMENT '省份id' ,
`city_code`  varchar(25)  NOT NULL COMMENT '城市id' ,
`district_code`  varchar(25)  NOT NULL COMMENT '区域县级id' ,
`channel`  int(2) NOT NULL COMMENT '城市信息所属方 0 本地省市县 1 去呼呼  其他可扩展' ,
UNIQUE INDEX `uniq_idx_1` (`hotel_id`, `channel`) USING BTREE
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='酒店信息扩展表'
ROW_FORMAT=DYNAMIC
;



-- ----------------------- 2018-08-10 新增数据库酒店的扩展类省市县信息 end ---------------------



-- -------------------------重构 省市县信息----------------------------------------
#新增省市县
CREATE TABLE `province` (
`code`  varchar(25) NOT NULL COMMENT '省份编码' ,
`name`  varchar(30) NOT NULL COMMENT '省份名称' ,
`country_code`  varchar(25)  NOT NULL COMMENT '国家编码' ,
PRIMARY KEY (`code`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='省份表'
ROW_FORMAT=DYNAMIC
;



CREATE TABLE `city` (
`code`  varchar(25)  NOT NULL COMMENT '城市编码' ,
`name`  varchar(30)  NOT NULL COMMENT '城市名称' ,
`province_code`  varchar(25)  NOT NULL COMMENT '省份编码' ,
PRIMARY KEY (`code`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='城市表'
ROW_FORMAT=DYNAMIC
;



CREATE TABLE `district` (
`code`  varchar(25)  NOT NULL COMMENT '区域编码' ,
`name`  varchar(30)  NOT NULL COMMENT '区域名称' ,
`city_code`  varchar(25)  NOT NULL COMMENT '所属城市' ,
PRIMARY KEY (`code`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='区域表'
ROW_FORMAT=DYNAMIC
;


#新增省市县
CREATE TABLE `ota_province` (
`code`  varchar(25) NOT NULL COMMENT '省份编码' ,
`name`  varchar(30) NOT NULL COMMENT '省份名称' ,
`country_code`  varchar(25)  NOT NULL COMMENT '渠道国家编码' ,
`channel`  int(2) NOT NULL COMMENT '城市信息所属方 1 去呼呼  其他可扩展' ,
`province_code`  varchar(25) NOT NULL COMMENT '本地省与渠道省的对应' ,
 UNIQUE INDEX `uniq_idx_1` (`code`,`channel`) USING BTREE,
PRIMARY KEY (`code`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='省份表'
ROW_FORMAT=DYNAMIC
;



CREATE TABLE `ota_city` (
`code`  varchar(25)  NOT NULL COMMENT '城市编码' ,
`name`  varchar(30)  NOT NULL COMMENT '城市名称' ,
`province_code`  varchar(25)  NOT NULL COMMENT '省份编码' ,
`channel`  int(2) NOT NULL COMMENT '城市信息所属方 1 去呼呼  其他可扩展' ,
`city_code`  varchar(25) NOT NULL COMMENT '本地市与渠道市的对应' ,
 UNIQUE INDEX `uniq_idx_1` (`code`,`channel`) USING BTREE,
PRIMARY KEY (`code`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='城市表'
ROW_FORMAT=DYNAMIC
;



CREATE TABLE `ota_district` (
`code`  varchar(25)  NOT NULL COMMENT '区域编码' ,
`name`  varchar(30)  NOT NULL COMMENT '区域名称' ,
`city_code`  varchar(25)  NOT NULL COMMENT '所属城市' ,
`channel`  int(2) NOT NULL COMMENT '城市信息所属方 1 去呼呼  其他可扩展' ,
`district_code`  varchar(25) NOT NULL COMMENT '本地区与渠道区的对应' ,
 UNIQUE INDEX `uniq_idx_1` (`code`,`channel`) USING BTREE,
PRIMARY KEY (`code`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='区域表'
ROW_FORMAT=DYNAMIC
;



-- -------------------------------重构 省市县信息-------------------------------------

-- --------------------------添加渠道房型和本地房型的关联关系   start-------------------------
#添加关联关系字段ota_room_type_id
ALTER table hotel_room_type add column ota_room_type_id int(10) not null DEFAULT 0 comment "渠道和本地的房型匹配"

UPDATE  hotel_room_type set ota_room_type_id = 0 WHERE  room_type_id =2;
UPDATE  hotel_room_type set ota_room_type_id = 1 WHERE  room_type_id =1;
UPDATE  hotel_room_type set ota_room_type_id = 3 WHERE  room_type_id =6;
UPDATE  hotel_room_type set ota_room_type_id = 5 WHERE  room_type_id =4;
UPDATE  hotel_room_type set ota_room_type_id = 99 WHERE  room_type_id =8;

#添加新的房型信息
INSERT INTO hotel_room_type(room_type_id,room_type_name,ota_room_type_id) values(9,"上下铺",6);
INSERT INTO hotel_room_type(room_type_id,room_type_name,ota_room_type_id) values(10,"通铺",7);
INSERT INTO hotel_room_type(room_type_id,room_type_name,ota_room_type_id) values(11,"大/双床",2);
INSERT INTO hotel_room_type(room_type_id,room_type_name,ota_room_type_id) values(12,"一单一双",4);
INSERT INTO hotel_room_type(room_type_id,room_type_name,ota_room_type_id) values(13,"榻榻米",8);
INSERT INTO hotel_room_type(room_type_id,room_type_name,ota_room_type_id) values(14,"水床",9);
INSERT INTO hotel_room_type(room_type_id,room_type_name,ota_room_type_id) values(15,"圆床",10);

-- -------------------------添加渠道房型和本地房型的关联关系 end-------------------------------


-- --------------------------初始化省市先区域信息 end-----------------------------------------
#本地省市区信息
#省份信息
INSERT INTO `province` VALUES ('103', '吉林', 'china');
INSERT INTO `province` VALUES ('12', '北京', 'china');
INSERT INTO `province` VALUES ('125', '黑龙江', 'china');
INSERT INTO `province` VALUES ('13', '天津', 'china');
INSERT INTO `province` VALUES ('14', '上海', 'china');
INSERT INTO `province` VALUES ('191', '江苏', 'china');
INSERT INTO `province` VALUES ('192', '浙江', 'china');
INSERT INTO `province` VALUES ('193', '安徽', 'china');
INSERT INTO `province` VALUES ('194', '福建', 'china');
INSERT INTO `province` VALUES ('195', '江西', 'china');
INSERT INTO `province` VALUES ('196', '山东', 'china');
INSERT INTO `province` VALUES ('197', '河南', 'china');
INSERT INTO `province` VALUES ('198', '湖北', 'china');
INSERT INTO `province` VALUES ('199', '湖南', 'china');
INSERT INTO `province` VALUES ('200', '广东', 'china');
INSERT INTO `province` VALUES ('201', '甘肃', 'china');
INSERT INTO `province` VALUES ('202', '四川', 'china');
INSERT INTO `province` VALUES ('203', '贵州', 'china');
INSERT INTO `province` VALUES ('204', '海南', 'china');
INSERT INTO `province` VALUES ('205', '云南', 'china');
INSERT INTO `province` VALUES ('206', '青海', 'china');
INSERT INTO `province` VALUES ('207', '陕西', 'china');
INSERT INTO `province` VALUES ('208', '台湾', 'china');
INSERT INTO `province` VALUES ('209', '广西', 'china');
INSERT INTO `province` VALUES ('210', '西藏', 'china');
INSERT INTO `province` VALUES ('211', '宁夏', 'china');
INSERT INTO `province` VALUES ('212', '新疆', 'china');
INSERT INTO `province` VALUES ('213', '内蒙古', 'china');
INSERT INTO `province` VALUES ('214', '香港', 'china');
INSERT INTO `province` VALUES ('215', '澳门', 'china');
INSERT INTO `province` VALUES ('26', '重庆', 'china');
INSERT INTO `province` VALUES ('59', '河北', 'china');
INSERT INTO `province` VALUES ('74', '山西', 'china');
INSERT INTO `province` VALUES ('96', '辽宁', 'china');


#城市信息
INSERT INTO `city` VALUES ('aba', '阿坝', '202');
INSERT INTO `city` VALUES ('akesu', '阿克苏', '212');
INSERT INTO `city` VALUES ('alaer', '阿拉尔', '212');
INSERT INTO `city` VALUES ('alashan', '阿拉善盟', '213');
INSERT INTO `city` VALUES ('aletai', '阿勒泰', '212');
INSERT INTO `city` VALUES ('ali', '阿里', '210');
INSERT INTO `city` VALUES ('ankang', '安康', '207');
INSERT INTO `city` VALUES ('anqing', '安庆', '193');
INSERT INTO `city` VALUES ('anshan', '鞍山', '96');
INSERT INTO `city` VALUES ('anshun', '安顺', '203');
INSERT INTO `city` VALUES ('anyang', '安阳', '197');
INSERT INTO `city` VALUES ('baicheng', '白城', '103');
INSERT INTO `city` VALUES ('baise', '百色', '209');
INSERT INTO `city` VALUES ('baisha', '白沙', '204');
INSERT INTO `city` VALUES ('baishan', '白山', '103');
INSERT INTO `city` VALUES ('baiyin', '白银', '201');
INSERT INTO `city` VALUES ('bangbu', '蚌埠', '193');
INSERT INTO `city` VALUES ('baoding', '保定', '59');
INSERT INTO `city` VALUES ('baoji', '宝鸡', '207');
INSERT INTO `city` VALUES ('baoshan', '保山', '205');
INSERT INTO `city` VALUES ('baoting', '保亭', '204');
INSERT INTO `city` VALUES ('baotou', '包头', '213');
INSERT INTO `city` VALUES ('bayannaoer', '巴彦淖尔', '213');
INSERT INTO `city` VALUES ('bayinguoleng', '巴音郭楞', '212');
INSERT INTO `city` VALUES ('bazhong', '巴中', '202');
INSERT INTO `city` VALUES ('beihai', '北海', '209');
INSERT INTO `city` VALUES ('beijing_city', '北京', '12');
INSERT INTO `city` VALUES ('benxi', '本溪', '96');
INSERT INTO `city` VALUES ('bijie', '毕节', '203');
INSERT INTO `city` VALUES ('binzhou', '滨州', '196');
INSERT INTO `city` VALUES ('boertala', '博尔塔拉', '212');
INSERT INTO `city` VALUES ('bozhou', '亳州', '193');
INSERT INTO `city` VALUES ('cangzhou', '沧州', '59');
INSERT INTO `city` VALUES ('changchun', '长春', '103');
INSERT INTO `city` VALUES ('changde', '常德', '199');
INSERT INTO `city` VALUES ('changdu', '昌都', '210');
INSERT INTO `city` VALUES ('changji', '昌吉', '212');
INSERT INTO `city` VALUES ('changjiang', '昌江', '204');
INSERT INTO `city` VALUES ('changsha', '长沙', '199');
INSERT INTO `city` VALUES ('changzhi', '长治', '74');
INSERT INTO `city` VALUES ('changzhou', '常州', '191');
INSERT INTO `city` VALUES ('chang_hua', '彰化', '208');
INSERT INTO `city` VALUES ('chaohu', '巢湖', '193');
INSERT INTO `city` VALUES ('chaoyang', '朝阳', '96');
INSERT INTO `city` VALUES ('chaozhou', '潮州', '200');
INSERT INTO `city` VALUES ('chengde', '承德', '59');
INSERT INTO `city` VALUES ('chengdu', '成都', '202');
INSERT INTO `city` VALUES ('chengmai', '澄迈', '204');
INSERT INTO `city` VALUES ('chenzhou', '郴州', '199');
INSERT INTO `city` VALUES ('chiaohsi', '礁溪', '208');
INSERT INTO `city` VALUES ('chiayi', '嘉义', '208');
INSERT INTO `city` VALUES ('chifeng', '赤峰', '213');
INSERT INTO `city` VALUES ('chizhou', '池州', '193');
INSERT INTO `city` VALUES ('chongqing_city', '重庆', '26');
INSERT INTO `city` VALUES ('chongzuo', '崇左', '209');
INSERT INTO `city` VALUES ('chuxiong', '楚雄', '205');
INSERT INTO `city` VALUES ('chuzhou', '滁州', '193');
INSERT INTO `city` VALUES ('cingjing', '清境', '208');
INSERT INTO `city` VALUES ('dali', '大理', '205');
INSERT INTO `city` VALUES ('dalian', '大连', '96');
INSERT INTO `city` VALUES ('dandong', '丹东', '96');
INSERT INTO `city` VALUES ('danzhou', '儋州', '204');
INSERT INTO `city` VALUES ('daqing', '大庆', '125');
INSERT INTO `city` VALUES ('datong', '大同', '74');
INSERT INTO `city` VALUES ('daxinganling', '大兴安岭', '125');
INSERT INTO `city` VALUES ('dazhou', '达州', '202');
INSERT INTO `city` VALUES ('dehong', '德宏', '205');
INSERT INTO `city` VALUES ('deyang', '德阳', '202');
INSERT INTO `city` VALUES ('dezhou', '德州', '196');
INSERT INTO `city` VALUES ('dingan', '定安', '204');
INSERT INTO `city` VALUES ('dingxi', '定西', '201');
INSERT INTO `city` VALUES ('diqing', '迪庆', '205');
INSERT INTO `city` VALUES ('dongfang', '东方', '204');
INSERT INTO `city` VALUES ('dongguan', '东莞', '200');
INSERT INTO `city` VALUES ('dongying', '东营', '196');
INSERT INTO `city` VALUES ('eerduosi', '鄂尔多斯', '213');
INSERT INTO `city` VALUES ('enshi', '恩施自治州', '198');
INSERT INTO `city` VALUES ('ezhou', '鄂州', '198');
INSERT INTO `city` VALUES ('fangchenggang', '防城港', '209');
INSERT INTO `city` VALUES ('foshan', '佛山', '200');
INSERT INTO `city` VALUES ('fushun', '抚顺', '96');
INSERT INTO `city` VALUES ('fuxin', '阜新', '96');
INSERT INTO `city` VALUES ('fuyang_anhui', '阜阳', '193');
INSERT INTO `city` VALUES ('fuzhou_fujian', '福州', '194');
INSERT INTO `city` VALUES ('fuzhou_jiangxi', '抚州', '195');
INSERT INTO `city` VALUES ('gannan', '甘南', '201');
INSERT INTO `city` VALUES ('ganzhou', '赣州', '195');
INSERT INTO `city` VALUES ('ganzi', '甘孜', '202');
INSERT INTO `city` VALUES ('guangan', '广安', '202');
INSERT INTO `city` VALUES ('guangyuan', '广元', '202');
INSERT INTO `city` VALUES ('guangzhou', '广州', '200');
INSERT INTO `city` VALUES ('guigang', '贵港', '209');
INSERT INTO `city` VALUES ('guilin', '桂林', '209');
INSERT INTO `city` VALUES ('guiyang', '贵阳', '203');
INSERT INTO `city` VALUES ('guoluo', '果洛', '206');
INSERT INTO `city` VALUES ('guyuan', '固原', '211');
INSERT INTO `city` VALUES ('haerbin', '哈尔滨', '125');
INSERT INTO `city` VALUES ('haibei', '海北', '206');
INSERT INTO `city` VALUES ('haidong', '海东', '206');
INSERT INTO `city` VALUES ('haikou', '海口', '204');
INSERT INTO `city` VALUES ('hainanzangzu', '海南藏族自治州', '206');
INSERT INTO `city` VALUES ('haixi', '海西', '206');
INSERT INTO `city` VALUES ('hami', '哈密', '212');
INSERT INTO `city` VALUES ('handan', '邯郸', '59');
INSERT INTO `city` VALUES ('hangzhou', '杭州', '192');
INSERT INTO `city` VALUES ('hanzhong', '汉中', '207');
INSERT INTO `city` VALUES ('hebi', '鹤壁', '197');
INSERT INTO `city` VALUES ('hechi', '河池', '209');
INSERT INTO `city` VALUES ('hefei', '合肥', '193');
INSERT INTO `city` VALUES ('hegang', '鹤岗', '125');
INSERT INTO `city` VALUES ('heihe', '黑河', '125');
INSERT INTO `city` VALUES ('hengshui', '衡水', '59');
INSERT INTO `city` VALUES ('hengyang', '衡阳', '199');
INSERT INTO `city` VALUES ('hetian', '和田', '212');
INSERT INTO `city` VALUES ('heyuan', '河源', '200');
INSERT INTO `city` VALUES ('heze', '菏泽', '196');
INSERT INTO `city` VALUES ('hezhou', '贺州', '209');
INSERT INTO `city` VALUES ('honghe', '红河', '205');
INSERT INTO `city` VALUES ('hongkong_city', '香港', '214');
INSERT INTO `city` VALUES ('huaian', '淮安', '191');
INSERT INTO `city` VALUES ('huaibei', '淮北', '193');
INSERT INTO `city` VALUES ('huaihua', '怀化', '199');
INSERT INTO `city` VALUES ('huainan', '淮南', '193');
INSERT INTO `city` VALUES ('hualian', '花莲县', '208');
INSERT INTO `city` VALUES ('huanggang', '黄冈', '198');
INSERT INTO `city` VALUES ('huangnan', '黄南', '206');
INSERT INTO `city` VALUES ('huangshan', '黄山', '193');
INSERT INTO `city` VALUES ('huangshi', '黄石', '198');
INSERT INTO `city` VALUES ('huhehaote', '呼和浩特', '213');
INSERT INTO `city` VALUES ('huizhou_guangdong', '惠州', '200');
INSERT INTO `city` VALUES ('huludao', '葫芦岛', '96');
INSERT INTO `city` VALUES ('hulunbeier', '呼伦贝尔', '213');
INSERT INTO `city` VALUES ('huzhou', '湖州', '192');
INSERT INTO `city` VALUES ('jhongli', '中坜', '208');
INSERT INTO `city` VALUES ('jiamusi', '佳木斯', '125');
INSERT INTO `city` VALUES ('jian', '吉安', '195');
INSERT INTO `city` VALUES ('jiangmen', '江门', '200');
INSERT INTO `city` VALUES ('jiaozuo', '焦作', '197');
INSERT INTO `city` VALUES ('jiaxing', '嘉兴', '192');
INSERT INTO `city` VALUES ('jiayi', '嘉义县', '208');
INSERT INTO `city` VALUES ('jiayuguan', '嘉峪关', '201');
INSERT INTO `city` VALUES ('jieyang', '揭阳', '200');
INSERT INTO `city` VALUES ('jilin_city', '吉林', '103');
INSERT INTO `city` VALUES ('jinan', '济南', '196');
INSERT INTO `city` VALUES ('jinchang', '金昌', '201');
INSERT INTO `city` VALUES ('jincheng', '晋城', '74');
INSERT INTO `city` VALUES ('jingdezhen', '景德镇', '195');
INSERT INTO `city` VALUES ('jingmen', '荆门', '198');
INSERT INTO `city` VALUES ('jingzhou', '荆州', '198');
INSERT INTO `city` VALUES ('jinhua', '金华', '192');
INSERT INTO `city` VALUES ('jining', '济宁', '196');
INSERT INTO `city` VALUES ('jinmen', '金门县', '208');
INSERT INTO `city` VALUES ('jinzhong', '晋中', '74');
INSERT INTO `city` VALUES ('jinzhou', '锦州', '96');
INSERT INTO `city` VALUES ('jiujiang', '九江', '195');
INSERT INTO `city` VALUES ('jiuquan', '酒泉', '201');
INSERT INTO `city` VALUES ('jixi', '鸡西', '125');
INSERT INTO `city` VALUES ('jiyuan', '济源', '197');
INSERT INTO `city` VALUES ('kaifeng', '开封', '197');
INSERT INTO `city` VALUES ('kaohsiung', '高雄', '208');
INSERT INTO `city` VALUES ('kashi', '喀什', '212');
INSERT INTO `city` VALUES ('keelung', '基隆', '208');
INSERT INTO `city` VALUES ('kelamayi', '克拉玛依', '212');
INSERT INTO `city` VALUES ('kezilesukeerkezi', '克孜勒苏柯尔克孜', '212');
INSERT INTO `city` VALUES ('kunming', '昆明', '205');
INSERT INTO `city` VALUES ('laibin', '来宾', '209');
INSERT INTO `city` VALUES ('laiwu', '莱芜', '196');
INSERT INTO `city` VALUES ('langfang', '廊坊', '59');
INSERT INTO `city` VALUES ('lanzhou', '兰州', '201');
INSERT INTO `city` VALUES ('lasa', '拉萨', '210');
INSERT INTO `city` VALUES ('ledong', '乐东', '204');
INSERT INTO `city` VALUES ('leshan', '乐山', '202');
INSERT INTO `city` VALUES ('liangshan', '凉山', '202');
INSERT INTO `city` VALUES ('lianyungang', '连云港', '191');
INSERT INTO `city` VALUES ('liaocheng', '聊城', '196');
INSERT INTO `city` VALUES ('liaoyang', '辽阳', '96');
INSERT INTO `city` VALUES ('liaoyuan', '辽源', '103');
INSERT INTO `city` VALUES ('lijiang', '丽江', '205');
INSERT INTO `city` VALUES ('lincang', '临沧', '205');
INSERT INTO `city` VALUES ('linfen', '临汾', '74');
INSERT INTO `city` VALUES ('lingao', '临高', '204');
INSERT INTO `city` VALUES ('lingshui', '陵水', '204');
INSERT INTO `city` VALUES ('linxia', '临夏', '201');
INSERT INTO `city` VALUES ('linyi', '临沂', '196');
INSERT INTO `city` VALUES ('linzhi', '林芝', '210');
INSERT INTO `city` VALUES ('lishui', '丽水', '192');
INSERT INTO `city` VALUES ('liuan', '六安', '193');
INSERT INTO `city` VALUES ('liupanshui', '六盘水', '203');
INSERT INTO `city` VALUES ('liuzhou', '柳州', '209');
INSERT INTO `city` VALUES ('longnan', '陇南', '201');
INSERT INTO `city` VALUES ('longtan', '龙潭', '208');
INSERT INTO `city` VALUES ('longyan', '龙岩', '194');
INSERT INTO `city` VALUES ('lotung', '罗东', '208');
INSERT INTO `city` VALUES ('loudi', '娄底', '199');
INSERT INTO `city` VALUES ('luohe', '漯河', '197');
INSERT INTO `city` VALUES ('luoyang', '洛阳', '197');
INSERT INTO `city` VALUES ('luzhou', '泸州', '202');
INSERT INTO `city` VALUES ('lvliang', '吕梁', '74');
INSERT INTO `city` VALUES ('maanshan', '马鞍山', '193');
INSERT INTO `city` VALUES ('macao_city', '澳门', '215');
INSERT INTO `city` VALUES ('maoming', '茂名', '200');
INSERT INTO `city` VALUES ('meishan', '眉山', '202');
INSERT INTO `city` VALUES ('meizhou', '梅州', '200');
INSERT INTO `city` VALUES ('mianyang', '绵阳', '202');
INSERT INTO `city` VALUES ('miaoli_xian', '苗栗县', '208');
INSERT INTO `city` VALUES ('mudanjiang', '牡丹江', '125');
INSERT INTO `city` VALUES ('nanchang', '南昌', '195');
INSERT INTO `city` VALUES ('nanchong', '南充', '202');
INSERT INTO `city` VALUES ('nanjing', '南京', '191');
INSERT INTO `city` VALUES ('nanning', '南宁', '209');
INSERT INTO `city` VALUES ('nanping', '南平', '194');
INSERT INTO `city` VALUES ('nansha', '南沙', '204');
INSERT INTO `city` VALUES ('nantong', '南通', '191');
INSERT INTO `city` VALUES ('nantou', '南投', '208');
INSERT INTO `city` VALUES ('nanyang', '南阳', '197');
INSERT INTO `city` VALUES ('naqu', '那曲', '210');
INSERT INTO `city` VALUES ('neijiang', '内江', '202');
INSERT INTO `city` VALUES ('ningbo', '宁波', '192');
INSERT INTO `city` VALUES ('ningde', '宁德', '194');
INSERT INTO `city` VALUES ('nujiang', '怒江', '205');
INSERT INTO `city` VALUES ('panjin', '盘锦', '96');
INSERT INTO `city` VALUES ('panzhihua', '攀枝花', '202');
INSERT INTO `city` VALUES ('penghu_xian', '澎湖县', '208');
INSERT INTO `city` VALUES ('pingdingshan', '平顶山', '197');
INSERT INTO `city` VALUES ('pingdong_xian', '屏东县', '208');
INSERT INTO `city` VALUES ('pingliang', '平凉', '201');
INSERT INTO `city` VALUES ('pingxiang', '萍乡', '195');
INSERT INTO `city` VALUES ('puer', '普洱', '205');
INSERT INTO `city` VALUES ('putian', '莆田', '194');
INSERT INTO `city` VALUES ('puyang', '濮阳', '197');
INSERT INTO `city` VALUES ('qiandongnan', '黔东南', '203');
INSERT INTO `city` VALUES ('qiannan', '黔南', '203');
INSERT INTO `city` VALUES ('qianxinan', '黔西南', '203');
INSERT INTO `city` VALUES ('qingdao', '青岛', '196');
INSERT INTO `city` VALUES ('qinghaihu', '青海湖', '206');
INSERT INTO `city` VALUES ('qingyang', '庆阳', '201');
INSERT INTO `city` VALUES ('qingyuan', '清远', '200');
INSERT INTO `city` VALUES ('qinhuangdao', '秦皇岛', '59');
INSERT INTO `city` VALUES ('qinzhou', '钦州', '209');
INSERT INTO `city` VALUES ('qionghai', '琼海', '204');
INSERT INTO `city` VALUES ('qiongzhong', '琼中', '204');
INSERT INTO `city` VALUES ('qiqihaer', '齐齐哈尔', '125');
INSERT INTO `city` VALUES ('qitaihe', '七台河', '125');
INSERT INTO `city` VALUES ('quanzhou', '泉州', '194');
INSERT INTO `city` VALUES ('qujing', '曲靖', '205');
INSERT INTO `city` VALUES ('quzhou', '衢州', '192');
INSERT INTO `city` VALUES ('rikaze', '日喀则', '210');
INSERT INTO `city` VALUES ('rizhao', '日照', '196');
INSERT INTO `city` VALUES ('sanmenxia', '三门峡', '197');
INSERT INTO `city` VALUES ('sanming', '三明', '194');
INSERT INTO `city` VALUES ('sanya', '三亚', '204');
INSERT INTO `city` VALUES ('shanghai_city', '上海', '14');
INSERT INTO `city` VALUES ('shangluo', '商洛', '207');
INSERT INTO `city` VALUES ('shangqiu', '商丘', '197');
INSERT INTO `city` VALUES ('shangrao', '上饶', '195');
INSERT INTO `city` VALUES ('shannan', '山南', '210');
INSERT INTO `city` VALUES ('shantou', '汕头', '200');
INSERT INTO `city` VALUES ('shanwei', '汕尾', '200');
INSERT INTO `city` VALUES ('shaoguan', '韶关', '200');
INSERT INTO `city` VALUES ('shaoxing', '绍兴', '192');
INSERT INTO `city` VALUES ('shaoyang', '邵阳', '199');
INSERT INTO `city` VALUES ('shennongjia', '神农架', '198');
INSERT INTO `city` VALUES ('shenyang', '沈阳', '96');
INSERT INTO `city` VALUES ('shenzhen', '深圳', '200');
INSERT INTO `city` VALUES ('shihezi', '石河子', '212');
INSERT INTO `city` VALUES ('shijiazhuang', '石家庄', '59');
INSERT INTO `city` VALUES ('shiyan', '十堰', '198');
INSERT INTO `city` VALUES ('shizuishan', '石嘴山', '211');
INSERT INTO `city` VALUES ('shuangyashan', '双鸭山', '125');
INSERT INTO `city` VALUES ('shuozhou', '朔州', '74');
INSERT INTO `city` VALUES ('siping', '四平', '103');
INSERT INTO `city` VALUES ('songyuan', '松原', '103');
INSERT INTO `city` VALUES ('suihua', '绥化', '125');
INSERT INTO `city` VALUES ('suining', '遂宁', '202');
INSERT INTO `city` VALUES ('suizhou', '随州', '198');
INSERT INTO `city` VALUES ('suqian', '宿迁', '191');
INSERT INTO `city` VALUES ('suzhou_anhui', '宿州', '193');
INSERT INTO `city` VALUES ('suzhou_jiangsu', '苏州', '191');
INSERT INTO `city` VALUES ('tacheng', '塔城', '212');
INSERT INTO `city` VALUES ('taian', '泰安', '196');
INSERT INTO `city` VALUES ('taidong', '台东', '208');
INSERT INTO `city` VALUES ('tainan', '台南', '208');
INSERT INTO `city` VALUES ('tainan_xian', '台南县', '208');
INSERT INTO `city` VALUES ('taipei', '台北', '208');
INSERT INTO `city` VALUES ('taipei_xian', '台北县', '208');
INSERT INTO `city` VALUES ('taiyuan', '太原', '74');
INSERT INTO `city` VALUES ('taizhong', '台中', '208');
INSERT INTO `city` VALUES ('taizhong_xian', '台中县', '208');
INSERT INTO `city` VALUES ('taizhou_jiangsu', '泰州', '191');
INSERT INTO `city` VALUES ('taizhou_zhejiang', '台州', '192');
INSERT INTO `city` VALUES ('tamsui', '淡水', '208');
INSERT INTO `city` VALUES ('tangshan', '唐山', '59');
INSERT INTO `city` VALUES ('taoyuan', '桃园', '208');
INSERT INTO `city` VALUES ('tianjin_city', '天津', '13');
INSERT INTO `city` VALUES ('tianmen', '天门', '198');
INSERT INTO `city` VALUES ('tianshui', '天水', '201');
INSERT INTO `city` VALUES ('tieling', '铁岭', '96');
INSERT INTO `city` VALUES ('tonghua', '通化', '103');
INSERT INTO `city` VALUES ('tongliao', '通辽', '213');
INSERT INTO `city` VALUES ('tongling', '铜陵', '193');
INSERT INTO `city` VALUES ('tongren', '铜仁', '203');
INSERT INTO `city` VALUES ('tongzhou', '铜川', '207');
INSERT INTO `city` VALUES ('tulufan', '吐鲁番', '212');
INSERT INTO `city` VALUES ('tumushuke', '图木舒克', '212');
INSERT INTO `city` VALUES ('tunchang', '屯昌', '204');
INSERT INTO `city` VALUES ('wanning', '万宁', '204');
INSERT INTO `city` VALUES ('weifang', '潍坊', '196');
INSERT INTO `city` VALUES ('weihai', '威海', '196');
INSERT INTO `city` VALUES ('weinan', '渭南', '207');
INSERT INTO `city` VALUES ('wenchang', '文昌', '204');
INSERT INTO `city` VALUES ('wenshan', '文山', '205');
INSERT INTO `city` VALUES ('wenzhou', '温州', '192');
INSERT INTO `city` VALUES ('wuhai', '乌海', '213');
INSERT INTO `city` VALUES ('wuhan', '武汉', '198');
INSERT INTO `city` VALUES ('wuhu', '芜湖', '193');
INSERT INTO `city` VALUES ('wujiaqu', '五家渠', '212');
INSERT INTO `city` VALUES ('wulanchabu', '乌兰察布', '213');
INSERT INTO `city` VALUES ('wulumuqi', '乌鲁木齐', '212');
INSERT INTO `city` VALUES ('wuwei', '武威', '201');
INSERT INTO `city` VALUES ('wuxi', '无锡', '191');
INSERT INTO `city` VALUES ('wuzhishan', '五指山', '204');
INSERT INTO `city` VALUES ('wuzhong', '吴忠', '211');
INSERT INTO `city` VALUES ('wuzhou', '梧州', '209');
INSERT INTO `city` VALUES ('xiamen', '厦门', '194');
INSERT INTO `city` VALUES ('xian', '西安', '207');
INSERT INTO `city` VALUES ('xiangfan', '襄樊', '198');
INSERT INTO `city` VALUES ('xiangtan', '湘潭', '199');
INSERT INTO `city` VALUES ('xiangxi', '湘西', '199');
INSERT INTO `city` VALUES ('xianning', '咸宁', '198');
INSERT INTO `city` VALUES ('xiantao', '仙桃', '198');
INSERT INTO `city` VALUES ('xianyang', '咸阳', '207');
INSERT INTO `city` VALUES ('xiaogan', '孝感', '198');
INSERT INTO `city` VALUES ('xilinguole', '锡林郭勒盟', '213');
INSERT INTO `city` VALUES ('xingan', '兴安盟', '213');
INSERT INTO `city` VALUES ('xingtai', '邢台', '59');
INSERT INTO `city` VALUES ('xining', '西宁', '206');
INSERT INTO `city` VALUES ('xinxiang', '新乡', '197');
INSERT INTO `city` VALUES ('xinyang', '信阳', '197');
INSERT INTO `city` VALUES ('xinyu', '新余', '195');
INSERT INTO `city` VALUES ('xinzhou', '忻州', '74');
INSERT INTO `city` VALUES ('xinzhu', '新竹', '208');
INSERT INTO `city` VALUES ('xisha', '西沙', '204');
INSERT INTO `city` VALUES ('xishuangbanna', '西双版纳', '205');
INSERT INTO `city` VALUES ('xuancheng', '宣城', '193');
INSERT INTO `city` VALUES ('xuchang', '许昌', '197');
INSERT INTO `city` VALUES ('xuzhou', '徐州', '191');
INSERT INTO `city` VALUES ('yaan', '雅安', '202');
INSERT INTO `city` VALUES ('yanan', '延安', '207');
INSERT INTO `city` VALUES ('yanbian', '延边', '103');
INSERT INTO `city` VALUES ('yancheng', '盐城', '191');
INSERT INTO `city` VALUES ('yangjiang', '阳江', '200');
INSERT INTO `city` VALUES ('yangquan', '阳泉', '74');
INSERT INTO `city` VALUES ('yangzhou', '扬州', '191');
INSERT INTO `city` VALUES ('yantai', '烟台', '196');
INSERT INTO `city` VALUES ('yibin', '宜宾', '202');
INSERT INTO `city` VALUES ('yichang', '宜昌', '198');
INSERT INTO `city` VALUES ('yichun_heilongjiang', '伊春', '125');
INSERT INTO `city` VALUES ('yichun_jiangxi', '宜春', '195');
INSERT INTO `city` VALUES ('yilan_taiwan', '宜兰县', '208');
INSERT INTO `city` VALUES ('yili', '伊犁', '212');
INSERT INTO `city` VALUES ('yinchuan', '银川', '211');
INSERT INTO `city` VALUES ('yingkou', '营口', '96');
INSERT INTO `city` VALUES ('yingtan', '鹰潭', '195');
INSERT INTO `city` VALUES ('yiyang', '益阳', '199');
INSERT INTO `city` VALUES ('yongzhou', '永州', '199');
INSERT INTO `city` VALUES ('yueyang', '岳阳', '199');
INSERT INTO `city` VALUES ('yulin_guangxi', '玉林', '209');
INSERT INTO `city` VALUES ('yulin_shanxi_02', '榆林', '207');
INSERT INTO `city` VALUES ('yuncheng', '运城', '74');
INSERT INTO `city` VALUES ('yunfu', '云浮', '200');
INSERT INTO `city` VALUES ('yunlin', '云林', '208');
INSERT INTO `city` VALUES ('yushu', '玉树', '206');
INSERT INTO `city` VALUES ('yuxi', '玉溪', '205');
INSERT INTO `city` VALUES ('zaozhuang', '枣庄', '196');
INSERT INTO `city` VALUES ('zhangjiajie', '张家界', '199');
INSERT INTO `city` VALUES ('zhangjiakou', '张家口', '59');
INSERT INTO `city` VALUES ('zhangye', '张掖', '201');
INSERT INTO `city` VALUES ('zhangzhou', '漳州', '194');
INSERT INTO `city` VALUES ('zhanjiang', '湛江', '200');
INSERT INTO `city` VALUES ('zhaoqing', '肇庆', '200');
INSERT INTO `city` VALUES ('zhaotong', '昭通', '205');
INSERT INTO `city` VALUES ('zhengzhou', '郑州', '197');
INSERT INTO `city` VALUES ('zhenjiang', '镇江', '191');
INSERT INTO `city` VALUES ('zhongsha', '中沙', '204');
INSERT INTO `city` VALUES ('zhongshan', '中山', '200');
INSERT INTO `city` VALUES ('zhongwei', '中卫', '211');
INSERT INTO `city` VALUES ('zhoukou', '周口', '197');
INSERT INTO `city` VALUES ('zhoushan', '舟山', '192');
INSERT INTO `city` VALUES ('zhuhai', '珠海', '200');
INSERT INTO `city` VALUES ('zhumadian', '驻马店', '197');
INSERT INTO `city` VALUES ('zhuzhou', '株洲', '199');
INSERT INTO `city` VALUES ('zibo', '淄博', '196');
INSERT INTO `city` VALUES ('zigong', '自贡', '202');
INSERT INTO `city` VALUES ('ziyang', '资阳', '202');
INSERT INTO `city` VALUES ('zunyi', '遵义', '203');


#区域信息
INSERT INTO `district` VALUES ('1148201', '文殊', 'jiayuguan');
INSERT INTO `district` VALUES ('11482010', '新华', 'jiayuguan');
INSERT INTO `district` VALUES ('1148202', '峪泉', 'jiayuguan');
INSERT INTO `district` VALUES ('1148203', '新城', 'jiayuguan');
INSERT INTO `district` VALUES ('1148204', '朝阳', 'jiayuguan');
INSERT INTO `district` VALUES ('1148205', '峪苑', 'jiayuguan');
INSERT INTO `district` VALUES ('1148206', '前进', 'jiayuguan');
INSERT INTO `district` VALUES ('1148207', '建设', 'jiayuguan');
INSERT INTO `district` VALUES ('1148208', '五一', 'jiayuguan');
INSERT INTO `district` VALUES ('1148209', '胜利', 'jiayuguan');
INSERT INTO `district` VALUES ('115850', '河西区', 'sanya');
INSERT INTO `district` VALUES ('115851', '河东区', 'sanya');
INSERT INTO `district` VALUES ('115852', '海棠湾', 'sanya');
INSERT INTO `district` VALUES ('115853', '三亚湾', 'sanya');
INSERT INTO `district` VALUES ('115854', '亚龙湾', 'sanya');
INSERT INTO `district` VALUES ('115855', '大东海', 'sanya');
INSERT INTO `district` VALUES ('115856', '凤凰岛', 'sanya');
INSERT INTO `district` VALUES ('115857', '田独', 'sanya');
INSERT INTO `district` VALUES ('115858', '崖城', 'sanya');
INSERT INTO `district` VALUES ('115859', '天涯', 'sanya');
INSERT INTO `district` VALUES ('115860', '育才', 'sanya');
INSERT INTO `district` VALUES ('3136201', '石岐区', 'zhongshan');
INSERT INTO `district` VALUES ('3136202', '西区', 'zhongshan');
INSERT INTO `district` VALUES ('3136203', '东区', 'zhongshan');
INSERT INTO `district` VALUES ('3136204', '南区', 'zhongshan');
INSERT INTO `district` VALUES ('3136205', '火炬区', 'zhongshan');
INSERT INTO `district` VALUES ('3136206', '黄圃', 'zhongshan');
INSERT INTO `district` VALUES ('3136207', '阜沙', 'zhongshan');
INSERT INTO `district` VALUES ('3136208', '三角', 'zhongshan');
INSERT INTO `district` VALUES ('3136209', '民众', 'zhongshan');
INSERT INTO `district` VALUES ('3136210', '南朗', 'zhongshan');
INSERT INTO `district` VALUES ('3136211', '五桂山', 'zhongshan');
INSERT INTO `district` VALUES ('3136212', '板芙', 'zhongshan');
INSERT INTO `district` VALUES ('3136213', '神湾', 'zhongshan');
INSERT INTO `district` VALUES ('3136214', '小榄', 'zhongshan');
INSERT INTO `district` VALUES ('3136215', '东升', 'zhongshan');
INSERT INTO `district` VALUES ('3136216', '东凤', 'zhongshan');
INSERT INTO `district` VALUES ('3136217', '南头', 'zhongshan');
INSERT INTO `district` VALUES ('3136218', '横栏', 'zhongshan');
INSERT INTO `district` VALUES ('3136219', '大涌', 'zhongshan');
INSERT INTO `district` VALUES ('3136220', '沙溪', 'zhongshan');
INSERT INTO `district` VALUES ('3136221', '三乡', 'zhongshan');
INSERT INTO `district` VALUES ('3136222', '港口', 'zhongshan');
INSERT INTO `district` VALUES ('3136301', '莞城区', 'dongguan');
INSERT INTO `district` VALUES ('3136302', '东城区', 'dongguan');
INSERT INTO `district` VALUES ('3136303', '南城区', 'dongguan');
INSERT INTO `district` VALUES ('3136304', '万江区', 'dongguan');
INSERT INTO `district` VALUES ('3136305', '虎门', 'dongguan');
INSERT INTO `district` VALUES ('3136306', '厚街', 'dongguan');
INSERT INTO `district` VALUES ('3136307', '樟木头', 'dongguan');
INSERT INTO `district` VALUES ('3136308', '常平', 'dongguan');
INSERT INTO `district` VALUES ('3136309', '长安', 'dongguan');
INSERT INTO `district` VALUES ('3136310', '石碣', 'dongguan');
INSERT INTO `district` VALUES ('3136311', '石龙', 'dongguan');
INSERT INTO `district` VALUES ('3136312', '凤岗', 'dongguan');
INSERT INTO `district` VALUES ('3136313', '黄江', 'dongguan');
INSERT INTO `district` VALUES ('3136314', '塘厦', 'dongguan');
INSERT INTO `district` VALUES ('3136315', '清溪', 'dongguan');
INSERT INTO `district` VALUES ('3136316', '茶山', 'dongguan');
INSERT INTO `district` VALUES ('3136317', '石排', 'dongguan');
INSERT INTO `district` VALUES ('3136318', '企石', 'dongguan');
INSERT INTO `district` VALUES ('3136319', '谢岗', 'dongguan');
INSERT INTO `district` VALUES ('3136320', '大朗', 'dongguan');
INSERT INTO `district` VALUES ('3136321', '寮步', 'dongguan');
INSERT INTO `district` VALUES ('3136322', '东坑', 'dongguan');
INSERT INTO `district` VALUES ('3136323', '横沥', 'dongguan');
INSERT INTO `district` VALUES ('3136324', '大岭山', 'dongguan');
INSERT INTO `district` VALUES ('3136325', '沙田', 'dongguan');
INSERT INTO `district` VALUES ('3136326', '道滘', 'dongguan');
INSERT INTO `district` VALUES ('3136327', '高埗', 'dongguan');
INSERT INTO `district` VALUES ('3136328', '望牛墩', 'dongguan');
INSERT INTO `district` VALUES ('3136329', '中堂', 'dongguan');
INSERT INTO `district` VALUES ('3136330', '麻涌', 'dongguan');
INSERT INTO `district` VALUES ('3136331', '洪梅', 'dongguan');
INSERT INTO `district` VALUES ('3136332', '松山湖', 'dongguan');
INSERT INTO `district` VALUES ('32672', '井研县', 'leshan');
INSERT INTO `district` VALUES ('32673', '驿城区', 'zhumadian');
INSERT INTO `district` VALUES ('32674', '兴宾区', 'laibin');
INSERT INTO `district` VALUES ('32675', '洛龙区', 'luoyang');
INSERT INTO `district` VALUES ('32676', '易门县', 'yuxi');
INSERT INTO `district` VALUES ('32677', '曲松县', 'shannan');
INSERT INTO `district` VALUES ('32678', '钦南区', 'qinzhou');
INSERT INTO `district` VALUES ('32679', '富宁县', 'wenshan');
INSERT INTO `district` VALUES ('32680', '四子王旗', 'wulanchabu');
INSERT INTO `district` VALUES ('32681', '江南区', 'nanning');
INSERT INTO `district` VALUES ('32682', '红塔区', 'yuxi');
INSERT INTO `district` VALUES ('32683', '城阳区', 'qingdao');
INSERT INTO `district` VALUES ('32684', '花溪区', 'guiyang');
INSERT INTO `district` VALUES ('32685', '绥棱县', 'suihua');
INSERT INTO `district` VALUES ('32686', '沽源县', 'zhangjiakou');
INSERT INTO `district` VALUES ('32687', '香坊区', 'haerbin');
INSERT INTO `district` VALUES ('32688', '张店区', 'zibo');
INSERT INTO `district` VALUES ('32689', '黑水县', 'aba');
INSERT INTO `district` VALUES ('32690', '汉阳区', 'wuhan');
INSERT INTO `district` VALUES ('32691', '广平县', 'handan');
INSERT INTO `district` VALUES ('32692', '依兰县', 'haerbin');
INSERT INTO `district` VALUES ('32693', '南皮县', 'cangzhou');
INSERT INTO `district` VALUES ('32694', '施秉县', 'qiandongnan');
INSERT INTO `district` VALUES ('32695', '良庆区', 'nanning');
INSERT INTO `district` VALUES ('32696', '襄阳区', 'xiangfan');
INSERT INTO `district` VALUES ('32697', '万盛区', 'chongqing_city');
INSERT INTO `district` VALUES ('32698', '新县', 'xinyang');
INSERT INTO `district` VALUES ('32699', '芦淞区', 'zhuzhou');
INSERT INTO `district` VALUES ('32700', '磁县', 'handan');
INSERT INTO `district` VALUES ('32701', '澄迈县', 'chengmai');
INSERT INTO `district` VALUES ('32702', '建水县', 'honghe');
INSERT INTO `district` VALUES ('32703', '玉州区', 'yulin_guangxi');
INSERT INTO `district` VALUES ('32704', '黄南藏族自治州', 'huangnan');
INSERT INTO `district` VALUES ('32705', '金阊区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('32706', '略阳县', 'hanzhong');
INSERT INTO `district` VALUES ('32707', '覃塘区', 'guigang');
INSERT INTO `district` VALUES ('32708', '无极县', 'shijiazhuang');
INSERT INTO `district` VALUES ('32709', '新华区', 'cangzhou');
INSERT INTO `district` VALUES ('32710', '大化瑶族自治县', 'hechi');
INSERT INTO `district` VALUES ('32711', '革吉县', 'ali');
INSERT INTO `district` VALUES ('32712', '青羊区', 'chengdu');
INSERT INTO `district` VALUES ('32713', '石柱土家族自治县', 'chongqing_city');
INSERT INTO `district` VALUES ('32714', '海安县', 'nantong');
INSERT INTO `district` VALUES ('32715', '逊克县', 'heihe');
INSERT INTO `district` VALUES ('32716', '河口区', 'dongying');
INSERT INTO `district` VALUES ('32717', '互助土族自治县', 'haidong');
INSERT INTO `district` VALUES ('32718', '闽清县', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('32719', '沂源县', 'zibo');
INSERT INTO `district` VALUES ('32720', '永泰县', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('32721', '仙居县', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('32722', '泰山区', 'taian');
INSERT INTO `district` VALUES ('32723', '堆龙德庆县', 'lasa');
INSERT INTO `district` VALUES ('32724', '余江县', 'yingtan');
INSERT INTO `district` VALUES ('32725', '牟定县', 'chuxiong');
INSERT INTO `district` VALUES ('32726', '霞山区', 'zhanjiang');
INSERT INTO `district` VALUES ('32727', '山南地区', 'shannan');
INSERT INTO `district` VALUES ('32728', '毕节地区', 'bijie');
INSERT INTO `district` VALUES ('32729', '绥江县', 'zhaotong');
INSERT INTO `district` VALUES ('32730', '沈河区', 'shenyang');
INSERT INTO `district` VALUES ('32731', '肇州县', 'daqing');
INSERT INTO `district` VALUES ('32732', '特克斯县', 'yili');
INSERT INTO `district` VALUES ('32733', '武定县', 'chuxiong');
INSERT INTO `district` VALUES ('32734', '阿拉善右旗', 'alashan');
INSERT INTO `district` VALUES ('32735', '瓮安县', 'qiannan');
INSERT INTO `district` VALUES ('32736', '寻甸回族彝族自治县', 'kunming');
INSERT INTO `district` VALUES ('32737', '栾城县', 'shijiazhuang');
INSERT INTO `district` VALUES ('32738', '丹巴县', 'ganzi');
INSERT INTO `district` VALUES ('32739', '芝罘区', 'yantai');
INSERT INTO `district` VALUES ('32740', '怀远县', 'bangbu');
INSERT INTO `district` VALUES ('32741', '固始县', 'xinyang');
INSERT INTO `district` VALUES ('32742', '道县', 'yongzhou');
INSERT INTO `district` VALUES ('32743', '巴里坤哈萨克自治县', 'hami');
INSERT INTO `district` VALUES ('32744', '洪雅县', 'meishan');
INSERT INTO `district` VALUES ('32745', '富裕县', 'qiqihaer');
INSERT INTO `district` VALUES ('32746', '宁津县', 'dezhou');
INSERT INTO `district` VALUES ('32747', '青河县', 'aletai');
INSERT INTO `district` VALUES ('32748', '奉贤区', 'shanghai_city');
INSERT INTO `district` VALUES ('32749', '鹤山区', 'hebi');
INSERT INTO `district` VALUES ('32750', '秭归县', 'yichang');
INSERT INTO `district` VALUES ('32751', '霍山县', 'liuan');
INSERT INTO `district` VALUES ('32752', '抚宁县', 'qinhuangdao');
INSERT INTO `district` VALUES ('32753', '望城县', 'changsha');
INSERT INTO `district` VALUES ('32754', '永顺县', 'xiangxi');
INSERT INTO `district` VALUES ('32755', '浮山县', 'linfen');
INSERT INTO `district` VALUES ('32756', '东河区', 'baotou');
INSERT INTO `district` VALUES ('32757', '睢县', 'shangqiu');
INSERT INTO `district` VALUES ('32758', '濉溪县', 'huaibei');
INSERT INTO `district` VALUES ('32759', '平利县', 'ankang');
INSERT INTO `district` VALUES ('32760', '城口县', 'chongqing_city');
INSERT INTO `district` VALUES ('32761', '陵川县', 'jincheng');
INSERT INTO `district` VALUES ('32762', '湖滨区', 'sanmenxia');
INSERT INTO `district` VALUES ('32763', '连山壮族瑶族自治县', 'qingyuan');
INSERT INTO `district` VALUES ('32764', '台江区', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('32765', '焉耆回族自治县', 'bayinguoleng');
INSERT INTO `district` VALUES ('32766', '威县', 'xingtai');
INSERT INTO `district` VALUES ('32767', '雅江县', 'ganzi');
INSERT INTO `district` VALUES ('32768', '巴南区', 'chongqing_city');
INSERT INTO `district` VALUES ('32769', '赫章县', 'bijie');
INSERT INTO `district` VALUES ('32770', '山丹县', 'zhangye');
INSERT INTO `district` VALUES ('32771', '咸丰县', 'enshi');
INSERT INTO `district` VALUES ('32772', '理塘县', 'ganzi');
INSERT INTO `district` VALUES ('32773', '霍邱县', 'liuan');
INSERT INTO `district` VALUES ('32774', '如东县', 'nantong');
INSERT INTO `district` VALUES ('32775', '上城区', 'hangzhou');
INSERT INTO `district` VALUES ('32776', '新罗区', 'longyan');
INSERT INTO `district` VALUES ('32777', '威宁彝族回族苗族自治县', 'bijie');
INSERT INTO `district` VALUES ('32778', '太仆寺旗', 'xilinguole');
INSERT INTO `district` VALUES ('32779', '桐梓县', 'zunyi');
INSERT INTO `district` VALUES ('32780', '繁昌县', 'wuhu');
INSERT INTO `district` VALUES ('32781', '福海县', 'aletai');
INSERT INTO `district` VALUES ('32782', '张家川回族自治县', 'tianshui');
INSERT INTO `district` VALUES ('32783', '灌阳县', 'guilin');
INSERT INTO `district` VALUES ('32784', '武隆县', 'chongqing_city');
INSERT INTO `district` VALUES ('32785', '桂阳县', 'chenzhou');
INSERT INTO `district` VALUES ('32786', '荔湾区', 'guangzhou');
INSERT INTO `district` VALUES ('32787', '云霄县', 'zhangzhou');
INSERT INTO `district` VALUES ('32788', '安义县', 'nanchang');
INSERT INTO `district` VALUES ('32789', '柞水县', 'shangluo');
INSERT INTO `district` VALUES ('32790', '马关县', 'wenshan');
INSERT INTO `district` VALUES ('32791', '屏南县', 'ningde');
INSERT INTO `district` VALUES ('32792', '兴山县', 'yichang');
INSERT INTO `district` VALUES ('32793', '南郑县', 'hanzhong');
INSERT INTO `district` VALUES ('32794', '邱县', 'handan');
INSERT INTO `district` VALUES ('32795', '渠县', 'dazhou');
INSERT INTO `district` VALUES ('32796', '浦口区', 'nanjing');
INSERT INTO `district` VALUES ('32797', '余干县', 'shangrao');
INSERT INTO `district` VALUES ('32798', '和田县', 'hetian');
INSERT INTO `district` VALUES ('32799', '蒲城县', 'weinan');
INSERT INTO `district` VALUES ('32800', '解放区', 'jiaozuo');
INSERT INTO `district` VALUES ('32801', '兴安县', 'guilin');
INSERT INTO `district` VALUES ('32802', '雷波县', 'liangshan');
INSERT INTO `district` VALUES ('32803', '金湾区', 'zhuhai');
INSERT INTO `district` VALUES ('32804', '铜山县', 'xuzhou');
INSERT INTO `district` VALUES ('32805', '蚌山区', 'bangbu');
INSERT INTO `district` VALUES ('32806', '监利县', 'jingzhou');
INSERT INTO `district` VALUES ('32807', '察隅县', 'linzhi');
INSERT INTO `district` VALUES ('32808', '阿克塞哈萨克族自治县', 'jiuquan');
INSERT INTO `district` VALUES ('32809', '卓资县', 'wulanchabu');
INSERT INTO `district` VALUES ('32810', '文安县', 'langfang');
INSERT INTO `district` VALUES ('32811', '麟游县', 'baoji');
INSERT INTO `district` VALUES ('32812', '来安县', 'chuzhou');
INSERT INTO `district` VALUES ('32813', '庆元县', 'lishui');
INSERT INTO `district` VALUES ('32814', '龙华区', 'haikou');
INSERT INTO `district` VALUES ('32815', '舟曲县', 'gannan');
INSERT INTO `district` VALUES ('32816', '东安区', 'mudanjiang');
INSERT INTO `district` VALUES ('32818', '神池县', 'xinzhou');
INSERT INTO `district` VALUES ('32819', '渭城区', 'xianyang');
INSERT INTO `district` VALUES ('32820', '仓山区', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('32821', '丰润区', 'tangshan');
INSERT INTO `district` VALUES ('32822', '上林县', 'nanning');
INSERT INTO `district` VALUES ('32823', '腾冲县', 'baoshan');
INSERT INTO `district` VALUES ('32824', '文成县', 'wenzhou');
INSERT INTO `district` VALUES ('32825', '奉新县', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('32826', '环县', 'qingyang');
INSERT INTO `district` VALUES ('32827', '红桥区', 'tianjin_city');
INSERT INTO `district` VALUES ('32828', '达尔罕茂明安联合旗', 'baotou');
INSERT INTO `district` VALUES ('32829', '于田县', 'hetian');
INSERT INTO `district` VALUES ('32830', '二道区', 'changchun');
INSERT INTO `district` VALUES ('32831', '管城回族区', 'zhengzhou');
INSERT INTO `district` VALUES ('32832', '奈曼旗', 'tongliao');
INSERT INTO `district` VALUES ('32833', '正定县', 'shijiazhuang');
INSERT INTO `district` VALUES ('32834', '岱山县', 'zhoushan');
INSERT INTO `district` VALUES ('32835', '盐池县', 'wuzhong');
INSERT INTO `district` VALUES ('32836', '鲁山县', 'pingdingshan');
INSERT INTO `district` VALUES ('32837', '镇雄县', 'zhaotong');
INSERT INTO `district` VALUES ('32838', '汉南区', 'wuhan');
INSERT INTO `district` VALUES ('32839', '皋兰县', 'lanzhou');
INSERT INTO `district` VALUES ('32840', '加查县', 'shannan');
INSERT INTO `district` VALUES ('32841', '魏都区', 'xuchang');
INSERT INTO `district` VALUES ('32842', '得荣县', 'ganzi');
INSERT INTO `district` VALUES ('32843', '沧县', 'cangzhou');
INSERT INTO `district` VALUES ('32844', '九龙坡区', 'chongqing_city');
INSERT INTO `district` VALUES ('32845', '土默特左旗', 'huhehaote');
INSERT INTO `district` VALUES ('32846', '武川县', 'huhehaote');
INSERT INTO `district` VALUES ('32847', '嫩江县', 'heihe');
INSERT INTO `district` VALUES ('32848', '永昌县', 'jinchang');
INSERT INTO `district` VALUES ('32849', '合浦县', 'beihai');
INSERT INTO `district` VALUES ('32850', '封丘县', 'xinxiang');
INSERT INTO `district` VALUES ('32852', '达坂城区', 'wulumuqi');
INSERT INTO `district` VALUES ('32853', '兴隆台区', 'panjin');
INSERT INTO `district` VALUES ('32854', '顺德区', 'foshan');
INSERT INTO `district` VALUES ('32855', '岐山县', 'baoji');
INSERT INTO `district` VALUES ('32856', '贡觉县', 'changdu');
INSERT INTO `district` VALUES ('32857', '湘西土家族苗族自治州', 'xiangxi');
INSERT INTO `district` VALUES ('32858', '盱眙县', 'huaian');
INSERT INTO `district` VALUES ('32859', '大竹县', 'dazhou');
INSERT INTO `district` VALUES ('32860', '龙门县', 'huizhou_guangdong');
INSERT INTO `district` VALUES ('32861', '南乐县', 'puyang');
INSERT INTO `district` VALUES ('32862', '江永县', 'yongzhou');
INSERT INTO `district` VALUES ('32863', '西区', 'panzhihua');
INSERT INTO `district` VALUES ('32864', '大名县', 'handan');
INSERT INTO `district` VALUES ('32865', '沧源佤族自治县', 'lincang');
INSERT INTO `district` VALUES ('32866', '丹棱县', 'meishan');
INSERT INTO `district` VALUES ('32867', '万年县', 'shangrao');
INSERT INTO `district` VALUES ('32868', '峰峰矿区', 'handan');
INSERT INTO `district` VALUES ('32869', '凤县', 'baoji');
INSERT INTO `district` VALUES ('32870', '建湖县', 'yancheng');
INSERT INTO `district` VALUES ('32871', '淮滨县', 'xinyang');
INSERT INTO `district` VALUES ('32872', '榕城区', 'jieyang');
INSERT INTO `district` VALUES ('32873', '龙南县', 'ganzhou');
INSERT INTO `district` VALUES ('32874', '麻阳苗族自治县', 'huaihua');
INSERT INTO `district` VALUES ('32875', '江北区', 'ningbo');
INSERT INTO `district` VALUES ('32876', '恭城瑶族自治县', 'guilin');
INSERT INTO `district` VALUES ('32877', '临漳县', 'handan');
INSERT INTO `district` VALUES ('32878', '龙游县', 'quzhou');
INSERT INTO `district` VALUES ('32879', '芜湖县', 'wuhu');
INSERT INTO `district` VALUES ('32880', '伊川县', 'luoyang');
INSERT INTO `district` VALUES ('32881', '新邱区', 'fuxin');
INSERT INTO `district` VALUES ('32882', '金牛区', 'chengdu');
INSERT INTO `district` VALUES ('32883', '那坡县', 'baise');
INSERT INTO `district` VALUES ('32884', '泗洪县', 'suqian');
INSERT INTO `district` VALUES ('32885', '武邑县', 'hengshui');
INSERT INTO `district` VALUES ('32886', '南木林县', 'rikaze');
INSERT INTO `district` VALUES ('32887', '范县', 'puyang');
INSERT INTO `district` VALUES ('32888', '杞县', 'kaifeng');
INSERT INTO `district` VALUES ('32889', '隆回县', 'shaoyang');
INSERT INTO `district` VALUES ('32890', '庆城县', 'qingyang');
INSERT INTO `district` VALUES ('32891', '应县', 'shuozhou');
INSERT INTO `district` VALUES ('32892', '高陵县', 'xian');
INSERT INTO `district` VALUES ('32893', '贾汪区', 'xuzhou');
INSERT INTO `district` VALUES ('32894', '佳县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('32895', '云和县', 'lishui');
INSERT INTO `district` VALUES ('32896', '阿克苏地区', 'akesu');
INSERT INTO `district` VALUES ('32897', '三台县', 'mianyang');
INSERT INTO `district` VALUES ('32898', '阳原县', 'zhangjiakou');
INSERT INTO `district` VALUES ('32899', '芒康县', 'changdu');
INSERT INTO `district` VALUES ('32900', '大安区', 'zigong');
INSERT INTO `district` VALUES ('32901', '旬邑县', 'xianyang');
INSERT INTO `district` VALUES ('32902', '清丰县', 'puyang');
INSERT INTO `district` VALUES ('32903', '沁县', 'changzhi');
INSERT INTO `district` VALUES ('32904', '永修县', 'jiujiang');
INSERT INTO `district` VALUES ('32905', '乳源瑶族自治县', 'shaoguan');
INSERT INTO `district` VALUES ('32906', '磐安县', 'jinhua');
INSERT INTO `district` VALUES ('32907', '开江县', 'dazhou');
INSERT INTO `district` VALUES ('32908', '和硕县', 'bayinguoleng');
INSERT INTO `district` VALUES ('32909', '德庆县', 'zhaoqing');
INSERT INTO `district` VALUES ('32910', '龙泉驿区', 'chengdu');
INSERT INTO `district` VALUES ('32911', '昌邑区', 'jilin_city');
INSERT INTO `district` VALUES ('32912', '桐柏县', 'nanyang');
INSERT INTO `district` VALUES ('32913', '壶关县', 'changzhi');
INSERT INTO `district` VALUES ('32914', '凤山县', 'hechi');
INSERT INTO `district` VALUES ('32915', '电白县', 'maoming');
INSERT INTO `district` VALUES ('32916', '和平区', 'shenyang');
INSERT INTO `district` VALUES ('32917', '绥阳县', 'zunyi');
INSERT INTO `district` VALUES ('32918', '郁南县', 'yunfu');
INSERT INTO `district` VALUES ('32919', '八步区', 'hezhou');
INSERT INTO `district` VALUES ('32921', '锦屏县', 'qiandongnan');
INSERT INTO `district` VALUES ('32922', '德清县', 'huzhou');
INSERT INTO `district` VALUES ('32923', '新津县', 'chengdu');
INSERT INTO `district` VALUES ('32924', '新绛县', 'yuncheng');
INSERT INTO `district` VALUES ('32925', '漾濞彝族自治县', 'dali');
INSERT INTO `district` VALUES ('32926', '东胜区', 'eerduosi');
INSERT INTO `district` VALUES ('32927', '麒麟区', 'qujing');
INSERT INTO `district` VALUES ('32928', '镇海区', 'ningbo');
INSERT INTO `district` VALUES ('32929', '乌尔禾区', 'kelamayi');
INSERT INTO `district` VALUES ('32930', '扎赉特旗', 'xingan');
INSERT INTO `district` VALUES ('32931', '府谷县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('32932', '新建县', 'nanchang');
INSERT INTO `district` VALUES ('32933', '红河县', 'honghe');
INSERT INTO `district` VALUES ('32934', '清徐县', 'taiyuan');
INSERT INTO `district` VALUES ('32935', '城关区', 'lanzhou');
INSERT INTO `district` VALUES ('32936', '麻栗坡县', 'wenshan');
INSERT INTO `district` VALUES ('32937', '新都区', 'chengdu');
INSERT INTO `district` VALUES ('32938', '黔西南布依族苗族自治州', 'qianxinan');
INSERT INTO `district` VALUES ('32939', '浠水县', 'huanggang');
INSERT INTO `district` VALUES ('32940', '颍泉区', 'fuyang_anhui');
INSERT INTO `district` VALUES ('32941', '衡阳县', 'hengyang');
INSERT INTO `district` VALUES ('32942', '墨竹工卡县', 'lasa');
INSERT INTO `district` VALUES ('32943', '阳信县', 'binzhou');
INSERT INTO `district` VALUES ('32944', '林芝县', 'linzhi');
INSERT INTO `district` VALUES ('32945', '城子河区', 'jixi');
INSERT INTO `district` VALUES ('32946', '同心县', 'wuzhong');
INSERT INTO `district` VALUES ('32947', '乐至县', 'ziyang');
INSERT INTO `district` VALUES ('32948', '兰西县', 'suihua');
INSERT INTO `district` VALUES ('32949', '克东县', 'qiqihaer');
INSERT INTO `district` VALUES ('32950', '长沙县', 'changsha');
INSERT INTO `district` VALUES ('32951', '利辛县', 'bozhou');
INSERT INTO `district` VALUES ('32952', '长安区', 'xian');
INSERT INTO `district` VALUES ('32953', '缙云县', 'lishui');
INSERT INTO `district` VALUES ('32954', '景泰县', 'baiyin');
INSERT INTO `district` VALUES ('32955', '临洮县', 'dingxi');
INSERT INTO `district` VALUES ('32956', '甘南藏族自治州', 'gannan');
INSERT INTO `district` VALUES ('32957', '宁海县', 'ningbo');
INSERT INTO `district` VALUES ('32958', '江汉区', 'wuhan');
INSERT INTO `district` VALUES ('32959', '定兴县', 'baoding');
INSERT INTO `district` VALUES ('32960', '郓城县', 'heze');
INSERT INTO `district` VALUES ('32961', '甘孜藏族自治州', 'ganzi');
INSERT INTO `district` VALUES ('32962', '咸安区', 'xianning');
INSERT INTO `district` VALUES ('32963', '临潼区', 'xian');
INSERT INTO `district` VALUES ('32964', '闵行区', 'shanghai_city');
INSERT INTO `district` VALUES ('32965', '吐鲁番地区', 'tulufan');
INSERT INTO `district` VALUES ('32966', '龙山县', 'xiangxi');
INSERT INTO `district` VALUES ('32967', '夏县', 'yuncheng');
INSERT INTO `district` VALUES ('32968', '西乡塘区', 'nanning');
INSERT INTO `district` VALUES ('32969', '洞头县', 'wenzhou');
INSERT INTO `district` VALUES ('32971', '聂荣县', 'naqu');
INSERT INTO `district` VALUES ('32972', '相城区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('32973', '太子河区', 'liaoyang');
INSERT INTO `district` VALUES ('32974', '永年县', 'handan');
INSERT INTO `district` VALUES ('32975', '鹤城区', 'huaihua');
INSERT INTO `district` VALUES ('32976', '星子县', 'jiujiang');
INSERT INTO `district` VALUES ('32978', '南漳县', 'xiangfan');
INSERT INTO `district` VALUES ('32979', '伽师县', 'kashi');
INSERT INTO `district` VALUES ('32980', '南芬区', 'benxi');
INSERT INTO `district` VALUES ('32981', '临淄区', 'zibo');
INSERT INTO `district` VALUES ('32982', '黄岩区', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('32983', '淇县', 'hebi');
INSERT INTO `district` VALUES ('32984', '双清区', 'shaoyang');
INSERT INTO `district` VALUES ('32985', '利通区', 'wuzhong');
INSERT INTO `district` VALUES ('32986', '萝北县', 'hegang');
INSERT INTO `district` VALUES ('32987', '枣强县', 'hengshui');
INSERT INTO `district` VALUES ('32988', '花山区', 'maanshan');
INSERT INTO `district` VALUES ('32989', '江川县', 'yuxi');
INSERT INTO `district` VALUES ('32990', '泸水县', 'nujiang');
INSERT INTO `district` VALUES ('32991', '沭阳县', 'suqian');
INSERT INTO `district` VALUES ('32992', '阳西县', 'yangjiang');
INSERT INTO `district` VALUES ('32993', '龙马潭区', 'luzhou');
INSERT INTO `district` VALUES ('32994', '巴林左旗', 'chifeng');
INSERT INTO `district` VALUES ('32995', '溪湖区', 'benxi');
INSERT INTO `district` VALUES ('32996', '射洪县', 'suining');
INSERT INTO `district` VALUES ('32997', '苏尼特右旗', 'xilinguole');
INSERT INTO `district` VALUES ('32998', '安远县', 'ganzhou');
INSERT INTO `district` VALUES ('32999', '巴彦县', 'haerbin');
INSERT INTO `district` VALUES ('33000', '德格县', 'ganzi');
INSERT INTO `district` VALUES ('33001', '秀英区', 'haikou');
INSERT INTO `district` VALUES ('33002', '揭东县', 'jieyang');
INSERT INTO `district` VALUES ('33003', '钟楼区', 'changzhou');
INSERT INTO `district` VALUES ('33004', '黄龙县', 'yanan');
INSERT INTO `district` VALUES ('33005', '都昌县', 'jiujiang');
INSERT INTO `district` VALUES ('33006', '九寨沟县', 'aba');
INSERT INTO `district` VALUES ('33007', '伊犁哈萨克自治州', 'yili');
INSERT INTO `district` VALUES ('33008', '屏边苗族自治县', 'honghe');
INSERT INTO `district` VALUES ('33009', '平舆县', 'zhumadian');
INSERT INTO `district` VALUES ('33010', '饶平县', 'chaozhou');
INSERT INTO `district` VALUES ('33011', '西平县', 'zhumadian');
INSERT INTO `district` VALUES ('33012', '新北区', 'changzhou');
INSERT INTO `district` VALUES ('33013', '大余县', 'ganzhou');
INSERT INTO `district` VALUES ('33014', '米林县', 'linzhi');
INSERT INTO `district` VALUES ('33015', '洋县', 'hanzhong');
INSERT INTO `district` VALUES ('33016', '罗湖区', 'shenzhen');
INSERT INTO `district` VALUES ('33017', '夷陵区', 'yichang');
INSERT INTO `district` VALUES ('33018', '澄城县', 'weinan');
INSERT INTO `district` VALUES ('33019', '河曲县', 'xinzhou');
INSERT INTO `district` VALUES ('33020', '会宁县', 'baiyin');
INSERT INTO `district` VALUES ('33021', '马龙县', 'qujing');
INSERT INTO `district` VALUES ('33022', '大同县', 'datong');
INSERT INTO `district` VALUES ('33023', '陆河县', 'shanwei');
INSERT INTO `district` VALUES ('33024', '秀峰区', 'guilin');
INSERT INTO `district` VALUES ('33025', '兴宁区', 'nanning');
INSERT INTO `district` VALUES ('33026', '平乡县', 'xingtai');
INSERT INTO `district` VALUES ('33027', '洛浦县', 'hetian');
INSERT INTO `district` VALUES ('33028', '港口区', 'fangchenggang');
INSERT INTO `district` VALUES ('33029', '宣汉县', 'dazhou');
INSERT INTO `district` VALUES ('33030', '莒南县', 'linyi');
INSERT INTO `district` VALUES ('33031', '榆中县', 'lanzhou');
INSERT INTO `district` VALUES ('33032', '崂山区', 'qingdao');
INSERT INTO `district` VALUES ('33033', '旌阳区', 'deyang');
INSERT INTO `district` VALUES ('33034', '金安区', 'liuan');
INSERT INTO `district` VALUES ('33035', '房山区', 'beijing_city');
INSERT INTO `district` VALUES ('33036', '白朗县', 'rikaze');
INSERT INTO `district` VALUES ('33037', '临桂县', 'guilin');
INSERT INTO `district` VALUES ('33038', '新平彝族傣族自治县', 'yuxi');
INSERT INTO `district` VALUES ('33039', '安化县', 'yiyang');
INSERT INTO `district` VALUES ('33040', '延庆县', 'beijing_city');
INSERT INTO `district` VALUES ('33041', '涵江区', 'putian');
INSERT INTO `district` VALUES ('33042', '博白县', 'yulin_guangxi');
INSERT INTO `district` VALUES ('33043', '遵义县', 'zunyi');
INSERT INTO `district` VALUES ('33044', '昭觉县', 'liangshan');
INSERT INTO `district` VALUES ('33045', '墨脱县', 'linzhi');
INSERT INTO `district` VALUES ('33046', '黄平县', 'qiandongnan');
INSERT INTO `district` VALUES ('33047', '合水县', 'qingyang');
INSERT INTO `district` VALUES ('33048', '三原县', 'xianyang');
INSERT INTO `district` VALUES ('33049', '马尾区', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('33050', '三都水族自治县', 'qiannan');
INSERT INTO `district` VALUES ('33051', '武进区', 'changzhou');
INSERT INTO `district` VALUES ('33052', '津南区', 'tianjin_city');
INSERT INTO `district` VALUES ('33053', '荔波县', 'qiannan');
INSERT INTO `district` VALUES ('33054', '路南区', 'tangshan');
INSERT INTO `district` VALUES ('33055', '新和县', 'akesu');
INSERT INTO `district` VALUES ('33056', '岱岳区', 'taian');
INSERT INTO `district` VALUES ('33057', '斗门区', 'zhuhai');
INSERT INTO `district` VALUES ('33058', '华池县', 'qingyang');
INSERT INTO `district` VALUES ('33059', '东陵区', 'shenyang');
INSERT INTO `district` VALUES ('33060', '南长区', 'wuxi');
INSERT INTO `district` VALUES ('33061', '旺苍县', 'guangyuan');
INSERT INTO `district` VALUES ('33062', '溧水县', 'nanjing');
INSERT INTO `district` VALUES ('33063', '海西蒙古族藏族自治州', 'haixi');
INSERT INTO `district` VALUES ('33064', '富拉尔基区', 'qiqihaer');
INSERT INTO `district` VALUES ('33065', '南溪县', 'yibin');
INSERT INTO `district` VALUES ('33066', '黄浦区', 'shanghai_city');
INSERT INTO `district` VALUES ('33067', '若羌县', 'bayinguoleng');
INSERT INTO `district` VALUES ('33068', '龙子湖区', 'bangbu');
INSERT INTO `district` VALUES ('33069', '翔安区', 'xiamen');
INSERT INTO `district` VALUES ('33070', '华亭县', 'pingliang');
INSERT INTO `district` VALUES ('33071', '禄丰县', 'chuxiong');
INSERT INTO `district` VALUES ('33072', '琼山区', 'haikou');
INSERT INTO `district` VALUES ('33073', '美兰区', 'haikou');
INSERT INTO `district` VALUES ('33074', '平山县', 'shijiazhuang');
INSERT INTO `district` VALUES ('33075', '木垒哈萨克自治县', 'changji');
INSERT INTO `district` VALUES ('33076', '和田地区', 'hetian');
INSERT INTO `district` VALUES ('33077', '长寿区', 'chongqing_city');
INSERT INTO `district` VALUES ('33078', '紫云苗族布依族自治县', 'anshun');
INSERT INTO `district` VALUES ('33079', '遂平县', 'zhumadian');
INSERT INTO `district` VALUES ('33080', '明水县', 'suihua');
INSERT INTO `district` VALUES ('33081', '宁晋县', 'xingtai');
INSERT INTO `district` VALUES ('33082', '乌拉特后旗', 'bayannaoer');
INSERT INTO `district` VALUES ('33083', '安阳县', 'anyang');
INSERT INTO `district` VALUES ('33084', '盐亭县', 'mianyang');
INSERT INTO `district` VALUES ('33085', '尉氏县', 'kaifeng');
INSERT INTO `district` VALUES ('33086', '迪庆藏族自治州', 'diqing');
INSERT INTO `district` VALUES ('33087', '巫山县', 'chongqing_city');
INSERT INTO `district` VALUES ('33088', '响水县', 'yancheng');
INSERT INTO `district` VALUES ('33089', '秀屿区', 'putian');
INSERT INTO `district` VALUES ('33090', '椒江区', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('33091', '通江县', 'bazhong');
INSERT INTO `district` VALUES ('33092', '广河县', 'linxia');
INSERT INTO `district` VALUES ('33093', '尤溪县', 'sanming');
INSERT INTO `district` VALUES ('33094', '西固区', 'lanzhou');
INSERT INTO `district` VALUES ('33095', '博野县', 'baoding');
INSERT INTO `district` VALUES ('33096', '息烽县', 'guiyang');
INSERT INTO `district` VALUES ('33097', '扶绥县', 'chongzuo');
INSERT INTO `district` VALUES ('33098', '昆都仑区', 'baotou');
INSERT INTO `district` VALUES ('33099', '房县', 'shiyan');
INSERT INTO `district` VALUES ('33100', '宁明县', 'chongzuo');
INSERT INTO `district` VALUES ('33101', '米东区', 'wulumuqi');
INSERT INTO `district` VALUES ('33102', '迭部县', 'gannan');
INSERT INTO `district` VALUES ('33103', '宁城县', 'chifeng');
INSERT INTO `district` VALUES ('33104', '怀集县', 'zhaoqing');
INSERT INTO `district` VALUES ('33105', '太湖县', 'anqing');
INSERT INTO `district` VALUES ('33106', '方山县', 'lvliang');
INSERT INTO `district` VALUES ('33107', '浮梁县', 'jingdezhen');
INSERT INTO `district` VALUES ('33108', '诏安县', 'zhangzhou');
INSERT INTO `district` VALUES ('33109', '丰县', 'xuzhou');
INSERT INTO `district` VALUES ('33110', '沙湾区', 'leshan');
INSERT INTO `district` VALUES ('33111', '朗县', 'linzhi');
INSERT INTO `district` VALUES ('33112', '富民县', 'kunming');
INSERT INTO `district` VALUES ('33113', '通道侗族自治县', 'huaihua');
INSERT INTO `district` VALUES ('33114', '薛城区', 'zaozhuang');
INSERT INTO `district` VALUES ('33115', '吉木乃县', 'aletai');
INSERT INTO `district` VALUES ('33116', '保德县', 'xinzhou');
INSERT INTO `district` VALUES ('33117', '桃源县', 'changde');
INSERT INTO `district` VALUES ('33118', '江城区', 'yangjiang');
INSERT INTO `district` VALUES ('33119', '太康县', 'zhoukou');
INSERT INTO `district` VALUES ('33120', '崇信县', 'pingliang');
INSERT INTO `district` VALUES ('33121', '乌达区', 'wuhai');
INSERT INTO `district` VALUES ('33122', '潼南县', 'chongqing_city');
INSERT INTO `district` VALUES ('33123', '岚山区', 'rizhao');
INSERT INTO `district` VALUES ('33124', '六枝特区', 'liupanshui');
INSERT INTO `district` VALUES ('33125', '麦盖提县', 'kashi');
INSERT INTO `district` VALUES ('33126', '定陶县', 'heze');
INSERT INTO `district` VALUES ('33127', '祁县', 'jinzhong');
INSERT INTO `district` VALUES ('33128', '上高县', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('33129', '八道江区', 'baishan');
INSERT INTO `district` VALUES ('33130', '宾县', 'haerbin');
INSERT INTO `district` VALUES ('33131', '犍为县', 'leshan');
INSERT INTO `district` VALUES ('33132', '滴道区', 'jixi');
INSERT INTO `district` VALUES ('33133', '宏伟区', 'liaoyang');
INSERT INTO `district` VALUES ('33134', '栾川县', 'luoyang');
INSERT INTO `district` VALUES ('33135', '孟村回族自治县', 'cangzhou');
INSERT INTO `district` VALUES ('33136', '天等县', 'chongzuo');
INSERT INTO `district` VALUES ('33137', '广饶县', 'dongying');
INSERT INTO `district` VALUES ('33138', '祁连县', 'haibei');
INSERT INTO `district` VALUES ('33139', '古塔区', 'jinzhou');
INSERT INTO `district` VALUES ('33140', '宣州区', 'xuancheng');
INSERT INTO `district` VALUES ('33141', '索县', 'naqu');
INSERT INTO `district` VALUES ('33142', '苍山县', 'linyi');
INSERT INTO `district` VALUES ('33143', '海沧区', 'xiamen');
INSERT INTO `district` VALUES ('33144', '河南蒙古族自治县', 'huangnan');
INSERT INTO `district` VALUES ('33145', '和县', 'chaohu');
INSERT INTO `district` VALUES ('33146', '山海关区', 'qinhuangdao');
INSERT INTO `district` VALUES ('33147', '文圣区', 'liaoyang');
INSERT INTO `district` VALUES ('33148', '大姚县', 'chuxiong');
INSERT INTO `district` VALUES ('33149', '沧浪区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('33150', '武功县', 'xianyang');
INSERT INTO `district` VALUES ('33151', '五原县', 'bayannaoer');
INSERT INTO `district` VALUES ('33152', '晋安区', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('33153', '中阳县', 'lvliang');
INSERT INTO `district` VALUES ('33154', '呼兰区', 'haerbin');
INSERT INTO `district` VALUES ('33155', '白云区', 'guangzhou');
INSERT INTO `district` VALUES ('33156', '阜城县', 'hengshui');
INSERT INTO `district` VALUES ('33157', '中原区', 'zhengzhou');
INSERT INTO `district` VALUES ('33158', '塔什库尔干塔吉克自治县', 'kashi');
INSERT INTO `district` VALUES ('33159', '洛南县', 'shangluo');
INSERT INTO `district` VALUES ('33160', '嘉陵区', 'nanchong');
INSERT INTO `district` VALUES ('33162', '永嘉县', 'wenzhou');
INSERT INTO `district` VALUES ('33163', '峨山彝族自治县', 'yuxi');
INSERT INTO `district` VALUES ('33164', '旬阳县', 'ankang');
INSERT INTO `district` VALUES ('33165', '谢通门县', 'rikaze');
INSERT INTO `district` VALUES ('33166', '淅川县', 'nanyang');
INSERT INTO `district` VALUES ('33167', '徐闻县', 'zhanjiang');
INSERT INTO `district` VALUES ('33168', '美姑县', 'liangshan');
INSERT INTO `district` VALUES ('33169', '莘县', 'liaocheng');
INSERT INTO `district` VALUES ('33170', '红原县', 'aba');
INSERT INTO `district` VALUES ('33171', '秦安县', 'tianshui');
INSERT INTO `district` VALUES ('33172', '西华县', 'zhoukou');
INSERT INTO `district` VALUES ('33173', '宁县', 'qingyang');
INSERT INTO `district` VALUES ('33174', '桦南县', 'jiamusi');
INSERT INTO `district` VALUES ('33175', '剑河县', 'qiandongnan');
INSERT INTO `district` VALUES ('33176', '香洲区', 'zhuhai');
INSERT INTO `district` VALUES ('33177', '马边彝族自治县', 'leshan');
INSERT INTO `district` VALUES ('33178', '长丰县', 'hefei');
INSERT INTO `district` VALUES ('33179', '滦县', 'tangshan');
INSERT INTO `district` VALUES ('33180', '石阡县', 'tongren');
INSERT INTO `district` VALUES ('33181', '平安县', 'haidong');
INSERT INTO `district` VALUES ('33182', '贺兰县', 'yinchuan');
INSERT INTO `district` VALUES ('33183', '双江拉祜族佤族布朗族傣族自治县', 'lincang');
INSERT INTO `district` VALUES ('33184', '容县', 'yulin_guangxi');
INSERT INTO `district` VALUES ('33185', '雁山区', 'guilin');
INSERT INTO `district` VALUES ('33186', '云岩区', 'guiyang');
INSERT INTO `district` VALUES ('33187', '正蓝旗', 'xilinguole');
INSERT INTO `district` VALUES ('33188', '通海县', 'yuxi');
INSERT INTO `district` VALUES ('33189', '保靖县', 'xiangxi');
INSERT INTO `district` VALUES ('33190', '宽城满族自治县', 'chengde');
INSERT INTO `district` VALUES ('33191', '木里藏族自治县', 'liangshan');
INSERT INTO `district` VALUES ('33192', '长安区', 'shijiazhuang');
INSERT INTO `district` VALUES ('33193', '安新县', 'baoding');
INSERT INTO `district` VALUES ('33194', '肥西县', 'hefei');
INSERT INTO `district` VALUES ('33195', '兰坪白族普米族自治县', 'nujiang');
INSERT INTO `district` VALUES ('33196', '长白朝鲜族自治县', 'baishan');
INSERT INTO `district` VALUES ('33197', '武宣县', 'laibin');
INSERT INTO `district` VALUES ('33198', '北关区', 'anyang');
INSERT INTO `district` VALUES ('33199', '子洲县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('33200', '太和县', 'fuyang_anhui');
INSERT INTO `district` VALUES ('33201', '寿阳县', 'jinzhong');
INSERT INTO `district` VALUES ('33202', '烈山区', 'huaibei');
INSERT INTO `district` VALUES ('33203', '大英县', 'suining');
INSERT INTO `district` VALUES ('33204', '肇源县', 'daqing');
INSERT INTO `district` VALUES ('33205', '延川县', 'yanan');
INSERT INTO `district` VALUES ('33206', '雁峰区', 'hengyang');
INSERT INTO `district` VALUES ('33207', '湘阴县', 'yueyang');
INSERT INTO `district` VALUES ('33208', '周村区', 'zibo');
INSERT INTO `district` VALUES ('33209', '西山区', 'kunming');
INSERT INTO `district` VALUES ('33210', '筠连县', 'yibin');
INSERT INTO `district` VALUES ('33211', '庆云县', 'dezhou');
INSERT INTO `district` VALUES ('33212', '和静县', 'bayinguoleng');
INSERT INTO `district` VALUES ('33213', '远安县', 'yichang');
INSERT INTO `district` VALUES ('33214', '嘉善县', 'jiaxing');
INSERT INTO `district` VALUES ('33215', '石峰区', 'zhuzhou');
INSERT INTO `district` VALUES ('33216', '和布克赛尔蒙古自治县', 'tacheng');
INSERT INTO `district` VALUES ('33217', '岢岚县', 'xinzhou');
INSERT INTO `district` VALUES ('33218', '宣化区', 'zhangjiakou');
INSERT INTO `district` VALUES ('33219', '鱼峰区', 'liuzhou');
INSERT INTO `district` VALUES ('33220', '江津区', 'chongqing_city');
INSERT INTO `district` VALUES ('33221', '鼓楼区', 'xuzhou');
INSERT INTO `district` VALUES ('33222', '正宁县', 'qingyang');
INSERT INTO `district` VALUES ('33223', '瀍河回族区', 'luoyang');
INSERT INTO `district` VALUES ('33225', '岳池县', 'guangan');
INSERT INTO `district` VALUES ('33226', '左权县', 'jinzhong');
INSERT INTO `district` VALUES ('33227', '达孜县', 'lasa');
INSERT INTO `district` VALUES ('33228', '门源回族自治县', 'haibei');
INSERT INTO `district` VALUES ('33229', '类乌齐县', 'changdu');
INSERT INTO `district` VALUES ('33230', '永靖县', 'linxia');
INSERT INTO `district` VALUES ('33231', '天全县', 'yaan');
INSERT INTO `district` VALUES ('33232', '容城县', 'baoding');
INSERT INTO `district` VALUES ('33233', '元坝区', 'guangyuan');
INSERT INTO `district` VALUES ('33234', '东区', 'panzhihua');
INSERT INTO `district` VALUES ('33235', '耿马傣族佤族自治县', 'lincang');
INSERT INTO `district` VALUES ('33236', '无棣县', 'binzhou');
INSERT INTO `district` VALUES ('33237', '河东区', 'tianjin_city');
INSERT INTO `district` VALUES ('33238', '麻章区', 'zhanjiang');
INSERT INTO `district` VALUES ('33239', '庄浪县', 'pingliang');
INSERT INTO `district` VALUES ('33240', '吴中区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('33241', '崇明县', 'shanghai_city');
INSERT INTO `district` VALUES ('33242', '珠山区', 'jingdezhen');
INSERT INTO `district` VALUES ('33243', '米脂县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('33244', '红花岗区', 'zunyi');
INSERT INTO `district` VALUES ('33245', '滨海县', 'yancheng');
INSERT INTO `district` VALUES ('33246', '全椒县', 'chuzhou');
INSERT INTO `district` VALUES ('33247', '盐山县', 'cangzhou');
INSERT INTO `district` VALUES ('33248', '延平区', 'nanping');
INSERT INTO `district` VALUES ('33249', '罗江县', 'deyang');
INSERT INTO `district` VALUES ('33250', '新邵县', 'shaoyang');
INSERT INTO `district` VALUES ('33251', '陆川县', 'yulin_guangxi');
INSERT INTO `district` VALUES ('33252', '怀安县', 'zhangjiakou');
INSERT INTO `district` VALUES ('33253', '红安县', 'huanggang');
INSERT INTO `district` VALUES ('33254', '梁园区', 'shangqiu');
INSERT INTO `district` VALUES ('33255', '巨鹿县', 'xingtai');
INSERT INTO `district` VALUES ('33256', '城厢区', 'putian');
INSERT INTO `district` VALUES ('33257', '和平区', 'tianjin_city');
INSERT INTO `district` VALUES ('33258', '满城县', 'baoding');
INSERT INTO `district` VALUES ('33259', '台儿庄区', 'zaozhuang');
INSERT INTO `district` VALUES ('33260', '青神县', 'meishan');
INSERT INTO `district` VALUES ('33261', '黄陵县', 'yanan');
INSERT INTO `district` VALUES ('33262', '长宁县', 'yibin');
INSERT INTO `district` VALUES ('33263', '坡头区', 'zhanjiang');
INSERT INTO `district` VALUES ('33264', '兴海县', 'hainanzangzu');
INSERT INTO `district` VALUES ('33265', '古田县', 'ningde');
INSERT INTO `district` VALUES ('33266', '沈丘县', 'zhoukou');
INSERT INTO `district` VALUES ('33267', '安吉县', 'huzhou');
INSERT INTO `district` VALUES ('33268', '涧西区', 'luoyang');
INSERT INTO `district` VALUES ('33269', '蓝田县', 'xian');
INSERT INTO `district` VALUES ('33270', '靖西县', 'baise');
INSERT INTO `district` VALUES ('33271', '久治县', 'guoluo');
INSERT INTO `district` VALUES ('33272', '剑阁县', 'guangyuan');
INSERT INTO `district` VALUES ('33273', '永定区', 'zhangjiajie');
INSERT INTO `district` VALUES ('33274', '绥滨县', 'hegang');
INSERT INTO `district` VALUES ('33275', '柳北区', 'liuzhou');
INSERT INTO `district` VALUES ('33276', '万州区', 'chongqing_city');
INSERT INTO `district` VALUES ('33277', '建始县', 'enshi');
INSERT INTO `district` VALUES ('33278', '兰考县', 'kaifeng');
INSERT INTO `district` VALUES ('33279', '阿鲁科尔沁旗', 'chifeng');
INSERT INTO `district` VALUES ('33280', '城中区', 'liuzhou');
INSERT INTO `district` VALUES ('33281', '林芝地区', 'linzhi');
INSERT INTO `district` VALUES ('33282', '红岗区', 'daqing');
INSERT INTO `district` VALUES ('33283', '海原县', 'zhongwei');
INSERT INTO `district` VALUES ('33284', '黄梅县', 'huanggang');
INSERT INTO `district` VALUES ('33285', '祁东县', 'hengyang');
INSERT INTO `district` VALUES ('33286', '古县', 'linfen');
INSERT INTO `district` VALUES ('33287', '惠阳区', 'huizhou_guangdong');
INSERT INTO `district` VALUES ('33288', '涉县', 'handan');
INSERT INTO `district` VALUES ('33289', '黄石港区', 'huangshi');
INSERT INTO `district` VALUES ('33290', '建华区', 'qiqihaer');
INSERT INTO `district` VALUES ('33291', '博湖县', 'bayinguoleng');
INSERT INTO `district` VALUES ('33292', '华容县', 'yueyang');
INSERT INTO `district` VALUES ('33293', '三元区', 'sanming');
INSERT INTO `district` VALUES ('33294', '绿园区', 'changchun');
INSERT INTO `district` VALUES ('33295', '浔阳区', 'jiujiang');
INSERT INTO `district` VALUES ('33296', '兴文县', 'yibin');
INSERT INTO `district` VALUES ('33297', '铁山港区', 'beihai');
INSERT INTO `district` VALUES ('33298', '若尔盖县', 'aba');
INSERT INTO `district` VALUES ('33299', '天河区', 'guangzhou');
INSERT INTO `district` VALUES ('33300', '桂东县', 'chenzhou');
INSERT INTO `district` VALUES ('33301', '罗山县', 'xinyang');
INSERT INTO `district` VALUES ('33302', '盘县', 'liupanshui');
INSERT INTO `district` VALUES ('33303', '五华县', 'meizhou');
INSERT INTO `district` VALUES ('33304', '宁蒗彝族自治县', 'lijiang');
INSERT INTO `district` VALUES ('33305', '微山县', 'jining');
INSERT INTO `district` VALUES ('33306', '临澧县', 'changde');
INSERT INTO `district` VALUES ('33307', '巴青县', 'naqu');
INSERT INTO `district` VALUES ('33308', '盐源县', 'liangshan');
INSERT INTO `district` VALUES ('33309', '西安区', 'liaoyuan');
INSERT INTO `district` VALUES ('33310', '海淀区', 'beijing_city');
INSERT INTO `district` VALUES ('33311', '呈贡县', 'kunming');
INSERT INTO `district` VALUES ('33312', '昭阳区', 'zhaotong');
INSERT INTO `district` VALUES ('33313', '安图县', 'yanbian');
INSERT INTO `district` VALUES ('33314', '洮北区', 'baicheng');
INSERT INTO `district` VALUES ('33315', '罗平县', 'qujing');
INSERT INTO `district` VALUES ('33316', '东兴区', 'neijiang');
INSERT INTO `district` VALUES ('33317', '肃州区', 'jiuquan');
INSERT INTO `district` VALUES ('33318', '上饶县', 'shangrao');
INSERT INTO `district` VALUES ('33319', '叶城县', 'kashi');
INSERT INTO `district` VALUES ('33320', '佛冈县', 'qingyuan');
INSERT INTO `district` VALUES ('33321', '昌图县', 'tieling');
INSERT INTO `district` VALUES ('33322', '清水河县', 'huhehaote');
INSERT INTO `district` VALUES ('33323', '资中县', 'neijiang');
INSERT INTO `district` VALUES ('33324', '元江哈尼族彝族傣族自治县', 'yuxi');
INSERT INTO `district` VALUES ('33325', '措美县', 'shannan');
INSERT INTO `district` VALUES ('33326', '密云县', 'beijing_city');
INSERT INTO `district` VALUES ('33327', '平果县', 'baise');
INSERT INTO `district` VALUES ('33328', '原州区', 'guyuan');
INSERT INTO `district` VALUES ('33329', '静安区', 'shanghai_city');
INSERT INTO `district` VALUES ('33330', '塔城地区', 'tacheng');
INSERT INTO `district` VALUES ('33331', '正安县', 'zunyi');
INSERT INTO `district` VALUES ('33332', '西双版纳傣族自治州', 'xishuangbanna');
INSERT INTO `district` VALUES ('33333', '玄武区', 'nanjing');
INSERT INTO `district` VALUES ('33334', '盘山县', 'panjin');
INSERT INTO `district` VALUES ('33335', '武昌区', 'wuhan');
INSERT INTO `district` VALUES ('33336', '泉港区', 'quanzhou');
INSERT INTO `district` VALUES ('33337', '广陵区', 'yangzhou');
INSERT INTO `district` VALUES ('33338', '开封县', 'kaifeng');
INSERT INTO `district` VALUES ('33339', '洞口县', 'shaoyang');
INSERT INTO `district` VALUES ('33340', '梨树县', 'siping');
INSERT INTO `district` VALUES ('33341', '碌曲县', 'gannan');
INSERT INTO `district` VALUES ('33342', '策勒县', 'hetian');
INSERT INTO `district` VALUES ('33343', '正阳县', 'zhumadian');
INSERT INTO `district` VALUES ('33344', '凌河区', 'jinzhou');
INSERT INTO `district` VALUES ('33345', '武鸣县', 'nanning');
INSERT INTO `district` VALUES ('33346', '濠江区', 'shantou');
INSERT INTO `district` VALUES ('33347', '龙亭区', 'kaifeng');
INSERT INTO `district` VALUES ('33348', '礼泉县', 'xianyang');
INSERT INTO `district` VALUES ('33349', '曹县', 'heze');
INSERT INTO `district` VALUES ('33350', '东宁县', 'mudanjiang');
INSERT INTO `district` VALUES ('33351', '集宁区', 'wulanchabu');
INSERT INTO `district` VALUES ('33352', '内乡县', 'nanyang');
INSERT INTO `district` VALUES ('33353', '桥东区', 'shijiazhuang');
INSERT INTO `district` VALUES ('33354', '运河区', 'cangzhou');
INSERT INTO `district` VALUES ('33355', '淇滨区', 'hebi');
INSERT INTO `district` VALUES ('33356', '那曲县', 'naqu');
INSERT INTO `district` VALUES ('33357', '始兴县', 'shaoguan');
INSERT INTO `district` VALUES ('33358', '巴州区', 'bazhong');
INSERT INTO `district` VALUES ('33359', '清苑县', 'baoding');
INSERT INTO `district` VALUES ('33360', '宁强县', 'hanzhong');
INSERT INTO `district` VALUES ('33361', '祁阳县', 'yongzhou');
INSERT INTO `district` VALUES ('33362', '朝阳区', 'changchun');
INSERT INTO `district` VALUES ('33363', '民乐县', 'zhangye');
INSERT INTO `district` VALUES ('33364', '青浦区', 'shanghai_city');
INSERT INTO `district` VALUES ('33365', '万山特区', 'tongren');
INSERT INTO `district` VALUES ('33366', '开县', 'chongqing_city');
INSERT INTO `district` VALUES ('33367', '黎平县', 'qiandongnan');
INSERT INTO `district` VALUES ('33368', '肃宁县', 'cangzhou');
INSERT INTO `district` VALUES ('33369', '平江区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('33370', '乐亭县', 'tangshan');
INSERT INTO `district` VALUES ('33371', '平昌县', 'bazhong');
INSERT INTO `district` VALUES ('33372', '吉隆县', 'rikaze');
INSERT INTO `district` VALUES ('33373', '科尔沁左翼后旗', 'tongliao');
INSERT INTO `district` VALUES ('33374', '三门县', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('33375', '依安县', 'qiqihaer');
INSERT INTO `district` VALUES ('33376', '德化县', 'quanzhou');
INSERT INTO `district` VALUES ('33377', '镇原县', 'qingyang');
INSERT INTO `district` VALUES ('33378', '沙河口区', 'dalian');
INSERT INTO `district` VALUES ('33379', '沿河土家族自治县', 'tongren');
INSERT INTO `district` VALUES ('33380', '桓仁满族自治县', 'benxi');
INSERT INTO `district` VALUES ('33381', '鹿邑县', 'zhoukou');
INSERT INTO `district` VALUES ('33382', '东风区', 'jiamusi');
INSERT INTO `district` VALUES ('33383', '源汇区', 'luohe');
INSERT INTO `district` VALUES ('33384', '哈巴河县', 'aletai');
INSERT INTO `district` VALUES ('33385', '江北区', 'chongqing_city');
INSERT INTO `district` VALUES ('33386', '迎泽区', 'taiyuan');
INSERT INTO `district` VALUES ('33387', '周至县', 'xian');
INSERT INTO `district` VALUES ('33388', '惠济区', 'zhengzhou');
INSERT INTO `district` VALUES ('33389', '道里区', 'haerbin');
INSERT INTO `district` VALUES ('33390', '綦江县', 'chongqing_city');
INSERT INTO `district` VALUES ('33391', '象山区', 'guilin');
INSERT INTO `district` VALUES ('33392', '沂水县', 'linyi');
INSERT INTO `district` VALUES ('33393', '阳城县', 'jincheng');
INSERT INTO `district` VALUES ('33394', '富源县', 'qujing');
INSERT INTO `district` VALUES ('33395', '黄岛区', 'qingdao');
INSERT INTO `district` VALUES ('33396', '嘉禾县', 'chenzhou');
INSERT INTO `district` VALUES ('33397', '榕江县', 'qiandongnan');
INSERT INTO `district` VALUES ('33398', '民权县', 'shangqiu');
INSERT INTO `district` VALUES ('33399', '东至县', 'chizhou');
INSERT INTO `district` VALUES ('33400', '苍梧县', 'wuzhou');
INSERT INTO `district` VALUES ('33401', '普兰县', 'ali');
INSERT INTO `district` VALUES ('33402', '广德县', 'xuancheng');
INSERT INTO `district` VALUES ('33403', '宝应县', 'yangzhou');
INSERT INTO `district` VALUES ('33404', '新华区', 'shijiazhuang');
INSERT INTO `district` VALUES ('33405', '徽州区', 'huangshan');
INSERT INTO `district` VALUES ('33406', '上栗县', 'pingxiang');
INSERT INTO `district` VALUES ('33407', '郫县', 'chengdu');
INSERT INTO `district` VALUES ('33408', '开平区', 'tangshan');
INSERT INTO `district` VALUES ('33409', '岳西县', 'anqing');
INSERT INTO `district` VALUES ('33410', '西城区', 'beijing_city');
INSERT INTO `district` VALUES ('33411', '饶阳县', 'hengshui');
INSERT INTO `district` VALUES ('33412', '西秀区', 'anshun');
INSERT INTO `district` VALUES ('33413', '静宁县', 'pingliang');
INSERT INTO `district` VALUES ('33414', '伊吾县', 'hami');
INSERT INTO `district` VALUES ('33415', '乾县', 'xianyang');
INSERT INTO `district` VALUES ('33416', '雁塔区', 'xian');
INSERT INTO `district` VALUES ('33417', '铁山区', 'huangshi');
INSERT INTO `district` VALUES ('33418', '方正县', 'haerbin');
INSERT INTO `district` VALUES ('33419', '西安区', 'mudanjiang');
INSERT INTO `district` VALUES ('33420', '同安区', 'xiamen');
INSERT INTO `district` VALUES ('33421', '蕉城区', 'ningde');
INSERT INTO `district` VALUES ('33422', '秦都区', 'xianyang');
INSERT INTO `district` VALUES ('33423', '江洲区', 'chongzuo');
INSERT INTO `district` VALUES ('33424', '孝南区', 'xiaogan');
INSERT INTO `district` VALUES ('33425', '沙坡头区', 'zhongwei');
INSERT INTO `district` VALUES ('33426', '嘉黎县', 'naqu');
INSERT INTO `district` VALUES ('33427', '丹凤县', 'shangluo');
INSERT INTO `district` VALUES ('33428', '杨浦区', 'shanghai_city');
INSERT INTO `district` VALUES ('33429', '衡南县', 'hengyang');
INSERT INTO `district` VALUES ('33430', '曲江区', 'shaoguan');
INSERT INTO `district` VALUES ('33431', '进贤县', 'nanchang');
INSERT INTO `district` VALUES ('33432', '南岸区', 'chongqing_city');
INSERT INTO `district` VALUES ('33433', '达拉特旗', 'eerduosi');
INSERT INTO `district` VALUES ('33434', '望江县', 'anqing');
INSERT INTO `district` VALUES ('33435', '芷江侗族自治县', 'huaihua');
INSERT INTO `district` VALUES ('33436', '马尔康县', 'aba');
INSERT INTO `district` VALUES ('33437', '港闸区', 'nantong');
INSERT INTO `district` VALUES ('33438', '安塞县', 'yanan');
INSERT INTO `district` VALUES ('33439', '通化县', 'tonghua');
INSERT INTO `district` VALUES ('33440', '大关县', 'zhaotong');
INSERT INTO `district` VALUES ('33441', '留坝县', 'hanzhong');
INSERT INTO `district` VALUES ('33442', '下花园区', 'zhangjiakou');
INSERT INTO `district` VALUES ('33443', '四方区', 'qingdao');
INSERT INTO `district` VALUES ('33444', '寿宁县', 'ningde');
INSERT INTO `district` VALUES ('33445', '金平区', 'shantou');
INSERT INTO `district` VALUES ('33446', '九江县', 'jiujiang');
INSERT INTO `district` VALUES ('33447', '曲沃县', 'linfen');
INSERT INTO `district` VALUES ('33448', '梓潼县', 'mianyang');
INSERT INTO `district` VALUES ('33449', '青冈县', 'suihua');
INSERT INTO `district` VALUES ('33450', '徐汇区', 'shanghai_city');
INSERT INTO `district` VALUES ('33451', '尉犁县', 'bayinguoleng');
INSERT INTO `district` VALUES ('33452', '新蔡县', 'zhumadian');
INSERT INTO `district` VALUES ('33453', '乡城县', 'ganzi');
INSERT INTO `district` VALUES ('33454', '尖扎县', 'huangnan');
INSERT INTO `district` VALUES ('33455', '当雄县', 'lasa');
INSERT INTO `district` VALUES ('33456', '临夏县', 'linxia');
INSERT INTO `district` VALUES ('33457', '务川仡佬族苗族自治县', 'zunyi');
INSERT INTO `district` VALUES ('33458', '天元区', 'zhuzhou');
INSERT INTO `district` VALUES ('33459', '华坪县', 'lijiang');
INSERT INTO `district` VALUES ('33461', '改则县', 'ali');
INSERT INTO `district` VALUES ('33462', '罗田县', 'huanggang');
INSERT INTO `district` VALUES ('33463', '玉屏侗族自治县', 'tongren');
INSERT INTO `district` VALUES ('33464', '海州区', 'lianyungang');
INSERT INTO `district` VALUES ('33465', '溆浦县', 'huaihua');
INSERT INTO `district` VALUES ('33466', '冕宁县', 'liangshan');
INSERT INTO `district` VALUES ('33467', '永定县', 'longyan');
INSERT INTO `district` VALUES ('33468', '湛河区', 'pingdingshan');
INSERT INTO `district` VALUES ('33469', '沅陵县', 'huaihua');
INSERT INTO `district` VALUES ('33470', '历城区', 'jinan');
INSERT INTO `district` VALUES ('33471', '黎城县', 'changzhi');
INSERT INTO `district` VALUES ('33472', '莲都区', 'lishui');
INSERT INTO `district` VALUES ('33473', '米易县', 'panzhihua');
INSERT INTO `district` VALUES ('33474', '承德县', 'chengde');
INSERT INTO `district` VALUES ('33475', '娄星区', 'loudi');
INSERT INTO `district` VALUES ('33476', '东辽县', 'liaoyuan');
INSERT INTO `district` VALUES ('33477', '宁江区', 'songyuan');
INSERT INTO `district` VALUES ('33478', '尚义县', 'zhangjiakou');
INSERT INTO `district` VALUES ('33479', '江海区', 'jiangmen');
INSERT INTO `district` VALUES ('33480', '宽甸满族自治县', 'dandong');
INSERT INTO `district` VALUES ('33481', '铜鼓县', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('33482', '勉县', 'hanzhong');
INSERT INTO `district` VALUES ('33483', '衡东县', 'hengyang');
INSERT INTO `district` VALUES ('33484', '望都县', 'baoding');
INSERT INTO `district` VALUES ('33485', '回民区', 'huhehaote');
INSERT INTO `district` VALUES ('33486', '云县', 'lincang');
INSERT INTO `district` VALUES ('33487', '鄄城县', 'heze');
INSERT INTO `district` VALUES ('33488', '盐边县', 'panzhihua');
INSERT INTO `district` VALUES ('33489', '卢龙县', 'qinhuangdao');
INSERT INTO `district` VALUES ('33490', '古浪县', 'wuwei');
INSERT INTO `district` VALUES ('33491', '东洲区', 'fushun');
INSERT INTO `district` VALUES ('33492', '定襄县', 'xinzhou');
INSERT INTO `district` VALUES ('33493', '独山县', 'qiannan');
INSERT INTO `district` VALUES ('33494', '乌审旗', 'eerduosi');
INSERT INTO `district` VALUES ('33495', '师宗县', 'qujing');
INSERT INTO `district` VALUES ('33496', '文山县', 'wenshan');
INSERT INTO `district` VALUES ('33497', '石渠县', 'ganzi');
INSERT INTO `district` VALUES ('33498', '甘德县', 'guoluo');
INSERT INTO `district` VALUES ('33499', '潮阳区', 'shantou');
INSERT INTO `district` VALUES ('33500', '永德县', 'lincang');
INSERT INTO `district` VALUES ('33501', '兴隆县', 'chengde');
INSERT INTO `district` VALUES ('33502', '安泽县', 'linfen');
INSERT INTO `district` VALUES ('33503', '邕宁区', 'nanning');
INSERT INTO `district` VALUES ('33504', '顺昌县', 'nanping');
INSERT INTO `district` VALUES ('33505', '梅列区', 'sanming');
INSERT INTO `district` VALUES ('33506', '蓬安县', 'nanchong');
INSERT INTO `district` VALUES ('33507', '石楼县', 'lvliang');
INSERT INTO `district` VALUES ('33508', '定远县', 'chuzhou');
INSERT INTO `district` VALUES ('33509', '徽县', 'longnan');
INSERT INTO `district` VALUES ('33510', '松桃苗族自治县', 'tongren');
INSERT INTO `district` VALUES ('33511', '庐山区', 'jiujiang');
INSERT INTO `district` VALUES ('33512', '眉县', 'baoji');
INSERT INTO `district` VALUES ('33513', '镇平县', 'nanyang');
INSERT INTO `district` VALUES ('33514', '清河区', 'huaian');
INSERT INTO `district` VALUES ('33515', '武都区', 'longnan');
INSERT INTO `district` VALUES ('33516', '含山县', 'chaohu');
INSERT INTO `district` VALUES ('33517', '井陉矿区', 'shijiazhuang');
INSERT INTO `district` VALUES ('33518', '平和县', 'zhangzhou');
INSERT INTO `district` VALUES ('33519', '鸡冠区', 'jixi');
INSERT INTO `district` VALUES ('33520', '大兴区', 'beijing_city');
INSERT INTO `district` VALUES ('33521', '砚山县', 'wenshan');
INSERT INTO `district` VALUES ('33522', '平远县', 'meizhou');
INSERT INTO `district` VALUES ('33523', '尼玛县', 'naqu');
INSERT INTO `district` VALUES ('33524', '蕲春县', 'huanggang');
INSERT INTO `district` VALUES ('33525', '金川区', 'jinchang');
INSERT INTO `district` VALUES ('33526', '玉环县', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('33527', '石景山区', 'beijing_city');
INSERT INTO `district` VALUES ('33528', '黄陂区', 'wuhan');
INSERT INTO `district` VALUES ('33529', '平遥县', 'jinzhong');
INSERT INTO `district` VALUES ('33530', '临武县', 'chenzhou');
INSERT INTO `district` VALUES ('33531', '武侯区', 'chengdu');
INSERT INTO `district` VALUES ('33532', '镇远县', 'qiandongnan');
INSERT INTO `district` VALUES ('33533', '象州县', 'laibin');
INSERT INTO `district` VALUES ('33534', '元谋县', 'chuxiong');
INSERT INTO `district` VALUES ('33535', '新宾满族自治县', 'fushun');
INSERT INTO `district` VALUES ('33536', '蒙城县', 'bozhou');
INSERT INTO `district` VALUES ('33537', '汾西县', 'linfen');
INSERT INTO `district` VALUES ('33538', '歙县', 'huangshan');
INSERT INTO `district` VALUES ('33539', '禄劝彝族苗族自治县', 'kunming');
INSERT INTO `district` VALUES ('33540', '井陉县', 'shijiazhuang');
INSERT INTO `district` VALUES ('33541', '武宁县', 'jiujiang');
INSERT INTO `district` VALUES ('33542', '山城区', 'hebi');
INSERT INTO `district` VALUES ('33543', '马村区', 'jiaozuo');
INSERT INTO `district` VALUES ('33544', '信州区', 'shangrao');
INSERT INTO `district` VALUES ('33545', '吉县', 'linfen');
INSERT INTO `district` VALUES ('33546', '清河区', 'tieling');
INSERT INTO `district` VALUES ('33547', '铁锋区', 'qiqihaer');
INSERT INTO `district` VALUES ('33548', '怒江傈僳族自治州', 'nujiang');
INSERT INTO `district` VALUES ('33549', '乐东黎族自治县', 'ledong');
INSERT INTO `district` VALUES ('33550', '西林县', 'baise');
INSERT INTO `district` VALUES ('33551', '西丰县', 'tieling');
INSERT INTO `district` VALUES ('33552', '通州区', 'beijing_city');
INSERT INTO `district` VALUES ('33553', '布尔津县', 'aletai');
INSERT INTO `district` VALUES ('33554', '勐腊县', 'xishuangbanna');
INSERT INTO `district` VALUES ('33555', '修武县', 'jiaozuo');
INSERT INTO `district` VALUES ('33556', '湟中县', 'xining');
INSERT INTO `district` VALUES ('33557', '新宁县', 'shaoyang');
INSERT INTO `district` VALUES ('33558', '申扎县', 'naqu');
INSERT INTO `district` VALUES ('33559', '循化撒拉族自治县', 'haidong');
INSERT INTO `district` VALUES ('33560', '蓝山县', 'yongzhou');
INSERT INTO `district` VALUES ('33561', '临邑县', 'dezhou');
INSERT INTO `district` VALUES ('33562', '灌云县', 'lianyungang');
INSERT INTO `district` VALUES ('33563', '彭阳县', 'guyuan');
INSERT INTO `district` VALUES ('33564', '多伦县', 'xilinguole');
INSERT INTO `district` VALUES ('33565', '常山县', 'quzhou');
INSERT INTO `district` VALUES ('33566', '忻府区', 'xinzhou');
INSERT INTO `district` VALUES ('33567', '色达县', 'ganzi');
INSERT INTO `district` VALUES ('33568', '奉节县', 'chongqing_city');
INSERT INTO `district` VALUES ('33569', '崇川区', 'nantong');
INSERT INTO `district` VALUES ('33570', '龙城区', 'chaoyang');
INSERT INTO `district` VALUES ('33571', '凤冈县', 'zunyi');
INSERT INTO `district` VALUES ('33572', '鹤峰县', 'enshi');
INSERT INTO `district` VALUES ('33573', '泸定县', 'ganzi');
INSERT INTO `district` VALUES ('33574', '隆子县', 'shannan');
INSERT INTO `district` VALUES ('33575', '广宁县', 'zhaoqing');
INSERT INTO `district` VALUES ('33576', '庆安县', 'suihua');
INSERT INTO `district` VALUES ('33577', '端州区', 'zhaoqing');
INSERT INTO `district` VALUES ('33578', '渝水区', 'xinyu');
INSERT INTO `district` VALUES ('33579', '湖口县', 'jiujiang');
INSERT INTO `district` VALUES ('33580', '新巴尔虎右旗', 'hulunbeier');
INSERT INTO `district` VALUES ('33581', '上杭县', 'longyan');
INSERT INTO `district` VALUES ('33582', '农安县', 'changchun');
INSERT INTO `district` VALUES ('33583', '金川县', 'aba');
INSERT INTO `district` VALUES ('33584', '临夏回族自治州', 'linxia');
INSERT INTO `district` VALUES ('33585', '额敏县', 'tacheng');
INSERT INTO `district` VALUES ('33586', '东丽区', 'tianjin_city');
INSERT INTO `district` VALUES ('33587', '定日县', 'rikaze');
INSERT INTO `district` VALUES ('33588', '玛多县', 'guoluo');
INSERT INTO `district` VALUES ('33589', '东兰县', 'hechi');
INSERT INTO `district` VALUES ('33590', '山阴县', 'shuozhou');
INSERT INTO `district` VALUES ('33591', '张北县', 'zhangjiakou');
INSERT INTO `district` VALUES ('33592', '灵寿县', 'shijiazhuang');
INSERT INTO `district` VALUES ('33593', '滑县', 'anyang');
INSERT INTO `district` VALUES ('33594', '通河县', 'haerbin');
INSERT INTO `district` VALUES ('33595', '太和区', 'jinzhou');
INSERT INTO `district` VALUES ('33596', '镇安县', 'shangluo');
INSERT INTO `district` VALUES ('33597', '广安区', 'guangan');
INSERT INTO `district` VALUES ('33598', '青龙满族自治县', 'qinhuangdao');
INSERT INTO `district` VALUES ('33599', '叠彩区', 'guilin');
INSERT INTO `district` VALUES ('33600', '洛宁县', 'luoyang');
INSERT INTO `district` VALUES ('33601', '肃南裕固族自治县', 'zhangye');
INSERT INTO `district` VALUES ('33602', '北川羌族自治县', 'mianyang');
INSERT INTO `district` VALUES ('33603', '鸠江区', 'wuhu');
INSERT INTO `district` VALUES ('33604', '公安县', 'jingzhou');
INSERT INTO `district` VALUES ('33605', '桥西区', 'shijiazhuang');
INSERT INTO `district` VALUES ('33606', '高阳县', 'baoding');
INSERT INTO `district` VALUES ('33607', '漠河县', 'daxinganling');
INSERT INTO `district` VALUES ('33608', '惠山区', 'wuxi');
INSERT INTO `district` VALUES ('33609', '让胡路区', 'daqing');
INSERT INTO `district` VALUES ('33610', '嘉定区', 'shanghai_city');
INSERT INTO `district` VALUES ('33611', '峨边彝族自治县', 'leshan');
INSERT INTO `district` VALUES ('33612', '卫东区', 'pingdingshan');
INSERT INTO `district` VALUES ('33613', '蓬溪县', 'suining');
INSERT INTO `district` VALUES ('33614', '新河县', 'xingtai');
INSERT INTO `district` VALUES ('33615', '吴堡县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('33616', '银州区', 'tieling');
INSERT INTO `district` VALUES ('33617', '喀喇沁旗', 'chifeng');
INSERT INTO `district` VALUES ('33618', '磴口县', 'bayannaoer');
INSERT INTO `district` VALUES ('33619', '铁岭县', 'tieling');
INSERT INTO `district` VALUES ('33620', '武江区', 'shaoguan');
INSERT INTO `district` VALUES ('33621', '昌都地区', 'changdu');
INSERT INTO `district` VALUES ('33622', '蔚县', 'zhangjiakou');
INSERT INTO `district` VALUES ('33623', '瑶海区', 'hefei');
INSERT INTO `district` VALUES ('33624', '江达县', 'changdu');
INSERT INTO `district` VALUES ('33625', '汤原县', 'jiamusi');
INSERT INTO `district` VALUES ('33626', '白云区', 'guiyang');
INSERT INTO `district` VALUES ('33627', '萧山区', 'hangzhou');
INSERT INTO `district` VALUES ('33628', '肥乡县', 'handan');
INSERT INTO `district` VALUES ('33629', '甘洛县', 'liangshan');
INSERT INTO `district` VALUES ('33630', '鸡泽县', 'handan');
INSERT INTO `district` VALUES ('33631', '中牟县', 'zhengzhou');
INSERT INTO `district` VALUES ('33632', '丰南区', 'tangshan');
INSERT INTO `district` VALUES ('33633', '昂仁县', 'rikaze');
INSERT INTO `district` VALUES ('33634', '辉南县', 'tonghua');
INSERT INTO `district` VALUES ('33635', '旅顺口区', 'dalian');
INSERT INTO `district` VALUES ('33636', '云城区', 'yunfu');
INSERT INTO `district` VALUES ('33637', '岳阳楼区', 'yueyang');
INSERT INTO `district` VALUES ('33638', '汇川区', 'zunyi');
INSERT INTO `district` VALUES ('33639', '柳河县', 'tonghua');
INSERT INTO `district` VALUES ('33640', '泸西县', 'honghe');
INSERT INTO `district` VALUES ('33641', '临渭区', 'weinan');
INSERT INTO `district` VALUES ('33642', '临沭县', 'linyi');
INSERT INTO `district` VALUES ('33643', '仁和区', 'panzhihua');
INSERT INTO `district` VALUES ('33644', '象山县', 'ningbo');
INSERT INTO `district` VALUES ('33645', '西乡县', 'hanzhong');
INSERT INTO `district` VALUES ('33646', '博兴县', 'binzhou');
INSERT INTO `district` VALUES ('33647', '乌兰县', 'haixi');
INSERT INTO `district` VALUES ('33648', '柳林县', 'lvliang');
INSERT INTO `district` VALUES ('33649', '大宁县', 'linfen');
INSERT INTO `district` VALUES ('33650', '隆昌县', 'neijiang');
INSERT INTO `district` VALUES ('33651', '拱墅区', 'hangzhou');
INSERT INTO `district` VALUES ('33652', '政和县', 'nanping');
INSERT INTO `district` VALUES ('33653', '茂港区', 'maoming');
INSERT INTO `district` VALUES ('33654', '玛曲县', 'gannan');
INSERT INTO `district` VALUES ('33655', '晴隆县', 'qianxinan');
INSERT INTO `district` VALUES ('33656', '南关区', 'changchun');
INSERT INTO `district` VALUES ('33657', '荥经县', 'yaan');
INSERT INTO `district` VALUES ('33658', '金阳县', 'liangshan');
INSERT INTO `district` VALUES ('33659', '乌鲁木齐县', 'wulumuqi');
INSERT INTO `district` VALUES ('33660', '岭东区', 'shuangyashan');
INSERT INTO `district` VALUES ('33661', '南靖县', 'zhangzhou');
INSERT INTO `district` VALUES ('33662', '秀洲区', 'jiaxing');
INSERT INTO `district` VALUES ('33663', '广丰县', 'shangrao');
INSERT INTO `district` VALUES ('33664', '玛沁县', 'guoluo');
INSERT INTO `district` VALUES ('33665', '北湖区', 'chenzhou');
INSERT INTO `district` VALUES ('33666', '芦山县', 'yaan');
INSERT INTO `district` VALUES ('33667', '浚县', 'hebi');
INSERT INTO `district` VALUES ('33668', '临翔区', 'lincang');
INSERT INTO `district` VALUES ('33669', '会东县', 'liangshan');
INSERT INTO `district` VALUES ('33670', '上犹县', 'ganzhou');
INSERT INTO `district` VALUES ('33671', '保亭黎族苗族自治县', 'baoting');
INSERT INTO `district` VALUES ('33672', '河北区', 'tianjin_city');
INSERT INTO `district` VALUES ('33673', '泗阳县', 'suqian');
INSERT INTO `district` VALUES ('33674', '双桥区', 'chengde');
INSERT INTO `district` VALUES ('33675', '望谟县', 'qianxinan');
INSERT INTO `district` VALUES ('33676', '徐水县', 'baoding');
INSERT INTO `district` VALUES ('33677', '子长县', 'yanan');
INSERT INTO `district` VALUES ('33678', '隆林各族自治县', 'baise');
INSERT INTO `district` VALUES ('33679', '株洲县', 'zhuzhou');
INSERT INTO `district` VALUES ('33680', '蒲县', 'linfen');
INSERT INTO `district` VALUES ('33681', '会泽县', 'qujing');
INSERT INTO `district` VALUES ('33682', '伊金霍洛旗', 'eerduosi');
INSERT INTO `district` VALUES ('33683', '呼玛县', 'daxinganling');
INSERT INTO `district` VALUES ('33684', '伍家岗区', 'yichang');
INSERT INTO `district` VALUES ('33685', '商州区', 'shangluo');
INSERT INTO `district` VALUES ('33686', '兴仁县', 'qianxinan');
INSERT INTO `district` VALUES ('33687', '开福区', 'changsha');
INSERT INTO `district` VALUES ('33688', '岳阳县', 'yueyang');
INSERT INTO `district` VALUES ('33689', '万荣县', 'yuncheng');
INSERT INTO `district` VALUES ('33690', '卓尼县', 'gannan');
INSERT INTO `district` VALUES ('33691', '灞桥区', 'xian');
INSERT INTO `district` VALUES ('33692', '永善县', 'zhaotong');
INSERT INTO `district` VALUES ('33693', '嵊泗县', 'zhoushan');
INSERT INTO `district` VALUES ('33694', '长阳土家族自治县', 'yichang');
INSERT INTO `district` VALUES ('33695', '石门县', 'changde');
INSERT INTO `district` VALUES ('33696', '平定县', 'yangquan');
INSERT INTO `district` VALUES ('33697', '乡宁县', 'linfen');
INSERT INTO `district` VALUES ('33698', '台前县', 'puyang');
INSERT INTO `district` VALUES ('33699', '靖州苗族侗族自治县', 'huaihua');
INSERT INTO `district` VALUES ('33700', '新巴尔虎左旗', 'hulunbeier');
INSERT INTO `district` VALUES ('33701', '且末县', 'bayinguoleng');
INSERT INTO `district` VALUES ('33702', '古丈县', 'xiangxi');
INSERT INTO `district` VALUES ('33703', '靖远县', 'baiyin');
INSERT INTO `district` VALUES ('33704', '会同县', 'huaihua');
INSERT INTO `district` VALUES ('33705', '武山县', 'tianshui');
INSERT INTO `district` VALUES ('33706', '梅县', 'meizhou');
INSERT INTO `district` VALUES ('33707', '恩施土家族苗族自治州', 'enshi');
INSERT INTO `district` VALUES ('33708', '塔河县', 'daxinganling');
INSERT INTO `district` VALUES ('33709', '苏尼特左旗', 'xilinguole');
INSERT INTO `district` VALUES ('33710', '华容区', 'ezhou');
INSERT INTO `district` VALUES ('33711', '日喀则地区', 'rikaze');
INSERT INTO `district` VALUES ('33712', '长洲区', 'wuzhou');
INSERT INTO `district` VALUES ('33713', '仁寿县', 'meishan');
INSERT INTO `district` VALUES ('33714', '博爱县', 'jiaozuo');
INSERT INTO `district` VALUES ('33715', '新浦区', 'lianyungang');
INSERT INTO `district` VALUES ('33716', '印台区', 'tongzhou');
INSERT INTO `district` VALUES ('33717', '顺平县', 'baoding');
INSERT INTO `district` VALUES ('33718', '杏花岭区', 'taiyuan');
INSERT INTO `district` VALUES ('33719', '澄海区', 'shantou');
INSERT INTO `district` VALUES ('33720', '叶县', 'pingdingshan');
INSERT INTO `district` VALUES ('33721', '高青县', 'zibo');
INSERT INTO `district` VALUES ('33722', '龙岗区', 'shenzhen');
INSERT INTO `district` VALUES ('33723', '左云县', 'datong');
INSERT INTO `district` VALUES ('33724', '昌平区', 'beijing_city');
INSERT INTO `district` VALUES ('33725', '彭山县', 'meishan');
INSERT INTO `district` VALUES ('33726', '册亨县', 'qianxinan');
INSERT INTO `district` VALUES ('33727', '陇川县', 'dehong');
INSERT INTO `district` VALUES ('33728', '鄂托克前旗', 'eerduosi');
INSERT INTO `district` VALUES ('33729', '镇宁布依族苗族自治县', 'anshun');
INSERT INTO `district` VALUES ('33730', '宁武县', 'xinzhou');
INSERT INTO `district` VALUES ('33731', '梁平县', 'chongqing_city');
INSERT INTO `district` VALUES ('33732', '弋阳县', 'shangrao');
INSERT INTO `district` VALUES ('33733', '泽州县', 'jincheng');
INSERT INTO `district` VALUES ('33734', '牟平区', 'yantai');
INSERT INTO `district` VALUES ('33735', '龙山区', 'liaoyuan');
INSERT INTO `district` VALUES ('33736', '松溪县', 'nanping');
INSERT INTO `district` VALUES ('33737', '罗城仫佬族自治县', 'hechi');
INSERT INTO `district` VALUES ('33738', '邗江区', 'yangzhou');
INSERT INTO `district` VALUES ('33739', '陈巴尔虎旗', 'hulunbeier');
INSERT INTO `district` VALUES ('33740', '涞水县', 'baoding');
INSERT INTO `district` VALUES ('33741', '赵县', 'shijiazhuang');
INSERT INTO `district` VALUES ('33742', '成安县', 'handan');
INSERT INTO `district` VALUES ('33743', '嵩明县', 'kunming');
INSERT INTO `district` VALUES ('33744', '潮南区', 'shantou');
INSERT INTO `district` VALUES ('33745', '双塔区', 'chaoyang');
INSERT INTO `district` VALUES ('33746', '皇姑区', 'shenyang');
INSERT INTO `district` VALUES ('33747', '任城区', 'jining');
INSERT INTO `district` VALUES ('33748', '茶陵县', 'zhuzhou');
INSERT INTO `district` VALUES ('33749', '鄱阳县', 'shangrao');
INSERT INTO `district` VALUES ('33750', '道孚县', 'ganzi');
INSERT INTO `district` VALUES ('33751', '中站区', 'jiaozuo');
INSERT INTO `district` VALUES ('33752', '平罗县', 'shizuishan');
INSERT INTO `district` VALUES ('33753', '稷山县', 'yuncheng');
INSERT INTO `district` VALUES ('33754', '安平县', 'hengshui');
INSERT INTO `district` VALUES ('33755', '凤翔县', 'baoji');
INSERT INTO `district` VALUES ('33756', '西峰区', 'qingyang');
INSERT INTO `district` VALUES ('33757', '龙湖区', 'shantou');
INSERT INTO `district` VALUES ('33758', '梅江区', 'meizhou');
INSERT INTO `district` VALUES ('33759', '普格县', 'liangshan');
INSERT INTO `district` VALUES ('33760', '横县', 'nanning');
INSERT INTO `district` VALUES ('33761', '和政县', 'linxia');
INSERT INTO `district` VALUES ('33762', '新城区', 'xian');
INSERT INTO `district` VALUES ('33763', '杭锦后旗', 'bayannaoer');
INSERT INTO `district` VALUES ('33764', '西工区', 'luoyang');
INSERT INTO `district` VALUES ('33765', '德宏傣族景颇族自治州', 'dehong');
INSERT INTO `district` VALUES ('33766', '宿松县', 'anqing');
INSERT INTO `district` VALUES ('33767', '垣曲县', 'yuncheng');
INSERT INTO `district` VALUES ('33768', '文峰区', 'anyang');
INSERT INTO `district` VALUES ('33769', '双峰县', 'loudi');
INSERT INTO `district` VALUES ('33770', '玉泉区', 'huhehaote');
INSERT INTO `district` VALUES ('33771', '天柱县', 'qiandongnan');
INSERT INTO `district` VALUES ('33772', '化德县', 'wulanchabu');
INSERT INTO `district` VALUES ('33773', '宜章县', 'chenzhou');
INSERT INTO `district` VALUES ('33774', '昌江区', 'jingdezhen');
INSERT INTO `district` VALUES ('33775', '爱民区', 'mudanjiang');
INSERT INTO `district` VALUES ('33776', '分宜县', 'xinyu');
INSERT INTO `district` VALUES ('33777', '潜山县', 'anqing');
INSERT INTO `district` VALUES ('33778', '神农架林区', 'shennongjia');
INSERT INTO `district` VALUES ('33779', '东乡族自治县', 'linxia');
INSERT INTO `district` VALUES ('33780', '双台子区', 'panjin');
INSERT INTO `district` VALUES ('33781', '平陆县', 'yuncheng');
INSERT INTO `district` VALUES ('33782', '太谷县', 'jinzhong');
INSERT INTO `district` VALUES ('33783', '灵台县', 'pingliang');
INSERT INTO `district` VALUES ('33784', '汉滨区', 'ankang');
INSERT INTO `district` VALUES ('33785', '埇桥区', 'suzhou_anhui');
INSERT INTO `district` VALUES ('33786', '红河哈尼族彝族自治州', 'honghe');
INSERT INTO `district` VALUES ('33787', '辽阳县', 'liaoyang');
INSERT INTO `district` VALUES ('33788', '海兴县', 'cangzhou');
INSERT INTO `district` VALUES ('33789', '北戴河区', 'qinhuangdao');
INSERT INTO `district` VALUES ('33790', '布拖县', 'liangshan');
INSERT INTO `district` VALUES ('33791', '宝塔区', 'yanan');
INSERT INTO `district` VALUES ('33792', '永吉县', 'jilin_city');
INSERT INTO `district` VALUES ('33793', '神木县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('33794', '大荔县', 'weinan');
INSERT INTO `district` VALUES ('33795', '隆阳区', 'baoshan');
INSERT INTO `district` VALUES ('33796', '惠东县', 'huizhou_guangdong');
INSERT INTO `district` VALUES ('33797', '海南藏族自治州', 'hainanzangzu');
INSERT INTO `district` VALUES ('33798', '武义县', 'jinhua');
INSERT INTO `district` VALUES ('33799', '汝阳县', 'luoyang');
INSERT INTO `district` VALUES ('33800', '中江县', 'deyang');
INSERT INTO `district` VALUES ('33801', '平邑县', 'linyi');
INSERT INTO `district` VALUES ('33802', '矿区', 'yangquan');
INSERT INTO `district` VALUES ('33803', '渭滨区', 'baoji');
INSERT INTO `district` VALUES ('33804', '五华区', 'kunming');
INSERT INTO `district` VALUES ('33805', '离石区', 'lvliang');
INSERT INTO `district` VALUES ('33806', '闻喜县', 'yuncheng');
INSERT INTO `district` VALUES ('33807', '大武口区', 'shizuishan');
INSERT INTO `district` VALUES ('33808', '康乐县', 'linxia');
INSERT INTO `district` VALUES ('33809', '宁河县', 'tianjin_city');
INSERT INTO `district` VALUES ('33810', '嘉祥县', 'jining');
INSERT INTO `district` VALUES ('33811', '柳城县', 'liuzhou');
INSERT INTO `district` VALUES ('33812', '樊城区', 'xiangfan');
INSERT INTO `district` VALUES ('33813', '桦川县', 'jiamusi');
INSERT INTO `district` VALUES ('33814', '源城区', 'heyuan');
INSERT INTO `district` VALUES ('33815', '陕县', 'sanmenxia');
INSERT INTO `district` VALUES ('33816', '顺城区', 'fushun');
INSERT INTO `district` VALUES ('33817', '湘潭县', 'xiangtan');
INSERT INTO `district` VALUES ('33818', '静海县', 'tianjin_city');
INSERT INTO `district` VALUES ('33819', '中山区', 'dalian');
INSERT INTO `district` VALUES ('33820', '宁化县', 'sanming');
INSERT INTO `district` VALUES ('33821', '通渭县', 'dingxi');
INSERT INTO `district` VALUES ('33822', '博山区', 'zibo');
INSERT INTO `district` VALUES ('33823', '大悟县', 'xiaogan');
INSERT INTO `district` VALUES ('33824', '零陵区', 'yongzhou');
INSERT INTO `district` VALUES ('33825', '秀山土家族苗族自治县', 'chongqing_city');
INSERT INTO `district` VALUES ('33826', '城东区', 'xining');
INSERT INTO `district` VALUES ('33827', '阿巴嘎旗', 'xilinguole');
INSERT INTO `district` VALUES ('33828', '浑源县', 'datong');
INSERT INTO `district` VALUES ('33829', '细河区', 'fuxin');
INSERT INTO `district` VALUES ('33830', '江干区', 'hangzhou');
INSERT INTO `district` VALUES ('33831', '五莲县', 'rizhao');
INSERT INTO `district` VALUES ('33832', '铁西区', 'siping');
INSERT INTO `district` VALUES ('33833', '富平县', 'weinan');
INSERT INTO `district` VALUES ('33834', '合川区', 'chongqing_city');
INSERT INTO `district` VALUES ('33835', '福山区', 'yantai');
INSERT INTO `district` VALUES ('33836', '西和县', 'longnan');
INSERT INTO `district` VALUES ('33837', '猇亭区', 'yichang');
INSERT INTO `district` VALUES ('33838', '北仑区', 'ningbo');
INSERT INTO `district` VALUES ('33839', '蕉岭县', 'meizhou');
INSERT INTO `district` VALUES ('33840', '扶沟县', 'zhoukou');
INSERT INTO `district` VALUES ('33841', '永春县', 'quanzhou');
INSERT INTO `district` VALUES ('33842', '新兴县', 'yunfu');
INSERT INTO `district` VALUES ('33843', '自流井区', 'zigong');
INSERT INTO `district` VALUES ('33844', '老城区', 'luoyang');
INSERT INTO `district` VALUES ('33845', '围场满族蒙古族自治县', 'chengde');
INSERT INTO `district` VALUES ('33846', '双阳区', 'changchun');
INSERT INTO `district` VALUES ('33847', '五台县', 'xinzhou');
INSERT INTO `district` VALUES ('33848', '阿拉善盟', 'alashan');
INSERT INTO `district` VALUES ('33849', '惠城区', 'huizhou_guangdong');
INSERT INTO `district` VALUES ('33850', '夹江县', 'leshan');
INSERT INTO `district` VALUES ('33851', '鹰手营子矿区', 'chengde');
INSERT INTO `district` VALUES ('33852', '萝岗区', 'guangzhou');
INSERT INTO `district` VALUES ('33853', '白水县', 'weinan');
INSERT INTO `district` VALUES ('33854', '竹山县', 'shiyan');
INSERT INTO `district` VALUES ('33855', '果洛藏族自治州', 'guoluo');
INSERT INTO `district` VALUES ('33856', '甘井子区', 'dalian');
INSERT INTO `district` VALUES ('33857', '五寨县', 'xinzhou');
INSERT INTO `district` VALUES ('33858', '黄州区', 'huanggang');
INSERT INTO `district` VALUES ('33859', '疏勒县', 'kashi');
INSERT INTO `district` VALUES ('33860', '南昌县', 'nanchang');
INSERT INTO `district` VALUES ('33861', '都兰县', 'haixi');
INSERT INTO `district` VALUES ('33862', '邢台县', 'xingtai');
INSERT INTO `district` VALUES ('33863', '山阳区', 'jiaozuo');
INSERT INTO `district` VALUES ('33864', '八公山区', 'huainan');
INSERT INTO `district` VALUES ('33865', '揭西县', 'jieyang');
INSERT INTO `district` VALUES ('33866', '洛扎县', 'shannan');
INSERT INTO `district` VALUES ('33867', '翼城县', 'linfen');
INSERT INTO `district` VALUES ('33868', '大港区', 'tianjin_city');
INSERT INTO `district` VALUES ('33869', '淮阴区', 'huaian');
INSERT INTO `district` VALUES ('33871', '鄢陵县', 'xuchang');
INSERT INTO `district` VALUES ('33872', '金寨县', 'liuan');
INSERT INTO `district` VALUES ('33873', '哈密地区', 'hami');
INSERT INTO `district` VALUES ('33874', '迎江区', 'anqing');
INSERT INTO `district` VALUES ('33875', '新源县', 'yili');
INSERT INTO `district` VALUES ('33876', '陵县', 'dezhou');
INSERT INTO `district` VALUES ('33877', '民和回族土族自治县', 'haidong');
INSERT INTO `district` VALUES ('33878', '确山县', 'zhumadian');
INSERT INTO `district` VALUES ('33879', '曲周县', 'handan');
INSERT INTO `district` VALUES ('33880', '点军区', 'yichang');
INSERT INTO `district` VALUES ('33881', '雨花区', 'changsha');
INSERT INTO `district` VALUES ('33882', '防城区', 'fangchenggang');
INSERT INTO `district` VALUES ('33883', '宜良县', 'kunming');
INSERT INTO `district` VALUES ('33884', '喀喇沁左翼蒙古族自治县', 'chaoyang');
INSERT INTO `district` VALUES ('33885', '罗源县', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('33886', '白河县', 'ankang');
INSERT INTO `district` VALUES ('33887', '宁陵县', 'shangqiu');
INSERT INTO `district` VALUES ('33888', '合阳县', 'weinan');
INSERT INTO `district` VALUES ('33890', '凤阳县', 'chuzhou');
INSERT INTO `district` VALUES ('33891', '临猗县', 'yuncheng');
INSERT INTO `district` VALUES ('33892', '邵东县', 'shaoyang');
INSERT INTO `district` VALUES ('33893', '桥西区', 'zhangjiakou');
INSERT INTO `district` VALUES ('33894', '婺城区', 'jinhua');
INSERT INTO `district` VALUES ('33895', '海北藏族自治州', 'haibei');
INSERT INTO `district` VALUES ('33896', '屏山县', 'yibin');
INSERT INTO `district` VALUES ('33897', '虹口区', 'shanghai_city');
INSERT INTO `district` VALUES ('33898', '红古区', 'lanzhou');
INSERT INTO `district` VALUES ('33899', '康马县', 'rikaze');
INSERT INTO `district` VALUES ('33900', '鼎湖区', 'zhaoqing');
INSERT INTO `district` VALUES ('33901', '宜川县', 'yanan');
INSERT INTO `district` VALUES ('33902', '荔浦县', 'guilin');
INSERT INTO `district` VALUES ('33903', '淳化县', 'xianyang');
INSERT INTO `district` VALUES ('33904', '青山区', 'wuhan');
INSERT INTO `district` VALUES ('33905', '向阳区', 'hegang');
INSERT INTO `district` VALUES ('33906', '雨湖区', 'xiangtan');
INSERT INTO `district` VALUES ('33907', '崇阳县', 'xianning');
INSERT INTO `district` VALUES ('33908', '兴业县', 'yulin_guangxi');
INSERT INTO `district` VALUES ('33909', '岚皋县', 'ankang');
INSERT INTO `district` VALUES ('33910', '威远县', 'neijiang');
INSERT INTO `district` VALUES ('33911', '巨野县', 'heze');
INSERT INTO `district` VALUES ('33912', '科尔沁左翼中旗', 'tongliao');
INSERT INTO `district` VALUES ('33913', '凌云县', 'baise');
INSERT INTO `district` VALUES ('33914', '江安县', 'yibin');
INSERT INTO `district` VALUES ('33915', '下陆区', 'huangshi');
INSERT INTO `district` VALUES ('33916', '于洪区', 'shenyang');
INSERT INTO `district` VALUES ('33917', '路北区', 'tangshan');
INSERT INTO `district` VALUES ('33918', '龙沙区', 'qiqihaer');
INSERT INTO `district` VALUES ('33919', '南岗区', 'haerbin');
INSERT INTO `district` VALUES ('33920', '雷山县', 'qiandongnan');
INSERT INTO `district` VALUES ('33921', '长清区', 'jinan');
INSERT INTO `district` VALUES ('33922', '平阳县', 'wenzhou');
INSERT INTO `district` VALUES ('33923', '洛川县', 'yanan');
INSERT INTO `district` VALUES ('33924', '马山县', 'nanning');
INSERT INTO `district` VALUES ('33925', '梅里斯达翰尔族区', 'qiqihaer');
INSERT INTO `district` VALUES ('33926', '田林县', 'baise');
INSERT INTO `district` VALUES ('33927', '郧县', 'shiyan');
INSERT INTO `district` VALUES ('33928', '乌拉特中旗', 'bayannaoer');
INSERT INTO `district` VALUES ('33929', '阿坝藏族羌族自治州', 'aba');
INSERT INTO `district` VALUES ('33930', '大兴安岭地区', 'daxinganling');
INSERT INTO `district` VALUES ('33931', '济阳县', 'jinan');
INSERT INTO `district` VALUES ('33932', '错那县', 'shannan');
INSERT INTO `district` VALUES ('33933', '兴庆区', 'yinchuan');
INSERT INTO `district` VALUES ('33934', '桥东区', 'zhangjiakou');
INSERT INTO `district` VALUES ('33935', '镶黄旗', 'xilinguole');
INSERT INTO `district` VALUES ('33936', '阿城区', 'haerbin');
INSERT INTO `district` VALUES ('33937', '濮阳县', 'puyang');
INSERT INTO `district` VALUES ('33938', '漳县', 'dingxi');
INSERT INTO `district` VALUES ('33939', '头屯河区', 'wulumuqi');
INSERT INTO `district` VALUES ('33940', '独山子区', 'kelamayi');
INSERT INTO `district` VALUES ('33941', '新抚区', 'fushun');
INSERT INTO `district` VALUES ('33942', '合江县', 'luzhou');
INSERT INTO `district` VALUES ('33943', '印江土家族苗族自治县', 'tongren');
INSERT INTO `district` VALUES ('33944', '宝清县', 'shuangyashan');
INSERT INTO `district` VALUES ('33945', '泾川县', 'pingliang');
INSERT INTO `district` VALUES ('33946', '襄汾县', 'linfen');
INSERT INTO `district` VALUES ('33947', '交城县', 'lvliang');
INSERT INTO `district` VALUES ('33948', '灌南县', 'lianyungang');
INSERT INTO `district` VALUES ('33949', '工农区', 'hegang');
INSERT INTO `district` VALUES ('33950', '临朐县', 'weifang');
INSERT INTO `district` VALUES ('33952', '巍山彝族回族自治县', 'dali');
INSERT INTO `district` VALUES ('33953', '怀柔区', 'beijing_city');
INSERT INTO `district` VALUES ('33954', '泗县', 'suzhou_anhui');
INSERT INTO `district` VALUES ('33955', '海丰县', 'shanwei');
INSERT INTO `district` VALUES ('33956', '龙潭区', 'jilin_city');
INSERT INTO `district` VALUES ('33957', '芦溪县', 'pingxiang');
INSERT INTO `district` VALUES ('33958', '牡丹区', 'heze');
INSERT INTO `district` VALUES ('33959', '城中区', 'xining');
INSERT INTO `district` VALUES ('33960', '颍上县', 'fuyang_anhui');
INSERT INTO `district` VALUES ('33961', '白下区', 'nanjing');
INSERT INTO `district` VALUES ('33962', '沙县', 'sanming');
INSERT INTO `district` VALUES ('33963', '荣昌县', 'chongqing_city');
INSERT INTO `district` VALUES ('33964', '康平县', 'shenyang');
INSERT INTO `district` VALUES ('33965', '克什克腾旗', 'chifeng');
INSERT INTO `district` VALUES ('33966', '金水区', 'zhengzhou');
INSERT INTO `district` VALUES ('33967', '静乐县', 'xinzhou');
INSERT INTO `district` VALUES ('33968', '三山区', 'wuhu');
INSERT INTO `district` VALUES ('33969', '黑山县', 'jinzhou');
INSERT INTO `district` VALUES ('33970', '宁乡县', 'changsha');
INSERT INTO `district` VALUES ('33971', '浉河区', 'xinyang');
INSERT INTO `district` VALUES ('33972', '北碚区', 'chongqing_city');
INSERT INTO `district` VALUES ('33973', '清原满族自治县', 'fushun');
INSERT INTO `district` VALUES ('33974', '隰县', 'linfen');
INSERT INTO `district` VALUES ('33975', '全南县', 'ganzhou');
INSERT INTO `district` VALUES ('33976', '桑日县', 'shannan');
INSERT INTO `district` VALUES ('33977', '西塞山区', 'huangshi');
INSERT INTO `district` VALUES ('33978', '开化县', 'quzhou');
INSERT INTO `district` VALUES ('33979', '平谷区', 'beijing_city');
INSERT INTO `district` VALUES ('33980', '凉山彝族自治州', 'liangshan');
INSERT INTO `district` VALUES ('33981', '沙坪坝区', 'chongqing_city');
INSERT INTO `district` VALUES ('33982', '安多县', 'naqu');
INSERT INTO `district` VALUES ('33983', '于都县', 'ganzhou');
INSERT INTO `district` VALUES ('33984', '宝坻区', 'tianjin_city');
INSERT INTO `district` VALUES ('33985', '汉源县', 'yaan');
INSERT INTO `district` VALUES ('33986', '两当县', 'longnan');
INSERT INTO `district` VALUES ('33987', '东光县', 'cangzhou');
INSERT INTO `district` VALUES ('33988', '邯山区', 'handan');
INSERT INTO `district` VALUES ('33989', '银海区', 'beihai');
INSERT INTO `district` VALUES ('33990', '北辰区', 'tianjin_city');
INSERT INTO `district` VALUES ('33991', '岗巴县', 'rikaze');
INSERT INTO `district` VALUES ('33992', '裕华区', 'shijiazhuang');
INSERT INTO `district` VALUES ('33993', '红旗区', 'xinxiang');
INSERT INTO `district` VALUES ('33994', '阳新县', 'huangshi');
INSERT INTO `district` VALUES ('33995', '高台县', 'zhangye');
INSERT INTO `district` VALUES ('33996', '乾安县', 'songyuan');
INSERT INTO `district` VALUES ('33997', '平泉县', 'chengde');
INSERT INTO `district` VALUES ('33998', '游仙区', 'mianyang');
INSERT INTO `district` VALUES ('33999', '大新县', 'chongzuo');
INSERT INTO `district` VALUES ('34000', '普陀区', 'shanghai_city');
INSERT INTO `district` VALUES ('34001', '双滦区', 'chengde');
INSERT INTO `district` VALUES ('34002', '玛纳斯县', 'changji');
INSERT INTO `district` VALUES ('34003', '涡阳县', 'bozhou');
INSERT INTO `district` VALUES ('34004', '南岳区', 'hengyang');
INSERT INTO `district` VALUES ('34005', '鼓楼区', 'kaifeng');
INSERT INTO `district` VALUES ('34006', '同德县', 'hainanzangzu');
INSERT INTO `district` VALUES ('34007', '前进区', 'jiamusi');
INSERT INTO `district` VALUES ('34008', '建宁县', 'sanming');
INSERT INTO `district` VALUES ('34009', '夏津县', 'dezhou');
INSERT INTO `district` VALUES ('34010', '华县', 'weinan');
INSERT INTO `district` VALUES ('34011', '门头沟区', 'beijing_city');
INSERT INTO `district` VALUES ('34012', '集贤县', 'shuangyashan');
INSERT INTO `district` VALUES ('34013', '宜丰县', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('34014', '汉寿县', 'changde');
INSERT INTO `district` VALUES ('34015', '清流县', 'sanming');
INSERT INTO `district` VALUES ('34016', '松江区', 'shanghai_city');
INSERT INTO `district` VALUES ('34017', '原阳县', 'xinxiang');
INSERT INTO `district` VALUES ('34018', '芗城区', 'zhangzhou');
INSERT INTO `district` VALUES ('34019', '江阳区', 'luzhou');
INSERT INTO `district` VALUES ('34020', '宁陕县', 'ankang');
INSERT INTO `district` VALUES ('34021', '碑林区', 'xian');
INSERT INTO `district` VALUES ('34022', '沂南县', 'linyi');
INSERT INTO `district` VALUES ('34024', '云阳县', 'chongqing_city');
INSERT INTO `district` VALUES ('34025', '尧都区', 'linfen');
INSERT INTO `district` VALUES ('34026', '白云鄂博矿区', 'baotou');
INSERT INTO `district` VALUES ('34027', '漳浦县', 'zhangzhou');
INSERT INTO `district` VALUES ('34028', '卢氏县', 'sanmenxia');
INSERT INTO `district` VALUES ('34029', '东山区', 'hegang');
INSERT INTO `district` VALUES ('34030', '志丹县', 'yanan');
INSERT INTO `district` VALUES ('34031', '永寿县', 'xianyang');
INSERT INTO `district` VALUES ('34032', '茂县', 'aba');
INSERT INTO `district` VALUES ('34033', '泌阳县', 'zhumadian');
INSERT INTO `district` VALUES ('34034', '兴县', 'lvliang');
INSERT INTO `district` VALUES ('34035', '内丘县', 'xingtai');
INSERT INTO `district` VALUES ('34036', '乃东县', 'shannan');
INSERT INTO `district` VALUES ('34037', '船营区', 'jilin_city');
INSERT INTO `district` VALUES ('34038', '成县', 'longnan');
INSERT INTO `district` VALUES ('34039', '甘孜县', 'ganzi');
INSERT INTO `district` VALUES ('34040', '碾子山区', 'qiqihaer');
INSERT INTO `district` VALUES ('34041', '双柏县', 'chuxiong');
INSERT INTO `district` VALUES ('34042', '石屏县', 'honghe');
INSERT INTO `district` VALUES ('34043', '尼木县', 'lasa');
INSERT INTO `district` VALUES ('34044', '石林彝族自治县', 'kunming');
INSERT INTO `district` VALUES ('34045', '滦南县', 'tangshan');
INSERT INTO `district` VALUES ('34046', '日土县', 'ali');
INSERT INTO `district` VALUES ('34047', '禹会区', 'bangbu');
INSERT INTO `district` VALUES ('34048', '璧山县', 'chongqing_city');
INSERT INTO `district` VALUES ('34049', '铅山县', 'shangrao');
INSERT INTO `district` VALUES ('34050', '牧野区', 'xinxiang');
INSERT INTO `district` VALUES ('34051', '朝天区', 'guangyuan');
INSERT INTO `district` VALUES ('34052', '文山壮族苗族自治州', 'wenshan');
INSERT INTO `district` VALUES ('34053', '柯城区', 'quzhou');
INSERT INTO `district` VALUES ('34054', '开鲁县', 'tongliao');
INSERT INTO `district` VALUES ('34055', '白银区', 'baiyin');
INSERT INTO `district` VALUES ('34056', '望花区', 'fushun');
INSERT INTO `district` VALUES ('34057', '长兴县', 'huzhou');
INSERT INTO `district` VALUES ('34058', '隆德县', 'guyuan');
INSERT INTO `district` VALUES ('34059', '清新县', 'qingyuan');
INSERT INTO `district` VALUES ('34060', '延津县', 'xinxiang');
INSERT INTO `district` VALUES ('34061', '望奎县', 'suihua');
INSERT INTO `district` VALUES ('34062', '宁都县', 'ganzhou');
INSERT INTO `district` VALUES ('34063', '朝阳县', 'chaoyang');
INSERT INTO `district` VALUES ('34064', '余杭区', 'hangzhou');
INSERT INTO `district` VALUES ('34065', '海拉尔区', 'hulunbeier');
INSERT INTO `district` VALUES ('34066', '苏家屯区', 'shenyang');
INSERT INTO `district` VALUES ('34067', '鲤城区', 'quanzhou');
INSERT INTO `district` VALUES ('34068', '郾城区', 'luohe');
INSERT INTO `district` VALUES ('34069', '松潘县', 'aba');
INSERT INTO `district` VALUES ('34070', '壤塘县', 'aba');
INSERT INTO `district` VALUES ('34071', '鄞州区', 'ningbo');
INSERT INTO `district` VALUES ('34072', '思明区', 'xiamen');
INSERT INTO `district` VALUES ('34073', '营山县', 'nanchong');
INSERT INTO `district` VALUES ('34074', '阜南县', 'fuyang_anhui');
INSERT INTO `district` VALUES ('34075', '泽普县', 'kashi');
INSERT INTO `district` VALUES ('34076', '南陵县', 'wuhu');
INSERT INTO `district` VALUES ('34077', '友谊县', 'shuangyashan');
INSERT INTO `district` VALUES ('34078', '南县', 'yiyang');
INSERT INTO `district` VALUES ('34079', '巴塘县', 'ganzi');
INSERT INTO `district` VALUES ('34080', '化隆回族自治县', 'haidong');
INSERT INTO `district` VALUES ('34081', '敖汉旗', 'chifeng');
INSERT INTO `district` VALUES ('34082', '邵阳县', 'shaoyang');
INSERT INTO `district` VALUES ('34083', '大方县', 'bijie');
INSERT INTO `district` VALUES ('34084', '封开县', 'zhaoqing');
INSERT INTO `district` VALUES ('34085', '翁源县', 'shaoguan');
INSERT INTO `district` VALUES ('34086', '察哈尔右翼中旗', 'wulanchabu');
INSERT INTO `district` VALUES ('34087', '隆尧县', 'xingtai');
INSERT INTO `district` VALUES ('34088', '淄川区', 'zibo');
INSERT INTO `district` VALUES ('34089', '北塔区', 'shaoyang');
INSERT INTO `district` VALUES ('34090', '安居区', 'suining');
INSERT INTO `district` VALUES ('34091', '大城县', 'langfang');
INSERT INTO `district` VALUES ('34092', '江东区', 'ningbo');
INSERT INTO `district` VALUES ('34093', '富蕴县', 'aletai');
INSERT INTO `district` VALUES ('34094', '卢湾区', 'shanghai_city');
INSERT INTO `district` VALUES ('34095', '盐都区', 'yancheng');
INSERT INTO `district` VALUES ('34096', '茄子河区', 'qitaihe');
INSERT INTO `district` VALUES ('34097', '忻城县', 'laibin');
INSERT INTO `district` VALUES ('34098', '巴马瑶族自治县', 'hechi');
INSERT INTO `district` VALUES ('34099', '耀州区', 'tongzhou');
INSERT INTO `district` VALUES ('34100', '华宁县', 'yuxi');
INSERT INTO `district` VALUES ('34101', '大理白族自治州', 'dali');
INSERT INTO `district` VALUES ('34102', '绿春县', 'honghe');
INSERT INTO `district` VALUES ('34103', '惠安县', 'quanzhou');
INSERT INTO `district` VALUES ('34104', '张湾区', 'shiyan');
INSERT INTO `district` VALUES ('34105', '硚口区', 'wuhan');
INSERT INTO `district` VALUES ('34106', '潮安县', 'chaozhou');
INSERT INTO `district` VALUES ('34107', '浈江区', 'shaoguan');
INSERT INTO `district` VALUES ('34108', '湘桥区', 'chaozhou');
INSERT INTO `district` VALUES ('34109', '宽城区', 'changchun');
INSERT INTO `district` VALUES ('34110', '荆州区', 'jingzhou');
INSERT INTO `district` VALUES ('34111', '泾源县', 'guyuan');
INSERT INTO `district` VALUES ('34112', '东山县', 'zhangzhou');
INSERT INTO `district` VALUES ('34113', '渝中区', 'chongqing_city');
INSERT INTO `district` VALUES ('34114', '贵池区', 'chizhou');
INSERT INTO `district` VALUES ('34115', '岳普湖县', 'kashi');
INSERT INTO `district` VALUES ('34116', '宝山区', 'shanghai_city');
INSERT INTO `district` VALUES ('34117', '蒲江县', 'chengdu');
INSERT INTO `district` VALUES ('34118', '和顺县', 'jinzhong');
INSERT INTO `district` VALUES ('34119', '茌平县', 'liaocheng');
INSERT INTO `district` VALUES ('34120', '莱城区', 'laiwu');
INSERT INTO `district` VALUES ('34121', '矿区', 'datong');
INSERT INTO `district` VALUES ('34122', '亚东县', 'rikaze');
INSERT INTO `district` VALUES ('34123', '虎丘区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('34124', '彬县', 'xianyang');
INSERT INTO `district` VALUES ('34125', '定安县', 'dingan');
INSERT INTO `district` VALUES ('34126', '湘东区', 'pingxiang');
INSERT INTO `district` VALUES ('34127', '昌都县', 'changdu');
INSERT INTO `district` VALUES ('34128', '维扬区', 'yangzhou');
INSERT INTO `district` VALUES ('34129', '左贡县', 'changdu');
INSERT INTO `district` VALUES ('34130', '涿鹿县', 'zhangjiakou');
INSERT INTO `district` VALUES ('34131', '黟县', 'huangshan');
INSERT INTO `district` VALUES ('34132', '长治县', 'changzhi');
INSERT INTO `district` VALUES ('34133', '沛县', 'xuzhou');
INSERT INTO `district` VALUES ('34134', '东宝区', 'jingmen');
INSERT INTO `district` VALUES ('34135', '高邑县', 'shijiazhuang');
INSERT INTO `district` VALUES ('34136', '祁门县', 'huangshan');
INSERT INTO `district` VALUES ('34137', '盐津县', 'zhaotong');
INSERT INTO `district` VALUES ('34138', '利津县', 'dongying');
INSERT INTO `district` VALUES ('34139', '贵南县', 'hainanzangzu');
INSERT INTO `district` VALUES ('34140', '相山区', 'huaibei');
INSERT INTO `district` VALUES ('34141', '邻水县', 'guangan');
INSERT INTO `district` VALUES ('34142', '鄯善县', 'tulufan');
INSERT INTO `district` VALUES ('34143', '平阴县', 'jinan');
INSERT INTO `district` VALUES ('34144', '鲁甸县', 'zhaotong');
INSERT INTO `district` VALUES ('34145', '长汀县', 'longyan');
INSERT INTO `district` VALUES ('34146', '炉霍县', 'ganzi');
INSERT INTO `district` VALUES ('34147', '商城县', 'xinyang');
INSERT INTO `district` VALUES ('34148', '三穗县', 'qiandongnan');
INSERT INTO `district` VALUES ('34149', '梁河县', 'dehong');
INSERT INTO `district` VALUES ('34150', '泽库县', 'huangnan');
INSERT INTO `district` VALUES ('34151', '江源区', 'baishan');
INSERT INTO `district` VALUES ('34152', '章贡区', 'ganzhou');
INSERT INTO `district` VALUES ('34153', '川汇区', 'zhoukou');
INSERT INTO `district` VALUES ('34154', '东阿县', 'liaocheng');
INSERT INTO `district` VALUES ('34155', '吴起县', 'yanan');
INSERT INTO `district` VALUES ('34156', '颍州区', 'fuyang_anhui');
INSERT INTO `district` VALUES ('34157', '保康县', 'xiangfan');
INSERT INTO `district` VALUES ('34158', '东源县', 'heyuan');
INSERT INTO `district` VALUES ('34159', '南湖区', 'jiaxing');
INSERT INTO `district` VALUES ('34160', '金堂县', 'chengdu');
INSERT INTO `district` VALUES ('34161', '石鼓区', 'hengyang');
INSERT INTO `district` VALUES ('34162', '崇礼县', 'zhangjiakou');
INSERT INTO `district` VALUES ('34163', '花垣县', 'xiangxi');
INSERT INTO `district` VALUES ('34164', '大观区', 'anqing');
INSERT INTO `district` VALUES ('34165', '西湖区', 'hangzhou');
INSERT INTO `district` VALUES ('34166', '察哈尔右翼前旗', 'wulanchabu');
INSERT INTO `district` VALUES ('34167', '坊子区', 'weifang');
INSERT INTO `district` VALUES ('34168', '掇刀区', 'jingmen');
INSERT INTO `district` VALUES ('34169', '崇安区', 'wuxi');
INSERT INTO `district` VALUES ('34170', '恒山区', 'jixi');
INSERT INTO `district` VALUES ('34171', '巴楚县', 'kashi');
INSERT INTO `district` VALUES ('34172', '郸城县', 'zhoukou');
INSERT INTO `district` VALUES ('34173', '九里区', 'xuzhou');
INSERT INTO `district` VALUES ('34174', '鄂温克族自治旗', 'hulunbeier');
INSERT INTO `district` VALUES ('34176', '奎文区', 'weifang');
INSERT INTO `district` VALUES ('34177', '老边区', 'yingkou');
INSERT INTO `district` VALUES ('34178', '温江区', 'chengdu');
INSERT INTO `district` VALUES ('34179', '万载县', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('34180', '怀宁县', 'anqing');
INSERT INTO `district` VALUES ('34181', '泰宁县', 'sanming');
INSERT INTO `district` VALUES ('34182', '麻江县', 'qiandongnan');
INSERT INTO `district` VALUES ('34183', '荷塘区', 'zhuzhou');
INSERT INTO `district` VALUES ('34184', '南山区', 'shenzhen');
INSERT INTO `district` VALUES ('34185', '克山县', 'qiqihaer');
INSERT INTO `district` VALUES ('34186', '聂拉木县', 'rikaze');
INSERT INTO `district` VALUES ('34187', '和平县', 'heyuan');
INSERT INTO `district` VALUES ('34188', '轮台县', 'bayinguoleng');
INSERT INTO `district` VALUES ('34189', '宕昌县', 'longnan');
INSERT INTO `district` VALUES ('34190', '柏乡县', 'xingtai');
INSERT INTO `district` VALUES ('34191', '庐阳区', 'hefei');
INSERT INTO `district` VALUES ('34192', '昌黎县', 'qinhuangdao');
INSERT INTO `district` VALUES ('34193', '古冶区', 'tangshan');
INSERT INTO `district` VALUES ('34194', '万全县', 'zhangjiakou');
INSERT INTO `district` VALUES ('34195', '贵德县', 'hainanzangzu');
INSERT INTO `district` VALUES ('34196', '临县', 'lvliang');
INSERT INTO `district` VALUES ('34197', '西吉县', 'guyuan');
INSERT INTO `district` VALUES ('34198', '惠农区', 'shizuishan');
INSERT INTO `district` VALUES ('34199', '永和县', 'linfen');
INSERT INTO `district` VALUES ('34200', '淳安县', 'hangzhou');
INSERT INTO `district` VALUES ('34201', '万秀区', 'wuzhou');
INSERT INTO `district` VALUES ('34202', '石泉县', 'ankang');
INSERT INTO `district` VALUES ('34203', '交口县', 'lvliang');
INSERT INTO `district` VALUES ('34204', '连城县', 'longyan');
INSERT INTO `district` VALUES ('34205', '越城区', 'shaoxing');
INSERT INTO `district` VALUES ('34206', '怀仁县', 'shuozhou');
INSERT INTO `district` VALUES ('34207', '霞浦县', 'ningde');
INSERT INTO `district` VALUES ('34208', '东城区', 'beijing_city');
INSERT INTO `district` VALUES ('34209', '内黄县', 'anyang');
INSERT INTO `district` VALUES ('34210', '阳曲县', 'taiyuan');
INSERT INTO `district` VALUES ('34211', '阿坝县', 'aba');
INSERT INTO `district` VALUES ('34212', '嘉鱼县', 'xianning');
INSERT INTO `district` VALUES ('34213', '霍城县', 'yili');
INSERT INTO `district` VALUES ('34214', '罗甸县', 'qiannan');
INSERT INTO `district` VALUES ('34215', '历下区', 'jinan');
INSERT INTO `district` VALUES ('34216', '越西县', 'liangshan');
INSERT INTO `district` VALUES ('34217', '白玉县', 'ganzi');
INSERT INTO `district` VALUES ('34218', '沁水县', 'jincheng');
INSERT INTO `district` VALUES ('34219', '润州区', 'zhenjiang');
INSERT INTO `district` VALUES ('34220', '华安县', 'zhangzhou');
INSERT INTO `district` VALUES ('34221', '潢川县', 'xinyang');
INSERT INTO `district` VALUES ('34222', '英山县', 'huanggang');
INSERT INTO `district` VALUES ('34223', '康县', 'longnan');
INSERT INTO `district` VALUES ('34224', '北塘区', 'wuxi');
INSERT INTO `district` VALUES ('34225', '碟山区', 'wuzhou');
INSERT INTO `district` VALUES ('34226', '镜湖区', 'wuhu');
INSERT INTO `district` VALUES ('34227', '喀什地区', 'kashi');
INSERT INTO `district` VALUES ('34228', '固阳县', 'baotou');
INSERT INTO `district` VALUES ('34229', '定海区', 'zhoushan');
INSERT INTO `district` VALUES ('34230', '连云区', 'lianyungang');
INSERT INTO `district` VALUES ('34231', '迁西县', 'tangshan');
INSERT INTO `district` VALUES ('34232', '沙依巴克区', 'wulumuqi');
INSERT INTO `district` VALUES ('34233', '萨尔图区', 'daqing');
INSERT INTO `district` VALUES ('34234', '婺源县', 'shangrao');
INSERT INTO `district` VALUES ('34235', '七星区', 'guilin');
INSERT INTO `district` VALUES ('34236', '鼓楼区', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('34237', '安岳县', 'ziyang');
INSERT INTO `district` VALUES ('34238', '甘谷县', 'tianshui');
INSERT INTO `district` VALUES ('34239', '仲巴县', 'rikaze');
INSERT INTO `district` VALUES ('34240', '当涂县', 'maanshan');
INSERT INTO `district` VALUES ('34241', '陵水黎族自治县', 'lingshui');
INSERT INTO `district` VALUES ('34242', '新田县', 'yongzhou');
INSERT INTO `district` VALUES ('34243', '施甸县', 'baoshan');
INSERT INTO `district` VALUES ('34244', '绩溪县', 'xuancheng');
INSERT INTO `district` VALUES ('34245', '抚远县', 'jiamusi');
INSERT INTO `district` VALUES ('34246', '东川区', 'kunming');
INSERT INTO `district` VALUES ('34247', '汤阴县', 'anyang');
INSERT INTO `district` VALUES ('34248', '织金县', 'bijie');
INSERT INTO `district` VALUES ('34249', '宁远县', 'yongzhou');
INSERT INTO `district` VALUES ('34250', '桓台县', 'zibo');
INSERT INTO `district` VALUES ('34251', '渑池县', 'sanmenxia');
INSERT INTO `district` VALUES ('34252', '弥渡县', 'dali');
INSERT INTO `district` VALUES ('34253', '沐川县', 'leshan');
INSERT INTO `district` VALUES ('34254', '清河门区', 'fuxin');
INSERT INTO `district` VALUES ('34255', '阳谷县', 'liaocheng');
INSERT INTO `district` VALUES ('34256', '延边朝鲜族自治州', 'yanbian');
INSERT INTO `district` VALUES ('34257', '鱼台县', 'jining');
INSERT INTO `district` VALUES ('34258', '延长县', 'yanan');
INSERT INTO `district` VALUES ('34259', '连江县', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('34260', '五通桥区', 'leshan');
INSERT INTO `district` VALUES ('34261', '永胜县', 'lijiang');
INSERT INTO `district` VALUES ('34262', '临泉县', 'fuyang_anhui');
INSERT INTO `district` VALUES ('34263', '光山县', 'xinyang');
INSERT INTO `district` VALUES ('34264', '蠡县', 'baoding');
INSERT INTO `district` VALUES ('34265', '沾益县', 'qujing');
INSERT INTO `district` VALUES ('34266', '昭平县', 'hezhou');
INSERT INTO `district` VALUES ('34267', '集美区', 'xiamen');
INSERT INTO `district` VALUES ('34268', '固安县', 'langfang');
INSERT INTO `district` VALUES ('34269', '晋宁县', 'kunming');
INSERT INTO `district` VALUES ('34270', '汝城县', 'chenzhou');
INSERT INTO `district` VALUES ('34271', '班戈县', 'naqu');
INSERT INTO `district` VALUES ('34272', '元阳县', 'honghe');
INSERT INTO `district` VALUES ('34273', '铜梁县', 'chongqing_city');
INSERT INTO `district` VALUES ('34274', '东港区', 'rizhao');
INSERT INTO `district` VALUES ('34275', '宣恩县', 'enshi');
INSERT INTO `district` VALUES ('34276', '琅琊区', 'chuzhou');
INSERT INTO `district` VALUES ('34277', '高唐县', 'liaocheng');
INSERT INTO `district` VALUES ('34278', '丰顺县', 'meizhou');
INSERT INTO `district` VALUES ('34279', '麦积区', 'tianshui');
INSERT INTO `district` VALUES ('34280', '睢宁县', 'xuzhou');
INSERT INTO `district` VALUES ('34281', '南部县', 'nanchong');
INSERT INTO `district` VALUES ('34282', '高淳县', 'nanjing');
INSERT INTO `district` VALUES ('34283', '木兰县', 'haerbin');
INSERT INTO `district` VALUES ('34284', '扎囊县', 'shannan');
INSERT INTO `district` VALUES ('34285', '单县', 'heze');
INSERT INTO `district` VALUES ('34286', '洪泽县', 'huaian');
INSERT INTO `district` VALUES ('34287', '汶上县', 'jining');
INSERT INTO `district` VALUES ('34288', '淮上区', 'bangbu');
INSERT INTO `district` VALUES ('34289', '君山区', 'yueyang');
INSERT INTO `district` VALUES ('34290', '察布查尔锡伯自治县', 'yili');
INSERT INTO `district` VALUES ('34291', '绥中县', 'huludao');
INSERT INTO `district` VALUES ('34292', '西青区', 'tianjin_city');
INSERT INTO `district` VALUES ('34293', '雄县', 'baoding');
INSERT INTO `district` VALUES ('34294', '尖山区', 'shuangyashan');
INSERT INTO `district` VALUES ('34295', '孙吴县', 'heihe');
INSERT INTO `district` VALUES ('34296', '察雅县', 'changdu');
INSERT INTO `district` VALUES ('34297', '新龙县', 'ganzi');
INSERT INTO `district` VALUES ('34298', '鹿城区', 'wenzhou');
INSERT INTO `district` VALUES ('34299', '水城县', 'liupanshui');
INSERT INTO `district` VALUES ('34300', '大同区', 'daqing');
INSERT INTO `district` VALUES ('34301', '文水县', 'lvliang');
INSERT INTO `district` VALUES ('34302', '裕民县', 'tacheng');
INSERT INTO `district` VALUES ('34303', '长武县', 'xianyang');
INSERT INTO `district` VALUES ('34304', '巴林右旗', 'chifeng');
INSERT INTO `district` VALUES ('34305', '浦江县', 'jinhua');
INSERT INTO `district` VALUES ('34306', '乌拉特前旗', 'bayannaoer');
INSERT INTO `district` VALUES ('34307', '通川区', 'dazhou');
INSERT INTO `district` VALUES ('34308', '成武县', 'heze');
INSERT INTO `district` VALUES ('34309', '瓜州县', 'jiuquan');
INSERT INTO `district` VALUES ('34310', '新丰县', 'shaoguan');
INSERT INTO `district` VALUES ('34311', '连平县', 'heyuan');
INSERT INTO `district` VALUES ('34312', '凤庆县', 'lincang');
INSERT INTO `district` VALUES ('34313', '海州区', 'fuxin');
INSERT INTO `district` VALUES ('34314', '云溪区', 'yueyang');
INSERT INTO `district` VALUES ('34315', '天桥区', 'jinan');
INSERT INTO `district` VALUES ('34316', '汝南县', 'zhumadian');
INSERT INTO `district` VALUES ('34317', '将乐县', 'sanming');
INSERT INTO `district` VALUES ('34318', '盐田区', 'shenzhen');
INSERT INTO `district` VALUES ('34319', '通许县', 'kaifeng');
INSERT INTO `district` VALUES ('34320', '九原区', 'baotou');
INSERT INTO `district` VALUES ('34321', '托克逊县', 'tulufan');
INSERT INTO `district` VALUES ('34322', '仙游县', 'putian');
INSERT INTO `district` VALUES ('34323', '田东县', 'baise');
INSERT INTO `district` VALUES ('34324', '全州县', 'guilin');
INSERT INTO `district` VALUES ('34325', '南丹县', 'hechi');
INSERT INTO `district` VALUES ('34326', '遂昌县', 'lishui');
INSERT INTO `district` VALUES ('34327', '钦北区', 'qinzhou');
INSERT INTO `district` VALUES ('34328', '西夏区', 'yinchuan');
INSERT INTO `district` VALUES ('34329', '仪陇县', 'nanchong');
INSERT INTO `district` VALUES ('34330', '凤台县', 'huainan');
INSERT INTO `district` VALUES ('34331', '杜尔伯特蒙古族自治县', 'daqing');
INSERT INTO `district` VALUES ('34332', '文县', 'longnan');
INSERT INTO `district` VALUES ('34333', '阿瓦提县', 'akesu');
INSERT INTO `district` VALUES ('34334', '长岭县', 'songyuan');
INSERT INTO `district` VALUES ('34335', '红山区', 'chifeng');
INSERT INTO `district` VALUES ('34336', '黄埔区', 'guangzhou');
INSERT INTO `district` VALUES ('34337', '海勃湾区', 'wuhai');
INSERT INTO `district` VALUES ('34338', '献县', 'cangzhou');
INSERT INTO `district` VALUES ('34339', '安宁区', 'lanzhou');
INSERT INTO `district` VALUES ('34340', '天山区', 'wulumuqi');
INSERT INTO `district` VALUES ('34341', '萨迦县', 'rikaze');
INSERT INTO `district` VALUES ('34342', '铜官山区', 'tongling');
INSERT INTO `district` VALUES ('34343', '衡山县', 'hengyang');
INSERT INTO `district` VALUES ('34344', '积石山保安族东乡族撒拉族自治县', 'linxia');
INSERT INTO `district` VALUES ('34345', '小河区', 'guiyang');
INSERT INTO `district` VALUES ('34346', '墨玉县', 'hetian');
INSERT INTO `district` VALUES ('34347', '颍东区', 'fuyang_anhui');
INSERT INTO `district` VALUES ('34348', '栖霞区', 'nanjing');
INSERT INTO `district` VALUES ('34349', '安定区', 'dingxi');
INSERT INTO `district` VALUES ('34350', '衢江区', 'quzhou');
INSERT INTO `district` VALUES ('34351', '建昌县', 'huludao');
INSERT INTO `district` VALUES ('34352', '长泰县', 'zhangzhou');
INSERT INTO `district` VALUES ('34353', '辽中县', 'shenyang');
INSERT INTO `district` VALUES ('34354', '金平苗族瑶族傣族自治县', 'honghe');
INSERT INTO `district` VALUES ('34355', '珙县', 'yibin');
INSERT INTO `district` VALUES ('34356', '纳雍县', 'bijie');
INSERT INTO `district` VALUES ('34357', '松山区', 'chifeng');
INSERT INTO `district` VALUES ('34358', '阎良区', 'xian');
INSERT INTO `district` VALUES ('34359', '水富县', 'zhaotong');
INSERT INTO `district` VALUES ('34360', '郧西县', 'shiyan');
INSERT INTO `district` VALUES ('34361', '绍兴县', 'shaoxing');
INSERT INTO `district` VALUES ('34362', '青白江区', 'chengdu');
INSERT INTO `district` VALUES ('34363', '安源区', 'pingxiang');
INSERT INTO `district` VALUES ('34364', '松北区', 'haerbin');
INSERT INTO `district` VALUES ('34365', '武陟县', 'jiaozuo');
INSERT INTO `district` VALUES ('34366', '洛江区', 'quanzhou');
INSERT INTO `district` VALUES ('34367', '佛坪县', 'hanzhong');
INSERT INTO `district` VALUES ('34368', '平川区', 'baiyin');
INSERT INTO `district` VALUES ('34369', '新安县', 'luoyang');
INSERT INTO `district` VALUES ('34370', '江宁区', 'nanjing');
INSERT INTO `district` VALUES ('34371', '锦江区', 'chengdu');
INSERT INTO `district` VALUES ('34372', '宿城区', 'suqian');
INSERT INTO `district` VALUES ('34373', '复兴区', 'handan');
INSERT INTO `district` VALUES ('34374', '临颍县', 'luohe');
INSERT INTO `district` VALUES ('34375', '资阳区', 'yiyang');
INSERT INTO `district` VALUES ('34376', '资源县', 'guilin');
INSERT INTO `district` VALUES ('34377', '丰宁满族自治县', 'chengde');
INSERT INTO `district` VALUES ('34378', '环翠区', 'weihai');
INSERT INTO `district` VALUES ('34379', '阳朔县', 'guilin');
INSERT INTO `district` VALUES ('34380', '桥西区', 'xingtai');
INSERT INTO `district` VALUES ('34381', '南明区', 'guiyang');
INSERT INTO `district` VALUES ('34382', '大东区', 'shenyang');
INSERT INTO `district` VALUES ('34383', '澄江县', 'yuxi');
INSERT INTO `district` VALUES ('34384', '罗庄区', 'linyi');
INSERT INTO `district` VALUES ('34385', '南澳县', 'shantou');
INSERT INTO `district` VALUES ('34386', '慈利县', 'zhangjiajie');
INSERT INTO `district` VALUES ('34387', '繁峙县', 'xinzhou');
INSERT INTO `district` VALUES ('34388', '惠民县', 'binzhou');
INSERT INTO `district` VALUES ('34389', '雨花台区', 'nanjing');
INSERT INTO `district` VALUES ('34390', '东昌府区', 'liaocheng');
INSERT INTO `district` VALUES ('34391', '鼎城区', 'changde');
INSERT INTO `district` VALUES ('34392', '德安县', 'jiujiang');
INSERT INTO `district` VALUES ('34393', '二道江区', 'tonghua');
INSERT INTO `district` VALUES ('34394', '平鲁区', 'shuozhou');
INSERT INTO `district` VALUES ('34395', '琼结县', 'shannan');
INSERT INTO `district` VALUES ('34396', '下城区', 'hangzhou');
INSERT INTO `district` VALUES ('34397', '巴音郭楞蒙古自治州', 'bayinguoleng');
INSERT INTO `district` VALUES ('34398', '紫金县', 'heyuan');
INSERT INTO `district` VALUES ('34399', '青山湖区', 'nanchang');
INSERT INTO `district` VALUES ('34400', '林甸县', 'daqing');
INSERT INTO `district` VALUES ('34401', '辰溪县', 'huaihua');
INSERT INTO `district` VALUES ('34402', '雁江区', 'ziyang');
INSERT INTO `district` VALUES ('34403', '刚察县', 'haibei');
INSERT INTO `district` VALUES ('34404', '垦利县', 'dongying');
INSERT INTO `district` VALUES ('34405', '江口县', 'tongren');
INSERT INTO `district` VALUES ('34406', '灵山县', 'qinzhou');
INSERT INTO `district` VALUES ('34407', '勃利县', 'qitaihe');
INSERT INTO `district` VALUES ('34408', '巧家县', 'zhaotong');
INSERT INTO `district` VALUES ('34409', '襄垣县', 'changzhi');
INSERT INTO `district` VALUES ('34410', '海盐县', 'jiaxing');
INSERT INTO `district` VALUES ('34411', '金家庄区', 'maanshan');
INSERT INTO `district` VALUES ('34412', '鮁鱼圈区', 'yingkou');
INSERT INTO `district` VALUES ('34413', '水磨沟区', 'wulumuqi');
INSERT INTO `district` VALUES ('34414', '丛台区', 'handan');
INSERT INTO `district` VALUES ('34415', '大通区', 'huainan');
INSERT INTO `district` VALUES ('34416', '突泉县', 'xingan');
INSERT INTO `district` VALUES ('34417', '会理县', 'liangshan');
INSERT INTO `district` VALUES ('34418', '赣县', 'ganzhou');
INSERT INTO `district` VALUES ('34419', '召陵区', 'luohe');
INSERT INTO `district` VALUES ('34420', '光泽县', 'nanping');
INSERT INTO `district` VALUES ('34421', '武城县', 'dezhou');
INSERT INTO `district` VALUES ('34422', '铜陵县', 'tongling');
INSERT INTO `district` VALUES ('34423', '林西县', 'chifeng');
INSERT INTO `district` VALUES ('34424', '南谯区', 'chuzhou');
INSERT INTO `district` VALUES ('34425', '寻乌县', 'ganzhou');
INSERT INTO `district` VALUES ('34426', '赤坎区', 'zhanjiang');
INSERT INTO `district` VALUES ('34427', '偏关县', 'xinzhou');
INSERT INTO `district` VALUES ('34428', '钢城区', 'laiwu');
INSERT INTO `district` VALUES ('34429', '泾阳县', 'xianyang');
INSERT INTO `district` VALUES ('34430', '会昌县', 'ganzhou');
INSERT INTO `district` VALUES ('34431', '巩留县', 'yili');
INSERT INTO `district` VALUES ('34432', '阳东县', 'yangjiang');
INSERT INTO `district` VALUES ('34433', '榆阳区', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('34434', '贡山独龙族怒族自治县', 'nujiang');
INSERT INTO `district` VALUES ('34435', '平南县', 'guigang');
INSERT INTO `district` VALUES ('34436', '沙雅县', 'akesu');
INSERT INTO `district` VALUES ('34437', '洛隆县', 'changdu');
INSERT INTO `district` VALUES ('34438', '梁子湖区', 'ezhou');
INSERT INTO `district` VALUES ('34439', '奇台县', 'changji');
INSERT INTO `district` VALUES ('34440', '肃北蒙古族自治县', 'jiuquan');
INSERT INTO `district` VALUES ('34441', '平乐县', 'guilin');
INSERT INTO `district` VALUES ('34442', '饶河县', 'shuangyashan');
INSERT INTO `district` VALUES ('34443', '顺庆区', 'nanchong');
INSERT INTO `district` VALUES ('34444', '伊宁县', 'yili');
INSERT INTO `district` VALUES ('34445', '沙湾县', 'tacheng');
INSERT INTO `district` VALUES ('34446', '泸溪县', 'xiangxi');
INSERT INTO `district` VALUES ('34447', '宝山区', 'shuangyashan');
INSERT INTO `district` VALUES ('34448', '彰武县', 'fuxin');
INSERT INTO `district` VALUES ('34449', '达日县', 'guoluo');
INSERT INTO `district` VALUES ('34450', '闽侯县', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('34451', '南川区', 'chongqing_city');
INSERT INTO `district` VALUES ('34452', '建平县', 'chaoyang');
INSERT INTO `district` VALUES ('34453', '南召县', 'nanyang');
INSERT INTO `district` VALUES ('34454', '拜泉县', 'qiqihaer');
INSERT INTO `district` VALUES ('34455', '平房区', 'haerbin');
INSERT INTO `district` VALUES ('34456', '亭湖区', 'yancheng');
INSERT INTO `district` VALUES ('34457', '陇西县', 'dingxi');
INSERT INTO `district` VALUES ('34458', '包河区', 'hefei');
INSERT INTO `district` VALUES ('34459', '平原县', 'dezhou');
INSERT INTO `district` VALUES ('34460', '淮阳县', 'zhoukou');
INSERT INTO `district` VALUES ('34461', '太平区', 'fuxin');
INSERT INTO `district` VALUES ('34462', '泾县', 'xuancheng');
INSERT INTO `district` VALUES ('34463', '普陀区', 'zhoushan');
INSERT INTO `district` VALUES ('34464', '馆陶县', 'handan');
INSERT INTO `district` VALUES ('34465', '芮城县', 'yuncheng');
INSERT INTO `district` VALUES ('34466', '襄城县', 'xuchang');
INSERT INTO `district` VALUES ('34468', '宝兴县', 'yaan');
INSERT INTO `district` VALUES ('34469', '龙安区', 'anyang');
INSERT INTO `district` VALUES ('34470', '通榆县', 'baicheng');
INSERT INTO `district` VALUES ('34471', '灵川县', 'guilin');
INSERT INTO `district` VALUES ('34472', '金凤区', 'yinchuan');
INSERT INTO `district` VALUES ('34473', '岑巩县', 'qiandongnan');
INSERT INTO `district` VALUES ('34474', '泗水县', 'jining');
INSERT INTO `district` VALUES ('34475', '茂南区', 'maoming');
INSERT INTO `district` VALUES ('34476', '比如县', 'naqu');
INSERT INTO `district` VALUES ('34477', '东丰县', 'liaoyuan');
INSERT INTO `district` VALUES ('34478', '南和县', 'xingtai');
INSERT INTO `district` VALUES ('34479', '赫山区', 'yiyang');
INSERT INTO `district` VALUES ('34480', '下关区', 'nanjing');
INSERT INTO `district` VALUES ('34481', '桑植县', 'zhangjiajie');
INSERT INTO `district` VALUES ('34482', '广宗县', 'xingtai');
INSERT INTO `district` VALUES ('34483', '克拉玛依区', 'kelamayi');
INSERT INTO `district` VALUES ('34484', '荣县', 'zigong');
INSERT INTO `district` VALUES ('34485', '许昌县', 'xuchang');
INSERT INTO `district` VALUES ('34486', '海南区', 'wuhai');
INSERT INTO `district` VALUES ('34487', '南汇区', 'shanghai_city');
INSERT INTO `district` VALUES ('34488', '云龙县', 'dali');
INSERT INTO `district` VALUES ('34489', '丰满区', 'jilin_city');
INSERT INTO `district` VALUES ('34490', '彝良县', 'zhaotong');
INSERT INTO `district` VALUES ('34491', '南山区', 'hegang');
INSERT INTO `district` VALUES ('34492', '官渡区', 'kunming');
INSERT INTO `district` VALUES ('34493', '天台县', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('34494', '浦东新区', 'shanghai_city');
INSERT INTO `district` VALUES ('34495', '梁山县', 'jining');
INSERT INTO `district` VALUES ('34496', '旌德县', 'xuancheng');
INSERT INTO `district` VALUES ('34497', '信丰县', 'ganzhou');
INSERT INTO `district` VALUES ('34498', '深泽县', 'shijiazhuang');
INSERT INTO `district` VALUES ('34499', '天峨县', 'hechi');
INSERT INTO `district` VALUES ('34500', '古蔺县', 'luzhou');
INSERT INTO `district` VALUES ('34501', '永登县', 'lanzhou');
INSERT INTO `district` VALUES ('34502', '赛罕区', 'huhehaote');
INSERT INTO `district` VALUES ('34503', '杜集区', 'huaibei');
INSERT INTO `district` VALUES ('34504', '喜德县', 'liangshan');
INSERT INTO `district` VALUES ('34505', '鄂城区', 'ezhou');
INSERT INTO `district` VALUES ('34506', '凤泉区', 'xinxiang');
INSERT INTO `district` VALUES ('34507', '李沧区', 'qingdao');
INSERT INTO `district` VALUES ('34508', '崆峒区', 'pingliang');
INSERT INTO `district` VALUES ('34509', '紫阳县', 'ankang');
INSERT INTO `district` VALUES ('34510', '临城县', 'xingtai');
INSERT INTO `district` VALUES ('34511', '成华区', 'chengdu');
INSERT INTO `district` VALUES ('34512', '石龙区', 'pingdingshan');
INSERT INTO `district` VALUES ('34513', '绥宁县', 'shaoyang');
INSERT INTO `district` VALUES ('34514', '金秀瑶族自治县', 'laibin');
INSERT INTO `district` VALUES ('34515', '青山区', 'baotou');
INSERT INTO `district` VALUES ('34516', '贡井区', 'zigong');
INSERT INTO `district` VALUES ('34517', '新城区', 'huhehaote');
INSERT INTO `district` VALUES ('34518', '月湖区', 'yingtan');
INSERT INTO `district` VALUES ('34519', '疏附县', 'kashi');
INSERT INTO `district` VALUES ('34520', '仁化县', 'shaoguan');
INSERT INTO `district` VALUES ('34521', '鼓楼区', 'nanjing');
INSERT INTO `district` VALUES ('34522', '垫江县', 'chongqing_city');
INSERT INTO `district` VALUES ('34523', '福贡县', 'nujiang');
INSERT INTO `district` VALUES ('34524', '海陵区', 'taizhou_jiangsu');
INSERT INTO `district` VALUES ('34525', '朝阳区', 'beijing_city');
INSERT INTO `district` VALUES ('34526', '高坪区', 'nanchong');
INSERT INTO `district` VALUES ('34527', '铁西区', 'shenyang');
INSERT INTO `district` VALUES ('34528', '金口河区', 'leshan');
INSERT INTO `district` VALUES ('34529', '河口瑶族自治县', 'honghe');
INSERT INTO `district` VALUES ('34530', '易县', 'baoding');
INSERT INTO `district` VALUES ('34531', '唐河县', 'nanyang');
INSERT INTO `district` VALUES ('34532', '隆化县', 'chengde');
INSERT INTO `district` VALUES ('34533', '东乌珠穆沁旗', 'xilinguole');
INSERT INTO `district` VALUES ('34534', '都安瑶族自治县', 'hechi');
INSERT INTO `district` VALUES ('34535', '涟水县', 'huaian');
INSERT INTO `district` VALUES ('34536', '景宁畲族自治县', 'lishui');
INSERT INTO `district` VALUES ('34537', '东营区', 'dongying');
INSERT INTO `district` VALUES ('34538', '钟山县', 'hezhou');
INSERT INTO `district` VALUES ('34539', '阜新蒙古族自治县', 'fuxin');
INSERT INTO `district` VALUES ('34540', '札达县', 'ali');
INSERT INTO `district` VALUES ('34541', '秦州区', 'tianshui');
INSERT INTO `district` VALUES ('34542', '丹寨县', 'qiandongnan');
INSERT INTO `district` VALUES ('34543', '嵩县', 'luoyang');
INSERT INTO `district` VALUES ('34544', '魏县', 'handan');
INSERT INTO `district` VALUES ('34545', '和林格尔县', 'huhehaote');
INSERT INTO `district` VALUES ('34546', '泉山区', 'xuzhou');
INSERT INTO `district` VALUES ('34547', '向阳区', 'jiamusi');
INSERT INTO `district` VALUES ('34548', '武胜县', 'guangan');
INSERT INTO `district` VALUES ('34549', '吴兴区', 'huzhou');
INSERT INTO `district` VALUES ('34550', '准格尔旗', 'eerduosi');
INSERT INTO `district` VALUES ('34551', '靖边县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('34552', '肥东县', 'hefei');
INSERT INTO `district` VALUES ('34553', '隆安县', 'nanning');
INSERT INTO `district` VALUES ('34554', '顺义区', 'beijing_city');
INSERT INTO `district` VALUES ('34555', '托里县', 'tacheng');
INSERT INTO `district` VALUES ('34556', '万柏林区', 'taiyuan');
INSERT INTO `district` VALUES ('34557', '镇赉县', 'baicheng');
INSERT INTO `district` VALUES ('34558', '新昌县', 'shaoxing');
INSERT INTO `district` VALUES ('34559', '兰山区', 'linyi');
INSERT INTO `district` VALUES ('34560', '户县', 'xian');
INSERT INTO `district` VALUES ('34561', '昔阳县', 'jinzhong');
INSERT INTO `district` VALUES ('34562', '卫滨区', 'xinxiang');
INSERT INTO `district` VALUES ('34563', '安次区', 'langfang');
INSERT INTO `district` VALUES ('34564', '潘集区', 'huainan');
INSERT INTO `district` VALUES ('34565', '永兴县', 'chenzhou');
INSERT INTO `district` VALUES ('34566', '蓟县', 'tianjin_city');
INSERT INTO `district` VALUES ('34567', '宜宾县', 'yibin');
INSERT INTO `district` VALUES ('34568', '镇康县', 'lincang');
INSERT INTO `district` VALUES ('34569', '林周县', 'lasa');
INSERT INTO `district` VALUES ('34570', '定结县', 'rikaze');
INSERT INTO `district` VALUES ('34571', '五峰土家族自治县', 'yichang');
INSERT INTO `district` VALUES ('34572', '武清区', 'tianjin_city');
INSERT INTO `district` VALUES ('34573', '兴安盟', 'xingan');
INSERT INTO `district` VALUES ('34574', '赞皇县', 'shijiazhuang');
INSERT INTO `district` VALUES ('34575', '兴国县', 'ganzhou');
INSERT INTO `district` VALUES ('34576', '道外区', 'haerbin');
INSERT INTO `district` VALUES ('34577', '香河县', 'langfang');
INSERT INTO `district` VALUES ('34578', '德昌县', 'liangshan');
INSERT INTO `district` VALUES ('34579', '安仁县', 'chenzhou');
INSERT INTO `district` VALUES ('34580', '孟津县', 'luoyang');
INSERT INTO `district` VALUES ('34581', '赣榆县', 'lianyungang');
INSERT INTO `district` VALUES ('34582', '前郭尔罗斯蒙古族自治县', 'songyuan');
INSERT INTO `district` VALUES ('34583', '千阳县', 'baoji');
INSERT INTO `district` VALUES ('34584', '莒县', 'rizhao');
INSERT INTO `district` VALUES ('34585', '靖宇县', 'baishan');
INSERT INTO `district` VALUES ('34586', '南沙区', 'guangzhou');
INSERT INTO `district` VALUES ('34587', '翁牛特旗', 'chifeng');
INSERT INTO `district` VALUES ('34588', '武强县', 'hengshui');
INSERT INTO `district` VALUES ('34589', '环江毛南族自治县', 'hechi');
INSERT INTO `district` VALUES ('34590', '南郊区', 'datong');
INSERT INTO `district` VALUES ('34591', '安溪县', 'quanzhou');
INSERT INTO `district` VALUES ('34592', '庐江县', 'chaohu');
INSERT INTO `district` VALUES ('34593', '息县', 'xinyang');
INSERT INTO `district` VALUES ('34594', '黄山区', 'huangshan');
INSERT INTO `district` VALUES ('34595', '尖草坪区', 'taiyuan');
INSERT INTO `district` VALUES ('34596', '鹤庆县', 'dali');
INSERT INTO `district` VALUES ('34597', '冷水滩区', 'yongzhou');
INSERT INTO `district` VALUES ('34598', '屯溪区', 'huangshan');
INSERT INTO `district` VALUES ('34599', '吴桥县', 'cangzhou');
INSERT INTO `district` VALUES ('34600', '英吉沙县', 'kashi');
INSERT INTO `district` VALUES ('34601', '四方台区', 'shuangyashan');
INSERT INTO `district` VALUES ('34602', '长海县', 'dalian');
INSERT INTO `district` VALUES ('34603', '宾川县', 'dali');
INSERT INTO `district` VALUES ('34604', '郯城县', 'linyi');
INSERT INTO `district` VALUES ('34605', '富川瑶族自治县', 'hezhou');
INSERT INTO `district` VALUES ('34606', '槐荫区', 'jinan');
INSERT INTO `district` VALUES ('34607', '谯城区', 'bozhou');
INSERT INTO `district` VALUES ('34608', '路桥区', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('34609', '青阳县', 'chizhou');
INSERT INTO `district` VALUES ('34610', '黔西县', 'bijie');
INSERT INTO `district` VALUES ('34611', '威信县', 'zhaotong');
INSERT INTO `district` VALUES ('34612', '遂溪县', 'zhanjiang');
INSERT INTO `district` VALUES ('34613', '班玛县', 'guoluo');
INSERT INTO `district` VALUES ('34614', '乐业县', 'baise');
INSERT INTO `district` VALUES ('34615', '枞阳县', 'anqing');
INSERT INTO `district` VALUES ('34616', '彭水苗族土家族自治县', 'chongqing_city');
INSERT INTO `district` VALUES ('34617', '西乌珠穆沁旗', 'xilinguole');
INSERT INTO `district` VALUES ('34618', '阜宁县', 'yancheng');
INSERT INTO `district` VALUES ('34619', '港北区', 'guigang');
INSERT INTO `district` VALUES ('34620', '习水县', 'zunyi');
INSERT INTO `district` VALUES ('34621', '宣化县', 'zhangjiakou');
INSERT INTO `district` VALUES ('34622', '武乡县', 'changzhi');
INSERT INTO `district` VALUES ('34623', '寿县', 'liuan');
INSERT INTO `district` VALUES ('34624', '临泽县', 'zhangye');
INSERT INTO `district` VALUES ('34625', '麻山区', 'jixi');
INSERT INTO `district` VALUES ('34626', '额济纳旗', 'alashan');
INSERT INTO `district` VALUES ('34627', '中方县', 'huaihua');
INSERT INTO `district` VALUES ('34628', '稻城县', 'ganzi');
INSERT INTO `district` VALUES ('34629', '石棉县', 'yaan');
INSERT INTO `district` VALUES ('34630', '新会区', 'jiangmen');
INSERT INTO `district` VALUES ('34631', '盂县', 'yangquan');
INSERT INTO `district` VALUES ('34632', '爱辉区', 'heihe');
INSERT INTO `district` VALUES ('34633', '惠来县', 'jieyang');
INSERT INTO `district` VALUES ('34634', '宜秀区', 'anqing');
INSERT INTO `district` VALUES ('34635', '勐海县', 'xishuangbanna');
INSERT INTO `district` VALUES ('34636', '南沙群岛', 'nansha');
INSERT INTO `district` VALUES ('34637', '郏县', 'pingdingshan');
INSERT INTO `district` VALUES ('34638', '云龙区', 'xuzhou');
INSERT INTO `district` VALUES ('34639', '民勤县', 'wuwei');
INSERT INTO `district` VALUES ('34640', '海曙区', 'ningbo');
INSERT INTO `district` VALUES ('34641', '纳溪区', 'luzhou');
INSERT INTO `district` VALUES ('34642', '长子县', 'changzhi');
INSERT INTO `district` VALUES ('34643', '通山县', 'xianning');
INSERT INTO `district` VALUES ('34644', '昭苏县', 'yili');
INSERT INTO `district` VALUES ('34645', '南华县', 'chuxiong');
INSERT INTO `district` VALUES ('34646', '桥东区', 'xingtai');
INSERT INTO `district` VALUES ('34647', '北林区', 'suihua');
INSERT INTO `district` VALUES ('34648', '萧县', 'suzhou_anhui');
INSERT INTO `district` VALUES ('34649', '正镶白旗', 'xilinguole');
INSERT INTO `district` VALUES ('34650', '潼关县', 'weinan');
INSERT INTO `district` VALUES ('34651', '九龙县', 'ganzi');
INSERT INTO `district` VALUES ('34652', '那曲地区', 'naqu');
INSERT INTO `district` VALUES ('34653', '花都区', 'guangzhou');
INSERT INTO `district` VALUES ('34654', '滨城区', 'binzhou');
INSERT INTO `district` VALUES ('34655', '察哈尔右翼后旗', 'wulanchabu');
INSERT INTO `district` VALUES ('34656', '商河县', 'jinan');
INSERT INTO `district` VALUES ('34657', '仁布县', 'rikaze');
INSERT INTO `district` VALUES ('34658', '玉山县', 'shangrao');
INSERT INTO `district` VALUES ('34659', '龙江县', 'qiqihaer');
INSERT INTO `district` VALUES ('34660', '天宁区', 'changzhou');
INSERT INTO `district` VALUES ('34661', '娄烦县', 'taiyuan');
INSERT INTO `district` VALUES ('34662', '洱源县', 'dali');
INSERT INTO `district` VALUES ('34663', '商都县', 'wulanchabu');
INSERT INTO `district` VALUES ('34664', '宜阳县', 'luoyang');
INSERT INTO `district` VALUES ('34665', '宝丰县', 'pingdingshan');
INSERT INTO `district` VALUES ('34666', '舞阳县', 'luohe');
INSERT INTO `district` VALUES ('34667', '阿里地区', 'ali');
INSERT INTO `district` VALUES ('34668', '右江区', 'baise');
INSERT INTO `district` VALUES ('34669', '镇坪县', 'ankang');
INSERT INTO `district` VALUES ('34670', '滨湖区', 'wuxi');
INSERT INTO `district` VALUES ('34671', '长顺县', 'qiannan');
INSERT INTO `district` VALUES ('34672', '藤县', 'wuzhou');
INSERT INTO `district` VALUES ('34673', '金州区', 'dalian');
INSERT INTO `district` VALUES ('34674', '酉阳土家族苗族自治县', 'chongqing_city');
INSERT INTO `district` VALUES ('34675', '裕安区', 'liuan');
INSERT INTO `district` VALUES ('34676', '平山区', 'benxi');
INSERT INTO `district` VALUES ('34677', '东安县', 'yongzhou');
INSERT INTO `district` VALUES ('34678', '丹徒区', 'zhenjiang');
INSERT INTO `district` VALUES ('34679', '河西区', 'tianjin_city');
INSERT INTO `district` VALUES ('34680', '盘龙区', 'kunming');
INSERT INTO `district` VALUES ('34681', '七里河区', 'lanzhou');
INSERT INTO `district` VALUES ('34682', '绥德县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('34683', '梨树区', 'jixi');
INSERT INTO `district` VALUES ('34684', '萨嘎县', 'rikaze');
INSERT INTO `district` VALUES ('34685', '融水苗族自治县', 'liuzhou');
INSERT INTO `district` VALUES ('34686', '鹿寨县', 'liuzhou');
INSERT INTO `district` VALUES ('34687', '扶风县', 'baoji');
INSERT INTO `district` VALUES ('34688', '二七区', 'zhengzhou');
INSERT INTO `district` VALUES ('34689', '沈北新区', 'shenyang');
INSERT INTO `district` VALUES ('34690', '卧龙区', 'nanyang');
INSERT INTO `district` VALUES ('34691', '任县', 'xingtai');
INSERT INTO `district` VALUES ('34692', '唐县', 'baoding');
INSERT INTO `district` VALUES ('34693', '弥勒县', 'honghe');
INSERT INTO `district` VALUES ('34694', '楚雄彝族自治州', 'chuxiong');
INSERT INTO `district` VALUES ('34695', '海晏县', 'haibei');
INSERT INTO `district` VALUES ('34696', '东明县', 'heze');
INSERT INTO `district` VALUES ('34697', '阿拉善左旗', 'alashan');
INSERT INTO `district` VALUES ('34698', '贞丰县', 'qianxinan');
INSERT INTO `district` VALUES ('34699', '行唐县', 'shijiazhuang');
INSERT INTO `district` VALUES ('34700', '大洼县', 'panjin');
INSERT INTO `district` VALUES ('34701', '民丰县', 'hetian');
INSERT INTO `district` VALUES ('34702', '金明区', 'kaifeng');
INSERT INTO `district` VALUES ('34703', '屯留县', 'changzhi');
INSERT INTO `district` VALUES ('34704', '汉台区', 'hanzhong');
INSERT INTO `district` VALUES ('34705', '莲花县', 'pingxiang');
INSERT INTO `district` VALUES ('34706', '广灵县', 'datong');
INSERT INTO `district` VALUES ('34707', '虞城县', 'shangqiu');
INSERT INTO `district` VALUES ('34708', '丰都县', 'chongqing_city');
INSERT INTO `district` VALUES ('34709', '楚州区', 'huaian');
INSERT INTO `district` VALUES ('34710', '镇巴县', 'hanzhong');
INSERT INTO `district` VALUES ('34711', '福田区', 'shenzhen');
INSERT INTO `district` VALUES ('34712', '邹平县', 'binzhou');
INSERT INTO `district` VALUES ('34713', '新晃侗族自治县', 'huaihua');
INSERT INTO `district` VALUES ('34714', '中宁县', 'zhongwei');
INSERT INTO `district` VALUES ('34715', '青县', 'cangzhou');
INSERT INTO `district` VALUES ('34716', '曲阳县', 'baoding');
INSERT INTO `district` VALUES ('34717', '皮山县', 'hetian');
INSERT INTO `district` VALUES ('34718', '蒙阴县', 'linyi');
INSERT INTO `district` VALUES ('34719', '明溪县', 'sanming');
INSERT INTO `district` VALUES ('34720', '杨陵区', 'xianyang');
INSERT INTO `district` VALUES ('34721', '商水县', 'zhoukou');
INSERT INTO `district` VALUES ('34722', '浦北县', 'qinzhou');
INSERT INTO `district` VALUES ('34723', '柳江县', 'liuzhou');
INSERT INTO `district` VALUES ('34724', '雨城区', 'yaan');
INSERT INTO `district` VALUES ('34725', '兴山区', 'hegang');
INSERT INTO `district` VALUES ('34726', '曾都区', 'suizhou');
INSERT INTO `district` VALUES ('34727', '景县', 'hengshui');
INSERT INTO `district` VALUES ('34728', '阿荣旗', 'hulunbeier');
INSERT INTO `district` VALUES ('34729', '修水县', 'jiujiang');
INSERT INTO `district` VALUES ('34730', '德钦县', 'diqing');
INSERT INTO `district` VALUES ('34731', '巴东县', 'enshi');
INSERT INTO `district` VALUES ('34732', '乐都县', 'haidong');
INSERT INTO `district` VALUES ('34733', '永清县', 'langfang');
INSERT INTO `district` VALUES ('34734', '通城县', 'xianning');
INSERT INTO `district` VALUES ('34735', '南海区', 'foshan');
INSERT INTO `district` VALUES ('34736', '抚松县', 'baishan');
INSERT INTO `district` VALUES ('34737', '德保县', 'baise');
INSERT INTO `district` VALUES ('34738', '乌镇', 'jiaxing');
INSERT INTO `district` VALUES ('34739', '小金县', 'aba');
INSERT INTO `district` VALUES ('34740', '顺河回族区', 'kaifeng');
INSERT INTO `district` VALUES ('34741', '呼图壁县', 'changji');
INSERT INTO `district` VALUES ('34742', '盈江县', 'dehong');
INSERT INTO `district` VALUES ('34743', '康定县', 'ganzi');
INSERT INTO `district` VALUES ('34744', '费县', 'linyi');
INSERT INTO `district` VALUES ('34745', '西畴县', 'wenshan');
INSERT INTO `district` VALUES ('34746', '阳明区', 'mudanjiang');
INSERT INTO `district` VALUES ('34747', '湄潭县', 'zunyi');
INSERT INTO `district` VALUES ('34748', '东西湖区', 'wuhan');
INSERT INTO `district` VALUES ('34749', '城固县', 'hanzhong');
INSERT INTO `district` VALUES ('34750', '平桥区', 'xinyang');
INSERT INTO `district` VALUES ('34751', '金台区', 'baoji');
INSERT INTO `district` VALUES ('34752', '谷城县', 'xiangfan');
INSERT INTO `district` VALUES ('34753', '上思县', 'fangchenggang');
INSERT INTO `district` VALUES ('34754', '吉利区', 'luoyang');
INSERT INTO `district` VALUES ('34755', '沁源县', 'changzhi');
INSERT INTO `district` VALUES ('34756', '理县', 'aba');
INSERT INTO `district` VALUES ('34757', '河东区', 'linyi');
INSERT INTO `district` VALUES ('34758', '长岛县', 'yantai');
INSERT INTO `district` VALUES ('34759', '柘荣县', 'ningde');
INSERT INTO `district` VALUES ('34760', '岷县', 'dingxi');
INSERT INTO `district` VALUES ('34761', '吉木萨尔县', 'changji');
INSERT INTO `district` VALUES ('34762', '元氏县', 'shijiazhuang');
INSERT INTO `district` VALUES ('34763', '东昌区', 'tonghua');
INSERT INTO `district` VALUES ('34764', '商南县', 'shangluo');
INSERT INTO `district` VALUES ('34765', '鸡东县', 'jixi');
INSERT INTO `district` VALUES ('34766', '龙港区', 'huludao');
INSERT INTO `district` VALUES ('34767', '石台县', 'chizhou');
INSERT INTO `district` VALUES ('34768', '怀来县', 'zhangjiakou');
INSERT INTO `district` VALUES ('34769', '清水县', 'tianshui');
INSERT INTO `district` VALUES ('34770', '晋源区', 'taiyuan');
INSERT INTO `district` VALUES ('34771', '甘泉县', 'yanan');
INSERT INTO `district` VALUES ('34772', '沙洋县', 'jingmen');
INSERT INTO `district` VALUES ('34773', '湟源县', 'xining');
INSERT INTO `district` VALUES ('34774', '江岸区', 'wuhan');
INSERT INTO `district` VALUES ('34775', '苏仙区', 'chenzhou');
INSERT INTO `district` VALUES ('34776', '宾阳县', 'nanning');
INSERT INTO `district` VALUES ('34777', '科尔沁区', 'tongliao');
INSERT INTO `district` VALUES ('34778', '黔南布依族苗族自治州', 'qiannan');
INSERT INTO `district` VALUES ('34779', '塘沽区', 'tianjin_city');
INSERT INTO `district` VALUES ('34780', '杭锦旗', 'eerduosi');
INSERT INTO `district` VALUES ('34781', '丁青县', 'changdu');
INSERT INTO `district` VALUES ('34782', '振兴区', 'dandong');
INSERT INTO `district` VALUES ('34783', '科尔沁右翼中旗', 'xingan');
INSERT INTO `district` VALUES ('34784', '陆良县', 'qujing');
INSERT INTO `district` VALUES ('34785', '桃江县', 'yiyang');
INSERT INTO `district` VALUES ('34786', '白塔区', 'liaoyang');
INSERT INTO `district` VALUES ('34787', '古城区', 'lijiang');
INSERT INTO `district` VALUES ('34788', '居巢区', 'chaohu');
INSERT INTO `district` VALUES ('34789', '临河区', 'bayannaoer');
INSERT INTO `district` VALUES ('34790', '安龙县', 'qianxinan');
INSERT INTO `district` VALUES ('34791', '灵石县', 'jinzhong');
INSERT INTO `district` VALUES ('34792', '潍城区', 'weifang');
INSERT INTO `district` VALUES ('34793', '宛城区', 'nanyang');
INSERT INTO `district` VALUES ('34794', '绛县', 'yuncheng');
INSERT INTO `district` VALUES ('34795', '昂昂溪区', 'qiqihaer');
INSERT INTO `district` VALUES ('34796', '蜀山区', 'hefei');
INSERT INTO `district` VALUES ('34797', '叙永县', 'luzhou');
INSERT INTO `district` VALUES ('34798', '雨山区', 'maanshan');
INSERT INTO `district` VALUES ('34799', '襄城区', 'xiangfan');
INSERT INTO `district` VALUES ('34800', '南涧彝族自治县', 'dali');
INSERT INTO `district` VALUES ('34801', '大祥区', 'shaoyang');
INSERT INTO `district` VALUES ('34802', '涪陵区', 'chongqing_city');
INSERT INTO `district` VALUES ('34803', '阳山县', 'qingyuan');
INSERT INTO `district` VALUES ('34804', '拜城县', 'akesu');
INSERT INTO `district` VALUES ('34805', '修文县', 'guiyang');
INSERT INTO `district` VALUES ('34806', '武平县', 'longyan');
INSERT INTO `district` VALUES ('34807', '西湖区', 'nanchang');
INSERT INTO `district` VALUES ('34808', '忠县', 'chongqing_city');
INSERT INTO `district` VALUES ('34809', '共和县', 'hainanzangzu');
INSERT INTO `district` VALUES ('34810', '永福县', 'guilin');
INSERT INTO `district` VALUES ('34811', '阜平县', 'baoding');
INSERT INTO `district` VALUES ('34812', '博罗县', 'huizhou_guangdong');
INSERT INTO `district` VALUES ('34813', '开阳县', 'guiyang');
INSERT INTO `district` VALUES ('34814', '西峡县', 'nanyang');
INSERT INTO `district` VALUES ('34815', '澧县', 'changde');
INSERT INTO `district` VALUES ('34816', '蔡甸区', 'wuhan');
INSERT INTO `district` VALUES ('34817', '汶川县', 'aba');
INSERT INTO `district` VALUES ('34818', '科尔沁右翼前旗', 'xingan');
INSERT INTO `district` VALUES ('34819', '涞源县', 'baoding');
INSERT INTO `district` VALUES ('34820', '永川区', 'chongqing_city');
INSERT INTO `district` VALUES ('34821', '昌乐县', 'weifang');
INSERT INTO `district` VALUES ('34822', '库伦旗', 'tongliao');
INSERT INTO `district` VALUES ('34823', '田家庵区', 'huainan');
INSERT INTO `district` VALUES ('34824', '齐河县', 'dezhou');
INSERT INTO `district` VALUES ('34825', '岚县', 'lvliang');
INSERT INTO `district` VALUES ('34826', '名山县', 'yaan');
INSERT INTO `district` VALUES ('34827', '谢家集区', 'huainan');
INSERT INTO `district` VALUES ('34828', '禅城区', 'foshan');
INSERT INTO `district` VALUES ('34829', '新兴区', 'qitaihe');
INSERT INTO `district` VALUES ('34830', '东海县', 'lianyungang');
INSERT INTO `district` VALUES ('34831', '金沙县', 'bijie');
INSERT INTO `district` VALUES ('34832', '普安县', 'qianxinan');
INSERT INTO `district` VALUES ('34833', '云安县', 'yunfu');
INSERT INTO `district` VALUES ('34834', '江陵县', 'jingzhou');
INSERT INTO `district` VALUES ('34835', '温县', 'jiaozuo');
INSERT INTO `district` VALUES ('34836', '崇文区', 'beijing_city');
INSERT INTO `district` VALUES ('34837', '夏邑县', 'shangqiu');
INSERT INTO `district` VALUES ('34838', '南开区', 'tianjin_city');
INSERT INTO `district` VALUES ('34839', '云梦县', 'xiaogan');
INSERT INTO `district` VALUES ('34840', '宁阳县', 'taian');
INSERT INTO `district` VALUES ('34841', '尼勒克县', 'yili');
INSERT INTO `district` VALUES ('34842', '桐庐县', 'hangzhou');
INSERT INTO `district` VALUES ('34843', '达县', 'dazhou');
INSERT INTO `district` VALUES ('34844', '华龙区', 'puyang');
INSERT INTO `district` VALUES ('34845', '玉龙纳西族自治县', 'lijiang');
INSERT INTO `district` VALUES ('34846', '太白县', 'baoji');
INSERT INTO `district` VALUES ('34847', '宁南县', 'liangshan');
INSERT INTO `district` VALUES ('34848', '闸北区', 'shanghai_city');
INSERT INTO `district` VALUES ('34849', '天镇县', 'datong');
INSERT INTO `district` VALUES ('34850', '莲湖区', 'xian');
INSERT INTO `district` VALUES ('34851', '汉阴县', 'ankang');
INSERT INTO `district` VALUES ('34852', '海港区', 'qinhuangdao');
INSERT INTO `district` VALUES ('34853', '芙蓉区', 'changsha');
INSERT INTO `district` VALUES ('34854', '措勤县', 'ali');
INSERT INTO `district` VALUES ('34855', '贡嘎县', 'shannan');
INSERT INTO `district` VALUES ('34856', '姚安县', 'chuxiong');
INSERT INTO `district` VALUES ('34857', '岳塘区', 'xiangtan');
INSERT INTO `district` VALUES ('34858', '铁东区', 'siping');
INSERT INTO `district` VALUES ('34859', '新化县', 'loudi');
INSERT INTO `district` VALUES ('34860', '海城区', 'beihai');
INSERT INTO `district` VALUES ('34861', '惠水县', 'qiannan');
INSERT INTO `district` VALUES ('34862', '大渡口区', 'chongqing_city');
INSERT INTO `district` VALUES ('34863', '渝北区', 'chongqing_city');
INSERT INTO `district` VALUES ('34864', '平武县', 'mianyang');
INSERT INTO `district` VALUES ('34865', '白碱滩区', 'kelamayi');
INSERT INTO `district` VALUES ('34866', '温宿县', 'akesu');
INSERT INTO `district` VALUES ('34867', '右玉县', 'shuozhou');
INSERT INTO `district` VALUES ('34868', '固镇县', 'bangbu');
INSERT INTO `district` VALUES ('34869', '平顺县', 'changzhi');
INSERT INTO `district` VALUES ('34870', '茅箭区', 'shiyan');
INSERT INTO `district` VALUES ('34871', '锡山区', 'wuxi');
INSERT INTO `district` VALUES ('34872', '丰泽区', 'quanzhou');
INSERT INTO `district` VALUES ('34873', '砀山县', 'suzhou_anhui');
INSERT INTO `district` VALUES ('34874', '戚墅堰区', 'changzhou');
INSERT INTO `district` VALUES ('34875', '康保县', 'zhangjiakou');
INSERT INTO `district` VALUES ('34876', '攸县', 'zhuzhou');
INSERT INTO `district` VALUES ('34877', '城西区', 'xining');
INSERT INTO `district` VALUES ('34878', '泰顺县', 'wenzhou');
INSERT INTO `district` VALUES ('34879', '代县', 'xinzhou');
INSERT INTO `district` VALUES ('34880', '融安县', 'liuzhou');
INSERT INTO `district` VALUES ('34881', '朔城区', 'shuozhou');
INSERT INTO `district` VALUES ('34882', '大田县', 'sanming');
INSERT INTO `district` VALUES ('34883', '龙胜各族自治县', 'guilin');
INSERT INTO `district` VALUES ('34884', '冠县', 'liaocheng');
INSERT INTO `district` VALUES ('34885', '拉孜县', 'rikaze');
INSERT INTO `district` VALUES ('34886', '同仁县', 'huangnan');
INSERT INTO `district` VALUES ('34887', '滨江区', 'hangzhou');
INSERT INTO `district` VALUES ('34888', '柯坪县', 'akesu');
INSERT INTO `district` VALUES ('34889', '礼县', 'longnan');
INSERT INTO `district` VALUES ('34890', '剑川县', 'dali');
INSERT INTO `district` VALUES ('34891', '南浔区', 'huzhou');
INSERT INTO `district` VALUES ('34892', '土默特右旗', 'baotou');
INSERT INTO `district` VALUES ('34893', '峄城区', 'zaozhuang');
INSERT INTO `district` VALUES ('34894', '义县', 'jinzhou');
INSERT INTO `district` VALUES ('34895', '普定县', 'anshun');
INSERT INTO `district` VALUES ('34897', '夏河县', 'gannan');
INSERT INTO `district` VALUES ('34898', '凉州区', 'wuwei');
INSERT INTO `district` VALUES ('34899', '洪洞县', 'linfen');
INSERT INTO `district` VALUES ('34900', '周宁县', 'ningde');
INSERT INTO `district` VALUES ('34901', '湾里区', 'nanchang');
INSERT INTO `district` VALUES ('34902', '黔江区', 'chongqing_city');
INSERT INTO `district` VALUES ('34903', '横峰县', 'shangrao');
INSERT INTO `district` VALUES ('34904', '彭泽县', 'jiujiang');
INSERT INTO `district` VALUES ('34905', '库车县', 'akesu');
INSERT INTO `district` VALUES ('34906', '弓长岭区', 'liaoyang');
INSERT INTO `district` VALUES ('34907', '阿勒泰地区', 'aletai');
INSERT INTO `district` VALUES ('34908', '边坝县', 'changdu');
INSERT INTO `district` VALUES ('34909', '凉城县', 'wulanchabu');
INSERT INTO `district` VALUES ('34910', '故城县', 'hengshui');
INSERT INTO `district` VALUES ('34911', '番禺区', 'guangzhou');
INSERT INTO `district` VALUES ('34912', '东平县', 'taian');
INSERT INTO `district` VALUES ('34913', '海东地区', 'haidong');
INSERT INTO `district` VALUES ('34914', '双流县', 'chengdu');
INSERT INTO `district` VALUES ('34915', '鄂托克旗', 'eerduosi');
INSERT INTO `district` VALUES ('34916', '新荣区', 'datong');
INSERT INTO `district` VALUES ('34917', '金湖县', 'huaian');
INSERT INTO `district` VALUES ('34918', '天心区', 'changsha');
INSERT INTO `district` VALUES ('34919', '松阳县', 'lishui');
INSERT INTO `district` VALUES ('34920', '社旗县', 'nanyang');
INSERT INTO `district` VALUES ('34921', '祥云县', 'dali');
INSERT INTO `district` VALUES ('34922', '定边县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('34923', '永宁县', 'yinchuan');
INSERT INTO `district` VALUES ('34924', '甘南县', 'qiqihaer');
INSERT INTO `district` VALUES ('34925', '湖里区', 'xiamen');
INSERT INTO `district` VALUES ('34926', '八宿县', 'changdu');
INSERT INTO `district` VALUES ('34927', '苍溪县', 'guangyuan');
INSERT INTO `district` VALUES ('34928', '伊通满族自治县', 'siping');
INSERT INTO `district` VALUES ('34929', '广南县', 'wenshan');
INSERT INTO `district` VALUES ('34930', '大厂回族自治县', 'langfang');
INSERT INTO `district` VALUES ('34931', '蒸湘区', 'hengyang');
INSERT INTO `district` VALUES ('34932', '榆社县', 'jinzhong');
INSERT INTO `district` VALUES ('34933', '荔城区', 'putian');
INSERT INTO `district` VALUES ('34934', '双牌县', 'yongzhou');
INSERT INTO `district` VALUES ('34935', '越秀区', 'guangzhou');
INSERT INTO `district` VALUES ('34936', '龙文区', 'zhangzhou');
INSERT INTO `district` VALUES ('34937', '城关区', 'lasa');
INSERT INTO `district` VALUES ('34938', '西陵区', 'yichang');
INSERT INTO `district` VALUES ('34939', '苍南县', 'wenzhou');
INSERT INTO `district` VALUES ('34940', '五河县', 'bangbu');
INSERT INTO `district` VALUES ('34941', '青川县', 'guangyuan');
INSERT INTO `district` VALUES ('34942', '柘城县', 'shangqiu');
INSERT INTO `district` VALUES ('34943', '蒙自县', 'honghe');
INSERT INTO `district` VALUES ('34944', '灵丘县', 'datong');
INSERT INTO `district` VALUES ('34946', '元宝区', 'dandong');
INSERT INTO `district` VALUES ('34947', '榆次区', 'jinzhong');
INSERT INTO `district` VALUES ('34948', '延寿县', 'haerbin');
INSERT INTO `district` VALUES ('34949', '金塔县', 'jiuquan');
INSERT INTO `district` VALUES ('34950', '香格里拉县', 'diqing');
INSERT INTO `district` VALUES ('34951', '清城区', 'qingyuan');
INSERT INTO `district` VALUES ('34952', '高港区', 'taizhou_jiangsu');
INSERT INTO `district` VALUES ('34953', '王益区', 'tongzhou');
INSERT INTO `district` VALUES ('34954', '新华区', 'pingdingshan');
INSERT INTO `district` VALUES ('34955', '钟山区', 'liupanshui');
INSERT INTO `district` VALUES ('34956', '桃山区', 'qitaihe');
INSERT INTO `district` VALUES ('34957', '沾化县', 'binzhou');
INSERT INTO `district` VALUES ('34958', '武陵源区', 'zhangjiajie');
INSERT INTO `district` VALUES ('34959', '振安区', 'dandong');
INSERT INTO `district` VALUES ('34960', '龙里县', 'qiannan');
INSERT INTO `district` VALUES ('34961', '弋江区', 'wuhu');
INSERT INTO `district` VALUES ('34962', '平潭县', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('34963', '工布江达县', 'linzhi');
INSERT INTO `district` VALUES ('34964', '明山区', 'benxi');
INSERT INTO `district` VALUES ('34965', '新乡县', 'xinxiang');
INSERT INTO `district` VALUES ('34966', '本溪满族自治县', 'benxi');
INSERT INTO `district` VALUES ('34967', '瓯海区', 'wenzhou');
INSERT INTO `district` VALUES ('34968', '横山县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('34969', '龙州县', 'chongzuo');
INSERT INTO `district` VALUES ('34970', '柳南区', 'liuzhou');
INSERT INTO `district` VALUES ('34971', '无为县', 'chaohu');
INSERT INTO `district` VALUES ('34972', '建邺区', 'nanjing');
INSERT INTO `district` VALUES ('34973', '大足县', 'chongqing_city');
INSERT INTO `district` VALUES ('34974', '金城江区', 'hechi');
INSERT INTO `district` VALUES ('34975', '龙陵县', 'baoshan');
INSERT INTO `district` VALUES ('34976', '海珠区', 'guangzhou');
INSERT INTO `district` VALUES ('34977', '贵定县', 'qiannan');
INSERT INTO `district` VALUES ('34978', '宣武区', 'beijing_city');
INSERT INTO `district` VALUES ('34979', '大埔县', 'meizhou');
INSERT INTO `district` VALUES ('34980', '宿豫区', 'suqian');
INSERT INTO `district` VALUES ('34981', '涪城区', 'mianyang');
INSERT INTO `district` VALUES ('34982', '思南县', 'tongren');
INSERT INTO `district` VALUES ('34983', '丰台区', 'beijing_city');
INSERT INTO `district` VALUES ('34984', '兴和县', 'wulanchabu');
INSERT INTO `district` VALUES ('34985', '大邑县', 'chengdu');
INSERT INTO `district` VALUES ('34986', '余庆县', 'zunyi');
INSERT INTO `district` VALUES ('34987', '郎溪县', 'xuancheng');
INSERT INTO `district` VALUES ('34988', '法库县', 'shenyang');
INSERT INTO `district` VALUES ('34989', '清浦区', 'huaian');
INSERT INTO `district` VALUES ('34990', '青田县', 'lishui');
INSERT INTO `district` VALUES ('34991', '新洲区', 'wuhan');
INSERT INTO `district` VALUES ('34992', '金东区', 'jinhua');
INSERT INTO `district` VALUES ('34993', '港南区', 'guigang');
INSERT INTO `district` VALUES ('34994', '天祝藏族自治县', 'wuwei');
INSERT INTO `district` VALUES ('34995', '长垣县', 'xinxiang');
INSERT INTO `district` VALUES ('34996', '武陵区', 'changde');
INSERT INTO `district` VALUES ('34997', '平塘县', 'qiannan');
INSERT INTO `district` VALUES ('34998', '小店区', 'taiyuan');
INSERT INTO `district` VALUES ('34999', '赤城县', 'zhangjiakou');
INSERT INTO `district` VALUES ('35000', '林口县', 'mudanjiang');
INSERT INTO `district` VALUES ('35001', '青云谱区', 'nanchang');
INSERT INTO `district` VALUES ('35002', '泰来县', 'qiqihaer');
INSERT INTO `district` VALUES ('35003', '波密县', 'linzhi');
INSERT INTO `district` VALUES ('35004', '永平县', 'dali');
INSERT INTO `district` VALUES ('35005', '竹溪县', 'shiyan');
INSERT INTO `district` VALUES ('35006', '双桥区', 'chongqing_city');
INSERT INTO `district` VALUES ('35007', '陈仓区', 'baoji');
INSERT INTO `district` VALUES ('35008', '江华瑶族自治县', 'yongzhou');
INSERT INTO `district` VALUES ('35009', '团风县', 'huanggang');
INSERT INTO `district` VALUES ('35010', '睢阳区', 'shangqiu');
INSERT INTO `district` VALUES ('35011', '珠晖区', 'hengyang');
INSERT INTO `district` VALUES ('35012', '秦淮区', 'nanjing');
INSERT INTO `district` VALUES ('35013', '平江县', 'yueyang');
INSERT INTO `district` VALUES ('35014', '南江县', 'bazhong');
INSERT INTO `district` VALUES ('35015', '殷都区', 'anyang');
INSERT INTO `district` VALUES ('35016', '南票区', 'huludao');
INSERT INTO `district` VALUES ('35017', '乌什县', 'akesu');
INSERT INTO `district` VALUES ('35019', '昌吉回族自治州', 'changji');
INSERT INTO `district` VALUES ('35020', '乌当区', 'guiyang');
INSERT INTO `district` VALUES ('35021', '高县', 'yibin');
INSERT INTO `district` VALUES ('35022', '城北区', 'xining');
INSERT INTO `district` VALUES ('35023', '巫溪县', 'chongqing_city');
INSERT INTO `district` VALUES ('35024', '翠屏区', 'yibin');
INSERT INTO `district` VALUES ('35025', '清河县', 'xingtai');
INSERT INTO `district` VALUES ('35026', '噶尔县', 'ali');
INSERT INTO `district` VALUES ('35027', '莫力达瓦达斡尔族自治旗', 'hulunbeier');
INSERT INTO `district` VALUES ('35028', '兴安区', 'hegang');
INSERT INTO `district` VALUES ('35029', '船山区', 'suining');
INSERT INTO `district` VALUES ('35030', '广阳区', 'langfang');
INSERT INTO `district` VALUES ('35031', '沿滩区', 'zigong');
INSERT INTO `district` VALUES ('35032', '扎鲁特旗', 'tongliao');
INSERT INTO `district` VALUES ('35033', '抚顺县', 'fushun');
INSERT INTO `district` VALUES ('35034', '江夏区', 'wuhan');
INSERT INTO `district` VALUES ('35035', '上蔡县', 'zhumadian');
INSERT INTO `district` VALUES ('35036', '江孜县', 'rikaze');
INSERT INTO `district` VALUES ('35037', '临西县', 'xingtai');
INSERT INTO `district` VALUES ('35038', '台江县', 'qiandongnan');
INSERT INTO `district` VALUES ('35039', '鄂伦春自治旗', 'hulunbeier');
INSERT INTO `district` VALUES ('35040', '未央区', 'xian');
INSERT INTO `district` VALUES ('35041', '连山区', 'huludao');
INSERT INTO `district` VALUES ('35042', '托克托县', 'huhehaote');
INSERT INTO `district` VALUES ('35043', '铜仁地区', 'tongren');
INSERT INTO `district` VALUES ('35044', '维西傈僳族自治县', 'diqing');
INSERT INTO `district` VALUES ('35045', '三水区', 'foshan');
INSERT INTO `district` VALUES ('35046', '富县', 'yanan');
INSERT INTO `district` VALUES ('35047', '陇县', 'baoji');
INSERT INTO `district` VALUES ('35048', '西充县', 'nanchong');
INSERT INTO `district` VALUES ('35049', '汉沽区', 'tianjin_city');
INSERT INTO `district` VALUES ('35050', '永仁县', 'chuxiong');
INSERT INTO `district` VALUES ('35051', '阳高县', 'datong');
INSERT INTO `district` VALUES ('35052', '临潭县', 'gannan');
INSERT INTO `district` VALUES ('35053', '道真仡佬族苗族自治县', 'zunyi');
INSERT INTO `district` VALUES ('35054', '青秀区', 'nanning');
INSERT INTO `district` VALUES ('35055', '连南瑶族自治县', 'qingyuan');
INSERT INTO `district` VALUES ('35056', '莱山区', 'yantai');
INSERT INTO `district` VALUES ('35057', '宜君县', 'tongzhou');
INSERT INTO `district` VALUES ('35058', '灵璧县', 'suzhou_anhui');
INSERT INTO `district` VALUES ('35059', '安乡县', 'changde');
INSERT INTO `district` VALUES ('35060', '汪清县', 'yanbian');
INSERT INTO `district` VALUES ('35061', '崇义县', 'ganzhou');
INSERT INTO `district` VALUES ('35062', '龙凤区', 'daqing');
INSERT INTO `district` VALUES ('35063', '盐湖区', 'yuncheng');
INSERT INTO `district` VALUES ('35064', '狮子山区', 'tongling');
INSERT INTO `district` VALUES ('35065', '龙川县', 'heyuan');
INSERT INTO `district` VALUES ('35066', '六合区', 'nanjing');
INSERT INTO `district` VALUES ('35068', '金山区', 'shanghai_city');
INSERT INTO `district` VALUES ('35069', '锡林郭勒盟', 'xilinguole');
INSERT INTO `district` VALUES ('35070', '唐海县', 'tangshan');
INSERT INTO `district` VALUES ('35071', '安县', 'mianyang');
INSERT INTO `district` VALUES ('35072', '山阳县', 'shangluo');
INSERT INTO `district` VALUES ('35073', '龙湾区', 'wenzhou');
INSERT INTO `district` VALUES ('35074', '舒城县', 'liuan');
INSERT INTO `district` VALUES ('35075', '曲水县', 'lasa');
INSERT INTO `district` VALUES ('35076', '蒙山县', 'wuzhou');
INSERT INTO `district` VALUES ('35077', '关岭布依族苗族自治县', 'anshun');
INSERT INTO `district` VALUES ('35078', '德江县', 'tongren');
INSERT INTO `district` VALUES ('35079', '滦平县', 'chengde');
INSERT INTO `district` VALUES ('35080', '三江侗族自治县', 'liuzhou');
INSERT INTO `district` VALUES ('35081', '泸县', 'luzhou');
INSERT INTO `district` VALUES ('35082', '靖安县', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('35083', '田阳县', 'baise');
INSERT INTO `district` VALUES ('35084', '袁州区', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('35085', '方城县', 'nanyang');
INSERT INTO `district` VALUES ('35086', '射阳县', 'yancheng');
INSERT INTO `district` VALUES ('35087', '渭源县', 'dingxi');
INSERT INTO `district` VALUES ('35088', '玉田县', 'tangshan');
INSERT INTO `district` VALUES ('35089', '黔东南苗族侗族自治州', 'qiandongnan');
INSERT INTO `district` VALUES ('35090', '高明区', 'foshan');
INSERT INTO `district` VALUES ('35091', '寒亭区', 'weifang');
INSERT INTO `district` VALUES ('35092', '甘州区', 'zhangye');
INSERT INTO `district` VALUES ('35093', '元宝山区', 'chifeng');
INSERT INTO `district` VALUES ('35094', '岳麓区', 'changsha');
INSERT INTO `district` VALUES ('35095', '石拐区', 'baotou');
INSERT INTO `district` VALUES ('35096', '扶余县', 'songyuan');
INSERT INTO `district` VALUES ('35097', '禹王台区', 'kaifeng');
INSERT INTO `district` VALUES ('35098', '定南县', 'ganzhou');
INSERT INTO `district` VALUES ('35099', '清涧县', 'yulin_shanxi_02');
INSERT INTO `district` VALUES ('35100', '长宁区', 'shanghai_city');
INSERT INTO `district` VALUES ('35101', '德城区', 'dezhou');
INSERT INTO `district` VALUES ('35102', '平坝县', 'anshun');
INSERT INTO `district` VALUES ('35103', '富顺县', 'zigong');
INSERT INTO `district` VALUES ('35104', '西岗区', 'dalian');
INSERT INTO `district` VALUES ('35105', '桃城区', 'hengshui');
INSERT INTO `district` VALUES ('35106', '站前区', 'yingkou');
INSERT INTO `district` VALUES ('35107', '宝安区', 'shenzhen');
INSERT INTO `district` VALUES ('35108', '金乡县', 'jining');
INSERT INTO `district` VALUES ('35109', '丘北县', 'wenshan');
INSERT INTO `district` VALUES ('35110', '东湖区', 'nanchang');
INSERT INTO `district` VALUES ('35111', '新野县', 'nanyang');
INSERT INTO `district` VALUES ('35112', '休宁县', 'huangshan');
INSERT INTO `district` VALUES ('35113', '山亭区', 'zaozhuang');
INSERT INTO `district` VALUES ('35114', '洪山区', 'wuhan');
INSERT INTO `district` VALUES ('35115', '凤凰县', 'xiangxi');
INSERT INTO `district` VALUES ('35116', '莎车县', 'kashi');
INSERT INTO `district` VALUES ('35117', '石城县', 'ganzhou');
INSERT INTO `district` VALUES ('35118', '大通回族土族自治县', 'xining');
INSERT INTO `district` VALUES ('35119', '获嘉县', 'xinxiang');
INSERT INTO `district` VALUES ('35120', '东坡区', 'meishan');
INSERT INTO `district` VALUES ('35121', '从江县', 'qiandongnan');
INSERT INTO `district` VALUES ('35122', '城步苗族自治县', 'shaoyang');
INSERT INTO `district` VALUES ('35123', '昌宁县', 'baoshan');
INSERT INTO `district` VALUES ('35124', '来凤县', 'enshi');
INSERT INTO `district` VALUES ('35125', '邯郸县', 'handan');
INSERT INTO `district` VALUES ('35126', '京山县', 'jingmen');
INSERT INTO `district` VALUES ('35127', '京口区', 'zhenjiang');
INSERT INTO `district` VALUES ('35128', '炎陵县', 'zhuzhou');
INSERT INTO `district` VALUES ('35129', '蓬江区', 'jiangmen');
INSERT INTO `district` VALUES ('35130', '上街区', 'zhengzhou');
INSERT INTO `district` VALUES ('35131', '浦城县', 'nanping');
INSERT INTO `district` VALUES ('35132', '孝昌县', 'xiaogan');
INSERT INTO `district` VALUES ('35133', '天峻县', 'haixi');
INSERT INTO `district` VALUES ('35134', '浪卡子县', 'shannan');
INSERT INTO `district` VALUES ('35135', '滨海新区', 'tianjin_city');
INSERT INTO `district` VALUES ('35136', '辛集市', 'shijiazhuang');
INSERT INTO `district` VALUES ('35137', '藁城市', 'shijiazhuang');
INSERT INTO `district` VALUES ('35138', '晋州市', 'shijiazhuang');
INSERT INTO `district` VALUES ('35139', '新乐市', 'shijiazhuang');
INSERT INTO `district` VALUES ('35140', '鹿泉市', 'shijiazhuang');
INSERT INTO `district` VALUES ('35141', '遵化市', 'tangshan');
INSERT INTO `district` VALUES ('35142', '迁安市', 'tangshan');
INSERT INTO `district` VALUES ('35143', '武安市', 'handan');
INSERT INTO `district` VALUES ('35144', '南宫市', 'xingtai');
INSERT INTO `district` VALUES ('35145', '沙河市', 'xingtai');
INSERT INTO `district` VALUES ('35146', '新市区', 'baoding');
INSERT INTO `district` VALUES ('35147', '北市区', 'baoding');
INSERT INTO `district` VALUES ('35148', '南市区', 'baoding');
INSERT INTO `district` VALUES ('35149', '涿州市', 'baoding');
INSERT INTO `district` VALUES ('35150', '定州市', 'baoding');
INSERT INTO `district` VALUES ('35151', '安国市', 'baoding');
INSERT INTO `district` VALUES ('35152', '高碑店市', 'baoding');
INSERT INTO `district` VALUES ('35153', '泊头市', 'cangzhou');
INSERT INTO `district` VALUES ('35154', '任丘市', 'cangzhou');
INSERT INTO `district` VALUES ('35155', '黄骅市', 'cangzhou');
INSERT INTO `district` VALUES ('35156', '河间市', 'cangzhou');
INSERT INTO `district` VALUES ('35157', '霸州市', 'langfang');
INSERT INTO `district` VALUES ('35158', '三河市', 'langfang');
INSERT INTO `district` VALUES ('35159', '冀州市', 'hengshui');
INSERT INTO `district` VALUES ('35160', '深州市', 'hengshui');
INSERT INTO `district` VALUES ('35161', '古交市', 'taiyuan');
INSERT INTO `district` VALUES ('35162', '城区', 'datong');
INSERT INTO `district` VALUES ('35163', '城区', 'yangquan');
INSERT INTO `district` VALUES ('35164', '郊区', 'yangquan');
INSERT INTO `district` VALUES ('35165', '城区', 'changzhi');
INSERT INTO `district` VALUES ('35166', '郊区', 'changzhi');
INSERT INTO `district` VALUES ('35167', '潞城市', 'changzhi');
INSERT INTO `district` VALUES ('35168', '城区', 'jincheng');
INSERT INTO `district` VALUES ('35169', '高平市', 'jincheng');
INSERT INTO `district` VALUES ('35170', '介休市', 'jinzhong');
INSERT INTO `district` VALUES ('35171', '永济市', 'yuncheng');
INSERT INTO `district` VALUES ('35172', '河津市', 'yuncheng');
INSERT INTO `district` VALUES ('35173', '原平市', 'xinzhou');
INSERT INTO `district` VALUES ('35174', '侯马市', 'linfen');
INSERT INTO `district` VALUES ('35175', '霍州市', 'linfen');
INSERT INTO `district` VALUES ('35176', '孝义市', 'lvliang');
INSERT INTO `district` VALUES ('35177', '汾阳市', 'lvliang');
INSERT INTO `district` VALUES ('35178', '霍林郭勒市', 'tongliao');
INSERT INTO `district` VALUES ('35179', '满洲里市', 'hulunbeier');
INSERT INTO `district` VALUES ('35180', '牙克石市', 'hulunbeier');
INSERT INTO `district` VALUES ('35181', '扎兰屯市', 'hulunbeier');
INSERT INTO `district` VALUES ('35182', '额尔古纳市', 'hulunbeier');
INSERT INTO `district` VALUES ('35183', '根河市', 'hulunbeier');
INSERT INTO `district` VALUES ('35184', '丰镇市', 'wulanchabu');
INSERT INTO `district` VALUES ('35185', '乌兰浩特市', 'xingan');
INSERT INTO `district` VALUES ('35186', '阿尔山市', 'xingan');
INSERT INTO `district` VALUES ('35187', '二连浩特市', 'xilinguole');
INSERT INTO `district` VALUES ('35188', '锡林浩特市', 'xilinguole');
INSERT INTO `district` VALUES ('35189', '新民市', 'shenyang');
INSERT INTO `district` VALUES ('35190', '瓦房店市', 'dalian');
INSERT INTO `district` VALUES ('35191', '普兰店市', 'dalian');
INSERT INTO `district` VALUES ('35192', '庄河市', 'dalian');
INSERT INTO `district` VALUES ('35194', '东港市', 'dandong');
INSERT INTO `district` VALUES ('35195', '凌海市', 'jinzhou');
INSERT INTO `district` VALUES ('35196', '北镇市', 'jinzhou');
INSERT INTO `district` VALUES ('35197', '鲅鱼圈区', 'yingkou');
INSERT INTO `district` VALUES ('35198', '盖州市', 'yingkou');
INSERT INTO `district` VALUES ('35199', '大石桥市', 'yingkou');
INSERT INTO `district` VALUES ('35200', '灯塔市', 'liaoyang');
INSERT INTO `district` VALUES ('35201', '调兵山市', 'tieling');
INSERT INTO `district` VALUES ('35202', '开原市', 'tieling');
INSERT INTO `district` VALUES ('35203', '北票市', 'chaoyang');
INSERT INTO `district` VALUES ('35204', '凌源市', 'chaoyang');
INSERT INTO `district` VALUES ('35205', '兴城市', 'huludao');
INSERT INTO `district` VALUES ('35206', '九台市', 'changchun');
INSERT INTO `district` VALUES ('35207', '榆树市', 'changchun');
INSERT INTO `district` VALUES ('35208', '德惠市', 'changchun');
INSERT INTO `district` VALUES ('35209', '蛟河市', 'jilin_city');
INSERT INTO `district` VALUES ('35210', '桦甸市', 'jilin_city');
INSERT INTO `district` VALUES ('35211', '舒兰市', 'jilin_city');
INSERT INTO `district` VALUES ('35212', '磐石市', 'jilin_city');
INSERT INTO `district` VALUES ('35213', '公主岭市', 'siping');
INSERT INTO `district` VALUES ('35214', '双辽市', 'siping');
INSERT INTO `district` VALUES ('35215', '梅河口市', 'tonghua');
INSERT INTO `district` VALUES ('35216', '集安市', 'tonghua');
INSERT INTO `district` VALUES ('35217', '临江市', 'baishan');
INSERT INTO `district` VALUES ('35218', '洮南市', 'baicheng');
INSERT INTO `district` VALUES ('35219', '大安市', 'baicheng');
INSERT INTO `district` VALUES ('35220', '延吉市', 'yanbian');
INSERT INTO `district` VALUES ('35221', '图们市', 'yanbian');
INSERT INTO `district` VALUES ('35222', '敦化市', 'yanbian');
INSERT INTO `district` VALUES ('35223', '珲春市', 'yanbian');
INSERT INTO `district` VALUES ('35224', '龙井市', 'yanbian');
INSERT INTO `district` VALUES ('35225', '和龙市', 'yanbian');
INSERT INTO `district` VALUES ('35226', '双城市', 'haerbin');
INSERT INTO `district` VALUES ('35227', '尚志市', 'haerbin');
INSERT INTO `district` VALUES ('35228', '五常市', 'haerbin');
INSERT INTO `district` VALUES ('35229', '梅里斯达斡尔族区', 'qiqihaer');
INSERT INTO `district` VALUES ('35230', '讷河市', 'qiqihaer');
INSERT INTO `district` VALUES ('35231', '虎林市', 'jixi');
INSERT INTO `district` VALUES ('35232', '密山市', 'jixi');
INSERT INTO `district` VALUES ('35233', '伊春区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35234', '南岔区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35235', '友好区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35236', '西林区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35237', '翠峦区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35238', '新青区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35239', '美溪区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35240', '金山屯区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35241', '五营区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35242', '乌马河区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35243', '汤旺河区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35244', '带岭区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35245', '乌伊岭区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35246', '红星区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35247', '上甘岭区', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35248', '嘉荫县', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35249', '铁力市', 'yichun_heilongjiang');
INSERT INTO `district` VALUES ('35250', '郊区', 'jiamusi');
INSERT INTO `district` VALUES ('35251', '同江市', 'jiamusi');
INSERT INTO `district` VALUES ('35252', '富锦市', 'jiamusi');
INSERT INTO `district` VALUES ('35253', '绥芬河市', 'mudanjiang');
INSERT INTO `district` VALUES ('35254', '海林市', 'mudanjiang');
INSERT INTO `district` VALUES ('35255', '宁安市', 'mudanjiang');
INSERT INTO `district` VALUES ('35256', '穆棱市', 'mudanjiang');
INSERT INTO `district` VALUES ('35257', '北安市', 'heihe');
INSERT INTO `district` VALUES ('35258', '五大连池市', 'heihe');
INSERT INTO `district` VALUES ('35259', '安达市', 'suihua');
INSERT INTO `district` VALUES ('35260', '肇东市', 'suihua');
INSERT INTO `district` VALUES ('35261', '海伦市', 'suihua');
INSERT INTO `district` VALUES ('35262', '江阴市', 'wuxi');
INSERT INTO `district` VALUES ('35263', '宜兴市', 'wuxi');
INSERT INTO `district` VALUES ('35264', '铜山区', 'xuzhou');
INSERT INTO `district` VALUES ('35265', '新沂市', 'xuzhou');
INSERT INTO `district` VALUES ('35266', '邳州市', 'xuzhou');
INSERT INTO `district` VALUES ('35267', '溧阳市', 'changzhou');
INSERT INTO `district` VALUES ('35268', '金坛市', 'changzhou');
INSERT INTO `district` VALUES ('35269', '常熟市', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('35270', '张家港市', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('35271', '昆山市', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('35272', '吴江区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('35273', '太仓市', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('35274', '启东市', 'nantong');
INSERT INTO `district` VALUES ('35275', '如皋市', 'nantong');
INSERT INTO `district` VALUES ('35276', '海门市', 'nantong');
INSERT INTO `district` VALUES ('35277', '东台市', 'yancheng');
INSERT INTO `district` VALUES ('35278', '大丰市', 'yancheng');
INSERT INTO `district` VALUES ('35279', '江都区', 'yangzhou');
INSERT INTO `district` VALUES ('35280', '仪征市', 'yangzhou');
INSERT INTO `district` VALUES ('35281', '高邮市', 'yangzhou');
INSERT INTO `district` VALUES ('35282', '丹阳市', 'zhenjiang');
INSERT INTO `district` VALUES ('35283', '扬中市', 'zhenjiang');
INSERT INTO `district` VALUES ('35284', '句容市', 'zhenjiang');
INSERT INTO `district` VALUES ('35285', '兴化市', 'taizhou_jiangsu');
INSERT INTO `district` VALUES ('35286', '靖江市', 'taizhou_jiangsu');
INSERT INTO `district` VALUES ('35287', '泰兴市', 'taizhou_jiangsu');
INSERT INTO `district` VALUES ('35288', '姜堰市', 'taizhou_jiangsu');
INSERT INTO `district` VALUES ('35289', '建德市', 'hangzhou');
INSERT INTO `district` VALUES ('35290', '富阳市', 'hangzhou');
INSERT INTO `district` VALUES ('35291', '临安市', 'hangzhou');
INSERT INTO `district` VALUES ('35292', '余姚市', 'ningbo');
INSERT INTO `district` VALUES ('35293', '慈溪市', 'ningbo');
INSERT INTO `district` VALUES ('35294', '奉化市', 'ningbo');
INSERT INTO `district` VALUES ('35295', '瑞安市', 'wenzhou');
INSERT INTO `district` VALUES ('35296', '乐清市', 'wenzhou');
INSERT INTO `district` VALUES ('35297', '海宁市', 'jiaxing');
INSERT INTO `district` VALUES ('35298', '平湖市', 'jiaxing');
INSERT INTO `district` VALUES ('35299', '桐乡市', 'jiaxing');
INSERT INTO `district` VALUES ('35300', '诸暨市', 'shaoxing');
INSERT INTO `district` VALUES ('35301', '上虞市', 'shaoxing');
INSERT INTO `district` VALUES ('35302', '嵊州市', 'shaoxing');
INSERT INTO `district` VALUES ('35303', '兰溪市', 'jinhua');
INSERT INTO `district` VALUES ('35304', '义乌市', 'jinhua');
INSERT INTO `district` VALUES ('35305', '东阳市', 'jinhua');
INSERT INTO `district` VALUES ('35306', '永康市', 'jinhua');
INSERT INTO `district` VALUES ('35307', '江山市', 'quzhou');
INSERT INTO `district` VALUES ('35308', '温岭市', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('35309', '临海市', 'taizhou_zhejiang');
INSERT INTO `district` VALUES ('35310', '龙泉市', 'lishui');
INSERT INTO `district` VALUES ('35311', '巢湖市', 'hefei');
INSERT INTO `district` VALUES ('35312', '郊区', 'tongling');
INSERT INTO `district` VALUES ('35313', '桐城市', 'anqing');
INSERT INTO `district` VALUES ('35314', '天长市', 'chuzhou');
INSERT INTO `district` VALUES ('35315', '明光市', 'chuzhou');
INSERT INTO `district` VALUES ('35316', '界首市', 'fuyang_anhui');
INSERT INTO `district` VALUES ('35317', '宁国市', 'xuancheng');
INSERT INTO `district` VALUES ('35318', '福清市', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('35319', '长乐市', 'fuzhou_fujian');
INSERT INTO `district` VALUES ('35320', '永安市', 'sanming');
INSERT INTO `district` VALUES ('35321', '金门县', 'quanzhou');
INSERT INTO `district` VALUES ('35322', '石狮市', 'quanzhou');
INSERT INTO `district` VALUES ('35323', '晋江市', 'quanzhou');
INSERT INTO `district` VALUES ('35324', '南安市', 'quanzhou');
INSERT INTO `district` VALUES ('35325', '龙海市', 'zhangzhou');
INSERT INTO `district` VALUES ('35326', '邵武市', 'nanping');
INSERT INTO `district` VALUES ('35327', '武夷山市', 'nanping');
INSERT INTO `district` VALUES ('35328', '建瓯市', 'nanping');
INSERT INTO `district` VALUES ('35329', '建阳市', 'nanping');
INSERT INTO `district` VALUES ('35330', '漳平市', 'longyan');
INSERT INTO `district` VALUES ('35331', '福安市', 'ningde');
INSERT INTO `district` VALUES ('35332', '福鼎市', 'ningde');
INSERT INTO `district` VALUES ('35333', '乐平市', 'jingdezhen');
INSERT INTO `district` VALUES ('35334', '瑞昌市', 'jiujiang');
INSERT INTO `district` VALUES ('35335', '共青城市', 'jiujiang');
INSERT INTO `district` VALUES ('35336', '贵溪市', 'yingtan');
INSERT INTO `district` VALUES ('35337', '瑞金市', 'ganzhou');
INSERT INTO `district` VALUES ('35338', '南康市', 'ganzhou');
INSERT INTO `district` VALUES ('35340', '丰城市', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('35341', '樟树市', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('35342', '高安市', 'yichun_jiangxi');
INSERT INTO `district` VALUES ('35343', '临川区', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35344', '南城县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35345', '黎川县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35346', '南丰县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35347', '崇仁县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35348', '乐安县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35349', '宜黄县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35350', '金溪县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35351', '资溪县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35352', '东乡县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35353', '广昌县', 'fuzhou_jiangxi');
INSERT INTO `district` VALUES ('35354', '德兴市', 'shangrao');
INSERT INTO `district` VALUES ('35355', '市中区', 'jinan');
INSERT INTO `district` VALUES ('35356', '章丘市', 'jinan');
INSERT INTO `district` VALUES ('35357', '市南区', 'qingdao');
INSERT INTO `district` VALUES ('35358', '市北区', 'qingdao');
INSERT INTO `district` VALUES ('35359', '胶州市', 'qingdao');
INSERT INTO `district` VALUES ('35360', '即墨市', 'qingdao');
INSERT INTO `district` VALUES ('35361', '平度市', 'qingdao');
INSERT INTO `district` VALUES ('35362', '胶南市', 'qingdao');
INSERT INTO `district` VALUES ('35363', '莱西市', 'qingdao');
INSERT INTO `district` VALUES ('35364', '市中区', 'zaozhuang');
INSERT INTO `district` VALUES ('35365', '滕州市', 'zaozhuang');
INSERT INTO `district` VALUES ('35366', '龙口市', 'yantai');
INSERT INTO `district` VALUES ('35367', '莱阳市', 'yantai');
INSERT INTO `district` VALUES ('35368', '莱州市', 'yantai');
INSERT INTO `district` VALUES ('35369', '蓬莱市', 'yantai');
INSERT INTO `district` VALUES ('35370', '招远市', 'yantai');
INSERT INTO `district` VALUES ('35371', '栖霞市', 'yantai');
INSERT INTO `district` VALUES ('35372', '海阳市', 'yantai');
INSERT INTO `district` VALUES ('35373', '青州市', 'weifang');
INSERT INTO `district` VALUES ('35374', '诸城市', 'weifang');
INSERT INTO `district` VALUES ('35375', '寿光市', 'weifang');
INSERT INTO `district` VALUES ('35376', '安丘市', 'weifang');
INSERT INTO `district` VALUES ('35377', '高密市', 'weifang');
INSERT INTO `district` VALUES ('35378', '昌邑市', 'weifang');
INSERT INTO `district` VALUES ('35379', '市中区', 'jining');
INSERT INTO `district` VALUES ('35380', '曲阜市', 'jining');
INSERT INTO `district` VALUES ('35381', '兖州市', 'jining');
INSERT INTO `district` VALUES ('35382', '邹城市', 'jining');
INSERT INTO `district` VALUES ('35383', '新泰市', 'taian');
INSERT INTO `district` VALUES ('35384', '肥城市', 'taian');
INSERT INTO `district` VALUES ('35385', '文登市', 'weihai');
INSERT INTO `district` VALUES ('35386', '荣成市', 'weihai');
INSERT INTO `district` VALUES ('35387', '乳山市', 'weihai');
INSERT INTO `district` VALUES ('35388', '乐陵市', 'dezhou');
INSERT INTO `district` VALUES ('35389', '禹城市', 'dezhou');
INSERT INTO `district` VALUES ('35390', '临清市', 'liaocheng');
INSERT INTO `district` VALUES ('35391', '巩义市', 'zhengzhou');
INSERT INTO `district` VALUES ('35392', '荥阳市', 'zhengzhou');
INSERT INTO `district` VALUES ('35393', '新密市', 'zhengzhou');
INSERT INTO `district` VALUES ('35394', '新郑市', 'zhengzhou');
INSERT INTO `district` VALUES ('35395', '登封市', 'zhengzhou');
INSERT INTO `district` VALUES ('35396', '偃师市', 'luoyang');
INSERT INTO `district` VALUES ('35397', '舞钢市', 'pingdingshan');
INSERT INTO `district` VALUES ('35398', '汝州市', 'pingdingshan');
INSERT INTO `district` VALUES ('35399', '林州市', 'anyang');
INSERT INTO `district` VALUES ('35400', '卫辉市', 'xinxiang');
INSERT INTO `district` VALUES ('35401', '辉县市', 'xinxiang');
INSERT INTO `district` VALUES ('35402', '沁阳市', 'jiaozuo');
INSERT INTO `district` VALUES ('35403', '孟州市', 'jiaozuo');
INSERT INTO `district` VALUES ('35404', '禹州市', 'xuchang');
INSERT INTO `district` VALUES ('35405', '长葛市', 'xuchang');
INSERT INTO `district` VALUES ('35406', '义马市', 'sanmenxia');
INSERT INTO `district` VALUES ('35407', '灵宝市', 'sanmenxia');
INSERT INTO `district` VALUES ('35408', '邓州市', 'nanyang');
INSERT INTO `district` VALUES ('35409', '永城市', 'shangqiu');
INSERT INTO `district` VALUES ('35410', '项城市', 'zhoukou');
INSERT INTO `district` VALUES ('35411', '大冶市', 'huangshi');
INSERT INTO `district` VALUES ('35412', '丹江口市', 'shiyan');
INSERT INTO `district` VALUES ('35413', '宜都市', 'yichang');
INSERT INTO `district` VALUES ('35414', '当阳市', 'yichang');
INSERT INTO `district` VALUES ('35415', '枝江市', 'yichang');
INSERT INTO `district` VALUES ('35416', '钟祥市', 'jingmen');
INSERT INTO `district` VALUES ('35417', '应城市', 'xiaogan');
INSERT INTO `district` VALUES ('35418', '安陆市', 'xiaogan');
INSERT INTO `district` VALUES ('35419', '汉川市', 'xiaogan');
INSERT INTO `district` VALUES ('35420', '沙市区', 'jingzhou');
INSERT INTO `district` VALUES ('35421', '石首市', 'jingzhou');
INSERT INTO `district` VALUES ('35422', '洪湖市', 'jingzhou');
INSERT INTO `district` VALUES ('35423', '松滋市', 'jingzhou');
INSERT INTO `district` VALUES ('35424', '麻城市', 'huanggang');
INSERT INTO `district` VALUES ('35425', '武穴市', 'huanggang');
INSERT INTO `district` VALUES ('35426', '赤壁市', 'xianning');
INSERT INTO `district` VALUES ('35427', '随县', 'suizhou');
INSERT INTO `district` VALUES ('35428', '广水市', 'suizhou');
INSERT INTO `district` VALUES ('35429', '恩施市', 'enshi');
INSERT INTO `district` VALUES ('35430', '利川市', 'enshi');
INSERT INTO `district` VALUES ('35431', '望城区', 'changsha');
INSERT INTO `district` VALUES ('35432', '浏阳市', 'changsha');
INSERT INTO `district` VALUES ('35433', '醴陵市', 'zhuzhou');
INSERT INTO `district` VALUES ('35434', '湘乡市', 'xiangtan');
INSERT INTO `district` VALUES ('35435', '韶山市', 'xiangtan');
INSERT INTO `district` VALUES ('35436', '耒阳市', 'hengyang');
INSERT INTO `district` VALUES ('35437', '常宁市', 'hengyang');
INSERT INTO `district` VALUES ('35438', '武冈市', 'shaoyang');
INSERT INTO `district` VALUES ('35439', '汨罗市', 'yueyang');
INSERT INTO `district` VALUES ('35440', '临湘市', 'yueyang');
INSERT INTO `district` VALUES ('35441', '津市市', 'changde');
INSERT INTO `district` VALUES ('35442', '沅江市', 'yiyang');
INSERT INTO `district` VALUES ('35443', '资兴市', 'chenzhou');
INSERT INTO `district` VALUES ('35444', '洪江市', 'huaihua');
INSERT INTO `district` VALUES ('35445', '冷水江市', 'loudi');
INSERT INTO `district` VALUES ('35446', '涟源市', 'loudi');
INSERT INTO `district` VALUES ('35447', '吉首市', 'xiangxi');
INSERT INTO `district` VALUES ('35448', '增城市', 'guangzhou');
INSERT INTO `district` VALUES ('35449', '从化市', 'guangzhou');
INSERT INTO `district` VALUES ('35450', '乐昌市', 'shaoguan');
INSERT INTO `district` VALUES ('35451', '南雄市', 'shaoguan');
INSERT INTO `district` VALUES ('35452', '台山市', 'jiangmen');
INSERT INTO `district` VALUES ('35453', '开平市', 'jiangmen');
INSERT INTO `district` VALUES ('35454', '鹤山市', 'jiangmen');
INSERT INTO `district` VALUES ('35455', '恩平市', 'jiangmen');
INSERT INTO `district` VALUES ('35456', '廉江市', 'zhanjiang');
INSERT INTO `district` VALUES ('35457', '雷州市', 'zhanjiang');
INSERT INTO `district` VALUES ('35458', '吴川市', 'zhanjiang');
INSERT INTO `district` VALUES ('35459', '高州市', 'maoming');
INSERT INTO `district` VALUES ('35460', '化州市', 'maoming');
INSERT INTO `district` VALUES ('35461', '信宜市', 'maoming');
INSERT INTO `district` VALUES ('35462', '高要市', 'zhaoqing');
INSERT INTO `district` VALUES ('35463', '四会市', 'zhaoqing');
INSERT INTO `district` VALUES ('35464', '兴宁市', 'meizhou');
INSERT INTO `district` VALUES ('35465', '城区', 'shanwei');
INSERT INTO `district` VALUES ('35466', '陆丰市', 'shanwei');
INSERT INTO `district` VALUES ('35467', '阳春市', 'yangjiang');
INSERT INTO `district` VALUES ('35468', '英德市', 'qingyuan');
INSERT INTO `district` VALUES ('35469', '连州市', 'qingyuan');
INSERT INTO `district` VALUES ('35470', '中山市', 'dongguan');
INSERT INTO `district` VALUES ('35471', '普宁市', 'jieyang');
INSERT INTO `district` VALUES ('35472', '罗定市', 'yunfu');
INSERT INTO `district` VALUES ('35473', '蝶山区', 'wuzhou');
INSERT INTO `district` VALUES ('35474', '岑溪市', 'wuzhou');
INSERT INTO `district` VALUES ('35475', '东兴市', 'fangchenggang');
INSERT INTO `district` VALUES ('35476', '桂平市', 'guigang');
INSERT INTO `district` VALUES ('35477', '北流市', 'yulin_guangxi');
INSERT INTO `district` VALUES ('35478', '宜州市', 'hechi');
INSERT INTO `district` VALUES ('35479', '合山市', 'laibin');
INSERT INTO `district` VALUES ('35480', '凭祥市', 'chongzuo');
INSERT INTO `district` VALUES ('35481', '綦江区', 'chongqing_city');
INSERT INTO `district` VALUES ('35482', '大足区', 'chongqing_city');
INSERT INTO `district` VALUES ('35483', '都江堰市', 'chengdu');
INSERT INTO `district` VALUES ('35484', '彭州市', 'chengdu');
INSERT INTO `district` VALUES ('35485', '邛崃市', 'chengdu');
INSERT INTO `district` VALUES ('35486', '崇州市', 'chengdu');
INSERT INTO `district` VALUES ('35487', '广汉市', 'deyang');
INSERT INTO `district` VALUES ('35488', '什邡市', 'deyang');
INSERT INTO `district` VALUES ('35489', '绵竹市', 'deyang');
INSERT INTO `district` VALUES ('35490', '江油市', 'mianyang');
INSERT INTO `district` VALUES ('35491', '利州区', 'guangyuan');
INSERT INTO `district` VALUES ('35492', '市中区', 'neijiang');
INSERT INTO `district` VALUES ('35493', '市中区', 'leshan');
INSERT INTO `district` VALUES ('35494', '峨眉山市', 'leshan');
INSERT INTO `district` VALUES ('35495', '阆中市', 'nanchong');
INSERT INTO `district` VALUES ('35496', '南溪区', 'yibin');
INSERT INTO `district` VALUES ('35497', '华蓥市', 'guangan');
INSERT INTO `district` VALUES ('35498', '万源市', 'dazhou');
INSERT INTO `district` VALUES ('35499', '简阳市', 'ziyang');
INSERT INTO `district` VALUES ('35500', '西昌市', 'liangshan');
INSERT INTO `district` VALUES ('35501', '清镇市', 'guiyang');
INSERT INTO `district` VALUES ('35502', '赤水市', 'zunyi');
INSERT INTO `district` VALUES ('35503', '仁怀市', 'zunyi');
INSERT INTO `district` VALUES ('35504', '七星关区', 'bijie');
INSERT INTO `district` VALUES ('35505', '碧江区', 'tongren');
INSERT INTO `district` VALUES ('35506', '万山区', 'tongren');
INSERT INTO `district` VALUES ('35507', '兴义市', 'qianxinan');
INSERT INTO `district` VALUES ('35508', '凯里市', 'qiandongnan');
INSERT INTO `district` VALUES ('35509', '都匀市', 'qiannan');
INSERT INTO `district` VALUES ('35510', '福泉市', 'qiannan');
INSERT INTO `district` VALUES ('35511', '呈贡区', 'kunming');
INSERT INTO `district` VALUES ('35512', '安宁市', 'kunming');
INSERT INTO `district` VALUES ('35513', '宣威市', 'qujing');
INSERT INTO `district` VALUES ('35514', '思茅区', 'puer');
INSERT INTO `district` VALUES ('35515', '宁洱哈尼族彝族自治县', 'puer');
INSERT INTO `district` VALUES ('35516', '墨江哈尼族自治县', 'puer');
INSERT INTO `district` VALUES ('35517', '景东彝族自治县', 'puer');
INSERT INTO `district` VALUES ('35518', '景谷傣族彝族自治县', 'puer');
INSERT INTO `district` VALUES ('35519', '镇沅彝族哈尼族拉祜族自治县', 'puer');
INSERT INTO `district` VALUES ('35520', '江城哈尼族彝族自治县', 'puer');
INSERT INTO `district` VALUES ('35521', '孟连傣族拉祜族佤族自治县', 'puer');
INSERT INTO `district` VALUES ('35522', '澜沧拉祜族自治县', 'puer');
INSERT INTO `district` VALUES ('35523', '西盟佤族自治县', 'puer');
INSERT INTO `district` VALUES ('35524', '楚雄市', 'chuxiong');
INSERT INTO `district` VALUES ('35525', '个旧市', 'honghe');
INSERT INTO `district` VALUES ('35526', '开远市', 'honghe');
INSERT INTO `district` VALUES ('35527', '蒙自市', 'honghe');
INSERT INTO `district` VALUES ('35528', '文山市', 'wenshan');
INSERT INTO `district` VALUES ('35529', '景洪市', 'xishuangbanna');
INSERT INTO `district` VALUES ('35530', '大理市', 'dali');
INSERT INTO `district` VALUES ('35531', '瑞丽市', 'dehong');
INSERT INTO `district` VALUES ('35532', '芒市', 'dehong');
INSERT INTO `district` VALUES ('35533', '日喀则市', 'rikaze');
INSERT INTO `district` VALUES ('35534', '兴平市', 'xianyang');
INSERT INTO `district` VALUES ('35535', '韩城市', 'weinan');
INSERT INTO `district` VALUES ('35536', '华阴市', 'weinan');
INSERT INTO `district` VALUES ('35537', '玉门市', 'jiuquan');
INSERT INTO `district` VALUES ('35538', '敦煌市', 'jiuquan');
INSERT INTO `district` VALUES ('35539', '临夏市', 'linxia');
INSERT INTO `district` VALUES ('35540', '合作市', 'gannan');
INSERT INTO `district` VALUES ('35541', '玉树县', 'yushu');
INSERT INTO `district` VALUES ('35542', '杂多县', 'yushu');
INSERT INTO `district` VALUES ('35543', '称多县', 'yushu');
INSERT INTO `district` VALUES ('35544', '治多县', 'yushu');
INSERT INTO `district` VALUES ('35545', '囊谦县', 'yushu');
INSERT INTO `district` VALUES ('35546', '曲麻莱县', 'yushu');
INSERT INTO `district` VALUES ('35547', '格尔木市', 'haixi');
INSERT INTO `district` VALUES ('35548', '德令哈市', 'haixi');
INSERT INTO `district` VALUES ('35549', '灵武市', 'yinchuan');
INSERT INTO `district` VALUES ('35550', '红寺堡区', 'wuzhong');
INSERT INTO `district` VALUES ('35551', '青铜峡市', 'wuzhong');
INSERT INTO `district` VALUES ('35552', '新市区', 'wulumuqi');
INSERT INTO `district` VALUES ('35553', '吐鲁番市', 'tulufan');
INSERT INTO `district` VALUES ('35554', '哈密市', 'hami');
INSERT INTO `district` VALUES ('35555', '昌吉市', 'changji');
INSERT INTO `district` VALUES ('35556', '阜康市', 'changji');
INSERT INTO `district` VALUES ('35557', '博乐市', 'boertala');
INSERT INTO `district` VALUES ('35558', '精河县', 'boertala');
INSERT INTO `district` VALUES ('35559', '温泉县', 'boertala');
INSERT INTO `district` VALUES ('35560', '库尔勒市', 'bayinguoleng');
INSERT INTO `district` VALUES ('35561', '阿克苏市', 'akesu');
INSERT INTO `district` VALUES ('35562', '阿图什市', 'kezilesukeerkezi');
INSERT INTO `district` VALUES ('35563', '阿克陶县', 'kezilesukeerkezi');
INSERT INTO `district` VALUES ('35564', '阿合奇县', 'kezilesukeerkezi');
INSERT INTO `district` VALUES ('35565', '乌恰县', 'kezilesukeerkezi');
INSERT INTO `district` VALUES ('35566', '喀什市', 'kashi');
INSERT INTO `district` VALUES ('35567', '和田市', 'hetian');
INSERT INTO `district` VALUES ('35568', '伊宁市', 'yili');
INSERT INTO `district` VALUES ('35569', '奎屯市', 'yili');
INSERT INTO `district` VALUES ('35570', '塔城市', 'tacheng');
INSERT INTO `district` VALUES ('35571', '乌苏市', 'tacheng');
INSERT INTO `district` VALUES ('35572', '阿勒泰市', 'aletai');
INSERT INTO `district` VALUES ('35573', '姑苏区', 'suzhou_jiangsu');
INSERT INTO `district` VALUES ('35574', '溧水区', 'nanjing');
INSERT INTO `district` VALUES ('35575', '曹妃甸区', 'tangshan');
INSERT INTO `district` VALUES ('35576', '名山区', 'yaan');
INSERT INTO `district` VALUES ('35577', '高淳区', 'nanjing');
INSERT INTO `district` VALUES ('35578', '襄州区', 'xiangfan');
INSERT INTO `district` VALUES ('35579', '博望区', 'maanshan');
INSERT INTO `district` VALUES ('35580', '清新区', 'qingyuan');
INSERT INTO `district` VALUES ('35581', '临桂区', 'guilin');
INSERT INTO `district` VALUES ('35582', '西市区', 'yingkou');
INSERT INTO `district` VALUES ('35583', '乐都区', 'haidong');


#渠道相关的省 市区

#省份
INSERT INTO `ota_province` VALUES ('103', '吉林', 'china', '1', '103');
INSERT INTO `ota_province` VALUES ('12', '北京', 'china', '1', '12');
INSERT INTO `ota_province` VALUES ('125', '黑龙江', 'china', '1', '125');
INSERT INTO `ota_province` VALUES ('13', '天津', 'china', '1', '13');
INSERT INTO `ota_province` VALUES ('14', '上海', 'china', '1', '14');
INSERT INTO `ota_province` VALUES ('191', '江苏', 'china', '1', '191');
INSERT INTO `ota_province` VALUES ('192', '浙江', 'china', '1', '192');
INSERT INTO `ota_province` VALUES ('193', '安徽', 'china', '1', '193');
INSERT INTO `ota_province` VALUES ('194', '福建', 'china', '1', '194');
INSERT INTO `ota_province` VALUES ('195', '江西', 'china', '1', '195');
INSERT INTO `ota_province` VALUES ('196', '山东', 'china', '1', '196');
INSERT INTO `ota_province` VALUES ('197', '河南', 'china', '1', '197');
INSERT INTO `ota_province` VALUES ('198', '湖北', 'china', '1', '198');
INSERT INTO `ota_province` VALUES ('199', '湖南', 'china', '1', '199');
INSERT INTO `ota_province` VALUES ('200', '广东', 'china', '1', '200');
INSERT INTO `ota_province` VALUES ('201', '甘肃', 'china', '1', '201');
INSERT INTO `ota_province` VALUES ('202', '四川', 'china', '1', '202');
INSERT INTO `ota_province` VALUES ('203', '贵州', 'china', '1', '203');
INSERT INTO `ota_province` VALUES ('204', '海南', 'china', '1', '204');
INSERT INTO `ota_province` VALUES ('205', '云南', 'china', '1', '205');
INSERT INTO `ota_province` VALUES ('206', '青海', 'china', '1', '206');
INSERT INTO `ota_province` VALUES ('207', '陕西', 'china', '1', '207');
INSERT INTO `ota_province` VALUES ('208', '台湾', 'china', '1', '208');
INSERT INTO `ota_province` VALUES ('209', '广西', 'china', '1', '209');
INSERT INTO `ota_province` VALUES ('210', '西藏', 'china', '1', '210');
INSERT INTO `ota_province` VALUES ('211', '宁夏', 'china', '1', '211');
INSERT INTO `ota_province` VALUES ('212', '新疆', 'china', '1', '212');
INSERT INTO `ota_province` VALUES ('213', '内蒙古', 'china', '1', '213');
INSERT INTO `ota_province` VALUES ('214', '香港', 'china', '1', '214');
INSERT INTO `ota_province` VALUES ('215', '澳门', 'china', '1', '215');
INSERT INTO `ota_province` VALUES ('26', '重庆', 'china', '1', '26');
INSERT INTO `ota_province` VALUES ('59', '河北', 'china', '1', '59');
INSERT INTO `ota_province` VALUES ('74', '山西', 'china', '1', '74');
INSERT INTO `ota_province` VALUES ('96', '辽宁', 'china', '1', '96');


#城市
INSERT INTO `ota_city` VALUES ('aba', '阿坝', '202', '1', 'aba');
INSERT INTO `ota_city` VALUES ('akesu', '阿克苏', '212', '1', 'akesu');
INSERT INTO `ota_city` VALUES ('alaer', '阿拉尔', '212', '1', 'alaer');
INSERT INTO `ota_city` VALUES ('alashan', '阿拉善盟', '213', '1', 'alashan');
INSERT INTO `ota_city` VALUES ('aletai', '阿勒泰', '212', '1', 'aletai');
INSERT INTO `ota_city` VALUES ('ali', '阿里', '210', '1', 'ali');
INSERT INTO `ota_city` VALUES ('ankang', '安康', '207', '1', 'ankang');
INSERT INTO `ota_city` VALUES ('anqing', '安庆', '193', '1', 'anqing');
INSERT INTO `ota_city` VALUES ('anshan', '鞍山', '96', '1', 'anshan');
INSERT INTO `ota_city` VALUES ('anshun', '安顺', '203', '1', 'anshun');
INSERT INTO `ota_city` VALUES ('anyang', '安阳', '197', '1', 'anyang');
INSERT INTO `ota_city` VALUES ('baicheng', '白城', '103', '1', 'baicheng');
INSERT INTO `ota_city` VALUES ('baise', '百色', '209', '1', 'baise');
INSERT INTO `ota_city` VALUES ('baisha', '白沙', '204', '1', 'baisha');
INSERT INTO `ota_city` VALUES ('baishan', '白山', '103', '1', 'baishan');
INSERT INTO `ota_city` VALUES ('baiyin', '白银', '201', '1', 'baiyin');
INSERT INTO `ota_city` VALUES ('bangbu', '蚌埠', '193', '1', 'bangbu');
INSERT INTO `ota_city` VALUES ('baoding', '保定', '59', '1', 'baoding');
INSERT INTO `ota_city` VALUES ('baoji', '宝鸡', '207', '1', 'baoji');
INSERT INTO `ota_city` VALUES ('baoshan', '保山', '205', '1', 'baoshan');
INSERT INTO `ota_city` VALUES ('baoting', '保亭', '204', '1', 'baoting');
INSERT INTO `ota_city` VALUES ('baotou', '包头', '213', '1', 'baotou');
INSERT INTO `ota_city` VALUES ('bayannaoer', '巴彦淖尔', '213', '1', 'bayannaoer');
INSERT INTO `ota_city` VALUES ('bayinguoleng', '巴音郭楞', '212', '1', 'bayinguoleng');
INSERT INTO `ota_city` VALUES ('bazhong', '巴中', '202', '1', 'bazhong');
INSERT INTO `ota_city` VALUES ('beihai', '北海', '209', '1', 'beihai');
INSERT INTO `ota_city` VALUES ('beijing_city', '北京', '12', '1', 'beijing_city');
INSERT INTO `ota_city` VALUES ('benxi', '本溪', '96', '1', 'benxi');
INSERT INTO `ota_city` VALUES ('bijie', '毕节', '203', '1', 'bijie');
INSERT INTO `ota_city` VALUES ('binzhou', '滨州', '196', '1', 'binzhou');
INSERT INTO `ota_city` VALUES ('boertala', '博尔塔拉', '212', '1', 'boertala');
INSERT INTO `ota_city` VALUES ('bozhou', '亳州', '193', '1', 'bozhou');
INSERT INTO `ota_city` VALUES ('cangzhou', '沧州', '59', '1', 'cangzhou');
INSERT INTO `ota_city` VALUES ('changchun', '长春', '103', '1', 'changchun');
INSERT INTO `ota_city` VALUES ('changde', '常德', '199', '1', 'changde');
INSERT INTO `ota_city` VALUES ('changdu', '昌都', '210', '1', 'changdu');
INSERT INTO `ota_city` VALUES ('changji', '昌吉', '212', '1', 'changji');
INSERT INTO `ota_city` VALUES ('changjiang', '昌江', '204', '1', 'changjiang');
INSERT INTO `ota_city` VALUES ('changsha', '长沙', '199', '1', 'changsha');
INSERT INTO `ota_city` VALUES ('changzhi', '长治', '74', '1', 'changzhi');
INSERT INTO `ota_city` VALUES ('changzhou', '常州', '191', '1', 'changzhou');
INSERT INTO `ota_city` VALUES ('chang_hua', '彰化', '208', '1', 'chang_hua');
INSERT INTO `ota_city` VALUES ('chaohu', '巢湖', '193', '1', 'chaohu');
INSERT INTO `ota_city` VALUES ('chaoyang', '朝阳', '96', '1', 'chaoyang');
INSERT INTO `ota_city` VALUES ('chaozhou', '潮州', '200', '1', 'chaozhou');
INSERT INTO `ota_city` VALUES ('chengde', '承德', '59', '1', 'chengde');
INSERT INTO `ota_city` VALUES ('chengdu', '成都', '202', '1', 'chengdu');
INSERT INTO `ota_city` VALUES ('chengmai', '澄迈', '204', '1', 'chengmai');
INSERT INTO `ota_city` VALUES ('chenzhou', '郴州', '199', '1', 'chenzhou');
INSERT INTO `ota_city` VALUES ('chiaohsi', '礁溪', '208', '1', 'chiaohsi');
INSERT INTO `ota_city` VALUES ('chiayi', '嘉义', '208', '1', 'chiayi');
INSERT INTO `ota_city` VALUES ('chifeng', '赤峰', '213', '1', 'chifeng');
INSERT INTO `ota_city` VALUES ('chizhou', '池州', '193', '1', 'chizhou');
INSERT INTO `ota_city` VALUES ('chongqing_city', '重庆', '26', '1', 'chongqing_city');
INSERT INTO `ota_city` VALUES ('chongzuo', '崇左', '209', '1', 'chongzuo');
INSERT INTO `ota_city` VALUES ('chuxiong', '楚雄', '205', '1', 'chuxiong');
INSERT INTO `ota_city` VALUES ('chuzhou', '滁州', '193', '1', 'chuzhou');
INSERT INTO `ota_city` VALUES ('cingjing', '清境', '208', '1', 'cingjing');
INSERT INTO `ota_city` VALUES ('dali', '大理', '205', '1', 'dali');
INSERT INTO `ota_city` VALUES ('dalian', '大连', '96', '1', 'dalian');
INSERT INTO `ota_city` VALUES ('dandong', '丹东', '96', '1', 'dandong');
INSERT INTO `ota_city` VALUES ('danzhou', '儋州', '204', '1', 'danzhou');
INSERT INTO `ota_city` VALUES ('daqing', '大庆', '125', '1', 'daqing');
INSERT INTO `ota_city` VALUES ('datong', '大同', '74', '1', 'datong');
INSERT INTO `ota_city` VALUES ('daxinganling', '大兴安岭', '125', '1', 'daxinganling');
INSERT INTO `ota_city` VALUES ('dazhou', '达州', '202', '1', 'dazhou');
INSERT INTO `ota_city` VALUES ('dehong', '德宏', '205', '1', 'dehong');
INSERT INTO `ota_city` VALUES ('deyang', '德阳', '202', '1', 'deyang');
INSERT INTO `ota_city` VALUES ('dezhou', '德州', '196', '1', 'dezhou');
INSERT INTO `ota_city` VALUES ('dingan', '定安', '204', '1', 'dingan');
INSERT INTO `ota_city` VALUES ('dingxi', '定西', '201', '1', 'dingxi');
INSERT INTO `ota_city` VALUES ('diqing', '迪庆', '205', '1', 'diqing');
INSERT INTO `ota_city` VALUES ('dongfang', '东方', '204', '1', 'dongfang');
INSERT INTO `ota_city` VALUES ('dongguan', '东莞', '200', '1', 'dongguan');
INSERT INTO `ota_city` VALUES ('dongying', '东营', '196', '1', 'dongying');
INSERT INTO `ota_city` VALUES ('eerduosi', '鄂尔多斯', '213', '1', 'eerduosi');
INSERT INTO `ota_city` VALUES ('enshi', '恩施自治州', '198', '1', 'enshi');
INSERT INTO `ota_city` VALUES ('ezhou', '鄂州', '198', '1', 'ezhou');
INSERT INTO `ota_city` VALUES ('fangchenggang', '防城港', '209', '1', 'fangchenggang');
INSERT INTO `ota_city` VALUES ('foshan', '佛山', '200', '1', 'foshan');
INSERT INTO `ota_city` VALUES ('fushun', '抚顺', '96', '1', 'fushun');
INSERT INTO `ota_city` VALUES ('fuxin', '阜新', '96', '1', 'fuxin');
INSERT INTO `ota_city` VALUES ('fuyang_anhui', '阜阳', '193', '1', 'fuyang_anhui');
INSERT INTO `ota_city` VALUES ('fuzhou_fujian', '福州', '194', '1', 'fuzhou_fujian');
INSERT INTO `ota_city` VALUES ('fuzhou_jiangxi', '抚州', '195', '1', 'fuzhou_jiangxi');
INSERT INTO `ota_city` VALUES ('gannan', '甘南', '201', '1', 'gannan');
INSERT INTO `ota_city` VALUES ('ganzhou', '赣州', '195', '1', 'ganzhou');
INSERT INTO `ota_city` VALUES ('ganzi', '甘孜', '202', '1', 'ganzi');
INSERT INTO `ota_city` VALUES ('guangan', '广安', '202', '1', 'guangan');
INSERT INTO `ota_city` VALUES ('guangyuan', '广元', '202', '1', 'guangyuan');
INSERT INTO `ota_city` VALUES ('guangzhou', '广州', '200', '1', 'guangzhou');
INSERT INTO `ota_city` VALUES ('guigang', '贵港', '209', '1', 'guigang');
INSERT INTO `ota_city` VALUES ('guilin', '桂林', '209', '1', 'guilin');
INSERT INTO `ota_city` VALUES ('guiyang', '贵阳', '203', '1', 'guiyang');
INSERT INTO `ota_city` VALUES ('guoluo', '果洛', '206', '1', 'guoluo');
INSERT INTO `ota_city` VALUES ('guyuan', '固原', '211', '1', 'guyuan');
INSERT INTO `ota_city` VALUES ('haerbin', '哈尔滨', '125', '1', 'haerbin');
INSERT INTO `ota_city` VALUES ('haibei', '海北', '206', '1', 'haibei');
INSERT INTO `ota_city` VALUES ('haidong', '海东', '206', '1', 'haidong');
INSERT INTO `ota_city` VALUES ('haikou', '海口', '204', '1', 'haikou');
INSERT INTO `ota_city` VALUES ('hainanzangzu', '海南藏族自治州', '206', '1', 'hainanzangzu');
INSERT INTO `ota_city` VALUES ('haixi', '海西', '206', '1', 'haixi');
INSERT INTO `ota_city` VALUES ('hami', '哈密', '212', '1', 'hami');
INSERT INTO `ota_city` VALUES ('handan', '邯郸', '59', '1', 'handan');
INSERT INTO `ota_city` VALUES ('hangzhou', '杭州', '192', '1', 'hangzhou');
INSERT INTO `ota_city` VALUES ('hanzhong', '汉中', '207', '1', 'hanzhong');
INSERT INTO `ota_city` VALUES ('hebi', '鹤壁', '197', '1', 'hebi');
INSERT INTO `ota_city` VALUES ('hechi', '河池', '209', '1', 'hechi');
INSERT INTO `ota_city` VALUES ('hefei', '合肥', '193', '1', 'hefei');
INSERT INTO `ota_city` VALUES ('hegang', '鹤岗', '125', '1', 'hegang');
INSERT INTO `ota_city` VALUES ('heihe', '黑河', '125', '1', 'heihe');
INSERT INTO `ota_city` VALUES ('hengshui', '衡水', '59', '1', 'hengshui');
INSERT INTO `ota_city` VALUES ('hengyang', '衡阳', '199', '1', 'hengyang');
INSERT INTO `ota_city` VALUES ('hetian', '和田', '212', '1', 'hetian');
INSERT INTO `ota_city` VALUES ('heyuan', '河源', '200', '1', 'heyuan');
INSERT INTO `ota_city` VALUES ('heze', '菏泽', '196', '1', 'heze');
INSERT INTO `ota_city` VALUES ('hezhou', '贺州', '209', '1', 'hezhou');
INSERT INTO `ota_city` VALUES ('honghe', '红河', '205', '1', 'honghe');
INSERT INTO `ota_city` VALUES ('hongkong_city', '香港', '214', '1', 'hongkong_city');
INSERT INTO `ota_city` VALUES ('huaian', '淮安', '191', '1', 'huaian');
INSERT INTO `ota_city` VALUES ('huaibei', '淮北', '193', '1', 'huaibei');
INSERT INTO `ota_city` VALUES ('huaihua', '怀化', '199', '1', 'huaihua');
INSERT INTO `ota_city` VALUES ('huainan', '淮南', '193', '1', 'huainan');
INSERT INTO `ota_city` VALUES ('hualian', '花莲县', '208', '1', 'hualian');
INSERT INTO `ota_city` VALUES ('huanggang', '黄冈', '198', '1', 'huanggang');
INSERT INTO `ota_city` VALUES ('huangnan', '黄南', '206', '1', 'huangnan');
INSERT INTO `ota_city` VALUES ('huangshan', '黄山', '193', '1', 'huangshan');
INSERT INTO `ota_city` VALUES ('huangshi', '黄石', '198', '1', 'huangshi');
INSERT INTO `ota_city` VALUES ('huhehaote', '呼和浩特', '213', '1', 'huhehaote');
INSERT INTO `ota_city` VALUES ('huizhou_guangdong', '惠州', '200', '1', 'huizhou_guangdong');
INSERT INTO `ota_city` VALUES ('huludao', '葫芦岛', '96', '1', 'huludao');
INSERT INTO `ota_city` VALUES ('hulunbeier', '呼伦贝尔', '213', '1', 'hulunbeier');
INSERT INTO `ota_city` VALUES ('huzhou', '湖州', '192', '1', 'huzhou');
INSERT INTO `ota_city` VALUES ('jhongli', '中坜', '208', '1', 'jhongli');
INSERT INTO `ota_city` VALUES ('jiamusi', '佳木斯', '125', '1', 'jiamusi');
INSERT INTO `ota_city` VALUES ('jian', '吉安', '195', '1', 'jian');
INSERT INTO `ota_city` VALUES ('jiangmen', '江门', '200', '1', 'jiangmen');
INSERT INTO `ota_city` VALUES ('jiaozuo', '焦作', '197', '1', 'jiaozuo');
INSERT INTO `ota_city` VALUES ('jiaxing', '嘉兴', '192', '1', 'jiaxing');
INSERT INTO `ota_city` VALUES ('jiayi', '嘉义县', '208', '1', 'jiayi');
INSERT INTO `ota_city` VALUES ('jiayuguan', '嘉峪关', '201', '1', 'jiayuguan');
INSERT INTO `ota_city` VALUES ('jieyang', '揭阳', '200', '1', 'jieyang');
INSERT INTO `ota_city` VALUES ('jilin_city', '吉林', '103', '1', 'jilin_city');
INSERT INTO `ota_city` VALUES ('jinan', '济南', '196', '1', 'jinan');
INSERT INTO `ota_city` VALUES ('jinchang', '金昌', '201', '1', 'jinchang');
INSERT INTO `ota_city` VALUES ('jincheng', '晋城', '74', '1', 'jincheng');
INSERT INTO `ota_city` VALUES ('jingdezhen', '景德镇', '195', '1', 'jingdezhen');
INSERT INTO `ota_city` VALUES ('jingmen', '荆门', '198', '1', 'jingmen');
INSERT INTO `ota_city` VALUES ('jingzhou', '荆州', '198', '1', 'jingzhou');
INSERT INTO `ota_city` VALUES ('jinhua', '金华', '192', '1', 'jinhua');
INSERT INTO `ota_city` VALUES ('jining', '济宁', '196', '1', 'jining');
INSERT INTO `ota_city` VALUES ('jinmen', '金门县', '208', '1', 'jinmen');
INSERT INTO `ota_city` VALUES ('jinzhong', '晋中', '74', '1', 'jinzhong');
INSERT INTO `ota_city` VALUES ('jinzhou', '锦州', '96', '1', 'jinzhou');
INSERT INTO `ota_city` VALUES ('jiujiang', '九江', '195', '1', 'jiujiang');
INSERT INTO `ota_city` VALUES ('jiuquan', '酒泉', '201', '1', 'jiuquan');
INSERT INTO `ota_city` VALUES ('jixi', '鸡西', '125', '1', 'jixi');
INSERT INTO `ota_city` VALUES ('jiyuan', '济源', '197', '1', 'jiyuan');
INSERT INTO `ota_city` VALUES ('kaifeng', '开封', '197', '1', 'kaifeng');
INSERT INTO `ota_city` VALUES ('kaohsiung', '高雄', '208', '1', 'kaohsiung');
INSERT INTO `ota_city` VALUES ('kashi', '喀什', '212', '1', 'kashi');
INSERT INTO `ota_city` VALUES ('keelung', '基隆', '208', '1', 'keelung');
INSERT INTO `ota_city` VALUES ('kelamayi', '克拉玛依', '212', '1', 'kelamayi');
INSERT INTO `ota_city` VALUES ('kezilesukeerkezi', '克孜勒苏柯尔克孜', '212', '1', 'kezilesukeerkezi');
INSERT INTO `ota_city` VALUES ('kunming', '昆明', '205', '1', 'kunming');
INSERT INTO `ota_city` VALUES ('laibin', '来宾', '209', '1', 'laibin');
INSERT INTO `ota_city` VALUES ('laiwu', '莱芜', '196', '1', 'laiwu');
INSERT INTO `ota_city` VALUES ('langfang', '廊坊', '59', '1', 'langfang');
INSERT INTO `ota_city` VALUES ('lanzhou', '兰州', '201', '1', 'lanzhou');
INSERT INTO `ota_city` VALUES ('lasa', '拉萨', '210', '1', 'lasa');
INSERT INTO `ota_city` VALUES ('ledong', '乐东', '204', '1', 'ledong');
INSERT INTO `ota_city` VALUES ('leshan', '乐山', '202', '1', 'leshan');
INSERT INTO `ota_city` VALUES ('liangshan', '凉山', '202', '1', 'liangshan');
INSERT INTO `ota_city` VALUES ('lianyungang', '连云港', '191', '1', 'lianyungang');
INSERT INTO `ota_city` VALUES ('liaocheng', '聊城', '196', '1', 'liaocheng');
INSERT INTO `ota_city` VALUES ('liaoyang', '辽阳', '96', '1', 'liaoyang');
INSERT INTO `ota_city` VALUES ('liaoyuan', '辽源', '103', '1', 'liaoyuan');
INSERT INTO `ota_city` VALUES ('lijiang', '丽江', '205', '1', 'lijiang');
INSERT INTO `ota_city` VALUES ('lincang', '临沧', '205', '1', 'lincang');
INSERT INTO `ota_city` VALUES ('linfen', '临汾', '74', '1', 'linfen');
INSERT INTO `ota_city` VALUES ('lingao', '临高', '204', '1', 'lingao');
INSERT INTO `ota_city` VALUES ('lingshui', '陵水', '204', '1', 'lingshui');
INSERT INTO `ota_city` VALUES ('linxia', '临夏', '201', '1', 'linxia');
INSERT INTO `ota_city` VALUES ('linyi', '临沂', '196', '1', 'linyi');
INSERT INTO `ota_city` VALUES ('linzhi', '林芝', '210', '1', 'linzhi');
INSERT INTO `ota_city` VALUES ('lishui', '丽水', '192', '1', 'lishui');
INSERT INTO `ota_city` VALUES ('liuan', '六安', '193', '1', 'liuan');
INSERT INTO `ota_city` VALUES ('liupanshui', '六盘水', '203', '1', 'liupanshui');
INSERT INTO `ota_city` VALUES ('liuzhou', '柳州', '209', '1', 'liuzhou');
INSERT INTO `ota_city` VALUES ('longnan', '陇南', '201', '1', 'longnan');
INSERT INTO `ota_city` VALUES ('longtan', '龙潭', '208', '1', 'longtan');
INSERT INTO `ota_city` VALUES ('longyan', '龙岩', '194', '1', 'longyan');
INSERT INTO `ota_city` VALUES ('lotung', '罗东', '208', '1', 'lotung');
INSERT INTO `ota_city` VALUES ('loudi', '娄底', '199', '1', 'loudi');
INSERT INTO `ota_city` VALUES ('luohe', '漯河', '197', '1', 'luohe');
INSERT INTO `ota_city` VALUES ('luoyang', '洛阳', '197', '1', 'luoyang');
INSERT INTO `ota_city` VALUES ('luzhou', '泸州', '202', '1', 'luzhou');
INSERT INTO `ota_city` VALUES ('lvliang', '吕梁', '74', '1', 'lvliang');
INSERT INTO `ota_city` VALUES ('maanshan', '马鞍山', '193', '1', 'maanshan');
INSERT INTO `ota_city` VALUES ('macao_city', '澳门', '215', '1', 'macao_city');
INSERT INTO `ota_city` VALUES ('maoming', '茂名', '200', '1', 'maoming');
INSERT INTO `ota_city` VALUES ('meishan', '眉山', '202', '1', 'meishan');
INSERT INTO `ota_city` VALUES ('meizhou', '梅州', '200', '1', 'meizhou');
INSERT INTO `ota_city` VALUES ('mianyang', '绵阳', '202', '1', 'mianyang');
INSERT INTO `ota_city` VALUES ('miaoli_xian', '苗栗县', '208', '1', 'miaoli_xian');
INSERT INTO `ota_city` VALUES ('mudanjiang', '牡丹江', '125', '1', 'mudanjiang');
INSERT INTO `ota_city` VALUES ('nanchang', '南昌', '195', '1', 'nanchang');
INSERT INTO `ota_city` VALUES ('nanchong', '南充', '202', '1', 'nanchong');
INSERT INTO `ota_city` VALUES ('nanjing', '南京', '191', '1', 'nanjing');
INSERT INTO `ota_city` VALUES ('nanning', '南宁', '209', '1', 'nanning');
INSERT INTO `ota_city` VALUES ('nanping', '南平', '194', '1', 'nanping');
INSERT INTO `ota_city` VALUES ('nansha', '南沙', '204', '1', 'nansha');
INSERT INTO `ota_city` VALUES ('nantong', '南通', '191', '1', 'nantong');
INSERT INTO `ota_city` VALUES ('nantou', '南投', '208', '1', 'nantou');
INSERT INTO `ota_city` VALUES ('nanyang', '南阳', '197', '1', 'nanyang');
INSERT INTO `ota_city` VALUES ('naqu', '那曲', '210', '1', 'naqu');
INSERT INTO `ota_city` VALUES ('neijiang', '内江', '202', '1', 'neijiang');
INSERT INTO `ota_city` VALUES ('ningbo', '宁波', '192', '1', 'ningbo');
INSERT INTO `ota_city` VALUES ('ningde', '宁德', '194', '1', 'ningde');
INSERT INTO `ota_city` VALUES ('nujiang', '怒江', '205', '1', 'nujiang');
INSERT INTO `ota_city` VALUES ('panjin', '盘锦', '96', '1', 'panjin');
INSERT INTO `ota_city` VALUES ('panzhihua', '攀枝花', '202', '1', 'panzhihua');
INSERT INTO `ota_city` VALUES ('penghu_xian', '澎湖县', '208', '1', 'penghu_xian');
INSERT INTO `ota_city` VALUES ('pingdingshan', '平顶山', '197', '1', 'pingdingshan');
INSERT INTO `ota_city` VALUES ('pingdong_xian', '屏东县', '208', '1', 'pingdong_xian');
INSERT INTO `ota_city` VALUES ('pingliang', '平凉', '201', '1', 'pingliang');
INSERT INTO `ota_city` VALUES ('pingxiang', '萍乡', '195', '1', 'pingxiang');
INSERT INTO `ota_city` VALUES ('puer', '普洱', '205', '1', 'puer');
INSERT INTO `ota_city` VALUES ('putian', '莆田', '194', '1', 'putian');
INSERT INTO `ota_city` VALUES ('puyang', '濮阳', '197', '1', 'puyang');
INSERT INTO `ota_city` VALUES ('qiandongnan', '黔东南', '203', '1', 'qiandongnan');
INSERT INTO `ota_city` VALUES ('qiannan', '黔南', '203', '1', 'qiannan');
INSERT INTO `ota_city` VALUES ('qianxinan', '黔西南', '203', '1', 'qianxinan');
INSERT INTO `ota_city` VALUES ('qingdao', '青岛', '196', '1', 'qingdao');
INSERT INTO `ota_city` VALUES ('qinghaihu', '青海湖', '206', '1', 'qinghaihu');
INSERT INTO `ota_city` VALUES ('qingyang', '庆阳', '201', '1', 'qingyang');
INSERT INTO `ota_city` VALUES ('qingyuan', '清远', '200', '1', 'qingyuan');
INSERT INTO `ota_city` VALUES ('qinhuangdao', '秦皇岛', '59', '1', 'qinhuangdao');
INSERT INTO `ota_city` VALUES ('qinzhou', '钦州', '209', '1', 'qinzhou');
INSERT INTO `ota_city` VALUES ('qionghai', '琼海', '204', '1', 'qionghai');
INSERT INTO `ota_city` VALUES ('qiongzhong', '琼中', '204', '1', 'qiongzhong');
INSERT INTO `ota_city` VALUES ('qiqihaer', '齐齐哈尔', '125', '1', 'qiqihaer');
INSERT INTO `ota_city` VALUES ('qitaihe', '七台河', '125', '1', 'qitaihe');
INSERT INTO `ota_city` VALUES ('quanzhou', '泉州', '194', '1', 'quanzhou');
INSERT INTO `ota_city` VALUES ('qujing', '曲靖', '205', '1', 'qujing');
INSERT INTO `ota_city` VALUES ('quzhou', '衢州', '192', '1', 'quzhou');
INSERT INTO `ota_city` VALUES ('rikaze', '日喀则', '210', '1', 'rikaze');
INSERT INTO `ota_city` VALUES ('rizhao', '日照', '196', '1', 'rizhao');
INSERT INTO `ota_city` VALUES ('sanmenxia', '三门峡', '197', '1', 'sanmenxia');
INSERT INTO `ota_city` VALUES ('sanming', '三明', '194', '1', 'sanming');
INSERT INTO `ota_city` VALUES ('sanya', '三亚', '204', '1', 'sanya');
INSERT INTO `ota_city` VALUES ('shanghai_city', '上海', '14', '1', 'shanghai_city');
INSERT INTO `ota_city` VALUES ('shangluo', '商洛', '207', '1', 'shangluo');
INSERT INTO `ota_city` VALUES ('shangqiu', '商丘', '197', '1', 'shangqiu');
INSERT INTO `ota_city` VALUES ('shangrao', '上饶', '195', '1', 'shangrao');
INSERT INTO `ota_city` VALUES ('shannan', '山南', '210', '1', 'shannan');
INSERT INTO `ota_city` VALUES ('shantou', '汕头', '200', '1', 'shantou');
INSERT INTO `ota_city` VALUES ('shanwei', '汕尾', '200', '1', 'shanwei');
INSERT INTO `ota_city` VALUES ('shaoguan', '韶关', '200', '1', 'shaoguan');
INSERT INTO `ota_city` VALUES ('shaoxing', '绍兴', '192', '1', 'shaoxing');
INSERT INTO `ota_city` VALUES ('shaoyang', '邵阳', '199', '1', 'shaoyang');
INSERT INTO `ota_city` VALUES ('shennongjia', '神农架', '198', '1', 'shennongjia');
INSERT INTO `ota_city` VALUES ('shenyang', '沈阳', '96', '1', 'shenyang');
INSERT INTO `ota_city` VALUES ('shenzhen', '深圳', '200', '1', 'shenzhen');
INSERT INTO `ota_city` VALUES ('shihezi', '石河子', '212', '1', 'shihezi');
INSERT INTO `ota_city` VALUES ('shijiazhuang', '石家庄', '59', '1', 'shijiazhuang');
INSERT INTO `ota_city` VALUES ('shiyan', '十堰', '198', '1', 'shiyan');
INSERT INTO `ota_city` VALUES ('shizuishan', '石嘴山', '211', '1', 'shizuishan');
INSERT INTO `ota_city` VALUES ('shuangyashan', '双鸭山', '125', '1', 'shuangyashan');
INSERT INTO `ota_city` VALUES ('shuozhou', '朔州', '74', '1', 'shuozhou');
INSERT INTO `ota_city` VALUES ('siping', '四平', '103', '1', 'siping');
INSERT INTO `ota_city` VALUES ('songyuan', '松原', '103', '1', 'songyuan');
INSERT INTO `ota_city` VALUES ('suihua', '绥化', '125', '1', 'suihua');
INSERT INTO `ota_city` VALUES ('suining', '遂宁', '202', '1', 'suining');
INSERT INTO `ota_city` VALUES ('suizhou', '随州', '198', '1', 'suizhou');
INSERT INTO `ota_city` VALUES ('suqian', '宿迁', '191', '1', 'suqian');
INSERT INTO `ota_city` VALUES ('suzhou_anhui', '宿州', '193', '1', 'suzhou_anhui');
INSERT INTO `ota_city` VALUES ('suzhou_jiangsu', '苏州', '191', '1', 'suzhou_jiangsu');
INSERT INTO `ota_city` VALUES ('tacheng', '塔城', '212', '1', 'tacheng');
INSERT INTO `ota_city` VALUES ('taian', '泰安', '196', '1', 'taian');
INSERT INTO `ota_city` VALUES ('taidong', '台东', '208', '1', 'taidong');
INSERT INTO `ota_city` VALUES ('tainan', '台南', '208', '1', 'tainan');
INSERT INTO `ota_city` VALUES ('tainan_xian', '台南县', '208', '1', 'tainan_xian');
INSERT INTO `ota_city` VALUES ('taipei', '台北', '208', '1', 'taipei');
INSERT INTO `ota_city` VALUES ('taipei_xian', '台北县', '208', '1', 'taipei_xian');
INSERT INTO `ota_city` VALUES ('taiyuan', '太原', '74', '1', 'taiyuan');
INSERT INTO `ota_city` VALUES ('taizhong', '台中', '208', '1', 'taizhong');
INSERT INTO `ota_city` VALUES ('taizhong_xian', '台中县', '208', '1', 'taizhong_xian');
INSERT INTO `ota_city` VALUES ('taizhou_jiangsu', '泰州', '191', '1', 'taizhou_jiangsu');
INSERT INTO `ota_city` VALUES ('taizhou_zhejiang', '台州', '192', '1', 'taizhou_zhejiang');
INSERT INTO `ota_city` VALUES ('tamsui', '淡水', '208', '1', 'tamsui');
INSERT INTO `ota_city` VALUES ('tangshan', '唐山', '59', '1', 'tangshan');
INSERT INTO `ota_city` VALUES ('taoyuan', '桃园', '208', '1', 'taoyuan');
INSERT INTO `ota_city` VALUES ('tianjin_city', '天津', '13', '1', 'tianjin_city');
INSERT INTO `ota_city` VALUES ('tianmen', '天门', '198', '1', 'tianmen');
INSERT INTO `ota_city` VALUES ('tianshui', '天水', '201', '1', 'tianshui');
INSERT INTO `ota_city` VALUES ('tieling', '铁岭', '96', '1', 'tieling');
INSERT INTO `ota_city` VALUES ('tonghua', '通化', '103', '1', 'tonghua');
INSERT INTO `ota_city` VALUES ('tongliao', '通辽', '213', '1', 'tongliao');
INSERT INTO `ota_city` VALUES ('tongling', '铜陵', '193', '1', 'tongling');
INSERT INTO `ota_city` VALUES ('tongren', '铜仁', '203', '1', 'tongren');
INSERT INTO `ota_city` VALUES ('tongzhou', '铜川', '207', '1', 'tongzhou');
INSERT INTO `ota_city` VALUES ('tulufan', '吐鲁番', '212', '1', 'tulufan');
INSERT INTO `ota_city` VALUES ('tumushuke', '图木舒克', '212', '1', 'tumushuke');
INSERT INTO `ota_city` VALUES ('tunchang', '屯昌', '204', '1', 'tunchang');
INSERT INTO `ota_city` VALUES ('wanning', '万宁', '204', '1', 'wanning');
INSERT INTO `ota_city` VALUES ('weifang', '潍坊', '196', '1', 'weifang');
INSERT INTO `ota_city` VALUES ('weihai', '威海', '196', '1', 'weihai');
INSERT INTO `ota_city` VALUES ('weinan', '渭南', '207', '1', 'weinan');
INSERT INTO `ota_city` VALUES ('wenchang', '文昌', '204', '1', 'wenchang');
INSERT INTO `ota_city` VALUES ('wenshan', '文山', '205', '1', 'wenshan');
INSERT INTO `ota_city` VALUES ('wenzhou', '温州', '192', '1', 'wenzhou');
INSERT INTO `ota_city` VALUES ('wuhai', '乌海', '213', '1', 'wuhai');
INSERT INTO `ota_city` VALUES ('wuhan', '武汉', '198', '1', 'wuhan');
INSERT INTO `ota_city` VALUES ('wuhu', '芜湖', '193', '1', 'wuhu');
INSERT INTO `ota_city` VALUES ('wujiaqu', '五家渠', '212', '1', 'wujiaqu');
INSERT INTO `ota_city` VALUES ('wulanchabu', '乌兰察布', '213', '1', 'wulanchabu');
INSERT INTO `ota_city` VALUES ('wulumuqi', '乌鲁木齐', '212', '1', 'wulumuqi');
INSERT INTO `ota_city` VALUES ('wuwei', '武威', '201', '1', 'wuwei');
INSERT INTO `ota_city` VALUES ('wuxi', '无锡', '191', '1', 'wuxi');
INSERT INTO `ota_city` VALUES ('wuzhishan', '五指山', '204', '1', 'wuzhishan');
INSERT INTO `ota_city` VALUES ('wuzhong', '吴忠', '211', '1', 'wuzhong');
INSERT INTO `ota_city` VALUES ('wuzhou', '梧州', '209', '1', 'wuzhou');
INSERT INTO `ota_city` VALUES ('xiamen', '厦门', '194', '1', 'xiamen');
INSERT INTO `ota_city` VALUES ('xian', '西安', '207', '1', 'xian');
INSERT INTO `ota_city` VALUES ('xiangfan', '襄樊', '198', '1', 'xiangfan');
INSERT INTO `ota_city` VALUES ('xiangtan', '湘潭', '199', '1', 'xiangtan');
INSERT INTO `ota_city` VALUES ('xiangxi', '湘西', '199', '1', 'xiangxi');
INSERT INTO `ota_city` VALUES ('xianning', '咸宁', '198', '1', 'xianning');
INSERT INTO `ota_city` VALUES ('xiantao', '仙桃', '198', '1', 'xiantao');
INSERT INTO `ota_city` VALUES ('xianyang', '咸阳', '207', '1', 'xianyang');
INSERT INTO `ota_city` VALUES ('xiaogan', '孝感', '198', '1', 'xiaogan');
INSERT INTO `ota_city` VALUES ('xilinguole', '锡林郭勒盟', '213', '1', 'xilinguole');
INSERT INTO `ota_city` VALUES ('xingan', '兴安盟', '213', '1', 'xingan');
INSERT INTO `ota_city` VALUES ('xingtai', '邢台', '59', '1', 'xingtai');
INSERT INTO `ota_city` VALUES ('xining', '西宁', '206', '1', 'xining');
INSERT INTO `ota_city` VALUES ('xinxiang', '新乡', '197', '1', 'xinxiang');
INSERT INTO `ota_city` VALUES ('xinyang', '信阳', '197', '1', 'xinyang');
INSERT INTO `ota_city` VALUES ('xinyu', '新余', '195', '1', 'xinyu');
INSERT INTO `ota_city` VALUES ('xinzhou', '忻州', '74', '1', 'xinzhou');
INSERT INTO `ota_city` VALUES ('xinzhu', '新竹', '208', '1', 'xinzhu');
INSERT INTO `ota_city` VALUES ('xisha', '西沙', '204', '1', 'xisha');
INSERT INTO `ota_city` VALUES ('xishuangbanna', '西双版纳', '205', '1', 'xishuangbanna');
INSERT INTO `ota_city` VALUES ('xuancheng', '宣城', '193', '1', 'xuancheng');
INSERT INTO `ota_city` VALUES ('xuchang', '许昌', '197', '1', 'xuchang');
INSERT INTO `ota_city` VALUES ('xuzhou', '徐州', '191', '1', 'xuzhou');
INSERT INTO `ota_city` VALUES ('yaan', '雅安', '202', '1', 'yaan');
INSERT INTO `ota_city` VALUES ('yanan', '延安', '207', '1', 'yanan');
INSERT INTO `ota_city` VALUES ('yanbian', '延边', '103', '1', 'yanbian');
INSERT INTO `ota_city` VALUES ('yancheng', '盐城', '191', '1', 'yancheng');
INSERT INTO `ota_city` VALUES ('yangjiang', '阳江', '200', '1', 'yangjiang');
INSERT INTO `ota_city` VALUES ('yangquan', '阳泉', '74', '1', 'yangquan');
INSERT INTO `ota_city` VALUES ('yangzhou', '扬州', '191', '1', 'yangzhou');
INSERT INTO `ota_city` VALUES ('yantai', '烟台', '196', '1', 'yantai');
INSERT INTO `ota_city` VALUES ('yibin', '宜宾', '202', '1', 'yibin');
INSERT INTO `ota_city` VALUES ('yichang', '宜昌', '198', '1', 'yichang');
INSERT INTO `ota_city` VALUES ('yichun_heilongjiang', '伊春', '125', '1', 'yichun_heilongjiang');
INSERT INTO `ota_city` VALUES ('yichun_jiangxi', '宜春', '195', '1', 'yichun_jiangxi');
INSERT INTO `ota_city` VALUES ('yilan_taiwan', '宜兰县', '208', '1', 'yilan_taiwan');
INSERT INTO `ota_city` VALUES ('yili', '伊犁', '212', '1', 'yili');
INSERT INTO `ota_city` VALUES ('yinchuan', '银川', '211', '1', 'yinchuan');
INSERT INTO `ota_city` VALUES ('yingkou', '营口', '96', '1', 'yingkou');
INSERT INTO `ota_city` VALUES ('yingtan', '鹰潭', '195', '1', 'yingtan');
INSERT INTO `ota_city` VALUES ('yiyang', '益阳', '199', '1', 'yiyang');
INSERT INTO `ota_city` VALUES ('yongzhou', '永州', '199', '1', 'yongzhou');
INSERT INTO `ota_city` VALUES ('yueyang', '岳阳', '199', '1', 'yueyang');
INSERT INTO `ota_city` VALUES ('yulin_guangxi', '玉林', '209', '1', 'yulin_guangxi');
INSERT INTO `ota_city` VALUES ('yulin_shanxi_02', '榆林', '207', '1', 'yulin_shanxi_02');
INSERT INTO `ota_city` VALUES ('yuncheng', '运城', '74', '1', 'yuncheng');
INSERT INTO `ota_city` VALUES ('yunfu', '云浮', '200', '1', 'yunfu');
INSERT INTO `ota_city` VALUES ('yunlin', '云林', '208', '1', 'yunlin');
INSERT INTO `ota_city` VALUES ('yushu', '玉树', '206', '1', 'yushu');
INSERT INTO `ota_city` VALUES ('yuxi', '玉溪', '205', '1', 'yuxi');
INSERT INTO `ota_city` VALUES ('zaozhuang', '枣庄', '196', '1', 'zaozhuang');
INSERT INTO `ota_city` VALUES ('zhangjiajie', '张家界', '199', '1', 'zhangjiajie');
INSERT INTO `ota_city` VALUES ('zhangjiakou', '张家口', '59', '1', 'zhangjiakou');
INSERT INTO `ota_city` VALUES ('zhangye', '张掖', '201', '1', 'zhangye');
INSERT INTO `ota_city` VALUES ('zhangzhou', '漳州', '194', '1', 'zhangzhou');
INSERT INTO `ota_city` VALUES ('zhanjiang', '湛江', '200', '1', 'zhanjiang');
INSERT INTO `ota_city` VALUES ('zhaoqing', '肇庆', '200', '1', 'zhaoqing');
INSERT INTO `ota_city` VALUES ('zhaotong', '昭通', '205', '1', 'zhaotong');
INSERT INTO `ota_city` VALUES ('zhengzhou', '郑州', '197', '1', 'zhengzhou');
INSERT INTO `ota_city` VALUES ('zhenjiang', '镇江', '191', '1', 'zhenjiang');
INSERT INTO `ota_city` VALUES ('zhongsha', '中沙', '204', '1', 'zhongsha');
INSERT INTO `ota_city` VALUES ('zhongshan', '中山', '200', '1', 'zhongshan');
INSERT INTO `ota_city` VALUES ('zhongwei', '中卫', '211', '1', 'zhongwei');
INSERT INTO `ota_city` VALUES ('zhoukou', '周口', '197', '1', 'zhoukou');
INSERT INTO `ota_city` VALUES ('zhoushan', '舟山', '192', '1', 'zhoushan');
INSERT INTO `ota_city` VALUES ('zhuhai', '珠海', '200', '1', 'zhuhai');
INSERT INTO `ota_city` VALUES ('zhumadian', '驻马店', '197', '1', 'zhumadian');
INSERT INTO `ota_city` VALUES ('zhuzhou', '株洲', '199', '1', 'zhuzhou');
INSERT INTO `ota_city` VALUES ('zibo', '淄博', '196', '1', 'zibo');
INSERT INTO `ota_city` VALUES ('zigong', '自贡', '202', '1', 'zigong');
INSERT INTO `ota_city` VALUES ('ziyang', '资阳', '202', '1', 'ziyang');
INSERT INTO `ota_city` VALUES ('zunyi', '遵义', '203', '1', 'zunyi');




#区域

INSERT INTO `ota_district` VALUES ('1148201', '文殊', 'jiayuguan', '1', '1148201');
INSERT INTO `ota_district` VALUES ('11482010', '新华', 'jiayuguan', '1', '11482010');
INSERT INTO `ota_district` VALUES ('1148202', '峪泉', 'jiayuguan', '1', '1148202');
INSERT INTO `ota_district` VALUES ('1148203', '新城', 'jiayuguan', '1', '1148203');
INSERT INTO `ota_district` VALUES ('1148204', '朝阳', 'jiayuguan', '1', '1148204');
INSERT INTO `ota_district` VALUES ('1148205', '峪苑', 'jiayuguan', '1', '1148205');
INSERT INTO `ota_district` VALUES ('1148206', '前进', 'jiayuguan', '1', '1148206');
INSERT INTO `ota_district` VALUES ('1148207', '建设', 'jiayuguan', '1', '1148207');
INSERT INTO `ota_district` VALUES ('1148208', '五一', 'jiayuguan', '1', '1148208');
INSERT INTO `ota_district` VALUES ('1148209', '胜利', 'jiayuguan', '1', '1148209');
INSERT INTO `ota_district` VALUES ('115850', '河西区', 'sanya', '1', '115850');
INSERT INTO `ota_district` VALUES ('115851', '河东区', 'sanya', '1', '115851');
INSERT INTO `ota_district` VALUES ('115852', '海棠湾', 'sanya', '1', '115852');
INSERT INTO `ota_district` VALUES ('115853', '三亚湾', 'sanya', '1', '115853');
INSERT INTO `ota_district` VALUES ('115854', '亚龙湾', 'sanya', '1', '115854');
INSERT INTO `ota_district` VALUES ('115855', '大东海', 'sanya', '1', '115855');
INSERT INTO `ota_district` VALUES ('115856', '凤凰岛', 'sanya', '1', '115856');
INSERT INTO `ota_district` VALUES ('115857', '田独', 'sanya', '1', '115857');
INSERT INTO `ota_district` VALUES ('115858', '崖城', 'sanya', '1', '115858');
INSERT INTO `ota_district` VALUES ('115859', '天涯', 'sanya', '1', '115859');
INSERT INTO `ota_district` VALUES ('115860', '育才', 'sanya', '1', '115860');
INSERT INTO `ota_district` VALUES ('3136201', '石岐区', 'zhongshan', '1', '3136201');
INSERT INTO `ota_district` VALUES ('3136202', '西区', 'zhongshan', '1', '3136202');
INSERT INTO `ota_district` VALUES ('3136203', '东区', 'zhongshan', '1', '3136203');
INSERT INTO `ota_district` VALUES ('3136204', '南区', 'zhongshan', '1', '3136204');
INSERT INTO `ota_district` VALUES ('3136205', '火炬区', 'zhongshan', '1', '3136205');
INSERT INTO `ota_district` VALUES ('3136206', '黄圃', 'zhongshan', '1', '3136206');
INSERT INTO `ota_district` VALUES ('3136207', '阜沙', 'zhongshan', '1', '3136207');
INSERT INTO `ota_district` VALUES ('3136208', '三角', 'zhongshan', '1', '3136208');
INSERT INTO `ota_district` VALUES ('3136209', '民众', 'zhongshan', '1', '3136209');
INSERT INTO `ota_district` VALUES ('3136210', '南朗', 'zhongshan', '1', '3136210');
INSERT INTO `ota_district` VALUES ('3136211', '五桂山', 'zhongshan', '1', '3136211');
INSERT INTO `ota_district` VALUES ('3136212', '板芙', 'zhongshan', '1', '3136212');
INSERT INTO `ota_district` VALUES ('3136213', '神湾', 'zhongshan', '1', '3136213');
INSERT INTO `ota_district` VALUES ('3136214', '小榄', 'zhongshan', '1', '3136214');
INSERT INTO `ota_district` VALUES ('3136215', '东升', 'zhongshan', '1', '3136215');
INSERT INTO `ota_district` VALUES ('3136216', '东凤', 'zhongshan', '1', '3136216');
INSERT INTO `ota_district` VALUES ('3136217', '南头', 'zhongshan', '1', '3136217');
INSERT INTO `ota_district` VALUES ('3136218', '横栏', 'zhongshan', '1', '3136218');
INSERT INTO `ota_district` VALUES ('3136219', '大涌', 'zhongshan', '1', '3136219');
INSERT INTO `ota_district` VALUES ('3136220', '沙溪', 'zhongshan', '1', '3136220');
INSERT INTO `ota_district` VALUES ('3136221', '三乡', 'zhongshan', '1', '3136221');
INSERT INTO `ota_district` VALUES ('3136222', '港口', 'zhongshan', '1', '3136222');
INSERT INTO `ota_district` VALUES ('3136301', '莞城区', 'dongguan', '1', '3136301');
INSERT INTO `ota_district` VALUES ('3136302', '东城区', 'dongguan', '1', '3136302');
INSERT INTO `ota_district` VALUES ('3136303', '南城区', 'dongguan', '1', '3136303');
INSERT INTO `ota_district` VALUES ('3136304', '万江区', 'dongguan', '1', '3136304');
INSERT INTO `ota_district` VALUES ('3136305', '虎门', 'dongguan', '1', '3136305');
INSERT INTO `ota_district` VALUES ('3136306', '厚街', 'dongguan', '1', '3136306');
INSERT INTO `ota_district` VALUES ('3136307', '樟木头', 'dongguan', '1', '3136307');
INSERT INTO `ota_district` VALUES ('3136308', '常平', 'dongguan', '1', '3136308');
INSERT INTO `ota_district` VALUES ('3136309', '长安', 'dongguan', '1', '3136309');
INSERT INTO `ota_district` VALUES ('3136310', '石碣', 'dongguan', '1', '3136310');
INSERT INTO `ota_district` VALUES ('3136311', '石龙', 'dongguan', '1', '3136311');
INSERT INTO `ota_district` VALUES ('3136312', '凤岗', 'dongguan', '1', '3136312');
INSERT INTO `ota_district` VALUES ('3136313', '黄江', 'dongguan', '1', '3136313');
INSERT INTO `ota_district` VALUES ('3136314', '塘厦', 'dongguan', '1', '3136314');
INSERT INTO `ota_district` VALUES ('3136315', '清溪', 'dongguan', '1', '3136315');
INSERT INTO `ota_district` VALUES ('3136316', '茶山', 'dongguan', '1', '3136316');
INSERT INTO `ota_district` VALUES ('3136317', '石排', 'dongguan', '1', '3136317');
INSERT INTO `ota_district` VALUES ('3136318', '企石', 'dongguan', '1', '3136318');
INSERT INTO `ota_district` VALUES ('3136319', '谢岗', 'dongguan', '1', '3136319');
INSERT INTO `ota_district` VALUES ('3136320', '大朗', 'dongguan', '1', '3136320');
INSERT INTO `ota_district` VALUES ('3136321', '寮步', 'dongguan', '1', '3136321');
INSERT INTO `ota_district` VALUES ('3136322', '东坑', 'dongguan', '1', '3136322');
INSERT INTO `ota_district` VALUES ('3136323', '横沥', 'dongguan', '1', '3136323');
INSERT INTO `ota_district` VALUES ('3136324', '大岭山', 'dongguan', '1', '3136324');
INSERT INTO `ota_district` VALUES ('3136325', '沙田', 'dongguan', '1', '3136325');
INSERT INTO `ota_district` VALUES ('3136326', '道滘', 'dongguan', '1', '3136326');
INSERT INTO `ota_district` VALUES ('3136327', '高埗', 'dongguan', '1', '3136327');
INSERT INTO `ota_district` VALUES ('3136328', '望牛墩', 'dongguan', '1', '3136328');
INSERT INTO `ota_district` VALUES ('3136329', '中堂', 'dongguan', '1', '3136329');
INSERT INTO `ota_district` VALUES ('3136330', '麻涌', 'dongguan', '1', '3136330');
INSERT INTO `ota_district` VALUES ('3136331', '洪梅', 'dongguan', '1', '3136331');
INSERT INTO `ota_district` VALUES ('3136332', '松山湖', 'dongguan', '1', '3136332');
INSERT INTO `ota_district` VALUES ('32672', '井研县', 'leshan', '1', '32672');
INSERT INTO `ota_district` VALUES ('32673', '驿城区', 'zhumadian', '1', '32673');
INSERT INTO `ota_district` VALUES ('32674', '兴宾区', 'laibin', '1', '32674');
INSERT INTO `ota_district` VALUES ('32675', '洛龙区', 'luoyang', '1', '32675');
INSERT INTO `ota_district` VALUES ('32676', '易门县', 'yuxi', '1', '32676');
INSERT INTO `ota_district` VALUES ('32677', '曲松县', 'shannan', '1', '32677');
INSERT INTO `ota_district` VALUES ('32678', '钦南区', 'qinzhou', '1', '32678');
INSERT INTO `ota_district` VALUES ('32679', '富宁县', 'wenshan', '1', '32679');
INSERT INTO `ota_district` VALUES ('32680', '四子王旗', 'wulanchabu', '1', '32680');
INSERT INTO `ota_district` VALUES ('32681', '江南区', 'nanning', '1', '32681');
INSERT INTO `ota_district` VALUES ('32682', '红塔区', 'yuxi', '1', '32682');
INSERT INTO `ota_district` VALUES ('32683', '城阳区', 'qingdao', '1', '32683');
INSERT INTO `ota_district` VALUES ('32684', '花溪区', 'guiyang', '1', '32684');
INSERT INTO `ota_district` VALUES ('32685', '绥棱县', 'suihua', '1', '32685');
INSERT INTO `ota_district` VALUES ('32686', '沽源县', 'zhangjiakou', '1', '32686');
INSERT INTO `ota_district` VALUES ('32687', '香坊区', 'haerbin', '1', '32687');
INSERT INTO `ota_district` VALUES ('32688', '张店区', 'zibo', '1', '32688');
INSERT INTO `ota_district` VALUES ('32689', '黑水县', 'aba', '1', '32689');
INSERT INTO `ota_district` VALUES ('32690', '汉阳区', 'wuhan', '1', '32690');
INSERT INTO `ota_district` VALUES ('32691', '广平县', 'handan', '1', '32691');
INSERT INTO `ota_district` VALUES ('32692', '依兰县', 'haerbin', '1', '32692');
INSERT INTO `ota_district` VALUES ('32693', '南皮县', 'cangzhou', '1', '32693');
INSERT INTO `ota_district` VALUES ('32694', '施秉县', 'qiandongnan', '1', '32694');
INSERT INTO `ota_district` VALUES ('32695', '良庆区', 'nanning', '1', '32695');
INSERT INTO `ota_district` VALUES ('32696', '襄阳区', 'xiangfan', '1', '32696');
INSERT INTO `ota_district` VALUES ('32697', '万盛区', 'chongqing_city', '1', '32697');
INSERT INTO `ota_district` VALUES ('32698', '新县', 'xinyang', '1', '32698');
INSERT INTO `ota_district` VALUES ('32699', '芦淞区', 'zhuzhou', '1', '32699');
INSERT INTO `ota_district` VALUES ('32700', '磁县', 'handan', '1', '32700');
INSERT INTO `ota_district` VALUES ('32701', '澄迈县', 'chengmai', '1', '32701');
INSERT INTO `ota_district` VALUES ('32702', '建水县', 'honghe', '1', '32702');
INSERT INTO `ota_district` VALUES ('32703', '玉州区', 'yulin_guangxi', '1', '32703');
INSERT INTO `ota_district` VALUES ('32704', '黄南藏族自治州', 'huangnan', '1', '32704');
INSERT INTO `ota_district` VALUES ('32705', '金阊区', 'suzhou_jiangsu', '1', '32705');
INSERT INTO `ota_district` VALUES ('32706', '略阳县', 'hanzhong', '1', '32706');
INSERT INTO `ota_district` VALUES ('32707', '覃塘区', 'guigang', '1', '32707');
INSERT INTO `ota_district` VALUES ('32708', '无极县', 'shijiazhuang', '1', '32708');
INSERT INTO `ota_district` VALUES ('32709', '新华区', 'cangzhou', '1', '32709');
INSERT INTO `ota_district` VALUES ('32710', '大化瑶族自治县', 'hechi', '1', '32710');
INSERT INTO `ota_district` VALUES ('32711', '革吉县', 'ali', '1', '32711');
INSERT INTO `ota_district` VALUES ('32712', '青羊区', 'chengdu', '1', '32712');
INSERT INTO `ota_district` VALUES ('32713', '石柱土家族自治县', 'chongqing_city', '1', '32713');
INSERT INTO `ota_district` VALUES ('32714', '海安县', 'nantong', '1', '32714');
INSERT INTO `ota_district` VALUES ('32715', '逊克县', 'heihe', '1', '32715');
INSERT INTO `ota_district` VALUES ('32716', '河口区', 'dongying', '1', '32716');
INSERT INTO `ota_district` VALUES ('32717', '互助土族自治县', 'haidong', '1', '32717');
INSERT INTO `ota_district` VALUES ('32718', '闽清县', 'fuzhou_fujian', '1', '32718');
INSERT INTO `ota_district` VALUES ('32719', '沂源县', 'zibo', '1', '32719');
INSERT INTO `ota_district` VALUES ('32720', '永泰县', 'fuzhou_fujian', '1', '32720');
INSERT INTO `ota_district` VALUES ('32721', '仙居县', 'taizhou_zhejiang', '1', '32721');
INSERT INTO `ota_district` VALUES ('32722', '泰山区', 'taian', '1', '32722');
INSERT INTO `ota_district` VALUES ('32723', '堆龙德庆县', 'lasa', '1', '32723');
INSERT INTO `ota_district` VALUES ('32724', '余江县', 'yingtan', '1', '32724');
INSERT INTO `ota_district` VALUES ('32725', '牟定县', 'chuxiong', '1', '32725');
INSERT INTO `ota_district` VALUES ('32726', '霞山区', 'zhanjiang', '1', '32726');
INSERT INTO `ota_district` VALUES ('32727', '山南地区', 'shannan', '1', '32727');
INSERT INTO `ota_district` VALUES ('32728', '毕节地区', 'bijie', '1', '32728');
INSERT INTO `ota_district` VALUES ('32729', '绥江县', 'zhaotong', '1', '32729');
INSERT INTO `ota_district` VALUES ('32730', '沈河区', 'shenyang', '1', '32730');
INSERT INTO `ota_district` VALUES ('32731', '肇州县', 'daqing', '1', '32731');
INSERT INTO `ota_district` VALUES ('32732', '特克斯县', 'yili', '1', '32732');
INSERT INTO `ota_district` VALUES ('32733', '武定县', 'chuxiong', '1', '32733');
INSERT INTO `ota_district` VALUES ('32734', '阿拉善右旗', 'alashan', '1', '32734');
INSERT INTO `ota_district` VALUES ('32735', '瓮安县', 'qiannan', '1', '32735');
INSERT INTO `ota_district` VALUES ('32736', '寻甸回族彝族自治县', 'kunming', '1', '32736');
INSERT INTO `ota_district` VALUES ('32737', '栾城县', 'shijiazhuang', '1', '32737');
INSERT INTO `ota_district` VALUES ('32738', '丹巴县', 'ganzi', '1', '32738');
INSERT INTO `ota_district` VALUES ('32739', '芝罘区', 'yantai', '1', '32739');
INSERT INTO `ota_district` VALUES ('32740', '怀远县', 'bangbu', '1', '32740');
INSERT INTO `ota_district` VALUES ('32741', '固始县', 'xinyang', '1', '32741');
INSERT INTO `ota_district` VALUES ('32742', '道县', 'yongzhou', '1', '32742');
INSERT INTO `ota_district` VALUES ('32743', '巴里坤哈萨克自治县', 'hami', '1', '32743');
INSERT INTO `ota_district` VALUES ('32744', '洪雅县', 'meishan', '1', '32744');
INSERT INTO `ota_district` VALUES ('32745', '富裕县', 'qiqihaer', '1', '32745');
INSERT INTO `ota_district` VALUES ('32746', '宁津县', 'dezhou', '1', '32746');
INSERT INTO `ota_district` VALUES ('32747', '青河县', 'aletai', '1', '32747');
INSERT INTO `ota_district` VALUES ('32748', '奉贤区', 'shanghai_city', '1', '32748');
INSERT INTO `ota_district` VALUES ('32749', '鹤山区', 'hebi', '1', '32749');
INSERT INTO `ota_district` VALUES ('32750', '秭归县', 'yichang', '1', '32750');
INSERT INTO `ota_district` VALUES ('32751', '霍山县', 'liuan', '1', '32751');
INSERT INTO `ota_district` VALUES ('32752', '抚宁县', 'qinhuangdao', '1', '32752');
INSERT INTO `ota_district` VALUES ('32753', '望城县', 'changsha', '1', '32753');
INSERT INTO `ota_district` VALUES ('32754', '永顺县', 'xiangxi', '1', '32754');
INSERT INTO `ota_district` VALUES ('32755', '浮山县', 'linfen', '1', '32755');
INSERT INTO `ota_district` VALUES ('32756', '东河区', 'baotou', '1', '32756');
INSERT INTO `ota_district` VALUES ('32757', '睢县', 'shangqiu', '1', '32757');
INSERT INTO `ota_district` VALUES ('32758', '濉溪县', 'huaibei', '1', '32758');
INSERT INTO `ota_district` VALUES ('32759', '平利县', 'ankang', '1', '32759');
INSERT INTO `ota_district` VALUES ('32760', '城口县', 'chongqing_city', '1', '32760');
INSERT INTO `ota_district` VALUES ('32761', '陵川县', 'jincheng', '1', '32761');
INSERT INTO `ota_district` VALUES ('32762', '湖滨区', 'sanmenxia', '1', '32762');
INSERT INTO `ota_district` VALUES ('32763', '连山壮族瑶族自治县', 'qingyuan', '1', '32763');
INSERT INTO `ota_district` VALUES ('32764', '台江区', 'fuzhou_fujian', '1', '32764');
INSERT INTO `ota_district` VALUES ('32765', '焉耆回族自治县', 'bayinguoleng', '1', '32765');
INSERT INTO `ota_district` VALUES ('32766', '威县', 'xingtai', '1', '32766');
INSERT INTO `ota_district` VALUES ('32767', '雅江县', 'ganzi', '1', '32767');
INSERT INTO `ota_district` VALUES ('32768', '巴南区', 'chongqing_city', '1', '32768');
INSERT INTO `ota_district` VALUES ('32769', '赫章县', 'bijie', '1', '32769');
INSERT INTO `ota_district` VALUES ('32770', '山丹县', 'zhangye', '1', '32770');
INSERT INTO `ota_district` VALUES ('32771', '咸丰县', 'enshi', '1', '32771');
INSERT INTO `ota_district` VALUES ('32772', '理塘县', 'ganzi', '1', '32772');
INSERT INTO `ota_district` VALUES ('32773', '霍邱县', 'liuan', '1', '32773');
INSERT INTO `ota_district` VALUES ('32774', '如东县', 'nantong', '1', '32774');
INSERT INTO `ota_district` VALUES ('32775', '上城区', 'hangzhou', '1', '32775');
INSERT INTO `ota_district` VALUES ('32776', '新罗区', 'longyan', '1', '32776');
INSERT INTO `ota_district` VALUES ('32777', '威宁彝族回族苗族自治县', 'bijie', '1', '32777');
INSERT INTO `ota_district` VALUES ('32778', '太仆寺旗', 'xilinguole', '1', '32778');
INSERT INTO `ota_district` VALUES ('32779', '桐梓县', 'zunyi', '1', '32779');
INSERT INTO `ota_district` VALUES ('32780', '繁昌县', 'wuhu', '1', '32780');
INSERT INTO `ota_district` VALUES ('32781', '福海县', 'aletai', '1', '32781');
INSERT INTO `ota_district` VALUES ('32782', '张家川回族自治县', 'tianshui', '1', '32782');
INSERT INTO `ota_district` VALUES ('32783', '灌阳县', 'guilin', '1', '32783');
INSERT INTO `ota_district` VALUES ('32784', '武隆县', 'chongqing_city', '1', '32784');
INSERT INTO `ota_district` VALUES ('32785', '桂阳县', 'chenzhou', '1', '32785');
INSERT INTO `ota_district` VALUES ('32786', '荔湾区', 'guangzhou', '1', '32786');
INSERT INTO `ota_district` VALUES ('32787', '云霄县', 'zhangzhou', '1', '32787');
INSERT INTO `ota_district` VALUES ('32788', '安义县', 'nanchang', '1', '32788');
INSERT INTO `ota_district` VALUES ('32789', '柞水县', 'shangluo', '1', '32789');
INSERT INTO `ota_district` VALUES ('32790', '马关县', 'wenshan', '1', '32790');
INSERT INTO `ota_district` VALUES ('32791', '屏南县', 'ningde', '1', '32791');
INSERT INTO `ota_district` VALUES ('32792', '兴山县', 'yichang', '1', '32792');
INSERT INTO `ota_district` VALUES ('32793', '南郑县', 'hanzhong', '1', '32793');
INSERT INTO `ota_district` VALUES ('32794', '邱县', 'handan', '1', '32794');
INSERT INTO `ota_district` VALUES ('32795', '渠县', 'dazhou', '1', '32795');
INSERT INTO `ota_district` VALUES ('32796', '浦口区', 'nanjing', '1', '32796');
INSERT INTO `ota_district` VALUES ('32797', '余干县', 'shangrao', '1', '32797');
INSERT INTO `ota_district` VALUES ('32798', '和田县', 'hetian', '1', '32798');
INSERT INTO `ota_district` VALUES ('32799', '蒲城县', 'weinan', '1', '32799');
INSERT INTO `ota_district` VALUES ('32800', '解放区', 'jiaozuo', '1', '32800');
INSERT INTO `ota_district` VALUES ('32801', '兴安县', 'guilin', '1', '32801');
INSERT INTO `ota_district` VALUES ('32802', '雷波县', 'liangshan', '1', '32802');
INSERT INTO `ota_district` VALUES ('32803', '金湾区', 'zhuhai', '1', '32803');
INSERT INTO `ota_district` VALUES ('32804', '铜山县', 'xuzhou', '1', '32804');
INSERT INTO `ota_district` VALUES ('32805', '蚌山区', 'bangbu', '1', '32805');
INSERT INTO `ota_district` VALUES ('32806', '监利县', 'jingzhou', '1', '32806');
INSERT INTO `ota_district` VALUES ('32807', '察隅县', 'linzhi', '1', '32807');
INSERT INTO `ota_district` VALUES ('32808', '阿克塞哈萨克族自治县', 'jiuquan', '1', '32808');
INSERT INTO `ota_district` VALUES ('32809', '卓资县', 'wulanchabu', '1', '32809');
INSERT INTO `ota_district` VALUES ('32810', '文安县', 'langfang', '1', '32810');
INSERT INTO `ota_district` VALUES ('32811', '麟游县', 'baoji', '1', '32811');
INSERT INTO `ota_district` VALUES ('32812', '来安县', 'chuzhou', '1', '32812');
INSERT INTO `ota_district` VALUES ('32813', '庆元县', 'lishui', '1', '32813');
INSERT INTO `ota_district` VALUES ('32814', '龙华区', 'haikou', '1', '32814');
INSERT INTO `ota_district` VALUES ('32815', '舟曲县', 'gannan', '1', '32815');
INSERT INTO `ota_district` VALUES ('32816', '东安区', 'mudanjiang', '1', '32816');
INSERT INTO `ota_district` VALUES ('32818', '神池县', 'xinzhou', '1', '32818');
INSERT INTO `ota_district` VALUES ('32819', '渭城区', 'xianyang', '1', '32819');
INSERT INTO `ota_district` VALUES ('32820', '仓山区', 'fuzhou_fujian', '1', '32820');
INSERT INTO `ota_district` VALUES ('32821', '丰润区', 'tangshan', '1', '32821');
INSERT INTO `ota_district` VALUES ('32822', '上林县', 'nanning', '1', '32822');
INSERT INTO `ota_district` VALUES ('32823', '腾冲县', 'baoshan', '1', '32823');
INSERT INTO `ota_district` VALUES ('32824', '文成县', 'wenzhou', '1', '32824');
INSERT INTO `ota_district` VALUES ('32825', '奉新县', 'yichun_jiangxi', '1', '32825');
INSERT INTO `ota_district` VALUES ('32826', '环县', 'qingyang', '1', '32826');
INSERT INTO `ota_district` VALUES ('32827', '红桥区', 'tianjin_city', '1', '32827');
INSERT INTO `ota_district` VALUES ('32828', '达尔罕茂明安联合旗', 'baotou', '1', '32828');
INSERT INTO `ota_district` VALUES ('32829', '于田县', 'hetian', '1', '32829');
INSERT INTO `ota_district` VALUES ('32830', '二道区', 'changchun', '1', '32830');
INSERT INTO `ota_district` VALUES ('32831', '管城回族区', 'zhengzhou', '1', '32831');
INSERT INTO `ota_district` VALUES ('32832', '奈曼旗', 'tongliao', '1', '32832');
INSERT INTO `ota_district` VALUES ('32833', '正定县', 'shijiazhuang', '1', '32833');
INSERT INTO `ota_district` VALUES ('32834', '岱山县', 'zhoushan', '1', '32834');
INSERT INTO `ota_district` VALUES ('32835', '盐池县', 'wuzhong', '1', '32835');
INSERT INTO `ota_district` VALUES ('32836', '鲁山县', 'pingdingshan', '1', '32836');
INSERT INTO `ota_district` VALUES ('32837', '镇雄县', 'zhaotong', '1', '32837');
INSERT INTO `ota_district` VALUES ('32838', '汉南区', 'wuhan', '1', '32838');
INSERT INTO `ota_district` VALUES ('32839', '皋兰县', 'lanzhou', '1', '32839');
INSERT INTO `ota_district` VALUES ('32840', '加查县', 'shannan', '1', '32840');
INSERT INTO `ota_district` VALUES ('32841', '魏都区', 'xuchang', '1', '32841');
INSERT INTO `ota_district` VALUES ('32842', '得荣县', 'ganzi', '1', '32842');
INSERT INTO `ota_district` VALUES ('32843', '沧县', 'cangzhou', '1', '32843');
INSERT INTO `ota_district` VALUES ('32844', '九龙坡区', 'chongqing_city', '1', '32844');
INSERT INTO `ota_district` VALUES ('32845', '土默特左旗', 'huhehaote', '1', '32845');
INSERT INTO `ota_district` VALUES ('32846', '武川县', 'huhehaote', '1', '32846');
INSERT INTO `ota_district` VALUES ('32847', '嫩江县', 'heihe', '1', '32847');
INSERT INTO `ota_district` VALUES ('32848', '永昌县', 'jinchang', '1', '32848');
INSERT INTO `ota_district` VALUES ('32849', '合浦县', 'beihai', '1', '32849');
INSERT INTO `ota_district` VALUES ('32850', '封丘县', 'xinxiang', '1', '32850');
INSERT INTO `ota_district` VALUES ('32852', '达坂城区', 'wulumuqi', '1', '32852');
INSERT INTO `ota_district` VALUES ('32853', '兴隆台区', 'panjin', '1', '32853');
INSERT INTO `ota_district` VALUES ('32854', '顺德区', 'foshan', '1', '32854');
INSERT INTO `ota_district` VALUES ('32855', '岐山县', 'baoji', '1', '32855');
INSERT INTO `ota_district` VALUES ('32856', '贡觉县', 'changdu', '1', '32856');
INSERT INTO `ota_district` VALUES ('32857', '湘西土家族苗族自治州', 'xiangxi', '1', '32857');
INSERT INTO `ota_district` VALUES ('32858', '盱眙县', 'huaian', '1', '32858');
INSERT INTO `ota_district` VALUES ('32859', '大竹县', 'dazhou', '1', '32859');
INSERT INTO `ota_district` VALUES ('32860', '龙门县', 'huizhou_guangdong', '1', '32860');
INSERT INTO `ota_district` VALUES ('32861', '南乐县', 'puyang', '1', '32861');
INSERT INTO `ota_district` VALUES ('32862', '江永县', 'yongzhou', '1', '32862');
INSERT INTO `ota_district` VALUES ('32863', '西区', 'panzhihua', '1', '32863');
INSERT INTO `ota_district` VALUES ('32864', '大名县', 'handan', '1', '32864');
INSERT INTO `ota_district` VALUES ('32865', '沧源佤族自治县', 'lincang', '1', '32865');
INSERT INTO `ota_district` VALUES ('32866', '丹棱县', 'meishan', '1', '32866');
INSERT INTO `ota_district` VALUES ('32867', '万年县', 'shangrao', '1', '32867');
INSERT INTO `ota_district` VALUES ('32868', '峰峰矿区', 'handan', '1', '32868');
INSERT INTO `ota_district` VALUES ('32869', '凤县', 'baoji', '1', '32869');
INSERT INTO `ota_district` VALUES ('32870', '建湖县', 'yancheng', '1', '32870');
INSERT INTO `ota_district` VALUES ('32871', '淮滨县', 'xinyang', '1', '32871');
INSERT INTO `ota_district` VALUES ('32872', '榕城区', 'jieyang', '1', '32872');
INSERT INTO `ota_district` VALUES ('32873', '龙南县', 'ganzhou', '1', '32873');
INSERT INTO `ota_district` VALUES ('32874', '麻阳苗族自治县', 'huaihua', '1', '32874');
INSERT INTO `ota_district` VALUES ('32875', '江北区', 'ningbo', '1', '32875');
INSERT INTO `ota_district` VALUES ('32876', '恭城瑶族自治县', 'guilin', '1', '32876');
INSERT INTO `ota_district` VALUES ('32877', '临漳县', 'handan', '1', '32877');
INSERT INTO `ota_district` VALUES ('32878', '龙游县', 'quzhou', '1', '32878');
INSERT INTO `ota_district` VALUES ('32879', '芜湖县', 'wuhu', '1', '32879');
INSERT INTO `ota_district` VALUES ('32880', '伊川县', 'luoyang', '1', '32880');
INSERT INTO `ota_district` VALUES ('32881', '新邱区', 'fuxin', '1', '32881');
INSERT INTO `ota_district` VALUES ('32882', '金牛区', 'chengdu', '1', '32882');
INSERT INTO `ota_district` VALUES ('32883', '那坡县', 'baise', '1', '32883');
INSERT INTO `ota_district` VALUES ('32884', '泗洪县', 'suqian', '1', '32884');
INSERT INTO `ota_district` VALUES ('32885', '武邑县', 'hengshui', '1', '32885');
INSERT INTO `ota_district` VALUES ('32886', '南木林县', 'rikaze', '1', '32886');
INSERT INTO `ota_district` VALUES ('32887', '范县', 'puyang', '1', '32887');
INSERT INTO `ota_district` VALUES ('32888', '杞县', 'kaifeng', '1', '32888');
INSERT INTO `ota_district` VALUES ('32889', '隆回县', 'shaoyang', '1', '32889');
INSERT INTO `ota_district` VALUES ('32890', '庆城县', 'qingyang', '1', '32890');
INSERT INTO `ota_district` VALUES ('32891', '应县', 'shuozhou', '1', '32891');
INSERT INTO `ota_district` VALUES ('32892', '高陵县', 'xian', '1', '32892');
INSERT INTO `ota_district` VALUES ('32893', '贾汪区', 'xuzhou', '1', '32893');
INSERT INTO `ota_district` VALUES ('32894', '佳县', 'yulin_shanxi_02', '1', '32894');
INSERT INTO `ota_district` VALUES ('32895', '云和县', 'lishui', '1', '32895');
INSERT INTO `ota_district` VALUES ('32896', '阿克苏地区', 'akesu', '1', '32896');
INSERT INTO `ota_district` VALUES ('32897', '三台县', 'mianyang', '1', '32897');
INSERT INTO `ota_district` VALUES ('32898', '阳原县', 'zhangjiakou', '1', '32898');
INSERT INTO `ota_district` VALUES ('32899', '芒康县', 'changdu', '1', '32899');
INSERT INTO `ota_district` VALUES ('32900', '大安区', 'zigong', '1', '32900');
INSERT INTO `ota_district` VALUES ('32901', '旬邑县', 'xianyang', '1', '32901');
INSERT INTO `ota_district` VALUES ('32902', '清丰县', 'puyang', '1', '32902');
INSERT INTO `ota_district` VALUES ('32903', '沁县', 'changzhi', '1', '32903');
INSERT INTO `ota_district` VALUES ('32904', '永修县', 'jiujiang', '1', '32904');
INSERT INTO `ota_district` VALUES ('32905', '乳源瑶族自治县', 'shaoguan', '1', '32905');
INSERT INTO `ota_district` VALUES ('32906', '磐安县', 'jinhua', '1', '32906');
INSERT INTO `ota_district` VALUES ('32907', '开江县', 'dazhou', '1', '32907');
INSERT INTO `ota_district` VALUES ('32908', '和硕县', 'bayinguoleng', '1', '32908');
INSERT INTO `ota_district` VALUES ('32909', '德庆县', 'zhaoqing', '1', '32909');
INSERT INTO `ota_district` VALUES ('32910', '龙泉驿区', 'chengdu', '1', '32910');
INSERT INTO `ota_district` VALUES ('32911', '昌邑区', 'jilin_city', '1', '32911');
INSERT INTO `ota_district` VALUES ('32912', '桐柏县', 'nanyang', '1', '32912');
INSERT INTO `ota_district` VALUES ('32913', '壶关县', 'changzhi', '1', '32913');
INSERT INTO `ota_district` VALUES ('32914', '凤山县', 'hechi', '1', '32914');
INSERT INTO `ota_district` VALUES ('32915', '电白县', 'maoming', '1', '32915');
INSERT INTO `ota_district` VALUES ('32916', '和平区', 'shenyang', '1', '32916');
INSERT INTO `ota_district` VALUES ('32917', '绥阳县', 'zunyi', '1', '32917');
INSERT INTO `ota_district` VALUES ('32918', '郁南县', 'yunfu', '1', '32918');
INSERT INTO `ota_district` VALUES ('32919', '八步区', 'hezhou', '1', '32919');
INSERT INTO `ota_district` VALUES ('32921', '锦屏县', 'qiandongnan', '1', '32921');
INSERT INTO `ota_district` VALUES ('32922', '德清县', 'huzhou', '1', '32922');
INSERT INTO `ota_district` VALUES ('32923', '新津县', 'chengdu', '1', '32923');
INSERT INTO `ota_district` VALUES ('32924', '新绛县', 'yuncheng', '1', '32924');
INSERT INTO `ota_district` VALUES ('32925', '漾濞彝族自治县', 'dali', '1', '32925');
INSERT INTO `ota_district` VALUES ('32926', '东胜区', 'eerduosi', '1', '32926');
INSERT INTO `ota_district` VALUES ('32927', '麒麟区', 'qujing', '1', '32927');
INSERT INTO `ota_district` VALUES ('32928', '镇海区', 'ningbo', '1', '32928');
INSERT INTO `ota_district` VALUES ('32929', '乌尔禾区', 'kelamayi', '1', '32929');
INSERT INTO `ota_district` VALUES ('32930', '扎赉特旗', 'xingan', '1', '32930');
INSERT INTO `ota_district` VALUES ('32931', '府谷县', 'yulin_shanxi_02', '1', '32931');
INSERT INTO `ota_district` VALUES ('32932', '新建县', 'nanchang', '1', '32932');
INSERT INTO `ota_district` VALUES ('32933', '红河县', 'honghe', '1', '32933');
INSERT INTO `ota_district` VALUES ('32934', '清徐县', 'taiyuan', '1', '32934');
INSERT INTO `ota_district` VALUES ('32935', '城关区', 'lanzhou', '1', '32935');
INSERT INTO `ota_district` VALUES ('32936', '麻栗坡县', 'wenshan', '1', '32936');
INSERT INTO `ota_district` VALUES ('32937', '新都区', 'chengdu', '1', '32937');
INSERT INTO `ota_district` VALUES ('32938', '黔西南布依族苗族自治州', 'qianxinan', '1', '32938');
INSERT INTO `ota_district` VALUES ('32939', '浠水县', 'huanggang', '1', '32939');
INSERT INTO `ota_district` VALUES ('32940', '颍泉区', 'fuyang_anhui', '1', '32940');
INSERT INTO `ota_district` VALUES ('32941', '衡阳县', 'hengyang', '1', '32941');
INSERT INTO `ota_district` VALUES ('32942', '墨竹工卡县', 'lasa', '1', '32942');
INSERT INTO `ota_district` VALUES ('32943', '阳信县', 'binzhou', '1', '32943');
INSERT INTO `ota_district` VALUES ('32944', '林芝县', 'linzhi', '1', '32944');
INSERT INTO `ota_district` VALUES ('32945', '城子河区', 'jixi', '1', '32945');
INSERT INTO `ota_district` VALUES ('32946', '同心县', 'wuzhong', '1', '32946');
INSERT INTO `ota_district` VALUES ('32947', '乐至县', 'ziyang', '1', '32947');
INSERT INTO `ota_district` VALUES ('32948', '兰西县', 'suihua', '1', '32948');
INSERT INTO `ota_district` VALUES ('32949', '克东县', 'qiqihaer', '1', '32949');
INSERT INTO `ota_district` VALUES ('32950', '长沙县', 'changsha', '1', '32950');
INSERT INTO `ota_district` VALUES ('32951', '利辛县', 'bozhou', '1', '32951');
INSERT INTO `ota_district` VALUES ('32952', '长安区', 'xian', '1', '32952');
INSERT INTO `ota_district` VALUES ('32953', '缙云县', 'lishui', '1', '32953');
INSERT INTO `ota_district` VALUES ('32954', '景泰县', 'baiyin', '1', '32954');
INSERT INTO `ota_district` VALUES ('32955', '临洮县', 'dingxi', '1', '32955');
INSERT INTO `ota_district` VALUES ('32956', '甘南藏族自治州', 'gannan', '1', '32956');
INSERT INTO `ota_district` VALUES ('32957', '宁海县', 'ningbo', '1', '32957');
INSERT INTO `ota_district` VALUES ('32958', '江汉区', 'wuhan', '1', '32958');
INSERT INTO `ota_district` VALUES ('32959', '定兴县', 'baoding', '1', '32959');
INSERT INTO `ota_district` VALUES ('32960', '郓城县', 'heze', '1', '32960');
INSERT INTO `ota_district` VALUES ('32961', '甘孜藏族自治州', 'ganzi', '1', '32961');
INSERT INTO `ota_district` VALUES ('32962', '咸安区', 'xianning', '1', '32962');
INSERT INTO `ota_district` VALUES ('32963', '临潼区', 'xian', '1', '32963');
INSERT INTO `ota_district` VALUES ('32964', '闵行区', 'shanghai_city', '1', '32964');
INSERT INTO `ota_district` VALUES ('32965', '吐鲁番地区', 'tulufan', '1', '32965');
INSERT INTO `ota_district` VALUES ('32966', '龙山县', 'xiangxi', '1', '32966');
INSERT INTO `ota_district` VALUES ('32967', '夏县', 'yuncheng', '1', '32967');
INSERT INTO `ota_district` VALUES ('32968', '西乡塘区', 'nanning', '1', '32968');
INSERT INTO `ota_district` VALUES ('32969', '洞头县', 'wenzhou', '1', '32969');
INSERT INTO `ota_district` VALUES ('32971', '聂荣县', 'naqu', '1', '32971');
INSERT INTO `ota_district` VALUES ('32972', '相城区', 'suzhou_jiangsu', '1', '32972');
INSERT INTO `ota_district` VALUES ('32973', '太子河区', 'liaoyang', '1', '32973');
INSERT INTO `ota_district` VALUES ('32974', '永年县', 'handan', '1', '32974');
INSERT INTO `ota_district` VALUES ('32975', '鹤城区', 'huaihua', '1', '32975');
INSERT INTO `ota_district` VALUES ('32976', '星子县', 'jiujiang', '1', '32976');
INSERT INTO `ota_district` VALUES ('32978', '南漳县', 'xiangfan', '1', '32978');
INSERT INTO `ota_district` VALUES ('32979', '伽师县', 'kashi', '1', '32979');
INSERT INTO `ota_district` VALUES ('32980', '南芬区', 'benxi', '1', '32980');
INSERT INTO `ota_district` VALUES ('32981', '临淄区', 'zibo', '1', '32981');
INSERT INTO `ota_district` VALUES ('32982', '黄岩区', 'taizhou_zhejiang', '1', '32982');
INSERT INTO `ota_district` VALUES ('32983', '淇县', 'hebi', '1', '32983');
INSERT INTO `ota_district` VALUES ('32984', '双清区', 'shaoyang', '1', '32984');
INSERT INTO `ota_district` VALUES ('32985', '利通区', 'wuzhong', '1', '32985');
INSERT INTO `ota_district` VALUES ('32986', '萝北县', 'hegang', '1', '32986');
INSERT INTO `ota_district` VALUES ('32987', '枣强县', 'hengshui', '1', '32987');
INSERT INTO `ota_district` VALUES ('32988', '花山区', 'maanshan', '1', '32988');
INSERT INTO `ota_district` VALUES ('32989', '江川县', 'yuxi', '1', '32989');
INSERT INTO `ota_district` VALUES ('32990', '泸水县', 'nujiang', '1', '32990');
INSERT INTO `ota_district` VALUES ('32991', '沭阳县', 'suqian', '1', '32991');
INSERT INTO `ota_district` VALUES ('32992', '阳西县', 'yangjiang', '1', '32992');
INSERT INTO `ota_district` VALUES ('32993', '龙马潭区', 'luzhou', '1', '32993');
INSERT INTO `ota_district` VALUES ('32994', '巴林左旗', 'chifeng', '1', '32994');
INSERT INTO `ota_district` VALUES ('32995', '溪湖区', 'benxi', '1', '32995');
INSERT INTO `ota_district` VALUES ('32996', '射洪县', 'suining', '1', '32996');
INSERT INTO `ota_district` VALUES ('32997', '苏尼特右旗', 'xilinguole', '1', '32997');
INSERT INTO `ota_district` VALUES ('32998', '安远县', 'ganzhou', '1', '32998');
INSERT INTO `ota_district` VALUES ('32999', '巴彦县', 'haerbin', '1', '32999');
INSERT INTO `ota_district` VALUES ('33000', '德格县', 'ganzi', '1', '33000');
INSERT INTO `ota_district` VALUES ('33001', '秀英区', 'haikou', '1', '33001');
INSERT INTO `ota_district` VALUES ('33002', '揭东县', 'jieyang', '1', '33002');
INSERT INTO `ota_district` VALUES ('33003', '钟楼区', 'changzhou', '1', '33003');
INSERT INTO `ota_district` VALUES ('33004', '黄龙县', 'yanan', '1', '33004');
INSERT INTO `ota_district` VALUES ('33005', '都昌县', 'jiujiang', '1', '33005');
INSERT INTO `ota_district` VALUES ('33006', '九寨沟县', 'aba', '1', '33006');
INSERT INTO `ota_district` VALUES ('33007', '伊犁哈萨克自治州', 'yili', '1', '33007');
INSERT INTO `ota_district` VALUES ('33008', '屏边苗族自治县', 'honghe', '1', '33008');
INSERT INTO `ota_district` VALUES ('33009', '平舆县', 'zhumadian', '1', '33009');
INSERT INTO `ota_district` VALUES ('33010', '饶平县', 'chaozhou', '1', '33010');
INSERT INTO `ota_district` VALUES ('33011', '西平县', 'zhumadian', '1', '33011');
INSERT INTO `ota_district` VALUES ('33012', '新北区', 'changzhou', '1', '33012');
INSERT INTO `ota_district` VALUES ('33013', '大余县', 'ganzhou', '1', '33013');
INSERT INTO `ota_district` VALUES ('33014', '米林县', 'linzhi', '1', '33014');
INSERT INTO `ota_district` VALUES ('33015', '洋县', 'hanzhong', '1', '33015');
INSERT INTO `ota_district` VALUES ('33016', '罗湖区', 'shenzhen', '1', '33016');
INSERT INTO `ota_district` VALUES ('33017', '夷陵区', 'yichang', '1', '33017');
INSERT INTO `ota_district` VALUES ('33018', '澄城县', 'weinan', '1', '33018');
INSERT INTO `ota_district` VALUES ('33019', '河曲县', 'xinzhou', '1', '33019');
INSERT INTO `ota_district` VALUES ('33020', '会宁县', 'baiyin', '1', '33020');
INSERT INTO `ota_district` VALUES ('33021', '马龙县', 'qujing', '1', '33021');
INSERT INTO `ota_district` VALUES ('33022', '大同县', 'datong', '1', '33022');
INSERT INTO `ota_district` VALUES ('33023', '陆河县', 'shanwei', '1', '33023');
INSERT INTO `ota_district` VALUES ('33024', '秀峰区', 'guilin', '1', '33024');
INSERT INTO `ota_district` VALUES ('33025', '兴宁区', 'nanning', '1', '33025');
INSERT INTO `ota_district` VALUES ('33026', '平乡县', 'xingtai', '1', '33026');
INSERT INTO `ota_district` VALUES ('33027', '洛浦县', 'hetian', '1', '33027');
INSERT INTO `ota_district` VALUES ('33028', '港口区', 'fangchenggang', '1', '33028');
INSERT INTO `ota_district` VALUES ('33029', '宣汉县', 'dazhou', '1', '33029');
INSERT INTO `ota_district` VALUES ('33030', '莒南县', 'linyi', '1', '33030');
INSERT INTO `ota_district` VALUES ('33031', '榆中县', 'lanzhou', '1', '33031');
INSERT INTO `ota_district` VALUES ('33032', '崂山区', 'qingdao', '1', '33032');
INSERT INTO `ota_district` VALUES ('33033', '旌阳区', 'deyang', '1', '33033');
INSERT INTO `ota_district` VALUES ('33034', '金安区', 'liuan', '1', '33034');
INSERT INTO `ota_district` VALUES ('33035', '房山区', 'beijing_city', '1', '33035');
INSERT INTO `ota_district` VALUES ('33036', '白朗县', 'rikaze', '1', '33036');
INSERT INTO `ota_district` VALUES ('33037', '临桂县', 'guilin', '1', '33037');
INSERT INTO `ota_district` VALUES ('33038', '新平彝族傣族自治县', 'yuxi', '1', '33038');
INSERT INTO `ota_district` VALUES ('33039', '安化县', 'yiyang', '1', '33039');
INSERT INTO `ota_district` VALUES ('33040', '延庆县', 'beijing_city', '1', '33040');
INSERT INTO `ota_district` VALUES ('33041', '涵江区', 'putian', '1', '33041');
INSERT INTO `ota_district` VALUES ('33042', '博白县', 'yulin_guangxi', '1', '33042');
INSERT INTO `ota_district` VALUES ('33043', '遵义县', 'zunyi', '1', '33043');
INSERT INTO `ota_district` VALUES ('33044', '昭觉县', 'liangshan', '1', '33044');
INSERT INTO `ota_district` VALUES ('33045', '墨脱县', 'linzhi', '1', '33045');
INSERT INTO `ota_district` VALUES ('33046', '黄平县', 'qiandongnan', '1', '33046');
INSERT INTO `ota_district` VALUES ('33047', '合水县', 'qingyang', '1', '33047');
INSERT INTO `ota_district` VALUES ('33048', '三原县', 'xianyang', '1', '33048');
INSERT INTO `ota_district` VALUES ('33049', '马尾区', 'fuzhou_fujian', '1', '33049');
INSERT INTO `ota_district` VALUES ('33050', '三都水族自治县', 'qiannan', '1', '33050');
INSERT INTO `ota_district` VALUES ('33051', '武进区', 'changzhou', '1', '33051');
INSERT INTO `ota_district` VALUES ('33052', '津南区', 'tianjin_city', '1', '33052');
INSERT INTO `ota_district` VALUES ('33053', '荔波县', 'qiannan', '1', '33053');
INSERT INTO `ota_district` VALUES ('33054', '路南区', 'tangshan', '1', '33054');
INSERT INTO `ota_district` VALUES ('33055', '新和县', 'akesu', '1', '33055');
INSERT INTO `ota_district` VALUES ('33056', '岱岳区', 'taian', '1', '33056');
INSERT INTO `ota_district` VALUES ('33057', '斗门区', 'zhuhai', '1', '33057');
INSERT INTO `ota_district` VALUES ('33058', '华池县', 'qingyang', '1', '33058');
INSERT INTO `ota_district` VALUES ('33059', '东陵区', 'shenyang', '1', '33059');
INSERT INTO `ota_district` VALUES ('33060', '南长区', 'wuxi', '1', '33060');
INSERT INTO `ota_district` VALUES ('33061', '旺苍县', 'guangyuan', '1', '33061');
INSERT INTO `ota_district` VALUES ('33062', '溧水县', 'nanjing', '1', '33062');
INSERT INTO `ota_district` VALUES ('33063', '海西蒙古族藏族自治州', 'haixi', '1', '33063');
INSERT INTO `ota_district` VALUES ('33064', '富拉尔基区', 'qiqihaer', '1', '33064');
INSERT INTO `ota_district` VALUES ('33065', '南溪县', 'yibin', '1', '33065');
INSERT INTO `ota_district` VALUES ('33066', '黄浦区', 'shanghai_city', '1', '33066');
INSERT INTO `ota_district` VALUES ('33067', '若羌县', 'bayinguoleng', '1', '33067');
INSERT INTO `ota_district` VALUES ('33068', '龙子湖区', 'bangbu', '1', '33068');
INSERT INTO `ota_district` VALUES ('33069', '翔安区', 'xiamen', '1', '33069');
INSERT INTO `ota_district` VALUES ('33070', '华亭县', 'pingliang', '1', '33070');
INSERT INTO `ota_district` VALUES ('33071', '禄丰县', 'chuxiong', '1', '33071');
INSERT INTO `ota_district` VALUES ('33072', '琼山区', 'haikou', '1', '33072');
INSERT INTO `ota_district` VALUES ('33073', '美兰区', 'haikou', '1', '33073');
INSERT INTO `ota_district` VALUES ('33074', '平山县', 'shijiazhuang', '1', '33074');
INSERT INTO `ota_district` VALUES ('33075', '木垒哈萨克自治县', 'changji', '1', '33075');
INSERT INTO `ota_district` VALUES ('33076', '和田地区', 'hetian', '1', '33076');
INSERT INTO `ota_district` VALUES ('33077', '长寿区', 'chongqing_city', '1', '33077');
INSERT INTO `ota_district` VALUES ('33078', '紫云苗族布依族自治县', 'anshun', '1', '33078');
INSERT INTO `ota_district` VALUES ('33079', '遂平县', 'zhumadian', '1', '33079');
INSERT INTO `ota_district` VALUES ('33080', '明水县', 'suihua', '1', '33080');
INSERT INTO `ota_district` VALUES ('33081', '宁晋县', 'xingtai', '1', '33081');
INSERT INTO `ota_district` VALUES ('33082', '乌拉特后旗', 'bayannaoer', '1', '33082');
INSERT INTO `ota_district` VALUES ('33083', '安阳县', 'anyang', '1', '33083');
INSERT INTO `ota_district` VALUES ('33084', '盐亭县', 'mianyang', '1', '33084');
INSERT INTO `ota_district` VALUES ('33085', '尉氏县', 'kaifeng', '1', '33085');
INSERT INTO `ota_district` VALUES ('33086', '迪庆藏族自治州', 'diqing', '1', '33086');
INSERT INTO `ota_district` VALUES ('33087', '巫山县', 'chongqing_city', '1', '33087');
INSERT INTO `ota_district` VALUES ('33088', '响水县', 'yancheng', '1', '33088');
INSERT INTO `ota_district` VALUES ('33089', '秀屿区', 'putian', '1', '33089');
INSERT INTO `ota_district` VALUES ('33090', '椒江区', 'taizhou_zhejiang', '1', '33090');
INSERT INTO `ota_district` VALUES ('33091', '通江县', 'bazhong', '1', '33091');
INSERT INTO `ota_district` VALUES ('33092', '广河县', 'linxia', '1', '33092');
INSERT INTO `ota_district` VALUES ('33093', '尤溪县', 'sanming', '1', '33093');
INSERT INTO `ota_district` VALUES ('33094', '西固区', 'lanzhou', '1', '33094');
INSERT INTO `ota_district` VALUES ('33095', '博野县', 'baoding', '1', '33095');
INSERT INTO `ota_district` VALUES ('33096', '息烽县', 'guiyang', '1', '33096');
INSERT INTO `ota_district` VALUES ('33097', '扶绥县', 'chongzuo', '1', '33097');
INSERT INTO `ota_district` VALUES ('33098', '昆都仑区', 'baotou', '1', '33098');
INSERT INTO `ota_district` VALUES ('33099', '房县', 'shiyan', '1', '33099');
INSERT INTO `ota_district` VALUES ('33100', '宁明县', 'chongzuo', '1', '33100');
INSERT INTO `ota_district` VALUES ('33101', '米东区', 'wulumuqi', '1', '33101');
INSERT INTO `ota_district` VALUES ('33102', '迭部县', 'gannan', '1', '33102');
INSERT INTO `ota_district` VALUES ('33103', '宁城县', 'chifeng', '1', '33103');
INSERT INTO `ota_district` VALUES ('33104', '怀集县', 'zhaoqing', '1', '33104');
INSERT INTO `ota_district` VALUES ('33105', '太湖县', 'anqing', '1', '33105');
INSERT INTO `ota_district` VALUES ('33106', '方山县', 'lvliang', '1', '33106');
INSERT INTO `ota_district` VALUES ('33107', '浮梁县', 'jingdezhen', '1', '33107');
INSERT INTO `ota_district` VALUES ('33108', '诏安县', 'zhangzhou', '1', '33108');
INSERT INTO `ota_district` VALUES ('33109', '丰县', 'xuzhou', '1', '33109');
INSERT INTO `ota_district` VALUES ('33110', '沙湾区', 'leshan', '1', '33110');
INSERT INTO `ota_district` VALUES ('33111', '朗县', 'linzhi', '1', '33111');
INSERT INTO `ota_district` VALUES ('33112', '富民县', 'kunming', '1', '33112');
INSERT INTO `ota_district` VALUES ('33113', '通道侗族自治县', 'huaihua', '1', '33113');
INSERT INTO `ota_district` VALUES ('33114', '薛城区', 'zaozhuang', '1', '33114');
INSERT INTO `ota_district` VALUES ('33115', '吉木乃县', 'aletai', '1', '33115');
INSERT INTO `ota_district` VALUES ('33116', '保德县', 'xinzhou', '1', '33116');
INSERT INTO `ota_district` VALUES ('33117', '桃源县', 'changde', '1', '33117');
INSERT INTO `ota_district` VALUES ('33118', '江城区', 'yangjiang', '1', '33118');
INSERT INTO `ota_district` VALUES ('33119', '太康县', 'zhoukou', '1', '33119');
INSERT INTO `ota_district` VALUES ('33120', '崇信县', 'pingliang', '1', '33120');
INSERT INTO `ota_district` VALUES ('33121', '乌达区', 'wuhai', '1', '33121');
INSERT INTO `ota_district` VALUES ('33122', '潼南县', 'chongqing_city', '1', '33122');
INSERT INTO `ota_district` VALUES ('33123', '岚山区', 'rizhao', '1', '33123');
INSERT INTO `ota_district` VALUES ('33124', '六枝特区', 'liupanshui', '1', '33124');
INSERT INTO `ota_district` VALUES ('33125', '麦盖提县', 'kashi', '1', '33125');
INSERT INTO `ota_district` VALUES ('33126', '定陶县', 'heze', '1', '33126');
INSERT INTO `ota_district` VALUES ('33127', '祁县', 'jinzhong', '1', '33127');
INSERT INTO `ota_district` VALUES ('33128', '上高县', 'yichun_jiangxi', '1', '33128');
INSERT INTO `ota_district` VALUES ('33129', '八道江区', 'baishan', '1', '33129');
INSERT INTO `ota_district` VALUES ('33130', '宾县', 'haerbin', '1', '33130');
INSERT INTO `ota_district` VALUES ('33131', '犍为县', 'leshan', '1', '33131');
INSERT INTO `ota_district` VALUES ('33132', '滴道区', 'jixi', '1', '33132');
INSERT INTO `ota_district` VALUES ('33133', '宏伟区', 'liaoyang', '1', '33133');
INSERT INTO `ota_district` VALUES ('33134', '栾川县', 'luoyang', '1', '33134');
INSERT INTO `ota_district` VALUES ('33135', '孟村回族自治县', 'cangzhou', '1', '33135');
INSERT INTO `ota_district` VALUES ('33136', '天等县', 'chongzuo', '1', '33136');
INSERT INTO `ota_district` VALUES ('33137', '广饶县', 'dongying', '1', '33137');
INSERT INTO `ota_district` VALUES ('33138', '祁连县', 'haibei', '1', '33138');
INSERT INTO `ota_district` VALUES ('33139', '古塔区', 'jinzhou', '1', '33139');
INSERT INTO `ota_district` VALUES ('33140', '宣州区', 'xuancheng', '1', '33140');
INSERT INTO `ota_district` VALUES ('33141', '索县', 'naqu', '1', '33141');
INSERT INTO `ota_district` VALUES ('33142', '苍山县', 'linyi', '1', '33142');
INSERT INTO `ota_district` VALUES ('33143', '海沧区', 'xiamen', '1', '33143');
INSERT INTO `ota_district` VALUES ('33144', '河南蒙古族自治县', 'huangnan', '1', '33144');
INSERT INTO `ota_district` VALUES ('33145', '和县', 'chaohu', '1', '33145');
INSERT INTO `ota_district` VALUES ('33146', '山海关区', 'qinhuangdao', '1', '33146');
INSERT INTO `ota_district` VALUES ('33147', '文圣区', 'liaoyang', '1', '33147');
INSERT INTO `ota_district` VALUES ('33148', '大姚县', 'chuxiong', '1', '33148');
INSERT INTO `ota_district` VALUES ('33149', '沧浪区', 'suzhou_jiangsu', '1', '33149');
INSERT INTO `ota_district` VALUES ('33150', '武功县', 'xianyang', '1', '33150');
INSERT INTO `ota_district` VALUES ('33151', '五原县', 'bayannaoer', '1', '33151');
INSERT INTO `ota_district` VALUES ('33152', '晋安区', 'fuzhou_fujian', '1', '33152');
INSERT INTO `ota_district` VALUES ('33153', '中阳县', 'lvliang', '1', '33153');
INSERT INTO `ota_district` VALUES ('33154', '呼兰区', 'haerbin', '1', '33154');
INSERT INTO `ota_district` VALUES ('33155', '白云区', 'guangzhou', '1', '33155');
INSERT INTO `ota_district` VALUES ('33156', '阜城县', 'hengshui', '1', '33156');
INSERT INTO `ota_district` VALUES ('33157', '中原区', 'zhengzhou', '1', '33157');
INSERT INTO `ota_district` VALUES ('33158', '塔什库尔干塔吉克自治县', 'kashi', '1', '33158');
INSERT INTO `ota_district` VALUES ('33159', '洛南县', 'shangluo', '1', '33159');
INSERT INTO `ota_district` VALUES ('33160', '嘉陵区', 'nanchong', '1', '33160');
INSERT INTO `ota_district` VALUES ('33162', '永嘉县', 'wenzhou', '1', '33162');
INSERT INTO `ota_district` VALUES ('33163', '峨山彝族自治县', 'yuxi', '1', '33163');
INSERT INTO `ota_district` VALUES ('33164', '旬阳县', 'ankang', '1', '33164');
INSERT INTO `ota_district` VALUES ('33165', '谢通门县', 'rikaze', '1', '33165');
INSERT INTO `ota_district` VALUES ('33166', '淅川县', 'nanyang', '1', '33166');
INSERT INTO `ota_district` VALUES ('33167', '徐闻县', 'zhanjiang', '1', '33167');
INSERT INTO `ota_district` VALUES ('33168', '美姑县', 'liangshan', '1', '33168');
INSERT INTO `ota_district` VALUES ('33169', '莘县', 'liaocheng', '1', '33169');
INSERT INTO `ota_district` VALUES ('33170', '红原县', 'aba', '1', '33170');
INSERT INTO `ota_district` VALUES ('33171', '秦安县', 'tianshui', '1', '33171');
INSERT INTO `ota_district` VALUES ('33172', '西华县', 'zhoukou', '1', '33172');
INSERT INTO `ota_district` VALUES ('33173', '宁县', 'qingyang', '1', '33173');
INSERT INTO `ota_district` VALUES ('33174', '桦南县', 'jiamusi', '1', '33174');
INSERT INTO `ota_district` VALUES ('33175', '剑河县', 'qiandongnan', '1', '33175');
INSERT INTO `ota_district` VALUES ('33176', '香洲区', 'zhuhai', '1', '33176');
INSERT INTO `ota_district` VALUES ('33177', '马边彝族自治县', 'leshan', '1', '33177');
INSERT INTO `ota_district` VALUES ('33178', '长丰县', 'hefei', '1', '33178');
INSERT INTO `ota_district` VALUES ('33179', '滦县', 'tangshan', '1', '33179');
INSERT INTO `ota_district` VALUES ('33180', '石阡县', 'tongren', '1', '33180');
INSERT INTO `ota_district` VALUES ('33181', '平安县', 'haidong', '1', '33181');
INSERT INTO `ota_district` VALUES ('33182', '贺兰县', 'yinchuan', '1', '33182');
INSERT INTO `ota_district` VALUES ('33183', '双江拉祜族佤族布朗族傣族自治县', 'lincang', '1', '33183');
INSERT INTO `ota_district` VALUES ('33184', '容县', 'yulin_guangxi', '1', '33184');
INSERT INTO `ota_district` VALUES ('33185', '雁山区', 'guilin', '1', '33185');
INSERT INTO `ota_district` VALUES ('33186', '云岩区', 'guiyang', '1', '33186');
INSERT INTO `ota_district` VALUES ('33187', '正蓝旗', 'xilinguole', '1', '33187');
INSERT INTO `ota_district` VALUES ('33188', '通海县', 'yuxi', '1', '33188');
INSERT INTO `ota_district` VALUES ('33189', '保靖县', 'xiangxi', '1', '33189');
INSERT INTO `ota_district` VALUES ('33190', '宽城满族自治县', 'chengde', '1', '33190');
INSERT INTO `ota_district` VALUES ('33191', '木里藏族自治县', 'liangshan', '1', '33191');
INSERT INTO `ota_district` VALUES ('33192', '长安区', 'shijiazhuang', '1', '33192');
INSERT INTO `ota_district` VALUES ('33193', '安新县', 'baoding', '1', '33193');
INSERT INTO `ota_district` VALUES ('33194', '肥西县', 'hefei', '1', '33194');
INSERT INTO `ota_district` VALUES ('33195', '兰坪白族普米族自治县', 'nujiang', '1', '33195');
INSERT INTO `ota_district` VALUES ('33196', '长白朝鲜族自治县', 'baishan', '1', '33196');
INSERT INTO `ota_district` VALUES ('33197', '武宣县', 'laibin', '1', '33197');
INSERT INTO `ota_district` VALUES ('33198', '北关区', 'anyang', '1', '33198');
INSERT INTO `ota_district` VALUES ('33199', '子洲县', 'yulin_shanxi_02', '1', '33199');
INSERT INTO `ota_district` VALUES ('33200', '太和县', 'fuyang_anhui', '1', '33200');
INSERT INTO `ota_district` VALUES ('33201', '寿阳县', 'jinzhong', '1', '33201');
INSERT INTO `ota_district` VALUES ('33202', '烈山区', 'huaibei', '1', '33202');
INSERT INTO `ota_district` VALUES ('33203', '大英县', 'suining', '1', '33203');
INSERT INTO `ota_district` VALUES ('33204', '肇源县', 'daqing', '1', '33204');
INSERT INTO `ota_district` VALUES ('33205', '延川县', 'yanan', '1', '33205');
INSERT INTO `ota_district` VALUES ('33206', '雁峰区', 'hengyang', '1', '33206');
INSERT INTO `ota_district` VALUES ('33207', '湘阴县', 'yueyang', '1', '33207');
INSERT INTO `ota_district` VALUES ('33208', '周村区', 'zibo', '1', '33208');
INSERT INTO `ota_district` VALUES ('33209', '西山区', 'kunming', '1', '33209');
INSERT INTO `ota_district` VALUES ('33210', '筠连县', 'yibin', '1', '33210');
INSERT INTO `ota_district` VALUES ('33211', '庆云县', 'dezhou', '1', '33211');
INSERT INTO `ota_district` VALUES ('33212', '和静县', 'bayinguoleng', '1', '33212');
INSERT INTO `ota_district` VALUES ('33213', '远安县', 'yichang', '1', '33213');
INSERT INTO `ota_district` VALUES ('33214', '嘉善县', 'jiaxing', '1', '33214');
INSERT INTO `ota_district` VALUES ('33215', '石峰区', 'zhuzhou', '1', '33215');
INSERT INTO `ota_district` VALUES ('33216', '和布克赛尔蒙古自治县', 'tacheng', '1', '33216');
INSERT INTO `ota_district` VALUES ('33217', '岢岚县', 'xinzhou', '1', '33217');
INSERT INTO `ota_district` VALUES ('33218', '宣化区', 'zhangjiakou', '1', '33218');
INSERT INTO `ota_district` VALUES ('33219', '鱼峰区', 'liuzhou', '1', '33219');
INSERT INTO `ota_district` VALUES ('33220', '江津区', 'chongqing_city', '1', '33220');
INSERT INTO `ota_district` VALUES ('33221', '鼓楼区', 'xuzhou', '1', '33221');
INSERT INTO `ota_district` VALUES ('33222', '正宁县', 'qingyang', '1', '33222');
INSERT INTO `ota_district` VALUES ('33223', '瀍河回族区', 'luoyang', '1', '33223');
INSERT INTO `ota_district` VALUES ('33225', '岳池县', 'guangan', '1', '33225');
INSERT INTO `ota_district` VALUES ('33226', '左权县', 'jinzhong', '1', '33226');
INSERT INTO `ota_district` VALUES ('33227', '达孜县', 'lasa', '1', '33227');
INSERT INTO `ota_district` VALUES ('33228', '门源回族自治县', 'haibei', '1', '33228');
INSERT INTO `ota_district` VALUES ('33229', '类乌齐县', 'changdu', '1', '33229');
INSERT INTO `ota_district` VALUES ('33230', '永靖县', 'linxia', '1', '33230');
INSERT INTO `ota_district` VALUES ('33231', '天全县', 'yaan', '1', '33231');
INSERT INTO `ota_district` VALUES ('33232', '容城县', 'baoding', '1', '33232');
INSERT INTO `ota_district` VALUES ('33233', '元坝区', 'guangyuan', '1', '33233');
INSERT INTO `ota_district` VALUES ('33234', '东区', 'panzhihua', '1', '33234');
INSERT INTO `ota_district` VALUES ('33235', '耿马傣族佤族自治县', 'lincang', '1', '33235');
INSERT INTO `ota_district` VALUES ('33236', '无棣县', 'binzhou', '1', '33236');
INSERT INTO `ota_district` VALUES ('33237', '河东区', 'tianjin_city', '1', '33237');
INSERT INTO `ota_district` VALUES ('33238', '麻章区', 'zhanjiang', '1', '33238');
INSERT INTO `ota_district` VALUES ('33239', '庄浪县', 'pingliang', '1', '33239');
INSERT INTO `ota_district` VALUES ('33240', '吴中区', 'suzhou_jiangsu', '1', '33240');
INSERT INTO `ota_district` VALUES ('33241', '崇明县', 'shanghai_city', '1', '33241');
INSERT INTO `ota_district` VALUES ('33242', '珠山区', 'jingdezhen', '1', '33242');
INSERT INTO `ota_district` VALUES ('33243', '米脂县', 'yulin_shanxi_02', '1', '33243');
INSERT INTO `ota_district` VALUES ('33244', '红花岗区', 'zunyi', '1', '33244');
INSERT INTO `ota_district` VALUES ('33245', '滨海县', 'yancheng', '1', '33245');
INSERT INTO `ota_district` VALUES ('33246', '全椒县', 'chuzhou', '1', '33246');
INSERT INTO `ota_district` VALUES ('33247', '盐山县', 'cangzhou', '1', '33247');
INSERT INTO `ota_district` VALUES ('33248', '延平区', 'nanping', '1', '33248');
INSERT INTO `ota_district` VALUES ('33249', '罗江县', 'deyang', '1', '33249');
INSERT INTO `ota_district` VALUES ('33250', '新邵县', 'shaoyang', '1', '33250');
INSERT INTO `ota_district` VALUES ('33251', '陆川县', 'yulin_guangxi', '1', '33251');
INSERT INTO `ota_district` VALUES ('33252', '怀安县', 'zhangjiakou', '1', '33252');
INSERT INTO `ota_district` VALUES ('33253', '红安县', 'huanggang', '1', '33253');
INSERT INTO `ota_district` VALUES ('33254', '梁园区', 'shangqiu', '1', '33254');
INSERT INTO `ota_district` VALUES ('33255', '巨鹿县', 'xingtai', '1', '33255');
INSERT INTO `ota_district` VALUES ('33256', '城厢区', 'putian', '1', '33256');
INSERT INTO `ota_district` VALUES ('33257', '和平区', 'tianjin_city', '1', '33257');
INSERT INTO `ota_district` VALUES ('33258', '满城县', 'baoding', '1', '33258');
INSERT INTO `ota_district` VALUES ('33259', '台儿庄区', 'zaozhuang', '1', '33259');
INSERT INTO `ota_district` VALUES ('33260', '青神县', 'meishan', '1', '33260');
INSERT INTO `ota_district` VALUES ('33261', '黄陵县', 'yanan', '1', '33261');
INSERT INTO `ota_district` VALUES ('33262', '长宁县', 'yibin', '1', '33262');
INSERT INTO `ota_district` VALUES ('33263', '坡头区', 'zhanjiang', '1', '33263');
INSERT INTO `ota_district` VALUES ('33264', '兴海县', 'hainanzangzu', '1', '33264');
INSERT INTO `ota_district` VALUES ('33265', '古田县', 'ningde', '1', '33265');
INSERT INTO `ota_district` VALUES ('33266', '沈丘县', 'zhoukou', '1', '33266');
INSERT INTO `ota_district` VALUES ('33267', '安吉县', 'huzhou', '1', '33267');
INSERT INTO `ota_district` VALUES ('33268', '涧西区', 'luoyang', '1', '33268');
INSERT INTO `ota_district` VALUES ('33269', '蓝田县', 'xian', '1', '33269');
INSERT INTO `ota_district` VALUES ('33270', '靖西县', 'baise', '1', '33270');
INSERT INTO `ota_district` VALUES ('33271', '久治县', 'guoluo', '1', '33271');
INSERT INTO `ota_district` VALUES ('33272', '剑阁县', 'guangyuan', '1', '33272');
INSERT INTO `ota_district` VALUES ('33273', '永定区', 'zhangjiajie', '1', '33273');
INSERT INTO `ota_district` VALUES ('33274', '绥滨县', 'hegang', '1', '33274');
INSERT INTO `ota_district` VALUES ('33275', '柳北区', 'liuzhou', '1', '33275');
INSERT INTO `ota_district` VALUES ('33276', '万州区', 'chongqing_city', '1', '33276');
INSERT INTO `ota_district` VALUES ('33277', '建始县', 'enshi', '1', '33277');
INSERT INTO `ota_district` VALUES ('33278', '兰考县', 'kaifeng', '1', '33278');
INSERT INTO `ota_district` VALUES ('33279', '阿鲁科尔沁旗', 'chifeng', '1', '33279');
INSERT INTO `ota_district` VALUES ('33280', '城中区', 'liuzhou', '1', '33280');
INSERT INTO `ota_district` VALUES ('33281', '林芝地区', 'linzhi', '1', '33281');
INSERT INTO `ota_district` VALUES ('33282', '红岗区', 'daqing', '1', '33282');
INSERT INTO `ota_district` VALUES ('33283', '海原县', 'zhongwei', '1', '33283');
INSERT INTO `ota_district` VALUES ('33284', '黄梅县', 'huanggang', '1', '33284');
INSERT INTO `ota_district` VALUES ('33285', '祁东县', 'hengyang', '1', '33285');
INSERT INTO `ota_district` VALUES ('33286', '古县', 'linfen', '1', '33286');
INSERT INTO `ota_district` VALUES ('33287', '惠阳区', 'huizhou_guangdong', '1', '33287');
INSERT INTO `ota_district` VALUES ('33288', '涉县', 'handan', '1', '33288');
INSERT INTO `ota_district` VALUES ('33289', '黄石港区', 'huangshi', '1', '33289');
INSERT INTO `ota_district` VALUES ('33290', '建华区', 'qiqihaer', '1', '33290');
INSERT INTO `ota_district` VALUES ('33291', '博湖县', 'bayinguoleng', '1', '33291');
INSERT INTO `ota_district` VALUES ('33292', '华容县', 'yueyang', '1', '33292');
INSERT INTO `ota_district` VALUES ('33293', '三元区', 'sanming', '1', '33293');
INSERT INTO `ota_district` VALUES ('33294', '绿园区', 'changchun', '1', '33294');
INSERT INTO `ota_district` VALUES ('33295', '浔阳区', 'jiujiang', '1', '33295');
INSERT INTO `ota_district` VALUES ('33296', '兴文县', 'yibin', '1', '33296');
INSERT INTO `ota_district` VALUES ('33297', '铁山港区', 'beihai', '1', '33297');
INSERT INTO `ota_district` VALUES ('33298', '若尔盖县', 'aba', '1', '33298');
INSERT INTO `ota_district` VALUES ('33299', '天河区', 'guangzhou', '1', '33299');
INSERT INTO `ota_district` VALUES ('33300', '桂东县', 'chenzhou', '1', '33300');
INSERT INTO `ota_district` VALUES ('33301', '罗山县', 'xinyang', '1', '33301');
INSERT INTO `ota_district` VALUES ('33302', '盘县', 'liupanshui', '1', '33302');
INSERT INTO `ota_district` VALUES ('33303', '五华县', 'meizhou', '1', '33303');
INSERT INTO `ota_district` VALUES ('33304', '宁蒗彝族自治县', 'lijiang', '1', '33304');
INSERT INTO `ota_district` VALUES ('33305', '微山县', 'jining', '1', '33305');
INSERT INTO `ota_district` VALUES ('33306', '临澧县', 'changde', '1', '33306');
INSERT INTO `ota_district` VALUES ('33307', '巴青县', 'naqu', '1', '33307');
INSERT INTO `ota_district` VALUES ('33308', '盐源县', 'liangshan', '1', '33308');
INSERT INTO `ota_district` VALUES ('33309', '西安区', 'liaoyuan', '1', '33309');
INSERT INTO `ota_district` VALUES ('33310', '海淀区', 'beijing_city', '1', '33310');
INSERT INTO `ota_district` VALUES ('33311', '呈贡县', 'kunming', '1', '33311');
INSERT INTO `ota_district` VALUES ('33312', '昭阳区', 'zhaotong', '1', '33312');
INSERT INTO `ota_district` VALUES ('33313', '安图县', 'yanbian', '1', '33313');
INSERT INTO `ota_district` VALUES ('33314', '洮北区', 'baicheng', '1', '33314');
INSERT INTO `ota_district` VALUES ('33315', '罗平县', 'qujing', '1', '33315');
INSERT INTO `ota_district` VALUES ('33316', '东兴区', 'neijiang', '1', '33316');
INSERT INTO `ota_district` VALUES ('33317', '肃州区', 'jiuquan', '1', '33317');
INSERT INTO `ota_district` VALUES ('33318', '上饶县', 'shangrao', '1', '33318');
INSERT INTO `ota_district` VALUES ('33319', '叶城县', 'kashi', '1', '33319');
INSERT INTO `ota_district` VALUES ('33320', '佛冈县', 'qingyuan', '1', '33320');
INSERT INTO `ota_district` VALUES ('33321', '昌图县', 'tieling', '1', '33321');
INSERT INTO `ota_district` VALUES ('33322', '清水河县', 'huhehaote', '1', '33322');
INSERT INTO `ota_district` VALUES ('33323', '资中县', 'neijiang', '1', '33323');
INSERT INTO `ota_district` VALUES ('33324', '元江哈尼族彝族傣族自治县', 'yuxi', '1', '33324');
INSERT INTO `ota_district` VALUES ('33325', '措美县', 'shannan', '1', '33325');
INSERT INTO `ota_district` VALUES ('33326', '密云县', 'beijing_city', '1', '33326');
INSERT INTO `ota_district` VALUES ('33327', '平果县', 'baise', '1', '33327');
INSERT INTO `ota_district` VALUES ('33328', '原州区', 'guyuan', '1', '33328');
INSERT INTO `ota_district` VALUES ('33329', '静安区', 'shanghai_city', '1', '33329');
INSERT INTO `ota_district` VALUES ('33330', '塔城地区', 'tacheng', '1', '33330');
INSERT INTO `ota_district` VALUES ('33331', '正安县', 'zunyi', '1', '33331');
INSERT INTO `ota_district` VALUES ('33332', '西双版纳傣族自治州', 'xishuangbanna', '1', '33332');
INSERT INTO `ota_district` VALUES ('33333', '玄武区', 'nanjing', '1', '33333');
INSERT INTO `ota_district` VALUES ('33334', '盘山县', 'panjin', '1', '33334');
INSERT INTO `ota_district` VALUES ('33335', '武昌区', 'wuhan', '1', '33335');
INSERT INTO `ota_district` VALUES ('33336', '泉港区', 'quanzhou', '1', '33336');
INSERT INTO `ota_district` VALUES ('33337', '广陵区', 'yangzhou', '1', '33337');
INSERT INTO `ota_district` VALUES ('33338', '开封县', 'kaifeng', '1', '33338');
INSERT INTO `ota_district` VALUES ('33339', '洞口县', 'shaoyang', '1', '33339');
INSERT INTO `ota_district` VALUES ('33340', '梨树县', 'siping', '1', '33340');
INSERT INTO `ota_district` VALUES ('33341', '碌曲县', 'gannan', '1', '33341');
INSERT INTO `ota_district` VALUES ('33342', '策勒县', 'hetian', '1', '33342');
INSERT INTO `ota_district` VALUES ('33343', '正阳县', 'zhumadian', '1', '33343');
INSERT INTO `ota_district` VALUES ('33344', '凌河区', 'jinzhou', '1', '33344');
INSERT INTO `ota_district` VALUES ('33345', '武鸣县', 'nanning', '1', '33345');
INSERT INTO `ota_district` VALUES ('33346', '濠江区', 'shantou', '1', '33346');
INSERT INTO `ota_district` VALUES ('33347', '龙亭区', 'kaifeng', '1', '33347');
INSERT INTO `ota_district` VALUES ('33348', '礼泉县', 'xianyang', '1', '33348');
INSERT INTO `ota_district` VALUES ('33349', '曹县', 'heze', '1', '33349');
INSERT INTO `ota_district` VALUES ('33350', '东宁县', 'mudanjiang', '1', '33350');
INSERT INTO `ota_district` VALUES ('33351', '集宁区', 'wulanchabu', '1', '33351');
INSERT INTO `ota_district` VALUES ('33352', '内乡县', 'nanyang', '1', '33352');
INSERT INTO `ota_district` VALUES ('33353', '桥东区', 'shijiazhuang', '1', '33353');
INSERT INTO `ota_district` VALUES ('33354', '运河区', 'cangzhou', '1', '33354');
INSERT INTO `ota_district` VALUES ('33355', '淇滨区', 'hebi', '1', '33355');
INSERT INTO `ota_district` VALUES ('33356', '那曲县', 'naqu', '1', '33356');
INSERT INTO `ota_district` VALUES ('33357', '始兴县', 'shaoguan', '1', '33357');
INSERT INTO `ota_district` VALUES ('33358', '巴州区', 'bazhong', '1', '33358');
INSERT INTO `ota_district` VALUES ('33359', '清苑县', 'baoding', '1', '33359');
INSERT INTO `ota_district` VALUES ('33360', '宁强县', 'hanzhong', '1', '33360');
INSERT INTO `ota_district` VALUES ('33361', '祁阳县', 'yongzhou', '1', '33361');
INSERT INTO `ota_district` VALUES ('33362', '朝阳区', 'changchun', '1', '33362');
INSERT INTO `ota_district` VALUES ('33363', '民乐县', 'zhangye', '1', '33363');
INSERT INTO `ota_district` VALUES ('33364', '青浦区', 'shanghai_city', '1', '33364');
INSERT INTO `ota_district` VALUES ('33365', '万山特区', 'tongren', '1', '33365');
INSERT INTO `ota_district` VALUES ('33366', '开县', 'chongqing_city', '1', '33366');
INSERT INTO `ota_district` VALUES ('33367', '黎平县', 'qiandongnan', '1', '33367');
INSERT INTO `ota_district` VALUES ('33368', '肃宁县', 'cangzhou', '1', '33368');
INSERT INTO `ota_district` VALUES ('33369', '平江区', 'suzhou_jiangsu', '1', '33369');
INSERT INTO `ota_district` VALUES ('33370', '乐亭县', 'tangshan', '1', '33370');
INSERT INTO `ota_district` VALUES ('33371', '平昌县', 'bazhong', '1', '33371');
INSERT INTO `ota_district` VALUES ('33372', '吉隆县', 'rikaze', '1', '33372');
INSERT INTO `ota_district` VALUES ('33373', '科尔沁左翼后旗', 'tongliao', '1', '33373');
INSERT INTO `ota_district` VALUES ('33374', '三门县', 'taizhou_zhejiang', '1', '33374');
INSERT INTO `ota_district` VALUES ('33375', '依安县', 'qiqihaer', '1', '33375');
INSERT INTO `ota_district` VALUES ('33376', '德化县', 'quanzhou', '1', '33376');
INSERT INTO `ota_district` VALUES ('33377', '镇原县', 'qingyang', '1', '33377');
INSERT INTO `ota_district` VALUES ('33378', '沙河口区', 'dalian', '1', '33378');
INSERT INTO `ota_district` VALUES ('33379', '沿河土家族自治县', 'tongren', '1', '33379');
INSERT INTO `ota_district` VALUES ('33380', '桓仁满族自治县', 'benxi', '1', '33380');
INSERT INTO `ota_district` VALUES ('33381', '鹿邑县', 'zhoukou', '1', '33381');
INSERT INTO `ota_district` VALUES ('33382', '东风区', 'jiamusi', '1', '33382');
INSERT INTO `ota_district` VALUES ('33383', '源汇区', 'luohe', '1', '33383');
INSERT INTO `ota_district` VALUES ('33384', '哈巴河县', 'aletai', '1', '33384');
INSERT INTO `ota_district` VALUES ('33385', '江北区', 'chongqing_city', '1', '33385');
INSERT INTO `ota_district` VALUES ('33386', '迎泽区', 'taiyuan', '1', '33386');
INSERT INTO `ota_district` VALUES ('33387', '周至县', 'xian', '1', '33387');
INSERT INTO `ota_district` VALUES ('33388', '惠济区', 'zhengzhou', '1', '33388');
INSERT INTO `ota_district` VALUES ('33389', '道里区', 'haerbin', '1', '33389');
INSERT INTO `ota_district` VALUES ('33390', '綦江县', 'chongqing_city', '1', '33390');
INSERT INTO `ota_district` VALUES ('33391', '象山区', 'guilin', '1', '33391');
INSERT INTO `ota_district` VALUES ('33392', '沂水县', 'linyi', '1', '33392');
INSERT INTO `ota_district` VALUES ('33393', '阳城县', 'jincheng', '1', '33393');
INSERT INTO `ota_district` VALUES ('33394', '富源县', 'qujing', '1', '33394');
INSERT INTO `ota_district` VALUES ('33395', '黄岛区', 'qingdao', '1', '33395');
INSERT INTO `ota_district` VALUES ('33396', '嘉禾县', 'chenzhou', '1', '33396');
INSERT INTO `ota_district` VALUES ('33397', '榕江县', 'qiandongnan', '1', '33397');
INSERT INTO `ota_district` VALUES ('33398', '民权县', 'shangqiu', '1', '33398');
INSERT INTO `ota_district` VALUES ('33399', '东至县', 'chizhou', '1', '33399');
INSERT INTO `ota_district` VALUES ('33400', '苍梧县', 'wuzhou', '1', '33400');
INSERT INTO `ota_district` VALUES ('33401', '普兰县', 'ali', '1', '33401');
INSERT INTO `ota_district` VALUES ('33402', '广德县', 'xuancheng', '1', '33402');
INSERT INTO `ota_district` VALUES ('33403', '宝应县', 'yangzhou', '1', '33403');
INSERT INTO `ota_district` VALUES ('33404', '新华区', 'shijiazhuang', '1', '33404');
INSERT INTO `ota_district` VALUES ('33405', '徽州区', 'huangshan', '1', '33405');
INSERT INTO `ota_district` VALUES ('33406', '上栗县', 'pingxiang', '1', '33406');
INSERT INTO `ota_district` VALUES ('33407', '郫县', 'chengdu', '1', '33407');
INSERT INTO `ota_district` VALUES ('33408', '开平区', 'tangshan', '1', '33408');
INSERT INTO `ota_district` VALUES ('33409', '岳西县', 'anqing', '1', '33409');
INSERT INTO `ota_district` VALUES ('33410', '西城区', 'beijing_city', '1', '33410');
INSERT INTO `ota_district` VALUES ('33411', '饶阳县', 'hengshui', '1', '33411');
INSERT INTO `ota_district` VALUES ('33412', '西秀区', 'anshun', '1', '33412');
INSERT INTO `ota_district` VALUES ('33413', '静宁县', 'pingliang', '1', '33413');
INSERT INTO `ota_district` VALUES ('33414', '伊吾县', 'hami', '1', '33414');
INSERT INTO `ota_district` VALUES ('33415', '乾县', 'xianyang', '1', '33415');
INSERT INTO `ota_district` VALUES ('33416', '雁塔区', 'xian', '1', '33416');
INSERT INTO `ota_district` VALUES ('33417', '铁山区', 'huangshi', '1', '33417');
INSERT INTO `ota_district` VALUES ('33418', '方正县', 'haerbin', '1', '33418');
INSERT INTO `ota_district` VALUES ('33419', '西安区', 'mudanjiang', '1', '33419');
INSERT INTO `ota_district` VALUES ('33420', '同安区', 'xiamen', '1', '33420');
INSERT INTO `ota_district` VALUES ('33421', '蕉城区', 'ningde', '1', '33421');
INSERT INTO `ota_district` VALUES ('33422', '秦都区', 'xianyang', '1', '33422');
INSERT INTO `ota_district` VALUES ('33423', '江洲区', 'chongzuo', '1', '33423');
INSERT INTO `ota_district` VALUES ('33424', '孝南区', 'xiaogan', '1', '33424');
INSERT INTO `ota_district` VALUES ('33425', '沙坡头区', 'zhongwei', '1', '33425');
INSERT INTO `ota_district` VALUES ('33426', '嘉黎县', 'naqu', '1', '33426');
INSERT INTO `ota_district` VALUES ('33427', '丹凤县', 'shangluo', '1', '33427');
INSERT INTO `ota_district` VALUES ('33428', '杨浦区', 'shanghai_city', '1', '33428');
INSERT INTO `ota_district` VALUES ('33429', '衡南县', 'hengyang', '1', '33429');
INSERT INTO `ota_district` VALUES ('33430', '曲江区', 'shaoguan', '1', '33430');
INSERT INTO `ota_district` VALUES ('33431', '进贤县', 'nanchang', '1', '33431');
INSERT INTO `ota_district` VALUES ('33432', '南岸区', 'chongqing_city', '1', '33432');
INSERT INTO `ota_district` VALUES ('33433', '达拉特旗', 'eerduosi', '1', '33433');
INSERT INTO `ota_district` VALUES ('33434', '望江县', 'anqing', '1', '33434');
INSERT INTO `ota_district` VALUES ('33435', '芷江侗族自治县', 'huaihua', '1', '33435');
INSERT INTO `ota_district` VALUES ('33436', '马尔康县', 'aba', '1', '33436');
INSERT INTO `ota_district` VALUES ('33437', '港闸区', 'nantong', '1', '33437');
INSERT INTO `ota_district` VALUES ('33438', '安塞县', 'yanan', '1', '33438');
INSERT INTO `ota_district` VALUES ('33439', '通化县', 'tonghua', '1', '33439');
INSERT INTO `ota_district` VALUES ('33440', '大关县', 'zhaotong', '1', '33440');
INSERT INTO `ota_district` VALUES ('33441', '留坝县', 'hanzhong', '1', '33441');
INSERT INTO `ota_district` VALUES ('33442', '下花园区', 'zhangjiakou', '1', '33442');
INSERT INTO `ota_district` VALUES ('33443', '四方区', 'qingdao', '1', '33443');
INSERT INTO `ota_district` VALUES ('33444', '寿宁县', 'ningde', '1', '33444');
INSERT INTO `ota_district` VALUES ('33445', '金平区', 'shantou', '1', '33445');
INSERT INTO `ota_district` VALUES ('33446', '九江县', 'jiujiang', '1', '33446');
INSERT INTO `ota_district` VALUES ('33447', '曲沃县', 'linfen', '1', '33447');
INSERT INTO `ota_district` VALUES ('33448', '梓潼县', 'mianyang', '1', '33448');
INSERT INTO `ota_district` VALUES ('33449', '青冈县', 'suihua', '1', '33449');
INSERT INTO `ota_district` VALUES ('33450', '徐汇区', 'shanghai_city', '1', '33450');
INSERT INTO `ota_district` VALUES ('33451', '尉犁县', 'bayinguoleng', '1', '33451');
INSERT INTO `ota_district` VALUES ('33452', '新蔡县', 'zhumadian', '1', '33452');
INSERT INTO `ota_district` VALUES ('33453', '乡城县', 'ganzi', '1', '33453');
INSERT INTO `ota_district` VALUES ('33454', '尖扎县', 'huangnan', '1', '33454');
INSERT INTO `ota_district` VALUES ('33455', '当雄县', 'lasa', '1', '33455');
INSERT INTO `ota_district` VALUES ('33456', '临夏县', 'linxia', '1', '33456');
INSERT INTO `ota_district` VALUES ('33457', '务川仡佬族苗族自治县', 'zunyi', '1', '33457');
INSERT INTO `ota_district` VALUES ('33458', '天元区', 'zhuzhou', '1', '33458');
INSERT INTO `ota_district` VALUES ('33459', '华坪县', 'lijiang', '1', '33459');
INSERT INTO `ota_district` VALUES ('33461', '改则县', 'ali', '1', '33461');
INSERT INTO `ota_district` VALUES ('33462', '罗田县', 'huanggang', '1', '33462');
INSERT INTO `ota_district` VALUES ('33463', '玉屏侗族自治县', 'tongren', '1', '33463');
INSERT INTO `ota_district` VALUES ('33464', '海州区', 'lianyungang', '1', '33464');
INSERT INTO `ota_district` VALUES ('33465', '溆浦县', 'huaihua', '1', '33465');
INSERT INTO `ota_district` VALUES ('33466', '冕宁县', 'liangshan', '1', '33466');
INSERT INTO `ota_district` VALUES ('33467', '永定县', 'longyan', '1', '33467');
INSERT INTO `ota_district` VALUES ('33468', '湛河区', 'pingdingshan', '1', '33468');
INSERT INTO `ota_district` VALUES ('33469', '沅陵县', 'huaihua', '1', '33469');
INSERT INTO `ota_district` VALUES ('33470', '历城区', 'jinan', '1', '33470');
INSERT INTO `ota_district` VALUES ('33471', '黎城县', 'changzhi', '1', '33471');
INSERT INTO `ota_district` VALUES ('33472', '莲都区', 'lishui', '1', '33472');
INSERT INTO `ota_district` VALUES ('33473', '米易县', 'panzhihua', '1', '33473');
INSERT INTO `ota_district` VALUES ('33474', '承德县', 'chengde', '1', '33474');
INSERT INTO `ota_district` VALUES ('33475', '娄星区', 'loudi', '1', '33475');
INSERT INTO `ota_district` VALUES ('33476', '东辽县', 'liaoyuan', '1', '33476');
INSERT INTO `ota_district` VALUES ('33477', '宁江区', 'songyuan', '1', '33477');
INSERT INTO `ota_district` VALUES ('33478', '尚义县', 'zhangjiakou', '1', '33478');
INSERT INTO `ota_district` VALUES ('33479', '江海区', 'jiangmen', '1', '33479');
INSERT INTO `ota_district` VALUES ('33480', '宽甸满族自治县', 'dandong', '1', '33480');
INSERT INTO `ota_district` VALUES ('33481', '铜鼓县', 'yichun_jiangxi', '1', '33481');
INSERT INTO `ota_district` VALUES ('33482', '勉县', 'hanzhong', '1', '33482');
INSERT INTO `ota_district` VALUES ('33483', '衡东县', 'hengyang', '1', '33483');
INSERT INTO `ota_district` VALUES ('33484', '望都县', 'baoding', '1', '33484');
INSERT INTO `ota_district` VALUES ('33485', '回民区', 'huhehaote', '1', '33485');
INSERT INTO `ota_district` VALUES ('33486', '云县', 'lincang', '1', '33486');
INSERT INTO `ota_district` VALUES ('33487', '鄄城县', 'heze', '1', '33487');
INSERT INTO `ota_district` VALUES ('33488', '盐边县', 'panzhihua', '1', '33488');
INSERT INTO `ota_district` VALUES ('33489', '卢龙县', 'qinhuangdao', '1', '33489');
INSERT INTO `ota_district` VALUES ('33490', '古浪县', 'wuwei', '1', '33490');
INSERT INTO `ota_district` VALUES ('33491', '东洲区', 'fushun', '1', '33491');
INSERT INTO `ota_district` VALUES ('33492', '定襄县', 'xinzhou', '1', '33492');
INSERT INTO `ota_district` VALUES ('33493', '独山县', 'qiannan', '1', '33493');
INSERT INTO `ota_district` VALUES ('33494', '乌审旗', 'eerduosi', '1', '33494');
INSERT INTO `ota_district` VALUES ('33495', '师宗县', 'qujing', '1', '33495');
INSERT INTO `ota_district` VALUES ('33496', '文山县', 'wenshan', '1', '33496');
INSERT INTO `ota_district` VALUES ('33497', '石渠县', 'ganzi', '1', '33497');
INSERT INTO `ota_district` VALUES ('33498', '甘德县', 'guoluo', '1', '33498');
INSERT INTO `ota_district` VALUES ('33499', '潮阳区', 'shantou', '1', '33499');
INSERT INTO `ota_district` VALUES ('33500', '永德县', 'lincang', '1', '33500');
INSERT INTO `ota_district` VALUES ('33501', '兴隆县', 'chengde', '1', '33501');
INSERT INTO `ota_district` VALUES ('33502', '安泽县', 'linfen', '1', '33502');
INSERT INTO `ota_district` VALUES ('33503', '邕宁区', 'nanning', '1', '33503');
INSERT INTO `ota_district` VALUES ('33504', '顺昌县', 'nanping', '1', '33504');
INSERT INTO `ota_district` VALUES ('33505', '梅列区', 'sanming', '1', '33505');
INSERT INTO `ota_district` VALUES ('33506', '蓬安县', 'nanchong', '1', '33506');
INSERT INTO `ota_district` VALUES ('33507', '石楼县', 'lvliang', '1', '33507');
INSERT INTO `ota_district` VALUES ('33508', '定远县', 'chuzhou', '1', '33508');
INSERT INTO `ota_district` VALUES ('33509', '徽县', 'longnan', '1', '33509');
INSERT INTO `ota_district` VALUES ('33510', '松桃苗族自治县', 'tongren', '1', '33510');
INSERT INTO `ota_district` VALUES ('33511', '庐山区', 'jiujiang', '1', '33511');
INSERT INTO `ota_district` VALUES ('33512', '眉县', 'baoji', '1', '33512');
INSERT INTO `ota_district` VALUES ('33513', '镇平县', 'nanyang', '1', '33513');
INSERT INTO `ota_district` VALUES ('33514', '清河区', 'huaian', '1', '33514');
INSERT INTO `ota_district` VALUES ('33515', '武都区', 'longnan', '1', '33515');
INSERT INTO `ota_district` VALUES ('33516', '含山县', 'chaohu', '1', '33516');
INSERT INTO `ota_district` VALUES ('33517', '井陉矿区', 'shijiazhuang', '1', '33517');
INSERT INTO `ota_district` VALUES ('33518', '平和县', 'zhangzhou', '1', '33518');
INSERT INTO `ota_district` VALUES ('33519', '鸡冠区', 'jixi', '1', '33519');
INSERT INTO `ota_district` VALUES ('33520', '大兴区', 'beijing_city', '1', '33520');
INSERT INTO `ota_district` VALUES ('33521', '砚山县', 'wenshan', '1', '33521');
INSERT INTO `ota_district` VALUES ('33522', '平远县', 'meizhou', '1', '33522');
INSERT INTO `ota_district` VALUES ('33523', '尼玛县', 'naqu', '1', '33523');
INSERT INTO `ota_district` VALUES ('33524', '蕲春县', 'huanggang', '1', '33524');
INSERT INTO `ota_district` VALUES ('33525', '金川区', 'jinchang', '1', '33525');
INSERT INTO `ota_district` VALUES ('33526', '玉环县', 'taizhou_zhejiang', '1', '33526');
INSERT INTO `ota_district` VALUES ('33527', '石景山区', 'beijing_city', '1', '33527');
INSERT INTO `ota_district` VALUES ('33528', '黄陂区', 'wuhan', '1', '33528');
INSERT INTO `ota_district` VALUES ('33529', '平遥县', 'jinzhong', '1', '33529');
INSERT INTO `ota_district` VALUES ('33530', '临武县', 'chenzhou', '1', '33530');
INSERT INTO `ota_district` VALUES ('33531', '武侯区', 'chengdu', '1', '33531');
INSERT INTO `ota_district` VALUES ('33532', '镇远县', 'qiandongnan', '1', '33532');
INSERT INTO `ota_district` VALUES ('33533', '象州县', 'laibin', '1', '33533');
INSERT INTO `ota_district` VALUES ('33534', '元谋县', 'chuxiong', '1', '33534');
INSERT INTO `ota_district` VALUES ('33535', '新宾满族自治县', 'fushun', '1', '33535');
INSERT INTO `ota_district` VALUES ('33536', '蒙城县', 'bozhou', '1', '33536');
INSERT INTO `ota_district` VALUES ('33537', '汾西县', 'linfen', '1', '33537');
INSERT INTO `ota_district` VALUES ('33538', '歙县', 'huangshan', '1', '33538');
INSERT INTO `ota_district` VALUES ('33539', '禄劝彝族苗族自治县', 'kunming', '1', '33539');
INSERT INTO `ota_district` VALUES ('33540', '井陉县', 'shijiazhuang', '1', '33540');
INSERT INTO `ota_district` VALUES ('33541', '武宁县', 'jiujiang', '1', '33541');
INSERT INTO `ota_district` VALUES ('33542', '山城区', 'hebi', '1', '33542');
INSERT INTO `ota_district` VALUES ('33543', '马村区', 'jiaozuo', '1', '33543');
INSERT INTO `ota_district` VALUES ('33544', '信州区', 'shangrao', '1', '33544');
INSERT INTO `ota_district` VALUES ('33545', '吉县', 'linfen', '1', '33545');
INSERT INTO `ota_district` VALUES ('33546', '清河区', 'tieling', '1', '33546');
INSERT INTO `ota_district` VALUES ('33547', '铁锋区', 'qiqihaer', '1', '33547');
INSERT INTO `ota_district` VALUES ('33548', '怒江傈僳族自治州', 'nujiang', '1', '33548');
INSERT INTO `ota_district` VALUES ('33549', '乐东黎族自治县', 'ledong', '1', '33549');
INSERT INTO `ota_district` VALUES ('33550', '西林县', 'baise', '1', '33550');
INSERT INTO `ota_district` VALUES ('33551', '西丰县', 'tieling', '1', '33551');
INSERT INTO `ota_district` VALUES ('33552', '通州区', 'beijing_city', '1', '33552');
INSERT INTO `ota_district` VALUES ('33553', '布尔津县', 'aletai', '1', '33553');
INSERT INTO `ota_district` VALUES ('33554', '勐腊县', 'xishuangbanna', '1', '33554');
INSERT INTO `ota_district` VALUES ('33555', '修武县', 'jiaozuo', '1', '33555');
INSERT INTO `ota_district` VALUES ('33556', '湟中县', 'xining', '1', '33556');
INSERT INTO `ota_district` VALUES ('33557', '新宁县', 'shaoyang', '1', '33557');
INSERT INTO `ota_district` VALUES ('33558', '申扎县', 'naqu', '1', '33558');
INSERT INTO `ota_district` VALUES ('33559', '循化撒拉族自治县', 'haidong', '1', '33559');
INSERT INTO `ota_district` VALUES ('33560', '蓝山县', 'yongzhou', '1', '33560');
INSERT INTO `ota_district` VALUES ('33561', '临邑县', 'dezhou', '1', '33561');
INSERT INTO `ota_district` VALUES ('33562', '灌云县', 'lianyungang', '1', '33562');
INSERT INTO `ota_district` VALUES ('33563', '彭阳县', 'guyuan', '1', '33563');
INSERT INTO `ota_district` VALUES ('33564', '多伦县', 'xilinguole', '1', '33564');
INSERT INTO `ota_district` VALUES ('33565', '常山县', 'quzhou', '1', '33565');
INSERT INTO `ota_district` VALUES ('33566', '忻府区', 'xinzhou', '1', '33566');
INSERT INTO `ota_district` VALUES ('33567', '色达县', 'ganzi', '1', '33567');
INSERT INTO `ota_district` VALUES ('33568', '奉节县', 'chongqing_city', '1', '33568');
INSERT INTO `ota_district` VALUES ('33569', '崇川区', 'nantong', '1', '33569');
INSERT INTO `ota_district` VALUES ('33570', '龙城区', 'chaoyang', '1', '33570');
INSERT INTO `ota_district` VALUES ('33571', '凤冈县', 'zunyi', '1', '33571');
INSERT INTO `ota_district` VALUES ('33572', '鹤峰县', 'enshi', '1', '33572');
INSERT INTO `ota_district` VALUES ('33573', '泸定县', 'ganzi', '1', '33573');
INSERT INTO `ota_district` VALUES ('33574', '隆子县', 'shannan', '1', '33574');
INSERT INTO `ota_district` VALUES ('33575', '广宁县', 'zhaoqing', '1', '33575');
INSERT INTO `ota_district` VALUES ('33576', '庆安县', 'suihua', '1', '33576');
INSERT INTO `ota_district` VALUES ('33577', '端州区', 'zhaoqing', '1', '33577');
INSERT INTO `ota_district` VALUES ('33578', '渝水区', 'xinyu', '1', '33578');
INSERT INTO `ota_district` VALUES ('33579', '湖口县', 'jiujiang', '1', '33579');
INSERT INTO `ota_district` VALUES ('33580', '新巴尔虎右旗', 'hulunbeier', '1', '33580');
INSERT INTO `ota_district` VALUES ('33581', '上杭县', 'longyan', '1', '33581');
INSERT INTO `ota_district` VALUES ('33582', '农安县', 'changchun', '1', '33582');
INSERT INTO `ota_district` VALUES ('33583', '金川县', 'aba', '1', '33583');
INSERT INTO `ota_district` VALUES ('33584', '临夏回族自治州', 'linxia', '1', '33584');
INSERT INTO `ota_district` VALUES ('33585', '额敏县', 'tacheng', '1', '33585');
INSERT INTO `ota_district` VALUES ('33586', '东丽区', 'tianjin_city', '1', '33586');
INSERT INTO `ota_district` VALUES ('33587', '定日县', 'rikaze', '1', '33587');
INSERT INTO `ota_district` VALUES ('33588', '玛多县', 'guoluo', '1', '33588');
INSERT INTO `ota_district` VALUES ('33589', '东兰县', 'hechi', '1', '33589');
INSERT INTO `ota_district` VALUES ('33590', '山阴县', 'shuozhou', '1', '33590');
INSERT INTO `ota_district` VALUES ('33591', '张北县', 'zhangjiakou', '1', '33591');
INSERT INTO `ota_district` VALUES ('33592', '灵寿县', 'shijiazhuang', '1', '33592');
INSERT INTO `ota_district` VALUES ('33593', '滑县', 'anyang', '1', '33593');
INSERT INTO `ota_district` VALUES ('33594', '通河县', 'haerbin', '1', '33594');
INSERT INTO `ota_district` VALUES ('33595', '太和区', 'jinzhou', '1', '33595');
INSERT INTO `ota_district` VALUES ('33596', '镇安县', 'shangluo', '1', '33596');
INSERT INTO `ota_district` VALUES ('33597', '广安区', 'guangan', '1', '33597');
INSERT INTO `ota_district` VALUES ('33598', '青龙满族自治县', 'qinhuangdao', '1', '33598');
INSERT INTO `ota_district` VALUES ('33599', '叠彩区', 'guilin', '1', '33599');
INSERT INTO `ota_district` VALUES ('33600', '洛宁县', 'luoyang', '1', '33600');
INSERT INTO `ota_district` VALUES ('33601', '肃南裕固族自治县', 'zhangye', '1', '33601');
INSERT INTO `ota_district` VALUES ('33602', '北川羌族自治县', 'mianyang', '1', '33602');
INSERT INTO `ota_district` VALUES ('33603', '鸠江区', 'wuhu', '1', '33603');
INSERT INTO `ota_district` VALUES ('33604', '公安县', 'jingzhou', '1', '33604');
INSERT INTO `ota_district` VALUES ('33605', '桥西区', 'shijiazhuang', '1', '33605');
INSERT INTO `ota_district` VALUES ('33606', '高阳县', 'baoding', '1', '33606');
INSERT INTO `ota_district` VALUES ('33607', '漠河县', 'daxinganling', '1', '33607');
INSERT INTO `ota_district` VALUES ('33608', '惠山区', 'wuxi', '1', '33608');
INSERT INTO `ota_district` VALUES ('33609', '让胡路区', 'daqing', '1', '33609');
INSERT INTO `ota_district` VALUES ('33610', '嘉定区', 'shanghai_city', '1', '33610');
INSERT INTO `ota_district` VALUES ('33611', '峨边彝族自治县', 'leshan', '1', '33611');
INSERT INTO `ota_district` VALUES ('33612', '卫东区', 'pingdingshan', '1', '33612');
INSERT INTO `ota_district` VALUES ('33613', '蓬溪县', 'suining', '1', '33613');
INSERT INTO `ota_district` VALUES ('33614', '新河县', 'xingtai', '1', '33614');
INSERT INTO `ota_district` VALUES ('33615', '吴堡县', 'yulin_shanxi_02', '1', '33615');
INSERT INTO `ota_district` VALUES ('33616', '银州区', 'tieling', '1', '33616');
INSERT INTO `ota_district` VALUES ('33617', '喀喇沁旗', 'chifeng', '1', '33617');
INSERT INTO `ota_district` VALUES ('33618', '磴口县', 'bayannaoer', '1', '33618');
INSERT INTO `ota_district` VALUES ('33619', '铁岭县', 'tieling', '1', '33619');
INSERT INTO `ota_district` VALUES ('33620', '武江区', 'shaoguan', '1', '33620');
INSERT INTO `ota_district` VALUES ('33621', '昌都地区', 'changdu', '1', '33621');
INSERT INTO `ota_district` VALUES ('33622', '蔚县', 'zhangjiakou', '1', '33622');
INSERT INTO `ota_district` VALUES ('33623', '瑶海区', 'hefei', '1', '33623');
INSERT INTO `ota_district` VALUES ('33624', '江达县', 'changdu', '1', '33624');
INSERT INTO `ota_district` VALUES ('33625', '汤原县', 'jiamusi', '1', '33625');
INSERT INTO `ota_district` VALUES ('33626', '白云区', 'guiyang', '1', '33626');
INSERT INTO `ota_district` VALUES ('33627', '萧山区', 'hangzhou', '1', '33627');
INSERT INTO `ota_district` VALUES ('33628', '肥乡县', 'handan', '1', '33628');
INSERT INTO `ota_district` VALUES ('33629', '甘洛县', 'liangshan', '1', '33629');
INSERT INTO `ota_district` VALUES ('33630', '鸡泽县', 'handan', '1', '33630');
INSERT INTO `ota_district` VALUES ('33631', '中牟县', 'zhengzhou', '1', '33631');
INSERT INTO `ota_district` VALUES ('33632', '丰南区', 'tangshan', '1', '33632');
INSERT INTO `ota_district` VALUES ('33633', '昂仁县', 'rikaze', '1', '33633');
INSERT INTO `ota_district` VALUES ('33634', '辉南县', 'tonghua', '1', '33634');
INSERT INTO `ota_district` VALUES ('33635', '旅顺口区', 'dalian', '1', '33635');
INSERT INTO `ota_district` VALUES ('33636', '云城区', 'yunfu', '1', '33636');
INSERT INTO `ota_district` VALUES ('33637', '岳阳楼区', 'yueyang', '1', '33637');
INSERT INTO `ota_district` VALUES ('33638', '汇川区', 'zunyi', '1', '33638');
INSERT INTO `ota_district` VALUES ('33639', '柳河县', 'tonghua', '1', '33639');
INSERT INTO `ota_district` VALUES ('33640', '泸西县', 'honghe', '1', '33640');
INSERT INTO `ota_district` VALUES ('33641', '临渭区', 'weinan', '1', '33641');
INSERT INTO `ota_district` VALUES ('33642', '临沭县', 'linyi', '1', '33642');
INSERT INTO `ota_district` VALUES ('33643', '仁和区', 'panzhihua', '1', '33643');
INSERT INTO `ota_district` VALUES ('33644', '象山县', 'ningbo', '1', '33644');
INSERT INTO `ota_district` VALUES ('33645', '西乡县', 'hanzhong', '1', '33645');
INSERT INTO `ota_district` VALUES ('33646', '博兴县', 'binzhou', '1', '33646');
INSERT INTO `ota_district` VALUES ('33647', '乌兰县', 'haixi', '1', '33647');
INSERT INTO `ota_district` VALUES ('33648', '柳林县', 'lvliang', '1', '33648');
INSERT INTO `ota_district` VALUES ('33649', '大宁县', 'linfen', '1', '33649');
INSERT INTO `ota_district` VALUES ('33650', '隆昌县', 'neijiang', '1', '33650');
INSERT INTO `ota_district` VALUES ('33651', '拱墅区', 'hangzhou', '1', '33651');
INSERT INTO `ota_district` VALUES ('33652', '政和县', 'nanping', '1', '33652');
INSERT INTO `ota_district` VALUES ('33653', '茂港区', 'maoming', '1', '33653');
INSERT INTO `ota_district` VALUES ('33654', '玛曲县', 'gannan', '1', '33654');
INSERT INTO `ota_district` VALUES ('33655', '晴隆县', 'qianxinan', '1', '33655');
INSERT INTO `ota_district` VALUES ('33656', '南关区', 'changchun', '1', '33656');
INSERT INTO `ota_district` VALUES ('33657', '荥经县', 'yaan', '1', '33657');
INSERT INTO `ota_district` VALUES ('33658', '金阳县', 'liangshan', '1', '33658');
INSERT INTO `ota_district` VALUES ('33659', '乌鲁木齐县', 'wulumuqi', '1', '33659');
INSERT INTO `ota_district` VALUES ('33660', '岭东区', 'shuangyashan', '1', '33660');
INSERT INTO `ota_district` VALUES ('33661', '南靖县', 'zhangzhou', '1', '33661');
INSERT INTO `ota_district` VALUES ('33662', '秀洲区', 'jiaxing', '1', '33662');
INSERT INTO `ota_district` VALUES ('33663', '广丰县', 'shangrao', '1', '33663');
INSERT INTO `ota_district` VALUES ('33664', '玛沁县', 'guoluo', '1', '33664');
INSERT INTO `ota_district` VALUES ('33665', '北湖区', 'chenzhou', '1', '33665');
INSERT INTO `ota_district` VALUES ('33666', '芦山县', 'yaan', '1', '33666');
INSERT INTO `ota_district` VALUES ('33667', '浚县', 'hebi', '1', '33667');
INSERT INTO `ota_district` VALUES ('33668', '临翔区', 'lincang', '1', '33668');
INSERT INTO `ota_district` VALUES ('33669', '会东县', 'liangshan', '1', '33669');
INSERT INTO `ota_district` VALUES ('33670', '上犹县', 'ganzhou', '1', '33670');
INSERT INTO `ota_district` VALUES ('33671', '保亭黎族苗族自治县', 'baoting', '1', '33671');
INSERT INTO `ota_district` VALUES ('33672', '河北区', 'tianjin_city', '1', '33672');
INSERT INTO `ota_district` VALUES ('33673', '泗阳县', 'suqian', '1', '33673');
INSERT INTO `ota_district` VALUES ('33674', '双桥区', 'chengde', '1', '33674');
INSERT INTO `ota_district` VALUES ('33675', '望谟县', 'qianxinan', '1', '33675');
INSERT INTO `ota_district` VALUES ('33676', '徐水县', 'baoding', '1', '33676');
INSERT INTO `ota_district` VALUES ('33677', '子长县', 'yanan', '1', '33677');
INSERT INTO `ota_district` VALUES ('33678', '隆林各族自治县', 'baise', '1', '33678');
INSERT INTO `ota_district` VALUES ('33679', '株洲县', 'zhuzhou', '1', '33679');
INSERT INTO `ota_district` VALUES ('33680', '蒲县', 'linfen', '1', '33680');
INSERT INTO `ota_district` VALUES ('33681', '会泽县', 'qujing', '1', '33681');
INSERT INTO `ota_district` VALUES ('33682', '伊金霍洛旗', 'eerduosi', '1', '33682');
INSERT INTO `ota_district` VALUES ('33683', '呼玛县', 'daxinganling', '1', '33683');
INSERT INTO `ota_district` VALUES ('33684', '伍家岗区', 'yichang', '1', '33684');
INSERT INTO `ota_district` VALUES ('33685', '商州区', 'shangluo', '1', '33685');
INSERT INTO `ota_district` VALUES ('33686', '兴仁县', 'qianxinan', '1', '33686');
INSERT INTO `ota_district` VALUES ('33687', '开福区', 'changsha', '1', '33687');
INSERT INTO `ota_district` VALUES ('33688', '岳阳县', 'yueyang', '1', '33688');
INSERT INTO `ota_district` VALUES ('33689', '万荣县', 'yuncheng', '1', '33689');
INSERT INTO `ota_district` VALUES ('33690', '卓尼县', 'gannan', '1', '33690');
INSERT INTO `ota_district` VALUES ('33691', '灞桥区', 'xian', '1', '33691');
INSERT INTO `ota_district` VALUES ('33692', '永善县', 'zhaotong', '1', '33692');
INSERT INTO `ota_district` VALUES ('33693', '嵊泗县', 'zhoushan', '1', '33693');
INSERT INTO `ota_district` VALUES ('33694', '长阳土家族自治县', 'yichang', '1', '33694');
INSERT INTO `ota_district` VALUES ('33695', '石门县', 'changde', '1', '33695');
INSERT INTO `ota_district` VALUES ('33696', '平定县', 'yangquan', '1', '33696');
INSERT INTO `ota_district` VALUES ('33697', '乡宁县', 'linfen', '1', '33697');
INSERT INTO `ota_district` VALUES ('33698', '台前县', 'puyang', '1', '33698');
INSERT INTO `ota_district` VALUES ('33699', '靖州苗族侗族自治县', 'huaihua', '1', '33699');
INSERT INTO `ota_district` VALUES ('33700', '新巴尔虎左旗', 'hulunbeier', '1', '33700');
INSERT INTO `ota_district` VALUES ('33701', '且末县', 'bayinguoleng', '1', '33701');
INSERT INTO `ota_district` VALUES ('33702', '古丈县', 'xiangxi', '1', '33702');
INSERT INTO `ota_district` VALUES ('33703', '靖远县', 'baiyin', '1', '33703');
INSERT INTO `ota_district` VALUES ('33704', '会同县', 'huaihua', '1', '33704');
INSERT INTO `ota_district` VALUES ('33705', '武山县', 'tianshui', '1', '33705');
INSERT INTO `ota_district` VALUES ('33706', '梅县', 'meizhou', '1', '33706');
INSERT INTO `ota_district` VALUES ('33707', '恩施土家族苗族自治州', 'enshi', '1', '33707');
INSERT INTO `ota_district` VALUES ('33708', '塔河县', 'daxinganling', '1', '33708');
INSERT INTO `ota_district` VALUES ('33709', '苏尼特左旗', 'xilinguole', '1', '33709');
INSERT INTO `ota_district` VALUES ('33710', '华容区', 'ezhou', '1', '33710');
INSERT INTO `ota_district` VALUES ('33711', '日喀则地区', 'rikaze', '1', '33711');
INSERT INTO `ota_district` VALUES ('33712', '长洲区', 'wuzhou', '1', '33712');
INSERT INTO `ota_district` VALUES ('33713', '仁寿县', 'meishan', '1', '33713');
INSERT INTO `ota_district` VALUES ('33714', '博爱县', 'jiaozuo', '1', '33714');
INSERT INTO `ota_district` VALUES ('33715', '新浦区', 'lianyungang', '1', '33715');
INSERT INTO `ota_district` VALUES ('33716', '印台区', 'tongzhou', '1', '33716');
INSERT INTO `ota_district` VALUES ('33717', '顺平县', 'baoding', '1', '33717');
INSERT INTO `ota_district` VALUES ('33718', '杏花岭区', 'taiyuan', '1', '33718');
INSERT INTO `ota_district` VALUES ('33719', '澄海区', 'shantou', '1', '33719');
INSERT INTO `ota_district` VALUES ('33720', '叶县', 'pingdingshan', '1', '33720');
INSERT INTO `ota_district` VALUES ('33721', '高青县', 'zibo', '1', '33721');
INSERT INTO `ota_district` VALUES ('33722', '龙岗区', 'shenzhen', '1', '33722');
INSERT INTO `ota_district` VALUES ('33723', '左云县', 'datong', '1', '33723');
INSERT INTO `ota_district` VALUES ('33724', '昌平区', 'beijing_city', '1', '33724');
INSERT INTO `ota_district` VALUES ('33725', '彭山县', 'meishan', '1', '33725');
INSERT INTO `ota_district` VALUES ('33726', '册亨县', 'qianxinan', '1', '33726');
INSERT INTO `ota_district` VALUES ('33727', '陇川县', 'dehong', '1', '33727');
INSERT INTO `ota_district` VALUES ('33728', '鄂托克前旗', 'eerduosi', '1', '33728');
INSERT INTO `ota_district` VALUES ('33729', '镇宁布依族苗族自治县', 'anshun', '1', '33729');
INSERT INTO `ota_district` VALUES ('33730', '宁武县', 'xinzhou', '1', '33730');
INSERT INTO `ota_district` VALUES ('33731', '梁平县', 'chongqing_city', '1', '33731');
INSERT INTO `ota_district` VALUES ('33732', '弋阳县', 'shangrao', '1', '33732');
INSERT INTO `ota_district` VALUES ('33733', '泽州县', 'jincheng', '1', '33733');
INSERT INTO `ota_district` VALUES ('33734', '牟平区', 'yantai', '1', '33734');
INSERT INTO `ota_district` VALUES ('33735', '龙山区', 'liaoyuan', '1', '33735');
INSERT INTO `ota_district` VALUES ('33736', '松溪县', 'nanping', '1', '33736');
INSERT INTO `ota_district` VALUES ('33737', '罗城仫佬族自治县', 'hechi', '1', '33737');
INSERT INTO `ota_district` VALUES ('33738', '邗江区', 'yangzhou', '1', '33738');
INSERT INTO `ota_district` VALUES ('33739', '陈巴尔虎旗', 'hulunbeier', '1', '33739');
INSERT INTO `ota_district` VALUES ('33740', '涞水县', 'baoding', '1', '33740');
INSERT INTO `ota_district` VALUES ('33741', '赵县', 'shijiazhuang', '1', '33741');
INSERT INTO `ota_district` VALUES ('33742', '成安县', 'handan', '1', '33742');
INSERT INTO `ota_district` VALUES ('33743', '嵩明县', 'kunming', '1', '33743');
INSERT INTO `ota_district` VALUES ('33744', '潮南区', 'shantou', '1', '33744');
INSERT INTO `ota_district` VALUES ('33745', '双塔区', 'chaoyang', '1', '33745');
INSERT INTO `ota_district` VALUES ('33746', '皇姑区', 'shenyang', '1', '33746');
INSERT INTO `ota_district` VALUES ('33747', '任城区', 'jining', '1', '33747');
INSERT INTO `ota_district` VALUES ('33748', '茶陵县', 'zhuzhou', '1', '33748');
INSERT INTO `ota_district` VALUES ('33749', '鄱阳县', 'shangrao', '1', '33749');
INSERT INTO `ota_district` VALUES ('33750', '道孚县', 'ganzi', '1', '33750');
INSERT INTO `ota_district` VALUES ('33751', '中站区', 'jiaozuo', '1', '33751');
INSERT INTO `ota_district` VALUES ('33752', '平罗县', 'shizuishan', '1', '33752');
INSERT INTO `ota_district` VALUES ('33753', '稷山县', 'yuncheng', '1', '33753');
INSERT INTO `ota_district` VALUES ('33754', '安平县', 'hengshui', '1', '33754');
INSERT INTO `ota_district` VALUES ('33755', '凤翔县', 'baoji', '1', '33755');
INSERT INTO `ota_district` VALUES ('33756', '西峰区', 'qingyang', '1', '33756');
INSERT INTO `ota_district` VALUES ('33757', '龙湖区', 'shantou', '1', '33757');
INSERT INTO `ota_district` VALUES ('33758', '梅江区', 'meizhou', '1', '33758');
INSERT INTO `ota_district` VALUES ('33759', '普格县', 'liangshan', '1', '33759');
INSERT INTO `ota_district` VALUES ('33760', '横县', 'nanning', '1', '33760');
INSERT INTO `ota_district` VALUES ('33761', '和政县', 'linxia', '1', '33761');
INSERT INTO `ota_district` VALUES ('33762', '新城区', 'xian', '1', '33762');
INSERT INTO `ota_district` VALUES ('33763', '杭锦后旗', 'bayannaoer', '1', '33763');
INSERT INTO `ota_district` VALUES ('33764', '西工区', 'luoyang', '1', '33764');
INSERT INTO `ota_district` VALUES ('33765', '德宏傣族景颇族自治州', 'dehong', '1', '33765');
INSERT INTO `ota_district` VALUES ('33766', '宿松县', 'anqing', '1', '33766');
INSERT INTO `ota_district` VALUES ('33767', '垣曲县', 'yuncheng', '1', '33767');
INSERT INTO `ota_district` VALUES ('33768', '文峰区', 'anyang', '1', '33768');
INSERT INTO `ota_district` VALUES ('33769', '双峰县', 'loudi', '1', '33769');
INSERT INTO `ota_district` VALUES ('33770', '玉泉区', 'huhehaote', '1', '33770');
INSERT INTO `ota_district` VALUES ('33771', '天柱县', 'qiandongnan', '1', '33771');
INSERT INTO `ota_district` VALUES ('33772', '化德县', 'wulanchabu', '1', '33772');
INSERT INTO `ota_district` VALUES ('33773', '宜章县', 'chenzhou', '1', '33773');
INSERT INTO `ota_district` VALUES ('33774', '昌江区', 'jingdezhen', '1', '33774');
INSERT INTO `ota_district` VALUES ('33775', '爱民区', 'mudanjiang', '1', '33775');
INSERT INTO `ota_district` VALUES ('33776', '分宜县', 'xinyu', '1', '33776');
INSERT INTO `ota_district` VALUES ('33777', '潜山县', 'anqing', '1', '33777');
INSERT INTO `ota_district` VALUES ('33778', '神农架林区', 'shennongjia', '1', '33778');
INSERT INTO `ota_district` VALUES ('33779', '东乡族自治县', 'linxia', '1', '33779');
INSERT INTO `ota_district` VALUES ('33780', '双台子区', 'panjin', '1', '33780');
INSERT INTO `ota_district` VALUES ('33781', '平陆县', 'yuncheng', '1', '33781');
INSERT INTO `ota_district` VALUES ('33782', '太谷县', 'jinzhong', '1', '33782');
INSERT INTO `ota_district` VALUES ('33783', '灵台县', 'pingliang', '1', '33783');
INSERT INTO `ota_district` VALUES ('33784', '汉滨区', 'ankang', '1', '33784');
INSERT INTO `ota_district` VALUES ('33785', '埇桥区', 'suzhou_anhui', '1', '33785');
INSERT INTO `ota_district` VALUES ('33786', '红河哈尼族彝族自治州', 'honghe', '1', '33786');
INSERT INTO `ota_district` VALUES ('33787', '辽阳县', 'liaoyang', '1', '33787');
INSERT INTO `ota_district` VALUES ('33788', '海兴县', 'cangzhou', '1', '33788');
INSERT INTO `ota_district` VALUES ('33789', '北戴河区', 'qinhuangdao', '1', '33789');
INSERT INTO `ota_district` VALUES ('33790', '布拖县', 'liangshan', '1', '33790');
INSERT INTO `ota_district` VALUES ('33791', '宝塔区', 'yanan', '1', '33791');
INSERT INTO `ota_district` VALUES ('33792', '永吉县', 'jilin_city', '1', '33792');
INSERT INTO `ota_district` VALUES ('33793', '神木县', 'yulin_shanxi_02', '1', '33793');
INSERT INTO `ota_district` VALUES ('33794', '大荔县', 'weinan', '1', '33794');
INSERT INTO `ota_district` VALUES ('33795', '隆阳区', 'baoshan', '1', '33795');
INSERT INTO `ota_district` VALUES ('33796', '惠东县', 'huizhou_guangdong', '1', '33796');
INSERT INTO `ota_district` VALUES ('33797', '海南藏族自治州', 'hainanzangzu', '1', '33797');
INSERT INTO `ota_district` VALUES ('33798', '武义县', 'jinhua', '1', '33798');
INSERT INTO `ota_district` VALUES ('33799', '汝阳县', 'luoyang', '1', '33799');
INSERT INTO `ota_district` VALUES ('33800', '中江县', 'deyang', '1', '33800');
INSERT INTO `ota_district` VALUES ('33801', '平邑县', 'linyi', '1', '33801');
INSERT INTO `ota_district` VALUES ('33802', '矿区', 'yangquan', '1', '33802');
INSERT INTO `ota_district` VALUES ('33803', '渭滨区', 'baoji', '1', '33803');
INSERT INTO `ota_district` VALUES ('33804', '五华区', 'kunming', '1', '33804');
INSERT INTO `ota_district` VALUES ('33805', '离石区', 'lvliang', '1', '33805');
INSERT INTO `ota_district` VALUES ('33806', '闻喜县', 'yuncheng', '1', '33806');
INSERT INTO `ota_district` VALUES ('33807', '大武口区', 'shizuishan', '1', '33807');
INSERT INTO `ota_district` VALUES ('33808', '康乐县', 'linxia', '1', '33808');
INSERT INTO `ota_district` VALUES ('33809', '宁河县', 'tianjin_city', '1', '33809');
INSERT INTO `ota_district` VALUES ('33810', '嘉祥县', 'jining', '1', '33810');
INSERT INTO `ota_district` VALUES ('33811', '柳城县', 'liuzhou', '1', '33811');
INSERT INTO `ota_district` VALUES ('33812', '樊城区', 'xiangfan', '1', '33812');
INSERT INTO `ota_district` VALUES ('33813', '桦川县', 'jiamusi', '1', '33813');
INSERT INTO `ota_district` VALUES ('33814', '源城区', 'heyuan', '1', '33814');
INSERT INTO `ota_district` VALUES ('33815', '陕县', 'sanmenxia', '1', '33815');
INSERT INTO `ota_district` VALUES ('33816', '顺城区', 'fushun', '1', '33816');
INSERT INTO `ota_district` VALUES ('33817', '湘潭县', 'xiangtan', '1', '33817');
INSERT INTO `ota_district` VALUES ('33818', '静海县', 'tianjin_city', '1', '33818');
INSERT INTO `ota_district` VALUES ('33819', '中山区', 'dalian', '1', '33819');
INSERT INTO `ota_district` VALUES ('33820', '宁化县', 'sanming', '1', '33820');
INSERT INTO `ota_district` VALUES ('33821', '通渭县', 'dingxi', '1', '33821');
INSERT INTO `ota_district` VALUES ('33822', '博山区', 'zibo', '1', '33822');
INSERT INTO `ota_district` VALUES ('33823', '大悟县', 'xiaogan', '1', '33823');
INSERT INTO `ota_district` VALUES ('33824', '零陵区', 'yongzhou', '1', '33824');
INSERT INTO `ota_district` VALUES ('33825', '秀山土家族苗族自治县', 'chongqing_city', '1', '33825');
INSERT INTO `ota_district` VALUES ('33826', '城东区', 'xining', '1', '33826');
INSERT INTO `ota_district` VALUES ('33827', '阿巴嘎旗', 'xilinguole', '1', '33827');
INSERT INTO `ota_district` VALUES ('33828', '浑源县', 'datong', '1', '33828');
INSERT INTO `ota_district` VALUES ('33829', '细河区', 'fuxin', '1', '33829');
INSERT INTO `ota_district` VALUES ('33830', '江干区', 'hangzhou', '1', '33830');
INSERT INTO `ota_district` VALUES ('33831', '五莲县', 'rizhao', '1', '33831');
INSERT INTO `ota_district` VALUES ('33832', '铁西区', 'siping', '1', '33832');
INSERT INTO `ota_district` VALUES ('33833', '富平县', 'weinan', '1', '33833');
INSERT INTO `ota_district` VALUES ('33834', '合川区', 'chongqing_city', '1', '33834');
INSERT INTO `ota_district` VALUES ('33835', '福山区', 'yantai', '1', '33835');
INSERT INTO `ota_district` VALUES ('33836', '西和县', 'longnan', '1', '33836');
INSERT INTO `ota_district` VALUES ('33837', '猇亭区', 'yichang', '1', '33837');
INSERT INTO `ota_district` VALUES ('33838', '北仑区', 'ningbo', '1', '33838');
INSERT INTO `ota_district` VALUES ('33839', '蕉岭县', 'meizhou', '1', '33839');
INSERT INTO `ota_district` VALUES ('33840', '扶沟县', 'zhoukou', '1', '33840');
INSERT INTO `ota_district` VALUES ('33841', '永春县', 'quanzhou', '1', '33841');
INSERT INTO `ota_district` VALUES ('33842', '新兴县', 'yunfu', '1', '33842');
INSERT INTO `ota_district` VALUES ('33843', '自流井区', 'zigong', '1', '33843');
INSERT INTO `ota_district` VALUES ('33844', '老城区', 'luoyang', '1', '33844');
INSERT INTO `ota_district` VALUES ('33845', '围场满族蒙古族自治县', 'chengde', '1', '33845');
INSERT INTO `ota_district` VALUES ('33846', '双阳区', 'changchun', '1', '33846');
INSERT INTO `ota_district` VALUES ('33847', '五台县', 'xinzhou', '1', '33847');
INSERT INTO `ota_district` VALUES ('33848', '阿拉善盟', 'alashan', '1', '33848');
INSERT INTO `ota_district` VALUES ('33849', '惠城区', 'huizhou_guangdong', '1', '33849');
INSERT INTO `ota_district` VALUES ('33850', '夹江县', 'leshan', '1', '33850');
INSERT INTO `ota_district` VALUES ('33851', '鹰手营子矿区', 'chengde', '1', '33851');
INSERT INTO `ota_district` VALUES ('33852', '萝岗区', 'guangzhou', '1', '33852');
INSERT INTO `ota_district` VALUES ('33853', '白水县', 'weinan', '1', '33853');
INSERT INTO `ota_district` VALUES ('33854', '竹山县', 'shiyan', '1', '33854');
INSERT INTO `ota_district` VALUES ('33855', '果洛藏族自治州', 'guoluo', '1', '33855');
INSERT INTO `ota_district` VALUES ('33856', '甘井子区', 'dalian', '1', '33856');
INSERT INTO `ota_district` VALUES ('33857', '五寨县', 'xinzhou', '1', '33857');
INSERT INTO `ota_district` VALUES ('33858', '黄州区', 'huanggang', '1', '33858');
INSERT INTO `ota_district` VALUES ('33859', '疏勒县', 'kashi', '1', '33859');
INSERT INTO `ota_district` VALUES ('33860', '南昌县', 'nanchang', '1', '33860');
INSERT INTO `ota_district` VALUES ('33861', '都兰县', 'haixi', '1', '33861');
INSERT INTO `ota_district` VALUES ('33862', '邢台县', 'xingtai', '1', '33862');
INSERT INTO `ota_district` VALUES ('33863', '山阳区', 'jiaozuo', '1', '33863');
INSERT INTO `ota_district` VALUES ('33864', '八公山区', 'huainan', '1', '33864');
INSERT INTO `ota_district` VALUES ('33865', '揭西县', 'jieyang', '1', '33865');
INSERT INTO `ota_district` VALUES ('33866', '洛扎县', 'shannan', '1', '33866');
INSERT INTO `ota_district` VALUES ('33867', '翼城县', 'linfen', '1', '33867');
INSERT INTO `ota_district` VALUES ('33868', '大港区', 'tianjin_city', '1', '33868');
INSERT INTO `ota_district` VALUES ('33869', '淮阴区', 'huaian', '1', '33869');
INSERT INTO `ota_district` VALUES ('33871', '鄢陵县', 'xuchang', '1', '33871');
INSERT INTO `ota_district` VALUES ('33872', '金寨县', 'liuan', '1', '33872');
INSERT INTO `ota_district` VALUES ('33873', '哈密地区', 'hami', '1', '33873');
INSERT INTO `ota_district` VALUES ('33874', '迎江区', 'anqing', '1', '33874');
INSERT INTO `ota_district` VALUES ('33875', '新源县', 'yili', '1', '33875');
INSERT INTO `ota_district` VALUES ('33876', '陵县', 'dezhou', '1', '33876');
INSERT INTO `ota_district` VALUES ('33877', '民和回族土族自治县', 'haidong', '1', '33877');
INSERT INTO `ota_district` VALUES ('33878', '确山县', 'zhumadian', '1', '33878');
INSERT INTO `ota_district` VALUES ('33879', '曲周县', 'handan', '1', '33879');
INSERT INTO `ota_district` VALUES ('33880', '点军区', 'yichang', '1', '33880');
INSERT INTO `ota_district` VALUES ('33881', '雨花区', 'changsha', '1', '33881');
INSERT INTO `ota_district` VALUES ('33882', '防城区', 'fangchenggang', '1', '33882');
INSERT INTO `ota_district` VALUES ('33883', '宜良县', 'kunming', '1', '33883');
INSERT INTO `ota_district` VALUES ('33884', '喀喇沁左翼蒙古族自治县', 'chaoyang', '1', '33884');
INSERT INTO `ota_district` VALUES ('33885', '罗源县', 'fuzhou_fujian', '1', '33885');
INSERT INTO `ota_district` VALUES ('33886', '白河县', 'ankang', '1', '33886');
INSERT INTO `ota_district` VALUES ('33887', '宁陵县', 'shangqiu', '1', '33887');
INSERT INTO `ota_district` VALUES ('33888', '合阳县', 'weinan', '1', '33888');
INSERT INTO `ota_district` VALUES ('33890', '凤阳县', 'chuzhou', '1', '33890');
INSERT INTO `ota_district` VALUES ('33891', '临猗县', 'yuncheng', '1', '33891');
INSERT INTO `ota_district` VALUES ('33892', '邵东县', 'shaoyang', '1', '33892');
INSERT INTO `ota_district` VALUES ('33893', '桥西区', 'zhangjiakou', '1', '33893');
INSERT INTO `ota_district` VALUES ('33894', '婺城区', 'jinhua', '1', '33894');
INSERT INTO `ota_district` VALUES ('33895', '海北藏族自治州', 'haibei', '1', '33895');
INSERT INTO `ota_district` VALUES ('33896', '屏山县', 'yibin', '1', '33896');
INSERT INTO `ota_district` VALUES ('33897', '虹口区', 'shanghai_city', '1', '33897');
INSERT INTO `ota_district` VALUES ('33898', '红古区', 'lanzhou', '1', '33898');
INSERT INTO `ota_district` VALUES ('33899', '康马县', 'rikaze', '1', '33899');
INSERT INTO `ota_district` VALUES ('33900', '鼎湖区', 'zhaoqing', '1', '33900');
INSERT INTO `ota_district` VALUES ('33901', '宜川县', 'yanan', '1', '33901');
INSERT INTO `ota_district` VALUES ('33902', '荔浦县', 'guilin', '1', '33902');
INSERT INTO `ota_district` VALUES ('33903', '淳化县', 'xianyang', '1', '33903');
INSERT INTO `ota_district` VALUES ('33904', '青山区', 'wuhan', '1', '33904');
INSERT INTO `ota_district` VALUES ('33905', '向阳区', 'hegang', '1', '33905');
INSERT INTO `ota_district` VALUES ('33906', '雨湖区', 'xiangtan', '1', '33906');
INSERT INTO `ota_district` VALUES ('33907', '崇阳县', 'xianning', '1', '33907');
INSERT INTO `ota_district` VALUES ('33908', '兴业县', 'yulin_guangxi', '1', '33908');
INSERT INTO `ota_district` VALUES ('33909', '岚皋县', 'ankang', '1', '33909');
INSERT INTO `ota_district` VALUES ('33910', '威远县', 'neijiang', '1', '33910');
INSERT INTO `ota_district` VALUES ('33911', '巨野县', 'heze', '1', '33911');
INSERT INTO `ota_district` VALUES ('33912', '科尔沁左翼中旗', 'tongliao', '1', '33912');
INSERT INTO `ota_district` VALUES ('33913', '凌云县', 'baise', '1', '33913');
INSERT INTO `ota_district` VALUES ('33914', '江安县', 'yibin', '1', '33914');
INSERT INTO `ota_district` VALUES ('33915', '下陆区', 'huangshi', '1', '33915');
INSERT INTO `ota_district` VALUES ('33916', '于洪区', 'shenyang', '1', '33916');
INSERT INTO `ota_district` VALUES ('33917', '路北区', 'tangshan', '1', '33917');
INSERT INTO `ota_district` VALUES ('33918', '龙沙区', 'qiqihaer', '1', '33918');
INSERT INTO `ota_district` VALUES ('33919', '南岗区', 'haerbin', '1', '33919');
INSERT INTO `ota_district` VALUES ('33920', '雷山县', 'qiandongnan', '1', '33920');
INSERT INTO `ota_district` VALUES ('33921', '长清区', 'jinan', '1', '33921');
INSERT INTO `ota_district` VALUES ('33922', '平阳县', 'wenzhou', '1', '33922');
INSERT INTO `ota_district` VALUES ('33923', '洛川县', 'yanan', '1', '33923');
INSERT INTO `ota_district` VALUES ('33924', '马山县', 'nanning', '1', '33924');
INSERT INTO `ota_district` VALUES ('33925', '梅里斯达翰尔族区', 'qiqihaer', '1', '33925');
INSERT INTO `ota_district` VALUES ('33926', '田林县', 'baise', '1', '33926');
INSERT INTO `ota_district` VALUES ('33927', '郧县', 'shiyan', '1', '33927');
INSERT INTO `ota_district` VALUES ('33928', '乌拉特中旗', 'bayannaoer', '1', '33928');
INSERT INTO `ota_district` VALUES ('33929', '阿坝藏族羌族自治州', 'aba', '1', '33929');
INSERT INTO `ota_district` VALUES ('33930', '大兴安岭地区', 'daxinganling', '1', '33930');
INSERT INTO `ota_district` VALUES ('33931', '济阳县', 'jinan', '1', '33931');
INSERT INTO `ota_district` VALUES ('33932', '错那县', 'shannan', '1', '33932');
INSERT INTO `ota_district` VALUES ('33933', '兴庆区', 'yinchuan', '1', '33933');
INSERT INTO `ota_district` VALUES ('33934', '桥东区', 'zhangjiakou', '1', '33934');
INSERT INTO `ota_district` VALUES ('33935', '镶黄旗', 'xilinguole', '1', '33935');
INSERT INTO `ota_district` VALUES ('33936', '阿城区', 'haerbin', '1', '33936');
INSERT INTO `ota_district` VALUES ('33937', '濮阳县', 'puyang', '1', '33937');
INSERT INTO `ota_district` VALUES ('33938', '漳县', 'dingxi', '1', '33938');
INSERT INTO `ota_district` VALUES ('33939', '头屯河区', 'wulumuqi', '1', '33939');
INSERT INTO `ota_district` VALUES ('33940', '独山子区', 'kelamayi', '1', '33940');
INSERT INTO `ota_district` VALUES ('33941', '新抚区', 'fushun', '1', '33941');
INSERT INTO `ota_district` VALUES ('33942', '合江县', 'luzhou', '1', '33942');
INSERT INTO `ota_district` VALUES ('33943', '印江土家族苗族自治县', 'tongren', '1', '33943');
INSERT INTO `ota_district` VALUES ('33944', '宝清县', 'shuangyashan', '1', '33944');
INSERT INTO `ota_district` VALUES ('33945', '泾川县', 'pingliang', '1', '33945');
INSERT INTO `ota_district` VALUES ('33946', '襄汾县', 'linfen', '1', '33946');
INSERT INTO `ota_district` VALUES ('33947', '交城县', 'lvliang', '1', '33947');
INSERT INTO `ota_district` VALUES ('33948', '灌南县', 'lianyungang', '1', '33948');
INSERT INTO `ota_district` VALUES ('33949', '工农区', 'hegang', '1', '33949');
INSERT INTO `ota_district` VALUES ('33950', '临朐县', 'weifang', '1', '33950');
INSERT INTO `ota_district` VALUES ('33952', '巍山彝族回族自治县', 'dali', '1', '33952');
INSERT INTO `ota_district` VALUES ('33953', '怀柔区', 'beijing_city', '1', '33953');
INSERT INTO `ota_district` VALUES ('33954', '泗县', 'suzhou_anhui', '1', '33954');
INSERT INTO `ota_district` VALUES ('33955', '海丰县', 'shanwei', '1', '33955');
INSERT INTO `ota_district` VALUES ('33956', '龙潭区', 'jilin_city', '1', '33956');
INSERT INTO `ota_district` VALUES ('33957', '芦溪县', 'pingxiang', '1', '33957');
INSERT INTO `ota_district` VALUES ('33958', '牡丹区', 'heze', '1', '33958');
INSERT INTO `ota_district` VALUES ('33959', '城中区', 'xining', '1', '33959');
INSERT INTO `ota_district` VALUES ('33960', '颍上县', 'fuyang_anhui', '1', '33960');
INSERT INTO `ota_district` VALUES ('33961', '白下区', 'nanjing', '1', '33961');
INSERT INTO `ota_district` VALUES ('33962', '沙县', 'sanming', '1', '33962');
INSERT INTO `ota_district` VALUES ('33963', '荣昌县', 'chongqing_city', '1', '33963');
INSERT INTO `ota_district` VALUES ('33964', '康平县', 'shenyang', '1', '33964');
INSERT INTO `ota_district` VALUES ('33965', '克什克腾旗', 'chifeng', '1', '33965');
INSERT INTO `ota_district` VALUES ('33966', '金水区', 'zhengzhou', '1', '33966');
INSERT INTO `ota_district` VALUES ('33967', '静乐县', 'xinzhou', '1', '33967');
INSERT INTO `ota_district` VALUES ('33968', '三山区', 'wuhu', '1', '33968');
INSERT INTO `ota_district` VALUES ('33969', '黑山县', 'jinzhou', '1', '33969');
INSERT INTO `ota_district` VALUES ('33970', '宁乡县', 'changsha', '1', '33970');
INSERT INTO `ota_district` VALUES ('33971', '浉河区', 'xinyang', '1', '33971');
INSERT INTO `ota_district` VALUES ('33972', '北碚区', 'chongqing_city', '1', '33972');
INSERT INTO `ota_district` VALUES ('33973', '清原满族自治县', 'fushun', '1', '33973');
INSERT INTO `ota_district` VALUES ('33974', '隰县', 'linfen', '1', '33974');
INSERT INTO `ota_district` VALUES ('33975', '全南县', 'ganzhou', '1', '33975');
INSERT INTO `ota_district` VALUES ('33976', '桑日县', 'shannan', '1', '33976');
INSERT INTO `ota_district` VALUES ('33977', '西塞山区', 'huangshi', '1', '33977');
INSERT INTO `ota_district` VALUES ('33978', '开化县', 'quzhou', '1', '33978');
INSERT INTO `ota_district` VALUES ('33979', '平谷区', 'beijing_city', '1', '33979');
INSERT INTO `ota_district` VALUES ('33980', '凉山彝族自治州', 'liangshan', '1', '33980');
INSERT INTO `ota_district` VALUES ('33981', '沙坪坝区', 'chongqing_city', '1', '33981');
INSERT INTO `ota_district` VALUES ('33982', '安多县', 'naqu', '1', '33982');
INSERT INTO `ota_district` VALUES ('33983', '于都县', 'ganzhou', '1', '33983');
INSERT INTO `ota_district` VALUES ('33984', '宝坻区', 'tianjin_city', '1', '33984');
INSERT INTO `ota_district` VALUES ('33985', '汉源县', 'yaan', '1', '33985');
INSERT INTO `ota_district` VALUES ('33986', '两当县', 'longnan', '1', '33986');
INSERT INTO `ota_district` VALUES ('33987', '东光县', 'cangzhou', '1', '33987');
INSERT INTO `ota_district` VALUES ('33988', '邯山区', 'handan', '1', '33988');
INSERT INTO `ota_district` VALUES ('33989', '银海区', 'beihai', '1', '33989');
INSERT INTO `ota_district` VALUES ('33990', '北辰区', 'tianjin_city', '1', '33990');
INSERT INTO `ota_district` VALUES ('33991', '岗巴县', 'rikaze', '1', '33991');
INSERT INTO `ota_district` VALUES ('33992', '裕华区', 'shijiazhuang', '1', '33992');
INSERT INTO `ota_district` VALUES ('33993', '红旗区', 'xinxiang', '1', '33993');
INSERT INTO `ota_district` VALUES ('33994', '阳新县', 'huangshi', '1', '33994');
INSERT INTO `ota_district` VALUES ('33995', '高台县', 'zhangye', '1', '33995');
INSERT INTO `ota_district` VALUES ('33996', '乾安县', 'songyuan', '1', '33996');
INSERT INTO `ota_district` VALUES ('33997', '平泉县', 'chengde', '1', '33997');
INSERT INTO `ota_district` VALUES ('33998', '游仙区', 'mianyang', '1', '33998');
INSERT INTO `ota_district` VALUES ('33999', '大新县', 'chongzuo', '1', '33999');
INSERT INTO `ota_district` VALUES ('34000', '普陀区', 'shanghai_city', '1', '34000');
INSERT INTO `ota_district` VALUES ('34001', '双滦区', 'chengde', '1', '34001');
INSERT INTO `ota_district` VALUES ('34002', '玛纳斯县', 'changji', '1', '34002');
INSERT INTO `ota_district` VALUES ('34003', '涡阳县', 'bozhou', '1', '34003');
INSERT INTO `ota_district` VALUES ('34004', '南岳区', 'hengyang', '1', '34004');
INSERT INTO `ota_district` VALUES ('34005', '鼓楼区', 'kaifeng', '1', '34005');
INSERT INTO `ota_district` VALUES ('34006', '同德县', 'hainanzangzu', '1', '34006');
INSERT INTO `ota_district` VALUES ('34007', '前进区', 'jiamusi', '1', '34007');
INSERT INTO `ota_district` VALUES ('34008', '建宁县', 'sanming', '1', '34008');
INSERT INTO `ota_district` VALUES ('34009', '夏津县', 'dezhou', '1', '34009');
INSERT INTO `ota_district` VALUES ('34010', '华县', 'weinan', '1', '34010');
INSERT INTO `ota_district` VALUES ('34011', '门头沟区', 'beijing_city', '1', '34011');
INSERT INTO `ota_district` VALUES ('34012', '集贤县', 'shuangyashan', '1', '34012');
INSERT INTO `ota_district` VALUES ('34013', '宜丰县', 'yichun_jiangxi', '1', '34013');
INSERT INTO `ota_district` VALUES ('34014', '汉寿县', 'changde', '1', '34014');
INSERT INTO `ota_district` VALUES ('34015', '清流县', 'sanming', '1', '34015');
INSERT INTO `ota_district` VALUES ('34016', '松江区', 'shanghai_city', '1', '34016');
INSERT INTO `ota_district` VALUES ('34017', '原阳县', 'xinxiang', '1', '34017');
INSERT INTO `ota_district` VALUES ('34018', '芗城区', 'zhangzhou', '1', '34018');
INSERT INTO `ota_district` VALUES ('34019', '江阳区', 'luzhou', '1', '34019');
INSERT INTO `ota_district` VALUES ('34020', '宁陕县', 'ankang', '1', '34020');
INSERT INTO `ota_district` VALUES ('34021', '碑林区', 'xian', '1', '34021');
INSERT INTO `ota_district` VALUES ('34022', '沂南县', 'linyi', '1', '34022');
INSERT INTO `ota_district` VALUES ('34024', '云阳县', 'chongqing_city', '1', '34024');
INSERT INTO `ota_district` VALUES ('34025', '尧都区', 'linfen', '1', '34025');
INSERT INTO `ota_district` VALUES ('34026', '白云鄂博矿区', 'baotou', '1', '34026');
INSERT INTO `ota_district` VALUES ('34027', '漳浦县', 'zhangzhou', '1', '34027');
INSERT INTO `ota_district` VALUES ('34028', '卢氏县', 'sanmenxia', '1', '34028');
INSERT INTO `ota_district` VALUES ('34029', '东山区', 'hegang', '1', '34029');
INSERT INTO `ota_district` VALUES ('34030', '志丹县', 'yanan', '1', '34030');
INSERT INTO `ota_district` VALUES ('34031', '永寿县', 'xianyang', '1', '34031');
INSERT INTO `ota_district` VALUES ('34032', '茂县', 'aba', '1', '34032');
INSERT INTO `ota_district` VALUES ('34033', '泌阳县', 'zhumadian', '1', '34033');
INSERT INTO `ota_district` VALUES ('34034', '兴县', 'lvliang', '1', '34034');
INSERT INTO `ota_district` VALUES ('34035', '内丘县', 'xingtai', '1', '34035');
INSERT INTO `ota_district` VALUES ('34036', '乃东县', 'shannan', '1', '34036');
INSERT INTO `ota_district` VALUES ('34037', '船营区', 'jilin_city', '1', '34037');
INSERT INTO `ota_district` VALUES ('34038', '成县', 'longnan', '1', '34038');
INSERT INTO `ota_district` VALUES ('34039', '甘孜县', 'ganzi', '1', '34039');
INSERT INTO `ota_district` VALUES ('34040', '碾子山区', 'qiqihaer', '1', '34040');
INSERT INTO `ota_district` VALUES ('34041', '双柏县', 'chuxiong', '1', '34041');
INSERT INTO `ota_district` VALUES ('34042', '石屏县', 'honghe', '1', '34042');
INSERT INTO `ota_district` VALUES ('34043', '尼木县', 'lasa', '1', '34043');
INSERT INTO `ota_district` VALUES ('34044', '石林彝族自治县', 'kunming', '1', '34044');
INSERT INTO `ota_district` VALUES ('34045', '滦南县', 'tangshan', '1', '34045');
INSERT INTO `ota_district` VALUES ('34046', '日土县', 'ali', '1', '34046');
INSERT INTO `ota_district` VALUES ('34047', '禹会区', 'bangbu', '1', '34047');
INSERT INTO `ota_district` VALUES ('34048', '璧山县', 'chongqing_city', '1', '34048');
INSERT INTO `ota_district` VALUES ('34049', '铅山县', 'shangrao', '1', '34049');
INSERT INTO `ota_district` VALUES ('34050', '牧野区', 'xinxiang', '1', '34050');
INSERT INTO `ota_district` VALUES ('34051', '朝天区', 'guangyuan', '1', '34051');
INSERT INTO `ota_district` VALUES ('34052', '文山壮族苗族自治州', 'wenshan', '1', '34052');
INSERT INTO `ota_district` VALUES ('34053', '柯城区', 'quzhou', '1', '34053');
INSERT INTO `ota_district` VALUES ('34054', '开鲁县', 'tongliao', '1', '34054');
INSERT INTO `ota_district` VALUES ('34055', '白银区', 'baiyin', '1', '34055');
INSERT INTO `ota_district` VALUES ('34056', '望花区', 'fushun', '1', '34056');
INSERT INTO `ota_district` VALUES ('34057', '长兴县', 'huzhou', '1', '34057');
INSERT INTO `ota_district` VALUES ('34058', '隆德县', 'guyuan', '1', '34058');
INSERT INTO `ota_district` VALUES ('34059', '清新县', 'qingyuan', '1', '34059');
INSERT INTO `ota_district` VALUES ('34060', '延津县', 'xinxiang', '1', '34060');
INSERT INTO `ota_district` VALUES ('34061', '望奎县', 'suihua', '1', '34061');
INSERT INTO `ota_district` VALUES ('34062', '宁都县', 'ganzhou', '1', '34062');
INSERT INTO `ota_district` VALUES ('34063', '朝阳县', 'chaoyang', '1', '34063');
INSERT INTO `ota_district` VALUES ('34064', '余杭区', 'hangzhou', '1', '34064');
INSERT INTO `ota_district` VALUES ('34065', '海拉尔区', 'hulunbeier', '1', '34065');
INSERT INTO `ota_district` VALUES ('34066', '苏家屯区', 'shenyang', '1', '34066');
INSERT INTO `ota_district` VALUES ('34067', '鲤城区', 'quanzhou', '1', '34067');
INSERT INTO `ota_district` VALUES ('34068', '郾城区', 'luohe', '1', '34068');
INSERT INTO `ota_district` VALUES ('34069', '松潘县', 'aba', '1', '34069');
INSERT INTO `ota_district` VALUES ('34070', '壤塘县', 'aba', '1', '34070');
INSERT INTO `ota_district` VALUES ('34071', '鄞州区', 'ningbo', '1', '34071');
INSERT INTO `ota_district` VALUES ('34072', '思明区', 'xiamen', '1', '34072');
INSERT INTO `ota_district` VALUES ('34073', '营山县', 'nanchong', '1', '34073');
INSERT INTO `ota_district` VALUES ('34074', '阜南县', 'fuyang_anhui', '1', '34074');
INSERT INTO `ota_district` VALUES ('34075', '泽普县', 'kashi', '1', '34075');
INSERT INTO `ota_district` VALUES ('34076', '南陵县', 'wuhu', '1', '34076');
INSERT INTO `ota_district` VALUES ('34077', '友谊县', 'shuangyashan', '1', '34077');
INSERT INTO `ota_district` VALUES ('34078', '南县', 'yiyang', '1', '34078');
INSERT INTO `ota_district` VALUES ('34079', '巴塘县', 'ganzi', '1', '34079');
INSERT INTO `ota_district` VALUES ('34080', '化隆回族自治县', 'haidong', '1', '34080');
INSERT INTO `ota_district` VALUES ('34081', '敖汉旗', 'chifeng', '1', '34081');
INSERT INTO `ota_district` VALUES ('34082', '邵阳县', 'shaoyang', '1', '34082');
INSERT INTO `ota_district` VALUES ('34083', '大方县', 'bijie', '1', '34083');
INSERT INTO `ota_district` VALUES ('34084', '封开县', 'zhaoqing', '1', '34084');
INSERT INTO `ota_district` VALUES ('34085', '翁源县', 'shaoguan', '1', '34085');
INSERT INTO `ota_district` VALUES ('34086', '察哈尔右翼中旗', 'wulanchabu', '1', '34086');
INSERT INTO `ota_district` VALUES ('34087', '隆尧县', 'xingtai', '1', '34087');
INSERT INTO `ota_district` VALUES ('34088', '淄川区', 'zibo', '1', '34088');
INSERT INTO `ota_district` VALUES ('34089', '北塔区', 'shaoyang', '1', '34089');
INSERT INTO `ota_district` VALUES ('34090', '安居区', 'suining', '1', '34090');
INSERT INTO `ota_district` VALUES ('34091', '大城县', 'langfang', '1', '34091');
INSERT INTO `ota_district` VALUES ('34092', '江东区', 'ningbo', '1', '34092');
INSERT INTO `ota_district` VALUES ('34093', '富蕴县', 'aletai', '1', '34093');
INSERT INTO `ota_district` VALUES ('34094', '卢湾区', 'shanghai_city', '1', '34094');
INSERT INTO `ota_district` VALUES ('34095', '盐都区', 'yancheng', '1', '34095');
INSERT INTO `ota_district` VALUES ('34096', '茄子河区', 'qitaihe', '1', '34096');
INSERT INTO `ota_district` VALUES ('34097', '忻城县', 'laibin', '1', '34097');
INSERT INTO `ota_district` VALUES ('34098', '巴马瑶族自治县', 'hechi', '1', '34098');
INSERT INTO `ota_district` VALUES ('34099', '耀州区', 'tongzhou', '1', '34099');
INSERT INTO `ota_district` VALUES ('34100', '华宁县', 'yuxi', '1', '34100');
INSERT INTO `ota_district` VALUES ('34101', '大理白族自治州', 'dali', '1', '34101');
INSERT INTO `ota_district` VALUES ('34102', '绿春县', 'honghe', '1', '34102');
INSERT INTO `ota_district` VALUES ('34103', '惠安县', 'quanzhou', '1', '34103');
INSERT INTO `ota_district` VALUES ('34104', '张湾区', 'shiyan', '1', '34104');
INSERT INTO `ota_district` VALUES ('34105', '硚口区', 'wuhan', '1', '34105');
INSERT INTO `ota_district` VALUES ('34106', '潮安县', 'chaozhou', '1', '34106');
INSERT INTO `ota_district` VALUES ('34107', '浈江区', 'shaoguan', '1', '34107');
INSERT INTO `ota_district` VALUES ('34108', '湘桥区', 'chaozhou', '1', '34108');
INSERT INTO `ota_district` VALUES ('34109', '宽城区', 'changchun', '1', '34109');
INSERT INTO `ota_district` VALUES ('34110', '荆州区', 'jingzhou', '1', '34110');
INSERT INTO `ota_district` VALUES ('34111', '泾源县', 'guyuan', '1', '34111');
INSERT INTO `ota_district` VALUES ('34112', '东山县', 'zhangzhou', '1', '34112');
INSERT INTO `ota_district` VALUES ('34113', '渝中区', 'chongqing_city', '1', '34113');
INSERT INTO `ota_district` VALUES ('34114', '贵池区', 'chizhou', '1', '34114');
INSERT INTO `ota_district` VALUES ('34115', '岳普湖县', 'kashi', '1', '34115');
INSERT INTO `ota_district` VALUES ('34116', '宝山区', 'shanghai_city', '1', '34116');
INSERT INTO `ota_district` VALUES ('34117', '蒲江县', 'chengdu', '1', '34117');
INSERT INTO `ota_district` VALUES ('34118', '和顺县', 'jinzhong', '1', '34118');
INSERT INTO `ota_district` VALUES ('34119', '茌平县', 'liaocheng', '1', '34119');
INSERT INTO `ota_district` VALUES ('34120', '莱城区', 'laiwu', '1', '34120');
INSERT INTO `ota_district` VALUES ('34121', '矿区', 'datong', '1', '34121');
INSERT INTO `ota_district` VALUES ('34122', '亚东县', 'rikaze', '1', '34122');
INSERT INTO `ota_district` VALUES ('34123', '虎丘区', 'suzhou_jiangsu', '1', '34123');
INSERT INTO `ota_district` VALUES ('34124', '彬县', 'xianyang', '1', '34124');
INSERT INTO `ota_district` VALUES ('34125', '定安县', 'dingan', '1', '34125');
INSERT INTO `ota_district` VALUES ('34126', '湘东区', 'pingxiang', '1', '34126');
INSERT INTO `ota_district` VALUES ('34127', '昌都县', 'changdu', '1', '34127');
INSERT INTO `ota_district` VALUES ('34128', '维扬区', 'yangzhou', '1', '34128');
INSERT INTO `ota_district` VALUES ('34129', '左贡县', 'changdu', '1', '34129');
INSERT INTO `ota_district` VALUES ('34130', '涿鹿县', 'zhangjiakou', '1', '34130');
INSERT INTO `ota_district` VALUES ('34131', '黟县', 'huangshan', '1', '34131');
INSERT INTO `ota_district` VALUES ('34132', '长治县', 'changzhi', '1', '34132');
INSERT INTO `ota_district` VALUES ('34133', '沛县', 'xuzhou', '1', '34133');
INSERT INTO `ota_district` VALUES ('34134', '东宝区', 'jingmen', '1', '34134');
INSERT INTO `ota_district` VALUES ('34135', '高邑县', 'shijiazhuang', '1', '34135');
INSERT INTO `ota_district` VALUES ('34136', '祁门县', 'huangshan', '1', '34136');
INSERT INTO `ota_district` VALUES ('34137', '盐津县', 'zhaotong', '1', '34137');
INSERT INTO `ota_district` VALUES ('34138', '利津县', 'dongying', '1', '34138');
INSERT INTO `ota_district` VALUES ('34139', '贵南县', 'hainanzangzu', '1', '34139');
INSERT INTO `ota_district` VALUES ('34140', '相山区', 'huaibei', '1', '34140');
INSERT INTO `ota_district` VALUES ('34141', '邻水县', 'guangan', '1', '34141');
INSERT INTO `ota_district` VALUES ('34142', '鄯善县', 'tulufan', '1', '34142');
INSERT INTO `ota_district` VALUES ('34143', '平阴县', 'jinan', '1', '34143');
INSERT INTO `ota_district` VALUES ('34144', '鲁甸县', 'zhaotong', '1', '34144');
INSERT INTO `ota_district` VALUES ('34145', '长汀县', 'longyan', '1', '34145');
INSERT INTO `ota_district` VALUES ('34146', '炉霍县', 'ganzi', '1', '34146');
INSERT INTO `ota_district` VALUES ('34147', '商城县', 'xinyang', '1', '34147');
INSERT INTO `ota_district` VALUES ('34148', '三穗县', 'qiandongnan', '1', '34148');
INSERT INTO `ota_district` VALUES ('34149', '梁河县', 'dehong', '1', '34149');
INSERT INTO `ota_district` VALUES ('34150', '泽库县', 'huangnan', '1', '34150');
INSERT INTO `ota_district` VALUES ('34151', '江源区', 'baishan', '1', '34151');
INSERT INTO `ota_district` VALUES ('34152', '章贡区', 'ganzhou', '1', '34152');
INSERT INTO `ota_district` VALUES ('34153', '川汇区', 'zhoukou', '1', '34153');
INSERT INTO `ota_district` VALUES ('34154', '东阿县', 'liaocheng', '1', '34154');
INSERT INTO `ota_district` VALUES ('34155', '吴起县', 'yanan', '1', '34155');
INSERT INTO `ota_district` VALUES ('34156', '颍州区', 'fuyang_anhui', '1', '34156');
INSERT INTO `ota_district` VALUES ('34157', '保康县', 'xiangfan', '1', '34157');
INSERT INTO `ota_district` VALUES ('34158', '东源县', 'heyuan', '1', '34158');
INSERT INTO `ota_district` VALUES ('34159', '南湖区', 'jiaxing', '1', '34159');
INSERT INTO `ota_district` VALUES ('34160', '金堂县', 'chengdu', '1', '34160');
INSERT INTO `ota_district` VALUES ('34161', '石鼓区', 'hengyang', '1', '34161');
INSERT INTO `ota_district` VALUES ('34162', '崇礼县', 'zhangjiakou', '1', '34162');
INSERT INTO `ota_district` VALUES ('34163', '花垣县', 'xiangxi', '1', '34163');
INSERT INTO `ota_district` VALUES ('34164', '大观区', 'anqing', '1', '34164');
INSERT INTO `ota_district` VALUES ('34165', '西湖区', 'hangzhou', '1', '34165');
INSERT INTO `ota_district` VALUES ('34166', '察哈尔右翼前旗', 'wulanchabu', '1', '34166');
INSERT INTO `ota_district` VALUES ('34167', '坊子区', 'weifang', '1', '34167');
INSERT INTO `ota_district` VALUES ('34168', '掇刀区', 'jingmen', '1', '34168');
INSERT INTO `ota_district` VALUES ('34169', '崇安区', 'wuxi', '1', '34169');
INSERT INTO `ota_district` VALUES ('34170', '恒山区', 'jixi', '1', '34170');
INSERT INTO `ota_district` VALUES ('34171', '巴楚县', 'kashi', '1', '34171');
INSERT INTO `ota_district` VALUES ('34172', '郸城县', 'zhoukou', '1', '34172');
INSERT INTO `ota_district` VALUES ('34173', '九里区', 'xuzhou', '1', '34173');
INSERT INTO `ota_district` VALUES ('34174', '鄂温克族自治旗', 'hulunbeier', '1', '34174');
INSERT INTO `ota_district` VALUES ('34176', '奎文区', 'weifang', '1', '34176');
INSERT INTO `ota_district` VALUES ('34177', '老边区', 'yingkou', '1', '34177');
INSERT INTO `ota_district` VALUES ('34178', '温江区', 'chengdu', '1', '34178');
INSERT INTO `ota_district` VALUES ('34179', '万载县', 'yichun_jiangxi', '1', '34179');
INSERT INTO `ota_district` VALUES ('34180', '怀宁县', 'anqing', '1', '34180');
INSERT INTO `ota_district` VALUES ('34181', '泰宁县', 'sanming', '1', '34181');
INSERT INTO `ota_district` VALUES ('34182', '麻江县', 'qiandongnan', '1', '34182');
INSERT INTO `ota_district` VALUES ('34183', '荷塘区', 'zhuzhou', '1', '34183');
INSERT INTO `ota_district` VALUES ('34184', '南山区', 'shenzhen', '1', '34184');
INSERT INTO `ota_district` VALUES ('34185', '克山县', 'qiqihaer', '1', '34185');
INSERT INTO `ota_district` VALUES ('34186', '聂拉木县', 'rikaze', '1', '34186');
INSERT INTO `ota_district` VALUES ('34187', '和平县', 'heyuan', '1', '34187');
INSERT INTO `ota_district` VALUES ('34188', '轮台县', 'bayinguoleng', '1', '34188');
INSERT INTO `ota_district` VALUES ('34189', '宕昌县', 'longnan', '1', '34189');
INSERT INTO `ota_district` VALUES ('34190', '柏乡县', 'xingtai', '1', '34190');
INSERT INTO `ota_district` VALUES ('34191', '庐阳区', 'hefei', '1', '34191');
INSERT INTO `ota_district` VALUES ('34192', '昌黎县', 'qinhuangdao', '1', '34192');
INSERT INTO `ota_district` VALUES ('34193', '古冶区', 'tangshan', '1', '34193');
INSERT INTO `ota_district` VALUES ('34194', '万全县', 'zhangjiakou', '1', '34194');
INSERT INTO `ota_district` VALUES ('34195', '贵德县', 'hainanzangzu', '1', '34195');
INSERT INTO `ota_district` VALUES ('34196', '临县', 'lvliang', '1', '34196');
INSERT INTO `ota_district` VALUES ('34197', '西吉县', 'guyuan', '1', '34197');
INSERT INTO `ota_district` VALUES ('34198', '惠农区', 'shizuishan', '1', '34198');
INSERT INTO `ota_district` VALUES ('34199', '永和县', 'linfen', '1', '34199');
INSERT INTO `ota_district` VALUES ('34200', '淳安县', 'hangzhou', '1', '34200');
INSERT INTO `ota_district` VALUES ('34201', '万秀区', 'wuzhou', '1', '34201');
INSERT INTO `ota_district` VALUES ('34202', '石泉县', 'ankang', '1', '34202');
INSERT INTO `ota_district` VALUES ('34203', '交口县', 'lvliang', '1', '34203');
INSERT INTO `ota_district` VALUES ('34204', '连城县', 'longyan', '1', '34204');
INSERT INTO `ota_district` VALUES ('34205', '越城区', 'shaoxing', '1', '34205');
INSERT INTO `ota_district` VALUES ('34206', '怀仁县', 'shuozhou', '1', '34206');
INSERT INTO `ota_district` VALUES ('34207', '霞浦县', 'ningde', '1', '34207');
INSERT INTO `ota_district` VALUES ('34208', '东城区', 'beijing_city', '1', '34208');
INSERT INTO `ota_district` VALUES ('34209', '内黄县', 'anyang', '1', '34209');
INSERT INTO `ota_district` VALUES ('34210', '阳曲县', 'taiyuan', '1', '34210');
INSERT INTO `ota_district` VALUES ('34211', '阿坝县', 'aba', '1', '34211');
INSERT INTO `ota_district` VALUES ('34212', '嘉鱼县', 'xianning', '1', '34212');
INSERT INTO `ota_district` VALUES ('34213', '霍城县', 'yili', '1', '34213');
INSERT INTO `ota_district` VALUES ('34214', '罗甸县', 'qiannan', '1', '34214');
INSERT INTO `ota_district` VALUES ('34215', '历下区', 'jinan', '1', '34215');
INSERT INTO `ota_district` VALUES ('34216', '越西县', 'liangshan', '1', '34216');
INSERT INTO `ota_district` VALUES ('34217', '白玉县', 'ganzi', '1', '34217');
INSERT INTO `ota_district` VALUES ('34218', '沁水县', 'jincheng', '1', '34218');
INSERT INTO `ota_district` VALUES ('34219', '润州区', 'zhenjiang', '1', '34219');
INSERT INTO `ota_district` VALUES ('34220', '华安县', 'zhangzhou', '1', '34220');
INSERT INTO `ota_district` VALUES ('34221', '潢川县', 'xinyang', '1', '34221');
INSERT INTO `ota_district` VALUES ('34222', '英山县', 'huanggang', '1', '34222');
INSERT INTO `ota_district` VALUES ('34223', '康县', 'longnan', '1', '34223');
INSERT INTO `ota_district` VALUES ('34224', '北塘区', 'wuxi', '1', '34224');
INSERT INTO `ota_district` VALUES ('34225', '碟山区', 'wuzhou', '1', '34225');
INSERT INTO `ota_district` VALUES ('34226', '镜湖区', 'wuhu', '1', '34226');
INSERT INTO `ota_district` VALUES ('34227', '喀什地区', 'kashi', '1', '34227');
INSERT INTO `ota_district` VALUES ('34228', '固阳县', 'baotou', '1', '34228');
INSERT INTO `ota_district` VALUES ('34229', '定海区', 'zhoushan', '1', '34229');
INSERT INTO `ota_district` VALUES ('34230', '连云区', 'lianyungang', '1', '34230');
INSERT INTO `ota_district` VALUES ('34231', '迁西县', 'tangshan', '1', '34231');
INSERT INTO `ota_district` VALUES ('34232', '沙依巴克区', 'wulumuqi', '1', '34232');
INSERT INTO `ota_district` VALUES ('34233', '萨尔图区', 'daqing', '1', '34233');
INSERT INTO `ota_district` VALUES ('34234', '婺源县', 'shangrao', '1', '34234');
INSERT INTO `ota_district` VALUES ('34235', '七星区', 'guilin', '1', '34235');
INSERT INTO `ota_district` VALUES ('34236', '鼓楼区', 'fuzhou_fujian', '1', '34236');
INSERT INTO `ota_district` VALUES ('34237', '安岳县', 'ziyang', '1', '34237');
INSERT INTO `ota_district` VALUES ('34238', '甘谷县', 'tianshui', '1', '34238');
INSERT INTO `ota_district` VALUES ('34239', '仲巴县', 'rikaze', '1', '34239');
INSERT INTO `ota_district` VALUES ('34240', '当涂县', 'maanshan', '1', '34240');
INSERT INTO `ota_district` VALUES ('34241', '陵水黎族自治县', 'lingshui', '1', '34241');
INSERT INTO `ota_district` VALUES ('34242', '新田县', 'yongzhou', '1', '34242');
INSERT INTO `ota_district` VALUES ('34243', '施甸县', 'baoshan', '1', '34243');
INSERT INTO `ota_district` VALUES ('34244', '绩溪县', 'xuancheng', '1', '34244');
INSERT INTO `ota_district` VALUES ('34245', '抚远县', 'jiamusi', '1', '34245');
INSERT INTO `ota_district` VALUES ('34246', '东川区', 'kunming', '1', '34246');
INSERT INTO `ota_district` VALUES ('34247', '汤阴县', 'anyang', '1', '34247');
INSERT INTO `ota_district` VALUES ('34248', '织金县', 'bijie', '1', '34248');
INSERT INTO `ota_district` VALUES ('34249', '宁远县', 'yongzhou', '1', '34249');
INSERT INTO `ota_district` VALUES ('34250', '桓台县', 'zibo', '1', '34250');
INSERT INTO `ota_district` VALUES ('34251', '渑池县', 'sanmenxia', '1', '34251');
INSERT INTO `ota_district` VALUES ('34252', '弥渡县', 'dali', '1', '34252');
INSERT INTO `ota_district` VALUES ('34253', '沐川县', 'leshan', '1', '34253');
INSERT INTO `ota_district` VALUES ('34254', '清河门区', 'fuxin', '1', '34254');
INSERT INTO `ota_district` VALUES ('34255', '阳谷县', 'liaocheng', '1', '34255');
INSERT INTO `ota_district` VALUES ('34256', '延边朝鲜族自治州', 'yanbian', '1', '34256');
INSERT INTO `ota_district` VALUES ('34257', '鱼台县', 'jining', '1', '34257');
INSERT INTO `ota_district` VALUES ('34258', '延长县', 'yanan', '1', '34258');
INSERT INTO `ota_district` VALUES ('34259', '连江县', 'fuzhou_fujian', '1', '34259');
INSERT INTO `ota_district` VALUES ('34260', '五通桥区', 'leshan', '1', '34260');
INSERT INTO `ota_district` VALUES ('34261', '永胜县', 'lijiang', '1', '34261');
INSERT INTO `ota_district` VALUES ('34262', '临泉县', 'fuyang_anhui', '1', '34262');
INSERT INTO `ota_district` VALUES ('34263', '光山县', 'xinyang', '1', '34263');
INSERT INTO `ota_district` VALUES ('34264', '蠡县', 'baoding', '1', '34264');
INSERT INTO `ota_district` VALUES ('34265', '沾益县', 'qujing', '1', '34265');
INSERT INTO `ota_district` VALUES ('34266', '昭平县', 'hezhou', '1', '34266');
INSERT INTO `ota_district` VALUES ('34267', '集美区', 'xiamen', '1', '34267');
INSERT INTO `ota_district` VALUES ('34268', '固安县', 'langfang', '1', '34268');
INSERT INTO `ota_district` VALUES ('34269', '晋宁县', 'kunming', '1', '34269');
INSERT INTO `ota_district` VALUES ('34270', '汝城县', 'chenzhou', '1', '34270');
INSERT INTO `ota_district` VALUES ('34271', '班戈县', 'naqu', '1', '34271');
INSERT INTO `ota_district` VALUES ('34272', '元阳县', 'honghe', '1', '34272');
INSERT INTO `ota_district` VALUES ('34273', '铜梁县', 'chongqing_city', '1', '34273');
INSERT INTO `ota_district` VALUES ('34274', '东港区', 'rizhao', '1', '34274');
INSERT INTO `ota_district` VALUES ('34275', '宣恩县', 'enshi', '1', '34275');
INSERT INTO `ota_district` VALUES ('34276', '琅琊区', 'chuzhou', '1', '34276');
INSERT INTO `ota_district` VALUES ('34277', '高唐县', 'liaocheng', '1', '34277');
INSERT INTO `ota_district` VALUES ('34278', '丰顺县', 'meizhou', '1', '34278');
INSERT INTO `ota_district` VALUES ('34279', '麦积区', 'tianshui', '1', '34279');
INSERT INTO `ota_district` VALUES ('34280', '睢宁县', 'xuzhou', '1', '34280');
INSERT INTO `ota_district` VALUES ('34281', '南部县', 'nanchong', '1', '34281');
INSERT INTO `ota_district` VALUES ('34282', '高淳县', 'nanjing', '1', '34282');
INSERT INTO `ota_district` VALUES ('34283', '木兰县', 'haerbin', '1', '34283');
INSERT INTO `ota_district` VALUES ('34284', '扎囊县', 'shannan', '1', '34284');
INSERT INTO `ota_district` VALUES ('34285', '单县', 'heze', '1', '34285');
INSERT INTO `ota_district` VALUES ('34286', '洪泽县', 'huaian', '1', '34286');
INSERT INTO `ota_district` VALUES ('34287', '汶上县', 'jining', '1', '34287');
INSERT INTO `ota_district` VALUES ('34288', '淮上区', 'bangbu', '1', '34288');
INSERT INTO `ota_district` VALUES ('34289', '君山区', 'yueyang', '1', '34289');
INSERT INTO `ota_district` VALUES ('34290', '察布查尔锡伯自治县', 'yili', '1', '34290');
INSERT INTO `ota_district` VALUES ('34291', '绥中县', 'huludao', '1', '34291');
INSERT INTO `ota_district` VALUES ('34292', '西青区', 'tianjin_city', '1', '34292');
INSERT INTO `ota_district` VALUES ('34293', '雄县', 'baoding', '1', '34293');
INSERT INTO `ota_district` VALUES ('34294', '尖山区', 'shuangyashan', '1', '34294');
INSERT INTO `ota_district` VALUES ('34295', '孙吴县', 'heihe', '1', '34295');
INSERT INTO `ota_district` VALUES ('34296', '察雅县', 'changdu', '1', '34296');
INSERT INTO `ota_district` VALUES ('34297', '新龙县', 'ganzi', '1', '34297');
INSERT INTO `ota_district` VALUES ('34298', '鹿城区', 'wenzhou', '1', '34298');
INSERT INTO `ota_district` VALUES ('34299', '水城县', 'liupanshui', '1', '34299');
INSERT INTO `ota_district` VALUES ('34300', '大同区', 'daqing', '1', '34300');
INSERT INTO `ota_district` VALUES ('34301', '文水县', 'lvliang', '1', '34301');
INSERT INTO `ota_district` VALUES ('34302', '裕民县', 'tacheng', '1', '34302');
INSERT INTO `ota_district` VALUES ('34303', '长武县', 'xianyang', '1', '34303');
INSERT INTO `ota_district` VALUES ('34304', '巴林右旗', 'chifeng', '1', '34304');
INSERT INTO `ota_district` VALUES ('34305', '浦江县', 'jinhua', '1', '34305');
INSERT INTO `ota_district` VALUES ('34306', '乌拉特前旗', 'bayannaoer', '1', '34306');
INSERT INTO `ota_district` VALUES ('34307', '通川区', 'dazhou', '1', '34307');
INSERT INTO `ota_district` VALUES ('34308', '成武县', 'heze', '1', '34308');
INSERT INTO `ota_district` VALUES ('34309', '瓜州县', 'jiuquan', '1', '34309');
INSERT INTO `ota_district` VALUES ('34310', '新丰县', 'shaoguan', '1', '34310');
INSERT INTO `ota_district` VALUES ('34311', '连平县', 'heyuan', '1', '34311');
INSERT INTO `ota_district` VALUES ('34312', '凤庆县', 'lincang', '1', '34312');
INSERT INTO `ota_district` VALUES ('34313', '海州区', 'fuxin', '1', '34313');
INSERT INTO `ota_district` VALUES ('34314', '云溪区', 'yueyang', '1', '34314');
INSERT INTO `ota_district` VALUES ('34315', '天桥区', 'jinan', '1', '34315');
INSERT INTO `ota_district` VALUES ('34316', '汝南县', 'zhumadian', '1', '34316');
INSERT INTO `ota_district` VALUES ('34317', '将乐县', 'sanming', '1', '34317');
INSERT INTO `ota_district` VALUES ('34318', '盐田区', 'shenzhen', '1', '34318');
INSERT INTO `ota_district` VALUES ('34319', '通许县', 'kaifeng', '1', '34319');
INSERT INTO `ota_district` VALUES ('34320', '九原区', 'baotou', '1', '34320');
INSERT INTO `ota_district` VALUES ('34321', '托克逊县', 'tulufan', '1', '34321');
INSERT INTO `ota_district` VALUES ('34322', '仙游县', 'putian', '1', '34322');
INSERT INTO `ota_district` VALUES ('34323', '田东县', 'baise', '1', '34323');
INSERT INTO `ota_district` VALUES ('34324', '全州县', 'guilin', '1', '34324');
INSERT INTO `ota_district` VALUES ('34325', '南丹县', 'hechi', '1', '34325');
INSERT INTO `ota_district` VALUES ('34326', '遂昌县', 'lishui', '1', '34326');
INSERT INTO `ota_district` VALUES ('34327', '钦北区', 'qinzhou', '1', '34327');
INSERT INTO `ota_district` VALUES ('34328', '西夏区', 'yinchuan', '1', '34328');
INSERT INTO `ota_district` VALUES ('34329', '仪陇县', 'nanchong', '1', '34329');
INSERT INTO `ota_district` VALUES ('34330', '凤台县', 'huainan', '1', '34330');
INSERT INTO `ota_district` VALUES ('34331', '杜尔伯特蒙古族自治县', 'daqing', '1', '34331');
INSERT INTO `ota_district` VALUES ('34332', '文县', 'longnan', '1', '34332');
INSERT INTO `ota_district` VALUES ('34333', '阿瓦提县', 'akesu', '1', '34333');
INSERT INTO `ota_district` VALUES ('34334', '长岭县', 'songyuan', '1', '34334');
INSERT INTO `ota_district` VALUES ('34335', '红山区', 'chifeng', '1', '34335');
INSERT INTO `ota_district` VALUES ('34336', '黄埔区', 'guangzhou', '1', '34336');
INSERT INTO `ota_district` VALUES ('34337', '海勃湾区', 'wuhai', '1', '34337');
INSERT INTO `ota_district` VALUES ('34338', '献县', 'cangzhou', '1', '34338');
INSERT INTO `ota_district` VALUES ('34339', '安宁区', 'lanzhou', '1', '34339');
INSERT INTO `ota_district` VALUES ('34340', '天山区', 'wulumuqi', '1', '34340');
INSERT INTO `ota_district` VALUES ('34341', '萨迦县', 'rikaze', '1', '34341');
INSERT INTO `ota_district` VALUES ('34342', '铜官山区', 'tongling', '1', '34342');
INSERT INTO `ota_district` VALUES ('34343', '衡山县', 'hengyang', '1', '34343');
INSERT INTO `ota_district` VALUES ('34344', '积石山保安族东乡族撒拉族自治县', 'linxia', '1', '34344');
INSERT INTO `ota_district` VALUES ('34345', '小河区', 'guiyang', '1', '34345');
INSERT INTO `ota_district` VALUES ('34346', '墨玉县', 'hetian', '1', '34346');
INSERT INTO `ota_district` VALUES ('34347', '颍东区', 'fuyang_anhui', '1', '34347');
INSERT INTO `ota_district` VALUES ('34348', '栖霞区', 'nanjing', '1', '34348');
INSERT INTO `ota_district` VALUES ('34349', '安定区', 'dingxi', '1', '34349');
INSERT INTO `ota_district` VALUES ('34350', '衢江区', 'quzhou', '1', '34350');
INSERT INTO `ota_district` VALUES ('34351', '建昌县', 'huludao', '1', '34351');
INSERT INTO `ota_district` VALUES ('34352', '长泰县', 'zhangzhou', '1', '34352');
INSERT INTO `ota_district` VALUES ('34353', '辽中县', 'shenyang', '1', '34353');
INSERT INTO `ota_district` VALUES ('34354', '金平苗族瑶族傣族自治县', 'honghe', '1', '34354');
INSERT INTO `ota_district` VALUES ('34355', '珙县', 'yibin', '1', '34355');
INSERT INTO `ota_district` VALUES ('34356', '纳雍县', 'bijie', '1', '34356');
INSERT INTO `ota_district` VALUES ('34357', '松山区', 'chifeng', '1', '34357');
INSERT INTO `ota_district` VALUES ('34358', '阎良区', 'xian', '1', '34358');
INSERT INTO `ota_district` VALUES ('34359', '水富县', 'zhaotong', '1', '34359');
INSERT INTO `ota_district` VALUES ('34360', '郧西县', 'shiyan', '1', '34360');
INSERT INTO `ota_district` VALUES ('34361', '绍兴县', 'shaoxing', '1', '34361');
INSERT INTO `ota_district` VALUES ('34362', '青白江区', 'chengdu', '1', '34362');
INSERT INTO `ota_district` VALUES ('34363', '安源区', 'pingxiang', '1', '34363');
INSERT INTO `ota_district` VALUES ('34364', '松北区', 'haerbin', '1', '34364');
INSERT INTO `ota_district` VALUES ('34365', '武陟县', 'jiaozuo', '1', '34365');
INSERT INTO `ota_district` VALUES ('34366', '洛江区', 'quanzhou', '1', '34366');
INSERT INTO `ota_district` VALUES ('34367', '佛坪县', 'hanzhong', '1', '34367');
INSERT INTO `ota_district` VALUES ('34368', '平川区', 'baiyin', '1', '34368');
INSERT INTO `ota_district` VALUES ('34369', '新安县', 'luoyang', '1', '34369');
INSERT INTO `ota_district` VALUES ('34370', '江宁区', 'nanjing', '1', '34370');
INSERT INTO `ota_district` VALUES ('34371', '锦江区', 'chengdu', '1', '34371');
INSERT INTO `ota_district` VALUES ('34372', '宿城区', 'suqian', '1', '34372');
INSERT INTO `ota_district` VALUES ('34373', '复兴区', 'handan', '1', '34373');
INSERT INTO `ota_district` VALUES ('34374', '临颍县', 'luohe', '1', '34374');
INSERT INTO `ota_district` VALUES ('34375', '资阳区', 'yiyang', '1', '34375');
INSERT INTO `ota_district` VALUES ('34376', '资源县', 'guilin', '1', '34376');
INSERT INTO `ota_district` VALUES ('34377', '丰宁满族自治县', 'chengde', '1', '34377');
INSERT INTO `ota_district` VALUES ('34378', '环翠区', 'weihai', '1', '34378');
INSERT INTO `ota_district` VALUES ('34379', '阳朔县', 'guilin', '1', '34379');
INSERT INTO `ota_district` VALUES ('34380', '桥西区', 'xingtai', '1', '34380');
INSERT INTO `ota_district` VALUES ('34381', '南明区', 'guiyang', '1', '34381');
INSERT INTO `ota_district` VALUES ('34382', '大东区', 'shenyang', '1', '34382');
INSERT INTO `ota_district` VALUES ('34383', '澄江县', 'yuxi', '1', '34383');
INSERT INTO `ota_district` VALUES ('34384', '罗庄区', 'linyi', '1', '34384');
INSERT INTO `ota_district` VALUES ('34385', '南澳县', 'shantou', '1', '34385');
INSERT INTO `ota_district` VALUES ('34386', '慈利县', 'zhangjiajie', '1', '34386');
INSERT INTO `ota_district` VALUES ('34387', '繁峙县', 'xinzhou', '1', '34387');
INSERT INTO `ota_district` VALUES ('34388', '惠民县', 'binzhou', '1', '34388');
INSERT INTO `ota_district` VALUES ('34389', '雨花台区', 'nanjing', '1', '34389');
INSERT INTO `ota_district` VALUES ('34390', '东昌府区', 'liaocheng', '1', '34390');
INSERT INTO `ota_district` VALUES ('34391', '鼎城区', 'changde', '1', '34391');
INSERT INTO `ota_district` VALUES ('34392', '德安县', 'jiujiang', '1', '34392');
INSERT INTO `ota_district` VALUES ('34393', '二道江区', 'tonghua', '1', '34393');
INSERT INTO `ota_district` VALUES ('34394', '平鲁区', 'shuozhou', '1', '34394');
INSERT INTO `ota_district` VALUES ('34395', '琼结县', 'shannan', '1', '34395');
INSERT INTO `ota_district` VALUES ('34396', '下城区', 'hangzhou', '1', '34396');
INSERT INTO `ota_district` VALUES ('34397', '巴音郭楞蒙古自治州', 'bayinguoleng', '1', '34397');
INSERT INTO `ota_district` VALUES ('34398', '紫金县', 'heyuan', '1', '34398');
INSERT INTO `ota_district` VALUES ('34399', '青山湖区', 'nanchang', '1', '34399');
INSERT INTO `ota_district` VALUES ('34400', '林甸县', 'daqing', '1', '34400');
INSERT INTO `ota_district` VALUES ('34401', '辰溪县', 'huaihua', '1', '34401');
INSERT INTO `ota_district` VALUES ('34402', '雁江区', 'ziyang', '1', '34402');
INSERT INTO `ota_district` VALUES ('34403', '刚察县', 'haibei', '1', '34403');
INSERT INTO `ota_district` VALUES ('34404', '垦利县', 'dongying', '1', '34404');
INSERT INTO `ota_district` VALUES ('34405', '江口县', 'tongren', '1', '34405');
INSERT INTO `ota_district` VALUES ('34406', '灵山县', 'qinzhou', '1', '34406');
INSERT INTO `ota_district` VALUES ('34407', '勃利县', 'qitaihe', '1', '34407');
INSERT INTO `ota_district` VALUES ('34408', '巧家县', 'zhaotong', '1', '34408');
INSERT INTO `ota_district` VALUES ('34409', '襄垣县', 'changzhi', '1', '34409');
INSERT INTO `ota_district` VALUES ('34410', '海盐县', 'jiaxing', '1', '34410');
INSERT INTO `ota_district` VALUES ('34411', '金家庄区', 'maanshan', '1', '34411');
INSERT INTO `ota_district` VALUES ('34412', '鮁鱼圈区', 'yingkou', '1', '34412');
INSERT INTO `ota_district` VALUES ('34413', '水磨沟区', 'wulumuqi', '1', '34413');
INSERT INTO `ota_district` VALUES ('34414', '丛台区', 'handan', '1', '34414');
INSERT INTO `ota_district` VALUES ('34415', '大通区', 'huainan', '1', '34415');
INSERT INTO `ota_district` VALUES ('34416', '突泉县', 'xingan', '1', '34416');
INSERT INTO `ota_district` VALUES ('34417', '会理县', 'liangshan', '1', '34417');
INSERT INTO `ota_district` VALUES ('34418', '赣县', 'ganzhou', '1', '34418');
INSERT INTO `ota_district` VALUES ('34419', '召陵区', 'luohe', '1', '34419');
INSERT INTO `ota_district` VALUES ('34420', '光泽县', 'nanping', '1', '34420');
INSERT INTO `ota_district` VALUES ('34421', '武城县', 'dezhou', '1', '34421');
INSERT INTO `ota_district` VALUES ('34422', '铜陵县', 'tongling', '1', '34422');
INSERT INTO `ota_district` VALUES ('34423', '林西县', 'chifeng', '1', '34423');
INSERT INTO `ota_district` VALUES ('34424', '南谯区', 'chuzhou', '1', '34424');
INSERT INTO `ota_district` VALUES ('34425', '寻乌县', 'ganzhou', '1', '34425');
INSERT INTO `ota_district` VALUES ('34426', '赤坎区', 'zhanjiang', '1', '34426');
INSERT INTO `ota_district` VALUES ('34427', '偏关县', 'xinzhou', '1', '34427');
INSERT INTO `ota_district` VALUES ('34428', '钢城区', 'laiwu', '1', '34428');
INSERT INTO `ota_district` VALUES ('34429', '泾阳县', 'xianyang', '1', '34429');
INSERT INTO `ota_district` VALUES ('34430', '会昌县', 'ganzhou', '1', '34430');
INSERT INTO `ota_district` VALUES ('34431', '巩留县', 'yili', '1', '34431');
INSERT INTO `ota_district` VALUES ('34432', '阳东县', 'yangjiang', '1', '34432');
INSERT INTO `ota_district` VALUES ('34433', '榆阳区', 'yulin_shanxi_02', '1', '34433');
INSERT INTO `ota_district` VALUES ('34434', '贡山独龙族怒族自治县', 'nujiang', '1', '34434');
INSERT INTO `ota_district` VALUES ('34435', '平南县', 'guigang', '1', '34435');
INSERT INTO `ota_district` VALUES ('34436', '沙雅县', 'akesu', '1', '34436');
INSERT INTO `ota_district` VALUES ('34437', '洛隆县', 'changdu', '1', '34437');
INSERT INTO `ota_district` VALUES ('34438', '梁子湖区', 'ezhou', '1', '34438');
INSERT INTO `ota_district` VALUES ('34439', '奇台县', 'changji', '1', '34439');
INSERT INTO `ota_district` VALUES ('34440', '肃北蒙古族自治县', 'jiuquan', '1', '34440');
INSERT INTO `ota_district` VALUES ('34441', '平乐县', 'guilin', '1', '34441');
INSERT INTO `ota_district` VALUES ('34442', '饶河县', 'shuangyashan', '1', '34442');
INSERT INTO `ota_district` VALUES ('34443', '顺庆区', 'nanchong', '1', '34443');
INSERT INTO `ota_district` VALUES ('34444', '伊宁县', 'yili', '1', '34444');
INSERT INTO `ota_district` VALUES ('34445', '沙湾县', 'tacheng', '1', '34445');
INSERT INTO `ota_district` VALUES ('34446', '泸溪县', 'xiangxi', '1', '34446');
INSERT INTO `ota_district` VALUES ('34447', '宝山区', 'shuangyashan', '1', '34447');
INSERT INTO `ota_district` VALUES ('34448', '彰武县', 'fuxin', '1', '34448');
INSERT INTO `ota_district` VALUES ('34449', '达日县', 'guoluo', '1', '34449');
INSERT INTO `ota_district` VALUES ('34450', '闽侯县', 'fuzhou_fujian', '1', '34450');
INSERT INTO `ota_district` VALUES ('34451', '南川区', 'chongqing_city', '1', '34451');
INSERT INTO `ota_district` VALUES ('34452', '建平县', 'chaoyang', '1', '34452');
INSERT INTO `ota_district` VALUES ('34453', '南召县', 'nanyang', '1', '34453');
INSERT INTO `ota_district` VALUES ('34454', '拜泉县', 'qiqihaer', '1', '34454');
INSERT INTO `ota_district` VALUES ('34455', '平房区', 'haerbin', '1', '34455');
INSERT INTO `ota_district` VALUES ('34456', '亭湖区', 'yancheng', '1', '34456');
INSERT INTO `ota_district` VALUES ('34457', '陇西县', 'dingxi', '1', '34457');
INSERT INTO `ota_district` VALUES ('34458', '包河区', 'hefei', '1', '34458');
INSERT INTO `ota_district` VALUES ('34459', '平原县', 'dezhou', '1', '34459');
INSERT INTO `ota_district` VALUES ('34460', '淮阳县', 'zhoukou', '1', '34460');
INSERT INTO `ota_district` VALUES ('34461', '太平区', 'fuxin', '1', '34461');
INSERT INTO `ota_district` VALUES ('34462', '泾县', 'xuancheng', '1', '34462');
INSERT INTO `ota_district` VALUES ('34463', '普陀区', 'zhoushan', '1', '34463');
INSERT INTO `ota_district` VALUES ('34464', '馆陶县', 'handan', '1', '34464');
INSERT INTO `ota_district` VALUES ('34465', '芮城县', 'yuncheng', '1', '34465');
INSERT INTO `ota_district` VALUES ('34466', '襄城县', 'xuchang', '1', '34466');
INSERT INTO `ota_district` VALUES ('34468', '宝兴县', 'yaan', '1', '34468');
INSERT INTO `ota_district` VALUES ('34469', '龙安区', 'anyang', '1', '34469');
INSERT INTO `ota_district` VALUES ('34470', '通榆县', 'baicheng', '1', '34470');
INSERT INTO `ota_district` VALUES ('34471', '灵川县', 'guilin', '1', '34471');
INSERT INTO `ota_district` VALUES ('34472', '金凤区', 'yinchuan', '1', '34472');
INSERT INTO `ota_district` VALUES ('34473', '岑巩县', 'qiandongnan', '1', '34473');
INSERT INTO `ota_district` VALUES ('34474', '泗水县', 'jining', '1', '34474');
INSERT INTO `ota_district` VALUES ('34475', '茂南区', 'maoming', '1', '34475');
INSERT INTO `ota_district` VALUES ('34476', '比如县', 'naqu', '1', '34476');
INSERT INTO `ota_district` VALUES ('34477', '东丰县', 'liaoyuan', '1', '34477');
INSERT INTO `ota_district` VALUES ('34478', '南和县', 'xingtai', '1', '34478');
INSERT INTO `ota_district` VALUES ('34479', '赫山区', 'yiyang', '1', '34479');
INSERT INTO `ota_district` VALUES ('34480', '下关区', 'nanjing', '1', '34480');
INSERT INTO `ota_district` VALUES ('34481', '桑植县', 'zhangjiajie', '1', '34481');
INSERT INTO `ota_district` VALUES ('34482', '广宗县', 'xingtai', '1', '34482');
INSERT INTO `ota_district` VALUES ('34483', '克拉玛依区', 'kelamayi', '1', '34483');
INSERT INTO `ota_district` VALUES ('34484', '荣县', 'zigong', '1', '34484');
INSERT INTO `ota_district` VALUES ('34485', '许昌县', 'xuchang', '1', '34485');
INSERT INTO `ota_district` VALUES ('34486', '海南区', 'wuhai', '1', '34486');
INSERT INTO `ota_district` VALUES ('34487', '南汇区', 'shanghai_city', '1', '34487');
INSERT INTO `ota_district` VALUES ('34488', '云龙县', 'dali', '1', '34488');
INSERT INTO `ota_district` VALUES ('34489', '丰满区', 'jilin_city', '1', '34489');
INSERT INTO `ota_district` VALUES ('34490', '彝良县', 'zhaotong', '1', '34490');
INSERT INTO `ota_district` VALUES ('34491', '南山区', 'hegang', '1', '34491');
INSERT INTO `ota_district` VALUES ('34492', '官渡区', 'kunming', '1', '34492');
INSERT INTO `ota_district` VALUES ('34493', '天台县', 'taizhou_zhejiang', '1', '34493');
INSERT INTO `ota_district` VALUES ('34494', '浦东新区', 'shanghai_city', '1', '34494');
INSERT INTO `ota_district` VALUES ('34495', '梁山县', 'jining', '1', '34495');
INSERT INTO `ota_district` VALUES ('34496', '旌德县', 'xuancheng', '1', '34496');
INSERT INTO `ota_district` VALUES ('34497', '信丰县', 'ganzhou', '1', '34497');
INSERT INTO `ota_district` VALUES ('34498', '深泽县', 'shijiazhuang', '1', '34498');
INSERT INTO `ota_district` VALUES ('34499', '天峨县', 'hechi', '1', '34499');
INSERT INTO `ota_district` VALUES ('34500', '古蔺县', 'luzhou', '1', '34500');
INSERT INTO `ota_district` VALUES ('34501', '永登县', 'lanzhou', '1', '34501');
INSERT INTO `ota_district` VALUES ('34502', '赛罕区', 'huhehaote', '1', '34502');
INSERT INTO `ota_district` VALUES ('34503', '杜集区', 'huaibei', '1', '34503');
INSERT INTO `ota_district` VALUES ('34504', '喜德县', 'liangshan', '1', '34504');
INSERT INTO `ota_district` VALUES ('34505', '鄂城区', 'ezhou', '1', '34505');
INSERT INTO `ota_district` VALUES ('34506', '凤泉区', 'xinxiang', '1', '34506');
INSERT INTO `ota_district` VALUES ('34507', '李沧区', 'qingdao', '1', '34507');
INSERT INTO `ota_district` VALUES ('34508', '崆峒区', 'pingliang', '1', '34508');
INSERT INTO `ota_district` VALUES ('34509', '紫阳县', 'ankang', '1', '34509');
INSERT INTO `ota_district` VALUES ('34510', '临城县', 'xingtai', '1', '34510');
INSERT INTO `ota_district` VALUES ('34511', '成华区', 'chengdu', '1', '34511');
INSERT INTO `ota_district` VALUES ('34512', '石龙区', 'pingdingshan', '1', '34512');
INSERT INTO `ota_district` VALUES ('34513', '绥宁县', 'shaoyang', '1', '34513');
INSERT INTO `ota_district` VALUES ('34514', '金秀瑶族自治县', 'laibin', '1', '34514');
INSERT INTO `ota_district` VALUES ('34515', '青山区', 'baotou', '1', '34515');
INSERT INTO `ota_district` VALUES ('34516', '贡井区', 'zigong', '1', '34516');
INSERT INTO `ota_district` VALUES ('34517', '新城区', 'huhehaote', '1', '34517');
INSERT INTO `ota_district` VALUES ('34518', '月湖区', 'yingtan', '1', '34518');
INSERT INTO `ota_district` VALUES ('34519', '疏附县', 'kashi', '1', '34519');
INSERT INTO `ota_district` VALUES ('34520', '仁化县', 'shaoguan', '1', '34520');
INSERT INTO `ota_district` VALUES ('34521', '鼓楼区', 'nanjing', '1', '34521');
INSERT INTO `ota_district` VALUES ('34522', '垫江县', 'chongqing_city', '1', '34522');
INSERT INTO `ota_district` VALUES ('34523', '福贡县', 'nujiang', '1', '34523');
INSERT INTO `ota_district` VALUES ('34524', '海陵区', 'taizhou_jiangsu', '1', '34524');
INSERT INTO `ota_district` VALUES ('34525', '朝阳区', 'beijing_city', '1', '34525');
INSERT INTO `ota_district` VALUES ('34526', '高坪区', 'nanchong', '1', '34526');
INSERT INTO `ota_district` VALUES ('34527', '铁西区', 'shenyang', '1', '34527');
INSERT INTO `ota_district` VALUES ('34528', '金口河区', 'leshan', '1', '34528');
INSERT INTO `ota_district` VALUES ('34529', '河口瑶族自治县', 'honghe', '1', '34529');
INSERT INTO `ota_district` VALUES ('34530', '易县', 'baoding', '1', '34530');
INSERT INTO `ota_district` VALUES ('34531', '唐河县', 'nanyang', '1', '34531');
INSERT INTO `ota_district` VALUES ('34532', '隆化县', 'chengde', '1', '34532');
INSERT INTO `ota_district` VALUES ('34533', '东乌珠穆沁旗', 'xilinguole', '1', '34533');
INSERT INTO `ota_district` VALUES ('34534', '都安瑶族自治县', 'hechi', '1', '34534');
INSERT INTO `ota_district` VALUES ('34535', '涟水县', 'huaian', '1', '34535');
INSERT INTO `ota_district` VALUES ('34536', '景宁畲族自治县', 'lishui', '1', '34536');
INSERT INTO `ota_district` VALUES ('34537', '东营区', 'dongying', '1', '34537');
INSERT INTO `ota_district` VALUES ('34538', '钟山县', 'hezhou', '1', '34538');
INSERT INTO `ota_district` VALUES ('34539', '阜新蒙古族自治县', 'fuxin', '1', '34539');
INSERT INTO `ota_district` VALUES ('34540', '札达县', 'ali', '1', '34540');
INSERT INTO `ota_district` VALUES ('34541', '秦州区', 'tianshui', '1', '34541');
INSERT INTO `ota_district` VALUES ('34542', '丹寨县', 'qiandongnan', '1', '34542');
INSERT INTO `ota_district` VALUES ('34543', '嵩县', 'luoyang', '1', '34543');
INSERT INTO `ota_district` VALUES ('34544', '魏县', 'handan', '1', '34544');
INSERT INTO `ota_district` VALUES ('34545', '和林格尔县', 'huhehaote', '1', '34545');
INSERT INTO `ota_district` VALUES ('34546', '泉山区', 'xuzhou', '1', '34546');
INSERT INTO `ota_district` VALUES ('34547', '向阳区', 'jiamusi', '1', '34547');
INSERT INTO `ota_district` VALUES ('34548', '武胜县', 'guangan', '1', '34548');
INSERT INTO `ota_district` VALUES ('34549', '吴兴区', 'huzhou', '1', '34549');
INSERT INTO `ota_district` VALUES ('34550', '准格尔旗', 'eerduosi', '1', '34550');
INSERT INTO `ota_district` VALUES ('34551', '靖边县', 'yulin_shanxi_02', '1', '34551');
INSERT INTO `ota_district` VALUES ('34552', '肥东县', 'hefei', '1', '34552');
INSERT INTO `ota_district` VALUES ('34553', '隆安县', 'nanning', '1', '34553');
INSERT INTO `ota_district` VALUES ('34554', '顺义区', 'beijing_city', '1', '34554');
INSERT INTO `ota_district` VALUES ('34555', '托里县', 'tacheng', '1', '34555');
INSERT INTO `ota_district` VALUES ('34556', '万柏林区', 'taiyuan', '1', '34556');
INSERT INTO `ota_district` VALUES ('34557', '镇赉县', 'baicheng', '1', '34557');
INSERT INTO `ota_district` VALUES ('34558', '新昌县', 'shaoxing', '1', '34558');
INSERT INTO `ota_district` VALUES ('34559', '兰山区', 'linyi', '1', '34559');
INSERT INTO `ota_district` VALUES ('34560', '户县', 'xian', '1', '34560');
INSERT INTO `ota_district` VALUES ('34561', '昔阳县', 'jinzhong', '1', '34561');
INSERT INTO `ota_district` VALUES ('34562', '卫滨区', 'xinxiang', '1', '34562');
INSERT INTO `ota_district` VALUES ('34563', '安次区', 'langfang', '1', '34563');
INSERT INTO `ota_district` VALUES ('34564', '潘集区', 'huainan', '1', '34564');
INSERT INTO `ota_district` VALUES ('34565', '永兴县', 'chenzhou', '1', '34565');
INSERT INTO `ota_district` VALUES ('34566', '蓟县', 'tianjin_city', '1', '34566');
INSERT INTO `ota_district` VALUES ('34567', '宜宾县', 'yibin', '1', '34567');
INSERT INTO `ota_district` VALUES ('34568', '镇康县', 'lincang', '1', '34568');
INSERT INTO `ota_district` VALUES ('34569', '林周县', 'lasa', '1', '34569');
INSERT INTO `ota_district` VALUES ('34570', '定结县', 'rikaze', '1', '34570');
INSERT INTO `ota_district` VALUES ('34571', '五峰土家族自治县', 'yichang', '1', '34571');
INSERT INTO `ota_district` VALUES ('34572', '武清区', 'tianjin_city', '1', '34572');
INSERT INTO `ota_district` VALUES ('34573', '兴安盟', 'xingan', '1', '34573');
INSERT INTO `ota_district` VALUES ('34574', '赞皇县', 'shijiazhuang', '1', '34574');
INSERT INTO `ota_district` VALUES ('34575', '兴国县', 'ganzhou', '1', '34575');
INSERT INTO `ota_district` VALUES ('34576', '道外区', 'haerbin', '1', '34576');
INSERT INTO `ota_district` VALUES ('34577', '香河县', 'langfang', '1', '34577');
INSERT INTO `ota_district` VALUES ('34578', '德昌县', 'liangshan', '1', '34578');
INSERT INTO `ota_district` VALUES ('34579', '安仁县', 'chenzhou', '1', '34579');
INSERT INTO `ota_district` VALUES ('34580', '孟津县', 'luoyang', '1', '34580');
INSERT INTO `ota_district` VALUES ('34581', '赣榆县', 'lianyungang', '1', '34581');
INSERT INTO `ota_district` VALUES ('34582', '前郭尔罗斯蒙古族自治县', 'songyuan', '1', '34582');
INSERT INTO `ota_district` VALUES ('34583', '千阳县', 'baoji', '1', '34583');
INSERT INTO `ota_district` VALUES ('34584', '莒县', 'rizhao', '1', '34584');
INSERT INTO `ota_district` VALUES ('34585', '靖宇县', 'baishan', '1', '34585');
INSERT INTO `ota_district` VALUES ('34586', '南沙区', 'guangzhou', '1', '34586');
INSERT INTO `ota_district` VALUES ('34587', '翁牛特旗', 'chifeng', '1', '34587');
INSERT INTO `ota_district` VALUES ('34588', '武强县', 'hengshui', '1', '34588');
INSERT INTO `ota_district` VALUES ('34589', '环江毛南族自治县', 'hechi', '1', '34589');
INSERT INTO `ota_district` VALUES ('34590', '南郊区', 'datong', '1', '34590');
INSERT INTO `ota_district` VALUES ('34591', '安溪县', 'quanzhou', '1', '34591');
INSERT INTO `ota_district` VALUES ('34592', '庐江县', 'chaohu', '1', '34592');
INSERT INTO `ota_district` VALUES ('34593', '息县', 'xinyang', '1', '34593');
INSERT INTO `ota_district` VALUES ('34594', '黄山区', 'huangshan', '1', '34594');
INSERT INTO `ota_district` VALUES ('34595', '尖草坪区', 'taiyuan', '1', '34595');
INSERT INTO `ota_district` VALUES ('34596', '鹤庆县', 'dali', '1', '34596');
INSERT INTO `ota_district` VALUES ('34597', '冷水滩区', 'yongzhou', '1', '34597');
INSERT INTO `ota_district` VALUES ('34598', '屯溪区', 'huangshan', '1', '34598');
INSERT INTO `ota_district` VALUES ('34599', '吴桥县', 'cangzhou', '1', '34599');
INSERT INTO `ota_district` VALUES ('34600', '英吉沙县', 'kashi', '1', '34600');
INSERT INTO `ota_district` VALUES ('34601', '四方台区', 'shuangyashan', '1', '34601');
INSERT INTO `ota_district` VALUES ('34602', '长海县', 'dalian', '1', '34602');
INSERT INTO `ota_district` VALUES ('34603', '宾川县', 'dali', '1', '34603');
INSERT INTO `ota_district` VALUES ('34604', '郯城县', 'linyi', '1', '34604');
INSERT INTO `ota_district` VALUES ('34605', '富川瑶族自治县', 'hezhou', '1', '34605');
INSERT INTO `ota_district` VALUES ('34606', '槐荫区', 'jinan', '1', '34606');
INSERT INTO `ota_district` VALUES ('34607', '谯城区', 'bozhou', '1', '34607');
INSERT INTO `ota_district` VALUES ('34608', '路桥区', 'taizhou_zhejiang', '1', '34608');
INSERT INTO `ota_district` VALUES ('34609', '青阳县', 'chizhou', '1', '34609');
INSERT INTO `ota_district` VALUES ('34610', '黔西县', 'bijie', '1', '34610');
INSERT INTO `ota_district` VALUES ('34611', '威信县', 'zhaotong', '1', '34611');
INSERT INTO `ota_district` VALUES ('34612', '遂溪县', 'zhanjiang', '1', '34612');
INSERT INTO `ota_district` VALUES ('34613', '班玛县', 'guoluo', '1', '34613');
INSERT INTO `ota_district` VALUES ('34614', '乐业县', 'baise', '1', '34614');
INSERT INTO `ota_district` VALUES ('34615', '枞阳县', 'anqing', '1', '34615');
INSERT INTO `ota_district` VALUES ('34616', '彭水苗族土家族自治县', 'chongqing_city', '1', '34616');
INSERT INTO `ota_district` VALUES ('34617', '西乌珠穆沁旗', 'xilinguole', '1', '34617');
INSERT INTO `ota_district` VALUES ('34618', '阜宁县', 'yancheng', '1', '34618');
INSERT INTO `ota_district` VALUES ('34619', '港北区', 'guigang', '1', '34619');
INSERT INTO `ota_district` VALUES ('34620', '习水县', 'zunyi', '1', '34620');
INSERT INTO `ota_district` VALUES ('34621', '宣化县', 'zhangjiakou', '1', '34621');
INSERT INTO `ota_district` VALUES ('34622', '武乡县', 'changzhi', '1', '34622');
INSERT INTO `ota_district` VALUES ('34623', '寿县', 'liuan', '1', '34623');
INSERT INTO `ota_district` VALUES ('34624', '临泽县', 'zhangye', '1', '34624');
INSERT INTO `ota_district` VALUES ('34625', '麻山区', 'jixi', '1', '34625');
INSERT INTO `ota_district` VALUES ('34626', '额济纳旗', 'alashan', '1', '34626');
INSERT INTO `ota_district` VALUES ('34627', '中方县', 'huaihua', '1', '34627');
INSERT INTO `ota_district` VALUES ('34628', '稻城县', 'ganzi', '1', '34628');
INSERT INTO `ota_district` VALUES ('34629', '石棉县', 'yaan', '1', '34629');
INSERT INTO `ota_district` VALUES ('34630', '新会区', 'jiangmen', '1', '34630');
INSERT INTO `ota_district` VALUES ('34631', '盂县', 'yangquan', '1', '34631');
INSERT INTO `ota_district` VALUES ('34632', '爱辉区', 'heihe', '1', '34632');
INSERT INTO `ota_district` VALUES ('34633', '惠来县', 'jieyang', '1', '34633');
INSERT INTO `ota_district` VALUES ('34634', '宜秀区', 'anqing', '1', '34634');
INSERT INTO `ota_district` VALUES ('34635', '勐海县', 'xishuangbanna', '1', '34635');
INSERT INTO `ota_district` VALUES ('34636', '南沙群岛', 'nansha', '1', '34636');
INSERT INTO `ota_district` VALUES ('34637', '郏县', 'pingdingshan', '1', '34637');
INSERT INTO `ota_district` VALUES ('34638', '云龙区', 'xuzhou', '1', '34638');
INSERT INTO `ota_district` VALUES ('34639', '民勤县', 'wuwei', '1', '34639');
INSERT INTO `ota_district` VALUES ('34640', '海曙区', 'ningbo', '1', '34640');
INSERT INTO `ota_district` VALUES ('34641', '纳溪区', 'luzhou', '1', '34641');
INSERT INTO `ota_district` VALUES ('34642', '长子县', 'changzhi', '1', '34642');
INSERT INTO `ota_district` VALUES ('34643', '通山县', 'xianning', '1', '34643');
INSERT INTO `ota_district` VALUES ('34644', '昭苏县', 'yili', '1', '34644');
INSERT INTO `ota_district` VALUES ('34645', '南华县', 'chuxiong', '1', '34645');
INSERT INTO `ota_district` VALUES ('34646', '桥东区', 'xingtai', '1', '34646');
INSERT INTO `ota_district` VALUES ('34647', '北林区', 'suihua', '1', '34647');
INSERT INTO `ota_district` VALUES ('34648', '萧县', 'suzhou_anhui', '1', '34648');
INSERT INTO `ota_district` VALUES ('34649', '正镶白旗', 'xilinguole', '1', '34649');
INSERT INTO `ota_district` VALUES ('34650', '潼关县', 'weinan', '1', '34650');
INSERT INTO `ota_district` VALUES ('34651', '九龙县', 'ganzi', '1', '34651');
INSERT INTO `ota_district` VALUES ('34652', '那曲地区', 'naqu', '1', '34652');
INSERT INTO `ota_district` VALUES ('34653', '花都区', 'guangzhou', '1', '34653');
INSERT INTO `ota_district` VALUES ('34654', '滨城区', 'binzhou', '1', '34654');
INSERT INTO `ota_district` VALUES ('34655', '察哈尔右翼后旗', 'wulanchabu', '1', '34655');
INSERT INTO `ota_district` VALUES ('34656', '商河县', 'jinan', '1', '34656');
INSERT INTO `ota_district` VALUES ('34657', '仁布县', 'rikaze', '1', '34657');
INSERT INTO `ota_district` VALUES ('34658', '玉山县', 'shangrao', '1', '34658');
INSERT INTO `ota_district` VALUES ('34659', '龙江县', 'qiqihaer', '1', '34659');
INSERT INTO `ota_district` VALUES ('34660', '天宁区', 'changzhou', '1', '34660');
INSERT INTO `ota_district` VALUES ('34661', '娄烦县', 'taiyuan', '1', '34661');
INSERT INTO `ota_district` VALUES ('34662', '洱源县', 'dali', '1', '34662');
INSERT INTO `ota_district` VALUES ('34663', '商都县', 'wulanchabu', '1', '34663');
INSERT INTO `ota_district` VALUES ('34664', '宜阳县', 'luoyang', '1', '34664');
INSERT INTO `ota_district` VALUES ('34665', '宝丰县', 'pingdingshan', '1', '34665');
INSERT INTO `ota_district` VALUES ('34666', '舞阳县', 'luohe', '1', '34666');
INSERT INTO `ota_district` VALUES ('34667', '阿里地区', 'ali', '1', '34667');
INSERT INTO `ota_district` VALUES ('34668', '右江区', 'baise', '1', '34668');
INSERT INTO `ota_district` VALUES ('34669', '镇坪县', 'ankang', '1', '34669');
INSERT INTO `ota_district` VALUES ('34670', '滨湖区', 'wuxi', '1', '34670');
INSERT INTO `ota_district` VALUES ('34671', '长顺县', 'qiannan', '1', '34671');
INSERT INTO `ota_district` VALUES ('34672', '藤县', 'wuzhou', '1', '34672');
INSERT INTO `ota_district` VALUES ('34673', '金州区', 'dalian', '1', '34673');
INSERT INTO `ota_district` VALUES ('34674', '酉阳土家族苗族自治县', 'chongqing_city', '1', '34674');
INSERT INTO `ota_district` VALUES ('34675', '裕安区', 'liuan', '1', '34675');
INSERT INTO `ota_district` VALUES ('34676', '平山区', 'benxi', '1', '34676');
INSERT INTO `ota_district` VALUES ('34677', '东安县', 'yongzhou', '1', '34677');
INSERT INTO `ota_district` VALUES ('34678', '丹徒区', 'zhenjiang', '1', '34678');
INSERT INTO `ota_district` VALUES ('34679', '河西区', 'tianjin_city', '1', '34679');
INSERT INTO `ota_district` VALUES ('34680', '盘龙区', 'kunming', '1', '34680');
INSERT INTO `ota_district` VALUES ('34681', '七里河区', 'lanzhou', '1', '34681');
INSERT INTO `ota_district` VALUES ('34682', '绥德县', 'yulin_shanxi_02', '1', '34682');
INSERT INTO `ota_district` VALUES ('34683', '梨树区', 'jixi', '1', '34683');
INSERT INTO `ota_district` VALUES ('34684', '萨嘎县', 'rikaze', '1', '34684');
INSERT INTO `ota_district` VALUES ('34685', '融水苗族自治县', 'liuzhou', '1', '34685');
INSERT INTO `ota_district` VALUES ('34686', '鹿寨县', 'liuzhou', '1', '34686');
INSERT INTO `ota_district` VALUES ('34687', '扶风县', 'baoji', '1', '34687');
INSERT INTO `ota_district` VALUES ('34688', '二七区', 'zhengzhou', '1', '34688');
INSERT INTO `ota_district` VALUES ('34689', '沈北新区', 'shenyang', '1', '34689');
INSERT INTO `ota_district` VALUES ('34690', '卧龙区', 'nanyang', '1', '34690');
INSERT INTO `ota_district` VALUES ('34691', '任县', 'xingtai', '1', '34691');
INSERT INTO `ota_district` VALUES ('34692', '唐县', 'baoding', '1', '34692');
INSERT INTO `ota_district` VALUES ('34693', '弥勒县', 'honghe', '1', '34693');
INSERT INTO `ota_district` VALUES ('34694', '楚雄彝族自治州', 'chuxiong', '1', '34694');
INSERT INTO `ota_district` VALUES ('34695', '海晏县', 'haibei', '1', '34695');
INSERT INTO `ota_district` VALUES ('34696', '东明县', 'heze', '1', '34696');
INSERT INTO `ota_district` VALUES ('34697', '阿拉善左旗', 'alashan', '1', '34697');
INSERT INTO `ota_district` VALUES ('34698', '贞丰县', 'qianxinan', '1', '34698');
INSERT INTO `ota_district` VALUES ('34699', '行唐县', 'shijiazhuang', '1', '34699');
INSERT INTO `ota_district` VALUES ('34700', '大洼县', 'panjin', '1', '34700');
INSERT INTO `ota_district` VALUES ('34701', '民丰县', 'hetian', '1', '34701');
INSERT INTO `ota_district` VALUES ('34702', '金明区', 'kaifeng', '1', '34702');
INSERT INTO `ota_district` VALUES ('34703', '屯留县', 'changzhi', '1', '34703');
INSERT INTO `ota_district` VALUES ('34704', '汉台区', 'hanzhong', '1', '34704');
INSERT INTO `ota_district` VALUES ('34705', '莲花县', 'pingxiang', '1', '34705');
INSERT INTO `ota_district` VALUES ('34706', '广灵县', 'datong', '1', '34706');
INSERT INTO `ota_district` VALUES ('34707', '虞城县', 'shangqiu', '1', '34707');
INSERT INTO `ota_district` VALUES ('34708', '丰都县', 'chongqing_city', '1', '34708');
INSERT INTO `ota_district` VALUES ('34709', '楚州区', 'huaian', '1', '34709');
INSERT INTO `ota_district` VALUES ('34710', '镇巴县', 'hanzhong', '1', '34710');
INSERT INTO `ota_district` VALUES ('34711', '福田区', 'shenzhen', '1', '34711');
INSERT INTO `ota_district` VALUES ('34712', '邹平县', 'binzhou', '1', '34712');
INSERT INTO `ota_district` VALUES ('34713', '新晃侗族自治县', 'huaihua', '1', '34713');
INSERT INTO `ota_district` VALUES ('34714', '中宁县', 'zhongwei', '1', '34714');
INSERT INTO `ota_district` VALUES ('34715', '青县', 'cangzhou', '1', '34715');
INSERT INTO `ota_district` VALUES ('34716', '曲阳县', 'baoding', '1', '34716');
INSERT INTO `ota_district` VALUES ('34717', '皮山县', 'hetian', '1', '34717');
INSERT INTO `ota_district` VALUES ('34718', '蒙阴县', 'linyi', '1', '34718');
INSERT INTO `ota_district` VALUES ('34719', '明溪县', 'sanming', '1', '34719');
INSERT INTO `ota_district` VALUES ('34720', '杨陵区', 'xianyang', '1', '34720');
INSERT INTO `ota_district` VALUES ('34721', '商水县', 'zhoukou', '1', '34721');
INSERT INTO `ota_district` VALUES ('34722', '浦北县', 'qinzhou', '1', '34722');
INSERT INTO `ota_district` VALUES ('34723', '柳江县', 'liuzhou', '1', '34723');
INSERT INTO `ota_district` VALUES ('34724', '雨城区', 'yaan', '1', '34724');
INSERT INTO `ota_district` VALUES ('34725', '兴山区', 'hegang', '1', '34725');
INSERT INTO `ota_district` VALUES ('34726', '曾都区', 'suizhou', '1', '34726');
INSERT INTO `ota_district` VALUES ('34727', '景县', 'hengshui', '1', '34727');
INSERT INTO `ota_district` VALUES ('34728', '阿荣旗', 'hulunbeier', '1', '34728');
INSERT INTO `ota_district` VALUES ('34729', '修水县', 'jiujiang', '1', '34729');
INSERT INTO `ota_district` VALUES ('34730', '德钦县', 'diqing', '1', '34730');
INSERT INTO `ota_district` VALUES ('34731', '巴东县', 'enshi', '1', '34731');
INSERT INTO `ota_district` VALUES ('34732', '乐都县', 'haidong', '1', '34732');
INSERT INTO `ota_district` VALUES ('34733', '永清县', 'langfang', '1', '34733');
INSERT INTO `ota_district` VALUES ('34734', '通城县', 'xianning', '1', '34734');
INSERT INTO `ota_district` VALUES ('34735', '南海区', 'foshan', '1', '34735');
INSERT INTO `ota_district` VALUES ('34736', '抚松县', 'baishan', '1', '34736');
INSERT INTO `ota_district` VALUES ('34737', '德保县', 'baise', '1', '34737');
INSERT INTO `ota_district` VALUES ('34738', '乌镇', 'jiaxing', '1', '34738');
INSERT INTO `ota_district` VALUES ('34739', '小金县', 'aba', '1', '34739');
INSERT INTO `ota_district` VALUES ('34740', '顺河回族区', 'kaifeng', '1', '34740');
INSERT INTO `ota_district` VALUES ('34741', '呼图壁县', 'changji', '1', '34741');
INSERT INTO `ota_district` VALUES ('34742', '盈江县', 'dehong', '1', '34742');
INSERT INTO `ota_district` VALUES ('34743', '康定县', 'ganzi', '1', '34743');
INSERT INTO `ota_district` VALUES ('34744', '费县', 'linyi', '1', '34744');
INSERT INTO `ota_district` VALUES ('34745', '西畴县', 'wenshan', '1', '34745');
INSERT INTO `ota_district` VALUES ('34746', '阳明区', 'mudanjiang', '1', '34746');
INSERT INTO `ota_district` VALUES ('34747', '湄潭县', 'zunyi', '1', '34747');
INSERT INTO `ota_district` VALUES ('34748', '东西湖区', 'wuhan', '1', '34748');
INSERT INTO `ota_district` VALUES ('34749', '城固县', 'hanzhong', '1', '34749');
INSERT INTO `ota_district` VALUES ('34750', '平桥区', 'xinyang', '1', '34750');
INSERT INTO `ota_district` VALUES ('34751', '金台区', 'baoji', '1', '34751');
INSERT INTO `ota_district` VALUES ('34752', '谷城县', 'xiangfan', '1', '34752');
INSERT INTO `ota_district` VALUES ('34753', '上思县', 'fangchenggang', '1', '34753');
INSERT INTO `ota_district` VALUES ('34754', '吉利区', 'luoyang', '1', '34754');
INSERT INTO `ota_district` VALUES ('34755', '沁源县', 'changzhi', '1', '34755');
INSERT INTO `ota_district` VALUES ('34756', '理县', 'aba', '1', '34756');
INSERT INTO `ota_district` VALUES ('34757', '河东区', 'linyi', '1', '34757');
INSERT INTO `ota_district` VALUES ('34758', '长岛县', 'yantai', '1', '34758');
INSERT INTO `ota_district` VALUES ('34759', '柘荣县', 'ningde', '1', '34759');
INSERT INTO `ota_district` VALUES ('34760', '岷县', 'dingxi', '1', '34760');
INSERT INTO `ota_district` VALUES ('34761', '吉木萨尔县', 'changji', '1', '34761');
INSERT INTO `ota_district` VALUES ('34762', '元氏县', 'shijiazhuang', '1', '34762');
INSERT INTO `ota_district` VALUES ('34763', '东昌区', 'tonghua', '1', '34763');
INSERT INTO `ota_district` VALUES ('34764', '商南县', 'shangluo', '1', '34764');
INSERT INTO `ota_district` VALUES ('34765', '鸡东县', 'jixi', '1', '34765');
INSERT INTO `ota_district` VALUES ('34766', '龙港区', 'huludao', '1', '34766');
INSERT INTO `ota_district` VALUES ('34767', '石台县', 'chizhou', '1', '34767');
INSERT INTO `ota_district` VALUES ('34768', '怀来县', 'zhangjiakou', '1', '34768');
INSERT INTO `ota_district` VALUES ('34769', '清水县', 'tianshui', '1', '34769');
INSERT INTO `ota_district` VALUES ('34770', '晋源区', 'taiyuan', '1', '34770');
INSERT INTO `ota_district` VALUES ('34771', '甘泉县', 'yanan', '1', '34771');
INSERT INTO `ota_district` VALUES ('34772', '沙洋县', 'jingmen', '1', '34772');
INSERT INTO `ota_district` VALUES ('34773', '湟源县', 'xining', '1', '34773');
INSERT INTO `ota_district` VALUES ('34774', '江岸区', 'wuhan', '1', '34774');
INSERT INTO `ota_district` VALUES ('34775', '苏仙区', 'chenzhou', '1', '34775');
INSERT INTO `ota_district` VALUES ('34776', '宾阳县', 'nanning', '1', '34776');
INSERT INTO `ota_district` VALUES ('34777', '科尔沁区', 'tongliao', '1', '34777');
INSERT INTO `ota_district` VALUES ('34778', '黔南布依族苗族自治州', 'qiannan', '1', '34778');
INSERT INTO `ota_district` VALUES ('34779', '塘沽区', 'tianjin_city', '1', '34779');
INSERT INTO `ota_district` VALUES ('34780', '杭锦旗', 'eerduosi', '1', '34780');
INSERT INTO `ota_district` VALUES ('34781', '丁青县', 'changdu', '1', '34781');
INSERT INTO `ota_district` VALUES ('34782', '振兴区', 'dandong', '1', '34782');
INSERT INTO `ota_district` VALUES ('34783', '科尔沁右翼中旗', 'xingan', '1', '34783');
INSERT INTO `ota_district` VALUES ('34784', '陆良县', 'qujing', '1', '34784');
INSERT INTO `ota_district` VALUES ('34785', '桃江县', 'yiyang', '1', '34785');
INSERT INTO `ota_district` VALUES ('34786', '白塔区', 'liaoyang', '1', '34786');
INSERT INTO `ota_district` VALUES ('34787', '古城区', 'lijiang', '1', '34787');
INSERT INTO `ota_district` VALUES ('34788', '居巢区', 'chaohu', '1', '34788');
INSERT INTO `ota_district` VALUES ('34789', '临河区', 'bayannaoer', '1', '34789');
INSERT INTO `ota_district` VALUES ('34790', '安龙县', 'qianxinan', '1', '34790');
INSERT INTO `ota_district` VALUES ('34791', '灵石县', 'jinzhong', '1', '34791');
INSERT INTO `ota_district` VALUES ('34792', '潍城区', 'weifang', '1', '34792');
INSERT INTO `ota_district` VALUES ('34793', '宛城区', 'nanyang', '1', '34793');
INSERT INTO `ota_district` VALUES ('34794', '绛县', 'yuncheng', '1', '34794');
INSERT INTO `ota_district` VALUES ('34795', '昂昂溪区', 'qiqihaer', '1', '34795');
INSERT INTO `ota_district` VALUES ('34796', '蜀山区', 'hefei', '1', '34796');
INSERT INTO `ota_district` VALUES ('34797', '叙永县', 'luzhou', '1', '34797');
INSERT INTO `ota_district` VALUES ('34798', '雨山区', 'maanshan', '1', '34798');
INSERT INTO `ota_district` VALUES ('34799', '襄城区', 'xiangfan', '1', '34799');
INSERT INTO `ota_district` VALUES ('34800', '南涧彝族自治县', 'dali', '1', '34800');
INSERT INTO `ota_district` VALUES ('34801', '大祥区', 'shaoyang', '1', '34801');
INSERT INTO `ota_district` VALUES ('34802', '涪陵区', 'chongqing_city', '1', '34802');
INSERT INTO `ota_district` VALUES ('34803', '阳山县', 'qingyuan', '1', '34803');
INSERT INTO `ota_district` VALUES ('34804', '拜城县', 'akesu', '1', '34804');
INSERT INTO `ota_district` VALUES ('34805', '修文县', 'guiyang', '1', '34805');
INSERT INTO `ota_district` VALUES ('34806', '武平县', 'longyan', '1', '34806');
INSERT INTO `ota_district` VALUES ('34807', '西湖区', 'nanchang', '1', '34807');
INSERT INTO `ota_district` VALUES ('34808', '忠县', 'chongqing_city', '1', '34808');
INSERT INTO `ota_district` VALUES ('34809', '共和县', 'hainanzangzu', '1', '34809');
INSERT INTO `ota_district` VALUES ('34810', '永福县', 'guilin', '1', '34810');
INSERT INTO `ota_district` VALUES ('34811', '阜平县', 'baoding', '1', '34811');
INSERT INTO `ota_district` VALUES ('34812', '博罗县', 'huizhou_guangdong', '1', '34812');
INSERT INTO `ota_district` VALUES ('34813', '开阳县', 'guiyang', '1', '34813');
INSERT INTO `ota_district` VALUES ('34814', '西峡县', 'nanyang', '1', '34814');
INSERT INTO `ota_district` VALUES ('34815', '澧县', 'changde', '1', '34815');
INSERT INTO `ota_district` VALUES ('34816', '蔡甸区', 'wuhan', '1', '34816');
INSERT INTO `ota_district` VALUES ('34817', '汶川县', 'aba', '1', '34817');
INSERT INTO `ota_district` VALUES ('34818', '科尔沁右翼前旗', 'xingan', '1', '34818');
INSERT INTO `ota_district` VALUES ('34819', '涞源县', 'baoding', '1', '34819');
INSERT INTO `ota_district` VALUES ('34820', '永川区', 'chongqing_city', '1', '34820');
INSERT INTO `ota_district` VALUES ('34821', '昌乐县', 'weifang', '1', '34821');
INSERT INTO `ota_district` VALUES ('34822', '库伦旗', 'tongliao', '1', '34822');
INSERT INTO `ota_district` VALUES ('34823', '田家庵区', 'huainan', '1', '34823');
INSERT INTO `ota_district` VALUES ('34824', '齐河县', 'dezhou', '1', '34824');
INSERT INTO `ota_district` VALUES ('34825', '岚县', 'lvliang', '1', '34825');
INSERT INTO `ota_district` VALUES ('34826', '名山县', 'yaan', '1', '34826');
INSERT INTO `ota_district` VALUES ('34827', '谢家集区', 'huainan', '1', '34827');
INSERT INTO `ota_district` VALUES ('34828', '禅城区', 'foshan', '1', '34828');
INSERT INTO `ota_district` VALUES ('34829', '新兴区', 'qitaihe', '1', '34829');
INSERT INTO `ota_district` VALUES ('34830', '东海县', 'lianyungang', '1', '34830');
INSERT INTO `ota_district` VALUES ('34831', '金沙县', 'bijie', '1', '34831');
INSERT INTO `ota_district` VALUES ('34832', '普安县', 'qianxinan', '1', '34832');
INSERT INTO `ota_district` VALUES ('34833', '云安县', 'yunfu', '1', '34833');
INSERT INTO `ota_district` VALUES ('34834', '江陵县', 'jingzhou', '1', '34834');
INSERT INTO `ota_district` VALUES ('34835', '温县', 'jiaozuo', '1', '34835');
INSERT INTO `ota_district` VALUES ('34836', '崇文区', 'beijing_city', '1', '34836');
INSERT INTO `ota_district` VALUES ('34837', '夏邑县', 'shangqiu', '1', '34837');
INSERT INTO `ota_district` VALUES ('34838', '南开区', 'tianjin_city', '1', '34838');
INSERT INTO `ota_district` VALUES ('34839', '云梦县', 'xiaogan', '1', '34839');
INSERT INTO `ota_district` VALUES ('34840', '宁阳县', 'taian', '1', '34840');
INSERT INTO `ota_district` VALUES ('34841', '尼勒克县', 'yili', '1', '34841');
INSERT INTO `ota_district` VALUES ('34842', '桐庐县', 'hangzhou', '1', '34842');
INSERT INTO `ota_district` VALUES ('34843', '达县', 'dazhou', '1', '34843');
INSERT INTO `ota_district` VALUES ('34844', '华龙区', 'puyang', '1', '34844');
INSERT INTO `ota_district` VALUES ('34845', '玉龙纳西族自治县', 'lijiang', '1', '34845');
INSERT INTO `ota_district` VALUES ('34846', '太白县', 'baoji', '1', '34846');
INSERT INTO `ota_district` VALUES ('34847', '宁南县', 'liangshan', '1', '34847');
INSERT INTO `ota_district` VALUES ('34848', '闸北区', 'shanghai_city', '1', '34848');
INSERT INTO `ota_district` VALUES ('34849', '天镇县', 'datong', '1', '34849');
INSERT INTO `ota_district` VALUES ('34850', '莲湖区', 'xian', '1', '34850');
INSERT INTO `ota_district` VALUES ('34851', '汉阴县', 'ankang', '1', '34851');
INSERT INTO `ota_district` VALUES ('34852', '海港区', 'qinhuangdao', '1', '34852');
INSERT INTO `ota_district` VALUES ('34853', '芙蓉区', 'changsha', '1', '34853');
INSERT INTO `ota_district` VALUES ('34854', '措勤县', 'ali', '1', '34854');
INSERT INTO `ota_district` VALUES ('34855', '贡嘎县', 'shannan', '1', '34855');
INSERT INTO `ota_district` VALUES ('34856', '姚安县', 'chuxiong', '1', '34856');
INSERT INTO `ota_district` VALUES ('34857', '岳塘区', 'xiangtan', '1', '34857');
INSERT INTO `ota_district` VALUES ('34858', '铁东区', 'siping', '1', '34858');
INSERT INTO `ota_district` VALUES ('34859', '新化县', 'loudi', '1', '34859');
INSERT INTO `ota_district` VALUES ('34860', '海城区', 'beihai', '1', '34860');
INSERT INTO `ota_district` VALUES ('34861', '惠水县', 'qiannan', '1', '34861');
INSERT INTO `ota_district` VALUES ('34862', '大渡口区', 'chongqing_city', '1', '34862');
INSERT INTO `ota_district` VALUES ('34863', '渝北区', 'chongqing_city', '1', '34863');
INSERT INTO `ota_district` VALUES ('34864', '平武县', 'mianyang', '1', '34864');
INSERT INTO `ota_district` VALUES ('34865', '白碱滩区', 'kelamayi', '1', '34865');
INSERT INTO `ota_district` VALUES ('34866', '温宿县', 'akesu', '1', '34866');
INSERT INTO `ota_district` VALUES ('34867', '右玉县', 'shuozhou', '1', '34867');
INSERT INTO `ota_district` VALUES ('34868', '固镇县', 'bangbu', '1', '34868');
INSERT INTO `ota_district` VALUES ('34869', '平顺县', 'changzhi', '1', '34869');
INSERT INTO `ota_district` VALUES ('34870', '茅箭区', 'shiyan', '1', '34870');
INSERT INTO `ota_district` VALUES ('34871', '锡山区', 'wuxi', '1', '34871');
INSERT INTO `ota_district` VALUES ('34872', '丰泽区', 'quanzhou', '1', '34872');
INSERT INTO `ota_district` VALUES ('34873', '砀山县', 'suzhou_anhui', '1', '34873');
INSERT INTO `ota_district` VALUES ('34874', '戚墅堰区', 'changzhou', '1', '34874');
INSERT INTO `ota_district` VALUES ('34875', '康保县', 'zhangjiakou', '1', '34875');
INSERT INTO `ota_district` VALUES ('34876', '攸县', 'zhuzhou', '1', '34876');
INSERT INTO `ota_district` VALUES ('34877', '城西区', 'xining', '1', '34877');
INSERT INTO `ota_district` VALUES ('34878', '泰顺县', 'wenzhou', '1', '34878');
INSERT INTO `ota_district` VALUES ('34879', '代县', 'xinzhou', '1', '34879');
INSERT INTO `ota_district` VALUES ('34880', '融安县', 'liuzhou', '1', '34880');
INSERT INTO `ota_district` VALUES ('34881', '朔城区', 'shuozhou', '1', '34881');
INSERT INTO `ota_district` VALUES ('34882', '大田县', 'sanming', '1', '34882');
INSERT INTO `ota_district` VALUES ('34883', '龙胜各族自治县', 'guilin', '1', '34883');
INSERT INTO `ota_district` VALUES ('34884', '冠县', 'liaocheng', '1', '34884');
INSERT INTO `ota_district` VALUES ('34885', '拉孜县', 'rikaze', '1', '34885');
INSERT INTO `ota_district` VALUES ('34886', '同仁县', 'huangnan', '1', '34886');
INSERT INTO `ota_district` VALUES ('34887', '滨江区', 'hangzhou', '1', '34887');
INSERT INTO `ota_district` VALUES ('34888', '柯坪县', 'akesu', '1', '34888');
INSERT INTO `ota_district` VALUES ('34889', '礼县', 'longnan', '1', '34889');
INSERT INTO `ota_district` VALUES ('34890', '剑川县', 'dali', '1', '34890');
INSERT INTO `ota_district` VALUES ('34891', '南浔区', 'huzhou', '1', '34891');
INSERT INTO `ota_district` VALUES ('34892', '土默特右旗', 'baotou', '1', '34892');
INSERT INTO `ota_district` VALUES ('34893', '峄城区', 'zaozhuang', '1', '34893');
INSERT INTO `ota_district` VALUES ('34894', '义县', 'jinzhou', '1', '34894');
INSERT INTO `ota_district` VALUES ('34895', '普定县', 'anshun', '1', '34895');
INSERT INTO `ota_district` VALUES ('34897', '夏河县', 'gannan', '1', '34897');
INSERT INTO `ota_district` VALUES ('34898', '凉州区', 'wuwei', '1', '34898');
INSERT INTO `ota_district` VALUES ('34899', '洪洞县', 'linfen', '1', '34899');
INSERT INTO `ota_district` VALUES ('34900', '周宁县', 'ningde', '1', '34900');
INSERT INTO `ota_district` VALUES ('34901', '湾里区', 'nanchang', '1', '34901');
INSERT INTO `ota_district` VALUES ('34902', '黔江区', 'chongqing_city', '1', '34902');
INSERT INTO `ota_district` VALUES ('34903', '横峰县', 'shangrao', '1', '34903');
INSERT INTO `ota_district` VALUES ('34904', '彭泽县', 'jiujiang', '1', '34904');
INSERT INTO `ota_district` VALUES ('34905', '库车县', 'akesu', '1', '34905');
INSERT INTO `ota_district` VALUES ('34906', '弓长岭区', 'liaoyang', '1', '34906');
INSERT INTO `ota_district` VALUES ('34907', '阿勒泰地区', 'aletai', '1', '34907');
INSERT INTO `ota_district` VALUES ('34908', '边坝县', 'changdu', '1', '34908');
INSERT INTO `ota_district` VALUES ('34909', '凉城县', 'wulanchabu', '1', '34909');
INSERT INTO `ota_district` VALUES ('34910', '故城县', 'hengshui', '1', '34910');
INSERT INTO `ota_district` VALUES ('34911', '番禺区', 'guangzhou', '1', '34911');
INSERT INTO `ota_district` VALUES ('34912', '东平县', 'taian', '1', '34912');
INSERT INTO `ota_district` VALUES ('34913', '海东地区', 'haidong', '1', '34913');
INSERT INTO `ota_district` VALUES ('34914', '双流县', 'chengdu', '1', '34914');
INSERT INTO `ota_district` VALUES ('34915', '鄂托克旗', 'eerduosi', '1', '34915');
INSERT INTO `ota_district` VALUES ('34916', '新荣区', 'datong', '1', '34916');
INSERT INTO `ota_district` VALUES ('34917', '金湖县', 'huaian', '1', '34917');
INSERT INTO `ota_district` VALUES ('34918', '天心区', 'changsha', '1', '34918');
INSERT INTO `ota_district` VALUES ('34919', '松阳县', 'lishui', '1', '34919');
INSERT INTO `ota_district` VALUES ('34920', '社旗县', 'nanyang', '1', '34920');
INSERT INTO `ota_district` VALUES ('34921', '祥云县', 'dali', '1', '34921');
INSERT INTO `ota_district` VALUES ('34922', '定边县', 'yulin_shanxi_02', '1', '34922');
INSERT INTO `ota_district` VALUES ('34923', '永宁县', 'yinchuan', '1', '34923');
INSERT INTO `ota_district` VALUES ('34924', '甘南县', 'qiqihaer', '1', '34924');
INSERT INTO `ota_district` VALUES ('34925', '湖里区', 'xiamen', '1', '34925');
INSERT INTO `ota_district` VALUES ('34926', '八宿县', 'changdu', '1', '34926');
INSERT INTO `ota_district` VALUES ('34927', '苍溪县', 'guangyuan', '1', '34927');
INSERT INTO `ota_district` VALUES ('34928', '伊通满族自治县', 'siping', '1', '34928');
INSERT INTO `ota_district` VALUES ('34929', '广南县', 'wenshan', '1', '34929');
INSERT INTO `ota_district` VALUES ('34930', '大厂回族自治县', 'langfang', '1', '34930');
INSERT INTO `ota_district` VALUES ('34931', '蒸湘区', 'hengyang', '1', '34931');
INSERT INTO `ota_district` VALUES ('34932', '榆社县', 'jinzhong', '1', '34932');
INSERT INTO `ota_district` VALUES ('34933', '荔城区', 'putian', '1', '34933');
INSERT INTO `ota_district` VALUES ('34934', '双牌县', 'yongzhou', '1', '34934');
INSERT INTO `ota_district` VALUES ('34935', '越秀区', 'guangzhou', '1', '34935');
INSERT INTO `ota_district` VALUES ('34936', '龙文区', 'zhangzhou', '1', '34936');
INSERT INTO `ota_district` VALUES ('34937', '城关区', 'lasa', '1', '34937');
INSERT INTO `ota_district` VALUES ('34938', '西陵区', 'yichang', '1', '34938');
INSERT INTO `ota_district` VALUES ('34939', '苍南县', 'wenzhou', '1', '34939');
INSERT INTO `ota_district` VALUES ('34940', '五河县', 'bangbu', '1', '34940');
INSERT INTO `ota_district` VALUES ('34941', '青川县', 'guangyuan', '1', '34941');
INSERT INTO `ota_district` VALUES ('34942', '柘城县', 'shangqiu', '1', '34942');
INSERT INTO `ota_district` VALUES ('34943', '蒙自县', 'honghe', '1', '34943');
INSERT INTO `ota_district` VALUES ('34944', '灵丘县', 'datong', '1', '34944');
INSERT INTO `ota_district` VALUES ('34946', '元宝区', 'dandong', '1', '34946');
INSERT INTO `ota_district` VALUES ('34947', '榆次区', 'jinzhong', '1', '34947');
INSERT INTO `ota_district` VALUES ('34948', '延寿县', 'haerbin', '1', '34948');
INSERT INTO `ota_district` VALUES ('34949', '金塔县', 'jiuquan', '1', '34949');
INSERT INTO `ota_district` VALUES ('34950', '香格里拉县', 'diqing', '1', '34950');
INSERT INTO `ota_district` VALUES ('34951', '清城区', 'qingyuan', '1', '34951');
INSERT INTO `ota_district` VALUES ('34952', '高港区', 'taizhou_jiangsu', '1', '34952');
INSERT INTO `ota_district` VALUES ('34953', '王益区', 'tongzhou', '1', '34953');
INSERT INTO `ota_district` VALUES ('34954', '新华区', 'pingdingshan', '1', '34954');
INSERT INTO `ota_district` VALUES ('34955', '钟山区', 'liupanshui', '1', '34955');
INSERT INTO `ota_district` VALUES ('34956', '桃山区', 'qitaihe', '1', '34956');
INSERT INTO `ota_district` VALUES ('34957', '沾化县', 'binzhou', '1', '34957');
INSERT INTO `ota_district` VALUES ('34958', '武陵源区', 'zhangjiajie', '1', '34958');
INSERT INTO `ota_district` VALUES ('34959', '振安区', 'dandong', '1', '34959');
INSERT INTO `ota_district` VALUES ('34960', '龙里县', 'qiannan', '1', '34960');
INSERT INTO `ota_district` VALUES ('34961', '弋江区', 'wuhu', '1', '34961');
INSERT INTO `ota_district` VALUES ('34962', '平潭县', 'fuzhou_fujian', '1', '34962');
INSERT INTO `ota_district` VALUES ('34963', '工布江达县', 'linzhi', '1', '34963');
INSERT INTO `ota_district` VALUES ('34964', '明山区', 'benxi', '1', '34964');
INSERT INTO `ota_district` VALUES ('34965', '新乡县', 'xinxiang', '1', '34965');
INSERT INTO `ota_district` VALUES ('34966', '本溪满族自治县', 'benxi', '1', '34966');
INSERT INTO `ota_district` VALUES ('34967', '瓯海区', 'wenzhou', '1', '34967');
INSERT INTO `ota_district` VALUES ('34968', '横山县', 'yulin_shanxi_02', '1', '34968');
INSERT INTO `ota_district` VALUES ('34969', '龙州县', 'chongzuo', '1', '34969');
INSERT INTO `ota_district` VALUES ('34970', '柳南区', 'liuzhou', '1', '34970');
INSERT INTO `ota_district` VALUES ('34971', '无为县', 'chaohu', '1', '34971');
INSERT INTO `ota_district` VALUES ('34972', '建邺区', 'nanjing', '1', '34972');
INSERT INTO `ota_district` VALUES ('34973', '大足县', 'chongqing_city', '1', '34973');
INSERT INTO `ota_district` VALUES ('34974', '金城江区', 'hechi', '1', '34974');
INSERT INTO `ota_district` VALUES ('34975', '龙陵县', 'baoshan', '1', '34975');
INSERT INTO `ota_district` VALUES ('34976', '海珠区', 'guangzhou', '1', '34976');
INSERT INTO `ota_district` VALUES ('34977', '贵定县', 'qiannan', '1', '34977');
INSERT INTO `ota_district` VALUES ('34978', '宣武区', 'beijing_city', '1', '34978');
INSERT INTO `ota_district` VALUES ('34979', '大埔县', 'meizhou', '1', '34979');
INSERT INTO `ota_district` VALUES ('34980', '宿豫区', 'suqian', '1', '34980');
INSERT INTO `ota_district` VALUES ('34981', '涪城区', 'mianyang', '1', '34981');
INSERT INTO `ota_district` VALUES ('34982', '思南县', 'tongren', '1', '34982');
INSERT INTO `ota_district` VALUES ('34983', '丰台区', 'beijing_city', '1', '34983');
INSERT INTO `ota_district` VALUES ('34984', '兴和县', 'wulanchabu', '1', '34984');
INSERT INTO `ota_district` VALUES ('34985', '大邑县', 'chengdu', '1', '34985');
INSERT INTO `ota_district` VALUES ('34986', '余庆县', 'zunyi', '1', '34986');
INSERT INTO `ota_district` VALUES ('34987', '郎溪县', 'xuancheng', '1', '34987');
INSERT INTO `ota_district` VALUES ('34988', '法库县', 'shenyang', '1', '34988');
INSERT INTO `ota_district` VALUES ('34989', '清浦区', 'huaian', '1', '34989');
INSERT INTO `ota_district` VALUES ('34990', '青田县', 'lishui', '1', '34990');
INSERT INTO `ota_district` VALUES ('34991', '新洲区', 'wuhan', '1', '34991');
INSERT INTO `ota_district` VALUES ('34992', '金东区', 'jinhua', '1', '34992');
INSERT INTO `ota_district` VALUES ('34993', '港南区', 'guigang', '1', '34993');
INSERT INTO `ota_district` VALUES ('34994', '天祝藏族自治县', 'wuwei', '1', '34994');
INSERT INTO `ota_district` VALUES ('34995', '长垣县', 'xinxiang', '1', '34995');
INSERT INTO `ota_district` VALUES ('34996', '武陵区', 'changde', '1', '34996');
INSERT INTO `ota_district` VALUES ('34997', '平塘县', 'qiannan', '1', '34997');
INSERT INTO `ota_district` VALUES ('34998', '小店区', 'taiyuan', '1', '34998');
INSERT INTO `ota_district` VALUES ('34999', '赤城县', 'zhangjiakou', '1', '34999');
INSERT INTO `ota_district` VALUES ('35000', '林口县', 'mudanjiang', '1', '35000');
INSERT INTO `ota_district` VALUES ('35001', '青云谱区', 'nanchang', '1', '35001');
INSERT INTO `ota_district` VALUES ('35002', '泰来县', 'qiqihaer', '1', '35002');
INSERT INTO `ota_district` VALUES ('35003', '波密县', 'linzhi', '1', '35003');
INSERT INTO `ota_district` VALUES ('35004', '永平县', 'dali', '1', '35004');
INSERT INTO `ota_district` VALUES ('35005', '竹溪县', 'shiyan', '1', '35005');
INSERT INTO `ota_district` VALUES ('35006', '双桥区', 'chongqing_city', '1', '35006');
INSERT INTO `ota_district` VALUES ('35007', '陈仓区', 'baoji', '1', '35007');
INSERT INTO `ota_district` VALUES ('35008', '江华瑶族自治县', 'yongzhou', '1', '35008');
INSERT INTO `ota_district` VALUES ('35009', '团风县', 'huanggang', '1', '35009');
INSERT INTO `ota_district` VALUES ('35010', '睢阳区', 'shangqiu', '1', '35010');
INSERT INTO `ota_district` VALUES ('35011', '珠晖区', 'hengyang', '1', '35011');
INSERT INTO `ota_district` VALUES ('35012', '秦淮区', 'nanjing', '1', '35012');
INSERT INTO `ota_district` VALUES ('35013', '平江县', 'yueyang', '1', '35013');
INSERT INTO `ota_district` VALUES ('35014', '南江县', 'bazhong', '1', '35014');
INSERT INTO `ota_district` VALUES ('35015', '殷都区', 'anyang', '1', '35015');
INSERT INTO `ota_district` VALUES ('35016', '南票区', 'huludao', '1', '35016');
INSERT INTO `ota_district` VALUES ('35017', '乌什县', 'akesu', '1', '35017');
INSERT INTO `ota_district` VALUES ('35019', '昌吉回族自治州', 'changji', '1', '35019');
INSERT INTO `ota_district` VALUES ('35020', '乌当区', 'guiyang', '1', '35020');
INSERT INTO `ota_district` VALUES ('35021', '高县', 'yibin', '1', '35021');
INSERT INTO `ota_district` VALUES ('35022', '城北区', 'xining', '1', '35022');
INSERT INTO `ota_district` VALUES ('35023', '巫溪县', 'chongqing_city', '1', '35023');
INSERT INTO `ota_district` VALUES ('35024', '翠屏区', 'yibin', '1', '35024');
INSERT INTO `ota_district` VALUES ('35025', '清河县', 'xingtai', '1', '35025');
INSERT INTO `ota_district` VALUES ('35026', '噶尔县', 'ali', '1', '35026');
INSERT INTO `ota_district` VALUES ('35027', '莫力达瓦达斡尔族自治旗', 'hulunbeier', '1', '35027');
INSERT INTO `ota_district` VALUES ('35028', '兴安区', 'hegang', '1', '35028');
INSERT INTO `ota_district` VALUES ('35029', '船山区', 'suining', '1', '35029');
INSERT INTO `ota_district` VALUES ('35030', '广阳区', 'langfang', '1', '35030');
INSERT INTO `ota_district` VALUES ('35031', '沿滩区', 'zigong', '1', '35031');
INSERT INTO `ota_district` VALUES ('35032', '扎鲁特旗', 'tongliao', '1', '35032');
INSERT INTO `ota_district` VALUES ('35033', '抚顺县', 'fushun', '1', '35033');
INSERT INTO `ota_district` VALUES ('35034', '江夏区', 'wuhan', '1', '35034');
INSERT INTO `ota_district` VALUES ('35035', '上蔡县', 'zhumadian', '1', '35035');
INSERT INTO `ota_district` VALUES ('35036', '江孜县', 'rikaze', '1', '35036');
INSERT INTO `ota_district` VALUES ('35037', '临西县', 'xingtai', '1', '35037');
INSERT INTO `ota_district` VALUES ('35038', '台江县', 'qiandongnan', '1', '35038');
INSERT INTO `ota_district` VALUES ('35039', '鄂伦春自治旗', 'hulunbeier', '1', '35039');
INSERT INTO `ota_district` VALUES ('35040', '未央区', 'xian', '1', '35040');
INSERT INTO `ota_district` VALUES ('35041', '连山区', 'huludao', '1', '35041');
INSERT INTO `ota_district` VALUES ('35042', '托克托县', 'huhehaote', '1', '35042');
INSERT INTO `ota_district` VALUES ('35043', '铜仁地区', 'tongren', '1', '35043');
INSERT INTO `ota_district` VALUES ('35044', '维西傈僳族自治县', 'diqing', '1', '35044');
INSERT INTO `ota_district` VALUES ('35045', '三水区', 'foshan', '1', '35045');
INSERT INTO `ota_district` VALUES ('35046', '富县', 'yanan', '1', '35046');
INSERT INTO `ota_district` VALUES ('35047', '陇县', 'baoji', '1', '35047');
INSERT INTO `ota_district` VALUES ('35048', '西充县', 'nanchong', '1', '35048');
INSERT INTO `ota_district` VALUES ('35049', '汉沽区', 'tianjin_city', '1', '35049');
INSERT INTO `ota_district` VALUES ('35050', '永仁县', 'chuxiong', '1', '35050');
INSERT INTO `ota_district` VALUES ('35051', '阳高县', 'datong', '1', '35051');
INSERT INTO `ota_district` VALUES ('35052', '临潭县', 'gannan', '1', '35052');
INSERT INTO `ota_district` VALUES ('35053', '道真仡佬族苗族自治县', 'zunyi', '1', '35053');
INSERT INTO `ota_district` VALUES ('35054', '青秀区', 'nanning', '1', '35054');
INSERT INTO `ota_district` VALUES ('35055', '连南瑶族自治县', 'qingyuan', '1', '35055');
INSERT INTO `ota_district` VALUES ('35056', '莱山区', 'yantai', '1', '35056');
INSERT INTO `ota_district` VALUES ('35057', '宜君县', 'tongzhou', '1', '35057');
INSERT INTO `ota_district` VALUES ('35058', '灵璧县', 'suzhou_anhui', '1', '35058');
INSERT INTO `ota_district` VALUES ('35059', '安乡县', 'changde', '1', '35059');
INSERT INTO `ota_district` VALUES ('35060', '汪清县', 'yanbian', '1', '35060');
INSERT INTO `ota_district` VALUES ('35061', '崇义县', 'ganzhou', '1', '35061');
INSERT INTO `ota_district` VALUES ('35062', '龙凤区', 'daqing', '1', '35062');
INSERT INTO `ota_district` VALUES ('35063', '盐湖区', 'yuncheng', '1', '35063');
INSERT INTO `ota_district` VALUES ('35064', '狮子山区', 'tongling', '1', '35064');
INSERT INTO `ota_district` VALUES ('35065', '龙川县', 'heyuan', '1', '35065');
INSERT INTO `ota_district` VALUES ('35066', '六合区', 'nanjing', '1', '35066');
INSERT INTO `ota_district` VALUES ('35068', '金山区', 'shanghai_city', '1', '35068');
INSERT INTO `ota_district` VALUES ('35069', '锡林郭勒盟', 'xilinguole', '1', '35069');
INSERT INTO `ota_district` VALUES ('35070', '唐海县', 'tangshan', '1', '35070');
INSERT INTO `ota_district` VALUES ('35071', '安县', 'mianyang', '1', '35071');
INSERT INTO `ota_district` VALUES ('35072', '山阳县', 'shangluo', '1', '35072');
INSERT INTO `ota_district` VALUES ('35073', '龙湾区', 'wenzhou', '1', '35073');
INSERT INTO `ota_district` VALUES ('35074', '舒城县', 'liuan', '1', '35074');
INSERT INTO `ota_district` VALUES ('35075', '曲水县', 'lasa', '1', '35075');
INSERT INTO `ota_district` VALUES ('35076', '蒙山县', 'wuzhou', '1', '35076');
INSERT INTO `ota_district` VALUES ('35077', '关岭布依族苗族自治县', 'anshun', '1', '35077');
INSERT INTO `ota_district` VALUES ('35078', '德江县', 'tongren', '1', '35078');
INSERT INTO `ota_district` VALUES ('35079', '滦平县', 'chengde', '1', '35079');
INSERT INTO `ota_district` VALUES ('35080', '三江侗族自治县', 'liuzhou', '1', '35080');
INSERT INTO `ota_district` VALUES ('35081', '泸县', 'luzhou', '1', '35081');
INSERT INTO `ota_district` VALUES ('35082', '靖安县', 'yichun_jiangxi', '1', '35082');
INSERT INTO `ota_district` VALUES ('35083', '田阳县', 'baise', '1', '35083');
INSERT INTO `ota_district` VALUES ('35084', '袁州区', 'yichun_jiangxi', '1', '35084');
INSERT INTO `ota_district` VALUES ('35085', '方城县', 'nanyang', '1', '35085');
INSERT INTO `ota_district` VALUES ('35086', '射阳县', 'yancheng', '1', '35086');
INSERT INTO `ota_district` VALUES ('35087', '渭源县', 'dingxi', '1', '35087');
INSERT INTO `ota_district` VALUES ('35088', '玉田县', 'tangshan', '1', '35088');
INSERT INTO `ota_district` VALUES ('35089', '黔东南苗族侗族自治州', 'qiandongnan', '1', '35089');
INSERT INTO `ota_district` VALUES ('35090', '高明区', 'foshan', '1', '35090');
INSERT INTO `ota_district` VALUES ('35091', '寒亭区', 'weifang', '1', '35091');
INSERT INTO `ota_district` VALUES ('35092', '甘州区', 'zhangye', '1', '35092');
INSERT INTO `ota_district` VALUES ('35093', '元宝山区', 'chifeng', '1', '35093');
INSERT INTO `ota_district` VALUES ('35094', '岳麓区', 'changsha', '1', '35094');
INSERT INTO `ota_district` VALUES ('35095', '石拐区', 'baotou', '1', '35095');
INSERT INTO `ota_district` VALUES ('35096', '扶余县', 'songyuan', '1', '35096');
INSERT INTO `ota_district` VALUES ('35097', '禹王台区', 'kaifeng', '1', '35097');
INSERT INTO `ota_district` VALUES ('35098', '定南县', 'ganzhou', '1', '35098');
INSERT INTO `ota_district` VALUES ('35099', '清涧县', 'yulin_shanxi_02', '1', '35099');
INSERT INTO `ota_district` VALUES ('35100', '长宁区', 'shanghai_city', '1', '35100');
INSERT INTO `ota_district` VALUES ('35101', '德城区', 'dezhou', '1', '35101');
INSERT INTO `ota_district` VALUES ('35102', '平坝县', 'anshun', '1', '35102');
INSERT INTO `ota_district` VALUES ('35103', '富顺县', 'zigong', '1', '35103');
INSERT INTO `ota_district` VALUES ('35104', '西岗区', 'dalian', '1', '35104');
INSERT INTO `ota_district` VALUES ('35105', '桃城区', 'hengshui', '1', '35105');
INSERT INTO `ota_district` VALUES ('35106', '站前区', 'yingkou', '1', '35106');
INSERT INTO `ota_district` VALUES ('35107', '宝安区', 'shenzhen', '1', '35107');
INSERT INTO `ota_district` VALUES ('35108', '金乡县', 'jining', '1', '35108');
INSERT INTO `ota_district` VALUES ('35109', '丘北县', 'wenshan', '1', '35109');
INSERT INTO `ota_district` VALUES ('35110', '东湖区', 'nanchang', '1', '35110');
INSERT INTO `ota_district` VALUES ('35111', '新野县', 'nanyang', '1', '35111');
INSERT INTO `ota_district` VALUES ('35112', '休宁县', 'huangshan', '1', '35112');
INSERT INTO `ota_district` VALUES ('35113', '山亭区', 'zaozhuang', '1', '35113');
INSERT INTO `ota_district` VALUES ('35114', '洪山区', 'wuhan', '1', '35114');
INSERT INTO `ota_district` VALUES ('35115', '凤凰县', 'xiangxi', '1', '35115');
INSERT INTO `ota_district` VALUES ('35116', '莎车县', 'kashi', '1', '35116');
INSERT INTO `ota_district` VALUES ('35117', '石城县', 'ganzhou', '1', '35117');
INSERT INTO `ota_district` VALUES ('35118', '大通回族土族自治县', 'xining', '1', '35118');
INSERT INTO `ota_district` VALUES ('35119', '获嘉县', 'xinxiang', '1', '35119');
INSERT INTO `ota_district` VALUES ('35120', '东坡区', 'meishan', '1', '35120');
INSERT INTO `ota_district` VALUES ('35121', '从江县', 'qiandongnan', '1', '35121');
INSERT INTO `ota_district` VALUES ('35122', '城步苗族自治县', 'shaoyang', '1', '35122');
INSERT INTO `ota_district` VALUES ('35123', '昌宁县', 'baoshan', '1', '35123');
INSERT INTO `ota_district` VALUES ('35124', '来凤县', 'enshi', '1', '35124');
INSERT INTO `ota_district` VALUES ('35125', '邯郸县', 'handan', '1', '35125');
INSERT INTO `ota_district` VALUES ('35126', '京山县', 'jingmen', '1', '35126');
INSERT INTO `ota_district` VALUES ('35127', '京口区', 'zhenjiang', '1', '35127');
INSERT INTO `ota_district` VALUES ('35128', '炎陵县', 'zhuzhou', '1', '35128');
INSERT INTO `ota_district` VALUES ('35129', '蓬江区', 'jiangmen', '1', '35129');
INSERT INTO `ota_district` VALUES ('35130', '上街区', 'zhengzhou', '1', '35130');
INSERT INTO `ota_district` VALUES ('35131', '浦城县', 'nanping', '1', '35131');
INSERT INTO `ota_district` VALUES ('35132', '孝昌县', 'xiaogan', '1', '35132');
INSERT INTO `ota_district` VALUES ('35133', '天峻县', 'haixi', '1', '35133');
INSERT INTO `ota_district` VALUES ('35134', '浪卡子县', 'shannan', '1', '35134');
INSERT INTO `ota_district` VALUES ('35135', '滨海新区', 'tianjin_city', '1', '35135');
INSERT INTO `ota_district` VALUES ('35136', '辛集市', 'shijiazhuang', '1', '35136');
INSERT INTO `ota_district` VALUES ('35137', '藁城市', 'shijiazhuang', '1', '35137');
INSERT INTO `ota_district` VALUES ('35138', '晋州市', 'shijiazhuang', '1', '35138');
INSERT INTO `ota_district` VALUES ('35139', '新乐市', 'shijiazhuang', '1', '35139');
INSERT INTO `ota_district` VALUES ('35140', '鹿泉市', 'shijiazhuang', '1', '35140');
INSERT INTO `ota_district` VALUES ('35141', '遵化市', 'tangshan', '1', '35141');
INSERT INTO `ota_district` VALUES ('35142', '迁安市', 'tangshan', '1', '35142');
INSERT INTO `ota_district` VALUES ('35143', '武安市', 'handan', '1', '35143');
INSERT INTO `ota_district` VALUES ('35144', '南宫市', 'xingtai', '1', '35144');
INSERT INTO `ota_district` VALUES ('35145', '沙河市', 'xingtai', '1', '35145');
INSERT INTO `ota_district` VALUES ('35146', '新市区', 'baoding', '1', '35146');
INSERT INTO `ota_district` VALUES ('35147', '北市区', 'baoding', '1', '35147');
INSERT INTO `ota_district` VALUES ('35148', '南市区', 'baoding', '1', '35148');
INSERT INTO `ota_district` VALUES ('35149', '涿州市', 'baoding', '1', '35149');
INSERT INTO `ota_district` VALUES ('35150', '定州市', 'baoding', '1', '35150');
INSERT INTO `ota_district` VALUES ('35151', '安国市', 'baoding', '1', '35151');
INSERT INTO `ota_district` VALUES ('35152', '高碑店市', 'baoding', '1', '35152');
INSERT INTO `ota_district` VALUES ('35153', '泊头市', 'cangzhou', '1', '35153');
INSERT INTO `ota_district` VALUES ('35154', '任丘市', 'cangzhou', '1', '35154');
INSERT INTO `ota_district` VALUES ('35155', '黄骅市', 'cangzhou', '1', '35155');
INSERT INTO `ota_district` VALUES ('35156', '河间市', 'cangzhou', '1', '35156');
INSERT INTO `ota_district` VALUES ('35157', '霸州市', 'langfang', '1', '35157');
INSERT INTO `ota_district` VALUES ('35158', '三河市', 'langfang', '1', '35158');
INSERT INTO `ota_district` VALUES ('35159', '冀州市', 'hengshui', '1', '35159');
INSERT INTO `ota_district` VALUES ('35160', '深州市', 'hengshui', '1', '35160');
INSERT INTO `ota_district` VALUES ('35161', '古交市', 'taiyuan', '1', '35161');
INSERT INTO `ota_district` VALUES ('35162', '城区', 'datong', '1', '35162');
INSERT INTO `ota_district` VALUES ('35163', '城区', 'yangquan', '1', '35163');
INSERT INTO `ota_district` VALUES ('35164', '郊区', 'yangquan', '1', '35164');
INSERT INTO `ota_district` VALUES ('35165', '城区', 'changzhi', '1', '35165');
INSERT INTO `ota_district` VALUES ('35166', '郊区', 'changzhi', '1', '35166');
INSERT INTO `ota_district` VALUES ('35167', '潞城市', 'changzhi', '1', '35167');
INSERT INTO `ota_district` VALUES ('35168', '城区', 'jincheng', '1', '35168');
INSERT INTO `ota_district` VALUES ('35169', '高平市', 'jincheng', '1', '35169');
INSERT INTO `ota_district` VALUES ('35170', '介休市', 'jinzhong', '1', '35170');
INSERT INTO `ota_district` VALUES ('35171', '永济市', 'yuncheng', '1', '35171');
INSERT INTO `ota_district` VALUES ('35172', '河津市', 'yuncheng', '1', '35172');
INSERT INTO `ota_district` VALUES ('35173', '原平市', 'xinzhou', '1', '35173');
INSERT INTO `ota_district` VALUES ('35174', '侯马市', 'linfen', '1', '35174');
INSERT INTO `ota_district` VALUES ('35175', '霍州市', 'linfen', '1', '35175');
INSERT INTO `ota_district` VALUES ('35176', '孝义市', 'lvliang', '1', '35176');
INSERT INTO `ota_district` VALUES ('35177', '汾阳市', 'lvliang', '1', '35177');
INSERT INTO `ota_district` VALUES ('35178', '霍林郭勒市', 'tongliao', '1', '35178');
INSERT INTO `ota_district` VALUES ('35179', '满洲里市', 'hulunbeier', '1', '35179');
INSERT INTO `ota_district` VALUES ('35180', '牙克石市', 'hulunbeier', '1', '35180');
INSERT INTO `ota_district` VALUES ('35181', '扎兰屯市', 'hulunbeier', '1', '35181');
INSERT INTO `ota_district` VALUES ('35182', '额尔古纳市', 'hulunbeier', '1', '35182');
INSERT INTO `ota_district` VALUES ('35183', '根河市', 'hulunbeier', '1', '35183');
INSERT INTO `ota_district` VALUES ('35184', '丰镇市', 'wulanchabu', '1', '35184');
INSERT INTO `ota_district` VALUES ('35185', '乌兰浩特市', 'xingan', '1', '35185');
INSERT INTO `ota_district` VALUES ('35186', '阿尔山市', 'xingan', '1', '35186');
INSERT INTO `ota_district` VALUES ('35187', '二连浩特市', 'xilinguole', '1', '35187');
INSERT INTO `ota_district` VALUES ('35188', '锡林浩特市', 'xilinguole', '1', '35188');
INSERT INTO `ota_district` VALUES ('35189', '新民市', 'shenyang', '1', '35189');
INSERT INTO `ota_district` VALUES ('35190', '瓦房店市', 'dalian', '1', '35190');
INSERT INTO `ota_district` VALUES ('35191', '普兰店市', 'dalian', '1', '35191');
INSERT INTO `ota_district` VALUES ('35192', '庄河市', 'dalian', '1', '35192');
INSERT INTO `ota_district` VALUES ('35194', '东港市', 'dandong', '1', '35194');
INSERT INTO `ota_district` VALUES ('35195', '凌海市', 'jinzhou', '1', '35195');
INSERT INTO `ota_district` VALUES ('35196', '北镇市', 'jinzhou', '1', '35196');
INSERT INTO `ota_district` VALUES ('35197', '鲅鱼圈区', 'yingkou', '1', '35197');
INSERT INTO `ota_district` VALUES ('35198', '盖州市', 'yingkou', '1', '35198');
INSERT INTO `ota_district` VALUES ('35199', '大石桥市', 'yingkou', '1', '35199');
INSERT INTO `ota_district` VALUES ('35200', '灯塔市', 'liaoyang', '1', '35200');
INSERT INTO `ota_district` VALUES ('35201', '调兵山市', 'tieling', '1', '35201');
INSERT INTO `ota_district` VALUES ('35202', '开原市', 'tieling', '1', '35202');
INSERT INTO `ota_district` VALUES ('35203', '北票市', 'chaoyang', '1', '35203');
INSERT INTO `ota_district` VALUES ('35204', '凌源市', 'chaoyang', '1', '35204');
INSERT INTO `ota_district` VALUES ('35205', '兴城市', 'huludao', '1', '35205');
INSERT INTO `ota_district` VALUES ('35206', '九台市', 'changchun', '1', '35206');
INSERT INTO `ota_district` VALUES ('35207', '榆树市', 'changchun', '1', '35207');
INSERT INTO `ota_district` VALUES ('35208', '德惠市', 'changchun', '1', '35208');
INSERT INTO `ota_district` VALUES ('35209', '蛟河市', 'jilin_city', '1', '35209');
INSERT INTO `ota_district` VALUES ('35210', '桦甸市', 'jilin_city', '1', '35210');
INSERT INTO `ota_district` VALUES ('35211', '舒兰市', 'jilin_city', '1', '35211');
INSERT INTO `ota_district` VALUES ('35212', '磐石市', 'jilin_city', '1', '35212');
INSERT INTO `ota_district` VALUES ('35213', '公主岭市', 'siping', '1', '35213');
INSERT INTO `ota_district` VALUES ('35214', '双辽市', 'siping', '1', '35214');
INSERT INTO `ota_district` VALUES ('35215', '梅河口市', 'tonghua', '1', '35215');
INSERT INTO `ota_district` VALUES ('35216', '集安市', 'tonghua', '1', '35216');
INSERT INTO `ota_district` VALUES ('35217', '临江市', 'baishan', '1', '35217');
INSERT INTO `ota_district` VALUES ('35218', '洮南市', 'baicheng', '1', '35218');
INSERT INTO `ota_district` VALUES ('35219', '大安市', 'baicheng', '1', '35219');
INSERT INTO `ota_district` VALUES ('35220', '延吉市', 'yanbian', '1', '35220');
INSERT INTO `ota_district` VALUES ('35221', '图们市', 'yanbian', '1', '35221');
INSERT INTO `ota_district` VALUES ('35222', '敦化市', 'yanbian', '1', '35222');
INSERT INTO `ota_district` VALUES ('35223', '珲春市', 'yanbian', '1', '35223');
INSERT INTO `ota_district` VALUES ('35224', '龙井市', 'yanbian', '1', '35224');
INSERT INTO `ota_district` VALUES ('35225', '和龙市', 'yanbian', '1', '35225');
INSERT INTO `ota_district` VALUES ('35226', '双城市', 'haerbin', '1', '35226');
INSERT INTO `ota_district` VALUES ('35227', '尚志市', 'haerbin', '1', '35227');
INSERT INTO `ota_district` VALUES ('35228', '五常市', 'haerbin', '1', '35228');
INSERT INTO `ota_district` VALUES ('35229', '梅里斯达斡尔族区', 'qiqihaer', '1', '35229');
INSERT INTO `ota_district` VALUES ('35230', '讷河市', 'qiqihaer', '1', '35230');
INSERT INTO `ota_district` VALUES ('35231', '虎林市', 'jixi', '1', '35231');
INSERT INTO `ota_district` VALUES ('35232', '密山市', 'jixi', '1', '35232');
INSERT INTO `ota_district` VALUES ('35233', '伊春区', 'yichun_heilongjiang', '1', '35233');
INSERT INTO `ota_district` VALUES ('35234', '南岔区', 'yichun_heilongjiang', '1', '35234');
INSERT INTO `ota_district` VALUES ('35235', '友好区', 'yichun_heilongjiang', '1', '35235');
INSERT INTO `ota_district` VALUES ('35236', '西林区', 'yichun_heilongjiang', '1', '35236');
INSERT INTO `ota_district` VALUES ('35237', '翠峦区', 'yichun_heilongjiang', '1', '35237');
INSERT INTO `ota_district` VALUES ('35238', '新青区', 'yichun_heilongjiang', '1', '35238');
INSERT INTO `ota_district` VALUES ('35239', '美溪区', 'yichun_heilongjiang', '1', '35239');
INSERT INTO `ota_district` VALUES ('35240', '金山屯区', 'yichun_heilongjiang', '1', '35240');
INSERT INTO `ota_district` VALUES ('35241', '五营区', 'yichun_heilongjiang', '1', '35241');
INSERT INTO `ota_district` VALUES ('35242', '乌马河区', 'yichun_heilongjiang', '1', '35242');
INSERT INTO `ota_district` VALUES ('35243', '汤旺河区', 'yichun_heilongjiang', '1', '35243');
INSERT INTO `ota_district` VALUES ('35244', '带岭区', 'yichun_heilongjiang', '1', '35244');
INSERT INTO `ota_district` VALUES ('35245', '乌伊岭区', 'yichun_heilongjiang', '1', '35245');
INSERT INTO `ota_district` VALUES ('35246', '红星区', 'yichun_heilongjiang', '1', '35246');
INSERT INTO `ota_district` VALUES ('35247', '上甘岭区', 'yichun_heilongjiang', '1', '35247');
INSERT INTO `ota_district` VALUES ('35248', '嘉荫县', 'yichun_heilongjiang', '1', '35248');
INSERT INTO `ota_district` VALUES ('35249', '铁力市', 'yichun_heilongjiang', '1', '35249');
INSERT INTO `ota_district` VALUES ('35250', '郊区', 'jiamusi', '1', '35250');
INSERT INTO `ota_district` VALUES ('35251', '同江市', 'jiamusi', '1', '35251');
INSERT INTO `ota_district` VALUES ('35252', '富锦市', 'jiamusi', '1', '35252');
INSERT INTO `ota_district` VALUES ('35253', '绥芬河市', 'mudanjiang', '1', '35253');
INSERT INTO `ota_district` VALUES ('35254', '海林市', 'mudanjiang', '1', '35254');
INSERT INTO `ota_district` VALUES ('35255', '宁安市', 'mudanjiang', '1', '35255');
INSERT INTO `ota_district` VALUES ('35256', '穆棱市', 'mudanjiang', '1', '35256');
INSERT INTO `ota_district` VALUES ('35257', '北安市', 'heihe', '1', '35257');
INSERT INTO `ota_district` VALUES ('35258', '五大连池市', 'heihe', '1', '35258');
INSERT INTO `ota_district` VALUES ('35259', '安达市', 'suihua', '1', '35259');
INSERT INTO `ota_district` VALUES ('35260', '肇东市', 'suihua', '1', '35260');
INSERT INTO `ota_district` VALUES ('35261', '海伦市', 'suihua', '1', '35261');
INSERT INTO `ota_district` VALUES ('35262', '江阴市', 'wuxi', '1', '35262');
INSERT INTO `ota_district` VALUES ('35263', '宜兴市', 'wuxi', '1', '35263');
INSERT INTO `ota_district` VALUES ('35264', '铜山区', 'xuzhou', '1', '35264');
INSERT INTO `ota_district` VALUES ('35265', '新沂市', 'xuzhou', '1', '35265');
INSERT INTO `ota_district` VALUES ('35266', '邳州市', 'xuzhou', '1', '35266');
INSERT INTO `ota_district` VALUES ('35267', '溧阳市', 'changzhou', '1', '35267');
INSERT INTO `ota_district` VALUES ('35268', '金坛市', 'changzhou', '1', '35268');
INSERT INTO `ota_district` VALUES ('35269', '常熟市', 'suzhou_jiangsu', '1', '35269');
INSERT INTO `ota_district` VALUES ('35270', '张家港市', 'suzhou_jiangsu', '1', '35270');
INSERT INTO `ota_district` VALUES ('35271', '昆山市', 'suzhou_jiangsu', '1', '35271');
INSERT INTO `ota_district` VALUES ('35272', '吴江区', 'suzhou_jiangsu', '1', '35272');
INSERT INTO `ota_district` VALUES ('35273', '太仓市', 'suzhou_jiangsu', '1', '35273');
INSERT INTO `ota_district` VALUES ('35274', '启东市', 'nantong', '1', '35274');
INSERT INTO `ota_district` VALUES ('35275', '如皋市', 'nantong', '1', '35275');
INSERT INTO `ota_district` VALUES ('35276', '海门市', 'nantong', '1', '35276');
INSERT INTO `ota_district` VALUES ('35277', '东台市', 'yancheng', '1', '35277');
INSERT INTO `ota_district` VALUES ('35278', '大丰市', 'yancheng', '1', '35278');
INSERT INTO `ota_district` VALUES ('35279', '江都区', 'yangzhou', '1', '35279');
INSERT INTO `ota_district` VALUES ('35280', '仪征市', 'yangzhou', '1', '35280');
INSERT INTO `ota_district` VALUES ('35281', '高邮市', 'yangzhou', '1', '35281');
INSERT INTO `ota_district` VALUES ('35282', '丹阳市', 'zhenjiang', '1', '35282');
INSERT INTO `ota_district` VALUES ('35283', '扬中市', 'zhenjiang', '1', '35283');
INSERT INTO `ota_district` VALUES ('35284', '句容市', 'zhenjiang', '1', '35284');
INSERT INTO `ota_district` VALUES ('35285', '兴化市', 'taizhou_jiangsu', '1', '35285');
INSERT INTO `ota_district` VALUES ('35286', '靖江市', 'taizhou_jiangsu', '1', '35286');
INSERT INTO `ota_district` VALUES ('35287', '泰兴市', 'taizhou_jiangsu', '1', '35287');
INSERT INTO `ota_district` VALUES ('35288', '姜堰市', 'taizhou_jiangsu', '1', '35288');
INSERT INTO `ota_district` VALUES ('35289', '建德市', 'hangzhou', '1', '35289');
INSERT INTO `ota_district` VALUES ('35290', '富阳市', 'hangzhou', '1', '35290');
INSERT INTO `ota_district` VALUES ('35291', '临安市', 'hangzhou', '1', '35291');
INSERT INTO `ota_district` VALUES ('35292', '余姚市', 'ningbo', '1', '35292');
INSERT INTO `ota_district` VALUES ('35293', '慈溪市', 'ningbo', '1', '35293');
INSERT INTO `ota_district` VALUES ('35294', '奉化市', 'ningbo', '1', '35294');
INSERT INTO `ota_district` VALUES ('35295', '瑞安市', 'wenzhou', '1', '35295');
INSERT INTO `ota_district` VALUES ('35296', '乐清市', 'wenzhou', '1', '35296');
INSERT INTO `ota_district` VALUES ('35297', '海宁市', 'jiaxing', '1', '35297');
INSERT INTO `ota_district` VALUES ('35298', '平湖市', 'jiaxing', '1', '35298');
INSERT INTO `ota_district` VALUES ('35299', '桐乡市', 'jiaxing', '1', '35299');
INSERT INTO `ota_district` VALUES ('35300', '诸暨市', 'shaoxing', '1', '35300');
INSERT INTO `ota_district` VALUES ('35301', '上虞市', 'shaoxing', '1', '35301');
INSERT INTO `ota_district` VALUES ('35302', '嵊州市', 'shaoxing', '1', '35302');
INSERT INTO `ota_district` VALUES ('35303', '兰溪市', 'jinhua', '1', '35303');
INSERT INTO `ota_district` VALUES ('35304', '义乌市', 'jinhua', '1', '35304');
INSERT INTO `ota_district` VALUES ('35305', '东阳市', 'jinhua', '1', '35305');
INSERT INTO `ota_district` VALUES ('35306', '永康市', 'jinhua', '1', '35306');
INSERT INTO `ota_district` VALUES ('35307', '江山市', 'quzhou', '1', '35307');
INSERT INTO `ota_district` VALUES ('35308', '温岭市', 'taizhou_zhejiang', '1', '35308');
INSERT INTO `ota_district` VALUES ('35309', '临海市', 'taizhou_zhejiang', '1', '35309');
INSERT INTO `ota_district` VALUES ('35310', '龙泉市', 'lishui', '1', '35310');
INSERT INTO `ota_district` VALUES ('35311', '巢湖市', 'hefei', '1', '35311');
INSERT INTO `ota_district` VALUES ('35312', '郊区', 'tongling', '1', '35312');
INSERT INTO `ota_district` VALUES ('35313', '桐城市', 'anqing', '1', '35313');
INSERT INTO `ota_district` VALUES ('35314', '天长市', 'chuzhou', '1', '35314');
INSERT INTO `ota_district` VALUES ('35315', '明光市', 'chuzhou', '1', '35315');
INSERT INTO `ota_district` VALUES ('35316', '界首市', 'fuyang_anhui', '1', '35316');
INSERT INTO `ota_district` VALUES ('35317', '宁国市', 'xuancheng', '1', '35317');
INSERT INTO `ota_district` VALUES ('35318', '福清市', 'fuzhou_fujian', '1', '35318');
INSERT INTO `ota_district` VALUES ('35319', '长乐市', 'fuzhou_fujian', '1', '35319');
INSERT INTO `ota_district` VALUES ('35320', '永安市', 'sanming', '1', '35320');
INSERT INTO `ota_district` VALUES ('35321', '金门县', 'quanzhou', '1', '35321');
INSERT INTO `ota_district` VALUES ('35322', '石狮市', 'quanzhou', '1', '35322');
INSERT INTO `ota_district` VALUES ('35323', '晋江市', 'quanzhou', '1', '35323');
INSERT INTO `ota_district` VALUES ('35324', '南安市', 'quanzhou', '1', '35324');
INSERT INTO `ota_district` VALUES ('35325', '龙海市', 'zhangzhou', '1', '35325');
INSERT INTO `ota_district` VALUES ('35326', '邵武市', 'nanping', '1', '35326');
INSERT INTO `ota_district` VALUES ('35327', '武夷山市', 'nanping', '1', '35327');
INSERT INTO `ota_district` VALUES ('35328', '建瓯市', 'nanping', '1', '35328');
INSERT INTO `ota_district` VALUES ('35329', '建阳市', 'nanping', '1', '35329');
INSERT INTO `ota_district` VALUES ('35330', '漳平市', 'longyan', '1', '35330');
INSERT INTO `ota_district` VALUES ('35331', '福安市', 'ningde', '1', '35331');
INSERT INTO `ota_district` VALUES ('35332', '福鼎市', 'ningde', '1', '35332');
INSERT INTO `ota_district` VALUES ('35333', '乐平市', 'jingdezhen', '1', '35333');
INSERT INTO `ota_district` VALUES ('35334', '瑞昌市', 'jiujiang', '1', '35334');
INSERT INTO `ota_district` VALUES ('35335', '共青城市', 'jiujiang', '1', '35335');
INSERT INTO `ota_district` VALUES ('35336', '贵溪市', 'yingtan', '1', '35336');
INSERT INTO `ota_district` VALUES ('35337', '瑞金市', 'ganzhou', '1', '35337');
INSERT INTO `ota_district` VALUES ('35338', '南康市', 'ganzhou', '1', '35338');
INSERT INTO `ota_district` VALUES ('35340', '丰城市', 'yichun_jiangxi', '1', '35340');
INSERT INTO `ota_district` VALUES ('35341', '樟树市', 'yichun_jiangxi', '1', '35341');
INSERT INTO `ota_district` VALUES ('35342', '高安市', 'yichun_jiangxi', '1', '35342');
INSERT INTO `ota_district` VALUES ('35343', '临川区', 'fuzhou_jiangxi', '1', '35343');
INSERT INTO `ota_district` VALUES ('35344', '南城县', 'fuzhou_jiangxi', '1', '35344');
INSERT INTO `ota_district` VALUES ('35345', '黎川县', 'fuzhou_jiangxi', '1', '35345');
INSERT INTO `ota_district` VALUES ('35346', '南丰县', 'fuzhou_jiangxi', '1', '35346');
INSERT INTO `ota_district` VALUES ('35347', '崇仁县', 'fuzhou_jiangxi', '1', '35347');
INSERT INTO `ota_district` VALUES ('35348', '乐安县', 'fuzhou_jiangxi', '1', '35348');
INSERT INTO `ota_district` VALUES ('35349', '宜黄县', 'fuzhou_jiangxi', '1', '35349');
INSERT INTO `ota_district` VALUES ('35350', '金溪县', 'fuzhou_jiangxi', '1', '35350');
INSERT INTO `ota_district` VALUES ('35351', '资溪县', 'fuzhou_jiangxi', '1', '35351');
INSERT INTO `ota_district` VALUES ('35352', '东乡县', 'fuzhou_jiangxi', '1', '35352');
INSERT INTO `ota_district` VALUES ('35353', '广昌县', 'fuzhou_jiangxi', '1', '35353');
INSERT INTO `ota_district` VALUES ('35354', '德兴市', 'shangrao', '1', '35354');
INSERT INTO `ota_district` VALUES ('35355', '市中区', 'jinan', '1', '35355');
INSERT INTO `ota_district` VALUES ('35356', '章丘市', 'jinan', '1', '35356');
INSERT INTO `ota_district` VALUES ('35357', '市南区', 'qingdao', '1', '35357');
INSERT INTO `ota_district` VALUES ('35358', '市北区', 'qingdao', '1', '35358');
INSERT INTO `ota_district` VALUES ('35359', '胶州市', 'qingdao', '1', '35359');
INSERT INTO `ota_district` VALUES ('35360', '即墨市', 'qingdao', '1', '35360');
INSERT INTO `ota_district` VALUES ('35361', '平度市', 'qingdao', '1', '35361');
INSERT INTO `ota_district` VALUES ('35362', '胶南市', 'qingdao', '1', '35362');
INSERT INTO `ota_district` VALUES ('35363', '莱西市', 'qingdao', '1', '35363');
INSERT INTO `ota_district` VALUES ('35364', '市中区', 'zaozhuang', '1', '35364');
INSERT INTO `ota_district` VALUES ('35365', '滕州市', 'zaozhuang', '1', '35365');
INSERT INTO `ota_district` VALUES ('35366', '龙口市', 'yantai', '1', '35366');
INSERT INTO `ota_district` VALUES ('35367', '莱阳市', 'yantai', '1', '35367');
INSERT INTO `ota_district` VALUES ('35368', '莱州市', 'yantai', '1', '35368');
INSERT INTO `ota_district` VALUES ('35369', '蓬莱市', 'yantai', '1', '35369');
INSERT INTO `ota_district` VALUES ('35370', '招远市', 'yantai', '1', '35370');
INSERT INTO `ota_district` VALUES ('35371', '栖霞市', 'yantai', '1', '35371');
INSERT INTO `ota_district` VALUES ('35372', '海阳市', 'yantai', '1', '35372');
INSERT INTO `ota_district` VALUES ('35373', '青州市', 'weifang', '1', '35373');
INSERT INTO `ota_district` VALUES ('35374', '诸城市', 'weifang', '1', '35374');
INSERT INTO `ota_district` VALUES ('35375', '寿光市', 'weifang', '1', '35375');
INSERT INTO `ota_district` VALUES ('35376', '安丘市', 'weifang', '1', '35376');
INSERT INTO `ota_district` VALUES ('35377', '高密市', 'weifang', '1', '35377');
INSERT INTO `ota_district` VALUES ('35378', '昌邑市', 'weifang', '1', '35378');
INSERT INTO `ota_district` VALUES ('35379', '市中区', 'jining', '1', '35379');
INSERT INTO `ota_district` VALUES ('35380', '曲阜市', 'jining', '1', '35380');
INSERT INTO `ota_district` VALUES ('35381', '兖州市', 'jining', '1', '35381');
INSERT INTO `ota_district` VALUES ('35382', '邹城市', 'jining', '1', '35382');
INSERT INTO `ota_district` VALUES ('35383', '新泰市', 'taian', '1', '35383');
INSERT INTO `ota_district` VALUES ('35384', '肥城市', 'taian', '1', '35384');
INSERT INTO `ota_district` VALUES ('35385', '文登市', 'weihai', '1', '35385');
INSERT INTO `ota_district` VALUES ('35386', '荣成市', 'weihai', '1', '35386');
INSERT INTO `ota_district` VALUES ('35387', '乳山市', 'weihai', '1', '35387');
INSERT INTO `ota_district` VALUES ('35388', '乐陵市', 'dezhou', '1', '35388');
INSERT INTO `ota_district` VALUES ('35389', '禹城市', 'dezhou', '1', '35389');
INSERT INTO `ota_district` VALUES ('35390', '临清市', 'liaocheng', '1', '35390');
INSERT INTO `ota_district` VALUES ('35391', '巩义市', 'zhengzhou', '1', '35391');
INSERT INTO `ota_district` VALUES ('35392', '荥阳市', 'zhengzhou', '1', '35392');
INSERT INTO `ota_district` VALUES ('35393', '新密市', 'zhengzhou', '1', '35393');
INSERT INTO `ota_district` VALUES ('35394', '新郑市', 'zhengzhou', '1', '35394');
INSERT INTO `ota_district` VALUES ('35395', '登封市', 'zhengzhou', '1', '35395');
INSERT INTO `ota_district` VALUES ('35396', '偃师市', 'luoyang', '1', '35396');
INSERT INTO `ota_district` VALUES ('35397', '舞钢市', 'pingdingshan', '1', '35397');
INSERT INTO `ota_district` VALUES ('35398', '汝州市', 'pingdingshan', '1', '35398');
INSERT INTO `ota_district` VALUES ('35399', '林州市', 'anyang', '1', '35399');
INSERT INTO `ota_district` VALUES ('35400', '卫辉市', 'xinxiang', '1', '35400');
INSERT INTO `ota_district` VALUES ('35401', '辉县市', 'xinxiang', '1', '35401');
INSERT INTO `ota_district` VALUES ('35402', '沁阳市', 'jiaozuo', '1', '35402');
INSERT INTO `ota_district` VALUES ('35403', '孟州市', 'jiaozuo', '1', '35403');
INSERT INTO `ota_district` VALUES ('35404', '禹州市', 'xuchang', '1', '35404');
INSERT INTO `ota_district` VALUES ('35405', '长葛市', 'xuchang', '1', '35405');
INSERT INTO `ota_district` VALUES ('35406', '义马市', 'sanmenxia', '1', '35406');
INSERT INTO `ota_district` VALUES ('35407', '灵宝市', 'sanmenxia', '1', '35407');
INSERT INTO `ota_district` VALUES ('35408', '邓州市', 'nanyang', '1', '35408');
INSERT INTO `ota_district` VALUES ('35409', '永城市', 'shangqiu', '1', '35409');
INSERT INTO `ota_district` VALUES ('35410', '项城市', 'zhoukou', '1', '35410');
INSERT INTO `ota_district` VALUES ('35411', '大冶市', 'huangshi', '1', '35411');
INSERT INTO `ota_district` VALUES ('35412', '丹江口市', 'shiyan', '1', '35412');
INSERT INTO `ota_district` VALUES ('35413', '宜都市', 'yichang', '1', '35413');
INSERT INTO `ota_district` VALUES ('35414', '当阳市', 'yichang', '1', '35414');
INSERT INTO `ota_district` VALUES ('35415', '枝江市', 'yichang', '1', '35415');
INSERT INTO `ota_district` VALUES ('35416', '钟祥市', 'jingmen', '1', '35416');
INSERT INTO `ota_district` VALUES ('35417', '应城市', 'xiaogan', '1', '35417');
INSERT INTO `ota_district` VALUES ('35418', '安陆市', 'xiaogan', '1', '35418');
INSERT INTO `ota_district` VALUES ('35419', '汉川市', 'xiaogan', '1', '35419');
INSERT INTO `ota_district` VALUES ('35420', '沙市区', 'jingzhou', '1', '35420');
INSERT INTO `ota_district` VALUES ('35421', '石首市', 'jingzhou', '1', '35421');
INSERT INTO `ota_district` VALUES ('35422', '洪湖市', 'jingzhou', '1', '35422');
INSERT INTO `ota_district` VALUES ('35423', '松滋市', 'jingzhou', '1', '35423');
INSERT INTO `ota_district` VALUES ('35424', '麻城市', 'huanggang', '1', '35424');
INSERT INTO `ota_district` VALUES ('35425', '武穴市', 'huanggang', '1', '35425');
INSERT INTO `ota_district` VALUES ('35426', '赤壁市', 'xianning', '1', '35426');
INSERT INTO `ota_district` VALUES ('35427', '随县', 'suizhou', '1', '35427');
INSERT INTO `ota_district` VALUES ('35428', '广水市', 'suizhou', '1', '35428');
INSERT INTO `ota_district` VALUES ('35429', '恩施市', 'enshi', '1', '35429');
INSERT INTO `ota_district` VALUES ('35430', '利川市', 'enshi', '1', '35430');
INSERT INTO `ota_district` VALUES ('35431', '望城区', 'changsha', '1', '35431');
INSERT INTO `ota_district` VALUES ('35432', '浏阳市', 'changsha', '1', '35432');
INSERT INTO `ota_district` VALUES ('35433', '醴陵市', 'zhuzhou', '1', '35433');
INSERT INTO `ota_district` VALUES ('35434', '湘乡市', 'xiangtan', '1', '35434');
INSERT INTO `ota_district` VALUES ('35435', '韶山市', 'xiangtan', '1', '35435');
INSERT INTO `ota_district` VALUES ('35436', '耒阳市', 'hengyang', '1', '35436');
INSERT INTO `ota_district` VALUES ('35437', '常宁市', 'hengyang', '1', '35437');
INSERT INTO `ota_district` VALUES ('35438', '武冈市', 'shaoyang', '1', '35438');
INSERT INTO `ota_district` VALUES ('35439', '汨罗市', 'yueyang', '1', '35439');
INSERT INTO `ota_district` VALUES ('35440', '临湘市', 'yueyang', '1', '35440');
INSERT INTO `ota_district` VALUES ('35441', '津市市', 'changde', '1', '35441');
INSERT INTO `ota_district` VALUES ('35442', '沅江市', 'yiyang', '1', '35442');
INSERT INTO `ota_district` VALUES ('35443', '资兴市', 'chenzhou', '1', '35443');
INSERT INTO `ota_district` VALUES ('35444', '洪江市', 'huaihua', '1', '35444');
INSERT INTO `ota_district` VALUES ('35445', '冷水江市', 'loudi', '1', '35445');
INSERT INTO `ota_district` VALUES ('35446', '涟源市', 'loudi', '1', '35446');
INSERT INTO `ota_district` VALUES ('35447', '吉首市', 'xiangxi', '1', '35447');
INSERT INTO `ota_district` VALUES ('35448', '增城市', 'guangzhou', '1', '35448');
INSERT INTO `ota_district` VALUES ('35449', '从化市', 'guangzhou', '1', '35449');
INSERT INTO `ota_district` VALUES ('35450', '乐昌市', 'shaoguan', '1', '35450');
INSERT INTO `ota_district` VALUES ('35451', '南雄市', 'shaoguan', '1', '35451');
INSERT INTO `ota_district` VALUES ('35452', '台山市', 'jiangmen', '1', '35452');
INSERT INTO `ota_district` VALUES ('35453', '开平市', 'jiangmen', '1', '35453');
INSERT INTO `ota_district` VALUES ('35454', '鹤山市', 'jiangmen', '1', '35454');
INSERT INTO `ota_district` VALUES ('35455', '恩平市', 'jiangmen', '1', '35455');
INSERT INTO `ota_district` VALUES ('35456', '廉江市', 'zhanjiang', '1', '35456');
INSERT INTO `ota_district` VALUES ('35457', '雷州市', 'zhanjiang', '1', '35457');
INSERT INTO `ota_district` VALUES ('35458', '吴川市', 'zhanjiang', '1', '35458');
INSERT INTO `ota_district` VALUES ('35459', '高州市', 'maoming', '1', '35459');
INSERT INTO `ota_district` VALUES ('35460', '化州市', 'maoming', '1', '35460');
INSERT INTO `ota_district` VALUES ('35461', '信宜市', 'maoming', '1', '35461');
INSERT INTO `ota_district` VALUES ('35462', '高要市', 'zhaoqing', '1', '35462');
INSERT INTO `ota_district` VALUES ('35463', '四会市', 'zhaoqing', '1', '35463');
INSERT INTO `ota_district` VALUES ('35464', '兴宁市', 'meizhou', '1', '35464');
INSERT INTO `ota_district` VALUES ('35465', '城区', 'shanwei', '1', '35465');
INSERT INTO `ota_district` VALUES ('35466', '陆丰市', 'shanwei', '1', '35466');
INSERT INTO `ota_district` VALUES ('35467', '阳春市', 'yangjiang', '1', '35467');
INSERT INTO `ota_district` VALUES ('35468', '英德市', 'qingyuan', '1', '35468');
INSERT INTO `ota_district` VALUES ('35469', '连州市', 'qingyuan', '1', '35469');
INSERT INTO `ota_district` VALUES ('35470', '中山市', 'dongguan', '1', '35470');
INSERT INTO `ota_district` VALUES ('35471', '普宁市', 'jieyang', '1', '35471');
INSERT INTO `ota_district` VALUES ('35472', '罗定市', 'yunfu', '1', '35472');
INSERT INTO `ota_district` VALUES ('35473', '蝶山区', 'wuzhou', '1', '35473');
INSERT INTO `ota_district` VALUES ('35474', '岑溪市', 'wuzhou', '1', '35474');
INSERT INTO `ota_district` VALUES ('35475', '东兴市', 'fangchenggang', '1', '35475');
INSERT INTO `ota_district` VALUES ('35476', '桂平市', 'guigang', '1', '35476');
INSERT INTO `ota_district` VALUES ('35477', '北流市', 'yulin_guangxi', '1', '35477');
INSERT INTO `ota_district` VALUES ('35478', '宜州市', 'hechi', '1', '35478');
INSERT INTO `ota_district` VALUES ('35479', '合山市', 'laibin', '1', '35479');
INSERT INTO `ota_district` VALUES ('35480', '凭祥市', 'chongzuo', '1', '35480');
INSERT INTO `ota_district` VALUES ('35481', '綦江区', 'chongqing_city', '1', '35481');
INSERT INTO `ota_district` VALUES ('35482', '大足区', 'chongqing_city', '1', '35482');
INSERT INTO `ota_district` VALUES ('35483', '都江堰市', 'chengdu', '1', '35483');
INSERT INTO `ota_district` VALUES ('35484', '彭州市', 'chengdu', '1', '35484');
INSERT INTO `ota_district` VALUES ('35485', '邛崃市', 'chengdu', '1', '35485');
INSERT INTO `ota_district` VALUES ('35486', '崇州市', 'chengdu', '1', '35486');
INSERT INTO `ota_district` VALUES ('35487', '广汉市', 'deyang', '1', '35487');
INSERT INTO `ota_district` VALUES ('35488', '什邡市', 'deyang', '1', '35488');
INSERT INTO `ota_district` VALUES ('35489', '绵竹市', 'deyang', '1', '35489');
INSERT INTO `ota_district` VALUES ('35490', '江油市', 'mianyang', '1', '35490');
INSERT INTO `ota_district` VALUES ('35491', '利州区', 'guangyuan', '1', '35491');
INSERT INTO `ota_district` VALUES ('35492', '市中区', 'neijiang', '1', '35492');
INSERT INTO `ota_district` VALUES ('35493', '市中区', 'leshan', '1', '35493');
INSERT INTO `ota_district` VALUES ('35494', '峨眉山市', 'leshan', '1', '35494');
INSERT INTO `ota_district` VALUES ('35495', '阆中市', 'nanchong', '1', '35495');
INSERT INTO `ota_district` VALUES ('35496', '南溪区', 'yibin', '1', '35496');
INSERT INTO `ota_district` VALUES ('35497', '华蓥市', 'guangan', '1', '35497');
INSERT INTO `ota_district` VALUES ('35498', '万源市', 'dazhou', '1', '35498');
INSERT INTO `ota_district` VALUES ('35499', '简阳市', 'ziyang', '1', '35499');
INSERT INTO `ota_district` VALUES ('35500', '西昌市', 'liangshan', '1', '35500');
INSERT INTO `ota_district` VALUES ('35501', '清镇市', 'guiyang', '1', '35501');
INSERT INTO `ota_district` VALUES ('35502', '赤水市', 'zunyi', '1', '35502');
INSERT INTO `ota_district` VALUES ('35503', '仁怀市', 'zunyi', '1', '35503');
INSERT INTO `ota_district` VALUES ('35504', '七星关区', 'bijie', '1', '35504');
INSERT INTO `ota_district` VALUES ('35505', '碧江区', 'tongren', '1', '35505');
INSERT INTO `ota_district` VALUES ('35506', '万山区', 'tongren', '1', '35506');
INSERT INTO `ota_district` VALUES ('35507', '兴义市', 'qianxinan', '1', '35507');
INSERT INTO `ota_district` VALUES ('35508', '凯里市', 'qiandongnan', '1', '35508');
INSERT INTO `ota_district` VALUES ('35509', '都匀市', 'qiannan', '1', '35509');
INSERT INTO `ota_district` VALUES ('35510', '福泉市', 'qiannan', '1', '35510');
INSERT INTO `ota_district` VALUES ('35511', '呈贡区', 'kunming', '1', '35511');
INSERT INTO `ota_district` VALUES ('35512', '安宁市', 'kunming', '1', '35512');
INSERT INTO `ota_district` VALUES ('35513', '宣威市', 'qujing', '1', '35513');
INSERT INTO `ota_district` VALUES ('35514', '思茅区', 'puer', '1', '35514');
INSERT INTO `ota_district` VALUES ('35515', '宁洱哈尼族彝族自治县', 'puer', '1', '35515');
INSERT INTO `ota_district` VALUES ('35516', '墨江哈尼族自治县', 'puer', '1', '35516');
INSERT INTO `ota_district` VALUES ('35517', '景东彝族自治县', 'puer', '1', '35517');
INSERT INTO `ota_district` VALUES ('35518', '景谷傣族彝族自治县', 'puer', '1', '35518');
INSERT INTO `ota_district` VALUES ('35519', '镇沅彝族哈尼族拉祜族自治县', 'puer', '1', '35519');
INSERT INTO `ota_district` VALUES ('35520', '江城哈尼族彝族自治县', 'puer', '1', '35520');
INSERT INTO `ota_district` VALUES ('35521', '孟连傣族拉祜族佤族自治县', 'puer', '1', '35521');
INSERT INTO `ota_district` VALUES ('35522', '澜沧拉祜族自治县', 'puer', '1', '35522');
INSERT INTO `ota_district` VALUES ('35523', '西盟佤族自治县', 'puer', '1', '35523');
INSERT INTO `ota_district` VALUES ('35524', '楚雄市', 'chuxiong', '1', '35524');
INSERT INTO `ota_district` VALUES ('35525', '个旧市', 'honghe', '1', '35525');
INSERT INTO `ota_district` VALUES ('35526', '开远市', 'honghe', '1', '35526');
INSERT INTO `ota_district` VALUES ('35527', '蒙自市', 'honghe', '1', '35527');
INSERT INTO `ota_district` VALUES ('35528', '文山市', 'wenshan', '1', '35528');
INSERT INTO `ota_district` VALUES ('35529', '景洪市', 'xishuangbanna', '1', '35529');
INSERT INTO `ota_district` VALUES ('35530', '大理市', 'dali', '1', '35530');
INSERT INTO `ota_district` VALUES ('35531', '瑞丽市', 'dehong', '1', '35531');
INSERT INTO `ota_district` VALUES ('35532', '芒市', 'dehong', '1', '35532');
INSERT INTO `ota_district` VALUES ('35533', '日喀则市', 'rikaze', '1', '35533');
INSERT INTO `ota_district` VALUES ('35534', '兴平市', 'xianyang', '1', '35534');
INSERT INTO `ota_district` VALUES ('35535', '韩城市', 'weinan', '1', '35535');
INSERT INTO `ota_district` VALUES ('35536', '华阴市', 'weinan', '1', '35536');
INSERT INTO `ota_district` VALUES ('35537', '玉门市', 'jiuquan', '1', '35537');
INSERT INTO `ota_district` VALUES ('35538', '敦煌市', 'jiuquan', '1', '35538');
INSERT INTO `ota_district` VALUES ('35539', '临夏市', 'linxia', '1', '35539');
INSERT INTO `ota_district` VALUES ('35540', '合作市', 'gannan', '1', '35540');
INSERT INTO `ota_district` VALUES ('35541', '玉树县', 'yushu', '1', '35541');
INSERT INTO `ota_district` VALUES ('35542', '杂多县', 'yushu', '1', '35542');
INSERT INTO `ota_district` VALUES ('35543', '称多县', 'yushu', '1', '35543');
INSERT INTO `ota_district` VALUES ('35544', '治多县', 'yushu', '1', '35544');
INSERT INTO `ota_district` VALUES ('35545', '囊谦县', 'yushu', '1', '35545');
INSERT INTO `ota_district` VALUES ('35546', '曲麻莱县', 'yushu', '1', '35546');
INSERT INTO `ota_district` VALUES ('35547', '格尔木市', 'haixi', '1', '35547');
INSERT INTO `ota_district` VALUES ('35548', '德令哈市', 'haixi', '1', '35548');
INSERT INTO `ota_district` VALUES ('35549', '灵武市', 'yinchuan', '1', '35549');
INSERT INTO `ota_district` VALUES ('35550', '红寺堡区', 'wuzhong', '1', '35550');
INSERT INTO `ota_district` VALUES ('35551', '青铜峡市', 'wuzhong', '1', '35551');
INSERT INTO `ota_district` VALUES ('35552', '新市区', 'wulumuqi', '1', '35552');
INSERT INTO `ota_district` VALUES ('35553', '吐鲁番市', 'tulufan', '1', '35553');
INSERT INTO `ota_district` VALUES ('35554', '哈密市', 'hami', '1', '35554');
INSERT INTO `ota_district` VALUES ('35555', '昌吉市', 'changji', '1', '35555');
INSERT INTO `ota_district` VALUES ('35556', '阜康市', 'changji', '1', '35556');
INSERT INTO `ota_district` VALUES ('35557', '博乐市', 'boertala', '1', '35557');
INSERT INTO `ota_district` VALUES ('35558', '精河县', 'boertala', '1', '35558');
INSERT INTO `ota_district` VALUES ('35559', '温泉县', 'boertala', '1', '35559');
INSERT INTO `ota_district` VALUES ('35560', '库尔勒市', 'bayinguoleng', '1', '35560');
INSERT INTO `ota_district` VALUES ('35561', '阿克苏市', 'akesu', '1', '35561');
INSERT INTO `ota_district` VALUES ('35562', '阿图什市', 'kezilesukeerkezi', '1', '35562');
INSERT INTO `ota_district` VALUES ('35563', '阿克陶县', 'kezilesukeerkezi', '1', '35563');
INSERT INTO `ota_district` VALUES ('35564', '阿合奇县', 'kezilesukeerkezi', '1', '35564');
INSERT INTO `ota_district` VALUES ('35565', '乌恰县', 'kezilesukeerkezi', '1', '35565');
INSERT INTO `ota_district` VALUES ('35566', '喀什市', 'kashi', '1', '35566');
INSERT INTO `ota_district` VALUES ('35567', '和田市', 'hetian', '1', '35567');
INSERT INTO `ota_district` VALUES ('35568', '伊宁市', 'yili', '1', '35568');
INSERT INTO `ota_district` VALUES ('35569', '奎屯市', 'yili', '1', '35569');
INSERT INTO `ota_district` VALUES ('35570', '塔城市', 'tacheng', '1', '35570');
INSERT INTO `ota_district` VALUES ('35571', '乌苏市', 'tacheng', '1', '35571');
INSERT INTO `ota_district` VALUES ('35572', '阿勒泰市', 'aletai', '1', '35572');
INSERT INTO `ota_district` VALUES ('35573', '姑苏区', 'suzhou_jiangsu', '1', '35573');
INSERT INTO `ota_district` VALUES ('35574', '溧水区', 'nanjing', '1', '35574');
INSERT INTO `ota_district` VALUES ('35575', '曹妃甸区', 'tangshan', '1', '35575');
INSERT INTO `ota_district` VALUES ('35576', '名山区', 'yaan', '1', '35576');
INSERT INTO `ota_district` VALUES ('35577', '高淳区', 'nanjing', '1', '35577');
INSERT INTO `ota_district` VALUES ('35578', '襄州区', 'xiangfan', '1', '35578');
INSERT INTO `ota_district` VALUES ('35579', '博望区', 'maanshan', '1', '35579');
INSERT INTO `ota_district` VALUES ('35580', '清新区', 'qingyuan', '1', '35580');
INSERT INTO `ota_district` VALUES ('35581', '临桂区', 'guilin', '1', '35581');
INSERT INTO `ota_district` VALUES ('35582', '西市区', 'yingkou', '1', '35582');
INSERT INTO `ota_district` VALUES ('35583', '乐都区', 'haidong', '1', '35583');



-- --------------------------添加酒店前台电话  ---------------------------------
ALTER TABLE hotel_info ADD COLUMN reception_phone varchar(20) comment "酒店前台电话"


-- -----------------------------------  分销系统升级的数据变更 2019 ----------------------------------------


-- --------------------------添加消息通话的酒店扩展信息 2019-01-15 ---------------------------------
ALTER table hotel_info add column msg_notify_phone varchar(40) DEFAULT NULL comment "短信通知号码 最多可设置三个";
ALTER table hotel_info add column voice_notify_phone varchar(20) DEFAULT NULL comment "语音通知号码";


-- --------------------------  添加日志操作的相关字段  ---------------------------------
ALTER table op_log add column hotel_room_type_id bigint(20) DEFAULT NULL comment "房型id";

ALTER table op_log add column sell_time datetime comment "售卖时间";

ALTER table op_log add column op_content varchar(50) DEFAULT NULL comment "操作内容";

ALTER table op_log add column op_type int DEFAULT NULL comment "操作类型 1房量 2房价 3开关房";

ALTER table op_log add column hotel_id bigint comment "酒店id";




-- ---------------------------------- 酒店结算 --------------------------------------------
##酒店设置表
CREATE TABLE hotel_settle_setting (
id bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id' ,
hotel_id bigint(20) NOT NULL COMMENT '酒店ID',
bank_account_type varchar(50)  DEFAULT NULL COMMENT '开户银行',
bank_account_name VARCHAR(50) DEFAULT NULL COMMENT '开户名称',
bank_account_cardNumber VARCHAR (30) DEFAULT NULL COMMENT '银行卡号',
bank_sign_phone VARCHAR(20) DEFAULT NULL COMMENT '签约手机号',
settle_msg_phone VARCHAR(20) DEFAULT NULL COMMENT '结算短信提醒手机号',
commission_rate int(2) NOT NULL DEFAULT 8 COMMENT '佣金设置(平台设置)',
settle_cycle_setting int(2) DEFAULT 3 COMMENT '结算周期',
create_time TIMESTAMP  COMMENT '创建时间',
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算设置表';

##结算订单表
CREATE TABLE hotel_settle_bill(
id bigint(20) NOT NULL AUTO_INCREMENT COMMENT '结算订单' ,
hotel_id bigint(20) NOT NULL COMMENT '酒店ID',
create_time TIMESTAMP  COMMENT '记账日期',
settle_cycle int(1) DEFAULT NULL COMMENT '结算周期',
settle_bank_type VARCHAR (30) DEFAULT NULL COMMENT '结算银行',
settle_bank_account VARCHAR(20) DEFAULT NULL COMMENT '结算账户',
settle_money decimal(10,2) NULL DEFAULT NULL COMMENT '结算金额',
commission_money decimal(10,2) NULL DEFAULT NULL COMMENT '佣金金额',
settle_status   int(2) NOT NULL DEFAULT 0 COMMENT '结算状态 0 未结算，1、正在结算、2、已经结算  3、结算冻结' ,
settle_time TIMESTAMP NULL COMMENT '结算时间',
start_date TIMESTAMP COMMENT '结算开始时间',
end_date TIMESTAMP COMMENT '结算结束时间',
remark VARCHAR(50)  COMMENT '备注',
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算账单表';

-- ------------------------------- 结算订单与订单明细的关联关系表 ------------------------------------
CREATE TABLE settle_ref_order(
bill_id bigint(20) NOT NULL COMMENT '结算账单id' ,
order_id bigint(20) NOT NULL COMMENT '订单明细' ,
UNIQUE INDEX `idx_bill_order` (`bill_id`, `order_id`) USING BTREE
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算账单与订单明细的关联表';


-- ------------------------------- 订单表添加结算状态，数据冗余为了便于查询和展示 -------------------------
ALTER TABLE hotel_order ADD column settle_status  int(2) NOT NULL DEFAULT 0 COMMENT '结算状态 0 未结算，1、正在结算、2、已经结算  3、结算冻结';


-- --------------------------------添加结算设置的 银行卡的使用类型  ---------------------------
ALTER TABLE hotel_settle_setting ADD COLUMN bank_use_type INT (2) NOT NULL DEFAULT 1 COMMENT '银行卡作用类型 1 个人 2公司' AFTER hotel_id;


-- ----------------------------发票信息存储 表 -----------------------------------
DROP TABLE IF EXISTS `b_invoice`;
CREATE TABLE `b_invoice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotelId` bigint(20) NOT NULL COMMENT "酒店id",
  `buyerName` varchar(255) NOT NULL COMMENT '购买方名称',
  `buyerTaxpayerNum` varchar(255) NOT NULL COMMENT '购买方纳税人识别号',
  `buyerAddress` varchar(255) DEFAULT NULL COMMENT '购买方公司注册地址',
  `buyerTel` varchar(255) DEFAULT NULL COMMENT '购买方电话',
  `buyerBankName` varchar(255) DEFAULT NULL COMMENT '购买方开户行',
  `buyerBankPeople` varchar(255) DEFAULT NULL COMMENT '购买方基本开户名',
  `buyerBankAccount` varchar(255) DEFAULT NULL COMMENT '购买方银行账号',
  `takerName` varchar(255) DEFAULT NULL COMMENT '收票人名称',
  `takerTel` varchar(255) DEFAULT NULL COMMENT '收票人手机号',
  `takerDetailAddress` varchar(255) DEFAULT NULL COMMENT '收票人详细地址',
  `takerAddress` varchar(255) DEFAULT NULL COMMENT '收票人地址',
  `takerEmail` varchar(255) DEFAULT NULL COMMENT '收票人邮箱',
  `itemName` varchar(255) DEFAULT NULL COMMENT '主开票项目名\r\n称',
  `goodsName` varchar(255) DEFAULT NULL COMMENT '货物名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `quantity` int(255)  NULL DEFAULT '1' COMMENT '数量',
  `unitPrice` decimal(10,2) DEFAULT NULL COMMENT '单价',
  `invoiceAmount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `taxRateValue` decimal(4,2) DEFAULT NULL COMMENT '税率',
  `taxRateAmount` decimal(10,2) DEFAULT NULL COMMENT '税额',
  `type` int(2)  NULL DEFAULT '0' COMMENT '发票类型 1：电子发票  2：专票',
  `casherName` varchar(20) NOT NULL  COMMENT '收款人名称',
  `reviewerName` varchar(20) NOT NULL  COMMENT '复核人名称',
  `drawerName` varchar(20) NOT NULL  COMMENT '开票人名称',
  `state` int(2)  NULL DEFAULT '0' COMMENT '0:审查中 1：发送中 2：已发送',
  `createDate` datetime DEFAULT NULL COMMENT '申请时间',
  `updateDate` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发票抬头信息';

-- ---------------------------发票记录 表 ------------------------------------------
DROP TABLE IF EXISTS `invoice_record`;
CREATE TABLE `invoice_record` (
`id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '发票记录主键' ,
`hotel_id`  bigint(20) NOT NULL COMMENT '酒店id' ,
`bill_id`  bigint(20) NOT NULL COMMENT '结算账单id' ,
`reqSerialNo`  varchar(50) NOT NULL COMMENT '发票请求流水号' ,
`seller_payerNum`  varchar(50) NOT NULL COMMENT '开票方纳税者账号' ,
`invoice_content`  varchar(50) NOT NULL COMMENT '开票内容' ,
`show_status`  int(2) NULL DEFAULT NULL COMMENT '发票可见状态 1 分销方 2 酒店方' ,
`create_time`  TIMESTAMP NOT NULL COMMENT '创建时间' ,
PRIMARY KEY (`id`)
)ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1
ROW_FORMAT=DYNAMIC;


-----------------------------------根据hotel_order的创表方法创建hotel_getOrder表------------------------------------------------
DROP TABLE IF EXISTS `hotel_getOrder`;
CREATE TABLE `hotel_getOrder` (
  `order_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '递增主键',
  `order_no` varchar(30) NOT NULL COMMENT '订单号',
  `order_channel` int(11) NOT NULL COMMENT '渠道',
  `sell_type` int(11) NOT NULL COMMENT '销售类型：置换/代销',
  `book_time` datetime NOT NULL COMMENT '预定时间',
  `book_user` varchar(10) DEFAULT '' COMMENT '预定人',
  `book_mobile` varchar(20) DEFAULT '' COMMENT '联系人',
  `hotel_id` bigint(20) NOT NULL COMMENT '酒店ID',
  `hotel_room_type_id` bigint(20) NOT NULL COMMENT '房型',
  `room_count` int(11) NOT NULL DEFAULT '1' COMMENT '房间数',
  `check_in_time` datetime NOT NULL COMMENT '入住时间',
  `check_out_time` datetime NOT NULL COMMENT '离店时间',
  `price` decimal(8,2) NOT NULL COMMENT '价格',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '订单状态',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `uniq_idx_order` (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=180000016 DEFAULT CHARSET=utf8 COMMENT='订单信息';


-- 修改
ALTER TABLE `hotel_getOrder`
  MODIFY COLUMN `book_user` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '预定人' AFTER `book_time`;

ALTER TABLE `hotel_getOrder`
  ADD COLUMN `book_remark` varchar(100) CHARACTER SET utf8 NULL DEFAULT '' COMMENT '备注' AFTER `state`;

ALTER TABLE hotel_getOrder ADD column settle_status  int(2) NOT NULL DEFAULT 0 COMMENT '结算状态 0 未结算，1、正在结算、2、已经结算  3、结算冻结';



---------------------------------------在酒店信息表hotel_info中添加列(平台上酒店唯一的id)---------------------------------------
ALTER TABLE `hotel_info`
  ADD COLUMN `hotel_only_id` varchar(100) CHARACTER SET utf8 NULL DEFAULT '' COMMENT '平台酒店id' AFTER `hotel_name`;

---------------------------------------在用户表user中添加用户电话列user_phone---------------------------------------
ALTER TABLE `user`
  ADD COLUMN `user_phone` varchar(100) CHARACTER SET utf8 NULL DEFAULT '' COMMENT '管理员电话' AFTER `user_nick`;

---------------------------------------在酒店信息表hotel_info中添加列(ota_phone)---------------------------------------
ALTER TABLE `hotel_info`
  ADD COLUMN `ota_phone` varchar(100) CHARACTER SET utf8 NULL DEFAULT '' COMMENT 'OTA管家电话' AFTER `voice_notify_phone`;

---------------创建邀请码表----------------------
DROP TABLE IF EXISTS `code`;
CREATE TABLE `code` (
  `code_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(50) DEFAULT NULL COMMENT '邀请码',
  `code_level` varchar(50) DEFAULT NULL COMMENT '邀请码级别',
  PRIMARY KEY (`code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邀请码表';

-------------------创建用户邀请码关系表------------------------
DROP TABLE IF EXISTS `user_code`;
CREATE TABLE `user_code` (
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户ID',
  `code_id` bigint(20) unsigned NOT NULL COMMENT '邀请码ID',
  `code_shangjia` varchar(50) DEFAULT NULL COMMENT '上家的邀请码',
  `code_my` varchar(50) DEFAULT NULL COMMENT '自己的邀请码',
  UNIQUE KEY `uniq_idx` (`user_id`,`code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户邀请码关系表';

-- 修改
ALTER TABLE `hotel_getOrder`
  MODIFY COLUMN `book_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '预定人' AFTER `book_time`;
-- 修改
ALTER TABLE `hotel_order`
  MODIFY COLUMN `book_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '预定人' AFTER `book_time`;

-- 创建企业信息表
DROP TABLE IF EXISTS `business`;
CREATE TABLE `business` (
  `business_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `business_name` varchar(255) DEFAULT NULL COMMENT '企业名称',
  `legal_person` varchar(255) DEFAULT NULL COMMENT '法人',
  `legal_phone` varchar(255) DEFAULT NULL COMMENT '法人联系方式',
  `business_phone` varchar(255) DEFAULT NULL COMMENT '固定电话',
  `business_email` varchar(255) DEFAULT NULL COMMENT '电子邮件',
  `business_address` varchar(255) DEFAULT NULL COMMENT '公司地址',
  `business_type` int(11)  DEFAULT NULL COMMENT '行业类别',
  `businessOrFactory` int(11) DEFAULT NULL COMMENT '经销商/厂家',
  `img_businessLicense` varchar(255) DEFAULT NULL COMMENT '营业执照',
  `img_shopHead` varchar(255) DEFAULT NULL COMMENT '公司/店铺门头',
  `img_factoryBook` varchar(255) DEFAULT NULL COMMENT '厂家授权书',
  `finance_person` varchar(255) DEFAULT NULL COMMENT '财务负责人',
  `finance_phone` varchar(255) DEFAULT NULL COMMENT '财务联系电话',
  `bank_type` varchar(255) DEFAULT NULL COMMENT '开户银行',
  `bank_number` varchar(255) DEFAULT NULL COMMENT '行号',
  `bank_addresss` varchar(255) DEFAULT NULL COMMENT '开户行地址',
  `account_name` varchar(255) DEFAULT NULL COMMENT '开户名称',
  `account_number` varchar(255) DEFAULT NULL COMMENT '账号',
  PRIMARY KEY (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业信息';
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- 图片表
DROP TABLE IF EXISTS `picture`;
CREATE TABLE `picture`  (
  `pid` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `img_businessLicense` longblob COMMENT '营业执照',
  `img_shopHead` longblob COMMENT '公司/店铺门头',
  `img_factoryBook` longblob COMMENT '厂家授权书',
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片表';

-- 在business表中添加时间列
ALTER TABLE `business`
  ADD COLUMN `success_date` datetime DEFAULT NULL COMMENT '申请通过的时间' AFTER `account_number`;

-- 创建酒店结算表
DROP TABLE IF EXISTS `jiudian`;
CREATE TABLE `jiudian` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settle_StartAndEnd` varchar(255) DEFAULT NULL COMMENT '结算周期',
  `settle_start` varchar(255) DEFAULT NULL COMMENT '结算开始时间',
  `settle_end` varchar(255) DEFAULT NULL COMMENT '结算结束时间',
  `hotel_name` varchar(255) DEFAULT NULL COMMENT '酒店名字',
  `hotel_id` bigint(20) DEFAULT NULL COMMENT '酒店id',
  `settle_money` varchar(255) DEFAULT NULL COMMENT '结算金额',
  `commission_money` varchar(255) DEFAULT NULL COMMENT '技术服务费',
  `settle_status` varchar(255) DEFAULT NULL COMMENT '结算状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='酒店结算表';
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- 创建供应商结算表
DROP TABLE IF EXISTS `gongyingshang`;
CREATE TABLE `gongyingshang` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settle_StartAndEnd` varchar(255) DEFAULT NULL COMMENT '结算周期',
  `settle_start` varchar(255) DEFAULT NULL COMMENT '结算开始时间',
  `settle_end` varchar(255) DEFAULT NULL COMMENT '结算结束时间',
  `business_name` varchar(255) DEFAULT NULL COMMENT '供应商名称',
  `business_id` bigint(20) DEFAULT NULL COMMENT '供应商id',
  `settle_money` varchar(255) DEFAULT NULL COMMENT '结算金额',
  `commission_money` varchar(255) DEFAULT NULL COMMENT '技术服务费',
  `settle_status` varchar(255) DEFAULT NULL COMMENT '结算状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商结算表';
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- 在订单表中添加每日采购价和总采购价
ALTER TABLE `hotel_order`
  ADD COLUMN `caigou_one` decimal(8,2) DEFAULT NULL COMMENT '每单采购价' AFTER `settle_status`;
ALTER TABLE `hotel_order`
  ADD COLUMN `caigou_total` decimal(8,2) DEFAULT NULL COMMENT '总采购价' AFTER `caigou_one`;
-- 在订单临时表中添加每日采购价和总采购价
ALTER TABLE `hotel_getorder`
  ADD COLUMN `caigou_one` decimal(8,2) DEFAULT NULL COMMENT '每单采购价' AFTER `settle_status`;
ALTER TABLE `hotel_getorder`
  ADD COLUMN `caigou_total` decimal(8,2) DEFAULT NULL COMMENT '总采购价' AFTER `caigou_one`;
-- 在酒店价格设置中添加每日采购价
ALTER TABLE `hotel_price_set`
  ADD COLUMN `caigou_one` decimal(8,2) DEFAULT NULL COMMENT '每单采购价' AFTER `unbook_type`;

-- 在订单表中添加酒店房型的名字(每家酒店都不一样)
ALTER TABLE `hotel_order`
  ADD COLUMN `p_hotel_room_type_name` varchar(255) DEFAULT NULL COMMENT '酒店平台上的房型名字' AFTER `hotel_room_type_id`;
-- 在订单临时表表中添加酒店房型的名字(每家酒店都不一样)
ALTER TABLE `hotel_getorder`
  ADD COLUMN `p_hotel_room_type_name` varchar(255) DEFAULT NULL COMMENT '酒店平台上的房型名字' AFTER `hotel_room_type_id`;

-- 在订单表中添加酒店订单确认号
ALTER TABLE `hotel_order`
  ADD COLUMN `hotel_confirm_number` varchar(255) DEFAULT NULL COMMENT '酒店订单确认号' AFTER `order_no`;
-- 在订单临时表表中添加酒店订单确认号
ALTER TABLE `hotel_getorder`
  ADD COLUMN `hotel_confirm_number` varchar(255) DEFAULT NULL COMMENT '酒店订单确认号' AFTER `order_no`;

-- 在酒店表hotel_info中添加酒店是否有效列isActive
ALTER TABLE `hotel_info`
  ADD COLUMN `isActive` int(2) DEFAULT NULL COMMENT '酒店是否有效' AFTER `ota_phone`;

-- 在酒店表hotel_info中添加酒店创建时间列createTime
ALTER TABLE `hotel_info`
  ADD COLUMN `createTime` datetime DEFAULT NULL COMMENT '酒店创建时间' AFTER `isActive`;

-- 在酒店表hotel_info中添加协议置换总金额列settle_totalMoney
ALTER TABLE `hotel_info`
  ADD COLUMN `settle_totalMoney` varchar(255) DEFAULT NULL COMMENT '协议置换总金额' AFTER `createTime`;

-- 在酒店表hotel_info中添加协议置换间夜数列settle_totalNight
ALTER TABLE `hotel_info`
  ADD COLUMN `settle_totalNight` varchar(255) DEFAULT NULL COMMENT '协议置换总金额' AFTER `settle_totalMoney`;

-- 创建上线酒店结算汇总表
DROP TABLE IF EXISTS `hotel_settle_isActive`;
CREATE TABLE `hotel_settle_isActive` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotelId` bigint(20) DEFAULT NULL COMMENT '酒店ID',
  `hotel_name` varchar(50) DEFAULT NULL COMMENT '酒店名称',
  `createTime` datetime DEFAULT NULL COMMENT '酒店创建时间',
  `settle_totalMoney` varchar(255) DEFAULT NULL COMMENT '协议置换总金额',
  `settle_alreadyMoney` varchar(255) DEFAULT NULL COMMENT '已置换总金额',
  `settle_haveMoney` varchar(255) DEFAULT NULL COMMENT '剩余置换总金额',
  `settle_totalNight` varchar(255) DEFAULT NULL COMMENT '协议置换间夜数',
  `settle_alreadyNight` varchar(255) DEFAULT NULL COMMENT '已置换间夜',
  `settle_haveNight` varchar(255) DEFAULT NULL COMMENT '剩余置换间夜',
  `order_totalMoney` varchar(255) DEFAULT NULL COMMENT '实际订单总金额',
  `commission_rate` int(2) DEFAULT NULL COMMENT '佣金率',
  `commission` varchar(255) DEFAULT NULL COMMENT '佣金',
  `needPay_money` varchar(255) DEFAULT NULL COMMENT '应付',
  `alreadyPay_money` varchar(255) DEFAULT NULL COMMENT '已付',
  `havePay_money` varchar(255) DEFAULT NULL COMMENT '余额',
   `todayTime` datetime DEFAULT NULL COMMENT '今天的时间记录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='上线酒店结算汇总表';
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- 下线酒店结算汇总表
DROP TABLE IF EXISTS `hotel_settle_isNotActive`;
CREATE TABLE `hotel_settle_isNotActive` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotelId` bigint(20) DEFAULT NULL COMMENT '酒店ID',
  `hotel_name` varchar(50) DEFAULT NULL COMMENT '酒店名称',
  `createTime` datetime DEFAULT NULL COMMENT '酒店创建时间',
  `settle_totalMoney` varchar(255) DEFAULT NULL COMMENT '协议置换总金额',
  `settle_alreadyMoney` varchar(255) DEFAULT NULL COMMENT '已置换总金额',
  `settle_haveMoney` varchar(255) DEFAULT NULL COMMENT '剩余置换总金额',
  `settle_totalNight` varchar(255) DEFAULT NULL COMMENT '协议置换间夜数',
  `settle_alreadyNight` varchar(255) DEFAULT NULL COMMENT '已置换间夜',
  `settle_haveNight` varchar(255) DEFAULT NULL COMMENT '剩余置换间夜',
  `order_totalMoney` varchar(255) DEFAULT NULL COMMENT '实际订单总金额',
  `commission_rate` int(2) DEFAULT NULL COMMENT '佣金率',
  `commission` varchar(255) DEFAULT NULL COMMENT '佣金',
  `needPay_money` varchar(255) DEFAULT NULL COMMENT '应付',
  `alreadyPay_money` varchar(255) DEFAULT NULL COMMENT '已付',
  `havePay_money` varchar(255) DEFAULT NULL COMMENT '余额',
   `todayTime` datetime DEFAULT NULL COMMENT '今天的时间记录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='下线酒店结算汇总表';
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- 在酒店表hotel_info中添加销售类型hotelType,1--a类  2--b类
ALTER TABLE `hotel_info`
  ADD COLUMN `hotelType` int(2) DEFAULT NULL COMMENT '销售类型' AFTER `settle_totalNight`;

  -- 在酒店表hotel_info中添加包房总金额baoFang_totalMoney
ALTER TABLE `hotel_info`
  ADD COLUMN `baoFang_totalMoney` varchar(255) DEFAULT NULL COMMENT '包房总金额' AFTER `hotelType`;

  -- 在酒店表hotel_info中添加投资人分润比例touZiRen_rates
ALTER TABLE `hotel_info`
  ADD COLUMN `touZiRen_rates` varchar(255) DEFAULT NULL COMMENT '投资人分润比例' AFTER `baoFang_totalMoney`;

-- 修改op_log中op_content操作内容的长度
ALTER TABLE `op_log`
  MODIFY COLUMN `op_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作内容' AFTER `sell_time`;

-- 在hotel_order和hotel_getorder订单表中添加间夜数列
ALTER TABLE `hotel_order`
  ADD COLUMN `nightC` int(11) DEFAULT NULL COMMENT '间夜数' AFTER `check_out_time`;
ALTER TABLE `hotel_getorder`
  ADD COLUMN `nightC` int(11) DEFAULT NULL COMMENT '间夜数' AFTER `check_out_time`;

-- 在hotel_order和hotel_getorder订单表中添加订单备注列(针对拆单问题)
ALTER TABLE `hotel_order`
  ADD COLUMN `remark_one` varchar(255) DEFAULT NULL COMMENT '订单备注' AFTER `caigou_total`;
ALTER TABLE `hotel_getorder`
  ADD COLUMN `remark_one` varchar(255) DEFAULT NULL COMMENT '订单备注' AFTER `caigou_total`;

-- 创建首页昨日/今日/汇总/酒店统计静态表
DROP TABLE IF EXISTS `charts`;
CREATE TABLE `charts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_today_size` varchar(50) DEFAULT NULL COMMENT '今日订单数',
  `order_today_night` varchar(50) DEFAULT NULL COMMENT '今日间夜数',
  `order_today_price` varchar(50) DEFAULT NULL COMMENT '今日订单额',
  `order_yesterday_size` varchar(50) DEFAULT NULL COMMENT '昨日订单数',
  `order_yesterday_night` varchar(50) DEFAULT NULL COMMENT '昨日间夜数',
  `order_yesterday_price` varchar(50) DEFAULT NULL COMMENT '昨日订单额',
  `order_all_size` varchar(50) DEFAULT NULL COMMENT '汇总订单总数',
  `order_all_night` varchar(50) DEFAULT NULL COMMENT '汇总总间夜数',
  `order_all_price` varchar(50) DEFAULT NULL COMMENT '汇总总订单额',
  `hotel_all_IsActive` varchar(50) DEFAULT NULL COMMENT '在线酒店数量',
  `hotel_all_NotIsActive` varchar(50) DEFAULT NULL COMMENT '下线酒店数量',
  `hotel_all_addInThisMonth` varchar(50) DEFAULT NULL COMMENT '本月新增酒店',
  `todayTime` datetime DEFAULT NULL COMMENT '今天的时间记录',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='昨日/今日/汇总/酒店统计静态表';



-- 在role角色表中添加角色描述列
ALTER TABLE `role`
  ADD COLUMN `role_description` varchar(255) DEFAULT NULL COMMENT '角色描述' AFTER `role_name`;
--在role中添加更新
UPDATE `role` SET  `role_description` = ' 超级管理员' WHERE `role_id` = 1;
UPDATE `role` SET  `role_description` = ' 管理员' WHERE `role_id` = 2;
UPDATE `role` SET  `role_description` = ' 酒店' WHERE `role_id` = 3;
INSERT INTO `role`( `role_name`, `role_description`) VALUES ( 'ROLE_SUPPLIER', '供应商');
INSERT INTO `role`( `role_name`, `role_description`) VALUES ( 'ROLE_OTA','运营');
INSERT INTO `role`( `role_name`, `role_description`) VALUES ( 'ROLE_FINANCE', '财务');
INSERT INTO `role`( `role_name`, `role_description`) VALUES ( 'ROLE_INVESTOR', '投资人');
INSERT INTO `role`( `role_name`, `role_description`) VALUES ( 'ROLE_SALE', '销售');

-- 菜单表
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `menu_url` varchar(50) DEFAULT NULL COMMENT '菜单的访问路径(url)',
  `menu_parentID` varchar(50) DEFAULT NULL COMMENT '父菜单ID',
  `level` varchar(50) DEFAULT NULL COMMENT '菜单级别',
   PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';


-- 角色菜单关联表
DROP TABLE IF EXISTS `role_menu`;
create table `role_menu` (
  `role_id` BIGINT UNSIGNED not null COMMENT '角色ID',
  `menu_id` BIGINT UNSIGNED not null COMMENT '菜单ID',
  UNIQUE INDEX uniq_idx (role_id,menu_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单关联表';


-- 权限表
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(50) DEFAULT NULL COMMENT '权限名称',
  `permission_description` varchar(50) DEFAULT NULL COMMENT '权限描述',
   PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';
-- 角色权限关联表
DROP TABLE IF EXISTS `role_permission`;
create table `role_permission` (
  `role_id` BIGINT UNSIGNED not null COMMENT '角色ID',
  `permission_id` BIGINT UNSIGNED not null COMMENT '权限ID',
  UNIQUE INDEX uniq_idx (role_id,permission_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限关联表';

-- 在user表中添加wx_openid用户微信openid字段
ALTER TABLE `user`
  ADD COLUMN `wx_openid` varchar(255) DEFAULT NULL COMMENT '用户微信openid' AFTER `status`;

  --删除user_role表中多余的关联,目前只是一对一
  	delete from user_role where user_id=1 and role_id=2;
		delete from user_role where user_id=2 and role_id=2;
		delete from user_role where user_id=3 and role_id=2;


ALTER TABLE `user`
  ADD COLUMN `role_name` varchar(100) DEFAULT NULL COMMENT '用户权限名称' AFTER `wx_openid`;

-- 将管理员名字改成供应商,因为创建的供应商账户是role_id=2
UPDATE `role` SET `role_name` = 'ROLE_ADMIN', `role_description` = '供应商' WHERE `role_id` = 2;

-- 删除原先的供应商角色
delete from role where `role_id` = 4;

--user表添加role_name字段
ALTER TABLE `user`
  ADD COLUMN `role_name` varchar(100) DEFAULT NULL COMMENT '用户权限名称' AFTER `wx_openid`;

--清空user表wx_openid字段
UPDATE user set wx_openid = null;

-- 创建酒店供应商关联表
DROP TABLE IF EXISTS `hotel_supplier`;
CREATE TABLE `hotel_supplier` (
  `hotel_supplier_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) DEFAULT NULL COMMENT '酒店ID',
  `business_id` bigint(20) DEFAULT NULL COMMENT '供应商ID',
  `hotelType` int(2) DEFAULT NULL COMMENT '销售类型:A类置换/B类分销',
  `commission_rate` int(2) DEFAULT NULL COMMENT '佣金比例',
  `settle_type` int(2) DEFAULT NULL COMMENT '置换类型;0--按照金额;1--按间夜',
  `settle_totalMoney` varchar(50) DEFAULT NULL COMMENT '协议置换总金额',
  `settle_totalNight` varchar(50) DEFAULT NULL COMMENT '协议置换间夜数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间:酒店和供应商关联的时间',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间:点上线按钮',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间:点下线按钮',
  `hotel_supplier_state` int(11) NOT NULL DEFAULT '0' COMMENT '状态:0待上线/1在线/2下线 默认是0',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `ext` varchar(255) DEFAULT NULL COMMENT '扩展项',
   PRIMARY KEY (`hotel_supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='酒店供应商关联表';

-- 修改销售类型列默认为1--A类置换
ALTER TABLE `hotel_supplier`
  MODIFY COLUMN `hotelType` int(2) DEFAULT '1' COMMENT '销售类型:1--A类置换;2--B类分销  ';

-- 在hotel_order中添加字段hotel_supplier_id,commission_rate,settle_money
ALTER TABLE `hotel_order`
ADD COLUMN `hotel_supplier_id` bigint(20) DEFAULT NULL COMMENT '酒店供应商关联id' AFTER `remark_one`;
ALTER TABLE `hotel_order`
ADD COLUMN `commission_rate` int(2) DEFAULT NULL COMMENT '佣金' AFTER `hotel_supplier_id`;
ALTER TABLE `hotel_order`
ADD COLUMN `settle_money` varchar(20) DEFAULT NULL COMMENT '结算金额' AFTER `commission_rate`;


-- 在hotel_getorder中添加字段hotel_supplier_id,commission_rate,settle_money
ALTER TABLE `hotel_getorder`
ADD COLUMN `hotel_supplier_id` bigint(20) DEFAULT NULL COMMENT '酒店供应商关联id' AFTER `remark_one`;
ALTER TABLE `hotel_getorder`
ADD COLUMN `commission_rate` int(2) DEFAULT NULL COMMENT '佣金' AFTER `hotel_supplier_id`;
ALTER TABLE `hotel_getorder`
ADD COLUMN `settle_money` varchar(20) DEFAULT NULL COMMENT '结算金额' AFTER `commission_rate`;

-- 在酒店表中修改销售类型列默认为1--A类置换
ALTER TABLE `hotel_info`
  MODIFY COLUMN `hotelType` int(2) DEFAULT '1' COMMENT '销售类型:1--A类置换;2--B类分销  ';

-- 创建order_caigou_one表,订单每天的结算单价统计
DROP TABLE IF EXISTS `order_caigou_one`;
CREATE TABLE `order_caigou_one` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20)NOT NULL COMMENT '订单ID',
  `check_in_time` date DEFAULT NULL COMMENT '入住时间:2020-03-18',
  `caigou_one` decimal(8,2) DEFAULT NULL COMMENT '每单采购价',
  `caigou_one_state` int(11) NOT NULL DEFAULT '0' COMMENT '订单状态:0-正常; 1-取消',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单的结算单价日期列表';


-- ①酒店评估表
DROP TABLE IF EXISTS `hotel_evaluation`;
CREATE TABLE `hotel_evaluation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20)NOT NULL COMMENT '当前用户ID',
  `hotel_name` varchar(255) DEFAULT NULL COMMENT '酒店名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `evaluation_time` datetime DEFAULT NULL COMMENT '评估时间',
  `evaluation_state` int(11)  DEFAULT NULL COMMENT '评估状态:0-正在评估; 1-评估完成',
  `businessLicense` int(11)  DEFAULT NULL COMMENT '营业执照:0-没有; 1-有',
  `fireLicense` int(11)  DEFAULT NULL COMMENT '消防证:0-没有; 1-有',
  `specialLicense` int(11)  DEFAULT NULL COMMENT '特种行业许可证:0-没有; 1-有',
  `publicWork` int(11)  DEFAULT NULL COMMENT '市政工程/临时封路:0-没有; 1-有',
  `agent` int(11)  DEFAULT NULL COMMENT '代理:0-没有; 1-有; 2-有两家',
  `sale_average` int(11) DEFAULT NULL COMMENT '月均销售额',
  `orderCount_average` int(11) DEFAULT NULL COMMENT '月均订单数',
  `nightC_average` int(11) DEFAULT NULL COMMENT '月均间夜数',
  `settle_average` int(11) DEFAULT NULL COMMENT '月均结算额',
  `scoreValue` int(11) DEFAULT NULL COMMENT '评分值',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='酒店评估表';

-- ②OTA数据表
DROP TABLE IF EXISTS `ota_data`;
CREATE TABLE `ota_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20)NOT NULL COMMENT '酒店ID',
  `platformCode` int(11) DEFAULT NULL COMMENT '平台编码',
  `platformName` varchar(255)  DEFAULT NULL COMMENT '平台名称',
  `platformNumber` varchar(255)  DEFAULT NULL COMMENT '账号',
  `platformPassword` varchar(255)  DEFAULT NULL COMMENT '密码',
  `code` varchar(255)  DEFAULT NULL COMMENT 'cookie/验证码',
  `platformHotelName` varchar(255) DEFAULT NULL COMMENT '平台酒店名称',
  `otaGrade` varchar(255) DEFAULT NULL COMMENT 'OTA评级(豪华型/舒适型等)',
  `otaScore` varchar(255) DEFAULT NULL COMMENT 'OTA评分',
  `commentTotal` int(11) DEFAULT NULL COMMENT '点评总数',
  `badCommentTotal` int(11) DEFAULT NULL COMMENT '差评数量',
  `renovation_time` datetime DEFAULT NULL COMMENT '装修时间',
  `hotelAddress` varchar(255) DEFAULT NULL COMMENT '酒店地址',
  `roomCount` int(11) DEFAULT NULL COMMENT '客房总数(房间数)',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OTA数据表';
-- ③房型房价表
DROP TABLE IF EXISTS `room_type_price`;
CREATE TABLE `room_type_price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20)NOT NULL COMMENT '酒店ID',
  `room_type_name` varchar(255) DEFAULT NULL COMMENT '房型名字',
  `room_price_average` int(11)DEFAULT NULL COMMENT '此房型平均房价',
  `settle_average` int(11)DEFAULT NULL COMMENT '平均结算价',
  `nightC` int(11)DEFAULT NULL COMMENT '间夜数',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房型房价表';
-- ④评价体系表
DROP TABLE IF EXISTS `evaluation_system`;
CREATE TABLE `evaluation_system` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) DEFAULT NULL COMMENT '项目名称',
  `project_code` varchar(255) DEFAULT NULL COMMENT '项目代码',
  `value` int(11)DEFAULT NULL COMMENT '定值(状态)',
  `minValue` int(11)DEFAULT NULL COMMENT '最小值',
  `maxValue` int(11)DEFAULT NULL COMMENT '最大值',
  `score` int(11)DEFAULT NULL COMMENT '分值',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评价体系表';
-- ⑤分值表
DROP TABLE IF EXISTS `hotel_score`;
CREATE TABLE `hotel_score` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20)NOT NULL COMMENT '酒店ID',
  `project_name` varchar(255) DEFAULT NULL COMMENT '项目名称',
  `score` int(11)DEFAULT NULL COMMENT '分值',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分值表';